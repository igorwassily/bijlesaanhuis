<?php
$thisPage="Approve Teachers";
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Pending Teachers</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
	require_once('includes/connection.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        
                        
                    </div>
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="teachers_approve" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>Email</th>
                                        <th>Full Name</th>
                                        <th>Date of Birth</th>
                                        <th>Telephone</th>
                                        <th>Full Address</th>
										
										<th>Motivation</th>
										<th>Passport</th>
										<th>School Document</th>
                                        <th>Available for Online Teaching?</th>
										
										<th>Courses Going to Teach</th>
										<th>Update</th>
										
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
											$stmt = $con->prepare("select contact.email, contact.firstname, contact.lastname, contact.dateofbirth, contact.telephone, contact.address, contact.postalcode, contact.city, contact.country, contact.userID, teacher.onlineteaching, user.active, contact.latitude, contact.longitude, contact.place_name  from contact INNER JOIN teacher on (teacher.contactID = contact.contactID) INNER JOIN user on (user.userID = contact.userID) INNER join usergroup on (user.usergroupID = usergroup.usergroupID) where usergroup.usergroupname = ? and   user.active = ? GROUP by user.userID order by user.userID desc");
										
	$usergroup = 'Teacher';
	$active = 0;
					$stmt->bind_param("si",$usergroup,$active);
					$stmt->execute();
					$stmt->bind_result($email,$firstname,$lastname,$dob,$telephone,$address,$postalcode,$city,$country,$userid,$onlineteaching,$active, $lat,$lng,$place_name);
					$stmt->store_result();
					while($stmt->fetch())
					{
	
									?>
									<tr>
									
										<td><?php echo $email; ?></td>
									<td><?php echo $firstname." ".$lastname; ?></td>
										<td><?php echo $dob; ?></td>
										<td><?php echo $telephone; ?></td>
										<!-- <td><?php //echo $address.", ".$city.", ".$postalcode.", ".$country; ?></td> -->
										<td><?php echo $place_name; ?></td>
										<?php
						
											$stmt1 = $con->prepare("SELECT IDdocument, Educationdocument, Motivation from applications where userID = ?");
											$stmt1->bind_param("i",$userid);
											$stmt1->execute();
											$stmt1->bind_result($id,$education,$motivation);
											$stmt1->store_result();
											$stmt1->fetch();
										?>
										<td><?php echo $motivation; ?></td>
										<td><a href="<?php if($id != '' || $id != null) 
										{
											echo "../".$id;
										}
											else
											{
											echo 'N/A';
											}
											?>" target="_blank" style="color: white !important;">View</a></td>
										<td><a href="<?php if($education != '' || $education != null) 
										{
											echo "../".$education;
										}
											else
											{
											echo 'N/A';
											}
											?>" target="_blank" style="color: white !important;">View</a></td>
										<td class="text-center"><?php if($onlineteaching == 1)
									{
										echo "Yes"; 
									}
						else
						{
							echo "No"; 
						}
						 ?></td>
										
										
										<td>
											<?php
						
											$stmt12 = $con->prepare("select courses.coursename from courses INNER JOIN teacher_courses ON (teacher_courses.courseID = courses.courseID) INNER join user on (teacher_courses.teacherID = user.userID) where user.userID = ?");
											$stmt12->bind_param("i",$userid);
											$stmt12->execute();
											$stmt12->bind_result($coursename);
											$stmt12->store_result();
											while($stmt12->fetch())
											{
												echo $coursename.", ";
											}
						
						?>				
											
										</td>
										<td><?php if($active == 1)
									{
										echo '<a href="actions/update-teacher-status.php?userid=' . $userid . '" class="list-group-item text-center" style="color: white !important;">Deactivate</a>'; 
									}
						else
						{
							echo '<a href="actions/update-teacher-status.php?userid=' . $userid . '" class="list-group-item text-center" style="color: white !important;">Activate</a>';   
						}
						 ?></td>
									</tr>
									<?php
					}
	?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
<script type="text/javascript">
	$(document).ready(function() {
    $('#teachers_approve').DataTable();
	});
		/*{
		"processing": true,
        "serverSide": true,
        "destroy" : true,
        "pageLength": 20,
        "lengthMenu": [ [20, 50, 100], [20, 50, 100]  ],
        'ajax': 'includes/bookingsOverviewProcessing.php',
        "order": [[1, 'asc']],
        columnDefs: [
            { width: 5, targets: 0 },
            { width: 30, targets: 2 },
            { width: 50, targets: 3 },
            { width: 385, targets: 4 },
			{
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 10 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 11 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 12 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 13 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 14 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 15 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 16 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 17 ],
                "visible": false,
                "searchable": false
            },
			{
    "targets": 0,
    "data" : null,
	"className" : 'details-control',
				"render": function ( data, type, full, meta ) {
		return '';
       }
  },
			{
    "targets": 3,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 4,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 7,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 8,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 9,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 10,
    "data": 10,
    "render": function ( data, type, full, meta ) {
		arrival_flight_time = data;
		return 'N/A in DB';
       }
  },
			{
    "targets": 11,
    "data": 11,
    "render": function ( data, type, full, meta ) {
		departure_flight_time = data;
		return 'N/A in DB';
       }
  },
			
			{
    "targets": 12,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 13,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 14,
    "data": 14,
    "render": function ( data, type, full, meta ) {
		agent_id = data;
		return 'N/A in DB';
       }
  },
			{
    "targets": 15,
    "data": 15,
    "render": function ( data, type, full, meta ) {
		supplier_id = data;
		return 'N/A in DB';
       }
  },
			{
    "targets": 16,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 17,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 18,
    "data" : 1,
				"render": function ( data, type, full, meta ) {
		return '<form method="post" action = "booking-single" target="_blank"><input type="hidden" name="booking-id" id="booking-id" value="'+data+'"/><button type="submit" name="booking-single-submit" class="btn btn-warning btn-round">VIEW</button></form>';
       }
  },
			{
    "targets": 19,
    "data" : 1,
				"render": function ( data, type, full, meta ) {
		return '<form method="post" action = "edit-bookings" target="_blank"><input type="hidden" name="bookingedit-id" id="bookingedit-id" value="'+data+'"/><button type="submit" name="booking-edit-submit" class="btn btn-warning btn-round">EDIT</button></form>';
       }
  }
        ]
    } );
     
  
    $('#bookings_overview tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );
	function format ( d ) {
    return '<table class="table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="bookings_overview" cellspacing="0" width="100%" style="margin-top: -1% !important;border-collapse: separate !important;margin-left: -1%;margin-bottom: -1% !important;text-align: left !important;font-size: 10px;padding: 0% !important;">'+
       
     
        '<tr>'+
            '<td>Arr. Flight</td>'+
            '<td>'+arrival_flight_time+'</td>'+
			'<td>Dep. Flight</td>'+
            '<td>'+departure_flight_time+'</td>'+
			'<td>P/U Times</td>'+
		'<td>N/A in DB</td>'+
			'<td>Hotels</td>'+
            '<td>N/A in DB</td>'+
        '</tr>'+
		 '<tr>'+
            '<td>Agent Code</td>'+
            '<td>'+agent_id+'</td>'+
            '<td>Event</td>'+
            '<td>'+supplier_id+'</td>'+
            '<td>Event Date</td>'+
			'<td>N/A in DB</td>'+
            '<td>booked by</td>'+
			'<td>N/A in DB</td>'+
        '</tr>'+
    '</table>';
}*/
</script>

</body>
</html>
<?php
}
?>