<?php
$thisPage="Students Registration";
/*session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{*/
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Students Registration</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        //require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content" style="margin: 0px !important;">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Students Registration
                </h2>
            </div>            
            <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i>&nbsp;&nbsp;&nbsp;Back to Home</a></li>
                   
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                    
                        <form id="studentRegistration" enctype="multipart/form-data">
							<h4>Create Account</h4>
							<fieldset>
								
								<div class="form-group form-float">
									<label for="username">Username</label>
									<input type="hidden" class="form-control" placeholder="User Group" name="choice" id="choice" value="Student-Registration" />	
									<input type="hidden" class="form-control" placeholder="User Group" name="usergroup" id="usergroup" value="Student" />
									<input type="text" class="form-control" placeholder="Username" name="username" id="username" required/>
									</div>
									<div class="form-group form-float">
									<label for="username">Password</label>
									<input type="password" class="form-control" placeholder="Password" name="password" id="password" minlength="8" required/>
									</div>
									<div class="form-group form-float">
									<label for="username">Email</label>
									<input type="email" class="form-control" placeholder="Email" name="email" id="email" required/>
									</div>

							</fieldset>
                            <h4>Student Information</h4>
                            <fieldset>
								
                            
								
								<div class="form-group form-float">
									<label for="booking-code">First Name</label>
									<input type="text" class="form-control" placeholder="First Name" name="first-name" id="first-name">
                                    
                                </div>


								<div class="form-group form-float">
									<label for="booking-code">Last Name</label>
									<input type="text" class="form-control" placeholder="Last Name" name="last-name" id="last-name">
                                    
                                </div>


                                <div class="form-group form-float">
									<label for="booking-code">Address</label>
									<input type="text" class="form-control" placeholder="Address" name="address" id="address">
                                    
                                </div>
								
								 
								<div class="form-group form-float">
									<div class="row">
									 <div class="col-lg-4 col-md-4 col-sm-12">
										 <label for="adults">Postal Code</label>
                                    	<input type="text" class="form-control" placeholder="Postal Code" name="postal-code" id="postal-code">
									</div>
									
									 <div class="col-lg-4 col-md-4 col-sm-12">
										<label for="children">City</label>
                                    <input type="text" class="form-control" placeholder="City" name="city" id="city">
									</div>
									
									 <div class="col-lg-4 col-md-4 col-sm-12">
										 <label for="enfants">Telephone</label>
                                    <input type="text" class="form-control" placeholder="Telephone" name="telephone" id="telephone">
									</div>
									</div>
									
                                </div>
								
								
								<!--<div class="form-group form-float">
									 <label for="agent">Choose Arrival By</label>
                                   <select class="form-control show-tick" id="booking-arrival-by" name="booking-arrival-by">
                                    <option value="">-- Please select --</option>
                                    <option value="aeroplane">Aeroplane</option>
                                    <option value="bus">Bus</option>
                                    
                                </select>
                                </div>-->
								
                               
                            </fieldset>
                            
                           
							
							<h4>Parent Information</h4>
								
                            <fieldset>
                              	<span style="color: white;">Address Same As Student Information </span><input type="checkbox" name="copy-address" id="copy-address" /><br/><br/>
                              	<div class="form-group form-float">
									<label for="booking-code">First Name</label>
									<input type="text" class="form-control" placeholder="First Name" name="parent-first-name" id="parent-first-name">
                                    
                                </div>


								<div class="form-group form-float">
									<label for="booking-code">Last Name</label>
									<input type="text" class="form-control" placeholder="Last Name" name="parent-last-name" id="parent-last-name">
                                    
                                </div>


                                <div class="form-group form-float">
									<label for="booking-code">Address</label>
									<input type="text" class="form-control" placeholder="Address" name="parent-address" id="parent-address">
                                    
                                </div>
								
								 
								<div class="form-group form-float">
									<div class="row">
									 <div class="col-lg-4 col-md-4 col-sm-12">
										 <label for="adults">Postal Code</label>
                                    	<input type="text" class="form-control" placeholder="Postal Code" name="parent-postal-code" id="parent-postal-code">
									</div>
									
									 <div class="col-lg-4 col-md-4 col-sm-12">
										<label for="children">City</label>
                                    <input type="text" class="form-control" placeholder="City" name="parent-city" id="parent-city">
									</div>
									
									 <div class="col-lg-4 col-md-4 col-sm-12">
										 <label for="enfants">Telephone</label>
                                    <input type="text" class="form-control" placeholder="Telephone" name="parent-telephone" id="parent-telephone">
									</div>
									</div>
									
                                </div>
								
                            </fieldset>
							
                        </form>
                   
                
            </div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
	
	
	<!-- Jquery steps Wizard Code -->
	<script type="text/javascript">
	$(function () {
    //Advanced form with validation
    var form = $('#studentRegistration').show();
    form.steps({
        headerTag: 'h4',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
          //  $.AdminOreo.input.activate();
            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
			
			
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
			//Jquery AJAX code to add client upon  insertion
            
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            //Code for Forms Data Submission
			run_waitMe($('body'), 1, "timer");
			$.ajax({
                            url: 'actions/add_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {


                          
                                   switch(result)
                                    {

                                      case "Error":
                                    
                                        
                                        setTimeout(function() {   
                                                 
                                               $('body').waitMe('hide');
                                               $('#studentRegistration')[0].empty(); 
                                              showNotification("alert-danger","Sorry!!! Your account cannot be created right now.","top","center","","");
                                               //$("#error-epm").html("There Was An Error Inserting The Record."); 
                                                  
}, 500); 
                                        break;

                                        case "Success":
                                    
                                        
                                       setTimeout(function() { 

                                                  $('body').waitMe('hide');
                                                  $('#studentRegistration')[0].reset();
                                                  showNotification("alert-success","Kindly, check your email to activate your account","top","center","","");
                                                 
                                                                                                    
}, 500);
											 window.location.replace("https://tutor.dev.we-think.nl");
                                        break;

                                    
                                    }
                              }
                      });
		
        }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'confirm': {
                equalTo: '#password'
            }
        }
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}
		
function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
        message: text
    },
        {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
}

</script>	

	<script>
	/* Condition for Arrival & Departure By Value */
		$( document ).ready(function() {
	
	$("#copy-address").change(function() {
    if(this.checked) {
        

        var address = $("#address").val();
        $("#parent-address").val(address);
    }
});
});

	</script>
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
</body>
</html>
<?php
//}
?>