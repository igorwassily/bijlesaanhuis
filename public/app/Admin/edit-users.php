<?php
$thisPage="Client Edit";
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Edit User Information</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');date('Y-m-d H:i:s');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}
	label
	{
		color: white;
	}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Edit Users
                
                </h2>
            </div>            
            <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
			<div class="col-lg-3 col-md-3 col-sm-12"></div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                
                    	<?php
					
	if(isset($_POST['user-edit-submit']))
	{
		$userID = test_input($_POST['useredit-id']);
										$stmt = $con->prepare("select userID, username, loginattempts from user where userID = ?");
		$stmt->bind_param("i",$userID);
										if($stmt->execute())
										{
											
											$stmt->bind_result($uID,$uName,$loginattempts);
											$stmt->store_result();
											$stmt->fetch();
										}
	
	?>
                        <form id="frmEditBooking" enctype="multipart/form-data">
							<br/><br/><br/><br/><br/>
							<div class="form-group form-float">
									<label for="booking-code">User Name</label>
									<input type="hidden" class="form-control" placeholder="Booking Code" name="choice" id="choice" value="EditUsers">
                                    <input type="text" class="form-control" placeholder="Booking Code" name="user-name" id="user-name" value = "<?php echo $uName; ?>">
									<input type="hidden" class="form-control" placeholder="Booking ID" name="user-id" id="user-id" value = "<?php echo $uID; ?>">
                                </div>
								
								<div class="form-group form-float">
									<label for="booking-code">Login Attempts</label>
									<input type="text" class="form-control" placeholder="Login Attempts" name="login-attempts" id="login-attempts" value = "<?php echo $loginattempts; ?>">
                                    
                                </div>
								
								 
								
									
									
							<button type="submit" name="user-edit-submit" class="btn btn-warning btn-round text-center">UPDATE</button>
							
                        </form> 
                   <?php
	}
	?>
                
            </div>
			<div class="col-lg-3 col-md-3 col-sm-12"></div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script>
	<script src="assets/bundles/vendorscripts.bundle.js"></script>
	
	<!-- Jquery steps Wizard Code -->
	<script type="text/javascript">
	
		$(document).ready(function(){ 
		//Update Project Manager
 function manualValidateUpdate(ev) {
     if(ev.target.checkValidity()==true)
     {
     ev.preventDefault();
     
    run_waitMe($('body'), 1, "timer");
     //$("#modal-projectmanager").modal('hide');
     /*$.loader({
            className:"blue-with-image",
            content:''
          });*/
     
                      $.ajax({
                            url: 'actions/edit_scripts.php',
                            type: 'POST',
                           data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {


                      
                                    switch(result)
                                    {

                                      case "Error":
                                    
                                        
                                        setTimeout(function() {   
                                                 
                                               $('body').waitMe('hide'); 
										   
                                               $('#employeeedit-form')[0].empty(); 
                                               $("#eresult").html(
        '<div id="error-eem" class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'There Was An Error Updating The Record.' + 
         '</div>');
                                               //$("#error-epm").html("There Was An Error Inserting The Record."); 
                                                  
}, 500); 
                                        break;

                                        case "Success":
                                    
                                        
                                        setTimeout(function() { 
                                                    $('body').waitMe('hide');
                                                    
											 window.location.replace("https://dev.we-think.nl/users-overview");
}, 500); 

                                        break;

                                    
                                    }
                              }
                      });
      }
 }$("#frmEditBooking").bind("submit", manualValidateUpdate);
			
		});
	
	</script>
	<script>
  $(function() { $( "#booking-arrival-date" ).datepicker(); });
  </script>
	<script>
  $(function() { $( "#booking-departure-date" ).datepicker(); });
  </script>
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
</body>
</html>
<?php
}
?>