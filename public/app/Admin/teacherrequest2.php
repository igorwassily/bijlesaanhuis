<?php
$thisPage="View teacher requests2";
require_once('includes/connection.php');
session_start();

if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{
	define("NOTIFICATION_REQUEST_LEVEL", 4);

	$query = "SELECT t.userID AS `teacherID`, t.requested_teacherlevel_rate_ID, CONCAT(c.firstname,' ' ,c.lastname)teachername, (SELECT `level` FROM teacherlevel_rate WHERE ID=t.requested_teacherlevel_rate_ID)rlevel, (SELECT `level` FROM teacherlevel_rate WHERE ID=t.teacherlevel_rate_ID)clevel, c.place_name,  a.Motivation FROM teacher t, applications a, contact c WHERE permission='Requested' AND c.userID=t.userID AND a.userID=t.userID ORDER BY teacherName";

	$result = mysqli_query($con, $query);

	$str = "";
	$i=0;
	while($row = mysqli_fetch_assoc($result)){
		$q2 = $con->prepare( 'INSERT INTO admin_msg_read(type,userID) VALUES("'.NOTIFICATION_REQUEST_LEVEL.'", "'.$row[teacherID].'")');
		$q2->execute();
		$i++;
		$str .= "<tr>
                        <td class='color'>$i</td>
                        <td><a href='/teacher-profile/?tid=$row[teacherID]'>$row[teachername]</a></td>
                        <!--<td>$row[place_name]</td>-->
                        <td class='color'>$row[clevel]</td>
                        <td class='color'>$row[rlevel]</td>
						<td class='color'>$row[Motivation]</td>
						<td class='color'><a href='/app/Admin/admin_edit_tutor_profile?tid=$i'>Profiel</a></td>
                        <td>
							<button type=\"button\" class=\"inline-elem-button btn btn-raised btn-primary btn-round waves-effect\" value=\"accept\" onclick=\"acceptLevel(this, $row[teacherID], $row[requested_teacherlevel_rate_ID])\" >Accepteren</button>
                            <button type=\"button\" class=\"inline-elem-button btn btn-raised btn-warning btn-round waves-effect\" value=\"cancel\" onclick=\"cancelLevel(this, $row[teacherID])\" >Afwijzen</button> 
                        </td>
                        </tr>
                        ";
	}


	?>

	<!doctype html>
	<html class="no-js " lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
		<title>View Courses</title>
		<?php
		require_once('includes/mainCSSFiles.php');
		?>
		<link href="/app/Admin/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
		<link href="/app/Admin/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
		<link rel="stylesheet" href="/app/Admin/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
		<link href='/app/Admin/assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
		<link href='/app/Admin/assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
		<link rel="stylesheet" href="/app/Admin/assets/css/bootstrap-multiselect.css" type="text/css">
		<style type="text/css">

			/*Placeholder Color */
			input{
				border: 1px solid #bdbdbd !important;

				color: white !important;
			}

			select{
				border: 1px solid #bdbdbd !important;

				color: black !important;
			}

			input:focus{
				background:transparent !important;
			}

			select:focus{
				background:transparent !important;
			}

			.wizard .content
			{
				/*overflow-y: hidden !important;*/
			}

			.wizard .content label {

				color: white !important;

			}
			.wizard>.steps .current a
			{
				background-color: #029898 !important;
			}
			.wizard>.steps .done a
			{
				background-color: #828f9380 !important;
			}
			.wizard>.actions a
			{
				background-color: #029898 !important;
			}
			.wizard>.actions .disabled a
			{
				background-color: #eee !important;
			}

			.btn.btn-simple{
				border-color: white !important;
			}
			.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
				color: white;
			}

			table
			{
				color: white;
			}
			.multiselect.dropdown-toggle.btn.btn-default
			{
				display: none !important;
			}

			.navbar.p-l-5.p-r-5
			{
				display: none !important;
			}

			input[type="text"] {
				height: 40px !important;
			}
			.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
				background-color: transparent !important;
			}

			.bootstrap-select[disabled] button
			{
				color: gray !important;
				border: 1px solid gray !important;
			}

			table, th
			{
				text-align: center !important;
			}
			.hide{
				display:none;
			}
			ul.dropdown-menu.inner {
				display: block;
			}

			ul.dropdown-menu.inner li a span{color: black;}


		</style>

	</head>
	<body class="theme-green">
	<!-- Page Loader -->
	<div class="page-loader-wrapper">
		<div class="loader">
			<div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
			<p>Please wait...</p>
		</div>
	</div>
	<!-- Overlay For Sidebars -->
	<div class="overlay"></div>


	<?php
	require_once('includes/header.php');
	require_once('includes/sidebarAdminDashboard.php');
	?>

	<!-- Main Content -->
	<section class="content page-calendar" style="margin-top: 0px !important;">
		<div class="block-header">
			<?php require_once('includes/adminTopBar.php'); ?>
		</div>
		<!-- Newly pasted code  Starts -->
		<div class="container-fluid">
			<div class="row clearfix">
				<div class="col-xl-12 col-lg-12 col-md-12">
					<div class="card">

						<div class="body">
							<!-- Nav tabs -->

							<!-- Tab panes -->
							<div class="tab-content">
								<div role="tabpanel" class="tab-pane" style="display: block !important;"> <b>Aangevaagde docenttypen</b>
									<!-- Exportable Table -->
									<div class="row clearfix">
										<div class="col-lg-12">
											<div class="card">
												<div class="body">
													<table class="table table-bordered table-striped table-hover dataTable">
														<thead>
														<tr>
															<th class="color">#</th>
															<th class="color">Naam docent</th>
															<!-- <th>Address</th> -->
															<th class="color">Huidig type</th>
															<th class="color">Nieuw type</th>
															<th class="color">Motivatie</th>
															<th class="color">Profiel</th>
															<th></th>
														</tr>
														</thead>
														<tfoot>
														<tr>
															<th class="color">#</th>
															<th class="color">Naam docent</th>
															<!-- <th>Address</th> -->
															<th class="color">Huidig type</th>
															<th class="color">Nieuw type</th>
															<th class="color">Motivatie</th>
															<th class="color">Profiel</th>
															<th></th>
														</tr>
														</tfoot>
														<tbody>
														<?php echo $str; ?>
														</tbody>
													</table>
												</div>
											</div>
										</div>
									</div>
									<!-- #END# Exportable Table -->
								</div>

							</div>
						</div>
					</div>

				</div>

			</div>
		</div>
		<!-- Newly pased Code ends -->
	</section>
	<?php
	require_once('includes/footerScripts.php');
	?>
	<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
	<script>

        var __obj= null;


        function cancelLevel(obj, tid){
            __obj=obj;
            $.ajax({
                url: 'techerlevel_processing.php',
                type: 'POST',
                data: {page:"cancel_level", tid:tid},
                success: function (result) {
                    if(result == "success"){
                        showNotification("alert-success", "Teacher Level has been Rejected", "bottom", "center", "", "");
                        $(__obj).parent().parent().remove();
                    }else{
                        showNotification("alert-danger", "Fail to Reject Teacher Level. Try later", "bottom", "center", "", "");
                    }
                }
            });  //ajax ends
        } //cancelLevel() ends

        function acceptLevel(obj, tid, rtlID){
            __obj=obj;
            $.ajax({
                url: 'techerlevel_processing.php',
                type: 'POST',
                data: {page:"accept_level", tid:tid, rtlID:rtlID},
                success: function (result) {
                    if(result == "success"){
                        showNotification("alert-success", "Teacher Level has been Accepted", "bottom", "center", "", "");
                        $(__obj).parent().parent().remove();
                    }else{
                        showNotification("alert-danger", "Fail to Accept Teacher Level. Try later", "bottom", "center", "", "");
                    }
                }
            });  //ajax ends
        } //acceptLevel() ends

	</script>
	<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
	<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
	</body>
	</html>
	<?php
}
?>
