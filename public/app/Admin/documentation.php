<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>Documentation</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Documentation
                
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Documentation</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
		  <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
						<div class="input-group">                
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-addon">
                    <i class="zmdi zmdi-search"></i>
                </span>
            </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
							<div class="row clearfix">
								<p>To begin your search,what topic can we help you with today?</p>
								</div>
								<div class="row clearfix">
								
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">alarm</i> <br><span class="icon-name">alarm</span> </div>
                                </div>
							</div>
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">backup</i> <br><span class="icon-name">backup</span> </div>
                                </div>
							</div>
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">build</i> <br><span class="icon-name">build</span> </div>
                                </div>
							</div>
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">done</i> <br><span class="icon-name">done</span> </div>
                                </div>
							</div>
						
						
						</div>
							<div class="row clearfix">
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">zoom_in</i> <br><span class="icon-name">Zoom In</span> </div>
                                </div>
							</div>
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">autorenew</i> <br><span class="icon-name">Autorenew</span> </div>
                                </div>
							</div>
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">explore</i> <br><span class="icon-name">Explore</span> </div>
                                </div>
							</div>
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">print</i> <br><span class="icon-name">Print</span> </div>
                                </div>
							</div>
						
						
						</div>
						<div class="row clearfix">
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">updates</i> <br><span class="icon-name">Updates</span> </div>
                                </div>
							</div>
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">favorite</i> <br><span class="icon-name">Favorite</span> </div>
                                </div>
							</div>
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">get_app</i> <br><span class="icon-name">Get App</span> </div>
                                </div>
							</div>
							<div class="col-lg-3 text-center">
								<div class="body m-b-10 bg-grey">
                                    <div class="demo-google-material-icon" > <i class="material-icons">delete</i> <br><span class="icon-name">Delete</span> </div>
                                </div>
							</div>
						
						
						</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
</body>
</html>
<?php
}
?>