<?php
error_reporting(0);
require_once("../includes/connection.php");
switch($_POST['choice'])
{
		/* Add School Type */
		case "SchoolType":
	
		$type = test_input($_POST['type-name']);
		$stmt02 = $con->prepare("SELECT typeName from schooltype where typeName = ?");
		$stmt02->bind_param("s",$type);
		$stmt02->execute();
		$stmt02->store_result();
		$count = $stmt02->num_rows;
		if($count > 0)
		{
			$stmt02->close();
			echo "Duplicate";
		}
		else
		{
		$stmt = $con->prepare("INSERT INTO schooltype (typeName) values (?)");
		$stmt->bind_param("s",$type);
		if($stmt->execute())
		{
			$stmt->close();
				$data = array();
			$stmt1 = $con->prepare("SELECT typeID, typeName from schooltype");
$stmt1->execute();
$stmt1->bind_result($typeID,$typeName);
$stmt1->store_result();
$data[] = array(
  'typeID'   => "",
  'typeName'   => "Select"
  );
while($stmt1->fetch())
{
	$data[] = array(
  'typeID'   =>   strval($typeID),
  'typeName'   => $typeName
  );
}
echo json_encode($data);
		}
		else
		{
			echo "Error";
		}
		}
	break;
		
		/* Add School Level */
		case "SchoolLevel":
		$level = test_input($_POST['level-name']);
		$stmt02 = $con->prepare("SELECT levelName from schoollevel where levelName = ?");
		$stmt02->bind_param("s",$level);
		$stmt02->execute();
		$stmt02->store_result();
		$count = $stmt02->num_rows;
		if($count > 0)
		{
			$stmt02->close();
			echo "Duplicate";
		}
		else
		{
		$stmt = $con->prepare("INSERT INTO schoollevel (levelName) values (?)");
		$stmt->bind_param("s",$level);
		if($stmt->execute())
		{
			$stmt->close();
				$data = array();
			$stmt1 = $con->prepare("SELECT levelID, levelName from schoollevel");
$stmt1->execute();
$stmt1->bind_result($levelID,$levelName);
$stmt1->store_result();
$data[] = array(
  'levelID'   => "",
  'levelName'   => "Select"
  );
while($stmt1->fetch())
{
	$data[] = array(
  'levelID'   =>   strval($levelID),
  'levelName'   => $levelName
  );
}
echo json_encode($data);
		}
		else
		{
			echo "Error";
		}
		}
	break;
		
		/* Add School Year */
		case "SchoolYear":
		$year = test_input($_POST['year-name']);
		$stmt02 = $con->prepare("SELECT yearName from schoolyear where yearName = ?");
		$stmt02->bind_param("s",$year);
		$stmt02->execute();
		$stmt02->store_result();
		$count = $stmt02->num_rows;
		if($count > 0)
		{
			$stmt02->close();
			echo "Duplicate";
		}
		else
		{
		$stmt = $con->prepare("INSERT INTO schoolyear (yearName) values (?)");
		$stmt->bind_param("s",$year);
		if($stmt->execute())
		{
			$stmt->close();
				$data = array();
			$stmt1 = $con->prepare("SELECT yearID, yearName from schoolyear");
$stmt1->execute();
$stmt1->bind_result($yearID,$yearName);
$stmt1->store_result();
$data[] = array(
  'yearID'   => "",
  'yearName'   => "Select"
  );
while($stmt1->fetch())
{
	$data[] = array(
  'yearID'   =>   strval($yearID),
  'yearName'   => $yearName
  );
}
echo json_encode($data);
		}
		else
		{
			echo "Error";
		}
		}
	break;
		
		
		/* Add School Year */
		case "SchoolCourse":
		$year = test_input($_POST['coursename']);
		$stmt02 = $con->prepare("SELECT courseName from schoolcourse where courseName = ?");
		$stmt02->bind_param("s",$year);
		$stmt02->execute();
		$stmt02->store_result();
		$count = $stmt02->num_rows;
		
		if($count > 0)
		{
			$stmt02->close();
			echo "Duplicate";
		}
		else
		{
		$stmt = $con->prepare("INSERT INTO schoolcourse (courseName) values (?)");
		$stmt->bind_param("s",$year);
			
		if($stmt->execute())
		{
			$stmt->close();
			$data = array();
			$stmt1 = $con->prepare("SELECT schoolcourseID, courseName from schoolcourse");
$stmt1->execute();
$stmt1->bind_result($yearID,$yearName);
$stmt1->store_result();
$data[] = array(
  'courseID'   => "",
  'courseName'   => "Select"
  );
while($stmt1->fetch())
{
	$data[] = array(
  'courseID'   =>   strval($yearID),
  'courseName'   => $yearName
  );
}
echo json_encode($data);
		}
		else
		{
			echo "Error";
		}
		}
	break;
		
		
		
		/* Add School Year */
		case "Course":
		if($_POST['selected-school-type']== ELEMENTRY_TYPE_ID)
		{
			$coursename = test_input($_POST['course-name']);
			$schooltype = test_input($_POST['select-school-type']);
			$stmt = $con->prepare("INSERT INTO courses (coursename,coursetype) values (?,?)");
		$stmt->bind_param("si",$coursename,$schooltype);
		if($stmt->execute())
		{
			echo "Success";
		}
		else
		{
			echo "Error";
		}
		}
		else
		{
			$courseid = test_input($_POST['select-school-course']);
			$schooltype = test_input($_POST['select-school-type']);
			$schoollevel = test_input($_POST['mschool-level']);
			$schoolyear = test_input($_POST['mschool-year']);
			$coursename = test_input($_POST['mschool-course']);
		
			$stmt = $con->prepare("INSERT INTO courses (schoolcourseID,coursename,coursetype,courselevel,courseyear) values (?,?,?,?,?)");
		$stmt->bind_param("issss",$courseid, $coursename,$schooltype,$schoollevel,$schoolyear);
		if($stmt->execute())
		{
			echo "Success";
		}
		else
		{
			echo "Error";
		}	
		}
	break;
}
?>
