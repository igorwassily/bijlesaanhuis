<?php
if (isset($_POST['login'])) {
    require_once('../includes/connection.php');
    session_start();
    $stmt = $con->prepare("SELECT adminID, adminEmail, adminUser 
                                    FROM admin 
                                     WHERE adminEmail = ? 
                                     AND adminPassword = ?");
    $stmt->bind_param('ss', $username, $password);
    $username = $_POST['username'];
    $pass = $_POST['password'];
    $password = md5($pass);
    $stmt->execute();
    $stmt->bind_result($adminID, $adminemail, $adminuser);
    $stmt->store_result();
    $stmt->fetch();

    $_SESSION['AdminUser'] = '';
    $_SESSION['AdminEmail'] = '';
    $_SESSION['AdminID'] = 0;

    if ($stmt->num_rows == 1) {
        $_SESSION['AdminUser'] = $adminuser;
        $_SESSION['AdminEmail'] = $adminemail;
        $_SESSION['AdminID'] = $adminID;

        header("Location: ../dashboard");
    } else {
        header("Location: ../index.php?msg=You Have Entered An Invalid Username Or Password");
    }
}