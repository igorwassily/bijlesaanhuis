<?php
require_once('../includes/connection.php');

session_start();


if(isset($_POST['subpage'])){
	if($_POST['subpage']=="coursetype"){
			$stype = (isset($_POST['stype']))? $_POST['stype'] : "";

			$str = "SELECT  * FROM schoolcourse  WHERE 1";
			$query = mysqli_query($con, $str);

			$options = "";
			while($d = mysqli_fetch_assoc($query)){
				$options .= "<option value='$d[schoolcourseID]'>$d[coursename]</option>";
			}

			echo $options;
	}// subpage==course
	
	elseif($_POST['subpage']=="courselevel"){
		 $str = "SELECT * FROM schoollevel sl WHERE sl.levelID NOT IN (SELECT GROUP_CONCAT(c.courselevel) FROM courses c WHERE c.schoolcourseID=$_POST[cid] GROUP BY c.courseID)";
			$query = mysqli_query($con, $str);

			$options = "<option value=''>Select</option>";
			while($d = mysqli_fetch_assoc($query)){
				$options .= "<option value='$d[levelID]'>$d[levelName]</option>";
			}

			echo $options;
	}// subpage=="courselevel"  ends
	
	elseif($_POST['subpage']=="courseyear"){
		$str = "SELECT yearID, yearName FROM schoolyear sy, courses c WHERE FIND_IN_SET(sy.yearID, courseyear) AND FIND_IN_SET($_POST[clid], c.courselevel) AND c.courseID=".$_POST['cid'];
			$query = mysqli_query($con, $str);

			$options = "<option value=''>Select</option>";
			while($d = mysqli_fetch_assoc($query)){
				$options .= "<option value='$d[yearID]'>$d[yearName]</option>";
			}

			echo $options;
	}// subpage=="courseyear"  ends
	
}//isset(subpage) ends

?>