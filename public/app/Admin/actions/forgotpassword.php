<?php
if(isset($_POST['forgotpassword']))
{
require_once('../includes/connection.php');
session_start();
$stmt = $con->prepare("SELECT userID from user WHERE username=? AND  email=? AND active = ? LIMIT 1");
$stmt->bind_param('ssi', $username, $email, $active);
$username = test_input($_POST['username']);
$email = test_input($_POST['email']);
$active = 1;
if($stmt->execute())
{
$stmt->bind_result($userID);
$stmt->store_result();
$stmt->fetch();
$count = $stmt->num_rows;
if($count == 1)
{
	$stmt->close();
	$verificationcode = md5(uniqid(rand(), true));
	$stmt01 = $con->prepare("UPDATE user set pwreset_token = ? where userID = ?");
	$stmt01->bind_param("si",$verificationcode,$userID);
	if($stmt01->execute())
	{
		$to      = $email; // Send email to our user
$subject = 'Account | Password Reset'; // Give the email a subject 
$message = 'Thanks for Password Reset!
You can reset your password by pressing the url below.
------------------------
Username: '.$username.'
------------------------
 
Please click this link to reset your password:
http://tutor.dev.we-think.nl/passwordReset?email='.$email.'&hash='.$verificationcode.'
'; // Our message above including the link
                     
$headers = 'From:noreply@tutor.dev.we-think.nl' . "\r\n"; // Set from headers
if(mail($to, $subject, $message, $headers))
{

	$sentmail = 1;
}
	if($sentmail == 1)
	{
		header("Location: ../forgotPasswordSuccess");	
			
	}
	}
}
else
{
	header("Location: ../forgotPassword.php?msg=You Have Entered An Invalid Email or Username");
}
}
else
{
        header("Location: ../forgotPassword.php?msg=There is some technical problem, Kindly try later.");
}
}
?>