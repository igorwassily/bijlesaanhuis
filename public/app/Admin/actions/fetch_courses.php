<?php
$data = array();
require_once('../includes/connection.php');
$typeID = test_input($_POST['typeID']);
$stmt = $con->prepare("SELECT courseID, coursename from courses where coursetype = ? AND courselevel IS NULL AND courseyear IS NULL");
$stmt->bind_param("i",$typeID);
$stmt->execute();
$stmt->bind_result($courseID,$courseName);
$stmt->store_result();
$data[] = array(
  'courseID'   => "",
  'courseName'   => "Select"
  );
while($stmt->fetch())
{
	$data[] = array(
  'courseID'   =>   strval($courseID),
  'courseName'   => $courseName
  );
}
echo json_encode($data);
?>