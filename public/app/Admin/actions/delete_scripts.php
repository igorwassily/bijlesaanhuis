<?php

require_once("../includes/connection.php");

switch($_POST['choice'])
{
	case "SchoolType" :
	$stmt = $con->prepare("DELETE from schooltype where typeID = ?");
	$id= test_input($_POST['id']);
	$stmt->bind_param("i",$id);
	if($stmt->execute())
	{
		echo "Success";
	}
	else
	{
		echo "Error";
	}
	break;
		
		case "SchoolLevel" :
	$stmt = $con->prepare("DELETE from schoollevel where levelID = ?");
	$id= test_input($_POST['id']);
	$stmt->bind_param("i",$id);
	if($stmt->execute())
	{
		echo "Success";
	}
	else
	{
		echo "Error";
	}
	break;
		
		case "SchoolYear" :
	$stmt = $con->prepare("DELETE from schoolyear where yearID = ?");
	$id= test_input($_POST['id']);
	$stmt->bind_param("i",$id);
	if($stmt->execute())
	{
		echo "Success";
	}
	else
	{
		echo "Error";
	}
	break;
		
		case "SchoolCourse" :
	$stmt = $con->prepare("DELETE from courses where courseID = ?");
	$id= test_input($_POST['id']);
	$stmt->bind_param("i",$id);
	if($stmt->execute())
	{
		echo "Success";
	}
	else
	{
		echo "Error";
	}
	break;
		
		case "deleteSchoolCourse" :
	$stmt = $con->prepare("DELETE FROM `schoolcourse` WHERE `schoolcourseID`= ?");
	$id= test_input($_POST['id']);
	$stmt->bind_param("i",$id);
	if($stmt->execute())
	{
		echo "Success";
	}
	else
	{
		echo "Error";
	}
	break;
		
	
	case "SchoolCourse_e" :
	$stmt = $con->prepare("UPDATE `schoolcourse` SET `courseName`=? WHERE `schoolcourseID`= ?");
	$id= test_input($_POST['id']);
	$courseName= test_input($_POST['courseName']);
	$stmt->bind_param("si",$courseName,$id);
	if($stmt->execute())
	{
		echo "Success";
	}
	else
	{
		echo "Error";
	}
	break;		
}



?>