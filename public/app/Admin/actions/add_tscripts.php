<?php
error_reporting(0);
require_once("../../includes/connection.php");

$userID = "";


switch ($_POST['choice']) {

    /* Student Registration */

    case "Student-Registration":


        if ($_POST['usergroup'] != "") {
            $usergroup = test_input($_POST['usergroup']);
        } else {
            $usergroup = "";
        }

        $stmt = $con->prepare("SELECT usergroupID from usergroup where usergroupname = ?");
        $stmt->bind_param("s", $usergroup);
        $stmt->execute();
        $stmt->bind_result($usergroupID);
        $stmt->fetch();
        $stmt->close();


        $stmt1 = $con->prepare("INSERT INTO user(usergroupID,username,password,email,verificationcode,active, registered_from) values (?,?,?,?,?,?,?)");

        if ($_POST['username'] != "") {

            $username = test_input($_POST['username']);
        } else {
            $username = "";
        }


        if ($_POST['password'] != "") {

            $password = md5(test_input($_POST['password']));
        } else {
            $password = "";
        }


        if ($_POST['email'] != "") {

            $email = test_input($_POST['email']);
        } else {
            $email = "";
        }

        $verificationcode = md5(uniqid(rand(), true));
        $active = 0;


        /**
         * check if referer otherwise use the request URI
         */
        $registered_from = 'nowhere';
        if (isset($_SERVER['HTTP_REFERER'])) {
            $registered_from = $_SERVER['HTTP_REFERER'];
        } elseif (isset($_SERVER['REQUEST_URI'])) {
            $registered_from = $_SERVER['REQUEST_URI'];
        }


        $stmt1->bind_param("issssis", $usergroupID, $username, $password, $email, $verificationcode, $active, $registered_from);

        if ($stmt1->execute()) {
            $userID = $stmt1->insert_id;
            $stmt1->close();
        } else {
            echo $con->error;
        }

        $stmt2 = $con->prepare("SELECT contacttypeID from contacttype where contacttype = ?");
        $stmt2->bind_param("s", $usergroup);
        $stmt2->execute();
        $stmt2->bind_result($contacttypeID);
        $stmt2->fetch();
        $stmt2->close();

        $stmt3 = $con->prepare("INSERT INTO contact (contacttypeID,userID,prmary,firstname,lastname,telephone,email,address,postalcode,city,country,emailverified, latitude, longitude, place_name) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        if ($_POST['first-name'] != "") {

            $firstname = test_input($_POST['first-name']);
        } else {
            $firstname = "";
        }

        if ($_POST['last-name'] != "") {

            $lastname = test_input($_POST['last-name']);
        } else {
            $lastname = "";
        }

        if ($_POST['address'] != "") {

            $address = test_input($_POST['address']);
        } else {
            $address = "";
        }

        if ($_POST['postal-code'] != "") {

            $postalcode = test_input($_POST['postal-code']);
        } else {
            $postalcode = "";
        }

        if ($_POST['city'] != "") {

            $city = test_input($_POST['city']);
        } else {
            $city = "";
        }

        if ($_POST['telephone'] != "") {

            $telephone = test_input($_POST['telephone']);
        } else {
            $telephone = "";
        }

        if ($_POST['country'] != "") {

            $country = test_input($_POST['country']);
        } else {
            $country = "";
        }

        if ($_POST['lat'] != "") {

            $lat = test_input($_POST['lat']);
        } else {
            $lat = "";
        }
        if ($_POST['lng'] != "") {

            $lng = test_input($_POST['lng']);
        } else {
            $lng = "";
        }
        if ($_POST['place_name'] != "") {

            $place_name = test_input($_POST['place_name']);
        } else {
            $place_name = "";
        }

        $emailverified = 0;
        $primary = 1;

        $stmt3->bind_param("iiissssssssisss", $contacttypeID, $userID, $primary, $firstname, $lastname, $telephone, $email, $address, $postalcode, $city, $country, $emailverified, $lat, $lng, $place_name);
        if ($stmt3->execute()) {
            $contactID = $stmt3->insert_id;
            $stmt3->close();
        }

        $stmt4 = $con->prepare("INSERT INTO student (userID,contactID) values (?,?)");
        $stmt4->bind_param("ii", $userID, $contactID);
        $stmt4->execute();
        $stmt4->close();

        $contacttype = "Parent";

        $stmt2 = $con->prepare("SELECT contacttypeID from contacttype where contacttype = ?");
        $stmt2->bind_param("s", $contacttype);
        $stmt2->execute();
        $stmt2->bind_result($contacttypeID);
        $stmt2->fetch();
        $stmt2->close();

        $stmt3 = $con->prepare("INSERT INTO contact (contacttypeID,userID,prmary,firstname,lastname,telephone,email,emailverified) values (?,?,?,?,?,?,?,?)");

        if ($_POST['parent-first-name'] != "") {

            $firstname = test_input($_POST['parent-first-name']);
        } else {
            $firstname = "";
        }

        if ($_POST['parent-last-name'] != "") {

            $lastname = test_input($_POST['parent-last-name']);
        } else {
            $lastname = "";
        }


        if ($_POST['parent-telephone'] != "") {

            $telephone = test_input($_POST['parent-telephone']);
        } else {
            $telephone = "";
        }

        if ($_POST['parent-email'] != "") {

            $parent_email = test_input($_POST['parent-email']);
        } else {
            $parent_email = "";
        }


        $emailverified = 0;
        $primary = 0;

        $stmt3->bind_param("iiissssi", $contacttypeID, $userID, $primary, $firstname, $lastname, $telephone, $parent_email, $emailverified);
        if ($stmt3->execute()) {
            $contactID = $stmt3->insert_id;
            $stmt3->close();
        }

        $stmt4 = $con->prepare("INSERT INTO student (userID,contactID) values (?,?)");
        $stmt4->bind_param("ii", $userID, $contactID);
        $stmt4->execute();
        $stmt4->close();

        $pname = test_input($_POST['parent-first-name']);
        $alink = SITE_URL . 'verify?email=' . $email . '&hash=' . $verificationcode;
        $name = $firstname . " " . $pname;

//if(registration_confirmation_student($email, $name, $alink))
//{

        $sentmail = 1;

        $pname = test_input($_POST['parent-last-name']) . ", " . test_input($_POST['parent-first-name']);
        $sname = $_POST['first-name'] . " " . $_POST['last-name'];

        //student_registration_confirmation_for_parent($parent_email, $_POST['first-name'], $sname, $pname);
//}
        if ($sentmail == 1) {
            echo "Success";

        } else {
            echo "Error";
        }
        break;

    /* Teacher Registration */

    case "Teacher-Registration":


        if ($_POST['usergroup'] != "") {
            $usergroup = test_input($_POST['usergroup']);
        } else {
            $usergroup = "";
        }

        $stmt = $con->prepare("SELECT usergroupID from usergroup where usergroupname = ?");
        $stmt->bind_param("s", $usergroup);
        $stmt->execute();
        $stmt->bind_result($usergroupID);
        $stmt->fetch();
        $stmt->close();


        $stmt1 = $con->prepare("INSERT INTO user(usergroupID,username,password,email,verificationcode,active) values (?,?,?,?,?,?)");

        if ($_POST['username'] != "") {

            $username = test_input($_POST['username']);
        } else {
            $username = "";
        }


        if ($_POST['password'] != "") {

            $password = md5(test_input($_POST['password']));
        } else {
            $password = "";
        }


        if ($_POST['email'] != "") {

            $email = test_input($_POST['email']);
        } else {
            $email = "";
        }

        $verificationcode = md5(uniqid(rand(), true));
        $active = 1;

        $stmt1->bind_param("issssi", $usergroupID, $username, $password, $email, $verificationcode, $active);
        if ($stmt1->execute()) {
            $userID = $stmt1->insert_id;
            $stmt1->close();
        } else {
            echo $con->error;
        }
        if ($_POST['teacherid'] != "") {

            $calendarstatusID = test_input($_POST['teacherid']);
        } else {
            $calendarstatusID = "";
        }

        $_q = "SELECT  cb.datee, cb.endtime, cb.starttime FROM `admincalendarbooking` cb WHERE calendarstatusID=$calendarstatusID";
        $_r = mysqli_query($con, $_q);
        $_f = mysqli_fetch_assoc($_r);

        $q = "UPDATE `admincalendarbooking` SET `teacherID` = '$userID', calendarstatusID=NULL WHERE `admincalendarbooking`.`calendarstatusID`=$calendarstatusID  ";

        $r = mysqli_query($con, $q);


        $count_courses = $_POST['fieldcounter'];//count($_POST['school-course']);
        $permission = "Accepted";

        for ($i = 1; $i <= $count_courses; $i++) {
            $course = $_POST['school-course' . $i][0];
            $type = $_POST["select-school-type$i"][0];
            if ($type == ELEMENTRY_TYPE_ID) {
                $stmt10 = $con->prepare("INSERT INTO teacher_courses(teacherID,schooltypeID,courseID,permission)values(?,?,?,?)");
                $stmt10->bind_param("iiis", $userID, $type, $course, $permission);
                $stmt10->execute();
                $stmt10->close();
            } else {
                $level = $_POST['school-level' . $i][0];
                $year = implode(",", $_POST["school-year$i"]);

                $stmt10 = $con->prepare("INSERT INTO teacher_courses(teacherID,schooltypeID,courseID,schoollevelID,schoolyearID,permission)values(?,?,?,?,?,?)");
                $stmt10->bind_param("iiiiss", $userID, $type, $course, $level, $year, $permission);
                $stmt10->execute();
                $stmt10->close();
            }
        }

        $stmt2 = $con->prepare("SELECT contacttypeID from contacttype where contacttype = ?");
        $stmt2->bind_param("s", $usergroup);
        $stmt2->execute();
        $stmt2->bind_result($contacttypeID);
        $stmt2->fetch();
        $stmt2->close();

        $stmt3 = $con->prepare("INSERT INTO contact (contacttypeID,userID,prmary,firstname,lastname,dateofbirth,telephone,email,skypeID,address,postalcode,city,emailverified,latitude,longitude,place_name) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

        if ($_POST['first-name'] != "") {

            $firstname = test_input($_POST['first-name']);
        } else {
            $firstname = "";
        }

        if ($_POST['last-name'] != "") {

            $lastname = test_input($_POST['last-name']);
        } else {
            $lastname = "";
        }
        if ($_POST['dob'] != "") {

            $dob = test_input($_POST['dob']);
        } else {
            $dob = "";
        }

        if ($_POST['address'] != "") {

            $address = test_input($_POST['address']);
        } else {
            $address = "";
        }

        if ($_POST['postal-code'] != "") {

            $postalcode = test_input($_POST['postal-code']);
        } else {
            $postalcode = "";
        }

        if ($_POST['city'] != "") {

            $city = test_input($_POST['city']);
        } else {
            $city = "";
        }

        if ($_POST['telephone'] != "") {

            $telephone = test_input($_POST['telephone']);
        } else {
            $telephone = "";
        }

        if ($_POST['lat'] != "") {

            $lat = test_input($_POST['lat']);
        } else {
            $lat = "";
        }
        if ($_POST['lng'] != "") {

            $lng = test_input($_POST['lng']);
        } else {
            $lng = "";
        }
        if ($_POST['place_name'] != "") {

            $place_name = test_input($_POST['place_name']);
        } else {
            $place_name = "";
        }

        if ($_POST['skypeid'] != "") {

            $skypeid = test_input($_POST['skypeid']);
        } else {
            $skypeid = "";
        }

        $emailverified = 0;
        $primary = 1;

        $stmt3->bind_param("iiisssssssssisss", $contacttypeID, $userID, $primary, $firstname, $lastname, $dob, $telephone, $email, $skypeid, $address, $postalcode, $city, $emailverified, $lat, $lng, $place_name);
        if ($stmt3->execute()) {
            $contactID = $stmt3->insert_id;
            $stmt3->close();
        }


        $stmt4 = $con->prepare("INSERT INTO teacher (userID,contactID,onlineteaching,travelRange) values (?,?,?,?)");
        if ($_POST['online-courses'] == "Yes") {

            $onlinecourses = 1;
        } else {
            $onlinecourses = 0;
        }


        if ($_POST['distance'] != "") {

            $travelRange = test_input($_POST['distance']);
        } else {
            $travelRange = "";
        }
        $stmt4->bind_param("iiis", $userID, $contactID, $onlinecourses, $travelRange);
        $stmt4->execute();
        $teacherid = $stmt4->insert_id;
        $stmt4->close();


        $stmt5 = $con->prepare("INSERT INTO applications (userID,IDdocument,Educationdocument,Motivation,Accepted) values (?,?,?,?,?)");

        if ($_POST['motivation'] != "") {

            $motivation = test_input($_POST['motivation']);
        } else {
            $motivation = "";
        }


        if (empty($_FILES['id-card']['tmp_name'])) {
            $idcard = "N/A";
        } else {
            $idcard = "";
            $num_files = count($_FILES['id-card']['tmp_name']);
            for ($i = 0; $i < $num_files; $i++) {
                $target_dir = "../../IDCards/";
                $target_file = $target_dir . basename($_FILES["id-card"]["name"][$i]);
                $file = basename($_FILES["id-card"]["name"][$i]);
                while (file_exists($target_file)) {
                    $rand = mt_rand(1, 100);
                    $target_file = $target_dir . $rand . "-" . basename($_FILES["id-card"]["name"][$i]);
                    $file = $rand . "-" . basename($_FILES["id-card"]["name"][$i]);
                }
                if (move_uploaded_file($_FILES["id-card"]["tmp_name"][$i], $target_file)) {
                    $idcard .= "../IDCards/";
                    $idcard .= $file;
                    $idcard .= ",";
                }
            }
            $idcard = substr(trim($idcard), 0, -1);
        }

        if (empty($_FILES['school-docs']['tmp_name'])) {
            $schooldocs = "N/A";
        } else {
            $schooldocs = "";
            $num_files = count($_FILES['school-docs']['tmp_name']);
            for ($i = 0; $i < $num_files; $i++) {
                $target_dir = "../../SchoolDocs/";
                $target_file = $target_dir . basename($_FILES["school-docs"]["name"][$i]);
                $file = basename($_FILES["school-docs"]["name"][$i]);
                while (file_exists($target_file)) {
                    $rand = mt_rand(1, 100);
                    $target_file = $target_dir . $rand . "-" . basename($_FILES["school-docs"]["name"][$i]);
                    $file = $rand . "-" . basename($_FILES["school-docs"]["name"][$i]);
                }
                if (move_uploaded_file($_FILES["school-docs"]["tmp_name"][$i], $target_file)) {
                    $schooldocs .= ".../SchoolDocs/";
                    $schooldocs .= $file;
                    $schooldocs .= ",";
                }
            }
            $schooldocs = substr(trim($schooldocs), 0, -1);
        }
        $accepted = 0;

        $stmt5->bind_param("isssi", $userID, $idcard, $schooldocs, $motivation, $accepted);
        if ($stmt5->execute()) {
            $applicationsID = $stmt5->insert_id;
            $stmt5->close();
        }

        $stmt6 = $con->prepare("INSERT INTO profile (image,approved) values (?,?)");

        $accepted = 0;
        $image = "../assets/images/profile_av.jpg";
        $stmt6->bind_param("si", $image, $accepted);
        if ($stmt6->execute()) {
            $profileID = $stmt6->insert_id;
            $stmt6->close();
        }

        $stmt7 = $con->prepare("UPDATE teacher set profileID = ? where teacherID = ?");
        $stmt7->bind_param("ii", $profileID, $teacherid);
        $stmt7->execute();

        $dateTime = $_f['datee'] . ", " . $_f['starttime'] . " - " . $_f['endtime'];
        /*
        if(registration_confirmation_tutor($email, $firstname, $dateTime))
        {
            consultation_confirmation_for_tutor($email, $firstname, $dateTime, "");
            forgot_password_for_tutor($email, $firstname);
            welcome_email_for_tutors($email, $firstname);
            $sentmail = 1;
            }
        */
        $sentmail = 1;
        if ($sentmail == 1) {
            echo "Success";

        } else {
            echo "Error";
        }
        break;

    case "teacherslot":
        $teacher = test_input($_POST['teacher']);
        echo $_POST['id'];
        $title = test_input($_POST['title']);
        $start = test_input($_POST['start']);
        $end = test_input($_POST['end']);

        $d = explode("T", $start);
        $d1 = explode("T", $end);

        $date = $d[0];
        echo $weekday = date("w", strtotime($date));
        $start1 = $d[1];
        $end1 = $d1[1];
        $status = 1;
        $stmt = $con->prepare("INSERT INTO teacherslots (teacherID,title,datee,starttime,endtime,dow,status) values (?,?,?,?,?,?,?)");
        $stmt->bind_param("issssii", $teacher, $title, $date, $start1, $end1, $weekday, $status);
        if ($stmt->execute()) {

            echo "Success";
        } else {
            echo "Error";
        }


        break;

    /* Slot Reservation by Student */
    case "teacherslotreservation":
        $teacher = test_input($_POST['teacher']);
        $student = test_input($_POST['student']);
        $start = test_input($_POST['start']);
        $end = test_input($_POST['end']);

        $d = explode("T", $start);
        $d1 = explode("T", $end);

        $date = $d[0];
        $start1 = $d[1];
        $end1 = $d1[1];
        $status = 1;
        $stmt01 = $con->prepare("SELECT slotID from teacherslots where (teacherID=? AND datee = ? AND starttime = ? AND endtime = ?)");
        $stmt01->bind_param("isss", $teacher, $date, $start1, $end1);
        $stmt01->execute();
        $stmt01->bind_result($slotID);
        $stmt01->store_result();
        $stmt01->fetch();
        $stmt01->close();
        $stmt = $con->prepare("INSERT INTO calendarbooking (studentID,teacherID,slotID,datee,starttime,endtime,accepted) values (?,?,?,?,?,?,?)");
        $stmt->bind_param("iiisssi", $student, $teacher, $slotID, $date, $start1, $end1, $status);
        if ($stmt->execute()) {
            echo "Success";
        } else {
            echo "Error";
        }


        break;
}
?>
