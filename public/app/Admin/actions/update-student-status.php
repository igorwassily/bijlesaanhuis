<?php
require_once("../includes/connection.php");
$userid = test_input($_GET['userid']);
$stmt01 = $con->prepare("SELECT email, active from user where userID = ?");
$stmt01->bind_param("i",$userid);
$stmt01->execute();
$stmt01->bind_result($email, $active1);
$stmt01->store_result();
$stmt01->fetch();
$stmt01->close();
if($active1 == 1)
{
$stmt = $con->prepare("UPDATE user set active = 0 where userID = ?");
$stmt->bind_param("i",$userid);
if($stmt->execute())
{
	$to      = $email; // Send email to our user
$subject = 'Account | DeActivation'; // Give the email a subject 
$message = 'Your account has been deactivated, you can contact administrator for further actions.';
                     
$headers = 'From:noreply@tutor.dev.we-think.nl' . "\r\n"; // Set from headers
mail($to, $subject, $message, $headers);
	header('Location: ../students-list.php?User Account Deactivated.');
}
}
else if($active1 == 0)
{
$stmt = $con->prepare("UPDATE user set active = 1 where userID = ?");

$stmt->bind_param("i",$userid);
if($stmt->execute())
{
	$to      = $email; // Send email to our user
$subject = 'Account | Activation'; // Give the email a subject 
$message = 'Your account has been activated, you can now login at https://tutor.dev.we-think.nl.';
$headers = 'From:noreply@tutor.dev.we-think.nl' . "\r\n"; // Set from headers
mail($to, $subject, $message, $headers);
	header('Location: ../students-list-approved.php?User Account Activated.');
}
}
?>