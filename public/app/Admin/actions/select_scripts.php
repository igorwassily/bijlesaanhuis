<?php

require_once("../includes/connection.php");

switch($_POST['choice'])
{
	case "SchoolType" :
	$id = test_input($_POST['id']);
	$stmt = $con->prepare("SELECT * from schooltype where typeID = ?");
	$stmt->bind_param("i",$id);
	if($stmt->execute())
	{
		$stmt->bind_result($id,$name);
		$stmt->store_result();
		$stmt->fetch();
		                        echo json_encode(array('ID'=>$id , 'Name' => $name, 'stat' => "Success"));
	}
	else
	{
		echo json_encode(array('stat' => "Error"));
	}
	break;
		
		case "SchoolLevel" :
	$id = test_input($_POST['id']);
	$stmt = $con->prepare("SELECT * from schoollevel where levelID = ?");
	$stmt->bind_param("i",$id);
	if($stmt->execute())
	{
		$stmt->bind_result($id,$name);
		$stmt->store_result();
		$stmt->fetch();
		                        echo json_encode(array('ID'=>$id , 'Name' => $name, 'stat' => "Success"));
	}
	else
	{
		echo json_encode(array('stat' => "Error"));
	}
	break;
		
		case "SchoolYear" :
	$id = test_input($_POST['id']);
	$stmt = $con->prepare("SELECT * from schoolyear where yearID = ?");
	$stmt->bind_param("i",$id);
	if($stmt->execute())
	{
		$stmt->bind_result($id,$name);
		$stmt->store_result();
		$stmt->fetch();
		                        echo json_encode(array('ID'=>$id , 'Name' => $name, 'stat' => "Success"));
	}
	else
	{
		echo json_encode(array('stat' => "Error"));
	}
	break;
		
		case "SchoolCourse" :
	$id = test_input($_POST['id']);
	$stmt = $con->prepare("SELECT * from courses where courseID = ?");
	$stmt->bind_param("i",$id);
	if($stmt->execute())
	{
		$stmt->bind_result($id,$name,$type,$level,$year);
		$stmt->store_result();
		$stmt->fetch();
		                        echo json_encode(array('ID'=>$id , 'Name' => $name, 'Type' => $type, 'Level' => $level, 'Year' => $year, 'stat' => "Success"));
	}
	else
	{
		echo json_encode(array('stat' => "Error"));
	}
	break;
		
}


?>