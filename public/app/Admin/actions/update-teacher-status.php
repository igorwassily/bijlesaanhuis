<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);
ob_start();
require_once("../includes/connection.php");
require_once("../../../../inc/Email.php");
$userid = test_input($_GET['userid']);
$stmt01 = $con->prepare("SELECT email, active from user where userID = ?");
$stmt01->bind_param("i",$userid);
$stmt01->execute();
$stmt01->bind_result($email,$active1);
$stmt01->store_result();
$stmt01->fetch();
$stmt01->close();

if($active1 == 1)
{
$stmt = $con->prepare("UPDATE user set active = ? where userID = ?");
$active = 0;
$stmt->bind_param("ii",$active,$userid);
if($stmt->execute())
{
	$to      = $email; // Send email to our user
$subject = 'Account | DeActivation'; // Give the email a subject 
$message = 'Your account has been deactivated, you can contact administrator for further actions.';
                     
$headers = 'From:noreply@tutor.dev.we-think.nl' . "\r\n"; // Set from headers
	mail($to, $subject, $message, $headers);

	header('Location: ../approve-teachers.php?User Account Deactivated.');
}
}
else if($active1 == 0)
{
$stmt = $con->prepare("UPDATE user set active = ? where userID = ?");
$active = 1;
$stmt->bind_param("ii",$active,$userid);
if($stmt->execute())
{

	$q = "SELECT firstname FROM `contact` WHERE userID=".$userid;
	$r = mysqli_query($con, $q);
	$f = mysqli_fetch_assoc($r);

	$email = new Email($db, 'welcome_mail_for_tutors');
	$email->Prepare($userid);
	$email->Send('teacher');

	header('Location: ../teacher-list.php?User Account Activated.');
}
}

?>