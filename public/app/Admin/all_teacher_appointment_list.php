<?php

require_once '../../../api/endpoint/Calendar.php';

$thisPage = "all Teachers appointment list";
session_start();
if (!isset($_SESSION['AdminUser'])) {
	header('Location: index.php');
} else {
	?>

    <!doctype html>
    <html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>Teachers Appointment List</title>
		<?php
		require_once('includes/connection.php');
		require_once('includes/mainCSSFiles.php');
		?>
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
              rel="stylesheet"/>
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet'/>
        <link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print'/>
        <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
        <style type="text/css">

            /*Placeholder Color */
            input {
                border: 1px solid #bdbdbd !important;

                color: #486066 !important;
            }

            select {
                border: 1px solid #bdbdbd !important;

                color: #486066 !important;
            }

            input:focus {
                background: transparent !Important;
            }

            select:focus {
                background: transparent !Important;
            }

            .wizard .content {
                /*overflow-y: hidden !important;*/
            }

            .wizard .content label {

                color: white !important;

            }

            .wizard > .steps .current a {
                background-color: #029898 !Important;
            }

            .wizard > .steps .done a {
                background-color: #828f9380 !Important;
            }

            .wizard > .actions a {
                background-color: #029898 !Important;
            }

            .wizard > .actions .disabled a {
                background-color: #eee !important;
            }

            .btn.btn-simple {
                border-color: #486066 !important;
            }

            .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
                color: white;
            }

            table {
                color: white;
            }

            .multiselect.dropdown-toggle.btn.btn-default {
                display: none !important;
            }

            .navbar.p-l-5.p-r-5 {
                display: none !important;
            }

            input[type="text"] {
                height: 40px !important;
            }

            .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
                background-color: transparent !important;
            }

            .bootstrap-select[disabled] button {
                color: gray !important;
                border: 1px solid gray !important;
            }

            .bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
                color: black;
            }

            .bootstrap-select.form-control:not([class*="col-"]) {
                width: auto !important;
            }
        </style>

		<?php
		$activePage = basename($_SERVER['PHP_SELF']);


		?>
    </head>
    <body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                     alt="Oreo"></div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>


	<?php
	require_once('includes/header.php');
	require_once('includes/sidebarAdminDashboard.php');
	require_once('includes/connection.php');
	?>

    <!-- Main Content -->
    <section class="content page-calendar" style="margin-top: 0px !important;">
        <div class="block-header">
			<?php require_once('includes/adminTopBar.php'); ?>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <div class="card">
                        <!-- <div class="header">
							<h3>Tutor Appointment List</h3>
						</div> -->
						<?php

						$year = (isset($_GET['hourFilter']) ? $_GET['year'] : date('Y'));
						$month = (isset($_GET['hourFilter']) ? $_GET['month'] : date('m'));

						?>

                        <div class="body">
                            <form method="GET">
                                <select id="month" name="month" class="form-control show-tick">
                                    <option <?php if ($month == "00") echo "selected"; ?> value="00">Every Month
                                    </option>
                                    <option <?php if ($month == "01") echo "selected"; ?> value="01">January</option>
                                    <option <?php if ($month == "02") echo "selected"; ?> value="02">February</option>
                                    <option <?php if ($month == "03") echo "selected"; ?> value="03">March</option>
                                    <option <?php if ($month == "04") echo "selected"; ?> value="04">April</option>
                                    <option <?php if ($month == "05") echo "selected"; ?> value="05">May</option>
                                    <option <?php if ($month == "06") echo "selected"; ?> value="06">June</option>
                                    <option <?php if ($month == "07") echo "selected"; ?> value="07">July</option>
                                    <option <?php if ($month == "08") echo "selected"; ?> value="08">August</option>
                                    <option <?php if ($month == "09") echo "selected"; ?> value="09">September</option>
                                    <option <?php if ($month == "10") echo "selected"; ?> value="10">October</option>
                                    <option <?php if ($month == "11") echo "selected"; ?> value="11">November</option>
                                    <option <?php if ($month == "12") echo "selected"; ?> value="12">December</option>
                                </select>
                                <select id="year" name="year" class="form-control show-tick">
                                    <option <?php if ($year == "2018") echo "selected"; ?> value="2018">2018</option>
                                    <option <?php if ($year == "2019") echo "selected"; ?> value="2019">2019</option>
                                    <option <?php if ($year == "2020") echo "selected"; ?> value="2020">2020</option>
                                    <option <?php if ($year == "2021") echo "selected"; ?> value="2021">2021</option>
                                    <option <?php if ($year == "2022") echo "selected"; ?> value="2022">2022</option>
                                </select>
                                <input type="submit"
                                       class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block"
                                       id="search-button" name="hourFilter" value="Zoeken"
                                       style="width: auto !important;">
                            </form>
                            <div class="table-responsive">
                                <table class="table table-bordered table-striped table-hover dataTable dt-responsive"
                                       style="font-size: 13px; color: #486066" id="teachers_approve" cellspacing="0"
                                       width="100%">
                                    <thead>
                                    <tr class="text-center">
                                        <th class="color">Naam</th>
                                        <th class="color">Docenttype</th>
                                        <th class="color">Uren</th>
                                        <th class="color">Kosten bijles</th>
                                        <th class="color">Kosten reizen</th>
                                        <th class="color">Kosten totaal</th>
                                        <th></th>

                                    </tr>
                                    </thead>

                                    <tbody>
									<?php
									$params = [
										'year' => $year
									];
									if ($month != "00") {
										$params['month'] = $month;
									}
									$calendar = new API\Endpoint\Calendar($db);
									$result = $calendar->ReportTutor($params);
									$data = $result->getResult();

									foreach ($data as $item) {
										echo "
                                        <tr>
											<td><a href='/app/admin_edit_tutor_profile.php?tid={$item['teacher']}'>{$item['forename']} {$item['surname']}</a></td>
                                            <td class='color'>{$item['level']}</td>
                                            <td class='color'>{$item['duration']}</td>
                                            <td class='color'>€ {$item['amount']}</td>
                                            <td class='color'>€ {$item['travel_cost']}</td>
                                            <td class='color'>€ {$item['sum']}</td>
                                            <td>
                                                <a href='teacher_appointment_list.php?tid={$item['teacher']}&month=$month&year=$year'>
                                                    Bekijk afspraken
                                                </a>
                                            </td>
                                        </tr>
                                        ";
									}
									?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
	<?php
	require_once('includes/footerScripts.php');
	?>
    <script type="text/javascript">
		$(document).ready(function () {
			$('#teachers_approve').DataTable({
				"language": {
					"aria": {
						"sortAscending": ": Oplopend sorteren",
						"sortDescending": ": Aflopend sorteren"
					},
					"emptyTable": "Geen gegevens beschikbaar in de tabel",
					"info": "Tonen van _START_ tot _END_ van _TOTAL_ inzendingen",
					"infoEmpty": "Tonen van 0 tot 0 van 0 inzendingen",
					"infoFiltered": "(Gefilterd van in totaal _MAX_ inzendingen)",
					"lengthMenu": "Toon _MENU_ inzendingen",
					"loadingRecords": "Laden...",
					"processing": "Verwerken...",
					"paginate": {
						"first": "Eerste",
						"last": "Laatste",
						"previous": "Vorige",
						"next": "Volgende"
					},
					"search": "Zoeken:",
					"zeroRecords": "Geen overeenkomende gegevens gevonden",
				}
			});

		});
    </script>

    </body>
    </html>
	<?php
}
?>