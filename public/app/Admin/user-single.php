<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>User Single</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
	<style type="text/css">
		.form-control.form-control-sm:placeholder
		{
			color: white !important;
		}
	</style>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
	require_once('includes/connection.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>User Single
              
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">User Single</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
					
					  <div class="body">
                        <div class="table-responsive">
							<?php
					
	if(isset($_POST['user-single-submit']))
	{
		$userID = test_input($_POST['user-id']);
										$stmt = $con->prepare("select userID, branchID, username, loginattempts, created_at, updated_by, updated_at from user where userID = ?");
		$stmt->bind_param("i",$userID);
										if($stmt->execute())
										{
											
											 $stmt->bind_result($uID, $bID,$username, $loginattempts, $cAt, $upBy, $upAt);
											$stmt->store_result();
											$stmt->fetch();
										}
	
	?>
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" id="user-single" cellspacing="0" width="100%">
                              <thead>
            <tr>
				<th>User ID</th>
				<th>Branch ID</th>
                <th>User Name</th>
                <th>Login Attempts</th>
                <th>Created At</th>
                <th>Last Updated At</th>
				<th>Last Updated By</th>
				
				
            </tr>
        </thead>
        <tbody>
			<tr>
			<td><?php echo $uID; ?></td>
			<td><?php echo $bID; ?></td>
			<td><?php echo $username; ?></td>
			<td><?php echo $loginattempts; ?></td>
			<td><?php echo $cAt; ?></td>
			<td><?php echo $upBy; ?></td>
			<td><?php echo $upAt; ?></td>
			</tr>
        </tbody>
   
    
                            </table>
							<?php
	}
	?>
                        </div>
                    </div>
					
					
					
                   
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
	<script type="text/javascript">
    $(document).ready(function() {
    $('#user-single').DataTable();
} );
</script>
</body>
</html>
<?php
}
?>