$(function () {
    let pages = [];
    let isLayout = false;
    let isEditPage = false;

    let selectHTML = `<div class="select-wrapper"><label for="template-name" style="display:block;">Template engine layout</label><select id="template-name" class="custom-select custom-select-lg">
                    <option value="default" selected="">default</option>
                </select></div>`;
    $('#page-options').prepend(selectHTML);

    $.ajax({
        url: '/api/Template/GetPageList',
        type: 'GET',
        success: function (result) {
            console.log("result", result);
            const layouts = result.filter(p => p.url.startsWith('layout/'));
            let selectHTML = `<div class="select-wrapper"><label for="layout-set" style="display:block;">Layout set</label><select id="layout-set" class="custom-select custom-select-lg">`;
            for (var i = 0; i < layouts.length; i++) {
                selectHTML += `<option value="${layouts[i].id}">${layouts[i].url.replace('layout/', '')}</option>`;
            }
            selectHTML += `</select></div>`;
            $('#layout-options').prepend(selectHTML);

            $('#layout-set').on('change', function () {
                isLayout = false;
                isEditPage = false;
                initFormByPage($('#layout-set').val());
                //addSectionOptions();
                designLayoutOptions(this);
            });
            $('#layout-set').val(layouts[0].id);
            initFormByPage($('#layout-set').val());
            designLayoutOptions($('#layout-set'));


            pages = result;
            selectHTML = `<div class="select-wrapper"><label for="page-set" style="display:block;">Page set</label><select id="page-set" class="custom-select custom-select-lg">`;
            for (var i = 0; i < pages.length; i++) {
                if (!pages[i].url.startsWith('layout/')) {
                    selectHTML += `<option value="${pages[i].id}">${pages[i].url}</option>`;
                }
            }
            selectHTML += `</select></div>`;
            $('#create-layout').before(selectHTML);

            $('#page-set').on('change', function () {
                isLayout = false;
                isEditPage = true;
                initFormByPage();
                //addSectionOptions();
                designLayoutOptions(this);
            });
            $('#page-set').val(null);
        },
        error: function (result) {
            console.log("error", result);
        }
    });

    $('#create-layout').on('click', function () {
        isLayout = true;
        isEditPage = false;
        $('#createTemplateParameters').empty();
        $('#layout-set').val(null);
        $('#page-set').val(null);
        initHeader();
        //addSectionOptions();
        designLayoutOptions(this);
    });

    function designLayoutOptions(element) {
        $('#create-layout, #layout-set, #page-set').removeClass('selected-option');
        $(element).addClass('selected-option');
    }

    function setPositions() {
        const templateSections = $('.template-section input[name*="-position"],.template-section input[name*="[position]"]');
        for (let i = 0; i < templateSections.length; i++) {
            $(templateSections[i]).val(i + 1);
        }
    }

    function initFormByPage(layoutId) {
        let selectVal = $('#page-set').val();
        if (layoutId) {
            $('#page-set').val(null);
            selectVal = $('#layout-set').val();
        } else {
            $('#layout-set').val(null);
            selectVal = $('#page-set').val();
        }
        $('#createTemplateParameters').empty();
        $.ajax({
            url: '/api/Template/GetSections',
            type: "POST",
            dataType: "json",
            data: { "page": "" + selectVal },
            success: function (result) {
                console.log("result", result);
                const sections = result;
                initHeader($('#page-set').val());
                addSections(sections);
            },
            error: function (result) {
                console.log("error", result);
            }
        });
    }

    function initHeader(pageId) {
        if (isLayout) {
            $('#createTemplateParameters').append(getTemplateHeaderHTML("", "", "layout"));
        } else if (pageId) {
            console.log(pageId);
            console.log(pages);
            const page = pages.find(p => p.id == pageId);
            const pageUrl = page.url.split('/');
            const fileName = pageUrl[pageUrl.length - 1];
            const directory = pageUrl.length > 1 ? pageUrl[0] : "";
            $('#createTemplateParameters').append(getTemplateHeaderHTML(page.id, fileName, directory, page.title, page.socialMediaTitle, page.description, page.socialMediaDescription));
        } else {
            $('#createTemplateParameters').append(getTemplateHeaderHTML());
        }
        addSectionOptions();
    }

    function addSections(sections, beforeElement) {
        for (var i = 0; sections && i < sections.length; i++) {
            var section = sections[i];
            var HTML = "";
            console.log("section", section);
            if (!section.name) {
                section.name = section.component;
                section.content = JSON.parse(section.data);
            }
            console.log("section", section);
            var videoSrcSet = null;
            var imageSrc = null;
            switch (section.name) {
                case "card":
                    HTML = getCardHTML((section.id && isEditPage) ? section.id : "", section.position);
                    break;

                case "card-numbers":
                    HTML = getCardNumbersHTML((section.id && isEditPage) ? section.id : "", section.position);
                    break;

                case "card-price":
                    var contents = (section.content && section.content.contents) ? section.content.contents : null;
                    //console.log("switch: contents",contents);
                    //console.log("switch: section",section);
                    HTML = getCardPriceHTML((section.id && isEditPage) ? section.id : "", contents, section.position);
                    break;

                case "card-topic":
                    var title = (section.content && section.content.title) ? section.content.title : '';
                    var contents = (section.content && section.content.contents) ? section.content.contents : null;
                    //console.log("switch: contents",contents);
                    //console.log("switch: section",section);
                    HTML = getCardTopicHTML((section.id && isEditPage) ? section.id : "", title, contents, section.position);
                    break;

                case "card-responsive-slider":
                    var title = (section.content && section.content.title) ? section.content.title : '';
                    var contents = (section.content && section.content.contents) ? section.content.contents : null;
                    //console.log("switch: contents",contents);
                    //console.log("switch: section",section);
                    HTML = getCardResponsiveSliderHTML((section.id && isEditPage) ? section.id : "", title, contents, section.position);
                    break;

                case "card-slider":
                    var contents = (section.content && section.content.contents) ? section.content.contents : null;
                    //console.log("switch: contents",contents);
                    //console.log("switch: section",section);
                    HTML = getCardSliderHTML((section.id && isEditPage) ? section.id : "", contents, section.position);
                    break;

                case "card-wide":
                    var title = (section.content && section.content.title) ? section.content.title : '';
                    var contents = (section.content && section.content.contents) ? section.content.contents : null;
                    //console.log("switch: contents",contents);
                    //console.log("switch: section",section);
                    HTML = getCardWideHTML((section.id && isEditPage) ? section.id : "", title, contents, section.position);
                    break;

                case "contact-form":
                    var contents = (section.content && section.content.contents) ? section.content.contents : null;
                    //console.log("switch: contents",contents);
                    //console.log("switch: section",section);
                    HTML = getContactFormHTML((section.id && isEditPage) ? section.id : "", contents, section.position);
                    break;

                case "contact-form-small":
                    var contents = (section.content && section.content.contents) ? section.content.contents : null;
                    //console.log("switch: contents",contents);
                    //console.log("switch: section",section);
                    HTML = getContactFormSmallHTML((section.id && isEditPage) ? section.id : "", contents, section.position);
                    break;

                case "contact-header":
                    var contents = (section.content && section.content.contents) ? section.content.contents : null;
                    //console.log("switch: contents",contents);
                    //console.log("switch: section",section);
                    HTML = getContactHeaderHTML((section.id && isEditPage) ? section.id : "", contents, section.position);
                    break;

                case "counting-number":
                    var title = (section.content && section.content.title) ? section.content.title : '';
                    var unit = (section.content && section.content.unit) ? section.content.unit : '';
                    var subtitle = (section.content && section.content.subtitle) ? section.content.subtitle : '';
                    HTML = getCountingNumberHTML((section.id && isEditPage) ? section.id : "", title, unit, subtitle, section.position);
                    break;

                case "followup-button":
                    var title = (section.content && section.content.title) ? section.content.title : '';
                    var description = (section.content && section.content.description) ? section.content.description : '';
                    var buttonText = (section.content && section.content.buttonText) ? section.content.buttonText : '';
                    var buttonLink = (section.content && section.content.buttonLink) ? section.content.buttonLink : '';
                    HTML = getFollowUpButtonHTML((section.id && isEditPage) ? section.id : "", title, description, buttonText, buttonLink, section.position);
                    break;

                case "question-collection":
                    var seperatorTitle = (section.content && section.content.seperatorTitle) ? section.content.seperatorTitle : '';
                    var title = (section.content && section.content.collectionTitle) ? section.content.collectionTitle : '';
                    var contents = (section.content && section.content.contents) ? section.content.contents : null;
                    console.log("switch: contents", contents);
                    //console.log("switch: section",section);
                    HTML = getQuestionCollectionHTML((section.id && isEditPage) ? section.id : "", seperatorTitle, title, contents, section.position);
                    break;

                case "searchbar-header":
                    var title = (section.content && section.content.title) ? section.content.title : "";
                    HTML = getSearchbarHeaderHTML((section.id && isEditPage) ? section.id : "", title, section.position);
                    break;

                case "text-info-section":
                    var title = (section.content && section.content.title) ? section.content.title : "";
                    var subtitle = (section.content && section.content.subtitle) ? section.content.subtitle : "";
                    var description = (section.content && section.content.description) ? section.content.description : "";
                    HTML = getTextInfoHTML((section.id && isEditPage) ? section.id : "", title, subtitle, description, section.position);
                    break;

                case "tutoring-info":
                    var title = (section.content && section.content.title) ? section.content.title : "";
                    var subtitle = (section.content && section.content.subtitle) ? section.content.subtitle : "";
                    var description = (section.content && section.content.description) ? section.content.description : "";
                    var hideButton = (section.content && section.content.hideButton) ? section.content.hideButton : false;
                    var mediaLeft = (section.content && section.content.mediaLeft) ? section.content.mediaLeft : false;
                    var showVideo = (section.content && section.content.showVideo) ? section.content.showVideo : false;
                    var showTutors = (section.content && section.content.showTutors) ? section.content.showTutors : false;
                    var showImage = (section.content && section.content.showImage) ? section.content.showImage : false;
                    imageSrc = (section.content && section.content.imageSrc) ? section.content.imageSrc : "";
                    var imageAltText = (section.content && section.content.imageAltText) ? section.content.imageAltText : "";
                    videoSrcSet = (section.content && section.content.videoSrcSet && section.content.videoSrcSet[0] && section.content.videoSrcSet[0].src) ? section.content.videoSrcSet[0].src : "";
                    var buttonText = (section.content && section.content.buttonText) ? section.content.buttonText : '';
                    var buttonLink = (section.content && section.content.buttonLink) ? section.content.buttonLink : '';
                    HTML = getTutoringInfoHTML((section.id && isEditPage) ? section.id : "", title, subtitle, description, hideButton, mediaLeft, showVideo, showTutors, showImage, imageSrc, imageAltText, videoSrcSet, buttonText, buttonLink, section.position);
                    break;
            }

            if (beforeElement) {
                $(beforeElement).after(HTML);
            } else {
                $('#createTemplateParameters').append(HTML);
            }
            if (videoSrcSet) {
                $('.last-added select[name*="tutoring-info-video-src-set"]').val(videoSrcSet);
            }
            if (imageSrc) {
                $('.last-added select[name*="tutoring-info-image-src"]').val(imageSrc);
            }
            addSectionOptions($('.last-added').parent());
            $('.last-added').removeClass('last-added');
            //$('#template-name').val($('#layout-set').val());
        }
    }

    function addSectionOptions(elem) {
        if (!elem || !$('.add-section').last().is($(elem))) {
            $('#createTemplateParameters').append(getSectionOptions());

            $('.add-section:last-child .add-section-option').on('click', function () {
                console.log("$(this).attr('id')", $(this).attr('id'));
                let id = $(this).attr('id');
                id = id.replace('add-', '');
                const sections = [{ name: id }];
                addSections(sections, $(this).parent());
                //addSectionOptions($(this).parent());
                deleteDoubleAddSections();
                setPositions();
            });
        }
    }

    function getSectionOptions() {
        return `<div class="add-section">
        <div class="btn add-section-option" id="add-card">Card</div>
        <div class="btn add-section-option" id="add-card-numbers">Card Numbers</div>
        <div class="btn add-section-option" id="add-card-price">Card Price</div>
        <div class="btn add-section-option" id="add-card-topic">Card Topic</div>
        <div class="btn add-section-option" id="add-card-responsive-slider">Card Responsive Slider</div>
        <div class="btn add-section-option" id="add-card-slider">Card Slider</div>
        <div class="btn add-section-option" id="add-card-wide">Card Wide</div>
        <div class="btn add-section-option" id="add-contact-form">Contact Form Large</div>
        <div class="btn add-section-option" id="add-contact-form-small">Contact Form Small</div>
        <div class="btn add-section-option" id="add-contact-header">Contact Header</div>
        <div class="btn add-section-option" id="add-counting-number">Counting Number</div>
        <div class="btn add-section-option" id="add-followup-button">Followup Button</div>
        <div class="btn add-section-option" id="add-question-collection">Question Collection</div>
        <div class="btn add-section-option" id="add-searchbar-header">Searchbar Header</div>
        <div class="btn add-section-option" id="add-text-info-section">Text Info</div>
        <div class="btn add-section-option" id="add-tutoring-info">Tutoring Info</div>
    </div>`;
    }

    function getTemplateHeaderHTML(pageId = "", fileName = "", fileDirectory = "", pageTitle = "", pageSocialMediaTitle = "", pageDescription = "", pageSocialMediaDescription = "") {
        let hideOnLayoutClass = isLayout ? 'd-none' : '';
        return `
    <div class="template-section last-added">
        <input type="hidden" name="page-id" value="${pageId}" />
        <h3 class="template-section__title">Name and directory</h3>
        <div class="template-section__parameters">
            <div class="template-section__parameters-wrapper">
                <label>${isLayout ? 'Layout Name' : 'Filename /filename'}</label>
                <input type="text" class="template-section__parameters-input" name="template-file-name" required value="${fileName}" />
            </div>
            <div class="template-section__parameters-wrapper ${hideOnLayoutClass}">
                <label>Sub-directory /sub-directory/filename</label>
                <input type="text" class="template-section__parameters-input" name="template-file-directory" value="${fileDirectory}" />
            </div>
            <div class="template-section__parameters-wrapper ${hideOnLayoutClass}">
                <label>Page title</label>
                <input type="text" class="template-section__parameters-input" name="header-page-title" ${isLayout ? '' : 'required'} value="${pageTitle}" />
            </div>
            <div class="template-section__parameters-wrapper ${hideOnLayoutClass}">
                <label>Social media title</label>
                <input type="text" class="template-section__parameters-input" name="header-social-media-title" value="${pageSocialMediaTitle}" />
            </div>
            <div class="template-section__parameters-wrapper ${hideOnLayoutClass}">
                <label>Page description</label>
                <input type="text" class="template-section__parameters-input" name="header-page-description" ${isLayout ? '' : 'required'} value="${pageDescription}" />
            </div>
            <div class="template-section__parameters-wrapper ${hideOnLayoutClass}">
                <label>Social media description</label>
                <input type="text" class="template-section__parameters-input" name="header-social-media-description" value="${pageSocialMediaDescription}" />
            </div>
        </div>
    </div>`;
    };

    function getCardHTML(sectionId, position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Cards (without numbers) Section</h3>
        <input type="hidden" name="card-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="card-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
    </div>`;
    };

    function getCardNumbersHTML(sectionId, position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Cards (with numbers) Section</h3>
        <input type="hidden" name="cards-numbers-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="cards-numbers-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
    </div>`;
    };

    function getCardPriceHTML(sectionId, contents, position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">3 Price Cards</h3>
        <input type="hidden" name="card-price-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="card-price-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
    </div>`;
    };

    function getCardTopicHTML(sectionId, title = "", contents, position) {
        sectionCount = $('.card-topic').length;
        let content = `
        <div class="template-section last-added">
            <h3 class="template-section__title">Card Topic Section</h3>
            <input type="hidden" class="card-topic" name="cardTopic[${sectionCount}][id]" value="${sectionId}" />
            <div class="template-section__parameters-wrapper">
                <label>Position</label>
                <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="cardTopic[${sectionCount}][position]" value="${position}" />
            </div>
            <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
            <div class="template-section__parameters">
                <div class="template-section__parameters-wrapper">
                    <label>Title</label>
                    <input type="text" class="template-section__parameters-input" name="cardTopic[${sectionCount}][title]" value="${title}" />
                </div>`;
        if (!contents) {
            content += `
                <hr/>
                <div class="template-section__parameters-wrapper">
                    <label>Title</label>
                    <input type="text" class="template-section__parameters-input cardTopic-${sectionCount}-title" name="cardTopic[${sectionCount}][contents][0][title]" value="" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Amazon S3 Image Link</label>
                    <input type="text" class="template-section__parameters-input" name="cardTopic[${sectionCount}][contents][0][imgSrc]" value=""/>
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Page Link</label>
                    <input type="text" class="template-section__parameters-input" name="cardTopic[${sectionCount}][contents][0][link]" value=""/>
                </div>`;
        } else {
            for (var k = 0; contents && k < contents.length; k++) {
                content += `
                <hr/>
                <div class="template-section__parameters-wrapper">
                    <label>Title</label>
                    <input type="text" class="template-section__parameters-input cardTopic-${sectionCount}-title" name="cardTopic[${sectionCount}][contents][${k}][title]" value="${contents[k].title}" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Amazon S3 Image Link</label>
                    <input type="text" class="template-section__parameters-input" name="cardTopic[${sectionCount}][contents][${k}][imgSrc]" value="${contents[k].imgSrc}"/>
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Page Link</label>
                    <input type="text" class="template-section__parameters-input" name="cardTopic[${sectionCount}][contents][${k}][link]" value="${contents[k].link}"/>
                </div>`;
            }
        }
        content += `</div>
            <div class="add-content-option">
                <div class="btn btn-success text-white" onclick="addContentHTML('cardTopic',${sectionCount},null,true,false,false,true,true);">&nbsp;+ Add&nbsp;</div>
            </div>
        </div>`;
        return content;
    };

    function getCardResponsiveSliderHTML(sectionId, title = "", contents, position) {
        sectionCount = $('.card-responsive-slider').length;
        let content = `
        <div class="template-section last-added">
            <h3 class="template-section__title">4 Cards. Card Slider on Mobile</h3>
            <input type="hidden" class="card-responsive-slider" name="cardResponsiveSlider[${sectionCount}][id]" value="${sectionId}" />
            <div class="template-section__parameters-wrapper">
                <label>Position</label>
                <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="cardResponsiveSlider[${sectionCount}][position]" value="${position}" />
            </div>
            <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
            <h5>4 Cards recommened</h5>
            <div class="template-section__parameters">
                <div class="template-section__parameters-wrapper">
                    <label>Title</label>
                    <input type="text" class="template-section__parameters-input" name="cardResponsiveSlider[${sectionCount}][title]" value="${title}" />
                </div>`;
        if (!contents) {
            content += `
                <hr/>
                <div class="template-section__parameters-wrapper">
                    <label>Title</label>
                    <input type="text" class="template-section__parameters-input cardResponsiveSlider-${sectionCount}-title" name="cardResponsiveSlider[${sectionCount}][contents][0][title]" value="" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Amazon S3 Image Link</label>
                    <input type="text" class="template-section__parameters-input" name="cardResponsiveSlider[${sectionCount}][contents][0][imgSrc]" value=""/>
                </div>`;
        } else {
            for (var k = 0; contents && k < contents.length; k++) {
                content += `
                <hr/>
                <div class="template-section__parameters-wrapper">
                    <label>Title</label>
                    <input type="text" class="template-section__parameters-input cardResponsiveSlider-${sectionCount}-title" name="cardResponsiveSlider[${sectionCount}][contents][${k}][title]" value="${contents[k].title}" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Amazon S3 Image Link</label>
                    <input type="text" class="template-section__parameters-input" name="cardResponsiveSlider[${sectionCount}][contents][${k}][imgSrc]" value="${contents[k].imgSrc}"/>
                </div>`;
            }
        }
        content += `</div>
            <div class="add-content-option">
                <div class="btn btn-success text-white" onclick="addContentHTML('cardResponsiveSlider',${sectionCount},null,true,false,false,true,false);">&nbsp;+ Add&nbsp;</div>
            </div>
        </div>`;
        return content;
    };
    function getCardSliderHTML(sectionId, contents, position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Slider Cards Section</h3>
        <input type="hidden" name="card-slider-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="card-slider-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
        <div class="template-section__parameters">
            <h5>Please insert a minimum of 4 cards</h5>
            <div class="template-section__parameters-wrapper">
                <label>Author</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-title[]" value="${getContent(contents, 0).title}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Course</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-subtitle[]" value="${getContent(contents, 0).subtitle}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Review</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-description[]" value="${getContent(contents, 0).description}"/>
            </div>
            <hr />
            <div class="template-section__parameters-wrapper">
                <label>Author</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-title[]" value="${getContent(contents, 1).title}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Course</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-subtitle[]" value="${getContent(contents, 1).subtitle}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Review</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-description[]" value="${getContent(contents, 1).description}"/>
            </div>
            <hr />
            <div class="template-section__parameters-wrapper">
                <label>Author</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-title[]" value="${getContent(contents, 2).title}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Course</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-subtitle[]" value="${getContent(contents, 2).subtitle}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Review</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-description[]" value="${getContent(contents, 2).description}"/>
            </div>
            <hr />
            <div class="template-section__parameters-wrapper">
                <label>Author</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-title[]" value="${getContent(contents, 3).title}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Course</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-subtitle[]" value="${getContent(contents, 3).subtitle}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Review</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-description[]" value="${getContent(contents, 3).description}"/>
            </div>
            <hr />
            <div class="template-section__parameters-wrapper">
                <label>Author</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-title[]" value="${getContent(contents, 4).title}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Course</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-subtitle[]" value="${getContent(contents, 4).subtitle}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Review</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-description[]" value="${getContent(contents, 4).description}"/>
            </div>
            <hr />
            <div class="template-section__parameters-wrapper">
                <label>Author</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-title[]" value="${getContent(contents, 5).title}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Course</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-subtitle[]" value="${getContent(contents, 5).subtitle}"/>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Review</label>
                <input type="text" class="template-section__parameters-input" name="card-slider-description[]" value="${getContent(contents, 5).description}"/>
            </div>
        </div>
    </div>`;
    };

    function getCardWideHTML(sectionId, title = '', contents, position) {
        sectionCount = $('.card-wide').length;
        let content = `
        <div class="template-section last-added">
            <h3 class="template-section__title">3 Wide Cards Section</h3>
            <input type="hidden" class="card-wide" name="cardWide[${sectionCount}][id]" value="${sectionId}" />
            <div class="template-section__parameters-wrapper">
                <label>Position</label>
                <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="cardWide[${sectionCount}][position]" value="${position}" />
            </div>
            <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
            <div class="template-section__parameters">
                <div class="template-section__parameters-wrapper">
                    <label>Title</label>
                    <input type="text" class="template-section__parameters-input" name="cardWide[${sectionCount}][title]" value="${title}" />
                </div>`;
        if (!contents) {
            content += `
                <hr/>
                <div class="template-section__parameters-wrapper">
                    <label>Title</label>
                    <input type="text" class="template-section__parameters-input cardWide-${sectionCount}-title" name="cardWide[${sectionCount}][contents][0][title]" value="" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Subtitle</label>
                    <input type="text" class="template-section__parameters-input cardWide-${sectionCount}-subtitle" name="cardWide[${sectionCount}][contents][0][subtitle]" value="" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Description</label>
                    <input type="text" class="template-section__parameters-input cardWide-${sectionCount}-description" name="cardWide[${sectionCount}][contents][0][description]" value="" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Amazon S3 Image Link</label>
                    <input type="text" class="template-section__parameters-input" name="cardWide[${sectionCount}][contents][0][imgSrc]" value=""/>
                </div>`;
        } else {
            for (var k = 0; contents && k < contents.length; k++) {
                content += `
                <hr/>
                <div class="template-section__parameters-wrapper">
                    <label>Title</label>
                    <input type="text" class="template-section__parameters-input cardWide-${sectionCount}-title" name="cardWide[${sectionCount}][contents][${k}][title]" value="${contents[k].title}" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Subtitle</label>
                    <input type="text" class="template-section__parameters-input cardWide-${sectionCount}-subtitle" name="cardWide[${sectionCount}][contents][${k}][subtitle]" value="${contents[k].subtitle}" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Description</label>
                    <input type="text" class="template-section__parameters-input cardWide-${sectionCount}-description" name="cardWide[${sectionCount}][contents][${k}][description]" value="${contents[k].description}" />
                </div>
                <div class="template-section__parameters-wrapper">
                    <label>Amazon S3 Image Link</label>
                    <input type="text" class="template-section__parameters-input" name="cardWide[${sectionCount}][contents][${k}][imgSrc]" value="${contents[k].imgSrc}"/>
                </div>`;
            }
        }
        content += `</div>
            <div class="add-content-option">
                <div class="btn btn-success text-white" onclick="addContentHTML('cardWide',${sectionCount},null,true,true,true,true,false);">&nbsp;+ Add&nbsp;</div>
            </div>
        </div>`;
        return content;
    };

    function getContactFormHTML(sectionId, contents, position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Large Contact Form Section</h3>
        <input type="hidden" name="contact-form-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="contact-form-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
    </div>`;
    };

    function getContactFormSmallHTML(sectionId, contents, position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Small Contact Form Section with LinkedIn Profile</h3>
        <input type="hidden" name="contact-form-small-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="contact-form-small-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
    </div>`;
    };

    function getContactHeaderHTML(sectionId, contents, position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Contact Header Section</h3>
        <input type="hidden" name="contact-header-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="contact-header-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
    </div>`;
    };

    function getCountingNumberHTML(sectionId, title = "", unit = "", subtitle = "", position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Counting Number Section</h3>
        <input type="hidden" name="counting-number-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="counting-number-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
        <div class="template-section__parameters">
            <div class="template-section__parameters-wrapper">
                <label>Title</label>
                <input type="text" class="template-section__parameters-input" name="counting-number-title[]" value="${title}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Unit</label>
                <input type="text" class="template-section__parameters-input" name="counting-number-unit[]" value="${unit}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Subtitle</label>
                <input type="text" class="template-section__parameters-input" name="counting-number-subtitle[]" value="${subtitle}" />
            </div>
        </div>
    </div>`;
    };
    function getFollowUpButtonHTML(sectionId, title = "", description = "", buttonText = "", buttonLink = "", position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Followup Button Section</h3>
        <input type="hidden" name="followup-button-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="followup-button-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
        <div class="template-section__parameters">
            <div class="template-section__parameters-wrapper">
                <label>Title</label>
                <input type="text" class="template-section__parameters-input" name="followup-button-title[]" value="${title}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Description</label>
                <input type="text" class="template-section__parameters-input" name="followup-button-description[]" value="${description}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Button Text</label>
                <input type="text" class="template-section__parameters-input" name="followup-button-button-text[]" value="${buttonText}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Button Link</label>
                <input type="text" class="template-section__parameters-input" name="followup-button-button-link[]" value="${buttonLink}" />
            </div>
        </div>
    </div>`;
    };
    function getQuestionCollectionHTML(sectionId, seperatorTitle, title, contents, position) {
        sectionCount = $('.question-collection').length;
        let content = `
    <div class="template-section last-added">
        <h3 class="template-section__title">Question Collection Section</h3>
        <input type="hidden" class="question-collection" name="questionCollection[${sectionCount}][id]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="questionCollection[${sectionCount}][position]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
        <div class="template-section__parameters">
            <div class="template-section__parameters-wrapper">
                <label>Seperator Title</label>
                <input type="text" class="template-section__parameters-input" name="questionCollection[${sectionCount}][seperatorTitle]" value="${seperatorTitle}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Collection Title</label>
                <input type="text" class="template-section__parameters-input" name="questionCollection[${sectionCount}][collectionTitle]" value="${title}" />
            </div>`;
        if (!contents) {
            content += `
            <hr/>
            <div class="template-section__parameters-wrapper">
                <label>Question</label>
                <input type="text" class="template-section__parameters-input questionCollection-${sectionCount}-title" name="questionCollection[${sectionCount}][contents][0][title]" value="" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Answer</label>
                <input type="text" class="template-section__parameters-input" name="questionCollection[${sectionCount}][contents][0][description]" value=""/>
            </div>`;
        } else {
            for (var k = 0; contents && k < contents.length; k++) {
                content += `
            <hr/>
            <div class="template-section__parameters-wrapper">
                <label>Question</label>
                <input type="text" class="template-section__parameters-input questionCollection-${sectionCount}-title" name="questionCollection[${sectionCount}][contents][${k}][title]" value="${contents[k].title}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Answer</label>
                <input type="text" class="template-section__parameters-input" name="questionCollection[${sectionCount}][contents][${k}][description]" value="${contents[k].description}"/>
            </div>`;
            }
        }
        content += `</div>
        <div class="add-content-option">
            <div class="btn btn-success text-white" onclick="addContentHTML('questionCollection',${sectionCount},null,'Question',false,'Answer',false,false);">&nbsp;+ Add&nbsp;</div>
        </div>
    </div>`;
        return content;
    };
    function getSearchbarHeaderHTML(sectionId, title = "", position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Searchbar Header Section</h3>
        <input type="hidden" name="searchbar-header-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="searchbar-header-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
        <div class="template-section__parameters">
            <div class="template-section__parameters-wrapper">
                <label>Title</label>
                <input type="text" class="template-section__parameters-input" name="searchbar-header-title[]" value="${title}" />
            </div>
        </div>
    </div>`;
    };
    function getTextInfoHTML(sectionId, title = "", subtitle = "", description = "", sectionBackgroundColor = "", position) {
        var sectionCount = $('.text-info').length;
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Text Info Section</h3>
        <input type="hidden" class="text-info" name="textInfoSection[${sectionCount}][id]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="textInfoSection[${sectionCount}][position]" value="${position}" />
        </div>
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
        <div class="template-section__parameters">
            <div class="template-section__parameters-wrapper">
                <label>Title</label>
                <input type="text" class="template-section__parameters-input" name="textInfoSection[${sectionCount}][title]" value="${title}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Subtitle</label>
                <input type="text" class="template-section__parameters-input" name="textInfoSection[${sectionCount}][subtitle]" value="${subtitle}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Description</label>
                <input type="text" class="template-section__parameters-input" name="textInfoSection[${sectionCount}][description]" value="${description}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Section Background Color (Insert hex string, e.g. #fafbfc)</label>
                <input type="text" class="template-section__parameters-input" name="textInfoSection[${sectionCount}][sectionBackgroundColor]" value="${sectionBackgroundColor}" placeholder="default:white"/>
            </div>
        </div>
    </div>`;
    };
    function getTutoringInfoHTML(sectionId, title = "", subtitle = "", description = "", hideButton = false, mediaLeft = false, showVideo = false, showTutors = false, showImage = false, imageSrc = "", imageAltText = "", videoSrcSet = "", buttonText = "VIND DOCENT", buttonLink = "/app/teachers-profile2", position) {
        return `
    <div class="template-section last-added">
        <h3 class="template-section__title">Tutoring Info Section</h3>
        <input type="hidden" name="tutoring-info-id[]" value="${sectionId}" />
        <div class="template-section__parameters-wrapper">
            <label>Position</label>
            <input type="number" class="template-section__parameters-input template-section__parameters-input--position" name="tutoring-info-position[]" value="${position}" />
        </div>
        <div class="close close-section" onclick="closeSection(this,${isEditPage})"">x</div>
        <div class="template-section__parameters">
            <div class="template-section__parameters-wrapper">
                <label>Title</label>
                <input type="text" class="template-section__parameters-input" name="tutoring-info-title[]" value="${title}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Subtitle</label>
                <input type="text" class="template-section__parameters-input" name="tutoring-info-subtitle[]" value="${subtitle}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Description</label>
                <input type="text" class="template-section__parameters-input" name="tutoring-info-description[]" value="${description}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Hide button</label>
                <input type="checkbox" class="template-section__parameters-input" name="tutoring-info-hide-button[]" ${hideButton ? "checked='checked'" : ""} value="tutoring-info-hide-button${$("input[name^='tutoring-info-hide-button']").length}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Media on the left side</label>
                <input type="checkbox" class="template-section__parameters-input" name="tutoring-info-media-left[]" ${mediaLeft ? "checked='checked'" : ""} value="tutoring-info-media-left${$("input[name^='tutoring-info-media-left']").length}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Show Video</label>
                <input type="checkbox" class="template-section__parameters-input" name="tutoring-info-show-video[]" ${showVideo ? "checked='checked'" : ""} value="tutoring-info-show-video${$("input[name^='tutoring-info-show-video']").length}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Video Src</label>
                <select class="template-section__parameters-input custom-select custom-select-lg" name="tutoring-info-video-src-set[]" value="${videoSrcSet}">
                    <option value="">None</option>
                    <option value="default-1" data-class="video1">Shorter</option>
                    <option value="default-2" data-class="video2">Longer</option>
                    <option value="default-3" data-class="video3">Edited</option>
                    <option value="default-4" data-class="video3">Video for tutors</option>
                </select>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Show tutor cards</label>
                <input type="checkbox" class="template-section__parameters-input" name="tutoring-info-show-tutors[]" ${showTutors ? "checked='checked'" : ""} value="tutoring-info-show-tutors${$("input[name^='tutoring-info-show-tutors']").length}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Show image</label>
                <input type="checkbox" class="template-section__parameters-input" name="tutoring-info-show-image[]" ${showImage ? "checked='checked'" : ""} value="tutoring-info-show-image${$("input[name^='tutoring-info-show-image']").length}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Image Src</label>
                <select class="template-section__parameters-input custom-select custom-select-lg" name="tutoring-info-image-src[]" value="${imageSrc}">
                    <option value="">None</option>
                    <option value="default-1" data-class="image1">Two girls</option>
                    <option value="default-2" data-class="image2">Boy and girl</option>
                    <option value="default-3" data-class="image3">Living room</option>
                    <option value="default-4" data-class="image4">Dark clothes, plant</option>
                </select>
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Button Text</label>
                <input type="text" class="template-section__parameters-input" name="tutoring-info-button-text[]" value="${buttonText}" />
            </div>
            <div class="template-section__parameters-wrapper">
                <label>Button Link</label>
                <input type="text" class="template-section__parameters-input" name="tutoring-info-button-link[]" value="${buttonLink}" />
            </div>
        </div>
    </div>`;
    };

    function getContent(contents, index) {
        //console.log("contents", contents)
        if (contents && contents[index]) {
            //console.log("contents index", contents[index])
            return contents[index];
        }

        //console.log("empty description", contents[index])
        return { title: '', subtitle: '', description: '' };
    }
});
function closeSection(element, isEditPage = true) {
    if (!isEditPage) {
        deleteSectionWrapper(element)
    } else {
        const sectionId = $(element).parent().find('input[name*="-id"],input[name*="[id]"]').val();
        if (sectionId) {
            console.log("sectionId", sectionId);
            $.ajax({
                url: '/api/Template/DeleteSection',
                data: { "id": sectionId },
                type: "POST",
                success: function (result) {
                    if (result && result.success && result.success == true) {
                        deleteSectionWrapper(element)
                    }
                    else {
                        if (result && result.error) {
                            console.log("Error: ", result);
                        }
                        console.log("#1 An error occured", result);
                    }
                },
                error: function (result) {
                    if (result && result.error) {
                        console.log("Error: ", result);
                    }
                    console.log("#2 An error occured", result);
                }
            });
        } else {
            console.log("message: ", "No Section Id found");
            deleteSectionWrapper(element)
        }
    }
}
function deleteSectionWrapper(element) {
    $(element).parent().remove();
    setPositionsAfterDelete();
}
function setPositionsAfterDelete() {
    const templateSections = $('.template-section input[name*="-position"],.template-section input[name*="[position]"]');
    for (let i = 0; i < templateSections.length; i++) {
        $(templateSections[i]).val(i + 1);
    }
    deleteDoubleAddSections();
}
function deleteDoubleAddSections() {
    const editSections = $('.template-section,.add-section');
    for (let i = 0; i < editSections.length - 1; i++) {
        if ($(editSections[i]).hasClass('add-section') && $(editSections[i]).next().hasClass('add-section')) {
            $(editSections[i]).remove();
        }
    }
}
function addContentHTML(sectionName, sectionCount, contents, title, subtitle, description, imgSrc, link) {
    console.log("sectionName", sectionName);
    console.log("sectionCount", sectionCount);
    console.log("contents", contents);
    const index = $(`.${sectionName}-${sectionCount}-title`).length;
    console.log("$(`.${sectionName}-${sectionCount}-title`)", $(`.${sectionName}-${sectionCount}-title`));
    console.log("index", index);
    let content = `
    <hr/><div class="delete-wrapper">`;
    if (title) {
        content += `<div class="template-section__parameters-wrapper">
            <label>${title === true ? 'Title' : title}</label>
            <input type="text" class="template-section__parameters-input ${sectionName}-${sectionCount}-title" name="${sectionName}[${sectionCount}][contents][${index}][title]" value="${contents ? getContent(contents, index).title : ''}" />
        </div>`;
    }
    if (subtitle) {
        content += `<div class="template-section__parameters-wrapper">
            <label>${subtitle === true ? 'Subtitle' : subtitle}</label>
            <input type="text" class="template-section__parameters-input" name="${sectionName}[${sectionCount}][contents][${index}][subtitle]" value="${contents ? getContent(contents, index).subtitle : ''}"/>
        </div>`;
    }
    if (description) {
        content += `<div class="template-section__parameters-wrapper">
            <label>${description === true ? 'Description' : description}</label>
            <input type="text" class="template-section__parameters-input" name="${sectionName}[${sectionCount}][contents][${index}][description]" value="${contents ? getContent(contents, index).description : ''}"/>
        </div>`;
    }
    if (imgSrc) {
        content += `<div class="template-section__parameters-wrapper">
            <label>${imgSrc === true ? 'Amazon S3 Image Link' : imgSrc}</label>
            <input type="text" class="template-section__parameters-input" name="${sectionName}[${sectionCount}][contents][${index}][imgSrc]" value="${contents ? getContent(contents, index).imgSrc : ''}"/>
        </div>`;
    }
    if (link) {
        content += `<div class="template-section__parameters-wrapper">
            <label>${link === true ? 'Page Link' : link}</label>
            <input type="text" class="template-section__parameters-input" name="${sectionName}[${sectionCount}][contents][${index}][link]" value="${contents ? getContent(contents, index).link : ''}"/>
        </div>`;
    }
    content += `<div class="close close-section" onclick="closeSection(this,false)" "="">x</div></div>`;
    console.log('result content', $(`.${sectionName}-${sectionCount}-title`));
    var parent = $(`.${sectionName}-${sectionCount}-title`).last().parent().parent();
    if ($(parent).hasClass('delete-wrapper')) {
        parent = $(parent).parent();
    }
    $(parent).append(content);
}
function getContent(contents, index) {
    //console.log("contents", contents)
    if (contents && contents[index]) {
        //console.log("contents index", contents[index])
        return contents[index];
    }
    //console.log("empty description", contents[index])
    return { title: '', subtitle: '', description: '' };
}