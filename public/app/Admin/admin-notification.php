<?php
$thisPage = "Notification";
session_start();

if (!isset($_SESSION['AdminUser'])) {
	header('Location: index.php');

} else {

	?>

    <!doctype html>
    <html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>Admin Slot Creation</title>
		<?php
		require_once('includes/connection.php');
		require_once('includes/mainCSSFiles.php');
		?>
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
              rel="stylesheet"/>
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">

        <!-- New  Style -->

        <!-- New  Style -->
        <style type="text/css">
            .inline-elem-button {
                margin-top: 20px;
            }

            .form-control, .btn-round.btn-simple {
                color: #ece6e6;
            }

            .input-group-addon {
                color: #ece6e6;
            }

            .text {
                color: black;
            }

            .margin-top {
                margin-top: 10px;
            }

            .checkbox {
                float: left;
                margin-right: 20px;
            }

            .margintop {
                margin-top: 20px;
                color: #5cc75f;
            }

            /*Placeholder Color */
            .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
                color: #486066;
            }
        </style>
		<?php
		$activePage = basename($_SERVER['PHP_SELF']);
		?>
    </head>
    <body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                     alt="Oreo"></div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>


	<?php
	/*require_once('includes/header.php');*/
	require_once('includes/sidebarAdminDashboard.php');

	if(isset($_POST['saveRespondTime']) && !empty($_POST['saveRespondTime'])){

		$respondTime = $con->escape_string($_POST['respondTime']);
		var_dump($respondTime);

		$query = "UPDATE variables SET value={$respondTime} WHERE name='respondTime'";
		$result = mysqli_query($con, $query);

		if($result->num_rows == NULL){
			$query  = "INSERT INTO variables (name, value) VALUES ('respondTime', {$respondTime})";
			$result = mysqli_query($con, $query);
		}

	}

	if(isset($_POST['saveRespondTimeChat']) && !empty($_POST['saveRespondTimeChat'])){

		$respondTime = $con->escape_string($_POST['respondTimeChat']);
		var_dump($respondTime);

		$query = "UPDATE variables SET value={$respondTime} WHERE name='respondTimeChat'";
		$result = mysqli_query($con, $query);

		if($result->num_rows == NULL){
			$query  = "INSERT INTO variables (name, value) VALUES ('respondTimeChat', {$respondTime})";
			$result = mysqli_query($con, $query);
		}

	}


	$query = "SELECT value FROM variables WHERE name='respondTime'";
	$result = mysqli_query($con, $query);
	$respondTimeDB = $result->fetch_assoc()['value'];

	$query = "SELECT value FROM variables WHERE name='respondTimeChat'";
	$result = mysqli_query($con, $query);
	$respondTimeDB2 = $result->fetch_assoc()['value'];


	?>

    <!-- Main Content -->
    <section class="content page-calendar" style="margin-top: 0px !important;">
        <div class="block-header">
			<?php require_once('includes/adminTopBar.php'); ?>
        </div>
        <div class="container-fluid">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <form id="myform" name="myform" method="POST" >
                    <div class="card demo-masked-input">
                        <div class="header">
                            <h2><strong>Notification</strong><small> Set responding time for teachers </small></h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix margintop">
                                <div class="col-lg-2 col-md-2">
                                    <b>Respond Time : </b>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                        <input type="text" class="form-control" name="respondTime"
                                               id="respondTime" placeholder="Only in Hours" value="<?php echo $respondTimeDB; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
	                    <input type="submit" class="btn btn-raised m-b-10 bg-green" name="saveRespondTime" value="Save">
                </form>
                <form id="myform" name="myform" method="POST" >
                    <div class="card demo-masked-input">
                        <div class="header">
                            <h2><strong>Notification</strong><small> Set responding time for teachers in Chat</small></h2>
                        </div>
                        <div class="body">
                            <div class="row clearfix margintop">
                                <div class="col-lg-2 col-md-2">
                                    <b>Respond Time Chat : </b>
                                    <div class="input-group">
                                        <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                        <input type="text" class="form-control" name="respondTimeChat"
                                               id="respondTimeChat" placeholder="Only in Hours" value="<?php echo $respondTimeDB2; ?>">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <input type="submit" class="btn btn-raised m-b-10 bg-green" name="saveRespondTimeChat" value="Save">
                </form>
            </div>
        </div>
    </section>
    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


	<?php
	require_once('includes/footerScriptsAddForms.php');
	?>


    <!-- New  JS -->

    <script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
    <!-- Bootstrap Colorpicker Js -->
    <script src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js -->
    <script src="assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js -->
    <script src="assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js -->
    <script src="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
    <!-- Bootstrap Tags Input Plugin Js -->
    <script src="assets/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js -->

    <script src="assets/js/pages/forms/advanced-form-elements.js"></script>
    <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
    <script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
    <!-- New  JS -->

    <script>

        $(document).ready(function () {


            $(".page-loader-wrapper").css("display", "none");

            $(document).ajaxStart(function () {
                $(document).css({'cursor': 'wait'});
            }).ajaxStop(function () {
                $(document.body).css({'cursor': 'default'});
            });

    </script>
    </body>
    </html>
    <div id="fullCalModal" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="margin-top: 0px !important;
padding-top: 0px !important;">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span
                                class="sr-only">close</span></button>
                    <h4 id="modalTitle" class="modal-title"></h4>
                </div>
                <div id="modalBody" class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
	<?php
}
?>