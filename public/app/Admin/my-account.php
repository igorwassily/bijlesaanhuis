A<?php
$thisPage="My Account";
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>My Account</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	.btn-block
	{
		width: 40% !important;
	}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        
		<div class="card">
            <div class="row">
                <div class="col-md-4 col-lg-4 col-xl-4">
                   <div class="card" style="background: transparent !important;">
                   
                    <div class="header">
                        <h2><strong>Personal Information</strong></h2>
                        
					   </div>
					   <div class="body">
						   <?php
			
					
									$adminid = $_SESSION['AdminEmail'];
									$stmt = $con->prepare("SELECT adminUser, adminEmail, adminContact from admin where adminEmail = ?");
	$stmt->bind_param("s",$adminid);
	$stmt->execute();
	$stmt->bind_result($username,$email,$contact);
	$stmt->store_result();
	$stmt->fetch();
	
	?>
						   <form method="post" action = "actions/edit-scripts" >
						   
							   <div class="form-group form-float">
									<label for="booking-code">Username</label>
								   <input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="AdminPersonalInfo">
								   <input type="hidden" class="form-control" placeholder="First Name" name="admin-id" id="admin-id" value="<?php echo $adminid; ?>">
									<input type="text" class="form-control" placeholder="First Name" name="username" id="username" value="<?php echo $username; ?>" readonly>
                                    
                                </div>


								<div class="form-group form-float">
									<label for="booking-code">Email</label>
									<input type="text" class="form-control" placeholder="Last Name" name="email" id="email" value="<?php echo $email; ?>" readonly>
                                    
                                </div>

                               


                                <div class="form-group form-float">
									<label for="booking-code">Contact</label>
									<input type="text" class="form-control" placeholder="Address" name="contact" id="contact" value="<?php echo $contact; ?>">
                                    
                                </div>
								
								 
								
						    <button type="submit" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="update-button" name="update-teacher"> UPDATE </button>
						   </form>
					   </div>
                </div>
                
				</div>
				<div class="col-md-4 col-lg-4 col-xl-4">
                    <div class="card" style="background: transparent !important;">
                   
                    <div class="header">
                        <h2><strong>Profile Information</strong></h2>
                        
					   </div>
					   <div class="body">
						   <?php
			
					
									$adminid = $_SESSION['AdminEmail'];
									$stmt = $con->prepare("select adminProfileImage from admin where adminEmail = ?");
									
	$stmt->bind_param("s",$adminid);
	$stmt->execute();
	$stmt->bind_result($profile);
	$stmt->store_result();
	$stmt->fetch();
	
	?>
						   <form method="post" action = "actions/edit-scripts"  enctype='multipart/form-data'>
						   
							  
									
								    <input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="AdminProfileInfo">
								   <input type="hidden" class="form-control" placeholder="First Name" name="admin-id" id="admin-id" value="<?php echo $adminid; ?>">
								 
									
                                    
                          


								

                                <div class="form-group form-float">
									<div class="row clearfix">
										<div class="col-md-6 col-lg-6 col-xl-6">
										<label for="booking-code">Profile Picture</label>
											<input type="hidden" name="current-image" value="<?php echo $profile; ?>" />
                                    <div class="image"><a href="#"><img src="<?php echo $profile; ?>" alt="User"></a></div>
										</div>
										<div class="col-md-6 col-lg-6 col-xl-6">
										<label for="booking-code">Upload New Picture</label>
									<input type="file" class="form-control" placeholder="Profile Image" name="profile-image" id="profile-image">
										</div>
									</div>
									</div>
                                    
                                    
                               

						    <button type="submit" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-level-btn" name="update-teacher"> UPDATE </button>
						   </form>
					   </div>
                </div>
                </div>
				<div class="col-md-4 col-lg-4 col-xl-4">
                        <div class="card" style="background: transparent !important;">
                   
                    <div class="header">
                        <h2><strong>Account Information</strong></h2>
                        
					   </div>
					   <div class="body">
						   <form  method="post" action = "actions/edit-scripts">
							   
							   <?php
			
					
									$adminid = $_SESSION['AdminEmail'];
									
	?>
						   
							   

                                <div class="form-group form-float">
                                    <label for="booking-code">Password</label>
									<input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="AdminAccountInfo">
								   <input type="hidden" class="form-control" placeholder="First Name" name="admin-id" id="admin-id" value="<?php echo $adminid; ?>">
                                    <input type="password" class="form-control" placeholder="Type New Password" name="password" id="password">
                                    
                                </div>


                              
						    <button type="submit" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-level-btn"  name="update-teacher"> UPDATE </button>
						   </form>
					   </div>
                </div>
                </div>
            </div>
        </div>
		
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


	
	  
<?php
    require_once('includes/footerScriptsAddForms.php');
?>

</body>
</html>
<?php
}
?>