<?php
$thisPage="Teacher subjchange";
require_once('includes/connection.php');
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{	
		if(!isset($_GET['tid'])){
			header('Location: index.php');
		}


		$sql = "SHOW COLUMNS FROM `teacher_courses` LIKE 'permission'";
		$result = mysqli_query($con, $sql);
		$row = mysqli_fetch_assoc($result);
		$type = $row['Type'];
	
		preg_match('/enum\((.*)\)$/', $type, $matches);
		$vals = explode(',', $matches[1]);
	
		$query = "SELECT tc.courseID cid, c.*, tc.permission,(SELECT t.typeName FROM schooltype t WHERE t.typeID=c.coursetype)typename, (SELECT GROUP_CONCAT(schoollevel.levelName) FROM schoollevel WHERE FIND_IN_SET(levelID, tc.schoollevelID))levelname, (SELECT GROUP_CONCAT(schoolyear.yearName) FROM schoolyear WHERE FIND_IN_SET(yearID, tc.schoolyearID))yearname FROM teacher_courses tc, courses c WHERE tc.courseID=c.schoolcourseID AND tc.schoollevelID=c.courselevel AND tc.teacherID=".$_GET['tid'];

            $result = mysqli_query($con, $query);

            $str = "";
            $i=0;
            while($row = mysqli_fetch_assoc($result)){
                $i++;
				
				$option="<select class='tsubj'>";
				
				foreach($vals as $k=>$v){
					$v = trim($v, "'");
					
					if($v == $row['permission'])
						$option.="<option value='$_GET[tid]_$row[cid]_$row[courselevel]_$v' selected>$v</option>";
				else						
						$option.="<option value='$_GET[tid]_$row[cid]_$row[courselevel]_$v'>$v</option>";
				}
				$option .= "</select>";
				
                $str .= "<tr>
                        <td>$i</td>
                        <td>$row[coursename]</td>
                        <td>$row[typename]</td>
                        <td>$row[levelname]</td>
                        <td>$row[yearname]</td>
                        <td>$option</td>
                        <td>
                            <button type=\"button\" class=\"inline-elem-button btn btn-raised btn-warning btn-round waves-effect\" value=\"cancel\" onclick=\"cancelLevel(this, $_GET[tid], $row[cid], $row[courselevel])\" >Delete</button> 
                        </td>
                        </tr>
                        ";
            }

	 

	
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>View Courses</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */

	
	select{	
border: 1px solid #bdbdbd !important;

color: black; !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: black !important;
}

table
{
    color: white;
}

	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	
	table, th
	{
		text-align: center !important;
	}
	.hide{
		display:none;
	}
ul.dropdown-menu.inner {
		display: block;
	}
	
ul.dropdown-menu.inner li a span{color: black;}
	
	
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
 	<!-- Newly pasted code  Starts -->
	<div class="container-fluid">
       
            <div class="row clearfix">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card">

                 <div class="body"> 
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs">
                        <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#pending">Subjects</a></li>
                    </ul>                        
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane in active" id="pending"> <b>Newly Requested Subjects</b>
                           <!-- Exportable Table -->
                            <div class="row clearfix">
                                <div class="col-lg-12">
                                    <div class="card">
                                       <div class="body">
                                            <table class="table table-bordered table-striped table-hover dataTable">
                                                <thead>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Subject Name</th>
                                                        <th>School Type</th>
                                                        <th>School Level</th>
                                                        <th>School Year</th>
                                                        <th>Permission</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tfoot>
                                                    <tr>
                                                        <th>#</th>
                                                        <th>Subject Name</th>
                                                        <th>School Type</th>
                                                        <th>School Level</th>
                                                        <th>School Year</th>
                                                        <th>Permission</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </tfoot>
                                                <tbody>
                                                    <?php echo $str; ?>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!-- #END# Exportable Table --> 
                        </div>
						
                    </div>
                </div>
            </div>
            
        </div>    
            
        </div>
    </div>
	<!-- Newly pased Code ends -->
</section>
<?php
    require_once('includes/footerScripts.php');
?>
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
<script>
	
	var __obj= null;

function cancelLevel(obj, tid, cid, clid){
	__obj=obj;
		 $.ajax({
			  url: 'techerlevel_processing.php',
			  type: 'POST',
			  data: {page:"delete_subj", tid:tid, cid:cid, clid:clid},
			  success: function (result) {
				  if(result == "success"){
						showNotification("alert-success", "Subject has been Deleted", "bottom", "center", "", "");	
						$(__obj).parent().parent().remove();
				  }else{
						showNotification("alert-danger", "Fail to delete subject. Try later", "bottom", "center", "", "");	
				  }
			  }
		  });  //ajax ends  
} //cancelLevel() ends
	
	$(document).ready(function(){
		$("select").change(function(){
			__obj = $(this);
			console.log($(this).val());
			 $.ajax({
			  url: 'techerlevel_processing.php',
			  type: 'POST',
			  data: {page:"change_subj", value:$(this).val()},
			  success: function (result) {
				  if(result == "success"){
						showNotification("alert-success", "Subject permission has been Changed", "bottom", "center", "", "");	
				  }else{
						showNotification("alert-danger", "Fail to change subject permission. Try later", "bottom", "center", "", "");	
				  }
			  }
		  });  //ajax ends  
		});
	});

</script>
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->		
</body>
</html>
<?php
}
?>