<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>Event: Snorkeling above water</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>

	<style type="text/css">
		.material-icons
		{
			vertical-align: bottom;
		}
	</style>
	<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

	<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
	
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Event: Snorkeling above water
                </h2>
				<small>Category: Excursions	

				</small>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Event: Snorkeling above water</li>
                </ul>                
            </div>
        </div>
    </div>
     <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                  <div class="body">
					  		  <div class="row clearfix">
						 
					
						  <div class="col-md-2 col-lg-2">
							  <a href="#" class="btn btn-info btn-round edit-btn" >CXL Event</a>
						</div>
						  <div class="col-md-2 col-lg-2">
							  <a href="#" class="btn btn-info btn-round edit-btn" >Credit Event</a>
						</div>
						  <div class="col-md-2 col-lg-2">
							  <a href="#" class="btn btn-info btn-round edit-btn" >Move booking</a>
						</div>
						  <div class="col-md-3 col-lg-3">
							  <select class="form-control show-tick">
                                    <option value="">Please select</option>
                                    <option value="10">Print Voucher</option>
                                    <option value="20">Download Voucher</option>
                                    <option value="30">Email Clientvoucher
</option>
                                   
                                </select>
						</div>
						   <div class="col-md-3 col-lg-3">
							   <select class="form-control show-tick">
                                    <option value="">Please select</option>
                                    <option value="10">Email Client Welcome</option>
                                    <option value="20">Email Client Generic</option>
                                    
                                </select>
						</div>
						  
					  
					  </div>
					  <br><br>
						  
					   <div class="row clearfix">
							<div class="col-md-12 col-lg-12">
								
                                 <div class="body table-responsive">
									
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="12" class="bg-purple"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Client Information</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <th>BC Code</th>
                                    <th>IH Code</th>
									<th>Date</th>
									<th>First Name</th>
									<th>Last Name</th>
									<th>Pax</th>
									<th>Hotel</th>
									<th>Putime</th>
									<th>Telephone</th>
									<th>Email</th>
									<th>Vouchernr.</th>
									
                                </tr>
								 <tr>
                                    
                                    <td>TIT100</td>
                                    <td>EVE124</td>
									<td>10/17/2018</td>
									<td>John</td>
									<td>Doe</td>
									<td>1/1/1</td>
									<td>Little Austria</td>
									<td>9:45</td>
									<td>55555555</td>
									<td>info@domain.tld</td>
									<td>15813521</td>
									
                                </tr>
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						 
                            </div>
					   <div class="row clearfix">
                           
					
						<div class="col-md-6 col-lg-6">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="6" class="bg-cyan"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Optional</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Meal</td>
                                    <td>Pasta</td>
									
									
                                </tr>
								 <tr>
                                    
                                    <td>Allergies</td>
                                    <td>Onion</td>
									
									
                                </tr>
								 <tr>
                                    
                                    <td>Wheelchair</td>
                                    <td>No</td>
									
									
                                </tr>
								 <tr>
                                    
                                    <td>Guide</td>
                                    <td>None</td>
									
									
                                </tr>
								 <tr>
                                    
                                    <td>fees</td>
                                    <td>350</td>
									
									
                                </tr>
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						   
						<div class="col-md-6 col-lg-6">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="6" class="g-bg-blush2"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Event Information	</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Supplier</td>
                                    <td>123travel</td>
									
									
                                </tr>
								 <tr>
                                    
                                    <td>Type</td>
                                    <td>Excursion</td>
									
									
                                </tr>
								 <tr>
                                    
                                    <td>Rep</td>
                                    <td>Andreas</td>
									
									
                                </tr>
							
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						 
                            </div>
				<div class="row clearfix">
                           
					
						<div class="col-md-6 col-lg-6">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="6" class="bg-light-blue"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Event Status</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Status</td>
                                    <td class="bg-green">Confirmed</td>
									
									
                                </tr>
								 <tr>
                                    
                                    <td>Payment</td>
                                    <td>Paid</td>
									
									
                                </tr>
								
								
								 
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						   
						<div class="col-md-6 col-lg-6">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="6" class="bg-orange"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Agent Information	</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                               
								 <tr>
                                    
                                    <td>Agent</td>
                                    <td>TUI</td>
									
									
                                </tr>
								 <tr>
                                    
                                    <td>Subagent</td>
                                    <td>TUIDUE</td>
									
									
                                </tr>
								
									
									
                               
							
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						 
                            </div>
					   <div class="row clearfix">
							<div class="col-md-12 col-lg-12">
								
                                 <div class="body table-responsive">
									
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="12" class="bg-light-blue"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Payment Information</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <th>TransferID</th>
                                    <th>TransferType</th>
									<th>Date</th>
									<th>Amount</th>
									<th>VAT</th>
									<th>User</th>
									
									
                                </tr>
								 <tr>
                                    
                                    <td>TRA2232332</td>
                                    <td>Cash</td>
									<td>10/17/2018</td>
									<td>500</td>
									<td>35</td>
									<td>Andreas</td>

									
                                </tr>
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						 
                            </div>
					   
					  
					   <div class="row clearfix">
							<div class="col-md-12 col-lg-12">
								
                                 <div class="body table-responsive">
									
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="12" class="g-bg-blue"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Communication Log</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                   
									<th>Date</th>
									<th>Type</th>
									<th>Template_name</th>
									<th>Receiver</th>
									
									
                                </tr>
								 <tr>
                                    
                                
									<td class="">10/17/2018</td>
									<td>Automated Email</td>
									<td>Pickup time</td>
									<td>info@domain.tld</td>
									
                                </tr>
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						 
                            </div>
					  
					
					  
				
				
                        </div>
				</div>
                </div>
            </div>
        
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
	
	<script src="assets/plugins/momentjs/moment.js"></script> <!-- Moment Plugin Js --> 
<!-- Bootstrap Material Datetime Picker Plugin Js --> 
<script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script> 

</body>
</html>
<?php
}
?>