<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>Update Licence Key</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Update Licence Key
               
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Update Licence Key</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
			 <div class="col-lg-3"></div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="body">
						<p>You can change your license key by entering your admin login details and new key below.<br>Requires full admin access permision</p>
						 <form>
                           
                            <div class="form-group">                                
                                <input type="text" id="email_address" class="form-control" placeholder="Kevin">
                            </div>
                            
                            <div class="form-group">                                
                                <input type="password" id="password" class="form-control" placeholder="********">
                            </div>
							 <div class="form-group">                                
                                <input type="text" id="new_license_key" class="form-control" placeholder="New License Key">
                            </div>
                            <div class="form-group">                                
                                <button type="button" class="btn btn-raised btn-primary btn-round waves-effect">Change License Key</button>
                            </div>
                           
                        </form>
                    </div>
                </div>
            </div>
			 <div class="col-lg-3"></div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
</body>
</html>
<?php
}
?>