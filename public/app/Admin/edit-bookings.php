<?php
$thisPage="Client Edit";
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Edit Booking Information</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');date('Y-m-d H:i:s');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}
	label
	{
		color: white;
	}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Edit Bookings
                
                </h2>
            </div>            
            <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
			<div class="col-lg-3 col-md-3 col-sm-12"></div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                
                    	<?php
					
	if(isset($_POST['booking-edit-submit']))
	{
		$bookingID = test_input($_POST['bookingedit-id']);
										$stmt = $con->prepare("select bookingID, bookingcode, branchID, clientID, agentID, hotelbookingid, transferbookingid, departuretransferbookingid, eventbookingid, arrival_date, arrival_by, arrival_flight, arrival_flight_time, departure_date, departure_by, departure_flight, departure_flight_time, adults, children, enfants, booking_files, guest_names, administration_note, operation_note, accounting_note, representative_note from booking where bookingID = ?");
		$stmt->bind_param("i",$bookingID);
										if($stmt->execute())
										{
											
											$stmt->bind_result($bID,$bCode,$brID,$clID,$agID,$hbID,$tbID,$dtbID,$ebID,$adate,$aby,$aflight,$aflighttime,$ddate,$dby,$dflight,$dflighttime,$adults,$children,$enfants,$bFiles,$gNames,$anote,$onote,$acnote,$rnote);
											$stmt->store_result();
											$stmt->fetch();
										}
	
	?>
                        <form id="frmEditBooking" enctype="multipart/form-data">
							<br/><br/><br/><br/><br/>
							<div class="form-group form-float">
									<label for="booking-code">Enter Booking Code</label>
									<input type="hidden" class="form-control" placeholder="Booking Code" name="choice" id="choice" value="EditBookings">
                                    <input type="text" class="form-control" placeholder="Booking Code" name="booking-code" id="booking-code" value = "<?php echo $bCode; ?>">
									<input type="hidden" class="form-control" placeholder="Booking ID" name="booking-id" id="booking-id" value = "<?php echo $bID; ?>">
                                </div>
								
								<div class="form-group form-float">
									<label for="booking-code">Enter Guest Names</label>
									<input type="text" class="form-control" placeholder="Guest Names (Comma Separated)" name="guest-names" id="guest-names" value = "<?php echo $gNames; ?>">
                                    
                                </div>
								
								 
								<div class="form-group form-float">
									<div class="row">
									 <div class="col-lg-4 col-md-4 col-sm-12">
										 <label for="adults">Enter Adults No.</label>
                                    	<input type="number" class="form-control" placeholder="Adults" name="adults" id="adults" value = "<?php echo $adults; ?>">
									</div>
									
									 <div class="col-lg-4 col-md-4 col-sm-12">
										<label for="children">Enter Children No.</label>
                                    <input type="number" class="form-control" placeholder="Children" name="children" id="children" value = "<?php echo $children; ?>">
									</div>
									
									 <div class="col-lg-4 col-md-4 col-sm-12">
										 <label for="enfants">Enter Enfants No.</label>
                                    <input type="number" class="form-control" placeholder="Enfants" name="enfants" id="enfants" value = "<?php echo $enfants; ?>">
									</div>
									</div>
									
                                </div>
								
								
								<div class="form-group form-float">
								<label for="pick-date">Choose Arrival Date</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control" placeholder="Please choose date & time..." id="booking-arrival-date" name="booking-arrival-date" value = "<?php echo $adate; ?>">
                                </div>
								</div>
								<div class="form-group form-float">
									 <label for="agent">Choose Arrival By</label>
                                   <select class="form-control show-tick" id="booking-arrival-by" name="booking-arrival-by">
                                    <option value="">-- Please select --</option>
                                    <option value="aeroplane">Aeroplane</option>
                                    <option value="bus">Bus</option>
                                    
                                </select>
                                </div>
								<div class="form-group form-float">
									<label for="booking-code">Enter Arrival Flight No.</label>
									<input type="text" class="form-control" placeholder="Enter Arrival Flight No." name="booking-arrival-flight" id="booking-arrival-flight" value = "<?php echo $aflight; ?>">
                                    
                                </div>
								<div class="form-group form-float">
								<label for="pick-date">Choose Arrival Time</label>
                                   <div class="input-group">
                                    
                                    <input type="time" class="form-control" placeholder="Please choose time..." id="booking-arrival-time" name="booking-arrival-time" value = "<?php echo $aflighttime; ?>">
                                </div>
								</div>
								
								<div class="form-group form-float">
								<label for="pick-date">Choose Departure Date</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control" placeholder="Please choose date & time..." id="booking-departure-date" name="booking-departure-date" value = "<?php echo $ddate; ?>">
                                </div>
								</div>
								<div class="form-group form-float">
									 <label for="agent">Choose Departure By</label>
                                   <select class="form-control show-tick" id="booking-departure-by" name="booking-departure-by">
                                    <option value="">-- Please select --</option>
                                    <option value="aeroplane">Aeroplane</option>
                                    <option value="bus">Bus</option>
                                    
                                </select>
                                </div>
								<div class="form-group form-float">
									<label for="booking-code">Enter Departure Flight No.</label>
									<input type="text" class="form-control" placeholder="Enter Departure Flight No." name="booking-departure-flight" id="booking-departure-flight" value = "<?php echo $dflight; ?>">
                                    
                                </div>
								<div class="form-group form-float">
								<label for="pick-date">Choose Departure Time</label>
                                   <div class="input-group">
                                    
                                    <input type="time" class="form-control" placeholder="Please choose time..." id="booking-departure-time" name="booking-departure-time" value = "<?php echo $dflighttime; ?>">
                                </div>
								</div>
								
								
								<!--<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-6">
								<label for="pick-date">Administration Note</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control" placeholder="Please Enter ...." id="administration-note" name="administration-note" value = "<?php echo $anote; ?>">
                                </div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="form-group form-float">
									<label for="pick-date">Operation Note</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control" placeholder="Please Enter ...." id="operation-note" name="operation-note" value = "<?php echo $onote; ?>">
                                </div>
									</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
									<div class="form-group form-float">
									 <label for="pick-date">Accounting Note</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control" placeholder="Please Enter ...." id="accounting-note" name="accounting-note" value = "<?php echo $acnote; ?>">
                                </div>
									</div>
										</div>
									<div class="col-lg-3 col-md-3 col-sm-6">
								<label for="pick-date">Representative Note</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control" placeholder="Please Enter ...." id="representative-note" name="representative-note" value = "<?php echo $rnote; ?>">
                                </div>
									</div>
								</div>-->
							<button type="submit" name="booking-edit-submit" class="btn btn-warning btn-round text-center">UPDATE</button>
							
                        </form> 
                   <?php
	}
	?>
                
            </div>
			<div class="col-lg-3 col-md-3 col-sm-12"></div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script>
	<script src="assets/bundles/vendorscripts.bundle.js"></script>
	
	<!-- Jquery steps Wizard Code -->
	<script type="text/javascript">
	
		$(document).ready(function(){ 
		//Update Project Manager
 function manualValidateUpdate(ev) {
     if(ev.target.checkValidity()==true)
     {
     ev.preventDefault();
     
    run_waitMe($('body'), 1, "timer");
     //$("#modal-projectmanager").modal('hide');
     /*$.loader({
            className:"blue-with-image",
            content:''
          });*/
     
                      $.ajax({
                            url: 'actions/edit_scripts.php',
                            type: 'POST',
                           data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {


                      
                                    switch(result)
                                    {

                                      case "Error":
                                    
                                        
                                        setTimeout(function() {   
                                                 
                                               $('body').waitMe('hide'); 
										   
                                               $('#employeeedit-form')[0].empty(); 
                                               $("#eresult").html(
        '<div id="error-eem" class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'There Was An Error Updating The Record.' + 
         '</div>');
                                               //$("#error-epm").html("There Was An Error Inserting The Record."); 
                                                  
}, 500); 
                                        break;

                                        case "Success":
                                    
                                        
                                        setTimeout(function() { 
                                                    $('body').waitMe('hide');
                                                    
											 window.location.replace("https://dev.we-think.nl/bookings-overview");
}, 500); 

                                        break;

                                    
                                    }
                              }
                      });
      }
 }$("#frmEditBooking").bind("submit", manualValidateUpdate);
			
		});
	
	</script>
	<script>
  $(function() { $( "#booking-arrival-date" ).datepicker(); });
  </script>
	<script>
  $(function() { $( "#booking-departure-date" ).datepicker(); });
  </script>
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
</body>
</html>
<?php
}
?>