<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>System Health Status</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>System Health Status
               </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">System Health Status</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                  <div class="body">
					  
					 <div class="row clearfix">
                            <div class="col-md-4 col-lg-4">
                               
											<div class="alert bg-green">
                            <p> <strong>16</strong>  <br>Successful Checks</p>
                        </div>
                                   <div class="panel-group full-body" id="accordion_13" role="tablist" aria-multiselectable="true">
                                 <div class="panel bg-green">
                                        <div class="panel-heading" role="tab" id="headingOne_13">
                                            <h4 class="panel-title"> <a role="button" data-toggle="collapse" data-parent="#accordion_13" href="#collapseOne_13" aria-expanded="true" aria-controls="collapseOne_13" class="">Successful Checks </a> </h4>
                                        </div>
                                        <div id="collapseOne_13" class="panel-collapse in collapse show" role="tabpanel" aria-labelledby="headingOne_13" aria-expanded="true" style="">
                                            <div class="panel-body"> 
											
												
												
												
												
												
												<div class="alert" style="background:white !important; color:black !important; border-radius:2px !important;margin-bottom: 0.25rem !important">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
												<div class="alert" style="background:white !important; color:black !important; border-radius:2px !important;margin-bottom: 0.25rem !important">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
												<div class="alert" style="background:white !important; color:black !important; border-radius:2px !important;margin-bottom: 0.25rem !important">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
											
											
											</div>
											
                                        </div>
                                    </div>
                                    
                                </div>
                                   
                                </div>
						  <div class="col-md-4 col-lg-4">
							  <div class="alert bg-orange">
                             <p> <strong>2</strong>  <br>Warnings!</p>
                        </div>
                                 <div class="panel-group full-body" id="accordion_13" role="tablist" aria-multiselectable="true">
                                 <div class="panel bg-orange">
                                        <div class="panel-heading" role="tab" id="headingTwo_13">
                                            <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_13" href="#collapseTwo_13" aria-expanded="false" aria-controls="collapseTwo_13"> Warnings </a> </h4>
                                        </div>
                                        <div id="collapseTwo_13" class="panel-collapse in collapse show" role="tabpanel" aria-labelledby="headingTwo_13" aria-expanded="false" style="">
                                            <div class="panel-body">
											<div class="alert" style="background:white !important; color:black !important; border-radius:2px !important;margin-bottom: 0.25rem !important">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
												<div class="alert" style="background:white !important; color:black !important; border-radius:2px !important;margin-bottom: 0.25rem !important">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
												<div class="alert" style="background:white !important; color:black !important; border-radius:2px !important;margin-bottom: 0.25rem !important">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
											</div>
                                        </div>
                                    </div>
                                    
                                </div>
                                   
                                </div>
						  <div class="col-md-4 col-lg-4">
							   <div class="alert bg-red">
                           <p> <strong>3</strong>  <br>Needing Attention!</p>
                        </div>
                                 <div class="panel-group full-body" id="accordion_13" role="tablist" aria-multiselectable="true">
                                 <div class="panel bg-red">
                                        <div class="panel-heading" role="tab" id="headingThree_13">
                                            <h4 class="panel-title"> <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion_13" href="#collapseThree_13" aria-expanded="false" aria-controls="collapseThree_13"> Needing Attention </a> </h4>
                                        </div>
                                        <div id="collapseThree_13" class="panel-collapse in collapse show" role="tabpanel" aria-labelledby="headingThree_13" aria-expanded="false">
                                            <div class="panel-body"> 
										
												<div class="alert" style="background:white !important; color:black !important; border-radius:2px !important;margin-bottom: 0.25rem !important">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
								
															<div class="alert" style="background:white !important; color:black !important; border-radius:2px !important;margin-bottom: 0.25rem !important">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
											<div class="alert" style="background:white !important; color:black !important; border-radius:2px !important;margin-bottom: 0.25rem !important">
                            <strong>Well done!</strong> You successfully read this important alert message.
                        </div>
								
													
								
												
											</div>
                                        </div>
                                    </div>
                                    
                                </div>
                                    
                                   
                                   
                                </div>
                            </div>
                        </div>
				</div>
                </div>
            </div>
        
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
</body>
</html>
<?php
}
?>