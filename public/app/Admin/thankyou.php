<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

    <title>Thankyou</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-green authentication sidebar-collapse">
<!-- Navbar -->

<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" style="background-image:url(img/online-3307293_1920.jpg)"></div>
    <div class="container">
        <div class="col-md-12 content-center" style="top:20% !Important;">
            <div class="card-plain" style="max-width: 500px !important;">
                <form class="form" method="" action="">
                    <div class="header">
                        <div class="logo-container">
                            <h2>BIJLES AAN HUIS</h2>
                        </div>
                        <h5>Thankyou for Registering Account with us. Kindly wait for Admin Approval.</h5>
                        <span>Do you have any query? Feel free to ask</span>
                    </div>
                   
                    <div class="footer text-center">
                        <a href="https://tutor.dev.we-think.nl/" class="btn btn-primary btn-round btn-lg btn-block waves-effect waves-light">GO TO HOMEPAGE</a>
                     
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer">
        <div class="container">
           
            <div class="copyright">
                &copy;
               
            </div>
        </div>
    </footer>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
</script>
</body>
</html>