<?php
$thisPage="Add Courses";
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Add Courses</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.css">

<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: #486066 !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: #486066;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled]{
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	
	.dropdown-menu > ul > li > a
	{
		color: #555 !important;
	}
	
	.btn-block
	{
		width: 40% !important;
	}
	
	#add-course-btn
	{
		width: 20% !important;
	}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="row">
                <div class="col-md-6 col-lg-3 col-xl-3">
                   <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Add School Type</strong></h2>
                        
                    </div>
					   <div class="body">
						   <form id="school-type">
						    <label for="enfants">Type</label>
						    <input type="hidden" class="form-control" placeholder="Enter Type" name="choice" id="choice" value="SchoolType"><br/>
                                    <input type="text" class="form-control" placeholder="Enter Type" name="type-name" id="type-name" required><br/>
						   <button type="submit" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-type-btn"> ADD </button>
						   </form>
					   </div>
                </div>
				</div>
				<div class="col-md-6 col-lg-3 col-xl-3">
                    <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Add School Level</strong></h2>
                        
                    </div>
					   <div class="body">
						   <form id="school-level">
						    <label for="enfants">Level</label>
							   <input type="hidden" class="form-control" placeholder="Enter Type" name="choice" id="choice" value="SchoolLevel"><br/>
                                    <input type="text" class="form-control" placeholder="Enter Level" name="level-name" id="level-name" required><br/>
						    <button type="submit" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-level-btn"> ADD </button>
						   </form>
					   </div>
                </div>
                </div>
				<div class="col-md-6 col-lg-3 col-xl-3">
                    <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Add School Year</strong></h2>
                        
                    </div>
					   <div class="body">
						   <form id="school-year">
						    <label for="enfants">Year</label>
                                    
							   <input type="hidden" class="form-control" placeholder="Enter Type" name="choice" id="choice" value="SchoolYear" ><br/>
							   <input type="text" class="form-control" placeholder="Enter Year" name="year-name" id="year-name" required><br/>
						    <button type="submit" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-year-btn"> ADD </button>
						   </form>
					   </div>
                </div>
                </div>
				<div class="col-md-6 col-lg-3 col-xl-3">
                    <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Add School Course</strong></h2>
                        
                    </div>
					   <div class="body">
						   <form id="school-course">
						    <label for="enfants">Course</label>
                                    
							   <input type="hidden" class="form-control" placeholder="Enter Type" name="choice" id="choice" value="SchoolCourse" ><br/>
							   <input type="text" class="form-control" placeholder="Enter Course" name="coursename" id="coursename" required><br/>
						    <button type="submit" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-year-btn"> ADD </button>
						   </form>
					   </div>
                </div>
                </div>
            </div>
			
			<div class="row">
				 <div class="col-md-12 col-lg-12 col-xl-12">
				<form id="add-course">
                
					<input type="hidden" class="form-control" placeholder="Enter Type" name="choice" id="choice" value="Course"><br/>
                   <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Add Course</strong></h2>
                        
                    </div>
					   <div class="body">
						   <div class="row">
						   
							   <div class="col-md-3 col-lg-3 col-xl-3">
						    <label for="dropoff-location">Select School Type</label>
                                   <div class="input-group">
                                    <select class="form-control show-tick"  name="select-school-type" id="select-school-type" required>
                               
										
                                    <?php
									
										$stmt = $con->prepare("SELECT DISTINCT typeID, typeName from schooltype group by typeName");
										$stmt->execute();
										$stmt->bind_result($typeID,$typeName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									
									<option value="<?php echo $typeID; ?>"><?php echo $typeName; ?></option>
									<?php
										}
									?>
                                </select>
									   
								</div>
						   </div>
							   
						   <div class="col-md-3 col-lg-3 col-xl-3">
						    <label for="dropoff-location">Select School Course</label>
							   
                                   <div class="input-group">
                                    <select class="form-control show-tick"  name="select-school-course" id="select-school-course" required>
                               		<option value="">Select</option>										
                                    <?php
									
										$stmt = $con->prepare("SELECT DISTINCT schoolcourseID, courseName from schoolcourse group by courseName");
										$stmt->execute();
										$stmt->bind_result($courseID,$courseName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $courseID; ?>"><?php echo $courseName; ?></option>
									<?php
										}
									?>
                                </select>
									   
								</div>
						   </div>

						   <div class="col-md-3 col-lg-3 col-xl-3 school-level-div">
						    <label for="dropoff-location">Select School Level</label>
                                   <div class="input-group">
                                    <select class="form-control show-tick" name="select-school-level" id="select-school-level">
										<option value="">Select</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoollevel group by levelName");
										$stmt->execute();
										$stmt->bind_result($levelID,$levelName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $levelID; ?>"><?php echo $levelName; ?></option>
									<?php
										}
									?>
                                </select>
							   </div>
								</div>
						   <div class="col-md-3 col-lg-3 col-xl-3 school-year-div">
						   
							    <label for="dropoff-location">Select School Year</label>
                                   <div class="input-group">
                                    <select class="form-control show-tick"  multiple="multiple" name="select-school-year" id="select-school-year">
                            
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoolyear group by yearName");
										$stmt->execute();
										$stmt->bind_result($yearID,$yearName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $yearID; ?>"><?php echo $yearName; ?></option>
									<?php
										}
									?>
                                </select>
							   </div>
						   </div>
							   
						   </div>
						   
							   <input type="hidden"  name="mschool-level" id="mschool-level" value="">
							   <input type="hidden"  name="mschool-year" id="mschool-year" value="">
							   <input type="hidden"  name="mschool-type" id="mschool-type" value="">
							   <input type="hidden"  name="mschool-course" id="mschool-course" value="">
							   
						   <button type="submit" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-course-btn"> ADD </button>
						 
					   </div>
					</div>
			
				
				</form>
            </div>
			</div>
        </div>
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


	
	<!-- Jquery steps Wizard Code -->
	<script type="text/javascript">
		
		
		
		init({
    title: 'basic select',
    desc: 'Multiple Select can take a regular select box.',
    links: ['multiple-select.css'],
    scripts: ['multiple-select.js']
  })
		
		
		
		$('select').multipleSelect()
		
		
		
		
	$(function () {
    //Advanced form with validation
    var form = $('#teacherRegistration').show();
    form.steps({
        headerTag: 'h4',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
          //  $.AdminOreo.input.activate();
            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
			
			
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
			//Jquery AJAX code to add client upon  insertion
            
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            //Code for Forms Data Submission
			run_waitMe($('body'), 1, "timer");
			$.ajax({
                            url: 'actions/add_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {


                          
                                   switch(result)
                                    {

                                      case "Error":
                                    
                                        
                                        setTimeout(function() {   
                                                 
                                               $('body').waitMe('hide');
                                               $('#teacherRegistration')[0].empty(); 
                                                showNotification("alert-danger","Sorry!!! Your account cannot be created right now.","top","center","","");
                                               //$("#error-epm").html("There Was An Error Inserting The Record."); 
                                                  
}, 500); 
                                        break;

                                         case "Success":
                                    
                                        
                                       setTimeout(function() { 

                                                  $('body').waitMe('hide');
                                                  $('#teacherRegistration')[0].reset();
                                                  showNotification("alert-success","Kindly, check your email to activate your account","top","center","","");
                                                 
                                                                                                    
}, 500);
                                        break;

                                    
                                    }
                              }
                      });
		
        }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'confirm': {
                equalTo: '#password'
            }
        }
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}
		
		
		function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
        message: text
    },
        {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
}
		
	
	</script>  
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
<script src='assets/plugins/fullcalendar/lib/moment.min.js'></script>
<script src='assets/plugins/fullcalendar/fullcalendar.min.js'></script>
<script src="assets/js/pages/calendar/calendar.js"></script>
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
	
<!-- Latest compiled and minified JavaScript -->
<script src="//cdnjs.cloudflare.com/ajax/libs/multiple-select/1.2.2/multiple-select.min.js"></script>
<script>

  $(document).ready(function() {

    /* Add New School Type */
 function manualValidateAdd(ev) {
     if(ev.target.checkValidity()==true)
     {
     ev.preventDefault();
    run_waitMe($('body'), 1, "timer");
         $.ajax({
                            url: 'actions/add_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {
								 
								 					if(result == "Error")
													{
														
													}
								 else if(result == "Duplicate")
													{
														 $('body').waitMe('hide');
                                                 	$('#school-type')[0].reset();
                                                  showNotification("alert-warning","School Type Already Exists.","top","center","","");
                                                                                                                                              
													}
								 else
								 {
                          //  alert(result);
                                    		setTimeout(function() { 
                                                  $('body').waitMe('hide');
                                                 	$('#school-type')[0].reset();
                                                  showNotification("alert-success","School Type Addedd Successfully.","top","center","","");
                                                                                                                                              }, 500);
								 $("#select-school-type").html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                					//alert(json_obj[i].courseID + " " + json_obj[i].courseName);
									$("#select-school-type").append('<option value="'+String(json_obj[i].typeID)+'">'+String(json_obj[i].typeName)+'</option>');
								}
					
								 $("#select-school-type").selectpicker('refresh');
								 }		
									
                              }
                      });
      }
 }$("#school-type").bind("submit", manualValidateAdd);
	  
	  
    /* Add New School Level */
 function manualValidateAdd01(ev) {
     if(ev.target.checkValidity()==true)
     {
     ev.preventDefault();
    run_waitMe($('body'), 1, "timer");
         $.ajax({
                            url: 'actions/add_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                            success: function (result) {
								 
								 					if(result == "Error")
													{
														
													}
								
								else if(result == "Duplicate")
													{
														 $('body').waitMe('hide');
                                                 	$('#school-level')[0].reset();
                                                  showNotification("alert-warning","School Level Already Exists.","top","center","","");
                                                                                                                                              
													}
								
								
								
								 else
								 {
                          //  alert(result);
                                    		setTimeout(function() { 
                                                  $('body').waitMe('hide');
                                                 	$('#school-level')[0].reset();
                                                  showNotification("alert-success","School Level Addedd Successfully.","top","center","","");
                                                                                                                                              }, 500);
								 $("#select-school-level").html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                					//alert(json_obj[i].courseID + " " + json_obj[i].courseName);
									$("#select-school-level").append('<option value="'+String(json_obj[i].levelID)+'">'+String(json_obj[i].levelName)+'</option>');
								}
					
								 $("#select-school-level").selectpicker('refresh');
								 }		
									
                              }
                      });
      }
 }$("#school-level").bind("submit", manualValidateAdd01);
	  
	  
    /* Add New School Year */
 function manualValidateAdd02(ev) {
     if(ev.target.checkValidity()==true)
     {
     ev.preventDefault();
    run_waitMe($('body'), 1, "timer");
         $.ajax({
                            url: 'actions/add_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                            success: function (result) {
								 
								 					if(result == "Error")
													{
														
													}
								else if(result == "Duplicate")
													{
														 $('body').waitMe('hide');
                                                 	$('#school-year')[0].reset();
                                                  showNotification("alert-warning","School Year Already Exists.","top","center","","");
                                                                                                                                              
													}
								 else
								 {
                          //  alert(result);
                                    		setTimeout(function() { 
                                                  $('body').waitMe('hide');
                                                 	$('#school-year')[0].reset();
                                                  showNotification("alert-success","School Year Addedd Successfully.","top","center","","");
                                                                                                                                              }, 500);
								 $("#select-school-year").html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                					//alert(json_obj[i].courseID + " " + json_obj[i].courseName);
									$("#select-school-year").append('<option value="'+String(json_obj[i].yearID)+'">'+String(json_obj[i].yearName)+'</option>');
								}
					
								 $("#select-school-year").selectpicker('refresh');
								 }		
									
                              }
                      });
      }
 }$("#school-year").bind("submit", manualValidateAdd02);
	  
	  
	  
	   /* Add New School Year */
 function manualValidateAdd05(ev) {
     if(ev.target.checkValidity()==true)
     {
     ev.preventDefault();
    run_waitMe($('body'), 1, "timer");
         $.ajax({
                            url: 'actions/add_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                            success: function (result) {
								 
								 					if(result == "Error")
													{
														
													}
								else if(result == "Duplicate")
													{
														 $('body').waitMe('hide');
                                                 	$('#school-year')[0].reset();
                                                  showNotification("alert-warning","School Year Already Exists.","top","center","","");
                                                                                                                                              
													}
								 else
								 {
                          //  alert(result);
                                    		setTimeout(function() { 
                                                  $('body').waitMe('hide');
                                                 	$('#school-year')[0].reset();
                                                  showNotification("alert-success","School Course Addedd Successfully.","top","center","","");
                                                                                                                                              }, 500);
								 }		
									
                              }
                      });
      }
 }$("#school-course").bind("submit", manualValidateAdd05);
	  
	   /* Add New School Year */
 function manualValidateAdd03(ev) {
     if(ev.target.checkValidity()==true)
     {
     ev.preventDefault();
		 var level = $("#select-school-level").val();
		 	//level = level.join(",");
		  $("#mschool-level").val(level);
		 
		 var year = $("#select-school-year").val();
		 year = year.join(",");
		  $("#mschool-year").val(year);
		 
		 var type = $("#select-school-type").val();
		// type = type.join(",");
		  $("#mschool-type").val(type);
		 
    run_waitMe($('body'), 1, "timer");
         $.ajax({
                            url: 'actions/add_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {
                          //  alert(result);
                                    switch(result)
                                    {

                                      case "Error":
                                    
                                       
                                        break;

                                        case "Success":
                                    		setTimeout(function() { 
                                                  $('body').waitMe('hide');
                                                 	$('#school-year')[0].reset();
                                                  showNotification("alert-success","Course Addedd Successfully.","top","center","","");
                                                                                                                                              }, 500);
											
												
                                        break;

                                    
                                    }
                              }
                      });
      }
 }$("#add-course").bind("submit", manualValidateAdd03);
	  
	  
	  /* Notification */
	  function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}
		
function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
        message: text
    },
        {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
}
	  
	 

  });

	
	$("document").ready(function(){
		$("#select-school-type" ).change(function(e) {
		  e.stopImmediatePropagation();
			
		 var text = $(this).find("option:selected").text();
		  var id = $(this).find("option:selected").val();
		  if(id == <?php echo ELEMENTRY_TYPE_ID; ?>)
		  {
			  $(".school-level-div").css('display','none');
			  $(".school-year-div").css('display','none');

			  $("#selected-school-type").val(text);
		  }
		  
		  else
		  {
			  $(".school-level-div").css('display','block');
			  $(".school-year-div").css('display','block');
			  // $(".school-course-div").css('display','block');
			 // $("#add-course-btn").prop("disabled", false);
			  $("#selected-school-type").val(text);
		  }
		/*	
		  var stype = $("#select-school-type").val();

		  stype = (typeof(stype) == "object")? stype.join(",") : stype;
		  var slevel = ""; //$("#school-level").children("option:selected").val()
		 // slevel =  (typeof(slevel) == "object")? slevel.join(",") : "";
	
		  var syear = ""; // $("#school-year").children("option:selected").val()
		 // syear =  (typeof(syear) == "object")? syear.join(",") : "";
		  
		   $.ajax({
			 url: 'actions/course_filter.php',
			 data: {subpage:"coursetype", stype:stype},
				  			type: "POST",
							//dataType: "json",
                            success: function (result) {
                					$("#select-school-course").html(result);
								$("#select-school-course").selectpicker('refresh');
								 searchTeacher();
							}
                      }); 
		 */
	  }); //show-tick ends
	
	$("#select-school-course").change(function(e){
		  e.stopImmediatePropagation();
		var cid = $(this).val(); 
		var ctext = $("#select-school-course :selected").text();
		$("#mschool-course").val(ctext);
		 $.ajax({
			 url: 'actions/course_filter.php',
			 data: {subpage:"courselevel", cid:cid},
			 type: "POST",
			 //dataType: "json",
			 success: function (result) {
				 $("#select-school-level").html(result);
				 $("#select-school-level").selectpicker('refresh');
			 }
		 }); //ajax ends
	});// change ends
		
		
	$("#select-school-level").change(function(e){
		  e.stopImmediatePropagation();
		var clid = $(this).val(); 
		var cid = $("#school-course1").val();
		 $.ajax({
			 url: 'actions/course_filter.php',
			 data: {subpage:"courseyear", cid:cid, clid:clid},
			 type: "POST",
			 //dataType: "json",
			 success: function (result) {
				 $("#-select-school-year").html(result);
				 $("#-select-school-year").selectpicker('refresh');
			 }
		 }); //ajax ends
	});// change ends
	
	});
</script>
	<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
</body>
</html>
<?php
}
?>



