<?php
session_start();
if (!isset($_SESSION['AdminID'])) {
    header('Location: index.php');
    exit();
}
require_once('includes/connection.php');
//require_once '../api/Template.php';

$thisPage = "Bijles create page";
$activePage = basename($_SERVER['PHP_SELF']);

//$template = new API\ENDPOINT\Template($con);
?>

<!doctype html>
<html class="no-js " lang="en">

<head>
    
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Generating Landingpages.">
    <title>Bijles template</title>
    <?php
    //require_once('includes/mainCSSFiles.php');
    // echo '<pre>'.print_r($con->get_charset()).'</pre>';
    ?>
    <meta name="robots" content="noindex" />
    <meta name="googlebot" content="noindex" />
    <meta name="googlebot-news" content="noindex" />

    <link rel="stylesheet" href="/app/Admin/assets/plugins/bootstrap/css/bootstrap.min.css">

    <!-- Custom Css -->
    <link rel="stylesheet" href="/app/Admin/assets/css/main.css">
    <link rel="stylesheet" href="/app/Admin/assets/css/color_skins.css">
    <link rel="stylesheet" href="/app/Admin/assets/plugins/waitme/waitMe.css">
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

    <link href="/app/Admin/assets/css/bijles-template-generator.css" rel="stylesheet" />
    <style type="text/css">
        .inline-elem-button {
            margin-top: 20px;
        }

        .form-control,
        .btn-round.btn-simple {
            color: #ece6e6;
        }

        .input-group-addon {
            color: #ece6e6;
        }

        .text {
            color: black;
        }

        .margin-top {
            margin-top: 10px;
        }

        .checkbox {
            float: left;
            margin-right: 20px;
        }

        .margintop {
            margin-top: 20px;
            color: #5cc75f;
        }

        .block-header {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            z-index: 100;
            background-color: inherit;
        }

        .theme-green .nav-tabs .nav-link.active {
            color: white !important;
            background-color: #5cc75f !important;
            border: 2px solid #5cc75f !important;
        }

        .nav-item {
            font-weight: 600;
        }

        .nav-tabs>.nav-item>.nav-link {
            color: #888;
            margin: 0;
            margin-right: 5px;
            background-color: transparent;
            border: 1px solid transparent;
            border-radius: 6px !important;
            font-size: 14px;
            padding: 11px 23px;
            line-height: 1.5;
        }

        .btn-primary {
            background: transparent !important;
            color: #5CC75F !important;
            border: 2px solid #5CC75F !important;
            border-radius: 22px !important;
            letter-spacing: 0;
            font-size: 14px;
            font-family: 'Poppins', Helvetica, Arial, Lucida, sans-serif !important;
            font-weight: 600 !important;
            text-transform: uppercase !important;
        }

        .btn-primary:hover,
        .theme-green .btn-primary:focus {
            color: #F1F1F1 !important;
            background-color: #5CC75F !important;
        }
    </style>
</head>

<body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo">
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <?php
    require_once('includes/sidebarAdminDashboard.php');
    ?>

    <!-- Main Content -->
    <section class="content page-calendar" style="margin-top: 0!important;">
        <div class="block-header">
            <?php require_once('includes/adminTopBar.php'); ?>
        </div>
        <div class="container-fluid">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card demo-masked-input">
                    <div class="header">
                        <h2><strong>Bijles</strong> template <small> Create pages per course </small>
                        </h2>
                    </div>
                    <?php
                    if (isset($_GET['status'])) {
                        if($_GET['status'] == 1) {
                            echo "<div><a style='color: #5cc75f;' href='{$_GET['successLink']}' target='_blank'>Your page</a> has been created!</div>";
                        } else {
                            echo "<div style='color:red'>{$_GET['message']}</div>";
                        }
                    }
                    ?>
                    <div class="body">
                        <h2>Sections</h2>
                        <form id="create-template" method="post" style="width: 100%" action="bijles-generate-template" accept-charset="utf-8">
                            <div id="layout-options">
                                <input id="create-layout" type="button" value="Create Layout" class="btn" />
                            </div>
                            <div id="createTemplateParameters"></div>
                            <!-- <input id="template-name" name="template-name" type="hidden" value="" /> -->
                            <!-- <select id="template-name" name="template-name" type="hidden" value="default">
                                <option value="default" selected="">default</option>
                            </select> -->
                            <div id="page-options">
                                <div class="submit-button-wrapper">
                                    <button type="button" class="btn btn-secondary btn-border--halfradius" onclick="setPositionsAfterDelete()">Set Positions</button>
                                    <input id="submitButton" type="submit" value="Save" name="save" class="btn btn-primary" />
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

    <?php
    //require_once('includes/footerScriptsAddForms.php');
    ?>
<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->

<script src="assets/plugins/waitme/waitMe.js"></script>
<script>
    function run_waitMe(el, num, effect){
    switch (num) {
      case 1:
      maxSize = '';
      textPos = 'vertical';
      break;
      case 2:
      maxSize = 30;
      textPos = 'vertical';
      break;
      case 3:
      maxSize = 30;
      textPos = 'horizontal';
      break;
    }
    el.waitMe({
      effect: effect,
      text: 'Please wait...',
      bg: 'rgba(255,255,255,0.7)',
      color: '#000',
      maxSize: maxSize,
      source: 'img.svg',
      textPos: textPos,
      onClose: function() {}
    });
  }

    </script>

    <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
    <script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->

    <script src="/app/Admin/assets/js/bijles-template.js"></script> <!-- template JS -->
</body>

</html>