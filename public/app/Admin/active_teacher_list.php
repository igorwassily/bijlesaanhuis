<?php
$thisPage="active Teachers list";
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Active Teachers List</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	input:focus{	
background:transparent !important;
	}
	
	select:focus{	
background:transparent !important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	div.card>div.header{color:white;}

    .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
        background-color: transparent !important;
    }

    .bootstrap-select[disabled] button
    {
        color: gray !important;
        border: 1px solid gray !important;
    }

    .bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
        color: black;
    }

    .bootstrap-select.form-control:not([class*="col-"]){
        width: auto !important;
    }
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
	require_once('includes/connection.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="header">
                        <h3>Getoond op site lijst</h3>
                    </div> -->
					
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px; color: #486066" id="teachers_approve" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th class="color">Voornaam</th>
                                        <th class="color">Achternaam</th>
                                        <th class="color">E-mailadres</th>
                                        <th class="color">Telefoonnummer</th>
                                        <th class="color">Adres</th>
                                        <th class="color">Label</th>
                                        <th class="color">Label Date</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
											//$stmt = $con->prepare("SELECT cb.teacherID, c.firstname, c.lastname, u.email, c.telephone, c.place_name FROM `calendarbooking` cb, teacher t, contact c, user u WHERE isClassTaken=1 AND DATEDIFF(CURDATE(), datee)<=".MINIMUM_NO_OF_DAYS_TO_CONSIDER_TUTOR_ACTIVE." AND cb.teacherID=t.userID AND cb.teacherID=c.userID AND cb.userID=u.userID AND u.disabled<>1 GROUP BY cb.teacherID");
	$stmt = $con->prepare("SELECT c.userID, c.firstname, c.lastname, c.email, c.telephone, c.place_name, c.address, c.postalcode, c.city, t.label_docenten_1, t.date_label_set_1 FROM teacher t LEFT JOIN contact c ON c.userID=t.userID LEFT JOIN user u ON u.userID=t.userID WHERE u.disabled<>1 AND t.teacherlevel_rate_ID IS NOT NULL GROUP BY u.userID  ");


					$stmt->execute();
					$stmt->bind_result($id,$firstname,$lastname,$email,$phone,$place_name, $address, $postCode, $city, $status, $date);
					$stmt->store_result();
					while($stmt->fetch())
					{
					?>
									<tr>
									
										<?php echo "<td><a href='../teacher-detailed-view.php?tid=$id'>$firstname </a></td>"; ?>
										<td class="color"><?php echo $lastname; ?></td>
										<td class="color"><?php echo $email; ?></td>		
										<td class="color"><?php echo $phone; ?></td>
										<td class="color"><?php echo trim($address . ', ' . $postCode . ' ' . $city, ' ,'); ?></td>
										<td class="color">
                                            <select class="selectpicker form-control show-tick change-teacher-level" data-id="<?php echo $id; ?>" onchange="updateLabel($(this));">
                                                <option value="0" <?php echo ($status == "0") ? "selected" : "" ?>>Selecteer</option>
                                                <option value="1" <?php echo ($status == "1") ? "selected" : "" ?>>1</option>
                                                <option value="3" <?php echo ($status == "3") ? "selected" : "" ?>>3</option>
                                                <option value="3.1" <?php echo ($status == "3.1") ? "selected" : "" ?>>3.1</option>
                                                <option value="6" <?php echo ($status == "6") ? "selected" : "" ?>>6</option>
                                                <option value="7" <?php echo ($status == "7") ? "selected" : "" ?>>7</option>
                                            </select>
                                        </td>
										<td class="color"><?php echo $date; ?></td>
										<td><a  href="../admin_edit_tutor_profile?tid=<?php echo $id?>" >Profiel</a> &nbsp;&nbsp;&nbsp;&nbsp;
											<a  href="#" class="delete" data-id="<?php echo $id?>">Verwijder</a>
										</td>										
									</tr>
				<?php }	?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
	
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="assets/js/pages/ui/notifications.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
		
		
    $('#teachers_approve').DataTable({
        "language": {
            "aria": {
                "sortAscending":  ": Oplopend sorteren",
                "sortDescending": ": Aflopend sorteren"
            },
            "emptyTable":     "Geen gegevens beschikbaar in de tabel",
            "info": "Tonen van _START_ tot _END_ van _TOTAL_ inzendingen",
            "infoEmpty":      "Tonen van 0 tot 0 van 0 inzendingen",
            "infoFiltered":   "(Gefilterd van in totaal _MAX_ inzendingen)",
            "lengthMenu": "Toon _MENU_ inzendingen",
            "loadingRecords": "Laden...",
            "processing": "Verwerken...",
            "paginate": {
                "first":"Eerste",
                "last":"Laatste",
                "previous": "Vorige",
                "next": "Volgende"
            },
            "search":"Zoeken:",
            "zeroRecords": "Geen overeenkomende gegevens gevonden",
        }
    });
	});

    function updateLabel(e){
        var inp = e.find(':selected').val();
        var tID = e.attr("data-id");

        if(inp != " "){

            $.ajax({
                url: 'admin-ajaxcalls.php',
                data: {subpage:"change_label_docenten_1", inp:inp, tID: tID},
                type: "POST",
                //dataType: "json",
                success: function (result) {
                    showNotification("alert-success", "Student Status is Successfully updated..!", "bottom", "center", "", "");
                }
            });
        }
    }
		
	$(".delete").click(function(){
		var id = $(this).attr("data-id");

        if(confirm("Gebruiker verwijderen?")) { //s

            $.ajax({
                type: "POST",
                url: "admin-ajaxcalls.php",
                data: {subpage: "delete_teacher", id: id},
                success: function (response) {
                    if (response.trim() == "success") {

                        showNotification("alert-primary", "Successfully Deleted the tutor", "bottom", "center", "", "");
                        location.reload();
                    } else {
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if ((xhr.response).trim() == "success") {

                        showNotification("alert-primary", "Successfully Deleted the tutor", "bottom", "center", "", "");
                        location.reload();
                    } else {
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                }
            });// ends ajax
        }
        else{
            return 0;
        }
	});
	
</script>

</body>
</html>
<?php
}
?>