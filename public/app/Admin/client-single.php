<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>Client Single</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
	<style type="text/css">
		.material-icons
		{
			vertical-align: bottom;
		}
	</style>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
	require_once('includes/connection.php');
	if(isset($_POST['client-single-submit']))
	{
		$clientid = $_POST['client-id'];
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Client Single
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Client Single</li>
                </ul>                
            </div>
        </div>
    </div>
     <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                  <div class="body">
					   	   <div class="row clearfix">
                           
					
						<div class="col-md-12 col-lg-12">
								
                                 <div class="body table-responsive">
									 <p>Actions/Functions</p>
                        <table class="table table-striped">
                            <thead>
                                
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td class="text-center"><a href="#" class="btn btn-info btn-round edit-btn" >Add Booking</a></td>
                                   <td class="text-center"><a href="#" class="btn btn-info btn-round edit-btn" >Add Event</a></td>
								   <td class="text-center"><a href="#" class="btn btn-info btn-round edit-btn" >Add Transfer</a></td>
									<td class="text-center"><a href="#" class="btn btn-info btn-round edit-btn" >Add Hotel</a></td>
									
                                </tr>
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
					  </div>
					  
					   <div class="row clearfix">
							<div class="col-md-12 col-lg-12">
								
                                 <div class="body table-responsive">
									
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="12" class="bg-purple"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Booking Information</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>BC</td>
                                    <td>BIHC</td>
									<td>Arrival</td>
									<td>Departure</td>
									<td>Adults</td>
									<td>Children</td>
									<td>Enfants</td>
									<td>Agent</td>
									<td>Subagent</td>
									<td>rep</td>
									<td>Status</td>
									
                                </tr>
								<?php
								$stmt = $con->prepare("SELECT bookingID, arrival_date, departure_date, adults, children, enfants, agentID from booking where clientID = ?");
								$stmt->bind_param("i",$clientid);
								if($stmt->execute())
								{
									$stmt->bind_result($bookingID, $arrival, $departure, $adults, $children, $enfant, $agentID);
									$stmt->store_result();
									while($stmt->fetch())
									{
										?>
								<tr>
                                    
                                    <td><?php echo $bookingID; ?></td>
                                    <td>BIHC</td>
									<td><?php echo $arrival; ?></td>
									<td><?php echo $departure; ?></td>
									<td><?php echo $adults; ?></td>
									<td><?php echo $children; ?></td>
									<td><?php echo $enfant; ?></td>
									<td><?php echo $agentID; ?></td>
									<td>Subagent</td>
									<td>rep</td>
									<td>Status</td>
									
                                </tr>
										<?php
									}
								}
									
										?>
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						 
                            </div>
					   <div class="row clearfix">
                           
					
						<div class="col-md-12 col-lg-12">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="12" class="bg-cyan"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Hotel Information</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>BC</td>
                                    <td>BIHC</td>
									<td>Hotel</td>
									<td>Room</td>
									<td>Arrival</td>
									<td>Departure</td>
									<td>Adults</td>
									<td>Children</td>
									<td>Enfants</td>
									<td>rep</td>
									<td>Status</td>
									
                                </tr>
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						 
                            </div>
				
					   <div class="row clearfix">
                           
					
						<div class="col-md-12 col-lg-12">
								
                                 <div class="body table-responsive">
									
                        <table class="table table-striped">
                            <thead>
                                 <tr>
                                    
                                    <th colspan="12" class="bg-light-blue"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Transfer Information</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>BC</td>
                                    <td>BIHC</td>
									<td>Date</td>
									<td>PUtime</td>
									<td>PU Location</td>
									<td>DO Location</td>
									<td>Adults</td>
									<td>Children</td>
									<td>Enfants</td>
									<td>rep</td>
									<td>Status</td>
									
                                </tr>
								
								<?php
								$stmt = $con->prepare("select t.bookingID, t.arrival_date, dt.pickup_time, dt.pickup_location, dt.dropoff_location, t.transfer_adult, t.transfer_child, t.transfer_enfant from transferbooking as t, departuretransferbooking as dt where t.bookingID = dt.bookingID and t.clientID = ?");
								$stmt->bind_param("i",$clientid);
								if($stmt->execute())
								{
									$stmt->bind_result($bookingID, $arrival, $pickuptime, $pickuplocation, $dropofflocation, $tadult, $tchild, $tenfant);
									$stmt->store_result();
									while($stmt->fetch())
									{
										?>
								<tr>
                                    
                                    <td><?php echo $bookingID; ?></td>
                                    <td>BIHC</td>
									<td><?php echo $arrival; ?></td>
									<td><?php echo $pickuptime; ?></td>
									<td><?php echo $pickuplocation; ?></td>
									<td><?php echo $dropofflocation; ?></td>
									<td><?php echo $tadult; ?></td>
									<td><?php echo $tchild; ?></td>
									<td><?php echo $tenfant; ?></td>
						
									<td>rep</td>
									<td>Status</td>
									
                                </tr>
										<?php
									}
								}
									
										?>
						
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						 
                            </div>
				
					   <div class="row clearfix">
                           
					
						<div class="col-md-12 col-lg-12">
								
                                 <div class="body table-responsive">
									
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="12" class="g-bg-blush2"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Event Information</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                     <td>BC</td>
                                    <td>BIHC</td>
									<td>Date</td>
									<td>PUtime</td>
									<td>PU Location</td>
									<td>Events</td>
									<td>Adults</td>
									<td>Children</td>
									<td>Enfants</td>
									<td>rep</td>
									<td>Status</td>
									
                                </tr>
						<?php
								$stmt = $con->prepare("select bookingID, event_date, event_pickup_time, pickup_location, eventID, event_adult, event_child, event_enfant from eventbooking where clientID = ?");
								$stmt->bind_param("i",$clientid);
								if($stmt->execute())
								{
									$stmt->bind_result($ebookingID, $edate, $epickuptime, $elocation, $events, $eadult, $echild, $eenfant);
									$stmt->store_result();
									while($stmt->fetch())
									{
										?>
								<tr>
                                    
                                    <td><?php echo $ebookingID; ?></td>
                                    <td>BIHC</td>
									<td><?php echo $edate; ?></td>
									<td><?php echo $epickuptime; ?></td>
									<td><?php echo $elocation; ?></td>
									<td><?php echo $events; ?></td>
									<td><?php echo $eadult; ?></td>
									<td><?php echo $echild; ?></td>
									<td><?php echo $eenfant; ?></td>
						
									<td>rep</td>
									<td>Status</td>
									
                                </tr>
										<?php
									}
								}
									
										?>
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						 
                            </div>
					  
					 <div class="row clearfix">
                            <div class="col-md-4 col-lg-4">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                            <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="g-bg-blue"><i class="material-icons">person</i>

<span>Client Information</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
								<?php
								$stmt = $con->prepare("SELECT client_first_name, client_last_name, client_company, client_address, client_city, client_state, client_postalcode, client_country, client_phonenumber from client where clientID = ?");
								$stmt->bind_param("i",$clientid);
								if($stmt->execute())
								{
									$stmt->bind_result($firstname, $lastname, $company, $address, $city, $state, $postalcode, $country, $phone);
									$stmt->store_result();
									$stmt->fetch();
										?>
										  <tr>
                                    
                                    <td>First Name</td>
                                    <td><?php echo $firstname; ?></td>
                                </tr>
								<tr>
                                    
                                    <td>Last Name</td>
                                    <td><?php echo $lastname; ?></td>
                                </tr>
								<tr>
                                    
                                    <td>Company Name</td>
                                    <td><?php echo $company; ?></td>
                                </tr>
								<tr>
                                    
                                    <td>Address</td>
                                    <td><?php echo $address; ?></td>
                                </tr>
								<tr>
                                    
                                    <td>City</td>
                                    <td><?php echo $city; ?></td>
                                </tr>
						
								<tr>
                                    
                                    <td>State/Region</td>
                                    <td><?php echo $state; ?></td>
                                </tr>
								<tr>
                                    
                                    <td>Postcode</td>
                                    <td><?php echo $postalcode; ?>
							
									
									</td>
                                </tr>
								<tr>
                                    
                                    <td>Country</td>
                                    <td><?php echo $country; ?></td>
                                </tr>
								<tr>
                                    
                                    <td>Phone Number</td>
                                    <td><?php echo $phone; ?></td>
                                </tr>
								<?php
								}
		?>
                          
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						  <div class="col-md-4 col-lg-4">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="g-bg-cgreen"><i class="material-icons">contact_mail</i>&nbsp;&nbsp;<span>Contacts/Subs</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>First Name</td>
                                    <td>Dummy</td>
                                </tr>
								<tr>
                                    
                                    <td>Last Name</td>
                                    <td>Dummy</td>
                                </tr>
								<tr>
                                    
                                    <td>Telephone</td>
                                    <td>Dummy</td>
                                </tr>
								<tr>
                                    
                                    <td>Email</td>
                                    <td>Dummy</td>
                                </tr>
								<tr>
                                    
                                    <td>Address 1</td>
                                    <td>Dummy</td>
                                </tr>
								<tr>
                                    
                                    <td>City</td>
                                    <td>Dummy</td>
                                </tr>
						
								<tr>
                                    
                                    <td>State/Region</td>
                                    <td>dummy</td>
                                </tr>
								<tr>
                                    
                                    <td>Postcode</td>
                                    <td>dummy
									
									</td>
                                </tr>
								<tr>
                                    
                                    <td>Country</td>
                                    <td>Dummy</td>
                                </tr>
								
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
							<div class="col-md-4 col-lg-4">
								
                                 <div class="body table-responsive">
									
                        <table class="table table-striped">
                            <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="g-bg-blush2"><i class="material-icons">mail</i>&nbsp;&nbsp;<span>Recent Emails</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>11/09/2018 15:04
</td>
                                    <td>Booking confirmation</td>
                                </tr>
								<tr>
                                    
                                    <td>16/08/2018 14:39 
</td>
                                    <td>Pickup time change request</td>
                                </tr>
								<tr>
                                    
                                    <td>16/08/2018 14:38
</td>
                                    <td>Event Cancellation</td>
                                </tr>
								<tr>
                                    
                                    <td>16/08/2018 14:35 
</td>
                                    <td>Transfer Issues</td>
                                </tr>
								<tr>
                                    
                                    <td>07/07/2018 06:00 
</td>
                                    <td>Bad Rep</td>
                                </tr>
						
								<tr>
                                    
                                    <td>Collections</td>
                                    <td>0 (€ 0,000)</td>
                                </tr>
								
								
							
								
							
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
					
						 
                            </div>
					  <div class="row clearfix">
                           
						 <div class="col-md-4 col-lg-4">
								
                                 <div class="body table-responsive">
									
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="bg-purple"><i class="material-icons">info</i>&nbsp;&nbsp;<span>Information</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Status</td>
                                    <td>Invalid</td>
                                </tr>
								<tr>
                                    
                                    <td>Client Group</td>
                                    <td>None</td>
                                </tr>
								<tr>
                                    
                                    <td>Signup Date</td>
                                    <td>29-09-2018</td>
                                </tr>
								<tr>
                                    
                                    <td>Client For</td>
                                    <td>7 Months</td>
                                </tr>
								<tr>
                                    
                                    <td>Last Login</td>
                                    <td>Date: IP Address: Host:</td>
                                </tr>
						
								<tr>
                                    
                                    <td>Email Varified</td>
                                    <td>Yes</td>
                                </tr>
							
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						   <div class="col-md-4 col-lg-4">
								
                                 <div class="body table-responsive">
									
                        <table class="table table-striped">
                            <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="bg-blue"><i class="material-icons">person_add</i>&nbsp;&nbsp;<span>Client Addition</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
								<?php
								$stmt = $con->prepare("SELECT client_insurancenumber, client_emergencycontact, client_wheelchair from client where clientID = ?");
								$stmt->bind_param("i",$clientid);
								if($stmt->execute())
								{
									$stmt->bind_result($insurancenumber,$emergencycontact,$wheelchair);
									$stmt->store_result();
									$stmt->fetch();
										?>
                                <tr>
                                    
                                    <td>Insurance nr.</td>
                                    <td><?php echo $insurancenumber; ?></td>
                                </tr>
								<tr>
                                    
                                    <td>Emergency contact</td>
                                    <td><?php echo $emergencycontact; ?></td>
                                </tr>
								<tr>
                                    
                                    <td>require wheelchair</td>
                                    <td><?php if($wheelchair == "1")
										{
											echo "Yes";
										}
										else
										{
										echo "No"; 
										}
										?></td>
                                </tr>
								<tr>
                                    
                                    <td>Special needs:</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Addition 1</td>
                                    <td></td>
                                </tr>
						
								<tr>
                                    
                                    <td>Addition 2</td>
                                    <td></td>
                                </tr>
								<?php
								}
		?>
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						  	<div class="col-md-4 col-lg-4">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="bg-cyan"><i class="material-icons">receipt</i>&nbsp;&nbsp;<span>Client Payment</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Paid</td>
                                    <td>4 (€ 13.720,60)</td>
                                </tr>
								<tr>
                                    
                                    <td>Draft</td>
                                    <td>1 (€ 0,000)</td>
                                </tr>
								<tr>
                                    
                                    <td>Unpaid/Due</td>
                                    <td>0 (€ 0,000)</td>
                                </tr>
								<tr>
                                    
                                    <td>Cancelled</td>
                                    <td>0 (€ 0,000)</td>
                                </tr>
								<tr>
                                    
                                    <td>Refunded</td>
                                    <td>0 (€ 0,000)</td>
                                </tr>
						
								<tr>
                                    
                                    <td>Collections</td>
                                    <td>0 (€ 0,000)</td>
                                </tr>
							
								
							
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
					
						 
                            </div>
					  <div class="row clearfix">
							  
							  
						<div class="col-md-12 col-lg-12">
							<h5>Notes</h5>
						  </div>
					  </div>
						  <div class="row clearfix">
							  
							  
						<div class="col-md-3 col-lg-3">
							
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="g-bg-blue"><i class="material-icons">assignment</i>&nbsp;&nbsp;<span>Operation</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Display of remarks:</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Operation:</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Bookingnumber-Remark Account:</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Bookingnumber-Remark Events:</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>IHEventnumber-Remark Agent:</td>
                                    <td></td>
                                </tr>
						
								<tr>
                                    
                                    <td>Bookingnumber-Remark:</td>
                                    <td></td>
                                </tr>
						
							
							
								
								
							
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div><div class="col-md-3 col-lg-3">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="g-bg-cgreen"><i class="material-icons">assignment</i>&nbsp;&nbsp;<span>Accounting</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Notes 1</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 2</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 3</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 4</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 5</td>
                                    <td></td>
                                </tr>
						
								<tr>
                                    
                                    <td>Notes 6</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 7</td>
                                    <td></td>
                                </tr>
						
							
							
								
								
							
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div><div class="col-md-3 col-lg-3">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="g-bg-blush2"><i class="material-icons">assignment</i>&nbsp;&nbsp;<span>Events</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Notes 1</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 2</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 3</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 4</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 5</td>
                                    <td></td>
                                </tr>
						
								<tr>
                                    
                                    <td>Notes 6</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 7</td>
                                    <td></td>
                                </tr>
						
							
							
								
								
							
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
						<div class="col-md-3 col-lg-3">
								
                                 <div class="body table-responsive">
									 
                        <table class="table table-striped">
                           <thead>
                                 <tr>
                                    
                                    <th colspan="2" class="bg-purple"><i class="material-icons">assignment</i>&nbsp;&nbsp;<span>Agents</span></th>
                             
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Notes 1</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 2</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 3</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 4</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 5</td>
                                    <td></td>
                                </tr>
						
								<tr>
                                    
                                    <td>Notes 6</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>Notes 7</td>
                                    <td></td>
                                </tr>
						
							
							
								
								
							
                               
                            </tbody>
                        </table>
                    </div>
                                   
                                </div>
							  
						 
                            </div>
					   <!--tart Here -->
					 
				
				
                        </div>
				</div>
                </div>
            </div>
        
    </div>
</section>
<?php
	}
    require_once('includes/footerScripts.php');
?>
</body>
</html>
<?php
	
}
?>