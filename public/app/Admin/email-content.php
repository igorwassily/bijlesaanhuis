<?php

session_start();
if (!isset($_SESSION['AdminID'])) {
	header('Location: index.php');
	exit();
}

require_once('includes/connection.php');

$thisPage = "E-Mail content";
$activePage = basename($_SERVER['PHP_SELF']);
?>

<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>E-Mail content</title>
	<?php
	require_once('includes/mainCSSFiles.php');
	?>
    <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
          rel="stylesheet"/>
    <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
    <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
    <script src="assets/plugins/tinymce/tinymce.min.js"></script>

    <style type="text/css">
        .inline-elem-button {
            margin-top: 20px;
        }

        .input-group-addon {
            color: #ece6e6;
        }

        .text {
            color: black;
        }

        .margin-top {
            margin-top: 10px;
        }

        .checkbox {
            float: left;
            margin-right: 20px;
        }

        .margintop {
            margin-top: 20px;
            color: #5cc75f;
        }

        /*Placeholder Color */
        .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
            color: #486066;
        }

        .block-header {
            position: -webkit-sticky;
            position: sticky;
            top: 0;
            z-index: 100;
            background-color: inherit;
        }

        .theme-green .nav-tabs .nav-link.active {
            color: white !important;
            background-color: #5cc75f !important;
            border: 2px solid #5cc75f !important;
        }

        .nav-item {
            font-weight: 600;
        }

        .nav-tabs > .nav-item > .nav-link {
            color: #888;
            margin: 0;
            margin-right: 5px;
            background-color: transparent;
            border: 1px solid transparent;
            border-radius: 6px !important;
            font-size: 14px;
            padding: 11px 23px;
            line-height: 1.5;
        }

        .btn-primary {
            background: transparent !important;
            color: #5CC75F !important;
            border: 2px solid #5CC75F !important;
            border-radius: 22px !important;
            letter-spacing: 0;
            font-size: 14px;
            font-family: 'Poppins', Helvetica, Arial, Lucida, sans-serif !important;
            font-weight: 600 !important;
            text-transform: uppercase !important;
        }

        .btn-primary:hover, .theme-green .btn-primary:focus {
            color: #F1F1F1 !important;
            background-color: #5CC75F !important;
        }
    </style>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo">
        </div>
        <p>Please wait...</p>
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
require_once('includes/sidebarAdminDashboard.php');

$sql = "SELECT * FROM email_tpl WHERE module = ''";
$query = $con->query($sql);
$templates = $query->fetch_all(MYSQLI_ASSOC);

if (isset($_POST['save'])) {
	foreach ($templates as $key => $tpl) {
		if (!empty($_POST["html_{$tpl['name']}"]) || !empty($_POST["text_{$tpl['name']}"])) {
			$name = $tpl['name'];

			$html = $_POST["html_$name"];
			$text = $_POST["text_$name"];
			$subject = $_POST["subject_$name"];
			$header = $_POST["header_$name"];

			$text = strip_tags($text);
			$subject = strip_tags($subject);

			$templates[$key]['html'] = $html;
			$templates[$key]['text'] = $text;
			$templates[$key]['header'] = $header;
			$templates[$key]['subject'] = $subject;

			$html = $con->real_escape_string($html);
			$text = $con->real_escape_string($text);
			$header = $con->real_escape_string($header);
			$subject = $con->real_escape_string($subject);

			$con->query("UPDATE email_tpl SET `html`='$html', `text`='$text', `subject`='$subject', `header`='$header' WHERE `name`='$name'");
		}
	}
}
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0!important;">
    <div class="block-header">
		<?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="col-xl-12 col-lg-12 col-md-12">
            <div class="card demo-masked-input">
                <div class="header">
                    <h2><strong>E-Mail</strong> content <small> Define the content of every email </small>
                    </h2>
                </div>
                <div class="body">
                    <b>Variables</b><br/>
                    <div class="row">
                        <div class="col-md-6">
                            The templates support using variables. A variable has to parts wrapped with <i>#</i>, an
                            entity and a name. Three entities can be used: teacher, student and parent. The following
                            names exist:
                            <ul>
                                <li>id</li>
                                <li>forename</li>
                                <li>surname</li>
                                <li>name (forename + surname)</li>
                                <li>email</li>
                                <li>phone</li>
                                <li>skype (only supported for teacher)</li>
                                <li>img (only supported for teacher)</li>
                                <li>address</li>
                                <li>postal_code</li>
                                <li>city</li>
                                <li>country</li>
                                <li>place</li>
                            </ul>
                        </div>
                        <div class="col-md-6">
                            A special case is an appointment, the variables <i>date</i>, <i>begin</i> and <i>end</i> can
                            be used without an entity.<br/>
                            Examples:
                            <ul>
                                <li>#teacher_id#</li>
                                <li>#teacher_img#</li>
                                <li>#student_name#</li>
                                <li>#parent_email#</li>
                                <li>#date#</li>
                                <li>#begin#</li>
                                <li>#end#</li>
                            </ul>
                        </div>
                    </div>
                    <form method="post" style="width: 100%">
                        <label><b>Select an E-Mail</b>
                            <select id="selEmail" class="form-control show-tick" onchange="tab()">
								<?php
								foreach ($templates as $i => $tpl) {
									?>
                                    <option value="<?php echo $tpl['name']; ?>" <?php if ($i == 0) echo 'selected'; ?>>
										<?php echo $tpl['name']; ?>
                                    </option>
									<?php
								}
								?>
                            </select>
                        </label><br/>
                        <label><b>Enter the subject</b>
                            <input class="form-control" style="width: 30.5rem" type="text" id="email_subject"
                                   value="<?php echo $templates[0]['subject']; ?>">
                        </label><br/>
                        <label for="email_html"><b>Enter the HTML body</b></label>
                        <textarea id="email_html"
                                  data-active="<?php echo $templates[0]['name']; ?>"><?php echo htmlspecialchars($templates[0]['html']); ?></textarea><br/>
                        <label for="email_text"><b>Enter the text body</b></label>
                        <textarea id="email_text"
                                  style="width: 100%;height: 500px"><?php echo $templates[0]['text']; ?></textarea>
                        <label for="header_html"><b>Enter an additional HTML header</b></label>
                        <textarea id="header_html"
                                  data-active="<?php echo $templates[0]['name']; ?>"><?php echo htmlspecialchars($templates[0]['header']); ?></textarea><br/>
						<?php
						foreach ($templates as $tpl) {
							?>
                            <input type="hidden" id="html_<?php echo $tpl['name']; ?>"
                                   name="html_<?php echo $tpl['name']; ?>"
                                   value="<?php echo htmlspecialchars($tpl['html']); ?>">
                            <input type="hidden" id="header_<?php echo $tpl['name']; ?>"
                                   name="header_<?php echo $tpl['name']; ?>"
                                   value="<?php echo htmlspecialchars($tpl['header']); ?>">
                            <input type="hidden" id="text_<?php echo $tpl['name']; ?>"
                                   name="text_<?php echo $tpl['name']; ?>"
                                   value="<?php echo $tpl['text']; ?>">
                            <input type="hidden" id="subject_<?php echo $tpl['name']; ?>"
                                   name="subject_<?php echo $tpl['name']; ?>"
                                   value="<?php echo htmlspecialchars($tpl['subject']); ?>">
							<?php
						}
						?>
                        <input type="hidden" name="save">
                        <div class="col-lg-3 col-md-6">
                            <button type="button" onclick="store();this.form.submit()"
                                    class="inline-elem-button btn btn-raised btn-primary btn-round waves-effect"
                                    value="confirm" id="submitBtn">Save
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<?php
require_once('includes/footerScriptsAddForms.php');
?>

<script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script>
<!-- Bootstrap Colorpicker Js -->
<script src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js -->
<script src="assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js -->
<script src="assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js -->
<script src="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script><!-- Bootstrap Tags Input Plugin Js -->
<script src="assets/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js -->

<script src="assets/js/pages/forms/advanced-form-elements.js"></script>
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->

<script>
	$(document).ready(function () {
		tinymce.init(
			{
				selector: '#email_html',
				plugins: ['code', 'image', 'link'],
				menubar: 'edit format insert tools',
				toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
				height: 500
			}
		);
		tinymce.init(
			{
				selector: '#header_html',
				plugins: ['code', 'image', 'link'],
				menubar: 'edit format insert tools',
				toolbar: 'undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | code',
				height: 500
			}
		);

		$(".page-loader-wrapper").css("display", "none");

		$(document).ajaxStart(function () {
			$(document).css({'cursor': 'wait'});
		}).ajaxStop(function () {
			$(document.body).css({'cursor': 'default'});
		});

		$('.nav-link').on('click', function () {
			tab($(this).attr('data-id'));
		});
	});

	function tab() {
		store();

		let id = $('#selEmail').find(':selected').val();
		tinyMCE.get('email_html').setContent($('#html_' + id).val());
		tinyMCE.get('header_html').setContent($('#header_' + id).val());
		$('#email_subject').val($('#subject_' + id).val());
		$('#email_html').attr('data-active', id);
		$('#email_text').val($('#text_' + id).val());
	}

	function store() {
		let active = $('#email_html').attr('data-active');
		$('#html_' + active).val(tinyMCE.get('email_html').getContent());
		$('#header_' + active).val(tinyMCE.get('header_html').getContent());
		$('#text_' + active).val($('#email_text').val());
		$('#subject_' + active).val($('#email_subject').val());
	}
</script>
</body>
</html>
