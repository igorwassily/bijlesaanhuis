<?php
$thisPage="View teacherlevel";
require_once('includes/connection.php');
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{

		$enums_query = "SELECT * FROM `teacherlevel_rate` WHERE isdeleted=0";
		$e_result = mysqli_query($con,$enums_query);
		$rate_arr = "";
		$option = "<option value=''>Select Level</option>";
		while($row = mysqli_fetch_array($e_result)){
			$rate_arr .=  "$row[ID]:Array(\"$row[internal_rate]\", $row[rate], \"$row[level]\", \"$row[cost_per_km]\", \"$row[next_teacherlevelID]\"),";
		 	$option .= "<option value='$row[ID]'>$row[internal_level]</option>";
		}
		$rate_arr = rtrim($rate_arr,",");

	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>View Courses</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">

	/*Placeholder Color */
input{
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}

	select{
border: 1px solid #bdbdbd !important;

color: #486066; !important;
	}

	input:focus{
background:transparent !Important;
	}

	select:focus{
background:transparent !Important;
	}

	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}

	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}

	.btn.btn-simple{
    border-color: #486066 !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: #486066;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}

	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}

	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}

	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}

	table, th
	{
		text-align: center !important;
	}
	.hide{
		display:none;
	}
ul.dropdown-menu.inner {
		display: block;
	}

ul.dropdown-menu.inner li a span{color: black;}

button.btn.dropdown-toggle.btn-round.btn-simple {
    color: white;
}

</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="row">
                <div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="card" style="background: transparent !important;">
                        <div class="header">
                            <h2><strong>Add Teacher Level</strong></h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xl-6">
                                    <form name="tlevel" id="tlevel" action="techerlevel_processing.php">
                                        <div class="form-group form-float">
                                            <label for="booking-code">Internal Level Label *</label>
                                            <input type="text" class="form-control" placeholder="Internal Level Label"
                                                   name="lname" id="lname" requried>
                                        </div>
                                        <button type="button"
                                                class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block"
                                                id="btn1" name="btn1">Submit
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="header">
                            <h2><strong>Adjust Boundaries</strong></h2>
                        </div>
                        <div class="body">
                            <div class="row">
                                <div class="col-md-6 col-lg-6 col-xl-6">
	                                <?php
	                                $r = mysqli_query($con, "SELECT value FROM variables WHERE name='freeTravelRange'");
	                                if ($r) {
		                                $r = mysqli_fetch_row($r);
		                                if ($r) {
			                                $boundary1 = $r[0];
		                                }
	                                }
	                                ?>
                                    <form name="tlevel" id="tlevel" action="techerlevel_processing.php">
                                        <div class="form-group form-float">
                                            <label for="boundary1">Free of travel costs until</label>
                                            <input type="number" min="1" max="100" class="form-control"
                                                   placeholder="Teaches at home until" name="boundary1"
                                                   id="boundary1" value="<?php echo $boundary1; ?>" step="0.1"/>
                                        </div>
                                        <div class="form-group form-float">
                                            <label for="boundary2">Teaches at home until</label>
                                            <input type="text" class="form-control"
                                                   placeholder="Free of travel costs until" name="boundary2"
                                                   id="boundary2" value="Set individual by teachers" disabled/>
                                        </div>
                                        <button type="button"
                                                class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block"
                                                id="btnBoundaries" name="btnBoundaries">Submit
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

				 <div class="col-md-6 col-lg-6 col-xl-6">
				   <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Update Level Rate</strong></h2>

                    </div>
					   <div class="body">
						   <div class="row">

						   <div class="col-md-6 col-lg-6 col-xl-6">
							   <form name="tlevel" id="tlevel" action="">
								<div class="form-group">
									<label for="ttlevel">Select Teacher Level *</label>
									<select class="form-control"  name="ttlevel" id="ttlevel" requried>
										<?php echo $option; ?>
									</select>

                                </div>
							   	<div class="form-group form-float">
									<label for="booking-code">External Level Label *</label>
									<input type="text" class="form-control" placeholder="External Level label" name="Elevel" id="Elevel" requried>

                                </div>
								<div class="form-group form-float">
									<label for="booking-code">Internal Level Rate *</label>
									<input type="text" class="form-control" placeholder="Internal Level Rate" name="Irate" id="Irate" requried>

                                </div>
								   <div class="form-group form-float">
									<label for="booking-code">External Level Rate *</label>
									<input type="text" class="form-control" placeholder="Level Rate" name="Erate" id="Erate" requried>
                                </div>
								   <div class="form-group form-float">
									<label for="booking-code">Cost per KM *</label>
									<input type="text" class="form-control" placeholder="Cost" name="cpkm" id="cpkm" requried>
                                </div>
								   <div class="form-group">
									<label for="ntlevel">Next Teacher Level *</label>
									<select class="form-control"  name="ntlevel" id="ntlevel" requried>
										<?php echo $option; ?>
										<option value="0">None</option>
									</select>

                                </div>
								   <button type="button" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="btn" name="btn">Submit</button>

							   </form>
						   </div>

						   </div>


					   </div>
                </div>

            </div>
			</div>
            <div class="row">
				 <div class="col-md-12 col-lg-12 col-xl-12">
				   <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Manage School Level</strong></h2>

                    </div>
					   <div class="body">
						   <div class="row">
						   <div class="col-md-12 col-lg-12 col-xl-12">
						    <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="view-schoollevels" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th class="color">ID</th>
                                        <th class="color">Internal Level</th>
                                        <th class="color">External Level</th>
                                        <th class="color">Internal Rate</th>
                                        <th class="color">External Rate</th>
                                        <th class="color">Cost per KM</th>
                                        <th class="color">Next Teacher Level</th>
                                        <th class="color">Delete</th>

                                    </tr>
                                </thead>

                                <tbody>
									<?php
											$stmt = $con->prepare("SELECT `ID`, `internal_level`,`level`,`internal_rate`,`rate`, cost_per_km, (SELECT CONCAT( internal_level, '(', level, ')') FROM teacherlevel_rate str WHERE str.`ID`= tr.next_teacherlevelID)nextTeacherNLevel FROM `teacherlevel_rate` tr WHERE isdeleted=0");
					$stmt->execute();
					$stmt->bind_result($id, $inlevel, $level, $irate, $erate, $cpkm, $nextTeacherLevel);
					$stmt->store_result();
					while($stmt->fetch())
					{

									?>
									<tr>

									<td class="color"><?php echo $id; ?></td>
									<td class="color"><?php echo $inlevel; ?></td>
									<td class="color"><?php echo $level; ?></td>
									<td class="color"><?php echo $irate; ?></td>
									<td class="color"><?php echo $erate; ?></td>
									<td class="color"><?php echo $cpkm; ?></td>
									<td class="color"><?php echo $nextTeacherLevel; ?></td>

										<td>
									<b><a href="javascript:;" class="delete-record-level" data-id="<?php echo $id; ?>" style="color: #5cc75f !important;"> Delete </a></b>
										</td>
									</tr>
									<?php
					}
	?>
                                </tbody>
                            </table>
                        </div>
						   </div>
						   <div class="col-md-3 col-lg-3 col-xl-3">

								</div>

						   </div>


					   </div>
                </div>

            </div>
			</div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
<script>

	var rate = {<?php echo $rate_arr; ?>};

	var obj = null;


  $(document).ready(function() {


	  $('#view-teacherlevel').DataTable();

	  $("#ttlevel").change(function(){
	  	var level = $(this).val();
		  if(!level)
			  return;

		  //clearing old values
		  $("#Irate").val("");
		  $("#Erate").val("");
		  $("#Elevel").val("");
		  $("#cpkm").val("");
		  $("#ntlevel").val("");
		  $('#ntlevel').selectpicker('refresh');

		  //putting values

		  $("#Irate").val(rate[level][0]);
		  $("#Erate").val(String(rate[level][1]).replace(".", ","));
		  $("#Elevel").val(rate[level][2]);
		  $("#cpkm").val(rate[level][3]);
		  $("#ntlevel").val(rate[level][4]);
		  $('#ntlevel').selectpicker('refresh');
	  });

	  $("#btn1").click(function () {
		  let lname = $("#lname").val();
		  lname = lname.trim();

		  if (!lname)
			  return;

		  $.ajax({
			  url: 'techerlevel_processing.php',
			  type: 'POST',
			  data: {page: "insert_level", level: lname},
			  success: function (result) {
				  if (result === "success") {
					  showNotification("alert-success", "Successfully Inserted", "bottom", "center", "", "");
					  document.location.reload();
				  } else {
					  showNotification("alert-danger", "Fail to Insert a record. Try later", "bottom", "center", "", "");
				  }
			  }
		  });  //ajax ends

	  });

	  $("#btnBoundaries").click(function () {
		  let b = $("#boundary1").val();
		  b = parseFloat(b);

		  if (b <= 0) return;

		  $.ajax({
			  url: 'techerlevel_processing.php',
			  type: 'POST',
			  data: {page: "update_boundaries", boundary1: b},
			  success: function (result) {
				  if (result === "success") {
					  showNotification("alert-success", "Successfully Inserted", "bottom", "center", "", "");
				  } else {
					  showNotification("alert-danger", "Fail to Insert a record. Try later", "bottom", "center", "", "");
				  }
			  }
		  });  //ajax ends

	  });



	   $("#btn").click(function(){
	  	 var level=  ($("select").val()).trim();
	  	 var Elevel=  ($("#Elevel").val()).trim();
	  	 var Erate=  ($("#Erate").val()).trim();
	  	 var Irate=  ($("#Irate").val()).trim();
		 var ntlevel =  ($("#ntlevel").val()).trim();
		 var cpkm =  ($("#cpkm").val()).trim();


		 if(!level || !Erate || !Irate || !cpkm)
			 return;


		  $.ajax({
			  url: 'techerlevel_processing.php',
			  type: 'POST',
			  data: {page:"update_levelrate", id:level, Erate:Erate, Irate:Irate, Elevel:Elevel, ntlevel:ntlevel, cpkm:cpkm},
			  success: function (result) {
				  if(result === "success"){
						showNotification("alert-success", "Successfully Updated", "bottom", "center", "", "");
						//document.location.reload();
				  }else{
						showNotification("alert-danger", "Fail to Update a record. Try later", "bottom", "center", "", "");
				  }
			  }
		  });  //ajax ends

	  });

	  /* Delete Modal Scripts */
	  $( document ).on( "click", "a.delete-record-level", function(e) {
          var id = $(this).attr("data-id");

		  obj = $(this);

          $.ajax({
			  url: 'techerlevel_processing.php',
			  type: 'POST',
			  data: {page:"delete_tlevel", id:id},
			  success: function (result) {
				  console.log(result);
				  if(result === "success"){
					showNotification("alert-success", "Successfully Deleted.", "bottom", "center", "", "");
					obj.parent().parent().parent().remove();
				  }else{
					showNotification("alert-danger", "Failed to Delete. Try again later", "bottom", "center", "", "");
				  }
			  }
		});  //ajax ends
   });// on ends



  });

</script>
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
</body>
</html>
<?php
}
?>