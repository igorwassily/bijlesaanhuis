<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Add Bookings</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/dropzone/dropzone.css">
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	
</style>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Add Bookings
                <small>Add Bookings Information including Hotel Bookings, Travel Bookings & Transfer Bookings</small>
                </h2>
            </div>            
            <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                    
                        <form id="wizard_with_validation" method="POST" action="actions/add-bookings.php" enctype="multipart/form-data" class="dropzone">
                            <h3>Booking Information</h3>
                            <fieldset>
                                <div class="form-group form-float">
									<label for="booking-code">Enter Booking Code</label>
                                    <input type="text" class="form-control" placeholder="Booking Code" name="booking-code" id="booking-code">
                                </div>
								
								 <div class="form-group form-float">
									 <label for="branch">Choose Branch</label>
                                   <select class="form-control show-tick" id="branch-id" name="branch-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="client">Choose Client</label>
                                   <select class="form-control show-tick" id="client-id" name="client-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="agent">Choose Agent</label>
                                   <select class="form-control show-tick" id="agent-id" name="agent-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
									 <label for="arrival-date">Choose Arrival Date Time</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control datetimepicker" placeholder="Please choose date & time..." id="arrival-date" name="arrival-date">
                                </div>
								
								<div class="form-group form-float">
									 <label for="arrival-by">Choose Arrival By</label>
                                   <select class="form-control show-tick" id="arrival-by" name="arrival-by">
                                    <option value="">-- Please select --</option>
                                    <option value="bus">Bus</option>
                                    <option value="boat">Boat</option>
                                    <option value="aeroplane">Aeroplane</option>
                                    <option value="other">Other</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									<label for="arrival-flight-number">Enter Arrival Flight Number</label>
                                    <input type="text" class="form-control" placeholder="Flight Number" name="arrival-flight-number" id="arrival-flight-number">
                                </div>
								
								<label for="arrival-flight-time">Choose Arrival Flight Time</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control timepicker" placeholder="Please choose time..." name="arrival-flight-time" id="arrival-flight-time">
                                </div>
								
								<label for="departure-date">Choose Departure Date</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control datetimepicker" placeholder="Please choose date & time..." id="departure-date" name="departure-date">
                                </div>
								
								<div class="form-group form-float">
									 <label for="departure-by">Choose Departure By</label>
                                   <select class="form-control show-tick" id="departure-by" name="departure-by">
                                    <option value="">-- Please select --</option>
                                    <option value="bus">Bus</option>
                                    <option value="boat">Boat</option>
                                    <option value="aeroplane">Aeroplane</option>
                                    <option value="other">Other</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									<label for="departure-flight-number">Enter Departure Flight Number</label>
                                    <input type="text" class="form-control" placeholder="Flight Number" name="departure-flight-number" id="departure-flight-number">
                                </div>
								
								<label for="departure-flight-time">Choose Departure Flight Time</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control timepicker" placeholder="Please choose time..." name="departure-flight-time" id="departure-flight-time">
                                </div>
								
								<div class="form-group form-float">
									<label for="adults">Enter Adults No.</label>
                                    <input type="number" class="form-control" placeholder="Adults" name="adults" id="adults">
                                </div>
								
								<div class="form-group form-float">
									<label for="children">Enter Children No.</label>
                                    <input type="number" class="form-control" placeholder="Children" name="children" id="children">
                                </div>
                               <div class="form-group form-float">
									<label for="enfants">Enter Enfants No.</label>
                                    <input type="number" class="form-control" placeholder="Enfants" name="enfants" id="enfants">
                                </div>
								
								<div class="form-group form-float">
									<label for="booking files">Select Booking Files</label>
                                   <div class="dz-message">
                                <div class="drag-icon-cph"> <i class="material-icons">touch_app</i> </div>
                                <h3>Drop files here or click to upload.</h3>
                                </div>
                            <div class="fallback">
                                <input name="booking-files" type="booking-files" multiple />
                            </div>
                                </div>
                               
                            </fieldset>
                            <h3>Hotel Booking Information</h3>
                            <fieldset>
                                
								
								 <div class="form-group form-float">
									 <label for="branch">Choose Branch</label>
                                   <select class="form-control show-tick" id="hotel-branch-id" name="hotel-branch-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								
								<div class="form-group form-float">
									 <label for="agent">Choose Agent</label>
                                   <select class="form-control show-tick" id="hotel-agent-id" name="hotel-agent-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								<div class="form-group form-float">
									 <label for="location">Choose Location</label>
                                   <select class="form-control show-tick" id="location-id" name="location-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								<label for="departure-date">Choose Departure Date</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control datetimepicker" placeholder="Please choose date & time..." id="hotel-departure-date" name="hotel-departure-date">
                                </div>
								
								
								<div class="form-group form-float">
									 <label for="location">Choose Booking Room</label>
                                   <select class="form-control show-tick" id="booking-room" name="booking-room" multiple="multiple">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
                               
                            </fieldset>
                           
							
							<h3>Transfer Booking Information</h3>
                            <fieldset>
                               <div class="form-group form-float">
									 <label for="branch">Choose Branch</label>
                                   <select class="form-control show-tick" id="transfer-branch-id" name="transfer-branch-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								<div class="form-group form-float">
									 <label for="agent">Choose Client</label>
                                   <select class="form-control show-tick" id="transfer-client-id" name="transfer-client-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="agent">Choose Agent</label>
                                   <select class="form-control show-tick" id="transfer-agent-id" name="transfer-agent-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								<label for="pick-date">Choose Pickup Date</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control datetimepicker" placeholder="Please choose date & time..." id="pickup-date" name="pickup-date">
                                </div>
								
								<label for="pickup-location">Choose Pickup Location</label>
                                   <div class="input-group">
                                    <select class="form-control show-tick" data-live-search="true" name="pickup-location" id="pickup-location">
                                    <option value="loc1">Loc1</option>
                                    <option value="loc2">Loc2</option>
                                    <option value="loc3">Loc3</option>
                                </select>
                                    
                                </div>
								
								<label for="pickup-time">Choose Pickup Time</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control timepicker" placeholder="Please choose time..." name="pickup-time" id="pickup-time">
                                </div>
								
								<label for="dropoff-location">Choose Drop Off Location</label>
                                   <div class="input-group">
                                    <select class="form-control show-tick" data-live-search="true" name="dropoff-location" id="dropoff-location">
                                    <option value="loc1">Loc1</option>
                                    <option value="loc2">Loc2</option>
                                    <option value="loc3">Loc3</option>
                                </select>
                                    
                                </div>
								
								<div class="form-group form-float">
									 <label for="transfer-vehicle">Choose Transfer Vehicle</label>
                                   <select class="form-control show-tick" id="transfer-vehicle" name="transfer-vehicle">
                                    <option value="">-- Please select --</option>
                                    <option value="car">Car</option>
                                    <option value="van">Van</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="transfer-type">Choose Transfer Type</label>
                                   <select class="form-control show-tick" id="transfer-vehicle" name="transfer-vehicle">
                                    <option value="">-- Please select --</option>
                                    <option value="joined">Joined</option>
                                    <option value="private">Private</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="transfer-driver">Choose Transfer Driver</label>
                                   <select class="form-control show-tick" id="transfer-driver" name="transfer-driver">
                                    <option value="">-- Please select --</option>
                                    <option value="driver01">Driver01</option>
                                    <option value="driver02">Driver02</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="transfer-driver">Do you need a Transfer Guide?</label>
                                   <select class="form-control show-tick" id="transfer-guide-need" name="transfer-guide-need">
                                    <option value="">-- Please select --</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="transfer-guide">Transfer Guide</label>
                                   <select class="form-control show-tick" id="transfer-guide" name="transfer-guide">
                                    <option value="">-- Please select --</option>
                                    <option value="guide01">Guide01</option>
                                    <option value="guide02">Guide02</option>
                                </select>
                                </div>
								
								
								<div class="form-group form-float">
									 <label for="transfer-tarrif">Transfer Tarrif</label>
                                   <select class="form-control show-tick" id="transfer-tarrif" name="transfer-tarrif">
                                    <option value="">-- Please select --</option>
                                    <option value="tarrif01">Tarrif01</option>
                                    <option value="tarrif02">Tarrif02</option>
                                </select>
                                </div>
                               
                            </fieldset>
							
							<h3>Event Booking Information</h3>
                            <fieldset>
                               <div class="form-group form-float">
									 <label for="branch">Choose Branch</label>
                                   <select class="form-control show-tick" id="event-branch-id" name="event-branch-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								<div class="form-group form-float">
									 <label for="agent">Choose Client</label>
                                   <select class="form-control show-tick" id="event-client-id" name="event-client-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="agent">Choose Event Category</label>
                                   <select class="form-control show-tick" id="event-category" name="event-category">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="agent">Choose Event</label>
                                   <select class="form-control show-tick" id="event" name="event">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="agent">Choose Supplier</label>
                                   <select class="form-control show-tick" id="supplier-id" name="supplier-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								<div class="form-group form-float">
									 <label for="agent">Choose Agent</label>
                                   <select class="form-control show-tick" id="transfer-agent-id" name="transfer-agent-id">
                                    <option value="">-- Please select --</option>
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="30">30</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                </div>
								
								
								
								<label for="pick-date">Choose Pickup Date</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control datetimepicker" placeholder="Please choose date & time..." id="event-pickup-date" name="event-pickup-date">
                                </div>
								
								<label for="pickup-location">Choose Pickup Location</label>
                                   <div class="input-group">
                                    <select class="form-control show-tick" data-live-search="true" name="event-pickup-location" id="event-pickup-location">
                                    <option value="loc1">Loc1</option>
                                    <option value="loc2">Loc2</option>
                                    <option value="loc3">Loc3</option>
                                </select>
                                    
                                </div>
								
								<label for="pickup-time">Choose Pickup Time</label>
                                   <div class="input-group">
                                    
                                    <input type="text" class="form-control timepicker" placeholder="Please choose time..." name="event-pickup-time" id="event-pickup-time">
                                </div>
								

								
								<div class="form-group form-float">
									 <label for="transfer-driver">Event Entree Fee?</label>
                                   <select class="form-control show-tick" id="event-entree-fee" name="event-entree-fee">
                                    <option value="">-- Please select --</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                </select>
                                </div>
								
								
								
								
								
                               
                            </fieldset>
                        </form>
                   
                
            </div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
 
<script src="assets/plugins/jquery-validation/jquery.validate.js"></script> <!-- Jquery Validation Plugin Css -->
<script src="assets/plugins/jquery-steps/jquery.steps.js"></script> <!-- JQuery Steps Plugin Js -->
<script src="assets/plugins/momentjs/moment.js"></script>
<script src="assets/js/pages/forms/form-wizard.js"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<script src="assets/js/pages/forms/basic-form-elements.js"></script> 
<script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script> 
<script src="assets/plugins/dropzone/dropzone.js"></script>
<script src="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
	
</body>
</html>
<?php
}
?>