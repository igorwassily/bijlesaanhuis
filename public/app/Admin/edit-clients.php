<?php
$thisPage="Client Edit";
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Edit Client Information</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');date('Y-m-d H:i:s');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}
	label
	{
		color: white;
	}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Edit Clients
                
                </h2>
            </div>            
            <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="dashboard.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
			<div class="col-lg-3 col-md-3 col-sm-12"></div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                
                    	<?php
					
	if(isset($_POST['client-edit-submit']))
	{
		$clientID = test_input($_POST['clientedit-id']);
										$stmt = $con->prepare("select clientID, client_first_name, client_last_name, client_company, client_address, client_city, client_state, client_postalcode, client_country, client_language, client_phonenumber, client_email, client_status, client_insurancenumber, client_insurancecompany, client_emergencycontact, client_wheelchair from client where clientID = ?");
		$stmt->bind_param("i",$clientID);
										if($stmt->execute())
										{
											
											$stmt->bind_result($clientid,$cfname,$clname,$ccompany,$caddress,$ccity,$cstate,$cpostalcode,$ccountry,$clanguage,$cphonenumber,$cemail,$cstatus,$cinsurancenumber,$cinsurancecompany,$cemergencycontact,$cwheelchair);
											$stmt->store_result();
											$stmt->fetch();
										}
	
	?>
                        <form id="frmEditClient" enctype="multipart/form-data">
							<br/><br/><br/><br/><br/>
							<div class="form-group form-float">
									<label for="booking-code">Enter Client First Name</label>
									<input type="hidden" class="form-control" placeholder="Booking Code" name="choice" id="choice" value="editClient">
								<input type="hidden" class="form-control" placeholder="Booking Code" name="client-id" id="client-id" value="<?php echo $clientid; ?>">
                                    <input type="text" class="form-control" placeholder="Client First Name" name="client-first-name" id="client-first-name" value="<?php echo $cfname; ?>">
                                </div>
								<div class="form-group form-float">
									<label for="booking-code">Enter Client Last Name</label>
                                    <input type="text" class="form-control" placeholder="Client Last Name" name="client-last-name" id="client-last-name" value="<?php echo $clname; ?>">
                                </div>
								
								<div class="form-group form-float">
									<label for="booking-code">Enter Client Company</label>
                                    <input type="text" class="form-control" placeholder="Client Company" name="client-company" id="client-company" value="<?php echo $ccompany; ?>">
                                </div>
								<div class="form-group form-float">
									<label for="booking-code">Enter Client Address</label>
                                    <input type="text" class="form-control" placeholder="Client Address" name="client-address" id="client-address" value="<?php echo $caddress; ?>">
                                </div>
								
								<div class="form-group form-float">
									<label for="booking-code">Enter Client City</label>
                                    <input type="text" class="form-control" placeholder="Client City" name="client-city" id="client-city" value="<?php echo $ccity; ?>">
                                </div>
								<div class="form-group form-float">
									<label for="booking-code">Enter Client State</label>
                                    <input type="text" class="form-control" placeholder="Client State" name="client-state" id="client-state" value="<?php echo $cstate; ?>">
                                </div>
								<div class="form-group form-float">
									<label for="booking-code">Enter Client Postal Code</label>
                                    <input type="text" class="form-control" placeholder="Client Postal Code" name="client-postal-code" id="client-postal-code" value="<?php echo $cpostalcode; ?>">
				</div>
								 
								
								
								<label for="dropoff-location">Choose Client Language</label>
                                   <div class="input-group">
                                    <select class="form-control show-tick" data-live-search="true" name="client-language" id="client-language">
										
                                    <option value="lang1">Lang1</option>
                                    <option value="lang2">Lang2</option>
                                    <option value="lang3">Lang3</option>
                                </select>
                                    
                                </div>
								
									<div class="form-group form-float">
									<label for="booking-code">Enter Client Telephone</label>
                                    <input type="text" class="form-control" placeholder="Client Phone Number" name="client-phone" id="client-phone" value="<?php echo $cphonenumber; ?>">
                                </div>
								<div class="form-group form-float">
									<label for="booking-code">Enter Client Email</label>
                                    <input type="email" class="form-control" placeholder="Client Email" name="client-email" id="client-email" value="<?php echo $cemail; ?>">
                                </div>	
									<label for="dropoff-location">Choose Client Country</label>
                                   <div class="input-group">
                                    <select class="form-control show-tick" data-live-search="true" name="client-country" id="client-country" >
                                    <option value="cou1">Cou1</option>
                                    <option value="cou2">Cou2</option>
                                    <option value="cou3">Cou3</option>
                                </select>
                                    
                                </div>
								
								<div class="form-group form-float">
									 <label for="branch">Choose Client Status</label>
                                   <select class="form-control show-tick" id="client-status" name="client-status">
                                    <option value="">-- Please select --</option>
                                    <option value="Active">Active</option>
                                    <option value="Inactive">Inactive</option>
                                    
                                </select>
                                </div>
								
								<div class="form-group form-float">
									<label for="booking-code">Enter Client Insurance Number</label>
                                    <input type="text" class="form-control" placeholder="Client Insurance Number" name="client-insurance-number" id="client-insurance-number" value="<?php echo $cinsurancenumber; ?>">
                                </div>
								<div class="form-group form-float">
									<label for="booking-code">Enter Client Insurance Company</label>
                                    <input type="text" class="form-control" placeholder="Client Insurance Company" name="client-insurance-company" id="client-insurance-company" value="<?php echo $cinsurancecompany; ?>">
                                </div>
								
								<div class="form-group form-float">
									<label for="booking-code">Enter Client Emergency Contact</label>
                                    <input type="text" class="form-control" placeholder="Client Emergency Contact" name="client-emergency-contact" id="client-emergency-contact" value="<?php echo $cemergencycontact; ?>">
                                </div>
								
								<div class="form-group form-float">
									 <label for="branch">Client Wheel Chair Required?</label>
                                   <select class="form-control show-tick" id="client-wheelchair" name="client-wheelchair">
                                    <option value="">-- Please select --</option>
                                    <option value="1">Yes</option>
                                    <option value="0">No</option>
                                
                                </select>
                                </div>
							<button type="submit" name="client-edit-submit" class="btn btn-warning btn-round text-center">UPDATE</button>
							
                        </form>
                   <?php
	}
	?>
                
            </div>
			<div class="col-lg-3 col-md-3 col-sm-12"></div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
	
	<!-- Jquery steps Wizard Code -->
	<script type="text/javascript">
	
		$(document).ready(function(){ 
		//Update Project Manager
 function manualValidateUpdate(ev) {
     if(ev.target.checkValidity()==true)
     {
     ev.preventDefault();
     
    run_waitMe($('body'), 1, "timer");
     //$("#modal-projectmanager").modal('hide');
     /*$.loader({
            className:"blue-with-image",
            content:''
          });*/
     
                      $.ajax({
                            url: 'actions/edit_scripts.php',
                            type: 'POST',
                           data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {


                      //     alert(result);
                                    switch(result)
                                    {

                                      case "Error":
                                    
                                        
                                        setTimeout(function() {   
                                                 
                                               $('body').waitMe('hide'); 
										   
                                               $('#employeeedit-form')[0].empty(); 
                                               $("#eresult").html(
        '<div id="error-eem" class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'There Was An Error Updating The Record.' + 
         '</div>');
                                               //$("#error-epm").html("There Was An Error Inserting The Record."); 
                                                  
}, 500); 
                                        break;

                                        case "Success":
                                    
                                        
                                        setTimeout(function() { 
                                                    $('body').waitMe('hide');
                                                    
											 window.location.replace("https://dev.we-think.nl/client-overview");
}, 500); 

                                        break;

                                    
                                    }
                              }
                      });
      }
 }$("#frmEditClient").bind("submit", manualValidateUpdate);
			
		});
	
	</script>
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
</body>
</html>
<?php
}
?>