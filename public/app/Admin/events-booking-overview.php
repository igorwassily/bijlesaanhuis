<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Events Booking Overview</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
<style type="text/css">
    
/*.table-bordered {

    border: 1px solid #dee2e6;

}

.table tr th {

    vertical-align: middle;
    border-top: 1px solid #dee2e6;

}

.table thead th {

    border-bottom: 2px solid #dee2e6;

}

.table tr td, .table tr th {

    vertical-align: middle;
    border-top: 1px solid #dee2e6;

}

.table-bordered td, .table-bordered th {

    border: 1px solid #dee2e6;
}*/

td.details-control {
    background: url('img/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('img/details_close.png') no-repeat center center;
}

	#bookings_overview td {
    padding: 8px;
}
	
.edit-btn
	{
		font-size: 15px;
font-weight: bolder;
padding: 8px 20px 8px 20px;
	}
	
	
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
</style>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Events Booking Overview
               
                </h2>
            </div>            
            <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Events Booking Overview</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">

                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable text-right dt-responsive" style="font-size: 13px;" id="Events_bookings_overview" cellspacing="0" width="100%">
                                <thead class="text-right">
                                    <tr>
										<th></th>
                                        <th>Date</th>
                                        <th>Event</th>
                                        <th>Supplier</th>
                                        <th>Client</th>
                                        <th>Hotel</th>
                                        <th>Roomnr.</th>
                                        <th>PU Time</th>
										<th>Phone</th>
										<th>BC</th>
										<th>EC</th>
										<th>Status</th>
										<th>Detail</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
                                    <tr>
										<td  class="details-control"></td>
                                        <td>10/17/2018</td>
                                        <td>Snorkeling Tour 2</td>
                                        <td>Trans Island Travel</td>
                                        <td>John, Doe</td>
                                        <td>Little Austria</td>
                                        <td>1</td>
										<td>9:45</td>
                                        <td>615155552</td>
										 <td>TIT100</td>
										 <td>EVE124</td>
										 <td>Confirmed</td>
										<td><a href="events-booking-single" class="btn btn-warning btn-round edit-btn"  target="_blank" >VIEW</a></td>
				
                                    </tr>
									 
									
									  
                                    
                                    
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
<script type="text/javascript">
function format ( d ) {
    return '<table class="table-bordered table-striped table-hover dataTable text-right dt-responsive" style="font-size: 13px;" id="bookings_overview" cellspacing="0" width="100%" style="margin-top: -1% !important;border-collapse: separate !important;margin-left: -1%;margin-bottom: -1% !important;text-align: left !important;font-size: 10px;padding: 0% !important;">'+
        '<tr>'+
            '<td>Arrival Flight</td>'+
            '<td>'+'PG999'+'</td>'+
            '<td>Agent Code</td>'+
            '<td>'+'TUI9935555'+'</td>'+
            '<td>Hotels</td>'+
            '<td>'+'4'+'</td>'+
            '<td>Client Language</td>'+
            '<td>'+'Swahili'+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Arrival Time</td>'+
            '<td>'+'10:15'+'</td>'+
            '<td>SubAgent Code</td>'+
            '<td>'+'TUIDEU765'+'</td>'+
            '<td>Excursions</td>'+
            '<td>'+'3'+'</td>'+
            '<td>Client Phonenumber</td>'+
            '<td>'+'+999 555 000'+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Departure Flight</td>'+
            '<td>PG555</td>'+
            '<td>Representative</td>'+
            '<td>'+'Andreas Holzhofer'+'</td>'+
            '<td>Transfers</td>'+
            '<td>'+'8'+'</td>'+
            '<td>Client Email</td>'+
            '<td>'+'pancake@nowplease.com'+'</td>'+
        '</tr>'+
        '<tr>'+
            '<td>Departure Time</td>'+
            '<td>16:59</td>'+
        '</tr>'+
    '</table>';
}
    
    $(document).ready(function() {
    var table = $('#bookings_overview').DataTable( {
        "order": [[1, 'asc']],
        columnDefs: [
            { width: 5, targets: 0 },
            { width: 30, targets: 1 },
            { width: 50, targets: 2 },
            { width: 385, targets: 3 },
        ]
    } );
     
  
    $('#bookings_overview tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );
</script>
</body>
</html>
<!-- Default Size -->
<div class="modal fade" id="bookingModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background: #373f46;">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel" style="color: #bdbdbd;">Update Booking Information</h4>
            </div>
            <div class="modal-body"> <div class="card">
              
                    <div class="body">
                        <form>
                            <label for="booking-code">Booking Code</label>
                            <div class="form-group">                                
                                <input type="text" id="booking_code" class="form-control" placeholder="Enter your booking code" value="55542">
                            </div>
                            <label for="ih-booking-code">IH Booking Code</label>
                            <div class="form-group">                                
                                <input type="text" id="ih_booking_code" class="form-control" placeholder="Enter your ih booking code" value="TIT0213">
                            </div>
                            <label for="client">Client Name</label>
                            <div class="form-group">                                
                                <input type="text" id="client" class="form-control" placeholder="Enter client name" value="Wuest Markus, Mr.">
                            </div>
							<label for="arrival">Arrival</label>
                            <div class="form-group">                                
                                <input type="text" id="arrival" class="form-control datetimepicker" placeholder="Choose Arrival Date" value="15/01/2012">
                            </div>
                            <button type="button" class="btn btn-info waves-effect">UPDATE</button>
							<button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                        </form>
                    </div>
                </div> </div>
            <div class="modal-footer">
                
                
            </div>
        </div>
    </div>
</div>
<?php
}
?>