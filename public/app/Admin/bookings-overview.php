<?php
$thisPage="Bookings Overview";
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Bookings Overview</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
<style type="text/css">

td.details-control {
    background: url('img/details_open.png') no-repeat center center;
    cursor: pointer;
}
tr.shown td.details-control {
    background: url('img/details_close.png') no-repeat center center;
}

	#bookings_overview td {
    padding: 8px;
}
	
.edit-btn
	{
		font-size: 15px;
font-weight: bolder;
padding: 8px 20px 8px 20px;
	}
	
	
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	div.dataTables_wrapper div.dataTables_length select
	{
		color: white !important;
		background-color: transparent !important;
		border: 1px solid white !important;
	}
	div.dataTables_wrapper div.dataTables_length select option[selected="selected"]
	{
		color: black !important;
	}
	
	div.dataTables_wrapper div.dataTables_length select:hover
	{
		color: white !important;
		background-color: transparent !important;
	}
	div.dataTables_wrapper div.dataTables_length select:active
	{
		color: black !important;
	}
</style>
<?php
	$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Booking Overview
                <small>Select a booking to extend the information, to edit a booking click on either the bookingcode or expand and click view</small>
                </h2>
            </div>            
            <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.html"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Dashboard</li>
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">

                <div class="card">
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="bookings_overview" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
										<th></th>
										<th>Booking ID</th>
                                        <th>Agent</th>
                                        <th>Agentcode</th>
                                        <th>Name</th>
                                        <th>Arrival</th>
                                        <th>Departure</th>
                                        <th>Pax</th>
                                        <th>Hotel</th>
										<th>OP Notes</th>
										<th>Arrival Flight Time</th>
                                        <th>Departure Flight Time</th>
										<th>PU Times</th>
										<th>Hotels</th>
										<th>AgentCode</th>
										<th>Event Supplier</th>
										<th>Event Date</th>
										<th>Booked By</th>
										<th class="all">Detail</th>
										<th class="all">Edit</th>
										
                                    </tr>
                                </thead>
                                
                                <tbody>
                                  
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
<script type="text/javascript">
	var arrival_flight_time="", departure_flight_time = "", agent_id = "", supplier_id = "";
    $(document).ready(function() {
    var table = $('#bookings_overview').DataTable( {
		"processing": true,
        "serverSide": true,
        "destroy" : true,
        "pageLength": 20,
        "lengthMenu": [ [20, 50, 100], [20, 50, 100]  ],
        'ajax': 'includes/bookingsOverviewProcessing.php',
        "order": [[1, 'asc']],
        columnDefs: [
            { width: 5, targets: 0 },
            { width: 30, targets: 2 },
            { width: 50, targets: 3 },
            { width: 385, targets: 4 },
			{
                "targets": [ 1 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 10 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 11 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 12 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 13 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 14 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 15 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 16 ],
                "visible": false,
                "searchable": false
            },
			{
                "targets": [ 17 ],
                "visible": false,
                "searchable": false
            },
			{
    "targets": 0,
    "data" : null,
	"className" : 'details-control',
				"render": function ( data, type, full, meta ) {
		return '';
       }
  },
			{
    "targets": 3,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 4,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 7,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 8,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 9,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 10,
    "data": 10,
    "render": function ( data, type, full, meta ) {
		arrival_flight_time = data;
		return 'N/A in DB';
       }
  },
			{
    "targets": 11,
    "data": 11,
    "render": function ( data, type, full, meta ) {
		departure_flight_time = data;
		return 'N/A in DB';
       }
  },
			
			{
    "targets": 12,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 13,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 14,
    "data": 14,
    "render": function ( data, type, full, meta ) {
		agent_id = data;
		return 'N/A in DB';
       }
  },
			{
    "targets": 15,
    "data": 15,
    "render": function ( data, type, full, meta ) {
		supplier_id = data;
		return 'N/A in DB';
       }
  },
			{
    "targets": 16,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 17,
    "data": null,
    "render": function ( data, type, full, meta ) {
		return 'N/A in DB';
       }
  },
			{
    "targets": 18,
    "data" : 1,
				"render": function ( data, type, full, meta ) {
		return '<form method="post" action = "booking-single" target="_blank"><input type="hidden" name="booking-id" id="booking-id" value="'+data+'"/><button type="submit" name="booking-single-submit" class="btn btn-warning btn-round">VIEW</button></form>';
       }
  },
			{
    "targets": 19,
    "data" : 1,
				"render": function ( data, type, full, meta ) {
		return '<form method="post" action = "edit-bookings" target="_blank"><input type="hidden" name="bookingedit-id" id="bookingedit-id" value="'+data+'"/><button type="submit" name="booking-edit-submit" class="btn btn-warning btn-round">EDIT</button></form>';
       }
  }
        ]
    } );
     
  
    $('#bookings_overview tbody').on('click', 'td.details-control', function () {
        var tr = $(this).closest('tr');
        var row = table.row( tr );
 
        if ( row.child.isShown() ) {
            // This row is already open - close it
            row.child.hide();
            tr.removeClass('shown');
        }
        else {
            // Open this row
            row.child( format(row.data()) ).show();
            tr.addClass('shown');
        }
    } );
} );
	function format ( d ) {
    return '<table class="table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="bookings_overview" cellspacing="0" width="100%" style="margin-top: -1% !important;border-collapse: separate !important;margin-left: -1%;margin-bottom: -1% !important;text-align: left !important;font-size: 10px;padding: 0% !important;">'+
       
     
        '<tr>'+
            '<td>Arr. Flight</td>'+
            '<td>'+arrival_flight_time+'</td>'+
			'<td>Dep. Flight</td>'+
            '<td>'+departure_flight_time+'</td>'+
			'<td>P/U Times</td>'+
		'<td>N/A in DB</td>'+
			'<td>Hotels</td>'+
            '<td>N/A in DB</td>'+
        '</tr>'+
		 '<tr>'+
            '<td>Agent Code</td>'+
            '<td>'+agent_id+'</td>'+
            '<td>Event</td>'+
            '<td>'+supplier_id+'</td>'+
            '<td>Event Date</td>'+
			'<td>N/A in DB</td>'+
            '<td>booked by</td>'+
			'<td>N/A in DB</td>'+
        '</tr>'+
    '</table>';
}
</script>
</body>
</html>
<!-- Default Size -->
<div class="modal fade" id="bookingModal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content" style="background: #373f46;">
            <div class="modal-header">
                <h4 class="title" id="defaultModalLabel" style="color: #bdbdbd;">Update Booking Information</h4>
            </div>
            <div class="modal-body"> <div class="card">
              
                    <div class="body">
                        <form>
                            <label for="booking-code">Booking Code</label>
                            <div class="form-group">                                
                                <input type="text" id="booking_code" class="form-control" placeholder="Enter your booking code" value="55542">
                            </div>
                            <label for="ih-booking-code">IH Booking Code</label>
                            <div class="form-group">                                
                                <input type="text" id="ih_booking_code" class="form-control" placeholder="Enter your ih booking code" value="TIT0213">
                            </div>
                            <label for="client">Client Name</label>
                            <div class="form-group">                                
                                <input type="text" id="client" class="form-control" placeholder="Enter client name" value="Wuest Markus, Mr.">
                            </div>
							<label for="arrival">Arrival</label>
                            <div class="form-group">                                
                                <input type="text" id="arrival" class="form-control datetimepicker" placeholder="Choose Arrival Date" value="15/01/2012">
                            </div>
                            <button type="button" class="btn btn-info waves-effect">UPDATE</button>
							<button type="button" class="btn btn-danger waves-effect" data-dismiss="modal">CLOSE</button>
                        </form>
                    </div>
                </div> </div>
            <div class="modal-footer">
                
                
            </div>
        </div>
    </div>
</div>
<?php
}
?>