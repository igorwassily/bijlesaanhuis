<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>Events Overview</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
	<style type="text/css">
		.form-control.form-control-sm:placeholder
		{
			color: white !important;
		}
	</style>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Events Overview
              
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Events Overview</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
					
					  <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" id="client-overview" cellspacing="0" width="100%">
                              <thead>
            <tr>
                <th>EventID</th>
                <th>Eventname</th>
                <th>Supplier</th>
                <th>Event_Type</th>
                <th>Event Status</th>
                <th>Price</th>
				<th>Sale Price</th>
				
				
            </tr>
        </thead>
        <tbody>
            <tr>
                <td>1</td>
                <td>Snorkel</td>
                <td>ABC Ltd.</td>
				<td>Watersport</td>
                <td>Not Available</td>
                <td>10</td>
                <td>8</td>
				
				
            </tr>
            <tr>
                <td>2</td>
                <td>Diving</td>
                <td>DEF Ltd.</td>
				<td>Safari</td>
                <td>Available</td>
                <td>449</td>
                <td>400</td>
				
				
            </tr>
            
           
            
        </tbody>
   
    
                            </table>
                        </div>
                    </div>
					
					
					
                   
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
	<script type="text/javascript">
	$(document).ready(function() {
    var table = $('#client-overview').DataTable( {
        "order": [[1, 'asc']]
    });
	});
	
	</script>
</body>
</html>
<?php
}
?>