<?php
$thisPage="Teachers Registration";
/*session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{*/
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Teachers Registration</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
      //  require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content" style="margin: 0px !important;">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Teachers Registration
                </h2>
            </div>            
            <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i>&nbsp;&nbsp;&nbsp;Back to Home</a></li>
                   
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                    
                        <form id="teacherRegistration" enctype="multipart/form-data">
							<h4>Create Account</h4>
							<fieldset>
								
								<div class="form-group form-float">
									<label for="username">Username</label>
                                    <input type="hidden" class="form-control" placeholder="User Group" name="choice" id="choice" value="Teacher-Registration" />    
									<input type="hidden" class="form-control" placeholder="User Group" name="usergroup" id="usergroup" value="Teacher" />
									<input type="text" class="form-control" placeholder="Username" name="username" id="username" required/>
									</div>
									<div class="form-group form-float">
									<label for="username">Password</label>
									<input type="password" class="form-control" placeholder="Password" name="password" id="password" minlength="8" required/>
									</div>
									<div class="form-group form-float">
									<label for="username">Email</label>
									<input type="email" class="form-control" placeholder="Email" name="email" id="email" required/>
									</div>

							</fieldset>
                            <h4>Teacher Information</h4>
                            <fieldset>
								
                            
								
								<div class="form-group form-float">
									<label for="booking-code">First Name</label>
									<input type="text" class="form-control" placeholder="First Name" name="first-name" id="first-name">
                                    
                                </div>


								<div class="form-group form-float">
									<label for="booking-code">Last Name</label>
									<input type="text" class="form-control" placeholder="Last Name" name="last-name" id="last-name">
                                    
                                </div>

                                <div class="form-group form-float">
                                    <label for="booking-code">Date of Birth</label>
                                    <input type="date" class="form-control" placeholder="Date of Birth" name="dob" id="dob">
                                    
                                </div>


                                <div class="form-group form-float">
									<label for="booking-code">Address</label>
									<input type="text" class="form-control" placeholder="Address" name="address" id="address">
                                    
                                </div>
								
								 
								<div class="form-group form-float">
									<div class="row">
									 <div class="col-lg-4 col-md-4 col-sm-12">
										 <label for="adults">Postal Code</label>
                                    	<input type="text" class="form-control" placeholder="Postal Code" name="postal-code" id="postal-code">
									</div>
									
									 <div class="col-lg-4 col-md-4 col-sm-12">
										<label for="children">City</label>
                                    <input type="text" class="form-control" placeholder="City" name="city" id="city">
									</div>
									
									 <div class="col-lg-4 col-md-4 col-sm-12">
										 <label for="enfants">Telephone</label>
                                    <input type="text" class="form-control" placeholder="Telephone" name="telephone" id="telephone">
									</div>
									</div>
									
                                </div>

                                <div class="form-group form-float">
                                    <label for="booking-code">Motivation</label>
                                    <input type="text" class="form-control" placeholder="Motivation" name="motivation" id="motivation">
                                    
                                </div>
								
								
						
								
                               
                            </fieldset>
                            
                
							<h4>Official Documents</h4>
                            <fieldset>
                                <p style="color: white;">Please upload some documents to verify your identity.</p>
                              	<div class="form-group form-float">
									<label for="booking-code">Passport/Drivers Licence ID card</label>
									<input type="file" class="form-control" placeholder="" name="id-card[]" id="id-card">
                                    
                                </div>
                                <div class="form-group form-float">
                                    <label for="booking-code">School Documentation</label>
                                    <input type="file" class="form-control" placeholder="" name="school-docs[]" id="school-docs">
                                    
                                </div>  							
                            </fieldset>

                            <h4>Courses and Distance</h4>
                            <fieldset>
                                <div class="form-group form-float">
                                    <label for="booking-code">Online Courses</label>
                                    <input type="radio" class="" placeholder="" name="online-courses" id="online-courses" value="Yes"> <span style="color: white;">Yes</span>
                                    <input type="radio" class="" placeholder="" name="online-courses" id="online-courses" value="No"> <span style="color: white;">No</span>
                                </div> 

                                <div class="row">
                                    
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Courses Select</strong> <small>Pick courses you would like to teach</small> </h2>
                        
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-6">
                                <p> <b>Select School Type *</b> </p>
                                <select class="form-control show-tick school-type" required name="school-type[]">
									<option value="">Select</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schooltype");
										$stmt->execute();
										$stmt->bind_result($typeID,$typeName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $typeID; ?>"><?php echo $typeName; ?></option>
									<?php
										}
									?>
                                </select>
                            </div>
							<div class="col-lg-3 col-md-6">
                                <p> <b>Select School Level *</b> </p>
                                <select class="form-control show-tick school-level" required name="school-level[]">
									<option value="" selected>Select</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoollevel");
										$stmt->execute();
										$stmt->bind_result($levelID,$levelName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $levelID; ?>"><?php echo $levelName; ?></option>
									<?php
										}
									?>
                                </select>
                            </div>
							<div class="col-lg-3 col-md-6">
                                <p> <b>Select School Year *</b> </p>
                                <select class="form-control show-tick school-year" required name="school-year[]">
									<option value="" selected>Select</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoolyear");
										$stmt->execute();
										$stmt->bind_result($yearID,$yearName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $yearID; ?>"><?php echo $yearName; ?></option>
									<?php
										}
									?>
                                </select>
                            </div>
							<div class="col-lg-3 col-md-6">
                                <p> <b>Select School Course *</b> </p>
                                <select class="form-control show-tick school-course" required name="school-course[]">
									
                                </select>
                            </div>
                        </div>
						<div class="row clearfix">
                            <div class="col-lg-3 col-md-6">
                                <p> <b>Select School Type *</b> </p>
                                <select class="form-control show-tick school-type" required name="school-type[]">
									<option value="">Select</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schooltype");
										$stmt->execute();
										$stmt->bind_result($typeID,$typeName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $typeID; ?>"><?php echo $typeName; ?></option>
									<?php
										}
									?>
                                </select>
                            </div>
							<div class="col-lg-3 col-md-6">
                                <p> <b>Select School Level *</b> </p>
                                <select class="form-control show-tick school-level" required name="school-level[]">
									<option value="" selected>Select</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoollevel");
										$stmt->execute();
										$stmt->bind_result($levelID,$levelName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $levelID; ?>"><?php echo $levelName; ?></option>
									<?php
										}
									?>
                                </select>
                            </div>
							<div class="col-lg-3 col-md-6">
                                <p> <b>Select School Year *</b> </p>
                                <select class="form-control show-tick school-year" required name="school-year[]">
									<option value="" selected>Select</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoolyear");
										$stmt->execute();
										$stmt->bind_result($yearID,$yearName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $yearID; ?>"><?php echo $yearName; ?></option>
									<?php
										}
									?>
                                </select>
                            </div>
							<div class="col-lg-3 col-md-6">
                                <p> <b>Select School Course *</b> </p>
                                <select class="form-control show-tick school-course" required name="school-course[]">
									
                                </select>
                            </div>
                        </div>
                    </div>
                </div>      
                            </div>
								</div>
								<div class="row">
                                <div class="col-md-6">
                                    
                                    <div class="form-group form-float">
                                    <label for="booking-code">Distance in (KM)</label>
                                    <input type="number" class="form-control" placeholder="Distance" name="distance" id="distance">
                                    
                                </div>
                                </div>
                                </div>                     
                            </fieldset>

                            <h4>Book a Conversation</h4>
                            <fieldset>
                                <div class="form-group form-float">
                                
 <div id='calendar'></div>
                                </div>                             
                            </fieldset>
							
                        </form>
                   
                
            </div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


	
	<!-- Jquery steps Wizard Code -->
	<script type="text/javascript">
	$(function () {
    //Advanced form with validation
    var form = $('#teacherRegistration').show();
    form.steps({
        headerTag: 'h4',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
          //  $.AdminOreo.input.activate();
            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
			
			
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
			//Jquery AJAX code to add client upon  insertion
            
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            //Code for Forms Data Submission
			run_waitMe($('body'), 1, "timer");
			$.ajax({
                            url: 'actions/add_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {


                          
                                   switch(result)
                                    {

                                      case "Error":
                                    
                                        
                                        setTimeout(function() {   
                                                 
                                               $('body').waitMe('hide');
                                               $('#teacherRegistration')[0].empty(); 
                                                showNotification("alert-danger","Sorry!!! Your account cannot be created right now.","top","center","","");
                                               //$("#error-epm").html("There Was An Error Inserting The Record."); 
                                                  
}, 500); 
                                        break;

                                         case "Success":
                                    
                                        
                                       setTimeout(function() { 

                                                  $('body').waitMe('hide');
                                                  $('#teacherRegistration')[0].reset();
										   window.location.replace("https://tutor.dev.we-think.nl/thankyou");
                                              
                                                 
                                                                                                    
}, 500);
                                        break;

                                    
                                    }
                              }
                      });
		
        }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'confirm': {
                equalTo: '#password'
            }
        }
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}
		
		
		function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
        message: text
    },
        {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
}
		
	
	</script>  
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
<script src='assets/plugins/fullcalendar/lib/moment.min.js'></script>
<script src='assets/plugins/fullcalendar/fullcalendar.min.js'></script>
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
<script>

  $(document).ready(function() {

    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'agendaWeek'
      },
      defaultDate: '2019-01-12',
      defaultView: 'agendaWeek',
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectHelper: true,
      select: function(start, end) {
        var title = prompt('Event Title:');
        var eventData;
        if (title) {
          eventData = {
            title: title,
            start: start,
            end: end
          };
          $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
        }
        $('#calendar').fullCalendar('unselect');
      },
      editable: true,
      eventLimit: true // allow "more" link when too many events
      /*events: [
        {
          title: 'All Day Event',
          start: '2019-01-01'
        },
        {
          title: 'Long Event',
          start: '2019-01-07',
          end: '2019-01-10'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2019-01-09T16:00:00'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2019-01-16T16:00:00'
        },
        {
          title: 'Conference',
          start: '2019-01-11',
          end: '2019-01-13'
        },
        {
          title: 'Meeting',
          start: '2019-01-12T10:30:00',
          end: '2019-01-12T12:30:00'
        },
        {
          title: 'Lunch',
          start: '2019-01-12T12:00:00'
        },
        {
          title: 'Meeting',
          start: '2019-01-12T14:30:00'
        },
        {
          title: 'Happy Hour',
          start: '2019-01-12T17:30:00'
        },
        {
          title: 'Dinner',
          start: '2019-01-12T20:00:00'
        },
        {
          title: 'Birthday Party',
          start: '2019-01-13T07:00:00'
        },
        {
          title: 'Click for Google',
          url: 'http://google.com/',
          start: '2019-01-28'
        }
      ]*/
    });

   /* Courses AJAX Calls */
	  
	  $(function(){
      $('.school-type').change(function(e){
		  e.stopImmediatePropagation();
          var text = $(this).find("option:selected").text();
		  var id = $(this).find("option:selected").val();
		  if(text == "Elementary")
		  {
			  var schoolType = $(this).find("option:selected").val();
			  $(this).parent().parent().siblings().find(".school-level").attr('disabled','disabled');
			  $(this).parent().parent().siblings().find(".school-year").attr('disabled','disabled');
			  $.ajax({
                            url: 'actions/fetch_courses.php',
                            data: {'typeID': schoolType},
				  			type: "POST",
							//dataType: "json",
                             success: function (result) {
								$(this).parent().parent().siblings().find(".school-course").html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                					//alert(json_obj[i].courseID + " " + json_obj[i].courseName);
									$(this).parent().parent().siblings().find(".school-course").append('<option value="'+String(json_obj[i].courseID)+'">'+String(json_obj[i].courseName)+'</option>');
								}
					
								 $(this).parent().parent().siblings().find(".school-course").selectpicker('refresh');
}
                      });
			  
	
		  }
		  
		  else
		  {
			  $(this).parent().parent().siblings().find(".school-level").removeAttr('disabled');
			  $(this).parent().parent().siblings().find(".school-year").removeAttr('disabled');
		  }
      });
 });

  });

</script>
</body>
</html>
<?php
//}
?>