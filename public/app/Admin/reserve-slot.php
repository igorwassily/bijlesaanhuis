<?php
$thisPage="Teachers Dashboard";
session_start();
if(!isset($_SESSION['Student']))
{
	header('Location: index.php');
}
else
{
	
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Reserve Slot</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
		if(isset($_GET['teacherid']))
		{
			$_SESSION['selectedTeacher'] = test_input($_GET['teacherid']);
		}
	else
	{
		$_SESSION['selectedTeacher'] = test_input($_POST['teacherid']);
	}
		
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
    color: black !important;
}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarStudentsDashboard.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Hi, <?php echo $_SESSION['Student']; ?>
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="logout"><i class="zmdi zmdi-home"></i> logout</a></li>
                    
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="body">
                        <button class="btn btn-primary btn-round waves-effect" id="change-view-today">today</button>
                        <button class="btn btn-default btn-simple btn-round waves-effect" id="change-view-day" >Day</button>
                        <button class="btn btn-default btn-simple btn-round waves-effect" id="change-view-week">Week</button>
                        <button class="btn btn-default btn-simple btn-round waves-effect" id="change-view-month">Month</button>
                        <div id="calendar" class="m-t-20" style="max-width: 100% !important;"></div>
                    </div>
                </div>
				
            </div>	
        </div>
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


	
	<!-- Jquery steps Wizard Code -->
	<?php
    require_once('includes/footerScriptsAddForms.php');
?>
<script src='assets/plugins/fullcalendar/lib/moment.min.js'></script>
<script src='assets/plugins/fullcalendar/fullcalendar.min.js'></script>
<script src='assets/plugins/fullcalendar/lib/moment.min.js'></script>
<!--<script src="assets/js/pages/calendar/calendar.js"></script>-->
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
<script>

  $(document).ready(function() {
	var today = moment().day();
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'agendaWeek'
      },
		    dayRender: function(date, cell) {
                    var today = $.fullCalendar.moment();
                    
                    if (date.get('date') == today.get('date')) {
                        cell.css("background-color", "transparent");
         }
    },
	editable:true,
      defaultDate: moment().format('YYYY-MM-DD'),
      defaultView: 'agendaWeek',
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectHelper: true,
	  events : 'includes/showSlots.php',
      /*select: function(start, end) {
        var title = prompt('Event Title:');
        var eventData;
        if (title) {
          eventData = {
            title: title,
            start: start,
            end: end
          };
          $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
        }
        $('#calendar').fullCalendar('unselect');
      },*/
		eventClick: function(calEvent, jsEvent, view) {
				if(calEvent.backgroundColor != 'red')
				{
				var choice = "teacherslotreservation";
				var teacher = <?php echo json_encode($_SESSION['selectedTeacher']) ?>;
				var student = <?php echo json_encode($_SESSION['StudentID']) ?>;
    			var start = calEvent.start;
				var end = calEvent.end;
				$.ajax({
                url: 'actions/add_scripts.php',
                data: {'choice': choice, 'teacher': teacher, 'student': student, 'start': start.toJSON(), 'end':end.toJSON()},
                type: "POST",
                success: function (json) {
					$(this).css('background-color', 'red');
					//$('#calendar').fullCalendar('refetchEvents');
                    alert("Slot Reserved Successfully");
					$('#calendar').fullCalendar( 'refetchEvents' );
                }
            });
				}
			else
			{
				alert("Slot Not Available");
			}
  },
      editable: true,
      eventLimit: true // allow "more" link when too many events
      /*events: [
        {
          title: 'All Day Event',
          start: '2019-01-01'
        },
        {
          title: 'Long Event',
          start: '2019-01-07',
          end: '2019-01-10'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2019-01-09T16:00:00'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2019-01-16T16:00:00'
        },
        {
          title: 'Conference',
          start: '2019-01-11',
          end: '2019-01-13'
        },
        {
          title: 'Meeting',
          start: '2019-01-12T10:30:00',
          end: '2019-01-12T12:30:00'
        },
        {
          title: 'Lunch',
          start: '2019-01-12T12:00:00'
        },
        {
          title: 'Meeting',
          start: '2019-01-12T14:30:00'
        },
        {
          title: 'Happy Hour',
          start: '2019-01-12T17:30:00'
        },
        {
          title: 'Dinner',
          start: '2019-01-12T20:00:00'
        },
        {
          title: 'Birthday Party',
          start: '2019-01-13T07:00:00'
        },
        {
          title: 'Click for Google',
          url: 'http://google.com/',
          start: '2019-01-28'
        }
      ]*/
    });
    $('#courses').multiselect();

  });

</script>
</body>
</html>
<?php
}
?>