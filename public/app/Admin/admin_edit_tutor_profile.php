<?php
$thisPage = "Edit Profile";
session_start();
if (!isset($_SESSION['AdminUser'])) {
	header('Location: index.php');
	die();
}

function translateToDutch($val)
{
	//$permission = array("Accepted"=>"Geaccepteerd", "Rejected"=>"Afgewezen", "Requested"=>"In behandeling");
	//return (isset($permission[$val]))? $permission[$val]) : "";
	if ($val == "Accepted")
		return "Geaccepteerd";
	else if ($val == "Rejected")
		return "Afgewezen";
	else if ($val == "Requested")
		return "In behandeling";
	return $val;
}

?>

<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>Wijzig profiel | Bijles Aan Huis</title>

    <!-- Custom Css -->
    <link rel="stylesheet" href="/app/assets/css/main.css">
    
	<?php
	require_once('includes/connection.php');
	require_once('includes/mainCSSFiles.php');
	?>
    
    <!--	    <link rel="stylesheet" href="../assets/plugins/bootstrap/css/bootstrap.min.css">-->
    <!--	    <link rel="stylesheet" href="../assets/plugins/dropzone/dropzone.css">-->
    <!--	    <link rel="stylesheet" href="../assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>-->
    <!--	    <link rel="stylesheet" href="../assets/plugins/morrisjs/morris.min.css" />-->

    <!-- JQuery DataTable Css -->
    <!--	    <link rel="stylesheet" href="../assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">-->

    <!-- Bootstrap Select Css -->

    <!-- Custom Css -->
    <!-- <link rel="stylesheet" href="/app/assets/css/main.css"> -->
    <!--	    <link rel="stylesheet" href="../assets/css/color_skins.css">-->
    <!--	    <link rel="stylesheet" href="../assets/plugins/waitme/waitMe.css">-->

    <link href="/app/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
          rel="stylesheet"/>
    <link href="/app/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
    <link rel="stylesheet" href="/app/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link href='/app/assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet'/>
    <link href='/app/assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print'/>
    <link rel="stylesheet" href="/app/assets/css/bootstrap-multiselect.css" type="text/css">
    <link rel="stylesheet" href="/app/assets/css/edit-profile-image.css" type="text/css">
    <link rel="stylesheet"
          href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet'/>
    <link rel='stylesheet'
          href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css'
          type='text/css'/>
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    



    <!-- Image Crop-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.css"
          type="text/css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.min.css"
          type="text/css"/>
    <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css'
          integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ'
          crossorigin='anonymous'>
    <link rel="stylesheet" href="/app/assets/css/chatapp.css">
    <style type="text/css">
        /*.jcrop-selection.jcrop-current  {
		 -webkit-border-radius: 50% !important;
		 -moz-border-radius: 50% !important;
		  border-radius: 50% !important;
			border: 1px solid #f1f1f1 !important;
		  margin: -1px;
		}*/
        /* #info .input-label {
            color:white;
            font-weight: 500;
        } */
        .fa, .far, .fas {
            font-family: "Font Awesome 5 Free" !important;
            font-size: 16px !important;
            color: #bbb !important;
        }

        /* jquery.Jcrop.min.css v0.9.12 (build:20130126) */
        .jcrop-holder {
            direction: ltr;
            text-align: left;
        }

        .jcrop-vline, .jcrop-hline {
            background: #FFF url(Jcrop.gif);
            font-size: 0;
            position: absolute;
        }

        .jcrop-vline {
            height: 100%;
            width: 1px !important;
        }

        .jcrop-vline.right {
            right: 0;
        }

        .jcrop-hline {
            height: 1px !important;
            width: 100%;
        }

        .jcrop-hline.bottom {
            bottom: 0;
        }

        .jcrop-tracker {
            -webkit-tap-highlight-color: transparent;
            -webkit-touch-callout: none;
            -webkit-user-select: none;
            height: 100%;
            width: 100%;
        }

        .jcrop-handle {
            background-color: #333;
            border: 1px #EEE solid;
            font-size: 1px;
            height: 7px;
            width: 7px;
        }

        .jcrop-handle.ord-n {
            left: 50%;
            margin-left: -4px;
            margin-top: -4px;
            top: 0;
        }

        .jcrop-handle.ord-s {
            bottom: 0;
            left: 50%;
            margin-bottom: -4px;
            margin-left: -4px;
        }

        .jcrop-handle.ord-e {
            margin-right: -4px;
            margin-top: -4px;
            right: 0;
            top: 50%;
        }

        .jcrop-handle.ord-w {
            left: 0;
            margin-left: -4px;
            margin-top: -4px;
            top: 50%;
        }

        .jcrop-handle.ord-nw {
            left: 0;
            margin-left: -4px;
            margin-top: -4px;
            top: 0;
        }

        .jcrop-handle.ord-ne {
            margin-right: -4px;
            margin-top: -4px;
            right: 0;
            top: 0;
        }

        .jcrop-handle.ord-se {
            bottom: 0;
            margin-bottom: -4px;
            margin-right: -4px;
            right: 0;
        }

        .jcrop-handle.ord-sw {
            bottom: 0;
            left: 0;
            margin-bottom: -4px;
            margin-left: -4px;
        }

        .jcrop-dragbar.ord-n, .jcrop-dragbar.ord-s {
            height: 7px;
            width: 100%;
        }

        .jcrop-dragbar.ord-e, .jcrop-dragbar.ord-w {
            height: 100%;
            width: 7px;
        }

        .jcrop-dragbar.ord-n {
            margin-top: -4px;
        }

        .jcrop-dragbar.ord-s {
            bottom: 0;
            margin-bottom: -4px;
        }

        .jcrop-dragbar.ord-e {
            margin-right: -4px;
            right: 0;
        }

        .jcrop-dragbar.ord-w {
            margin-left: -4px;
        }

        .jcrop-light .jcrop-vline, .jcrop-light .jcrop-hline {
            background: #FFF;
            filter: alpha(opacity=70) !important;
            opacity: .70 !important;
        }

        .jcrop-light .jcrop-handle {
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            background-color: #000;
            border-color: #FFF;
            border-radius: 3px;
        }

        .jcrop-dark .jcrop-vline, .jcrop-dark .jcrop-hline {
            background: #000;
            filter: alpha(opacity=70) !important;
            opacity: .7 !important;
        }

        .jcrop-dark .jcrop-handle {
            -moz-border-radius: 3px;
            -webkit-border-radius: 3px;
            background-color: #FFF;
            border-color: #000;
            border-radius: 3px;
        }

        .solid-line .jcrop-vline, .solid-line .jcrop-hline {
            background: #FFF;
        }

        .jcrop-holder img, img.jcrop-preview {
            max-width: none;
        }

        .mapboxgl-ctrl-attrib, .mapboxgl-ctrl-logo {
            display: none !important;
        }

        .mapboxgl-ctrl-geocoder li > a {
            color: black !important;
        }

        .mapboxgl-ctrl-geocoder input {
            color: black !important
        }


        /*Placeholder Color */
        input {
            border: 1px solid #bdbdbd !important;
        }

        label {
            font-size: 16px !important;
            margin-left: 14px !important
        }

        select {
            border: 1px solid #bdbdbd !important;

            /*color: white !important; */
        }

        .body-overflow {

        }

        /*	input:focus{
		background:transparent !Important;
			}

			select:focus{
		background:transparent !Important;
			}
			*/
        .modal-dialog {
            margin-top: 71px !important;
        }

        .fade {
            background: #66666694;
            opacity: 1;
        }

        .wizard .content {
            /*overflow-y: hidden !important;*/
        }

        /***********datatable content**********/
        th.sorting_asc:before {
            content: none !important;
        }

        th.sorting:before {
            content: none !important;
        }

        th.sorting_asc:after {
            content: none !important;
        }

        th.sorting:after {
            content: none !important;
        }

        .dataTables_length {
            display: none !important;
        }

        .dataTables_filter {
            display: none !important;
        }

        .dataTables_info {
            display: none !important;
        }

        .dataTables_paginate {
            display: none !important;
        }

        /***********************************************************/
        .wizard .content label {

            color: white !important;

        }

        .wizard > .steps .current a {
            background-color: #029898 !Important;
        }

        .wizard > .steps .done a {
            background-color: #828f9380 !Important;
        }

        .wizard > .actions a {
            background-color: #029898 !Important;
        }

        .wizard > .actions .disabled a {
            background-color: #eee !important;
        }

        .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
            color: white;
        }

        table {
            color: white;
        }

        .multiselect.dropdown-toggle.btn.btn-default {
            display: none !important;
        }


        .fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
            color: black !important;
        }


        .btn-block {
            width: 40% !Important;
        }

        .form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
            background-color: transparent !important;
        }

        .bootstrap-select[disabled] button {
            color: gray !important;
            border: 1px solid gray !important;
        }

        .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
            color: white;
        }

        .dropdown-menu > ul > li > a {
            color: #555 !important;
        }

        .errorLabel, .terrorLabel {
            color: red;
        }

        .hide {
            display: none;
        }

        ul.dropdown-menu.inner {
            display: block;
        }

        #radioBtn .notActive {
            color: #5cc75f !important;
            background-color: #fff !important;
            border: 1px solid #bbb !important;
        }

        #radioBtn a.active {
            background-color: #5CC75F;
            border: 1px solid #bbb;
        }

        .form-control {
            background-color: transparent;
        }

        span.bs-caret {
            display: none;
        }

        #coordinates {
            display: none;
        }

        button#update-more-courses {
            margin: auto;
        }

        .btn-group.bootstrap-select.show-tick.show {
            display: inline-block !important;
        }

        section {
            overflow: hidden;
        }

        .body {
            border: 2px solid #f1f1f1;
        }

        select.form-control.form-control-sm {
            height: 30px !important;
        }

        /*profile selection*/
        .grid {
            padding-bottom: 4%;
        }

        span.maxlabel {
            font-size: 12px !important;
            float: right !important;
        }

        .card .body {
            background-color: #FAFBFC !important;
        }

        li.dropdown-header {
            font-size: 16px !important;
            text-align: center;
        }

        button.actions-btn.btn.btn-default {
            width: 45% !important;
        }

        .bs-actionsbox .btn-group.btn-group-sm.btn-block {
            width: 100% !important;
        }

        /* padding edit profile popup */
        .custom-padding {
            padding: 0 0 2rem 0 !important;
            margin-top: 53px;
        }

        #image-edit-submit-box {
            display: none;
            z-index: 1;
            position: relative;
            top: inherit;
        }

        #jcrop img {
            border-radius: 4px;
        }

        #deresultcourse {
            display: block;
            position: fixed;
            bottom: 0;
            margin: 0 auto;
            z-index: 10000000;
            width: 200px;
            left: 0;
            right: 0;
        }

        /* extra popup modal styling */
        .modal h6 {
            text-transform: lowercase;
        }

        .modal .modal-title {
            font-size: 18px !important;
            font-weight: bold !important;
        }

        .modal .modal-text {
            padding: 0 20px 27px 20px;
        }

        .modal-content .modal-header button {
            right: 12px !important;
            top: 6px !important;
        }

        .custom-padding {
            padding: 0 0 1rem 0 !important;
            margin-top: 1rem;
        }

        @media screen and (min-width: 480px) and (max-width: 720px) {
            .edit-profile-content__image-container {
                float: none;
                width: 100%;
            }

            .edit-profile-content__information-container {
                float: none;
                width: 100%;
            }

            .image {
                padding: 0 12%;
            }
        }

        @media screen and (max-width: 480px) and (min-width: 720px) {
            .image {
                padding: 0;
            }
        }

    </style>
    <style type="text/css">
        #calendar {
            background-color: #fafafa !important;
        }

        .cal-month-day {
            color: black;
        }

        .cal-row-head {
            background-color: #fafafa !important;
        }

        .empty {
            background-color: white !important;
            border: 2px solid black;
        }

        .inline_time {
            color: #50d38a !important;
        }

        .inline_time_disabled {
            color: #6b7971 !important;
        }

        #cal-slide-content {
            font-family: inherit;
            color: #bdbdbd;
        }

        .page-header {
            height: 0 !important;
            padding-bottom: 0px;
            margin: -25px 0 50px;
            border-bottom: 0px;
        }

        .pull-right {
            pointer-events: none;
        }
    </style>
    <style type="text/css">

        /*Placeholder Color */
        input {
            border: 1px solid #bdbdbd !important;

            color: white !important;
        }

        select {
            border: 1px solid #bdbdbd !important;

            color: white !important;
        }

        input:focus {
            background: transparent !Important;
        }

        select:focus {
            background: transparent !Important;
        }

        .wizard .content {
            /*overflow-y: hidden !important;*/
        }

        .wizard .content label {

            color: white !important;

        }

        .wizard > .steps .current a {
            background-color: #029898 !Important;
        }

        .wizard > .steps .done a {
            background-color: #828f9380 !Important;
        }

        .wizard > .actions a {
            background-color: #029898 !Important;
        }

        .wizard > .actions .disabled a {
            background-color: #eee !important;
        }

        .btn.btn-simple {
            border-color: white !important;
        }

        .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
            color: white;
        }

        table {
            color: white;
        }

        .multiselect.dropdown-toggle.btn.btn-default {
            display: none !important;
        }

        .navbar.p-l-5.p-r-5 {
            display: none !important;
        }

        .card {
            border-radius: 10px !important;
            background-color: transparent;
        }

        .chat .chat-header {
            border-bottom: 0 solid #486066;
        }

        .chat {
            margin-left: 0 !important;
            margin-bottom: 0 !important;
            border-left: none !important;
        }

        #adminNote ~ label {
            display: none;
        }

        input {
            color: black!important;
        }

        .sidebar .menu .list li:not(:first-child) a {
            color: #fff;
        }

        .theme-green .sidebar .menu .list li.active > :first-child i, .theme-green .sidebar .menu .list li.active > :first-child span {
           color: #50d38a;
        }

        .m-l-0, .ls-toggle-menu section.content, .ls-closed section.content, .overlay-open .sidebar {
            margin-left: 0px !important;
        }
        /*.theme-green section.content::before {
            background-color: #5cdb94;
        } */
        #info {
            position: relative;
        }
        .navbar-header h2 {
            margin-left: 30px !important;
        }
        .block-header {
            padding: 0;
            margin-top: -20px;
        }
        .block-header {
            margin-bottom: 20px;
        }
        .block-header h2 {
            padding-top:20px;
        }
        .nav.navbar-nav li:first-child {
            z-index:1;
        }
    </style>
    <link href="/app/assets/plugins/croppie/demo.css" rel="stylesheet">
    <link href="/app/assets/plugins/croppie/croppie.css" rel="stylesheet">
	<?php
	$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                 alt="Oreo"></div>
        <p>Please wait...</p>
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php
require_once('includes/header.php');
require_once('includes/sidebarAdminDashboard.php');
?>

<style type="text/css">
    body section.edit-profile-content {
        margin: 0px 0 0 250px !important;
    }

    /* section.content::before {
        background-color: transparent !important;
    } */

    section.content form {
        width: 100%;
    }

    #pbe-footer-wa-wrap {
        display: none;
    }

    table, .btn, .btn:hover, .btn:focus {
        color: rgb(72, 96, 102)!important;
    }
    #newCourse .edit-profile-content__group--aligned {
        width: 32%;
        display: inline-block;
    }
    input {
        background: #fafbfc !important;
    }
    .edit-profile-form {
        padding-bottom: 200px !important;
    }
    #footer {
        display: none;
    }
</style>
    <?php
        $showAdmin = true;
        require_once('../includes/edit_teacher_profile.php');
    ?>

</body>
</html>