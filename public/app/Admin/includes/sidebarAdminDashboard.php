<!-- Left Sidebar -->
<style>
    .leftSlot{
        background-color: #FE5D5D  !important;
        font-size: 12px;
        line-height: 11px;
        color: white !important;
        background-color: #61a1de;
        border-radius: 30%;
        display: inline-block;
        height: 20px;
        padding: 5px 10px;
        position: relative;
        top: -5px;
    }
    .leftSlot:hover{
        color: white !important;
    }
</style>
<aside id="leftsidebar" class="sidebar">
  <div class="tab-content">
    <div class="tab-pane stretchRight active" id="dashboard">
      <div class="menu">
        <ul class="list">
         <li>
          <div class="user-info m-b-20 p-b-15">
            <?php
            $adminid = $_SESSION['AdminEmail'];
            define("NOTIFICATION_TEACHER_REGISTER", 2);
            define("NOTIFICATION_REQUEST_SUBJECT", 3);
            define("NOTIFICATION_REQUEST_LEVEL", 4);
            define("NOTIFICATION_LESSON_NO_REACT", 5);
            define("NOTIFICATION_MESSAGE_NO_REACT", 6);
            define("NOTIFICATION_LESSON_CANCEL", 7);
            define("NOTIFICATION_ALL_STUDENTS", 8);

            $stmt = $con->prepare("select adminProfileImage from admin where adminEmail = ?");

            $stmt->bind_param("s",$adminid);
            $stmt->execute();
            $stmt->bind_result($profile);
            $stmt->store_result();
            $stmt->fetch();


            /* NOTIFICATIONS */

            $arrayOfNotifications = [];
            $arrayOfNewReg = [];
            $arrayOfReqSubs = [];
            $arrayOfReqLevel = [];
            $arrayOfNotificationLesson = [];
            $arrayOfMessageNoReact = [];
            $arrayOfCanceledAppointment = [];
            $arrayOfAllStudentsList = [];


            // NEW REG QUERY
            $q = 'SELECT u.userID FROM `user` u , contact c, teacher t, admincalendarbooking acb WHERE usergroupID=2 AND disabled<>1 AND t.isConfirmed=0 AND t.teacherlevel_rate_ID IS NULL  AND u.userID=c.userID AND t.userID=c.userID AND acb.teacherID=t.userID';
            $q2 = mysqli_query($con,$q);


            while($row=mysqli_fetch_assoc($q2)) {
	            $arrayOfNewReg[] = $row['userID'];
            }


            /* REQUEST - SUBJECTS */
            $query2 = "SELECT teacher_courseID, schoolcourse.courseName, tc.datetime, tc.teacherID, tc.courseID, schoolcourse.schoolcourseID, schoolcourse.schoolcourseID, tc.permission, (SELECT typeName FROM schooltype st, courses WHERE st.typeID=courses.coursetype AND courses.schoolcourseID=tc.courseID LIMIT 1)typeName, (SELECT GROUP_CONCAT(schoollevel.levelName) FROM schoollevel WHERE FIND_IN_SET(levelID, tc.schoollevelID))levelName, (SELECT GROUP_CONCAT(schoolyear.yearName) FROM schoolyear WHERE FIND_IN_SET(yearID, tc.schoolyearID))yearName, (SELECT CONCAT(c.firstname,' ' ,c.lastname) FROM contact c WHERE c.userID=tc.teacherID LIMIT 1)teachername FROM schoolcourse, teacher_courses tc WHERE tc.courseID =schoolcourse.schoolcourseID AND tc.permission='Requested' ORDER BY tc.datetime DESC";
            $q23 = mysqli_query($con,$query2);

            while($row=mysqli_fetch_assoc($q23)) {
	            $arrayOfReqSubs[] = $row['teacher_courseID'];
            }

            /* REQUEST - SUBJECTS */
            $query3 = "SELECT t.userID AS `teacherID`, t.requested_teacherlevel_rate_ID, CONCAT(c.firstname,' ' ,c.lastname)teachername, (SELECT `level` FROM teacherlevel_rate WHERE ID=t.requested_teacherlevel_rate_ID)rlevel, (SELECT `level` FROM teacherlevel_rate WHERE ID=t.teacherlevel_rate_ID)clevel, c.place_name,  a.Motivation FROM teacher t, applications a, contact c WHERE permission='Requested' AND c.userID=t.userID AND a.userID=t.userID ORDER BY teacherName";
            $q233 = mysqli_query($con,$query3);

            while($row=mysqli_fetch_assoc($q233)) {
	            $arrayOfReqLevel[] = $row['teacherID'];
            }

            // NOTIFICATIONS - LESSON QUERY
            $query4 = "SELECT c.calendarbookingID FROM calendarbooking c LEFT JOIN teacher t ON c.teacherID = t.userID LEFT JOIN contact c2 ON c2.userID = t.userID LEFT JOIN student s  ON c.studentID = s.userID LEFT JOIN contact c3 on c3.contactID = s.contactID WHERE c.accepted = 0 AND c.isSlotCancelled = 0 AND c.timestamp <= now() + INTERVAL 24 HOUR AND c3.contacttypeID = 2;";
            $q4 = mysqli_query($con,$query4);

            while($row=mysqli_fetch_assoc($q4)) {
	            $arrayOfNotificationLesson[] = $row['calendarbookingID'];
            }

            // NOTIFICATIONS - MESSAGE NO REACT QUERY
            $query5 = "SELECT DISTINCT e.emailID FROM email e LEFT JOIN contact c ON e.receiverID = c.userID LEFT JOIN contact c2 ON e.senderID = c2.userID WHERE c.contacttypeID = 1 AND e.senderID != 0 AND c2.contacttypeID = 2 AND c2.prmary = 1 AND e.message != 'Test' AND e.message != ' ' AND e.message NOT LIKE '%Bijlesverzoek ingediend%' AND e.message NOT LIKE '%Bijles geannuleerd%' AND e.message NOT LIKE '%Bijles geannuleerd / geweigerd%' AND e.message NOT LIKE '%Afspraak ingeboekt%' AND e.firstMessage = 1 GROUP BY e.emailID DESC";
            $q5 = mysqli_query($con,$query5);

            while($row=mysqli_fetch_assoc($q5)) {
	            $arrayOfMessageNoReact[] = $row['emailID'];
            }

            // NOTIFICATIONS - CANCELED APPOINTMENT
            $query6 = "SELECT c.calendarbookingID FROM calendarbooking c LEFT JOIN teacher t ON c.teacherID = t.userID LEFT JOIN contact c2 ON c2.userID = t.userID LEFT JOIN student s  ON c.studentID = s.userID LEFT JOIN contact c3 on c3.contactID = s.contactID WHERE c.isSlotCancelled = 1 AND c.timestamp <= now() + INTERVAL 24 HOUR AND c3.contacttypeID = 2;";
            $q6 = mysqli_query($con,$query6);

            while($row=mysqli_fetch_assoc($q6)) {
	            $arrayOfCanceledAppointment[] = $row['calendarbookingID'];
            }

            // LEERLINGEN
            $query7 = "SELECT u.userID FROM user u, contact c, student s WHERE usergroupID = 1 AND disabled = 0 AND u.userID = c.userID AND c.contacttypeID = 2 AND s.contactID = c.contactID";
            $q7 = mysqli_query($con,$query7);

            while($row=mysqli_fetch_assoc($q7)) {
	            $arrayOfAllStudentsList[] = $row['userID'];
            }

            // NOTI QUERY
            $q = 'SELECT type, userID FROM admin_msg_read WHERE type IN('.NOTIFICATION_TEACHER_REGISTER.','.NOTIFICATION_REQUEST_SUBJECT.', '.NOTIFICATION_REQUEST_LEVEL.', '.NOTIFICATION_LESSON_NO_REACT.', '.NOTIFICATION_MESSAGE_NO_REACT.', '.NOTIFICATION_LESSON_CANCEL.', '.NOTIFICATION_ALL_STUDENTS.')';
            $q2 = mysqli_query($con,$q);

            while($row=mysqli_fetch_assoc($q2)) {
	            $arrayOfNotifications[$row['type']][] = $row['userID'];
            }




            /* TYPE = 2 => NEW REGISTRATIONS */
            $finalResultNewReg = count(array_diff($arrayOfNewReg,$arrayOfNotifications[NOTIFICATION_TEACHER_REGISTER]));
//            var_dump($finalResultNewReg);

            /* TYPE = 3 => REQUESTS / SUBJECTS */
            $finalResultReqSubs = count(array_diff($arrayOfReqSubs,$arrayOfNotifications[NOTIFICATION_REQUEST_SUBJECT]));
//            var_dump($finalResultReqSubs);

            /* TYPE = 4 => REQUESTS / LEVEL */
            $finalResultReqLevel = count(array_diff($arrayOfReqLevel,$arrayOfNotifications[NOTIFICATION_REQUEST_LEVEL]));
//            var_dump($finalResultReqLevel);

            /* TYPE = 5 => NOTIFICATION / LESSON NO REACT */
            $finalResultNoLessonReact = count(array_diff($arrayOfNotificationLesson, $arrayOfNotifications[NOTIFICATION_LESSON_NO_REACT]));

            /* TYPE = 6 => NOTIFICATION / MESSAGE NO REACT */
            $finalResultNoMessageReact = count(array_diff($arrayOfMessageNoReact, $arrayOfNotifications[NOTIFICATION_MESSAGE_NO_REACT]));

            /* TYPE = 7 => NOTIFICATION / CANCELED APPOINTMENT */
            $finalResultCanceledAppointment = count(array_diff($arrayOfCanceledAppointment, $arrayOfNotifications[NOTIFICATION_LESSON_CANCEL]));

            /* TYPE = 8 => ALL LEERLING */
            $finalResultAllStudents = count(array_diff($arrayOfAllStudentsList, $arrayOfNotifications[NOTIFICATION_ALL_STUDENTS]));

            ?>
            <!-- <div class="image"><a href="#"><img src="<?php echo $profile; ?>" alt="User" style="width:55% !important; height: 110px !important;"></a></div> -->
            <div class="detail mt-5">
              <!-- <h4>Hi, <?php echo ucwords($_SESSION['AdminUser']); ?></h4> -->
              <small>
                <span>
                  <!-- <a href="my-account">My Account&nbsp;&nbsp;&nbsp;&nbsp;|</a> -->
                  <a href="logout">Uitloggen</a>
                </span>
              </small>
            </div>
          </div>
        </li>

        <!-- --------------- Dashboard ------------------>
        <li<?php if ($thisPage=="Dashboard") echo " class=\"active open\""; ?>><a href="dashboard"><i class="zmdi zmdi-view-headline"></i><span>Home</span></a></li>

        
        <!-- --------------- Teacher ------------------>
        <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Docenten <?php if($finalResultNewReg != 0) {?><span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultNewReg; ?></span><?php } ?> </span> </a>
          <ul class="ml-menu">
            <li<?php if ($thisPage=="active Teachers list") echo " class=\"active open\""; ?>><a href="active_teacher_list"><span>Getoond op site</span></a></li>
            <li<?php if ($thisPage=="unconfirmed Teachers list") echo " class=\"active open\""; ?>><a href="unconfirmed_teacher_list"><span>Intake geaccepteerd</span></a></li>
            <li<?php if ($thisPage=="new  Teachers registered") echo " class=\"active open\""; ?>><a href="new_teacher_registeration"><span>Nieuwe registraties  <?php if($finalResultNewReg != 0) {?> <span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultNewReg; ?><?php } ?></span></span></a></li>


            <!--
				<li<?php //if ($thisPage=="Approve Teachers") echo " class=\"active open\""; ?>><a href="approve-teachers"><span>Pending Teachers</span></a></li>
            	<li<?php //if ($thisPage=="Teacher List") echo " class=\"active open\""; ?>><a href="teacher-list"><span>Approved Teachers</span></a></li>
			-->
			  <!-- <li<?php if ($thisPage=="Teacher subject List" || $thisPage=="Teacher subjchange") echo " class=\"active open\""; ?>><a href="teacher-subject"><span>Change subject</span></a></li> -->
            <!-- <li<?php if ($thisPage=="teacher registration") echo " class=\"active open\""; ?>><a href="../admin_teacher_registration.php"><span>Registration</span></a></li> -->

            
          </ul>
        </li>

        <!-- --------------- Students ------------------>
        <li<?php if ($thisPage=="all Student list") echo " class=\"active open\""; ?>><a href="all_student_list"><i class="zmdi zmdi-view-headline"></i><span>Leerlingen<?php if($finalResultAllStudents != 0) {?> <span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultAllStudents; ?><?php } ?></span></a></li>
        <!-- <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Leerlingen</span> </a>
          <ul class="ml-menu">
            <li<?php if ($thisPage=="all Student list") echo " class=\"active open\""; ?>><a href="all_student_list"><span>Students Overview</span></a></li>
            <li<?php if ($thisPage=="Student list finished") echo " class=\"active open\""; ?>><a href="student_list_finished"><span>Students finished</span></a></li>
          </ul>
        </li> -->

            <!-- --------------- Notifications ------------------>
            <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Notificaties <?php if( ($finalResultNoLessonReact+$finalResultCanceledAppointment+$finalResultNoMessageReact) != 0) {?><span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultNoLessonReact+$finalResultCanceledAppointment+$finalResultNoMessageReact; ?></span><?php }?></span></a>
                <ul class="ml-menu">
                    <li<?php if ($thisPage=="notifications") echo " class=\"active open\""; ?>><a href="notifications"><i class="zmdi zmdi-view-headline"></i><span>Bijlesverzoeken zonder reactie<?php if( ($finalResultNoLessonReact) != 0) {?><span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultNoLessonReact; ?></span><?php }?></span></a></li>
                    <li<?php if ($thisPage=="notifications3") echo " class=\"active open\""; ?>><a href="notifications3"><i class="zmdi zmdi-view-headline"></i><span>Geannuleerde bijlessen<?php if( ($finalResultCanceledAppointment) != 0) {?><span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultCanceledAppointment; ?></span><?php }?></span></a></li>
                    <li<?php if ($thisPage=="notifications2") echo " class=\"active open\""; ?>><a href="notifications2"><i class="zmdi zmdi-view-headline"></i><span>Eerste chat berichten zonder reactie<?php if( ($finalResultNoMessageReact) != 0) {?><span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultNoMessageReact; ?></span><?php }?></span></a></li>

                </ul>
            </li>



        <!-- --------------- Reports ------------------>
        <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Factureren</span> </a>
          <ul class="ml-menu">
			<li<?php if ($thisPage=="all Teachers appointment list") echo " class=\"active open\""; ?>><a href="all_teacher_appointment_list"><span>Docenten</span></a></li>
            <li<?php if ($thisPage=="all Students appointment list") echo " class=\"active open\""; ?>><a href="all_student_appointment_list"><span>Leerlingen</span></a></li>            
            <li<?php if ($thisPage=="all teacher travel cost") echo " class=\"active open\""; ?>><a href="all_teacher_travel_costs"><span>Custom Reisekosten</span></a></li>
          </ul>
        </li>
        <!-- --------------- Requests ------------------>
            <li><a href="javascript:void(0);" class="menu-toggle"><i
                            class="zmdi zmdi-view-headline"></i><span>Verzoeken <?php if( ($finalResultReqSubs+$finalResultReqLevel) != 0) {?><span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultReqSubs+$finalResultReqLevel; ?></span><?php }?></span> </a>
                <ul class="ml-menu">
                    <li<?php if ($thisPage == "View teacher requests") echo " class=\"active open\""; ?>><a
                                href="teacherrequest#pending"><span>Vakken  <?php if( ($finalResultReqSubs) != 0) {?><span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultReqSubs; ?></span><?php } ?></span></a></li>
                    <li<?php if ($thisPage == "View teacher requests2") echo " class=\"active open\""; ?>><a
                                href="teacherrequest2"><span>Docenttype  <?php if( ($finalResultReqLevel) != 0) {?><span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $finalResultReqLevel; ?></span><?php } ?></span></a></li>
                </ul>
            </li>


        <!-- --------------- Courses ------------------>
        <!--
		<li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Courses</span> </a>
          <ul class="ml-menu">			
             <li<?php //if ($thisPage=="View Courses") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="view-courses"><span>View Courses</span></a></li>
            <li<?php //if ($thisPage=="Add Courses") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="add-courses"><span>Add Courses</span></a></li>
          </ul>
        </li>
		-->

        <!-- --------------- Management ------------------>
        <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Variabelen</span> </a>
          <ul class="<?php if ($thisPage=="View Courses" || $thisPage=="Add Courses") echo "active open "; ?>ml-menu" keep-menu-open="true">
            <li <?php if ($thisPage=="View Courses" || $thisPage=="Add Courses") echo " class=\"active open\""; ?> keep-menu-open="true"> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Vakken</span></a>
              <ul class="ml-menu">
                <li<?php if ($thisPage=="View Courses") echo " class=\"active open\""; ?> keep-menu-open="true" style="margin-left: 30px;"><a href="view-courses"><span>Aanmaken</span></a></li>
                <li<?php if ($thisPage=="Add Courses") echo " class=\"active open\""; ?> keep-menu-open="true" style="margin-left: 30px;"><a href="add-courses"><span>Toevoegen</span></a></li>
              </ul>
            </li>
             <li<?php if ($thisPage=="View teacherlevel") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="view-teacherlevel"><span>Docenttype</span></a></li>
            <li<?php if ($thisPage=="Admin SlotCreation") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="admin-slotcreation"><span>Beschikbaarheid intake</span></a></li>
            <li<?php if ($thisPage=="Notification") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="admin-notification"><span>Notificaties</span></a></li>
          </ul>
        </li>

         <!-- Emails -->
        <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>E-Mails</span> </a>
          <ul class="<?php if ($thisPage=="E-Mail content" || $thisPage=="E-Mail template") echo "active open "; ?>ml-menu" keep-menu-open="true">
            <li<?php if ($thisPage=="E-Mail template") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="email-template"><span>E-mail template</span></a></li>
            <li<?php if ($thisPage=="E-Mail content") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="email-content"><span>E-mail content</span></a></li>
          </ul>
        </li>

        <!-- Template Generator -->
        <li <?php if ($thisPage=="Bijles create page" || $thisPage=="Site Map") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Bijles Template</span> </a>
          <ul class="<?php if ($thisPage=="Bijles create page" || $thisPage=="Site Map") echo "active open "; ?>ml-menu" keep-menu-open="true">
            <li <?php if ($thisPage=="Bijles create page") echo " class=\"active open\""; ?> keep-menu-open="true"> <a href="bijles-create-page"><span>Create page</span></a></li>
            <li <?php if ($thisPage=="Site Map") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="/admin/sitemap"><span>Site Map</span></a></li>
          </ul>
        </li>

        <!-- <li<?php if ($thisPage=="My Account") echo " class=\"active open\""; ?>><a href="my-account"><i class="zmdi zmdi-view-headline"></i><span>My Account</span></a></li> -->


      </ul>
    </div>
  </div>
</div>    
</aside>
