<meta name="robots" content="noindex" />
<meta name="googlebot" content="noindex" />
<meta name="googlebot-news" content="noindex" />

<link rel="stylesheet" href="/app/Admin/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/app/Admin/assets/plugins/dropzone/dropzone.css">
<link rel="stylesheet" href="/app/Admin/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<link rel="stylesheet" href="/app/Admin/assets/plugins/morrisjs/morris.min.css" />

<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="/app/Admin/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://wotcnow.com/pm/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">

<!-- Bootstrap Select Css -->
<!-- <link rel="stylesheet" href="/app/Admin/assets/plugins/bootstrap-select/css/bootstrap-select.css" /> -->

<!-- Custom Css -->
<link rel="stylesheet" href="/app/Admin/assets/css/main.css">
<link rel="stylesheet" href="/app/Admin/assets/css/color_skins.css">
<link rel="stylesheet" href="/app/Admin/assets/plugins/waitme/waitMe.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<style>
	#ui-datepicker-div{
z-index:350 !important;
}
</style>


