<!-- Top Bar -->
<nav class="navbar p-l-5 p-r-5">
    <ul class="nav navbar-nav navbar-left" style="text-align: right !important;">
        <li>
            <div class="navbar-header">
                <a href="javascript:void(0);" class="bars"></a>
                
            </div>
        </li>
		<li class="hidden-md-down"><a href="BlankPage" title="Events"><b>Calendar</b></a></li>
        <li class="hidden-md-down"><a href="BlankPage" title="Inbox"><b>Email</b></a></li>
        <li><a href="BlankPage" title="Contact List"><b>Contacts</b></a></li>
        <li><a href="BlankPage" ><b>Notifications</b></a></li>
		 <li><a href="BlankPage" ><b>Projects</b></a></li>
        <li class="hidden-sm-down">
            <div class="input-group">                
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-addon">
                    <i class="zmdi zmdi-search"></i>
                </span>
            </div>
        </li>        
        <li>
           
            <a href="logout" class="mega-menu" data-close="true"><b>Logout</b></a>
        </li>
    </ul>
</nav>
