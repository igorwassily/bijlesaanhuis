<?php

$cwd = getcwd();
chdir(__DIR__);
require_once '../../../../vendor/autoload.php';
require_once '../../../../inc/Common.php';
chdir($cwd);

$servername = getenv('RDS_HOSTNAME');
$username = getenv('RDS_USERNAME');
$password = getenv('RDS_PASSWORD');
$dbname = getenv('RDS_DB_NAME');

// Create connection
$con = new mysqli($servername, $username, $password, $dbname);
$con->query('SET NAMES utf8mb4');
$con->set_charset("utf8mb4");
// Check connection
if ($con->connect_error) {
	die("Connection failed: " . $con->connect_error);
}
$db = Common::GetDb();

//$con->set_charset("utf8");

//for ajax server processing
$sql_details = array(
	'user' => $username,
	'pass' => $password,
	'db' => $dbname,
	'host' => $servername
);


define("ELEMENTRY_TYPE_ID", 9);

define("ADMIN_ID", 1);
define("ELEMENTARY_COURSE_NAME", "Elementary Course");
define("SLOT_TIME_IN_MINIUTES", 20);
define("SLOT_TIME_GAP_IN_MINIUTES", 0);

define("MINIMUM_NO_OF_DAYS_TO_CONSIDER_TUTOR_ACTIVE", 30);
define("TUTOR_REGISTERED_IN_N_DAYS", 30);


function test_input($data)
{
	global $con;

	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);

	return $con->real_escape_string($data);
}


