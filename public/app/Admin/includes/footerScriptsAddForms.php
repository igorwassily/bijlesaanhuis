<!-- Jquery Core Js --> 
 <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->

<script src="assets/plugins/jquery-validation/jquery.validate.js"></script> <!-- Jquery Validation Plugin Css -->
<script src="assets/plugins/jquery-steps/jquery.steps.js"></script> <!-- JQuery Steps Plugin Js -->

	
<script src="assets/plugins/momentjs/moment.js"></script>
<script src="assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<script src="assets/js/pages/forms/basic-form-elements.js"></script> 
<script src="assets/js/pages/forms/advanced-form-elements.js"></script> 
<script src="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>
<script src="assets/plugins/waitme/waitMe.js"></script>
<script>
    function run_waitMe(el, num, effect){
    switch (num) {
      case 1:
      maxSize = '';
      textPos = 'vertical';
      break;
      case 2:
      maxSize = 30;
      textPos = 'vertical';
      break;
      case 3:
      maxSize = 30;
      textPos = 'horizontal';
      break;
    }
    el.waitMe({
      effect: effect,
      text: 'Please wait...',
      bg: 'rgba(255,255,255,0.7)',
      color: '#000',
      maxSize: maxSize,
      source: 'img.svg',
      textPos: textPos,
      onClose: function() {}
    });
  }

    </script>