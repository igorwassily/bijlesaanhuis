<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
if(isset($_SESSION['selectedTeacher']))
{
	$teacherid = $_SESSION['selectedTeacher'];
}
else
{
	$teacherid = $_SESSION['TeacherID'];
}
$status = 1;
require_once("connection.php");
$data = array();
$stmt = $con->prepare("SELECT ts.slotID, ts.teacherID, ts.title, ts.datee, ts.starttime, ts.endtime, ts.dow, ts.status, cb.teacherID, cb.datee, cb.starttime, cb.endtime from teacherslots ts LEFT JOIN calendarbooking cb ON (ts.slotID = cb.slotID) where ts.teacherID = ?");
$stmt->bind_param("i",$teacherid);
$stmt->execute();
$stmt->bind_result($slotid,$teacherID,$title,$datee,$starttime,$endtime,$dow,$status,$teacherID01,$datee01,$starttime01,$endtime01);
$stmt->store_result();
$arr = array(0,1,2,3,4,5,6);
while($stmt->fetch())
{
	if($teacherID01 != NULL)
	{
		$data[] = array(
  'title'   => $title,
  'start'   => $datee."T".$starttime,
  'end'   => $datee."T".$endtime,
  'backgroundColor' => 'red',
			'daysOfWeek' => $arr,
            'ranges' => array(
    0 => array(
        'start' => "2019-03-10",
        'end' => "2019-03-30",
    )
),
            'allDay' => false
  );	
	}
	else
	{
		$data[] = array(
  'title'   => $title,
  'start'   => $datee."T".$starttime,
  'end'   => $datee."T".$endtime,
  'backgroundColor' => 'green',
			'daysOfWeek' => $arr,
            'ranges' => array(
    0 => array(
        'start' => "2019-03-10",
        'end' => "2019-03-30",
    )
),
            'allDay' => false
  );
	}
}

echo json_encode($data);

?>