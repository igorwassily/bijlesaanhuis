<?php
require_once("connection.php");
// to implement ProjectManager Data Table
// DB table to use
$table = 'client';

// Table's primary key
$primaryKey = 'clientID';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => '`u`.`clientID`',     'dt' => 0, 'field' => 'clientID'),
	array( 'db' => '`u`.`client_first_name`',     'dt' => 1, 'field' => 'client_first_name'),
	array( 'db' => '`u`.`client_last_name`',     'dt' => 2, 'field' => 'client_last_name'),
	array( 'db' => '`b`.`arrival_date`',     'dt' => 3, 'field' => 'arrival_date'),
	array( 'db' => '`b`.`departure_date`',     'dt' => 4, 'field' => 'departure_date'),
	array( 'db' => '`b`.`hotelbookingID`',     'dt' => 5, 'field' => 'hotelbookingID'),
	array( 'db' => '`b`.`adults`',     'dt' => 6, 'field' => 'adults'),
	array( 'db' => '`b`.`children`',     'dt' => 7, 'field' => 'children'),
	array( 'db' => '`b`.`enfants`',     'dt' => 8, 'field' => 'enfants'),
	array( 'db' => '`b`.`bookingcode`',     'dt' => 9, 'field' => 'bookingcode')
);

// SQL server connection information



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

/*require( 'ssp.class.php' );
$where = "`ApplicationStatus` != '0'";

echo json_encode(
	//SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
    SSPCustom::simpleCustom( $_GET, $sql_details, $table, $primaryKey, $columns, $where )*/
    require( 'ssp.customized.class.php' );
//$where = "";

$joinQuery = "FROM `client` AS `u` LEFT JOIN `booking` AS `b` ON (`u`.`clientID` = `b`.`clientID`)";
$extraWhere = "";  

echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
);
?>