<?php
require_once("connection.php");
// to implement ProjectManager Data Table
// DB table to use
$table = 'booking';

// Table's primary key
$primaryKey = 'bookingID';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => '`u`.`bookingID`',     'dt' => 1, 'field' => 'bookingID'),
	array( 'db' => '`u`.`agentID`',     'dt' => 2, 'field' => 'agentID'),
	//array( 'db' => 'if(`u`.`ClientName` IS NULL ,"N/A",`cl`.`ClientNam`)',     'dt' => 2, 'field' => 'if(`u`.`ClientName` IS NULL ,"N/A",`cl`.`ClientNam`)'),
	//array( 'db' => '`le`.`LegalEntityName`',  'dt' => 3, 'field' => 'LegalEntityName'),
	//array( 'db' => 'if(`u`.`LegalEntity` IS NULL ,"N/A",`le`.`LegalEntityName`)',  'dt' => 3, 'field' => 'if(`u`.`LegalEntity` IS NULL ,"N/A",`le`.`LegalEntityName`)'),
	array( 'db' => '`u`.`arrival_date`',     'dt' => 5, 'field' => 'arrival_date'),
	array( 'db' => '`u`.`departure_date`',     'dt' => 6, 'field' => 'departure_date'),
	//array( 'db' => '`u`.`SentState`',     'dt' => 6, 'field' => 'SentState'),
	//array( 'db' => 'upper(`u`.`Certified`)',     'dt' => 7, 'field' => 'upper(`u`.`Certified`)'),
	//array( 'db' => 'upper(`u`.`FormComplete`)',     'dt' => 8, 'field' => 'upper(`u`.`FormComplete`)'),
	array( 'db' => '`u`.`arrival_flight_time`',     'dt' => 10, 'field' => 'arrival_flight_time'),
	array( 'db' => '`u`.`departure_flight_time`',     'dt' => 11, 'field' => 'departure_flight_time'),
	//array( 'db' => '`u`.`ApplicantAge`',     'dt' => 11, 'field' => 'ApplicantAge'),
	//array( 'db' => '`u`.`date_of_hire`',     'dt' => 12, 'field' => 'date_of_hire'),
	array( 'db' => '`u`.`agentID`',     'dt' => 14, 'field' => 'agentID'),
	array( 'db' => '`e`.`supplierID`',     'dt' => 15, 'field' => 'supplierID')
	//array( 'db' => '`u`.`ApplicantAge`',     'dt' => 12, 'field' => 'ApplicantAge'),
	//array( 'db' => '`u`.`date_of_hire`',     'dt' => 13, 'field' => 'date_of_hire'),
);

// SQL server connection information



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

/*require( 'ssp.class.php' );
$where = "`ApplicationStatus` != '0'";

echo json_encode(
	//SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
    SSPCustom::simpleCustom( $_GET, $sql_details, $table, $primaryKey, $columns, $where )*/
    require( 'ssp.customized.class.php' );
//$where = "";

$joinQuery = "FROM `booking` AS `u` INNER JOIN `eventbooking` AS `e` ON (`u`.`bookingID` = `e`.`bookingID`)";
$extraWhere = "";     

echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
);
?>