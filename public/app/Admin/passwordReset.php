<?php

require_once('includes/connection.php');
$email = test_input($_GET['email']);
$hash = test_input($_GET['hash']);

$stmt01 = $con->prepare("SELECT userID from user where email = ? AND pwreset_token = ?");
$stmt01->bind_param("ss",$email,$hash);
$stmt01->execute();
$stmt01->bind_result($userID);
$stmt01->store_result();
$stmt01->fetch();
$stmt01->close();
$stmt = $con->prepare("UPDATE user set pwreset_token = NULL where userID = ?");
$active = 1;
$stmt->bind_param("i",$userID);
if($stmt->execute())
{
	?>
	
﻿<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>BIJLES AAN HUIS Login</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/css/color_skins.css">
</head>

<body class="theme-green authentication sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
    <div class="container">        
        <div class="navbar-translate n_logo">
           
           
        </div>
        
    </div>
</nav>
<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" ></div>
    <div class="container">
        <div class="col-md-12 content-center">
				
            <div class="card-plain">
				<!-- Error Message -->
			<?php
				require_once("includes/connection.php");
								if(isset($_GET['msg']))
								{
									?>
							<div class="bs-example" style="font-size:10px;">
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo test_input($_GET['msg']); ?>
    </div>
</div>
							<?php
								}
							?>
            
			    <form class="form" method="post" action="actions/passwordReset" onsubmit="return checkPassword(this);">
                    <div class="header">
                        <div class="logo-container">
                        	<h2>BIJLES AAN HUIS</h2>
                            <!--<img src="img/Logo01.png" alt="">-->
                        </div>
                        <h5>Reset Password</h5>
                    </div>
                    <div class="content">                                                
                        <div class="input-group input-lg">
							<input type="hidden" placeholder="Password" class="form-control" name="userID" required maxlength="20" minlength="8" title="" value="<?php echo $userID; ?>" />
                            <input type="password" placeholder="Password" class="form-control" name="password" required maxlength="20" minlength="8" title="Password must contains Minimum 8 characters" id="password"/>
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
						<div class="input-group input-lg">
                            <input type="password" placeholder="Confirm Password" class="form-control" name="cpassword" required maxlength="20" minlength="8" title="Password must contains Minimum 8 characters" id="cpassword"/>
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
						<button type="submit" name="reset" class="btn btn-primary btn-round btn-lg btn-block">RESET PASSWORD</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer">
      
    </footer>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
	
function checkPassword(theForm) {
	if (theForm.password.value != theForm.cpassword.value)
	{
		alert('Password Must Be Same As Confirm Password');
		return false;
	} else {
		return true;
	}
}
</script>
</body>
</html>
<?php
}
else
{
	header('Location: expiredLink.php');
}

?>