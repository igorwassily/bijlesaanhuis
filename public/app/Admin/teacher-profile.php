<?php
  require_once('includes/connection.php');
$thisPage="My Profile";
session_start();

if(!isset($_SESSION['AdminID']))
{
	header('Location: index.php');
}
else
{
     // if(!isset($_GET['tid']))
     //    header('Location: index.php');

    $query = "SELECT * FROM `contact`c, teacher t LEFT JOIN `profile` p ON p.profileID=t.profileID WHERE c.userID=t.userID AND c.userID=$_GET[tid] LIMIT 1";

    $result = mysqli_query($con, $query);
    if(!$result)
        header('Location: index.php');


    $d = mysqli_fetch_assoc($result);
?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Teacher Profile</title>
<?php
		require_once('includes/connection.php');
        require_once('../includes/mainCSSFiles.php');
?>
 <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="calendar/css/calendar.css">

    <style type="text/css">
    #calendar { background-color:#fafafa !important; }
    .cal-month-day{color:black;}
    .cal-row-head {background-color: #fafafa !important;}
    .empty {  background-color: white !important; border: 2px solid black;}    
    .inline_time { color: #50d38a !important; }  
    .inline_time_disabled { color: #6b7971 !important; }
    #cal-slide-content{font-family:inherit; color:#bdbdbd;}
    .page-header{height: 0 !important;padding-bottom: 0px;margin: -25px 0 50px;border-bottom: 0px;}
    .page-calendar #calendar{max-width: 100%;}
		
.input-group{
    background-color: transparent !important;
}
.input-group-focus span{background-color: transparent !important;}
.input-group-focus #chatsender{     
    border:1px solid #ff5200c2 !important;
}

    </style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php 
        // require_once('includes/header.php');
        	require_once('includes/sidebarAdminDashboard.php');
			
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
   <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-xl-6 col-lg-7 col-md-12">
                <div class="card profile-header">
                    <div class="header">
                        <h2><strong>Personal</strong> Info</h2>
                    </div>
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-12">
                                <div class="profile-image float-md-right"> <img src="../<?php echo ($d['image'])? $d['image'] : DEFAULT_PIC; ?>" alt=""> </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <h4 class="m-t-0 m-b-0"><strong><?php echo $d['firstname']; ?></strong> <?php echo $d['lastname']; ?></h4>
                                <!--<span class="job_post">Ui UX Designer</span>-->
                                <p><?php echo $d['place_name']; ?><?php echo $d['country']; ?></p><br/><br/>
								<div>
                                   <?php if(!isset($_SESSION['Teacher'])){ ?>
                                    <button id="sendMsg" class="btn btn-primary btn-round btn-simple" data-type="prompt">Send Message</button>
									<?php }	?>
                                </div>
                            </div>                
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="col-xl-6 col-lg-5 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2><strong>SHORT</strong> BIO</h2>
                    </div>
                    <div class="body">
                        <?php echo $d['shortdescription']; ?>
                    </div>
                </div>
            </div>
        </div>
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12">
                <div class="card">
                    <div class="body">
                        <div class="calendar-page-header page-header">
                            <div class="pull-right form-inline">
                                <div class="btn-group">
                                    <button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
                                    <button class="btn btn-default" data-calendar-nav="today">Today</button>
                                    <button class="btn btn-primary" data-calendar-nav="next">Next >></button>
                                </div>
                            </div>
                            <h3></h3>
                        </div>
                        <div id="calendar"></div>                        
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
	<div class="modal modal-fullscreen fade" id="modal-delete-course" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		  <h6 class="modal-title" id="myModalLabel"><b class="popupMsg">Send Message to Teacher</b></h6>
      </div>
      <div class="modal-body">
        <div id="deresultcourse"></div>
                          <input type="hidden" id="tid" name="tid" value="<?php echo $_POST['tid']; ?>" />
                      <div class="form-group">
						  <div class="col-md-12 col-sm-12 col-xs-12" >
							  <div class="chat-message clearfix">
                            <div class="input-group p-t-15">
                                <input type="text" class="form-control" id="chatsender" style="height:40px;" autocomplete="off" placeholder="Enter text here..." autofocus>
                                <span class="input-group-addon">
                                    <i class="zmdi zmdi-mail-send"></i>
                                </span>
                            </div>                          
                        </div>
						  </div>
                      </div>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<?php
    require_once('includes/footerScriptsAddForms.php');
?>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
    <script type="text/javascript" src="calendar/js/calendar.js"></script>
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->

<script>
     // this function is added
Date.prototype.setTimezoneOffset = function(minutes) { 
    var _minutes;
    if (this.timezoneOffset == _minutes) {
        _minutes = this.getTimezoneOffset();
    } else {
        _minutes = this.timezoneOffset;
    }
    if (arguments.length) {
        this.timezoneOffset = minutes;
    } else {
        this.timezoneOffset = minutes = this.getTimezoneOffset();
    }
    return this.setTime(this.getTime() + (_minutes - minutes) * 6e4);
};


 var calendar;
 var options;

    var teacherid = <?php echo @$_GET['tid']; ?>;
    var hourlyCost = <?php echo (@$data['customhourly'])? @$data['customhourly'] : 0; ?>;
	var std_lat = "<?php echo @$_SESSION['std_lat']; ?>";
	var std_lng = "<?php echo @$_SESSION['std_lng']; ?>";
	var place_name = "<?php echo @$_SESSION['std_place_name']; ?>";
    
    var calendar;
    var options;

  $(document).ready(function() {

	  $("#sendMsg").click(function(){
          $('#modal-delete-course').appendTo("body").modal('show');		  
		  $("#chatsender").val("");	  
		  
	  });
	  
	  $(".cal-month-day").dblclick(function(e){e.preventDefault();});
  
  	$(".page-loader-wrapper").css("display", "none");
	  
    "use strict";

    options = {
        events_source: 'admin-ajaxcalls.php?teacherdashboard=3432e&ttid=<?php echo $_GET['tid'];?>',
        view: 'month',
        tmpl_path: 'calendar/tmpls/',
        tmpl_cache: false,
         // day: '2013-03-21',
        // day: 'now',

        merge_holidays: false,
        display_week_numbers: false,
        weekbox: false,
        //shows events which fits between time_start and time_end
        show_events_which_fits_time: true,

        onAfterEventsLoad: function(events) {
            if(!events) {
                return;
            }
            var list = $('#eventlist');
            list.html('');

            $.each(events, function(key, obj) {
                //converting the timeZone to UTC
                obj.start = new Date(parseInt(obj.start)).setTimezoneOffset(0);
                obj.end = new Date(parseInt(obj.end)).setTimezoneOffset(0);

                $(document.createElement('li'))
                    .html('<a href="' + obj.url + '">' + obj.title + '</a>')
                    .appendTo(list);
            });
        },
        onAfterViewLoad: function(view) {
            $('.page-header h3').text(this.getTitle());
            $('.btn-group button').removeClass('active');
            $('button[data-calendar-view="' + view + '"]').addClass('active');
        },
        classes: {
            months: {
                general: 'label'
            }
        }
    };

     calendar = $('#calendar').calendar(options);

    $('.btn-group button[data-calendar-nav]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.navigate($this.data('calendar-nav'));
        });
    });

    $('.btn-group button[data-calendar-view]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.view($this.data('calendar-view'));
        });
    });

    $('#first_day').change(function(){
        var value = $(this).val();
        value = value.length ? parseInt(value) : null;
        calendar.setOptions({first_day: value});
        calendar.view();
    });

    $('#language').change(function(){
        calendar.setLanguage($(this).val());
        calendar.view();
    });

    $('#events-in-modal').change(function(){
        var val = $(this).is(':checked') ? $(this).val() : null;
        calendar.setOptions({modal: val});
    });
    $('#format-12-hours').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({format12: val});
        calendar.view();
    });
    $('#show_wbn').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({display_week_numbers: val});
        calendar.view();
    });
    $('#show_wb').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({weekbox: val});
        calendar.view();
    });
    $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
        //e.preventDefault();
        //e.stopPropagation();
    });

    $(".cal-month-day").on("click",function(){
        console.log($(this).find(".cal-slide-content").html());
  
    });
	

    $(".list-item").on("mouseover",function(e){
        e.preventDefault(); 
        console.log($(this).html());
    });
	  
	  $("#chatsender").keyup(function(e){

        if(e.keyCode != 13)
            return;

        var msg = $(this).val();
        var subj = 'null';

        $.ajax({
            type: "POST", 
            url: "../includes/chat-ajax.php",
            data: { filename: "chat_text_msg_by_sender", msg: msg, senderId:"<?php echo $_SESSION['StudentID']; ?>", recvId:"<?php echo $_GET['tid']; ?>", subj:subj},
            dataType: "text",
            success: function(response) { 
                var res = JSON.parse(response);
                if(res.status == "success"){
                          showNotification("alert-success", "Message Sent !", "bottom", "center", "", "");
                }else{					
                          showNotification("alert-danger", "Something Went Wrong ! Try Again.", "bottom", "center", "", "");
				}
				$(".close").click();
            },
            error: function(xhr, ajaxOptions, thrownError) { 
                console.log("error", xhr.responseText); 
                if(xhr.responseText == "success"){
                          showNotification("alert-success", "Message Sent !", "bottom", "center", "", "");
                }else{					
                          showNotification("alert-danger", "Something Went Wrong ! Try Again.", "bottom", "center", "", "");
				}
				$(".close").click();
            }
        });  // ends ajax
    });     // () chatsender ends

  }); //main $()


  function removeButton(obj){
    $(obj).find(".inline-elem-buttons-box").remove();
  }


  function addButton(obj, id){
    console.log("addButton : ", obj);
    var cases = $(obj).attr("data-event");
    // if(cases == "empty"){
    //     $(obj).append('');
    // }
     $(obj).append('<span class="inline-elem-buttons-box">\
   <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, '+id+')">Cancel Slot</button>\
</span>');

  }// addButton function


  function cancelSlot(obj, id){
      $.ajax({
        type: "POST", 
        url: "../student-ajaxcall.php",
        data: { subpage:"slotCancellation", cbID: id},
        dataType: "json",
        success: function(response) { 
                      if(response == "success"){
                          console.log(response); 
                          showNotification("alert-success", "Slot Booking is Cancelled", "bottom", "center", "", "");
                       calendar = $('#calendar').calendar(options);
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                  },
        error: function(xhr, ajaxOptions, thrownError) { 
                      if(xhr.responseText == "success"){
                          console.log(xhr.responseText); 
                          showNotification("alert-success", "Slot Booking is Cancelled", "bottom", "center", "", "");
                       calendar = $('#calendar').calendar(options);
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);

                      }
      });// ends ajax
  }

  function booking(){

     $(document).css({'cursor' : 'wait'});
     $("#submitBtn").prop("disabled", true);    

    var slotData = "";
    var time = "";
    var date = "";
	  	
    var isFirst = false;
	var isLast = false;
	 
	var preObj = null;
	var nextObj = null;
	  
	var selectedSlotsNum = 0;
	var slotTime = Array();
	var slotDetail = Array();
	var milliseconds = 0;
	  
	var min_slot = 3;
	var slot_count = 0 ;
	var iserror = false;
	  
	  
    $("#cal-slide-content").find(".inline-elem-check").each(function(index, obj){
		
       if($(this).prop("checked")){			
		  slotTime.push({isGapSlot:0,  time:$(this).attr("data-time") , date:$(this).attr("data-date")});
		  selectedSlotsNum++;
			isFirst = true;
        }
		
		if(isFirst && !$(obj).prop("checked")){
			if(preObj)
		  		slotTime.push({isGapSlot:1, time:$(preObj).attr("data-time") , date:$(preObj).attr("data-date")});
		  	slotTime.push({isGapSlot:1, time:$(this).attr("data-time") , date:$(this).attr("data-date")});
			
			milliseconds = window.performance.now();
			
			slotDetail.push({slotGID:(teacherid+ milliseconds), slotDetail:slotTime});		
			
			if(selectedSlotsNum < min_slot){
				iserror = true;
				return;
			}
			   
			slotTime= [];
			isFirst = false;
			selectedSlotsNum=0;
		}
		
		if(!isFirst)
			preObj = obj;
    });
	  if(isFirst && !iserror){
			if(preObj)
		  		slotTime.push({isGapSlot:1,  time:$(preObj).attr("data-time") , date:$(preObj).attr("data-date")});
		  
			milliseconds = window.performance.now();			
			slotDetail.push({slotGID:(teacherid+ milliseconds), slotDetail:slotTime});		
		  
		  if(selectedSlotsNum < min_slot){
				iserror = true;
			}
		}
	  
	  	if(iserror){	
			showNotification("alert-danger", "Minimum 45mins consective slots can be booked", "bottom", "center", "", "");
			return;
		}
	  
	  console.log(slotDetail);
	  
	  
      $.ajax({
        type: "POST", 
        url: "../student-ajaxcall.php",
        data: { subpage:"slotBooking", teacherid: teacherid, slotData:slotDetail, hourlyCost:hourlyCost, std_lat:std_lat, std_lng:std_lng, place_name:place_name},
        dataType: "json",
        success: function(response) { 
                      if(response == "success"){
                          console.log(response); 
                          showNotification("alert-success", "Request has been sent", "bottom", "center", "", "");
                       calendar = $('#calendar').calendar(options);
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                  },
        error: function(xhr, ajaxOptions, thrownError) { 
                      if(xhr.responseText == "success"){
                          console.log(xhr.responseText); 
                          showNotification("alert-success", "Request has been sent", "bottom", "center", "", "");
                       calendar = $('#calendar').calendar(options);
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
		 }// func ends 
      });// ends ajax
  }
</script>

</body>
</html>
<?php
}
?>