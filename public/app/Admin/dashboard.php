<?php
$thisPage = "Dashboard";
session_start();
if (!isset($_SESSION['AdminUser'])) {
	header('Location: index.php');
} else {
	?>

    <!doctype html>
    <html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
        <title>Admin Dashboard</title>
		<?php
		require_once('includes/connection.php');
		require_once('includes/mainCSSFiles.php');
		?>
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
              rel="stylesheet"/>
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">

        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="../calendar/css/calendar.css">
        <style type="text/css">
            #calendar {
                background-color: #F1F1F1 !important;
            }

            .cal-month-day {
                color: black;
            }

            .cal-row-head {
                background-color: #fafafa !important;
            }

            .empty {
                background-color: white !important;
                border: 2px solid black;
            }

            .inline_time {
                color: #50d38a !important;
            }

            .inline_time_disabled {
                color: #6b7971 !important;
            }

            #cal-slide-content {
                font-family: inherit;
                color: #bdbdbd;
            }

            .page-header {
                height: 0 !important;
                padding-bottom: 0px;
                margin: -25px 0 50px;
                border-bottom: 0px;
            }
        </style>

        <style type="text/css">

            /*Placeholder Color */
            input {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            select {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            input:focus {
                background: transparent !Important;
            }

            select:focus {
                background: transparent !Important;
            }

            .wizard .content {
                /*overflow-y: hidden !important;*/
            }

            .wizard .content label {

                color: white !important;

            }

            .wizard > .steps .current a {
                background-color: #029898 !Important;
            }

            .wizard > .steps .done a {
                background-color: #828f9380 !Important;
            }

            .wizard > .actions a {
                background-color: #029898 !Important;
            }

            .wizard > .actions .disabled a {
                background-color: #eee !important;
            }

            .btn.btn-simple {
                border-color: white !important;
            }

            .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
                color: white;
            }

            table {
                color: white;
            }

            .multiselect.dropdown-toggle.btn.btn-default {
                display: none !important;
            }

            .navbar.p-l-5.p-r-5 {
                display: none !important;
            }

        </style>
		<?php
		$activePage = basename($_SERVER['PHP_SELF']);
		?>
    </head>
    <body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                     alt="Oreo"></div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>


	<?php
	require_once('includes/header.php');
	require_once('includes/sidebarAdminDashboard.php');
	?>

    <!-- Main Content -->
    <section class="content page-calendar" style="margin-top: 0px !important;">
        <div class="block-header">
			<?php require_once('includes/adminTopBar.php'); ?>
        </div>
        <div class="container-fluid">
            <div class="card">
                <div class="row">
                    <div class="col-12">
                        <div class="body">
                            <div class="calendar-page-header page-header">
                                <div class="pull-right form-inline">
                                    <div class="btn-group">
                                        <button class="btn btn-primary" data-calendar-nav="prev"><<</button>
                                        <button class="btn btn-default" data-calendar-nav="today">Vandaag</button>
                                        <button class="btn btn-primary" data-calendar-nav="next">>></button>
                                    </div>
                                </div>
                                <h3></h3>
                            </div>
                            <div id="calendar" style="width: 80%; margin: auto"></div>
                        </div>
                    </div>
                    <!-- <div class="col-md-4 col-lg-4 col-xl-4">
                    <div class="body">
						
                        <h4>Upcoming Interviews</h4>
                        <?php
					$isEmpty = true;
					$i = 1;
					$limit = 4;
					while ($row = mysqli_fetch_assoc($result)) {
						print_r($row);
						$color = ($row['accepted'] == 1) ? "b-primary" : "b-warning";
						$day = date("d", strtotime($row['datee']));
						$m = date("M", strtotime($row['datee']));
						$year = date("Y", strtotime($row['datee']));

						echo '<hr/><div class="event-name ' . $color . ' row">
                                        <div class="col-3 text-center">
                                            <h4>' . $day . '<span>' . $m . '</span><span>' . $year . '</span></h4>
                                        </div>
                                        <div class="col-9">
                                            <h6><a href="teacher-profile?tid=' . $row['teacherID'] . '">' . $row['firstname'] . " " . $row['lastname'] . '</h6>
                                            <address><i class="zmdi zmdi-time"></i> ' . $row['starttime'] . " - " . $row['endtime'] . '</address>
                                            <address><i class="zmdi zmdi-pin"></i>' . $row['place_name'] . '</address>
                                        </div>
                                    </div>';
						$isEmpty = false;
						$i++;
						if ($i > $limit) {
							echo "<hr/> <p style='text-align:right;'> <a href='admin-appointment'>VIEW ALL</a></p>";
							break;
						}
					}

					if ($isEmpty) {
						echo '<hr/> <p style="text-align:center;">No Upcoming Appointments</p>';
					}
					?>
                    </div>
                </div> -->
                </div>


            </div>
        </div>
    </section>

    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


	<?php
	require_once('includes/footerScriptsAddForms.php');

	?>


    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
    <script type="text/javascript" src="../calendar/js/calendar.js"></script>
    <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
    <script src="assets/js/pages/ui/notifications.js"></script>

    <script>
        // this function is added
        Date.prototype.setTimezoneOffset = function (minutes) {
            var _minutes;
            if (this.timezoneOffset == _minutes) {
                _minutes = this.getTimezoneOffset();
            } else {
                _minutes = this.timezoneOffset;
            }
            if (arguments.length) {
                this.timezoneOffset = minutes;
            } else {
                this.timezoneOffset = minutes = this.getTimezoneOffset();
            }
            return this.setTime(this.getTime() + (_minutes - minutes) * 6e4);
        };


        var calendar;
        var options;

        var calendar_day;
        var calandar_view;

        $(document).ready(function () {


            $(".page-loader-wrapper").css("display", "none");


            "use strict";
            let url = "/api/Calendar/GetFreeSlotsConsultation";
            options = {
                events_source: url,
                view: 'month',
                tmpl_path: 'calendar/tmpls/',
                tmpl_cache: false,
                // day: 'now',

                merge_holidays: false,
                display_week_numbers: false,
                weekbox: false,
                //shows events which fits between time_start and time_end
                show_events_which_fits_time: true,

                onAfterEventsLoad: function (events) {
                    if (!events) {
                        return;
                    }
                    var list = $('#eventlist');
                    list.html('');

                    $.each(events, function (key, obj) {
                        //converting the timeZone to UTC
                        obj.start = new Date(parseInt(obj.start)).setTimezoneOffset(0);
                        obj.end = new Date(parseInt(obj.end)).setTimezoneOffset(0);

                        $(document.createElement('li'))
                            .html('<a href="' + obj.url + '">' + obj.title + '</a>')
                            .appendTo(list);
                    });
                },
                onAfterViewLoad: function (view) {
                    $('.page-header h3').text(this.getTitle());
                    $('.btn-group button').removeClass('active');
                    $('button[data-calendar-view="' + view + '"]').addClass('active');
                },
                classes: {
                    months: {
                        general: 'label'
                    }
                }
            };

            calendar = $('#calendar').calendar(options);

            $('.btn-group button[data-calendar-nav]').each(function () {
                var $this = $(this);
                $this.click(function () {
                    calendar.navigate($this.data('calendar-nav'));
                });
            });


            $('#first_day').change(function () {
                var value = $(this).val();
                value = value.length ? parseInt(value) : null;
                calendar.setOptions({first_day: value});
                calendar.view();
            });





        }); //main $()

        function triggerAbsent(e, isChecked) {
            let url = '';

            if (isChecked) {
                url = '/api/Calendar/RemoveAbsentDay';
            } else {
                url = '/api/Calendar/AddAbsentDay';
            }

			 let day = e.data('date');

			 $.ajax({
				 type: "POST",
				 url: url,
				 data: {teacherID: 1, date: day},
				 dataType: "json",
				 success: function (response) {
					 showNotification("alert-success", "Success.", "bottom", "center", "", "");
					 setTimeout(function () {
						 document.location.reload();
					 }, "1000");
				 },
				 error: function (xhr, ajaxOptions, thrownError) {
					 showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
				 }
			 });// ends ajax

        }

        function removeButton(obj) {
            $(obj).find(".inline-elem-buttons-box").remove();
        }


        function addButton(obj, id, name) {
            console.log("addButton : ", obj);
            var cases = $(obj).attr("data-event");
            var btn = '';

            if (name == 0) {
                btn = '<span class="inline-elem-buttons-box">\
               <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, ' + id + ', ' + name + ')">Cancel Slot</button>\
            </span>';
            } else if (name == 1) {
                btn = '<span class="inline-elem-buttons-box">\
               <button type="button" class="inline-elem-button btn btn-raised btn-primary btn-round waves-effect" value="confirm" onclick="confirmSlot(this, ' + id + ', ' + name + ')">Confirm</button> \
               <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, ' + id + ', ' + name + ')">Cancel Slot</button>\
            </span>';
            } else if (name == 2) {
                btn = '<span class="inline-elem-buttons-box">\
               <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, ' + id + ', ' + name + ')">Cancel Slot</button>\
            </span>';
            } else if (name == 3) {
                btn = '<span class="inline-elem-buttons-box">\
               <button type="button" class="inline-elem-button btn btn-raised btn-primary btn-round waves-effect" value="cancel" onclick="confirmSlot(this, ' + id + ', ' + name + ')">Reactivate Slot</button>\
            </span>';
            }

            $(obj).append(btn);

        }// addButton function


        function confirmSlot(obj, id, name) {
            var data_opt;
            if (name == 1)
                data_opt = {subpage: "confrimPending", cbID: id};
            else if (name == 3)
                data_opt = {subpage: "reactivatefreeSlot", slotid: id};
            else
                return;


            $.ajax({
                type: "POST",
                url: "admin-ajaxcalls.php",
                data: data_opt,
                dataType: "json",
                success: function (response) {
                    if (response == "success") {
                        console.log(response);
                        showNotification("alert-success", "Booking is confirmed", "bottom", "center", "", "");
                    } else {
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                    $(document.body).css({'cursor': 'default'});
                    $("#submitBtn").prop("disabled", false);
                    calendar = $('#calendar').calendar(options);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (xhr.responseText == "success") {
                        console.log(xhr.responseText);
                        showNotification("alert-success", "Booking is confirmed", "bottom", "center", "", "");
                    } else {
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                    $(document.body).css({'cursor': 'default'});
                    $("#submitBtn").prop("disabled", false);
                    calendar = $('#calendar').calendar(options);

                }
            });// ends ajax
        }

        function cancelSlot(obj, id, name) {
            var subpage, cbID = '';
            if (name == 0) {
                subpage = "cancelBook";
                cbID = id;
            } else if (name == 1) {
                subpage = "cancelPending";
                cbID = id;
            } else if (name == 2) {
                subpage = "cancelfreeSlot";
                cbID = id;
            }


            var elem = $(obj).parent().parent();
            var ddate = elem.find(".event-item").attr("data-date");
            var time = elem.find(".inline_time").text();
            time = (time.trim()).split(" ");
            var stime = time[0] + " " + time[1];
            var etime = time[3] + " " + time[4];

            $.ajax({
                type: "POST",
                url: "admin-ajaxcalls.php",
                data: {subpage: subpage, cbID: cbID, ddate: ddate, stime: stime, etime: etime},
                dataType: "json",
                success: function (response) {
                    if (response == "success") {
                        console.log(response);
                        showNotification("alert-success", "Slot Booking is Cancelled", "bottom", "center", "", "");
                    } else {
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                    $(document.body).css({'cursor': 'default'});
                    $("#submitBtn").prop("disabled", false);
                    calendar = $('#calendar').calendar(options);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (xhr.responseText == "success") {
                        console.log(xhr.responseText);
                        showNotification("alert-success", "Slot Booking is Cancelled", "bottom", "center", "", "");
                    } else {
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                    $(document.body).css({'cursor': 'default'});
                    $("#submitBtn").prop("disabled", false);
                    calendar = $('#calendar').calendar(options);

                }
            });// ends ajax
        }

        function booking() {

            $(document).css({'cursor': 'wait'});
            $("#submitBtn").prop("disabled", true);

            var slotData = "";
            var stime = $(this).attr("data-stime");
            var etime = $(this).attr("data-etime");
            var date = $(this).attr("data-date");

            $("#cal-slide-content ").find(".inline-elem-check").each(function (index, obj) {
                if ($(this).prop("checked")) {
                    var stime = $(this).attr("data-stime");
                    var etime = $(this).attr("data-etime");
                    var date = $(this).attr("data-date");
                    slotData += $(this).val() + "," + date + "," + stime + "," + etime + "&";
                }
            });

            slotData = slotData.substring(0, slotData.length - 1);

            $.ajax({
                type: "POST",
                url: "admin-ajaxcalls.php",
                data: {subpage: "slotBooking", teacherid: teacherid, slotData: slotData, hourlyCost: hourlyCost},
                dataType: "json",
                success: function (response) {
                    if (response == "success") {
                        console.log(response);
                        showNotification("alert-success", "Request has been sent", "bottom", "center", "", "");
                    } else {
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                    $(document.body).css({'cursor': 'default'});
                    $("#submitBtn").prop("disabled", false);
                    calendar = $('#calendar').calendar(options);
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    if (xhr.responseText == "success") {
                        console.log(xhr.responseText);
                        showNotification("alert-success", "Request has been sent", "bottom", "center", "", "");
                    } else {
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                    $(document.body).css({'cursor': 'default'});
                    $("#submitBtn").prop("disabled", false);
                    calendar = $('#calendar').calendar(options);

                }
            });// ends ajax

        }// func ends

    </script>
    </body>
    </html>
	<?php
}
?>