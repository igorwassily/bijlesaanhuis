<?php
$thisPage="View Courses";
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>View Courses</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
		color: black !Important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	
	table, th
	{
		text-align: center !important;
	}
	select.form-control.form-control-sm:hover {
    color: black !important;
}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="card">
			<div class="row">
				 <div class="col-md-12 col-lg-12 col-xl-12">
				   <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Manage School Type</strong></h2>
                        
                    </div>
					   <div class="body">
						   <div class="row">
						   <div class="col-md-3 col-lg-3 col-xl-3">
						   </div>
						   
						   <div class="col-md-6 col-lg-6 col-xl-6">
						    <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="view-schooltypes" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th class="color">School Type ID</th>
                                        <th class="color">School Type Name</th>
                                     
                                        <th class="color">Delete</th>
                                        
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
											$stmt = $con->prepare("SELECT * from schooltype");
					$stmt->execute();
					$stmt->bind_result($id,$name);
					$stmt->store_result();
					while($stmt->fetch())
					{
	
									?>
									<tr>
									
										<td class="color"><?php echo $id; ?></td>
									<td class="color"><?php echo $name; ?></td>
									
										<td class="color">
									<b><a href="javascript:;" class="delete-record-type" data-id="<?php echo $id; ?>" style="color: #5cc75f !important;"> Delete </a></b>  
										</td>
									</tr>
									<?php
					}
	?>
                                </tbody>
                            </table>
                        </div>
						   </div>
						   <div class="col-md-3 col-lg-3 col-xl-3">
						    
								</div>
						
						   </div>
						   
						 
					   </div>
                </div>
			
            </div>
			</div>
            <div class="row">
				 <div class="col-md-12 col-lg-12 col-xl-12">
				   <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Manage School Level</strong></h2>
                        
                    </div>
					   <div class="body">
						   <div class="row">
						   <div class="col-md-3 col-lg-3 col-xl-3">
						   </div>
						   
						   <div class="col-md-6 col-lg-6 col-xl-6">
						    <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="view-schoollevels" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th class="color">ID</th>
                                        <th class="color">Name</th>
                                        
                                        <th class="color">Delete</th>
                                        
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
											$stmt = $con->prepare("SELECT * from schoollevel");
					$stmt->execute();
					$stmt->bind_result($id,$name);
					$stmt->store_result();
					while($stmt->fetch())
					{
	
									?>
									<tr>
									
										<td class="color"><?php echo $id; ?></td>
									<td class="color"><?php echo $name; ?></td>
									
										<td>
									<b><a href="javascript:;" class="delete-record-level" data-id="<?php echo $id; ?>" style="color: #5cc75f !important;"> Delete </a></b>  
										</td>
									</tr>
									<?php
					}
	?>
                                </tbody>
                            </table>
                        </div>
						   </div>
						   <div class="col-md-3 col-lg-3 col-xl-3">
						    
								</div>
						
						   </div>
						   
						 
					   </div>
                </div>
			
            </div>
			</div>
			<div class="row">
				 <div class="col-md-12 col-lg-12 col-xl-12">
				   <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Manage School Year</strong></h2>
                        
                    </div>
					   <div class="body">
						   <div class="row">
						   <div class="col-md-3 col-lg-3 col-xl-3">
						   </div>
						   
						   <div class="col-md-6 col-lg-6 col-xl-6">
						    <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="view-schoolyears" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th class="color">ID</th>
                                        <th class="color">Name</th>
                                        
                                        <th class="color">Delete</th>
                                        
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
											$stmt = $con->prepare("SELECT * from schoolyear");
					$stmt->execute();
					$stmt->bind_result($id,$name);
					$stmt->store_result();
					while($stmt->fetch())
					{
	
									?>
									<tr>
									
										<td class="color"><?php echo $id; ?></td>
									<td class="color"><?php echo $name; ?></td>
									
										<td>
									<b><a href="javascript:;" class="delete-record-year" data-id="<?php echo $id; ?>" style="color: #5cc75f !important;"> Delete </a></b>  
										</td>
									</tr>
									<?php
					}
	?>
                                </tbody>
                            </table>
                        </div>
						   </div>
						   <div class="col-md-3 col-lg-3 col-xl-3">
						    
								</div>
						
						   </div>
						   
						 
					   </div>
                </div>
			
            </div>
			</div>
			<div class="row">
				 <div class="col-md-12 col-lg-12 col-xl-12">
				   <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>Manage School Courses</strong></h2>
                        
                    </div>
					   <div class="body">
						   <div class="row">
						   <div class="col-md-3 col-lg-3 col-xl-3">
						   </div>
						   
						   <div class="col-md-6 col-lg-6 col-xl-6">
						    <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="view-schoolyears" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th class="color">ID</th>
                                        <th class="color">Name</th>                                        
                                        <th class="color">Delete</th>
                                        
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
											$stmt = $con->prepare("SELECT * from schoolcourse");
											$stmt->execute();
											$stmt->bind_result($id,$name);
											$stmt->store_result();
											while($stmt->fetch())
											{
	
									?>
									<tr>
									
										<td class="color"><?php echo $id; ?></td>
										<td><input style="background: #373f46;" class="courseName<?php echo $id; ?>" type="taxt" id="<?php echo $id; ?>" value="<?php echo $name; ?>" disabled="disabled"></td>
									
										<td>
									<b><a href="javascript:;" class="delete-record-scourse d<?php echo $id; ?>" data-id="<?php echo $id; ?>" style="color: #5cc75f !important;"> Delete | </a></b> 
							<b><a href="javascript:;" class="edit-record-scourse e<?php echo $id; ?>" data-id="<?php echo $id; ?>" style="color: #5cc75f !important;"> Edit </a></b> 
											
						<b><a href="javascript:;" style="display:none" class="update-record-scourse u<?php echo $id; ?>" data-id="<?php echo $id; ?>" style="color: #5cc75f !important;"> Update | </a></b> 
							<b><a href="javascript:;" style="display:none" class="cancel-record-scourse c<?php echo $id; ?>" data-id="<?php echo $id; ?>" style="color: #5cc75f !important;"> Cancel </a></b> 											
											
										</td>
									</tr>
									<?php
					}
	?>
                                </tbody>
                            </table>
                        </div>
						   </div>
						   <div class="col-md-3 col-lg-3 col-xl-3">
						    
								</div>
						
						   </div>
						   
						 
					   </div>
                </div>
			
            </div>
			</div>
		<div class="row">
				 <div class="col-md-12 col-lg-12 col-xl-12">
				   <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2><strong>School Courses Associated level and years</strong></h2>
                        
                    </div>
					   <div class="body">
						   <div class="row">
						   <div class="col-md-2 col-lg-2 col-xl-2">
						   </div>
						   
						   <div class="col-md-8 col-lg-8 col-xl-8">
						    <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="view-schoolcourses" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>ID</th>
                                        <th>Name</th>
										<th>School Type</th>
										<th>School Level</th>
										<th>School Year</th>
                                        
                                        <th>Delete</th>
                                        
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
											$stmt = $con->prepare("select courses.courseID, courses.coursename,  (SELECT GROUP_CONCAT(typeName) FROM schooltype WHERE FIND_IN_SET(typeID, courses.coursetype))typeName,  (SELECT GROUP_CONCAT(schoollevel.levelName) FROM schoollevel WHERE FIND_IN_SET(levelID, courses.courselevel))levelName, (SELECT GROUP_CONCAT(schoolyear.yearName) FROM schoolyear WHERE FIND_IN_SET(yearID, courses.courseyear))yearName
from courses ORDER BY coursename");
					$stmt->execute();
					$stmt->bind_result($id,$name,$type,$level,$year);
					$stmt->store_result();
					while($stmt->fetch())
					{
	
									?>
									<tr>
									
										<td class="color"><?php echo $id; ?></td>
									<td class="color"><?php echo $name; ?></td>
										<td class="color"><?php echo $type; ?></td>
									<td class="color"><?php echo $level; ?></td>
										<td class="color"><?php echo $year; ?></td>
									
										<td>
									<b><a href="javascript:;" class="delete-record-course" data-id="<?php echo $id; ?>" style="color: #5cc75f !important;"> Delete </a></b>  
										</td>
									</tr>
									<?php
					}
	?>
                                </tbody>
                            </table>
                        </div>
						   </div>
						   <div class="col-md-2 col-lg-2 col-xl-2">
						    
								</div>
						
						   </div>
						   
						 
					   </div>
                </div>
			
            </div>
			</div>
        </div>
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
	<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
		<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
<?php
    require_once('includes/footerScripts.php');
?>
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>

<script>

  $(document).ready(function() {
	  
	  $('#view-schooltypes').DataTable();
	   $('#view-schoollevels').DataTable();
	   $('#view-schoolyears').DataTable();
	  $('#view-schoolcourses').DataTable();
	  
	  
	  /* Delete Modal Scripts */
	  $( document ).on( "click", "a.delete-record-type", function(e) {
        run_waitMe($('#view-schooltypes'), 1, "timer");
            var id = $(this).attr("data-id");
//  alert(id);          
            
                                                 $('#view-schooltypes').waitMe('hide');
                                                 $("#delete-id-type").val(id);
                                                  //$("#modal-delete").modal('show');    
                                                  $('#modal-delete-type').appendTo("body").modal('show');                                                              
        });
	  
	  /* Delete Modal Scripts */
	  $( document ).on( "click", "a.delete-record-level", function(e) {
        run_waitMe($('#view-schoollevels'), 1, "timer");
            var id = $(this).attr("data-id");
//  alert(id);          
            
                                                 $('#view-schoollevels').waitMe('hide');
                                                 $("#delete-id-level").val(id);
                                                  //$("#modal-delete").modal('show');    
                                                  $('#modal-delete-level').appendTo("body").modal('show');                                                              
        });
	  
	  /* Delete Modal Scripts */
	  $( document ).on( "click", "a.delete-record-year", function(e) {
        run_waitMe($('#view-schoolyears'), 1, "timer");
            var id = $(this).attr("data-id");
//  alert(id);          
            
                                                 $('#view-schoolyears').waitMe('hide');
                                                 $("#delete-id-year").val(id);
                                                  //$("#modal-delete").modal('show');    
                                                  $('#modal-delete-year').appendTo("body").modal('show');                                                              
        });
	  
	  $( document ).on( "click", "a.delete-record-course", function(e) {
        run_waitMe($('#view-schoolcourses'), 1, "timer");
            var id = $(this).attr("data-id");
//  alert(id);          
            
                                                 $('#view-schoolcourses').waitMe('hide');
                                                 $("#delete-id-course").val(id);
                                                  //$("#modal-delete").modal('show');    
                                                  $('#modal-delete-course').appendTo("body").modal('show');                                                              
        });
	  
	  
	  
	  $( document ).on( "click", "a.edit-record-scourse", function(e) {
		
	  	   var id = $(this).attr("data-id");
		   $("#"+id).attr("disabled", false);
		  $("#"+id).focus();
		  $("a.d"+id).css("display", "none");
		  $("a.e"+id).css("display", "none");
		  $("a.c"+id).css("display", "inline");
		  $("a.u"+id).css("display", "inline");
	  });
	  
	  $( document ).on( "click", "a.cancel-record-scourse", function(e) {
		var id = $(this).attr("data-id");
		  $("#"+id).attr("disabled", true);
		  $("a.d"+id).css("display", "inline");
		  $("a.e"+id).css("display", "inline");
		  $("a.c"+id).css("display", "none");
		  $("a.u"+id).css("display", "none");		  
	  });
	  
	  $( document ).on( "click", "a.update-record-scourse", function(e) {
		var id = $(this).attr("data-id");
		  var cname = $(".courseName"+id).val();
                $.ajax({
                            url: 'actions/delete_scripts.php',
                            type: 'POST',
                            data: {id:id,courseName:cname,choice:"SchoolCourse_e"},
                             success: function (result) {
								showNotification("alert-success", "Record is Successsfully Updated", "bottom", "center", "", "");
							 }
				});
		  $("#"+id).attr("disabled", true);
		  $("a.d"+id).css("display", "inline");
		  $("a.e"+id).css("display", "inline");
		  $("a.c"+id).css("display", "none");
		  $("a.u"+id).css("display", "none");			  
	  });	  
function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
        message: text
    },
        {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
}	  
	  
	  $( document ).on( "click", "a.delete-record-scourse", function(e) {
        run_waitMe($('#view-schoolscourses'), 1, "timer");
            var id = $(this).attr("data-id");
//  alert(id);          
            
                                                 $('#view-schoolscourses').waitMe('hide');
                                                 $("#delete-id-scourse").val(id);
                                                  //$("#modal-delete").modal('show');    
                                                  $('#modal-delete-scourse').appendTo("body").modal('show');                                                              
        });
	  
	  /* Delete Yes Scripts */
	  $("#delete-yes-type").click(function(){
		  var id = $("#delete-id-type").val();
                $.ajax({
                            url: 'actions/delete_scripts.php',
                            type: 'POST',
                            data: {id:id,choice:"SchoolType"},
                             success: function (result) {
                                    switch(result)
                                    {
                                      case "Error":
                                               $("#deresulttype").html(
        '<div id="error-delete" class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'There Was An Error Processing Your Request.' + 
         '</div>'); 
											setTimeout(function() {   
												$("#deresulttype").html("");
                                               $("#modal-delete-type").modal('hide');
}, 1000);
                                        break;
                                        case "Success":
                                                 $("#deresulttype").html(
        '<div id="success-delete" class="alert alert-success alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'Records Deleted Successfully.' + 
         '</div>');  
                                                 setTimeout(function() {   
                                               $("#modal-delete-type").modal('hide');
                								window.location.replace("view-courses");
}, 1000);
                                        break;
                                    }
                              }
                      });    
          });
	  
	  $("#delete-yes-level").click(function(){
		  var id = $("#delete-id-level").val();
                $.ajax({
                            url: 'actions/delete_scripts.php',
                            type: 'POST',
                            data: {id:id,choice:"SchoolLevel"},
                             success: function (result) {
                                    switch(result)
                                    {
                                      case "Error":
                                               $("#deresultlevel").html(
        '<div id="error-delete" class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'There Was An Error Processing Your Request.' + 
         '</div>'); 
											setTimeout(function() {   
												$("#deresultlevel").html("");
                                               $("#modal-delete-level").modal('hide');
}, 1000);
                                        break;
                                        case "Success":
                                                 $("#deresultlevel").html(
        '<div id="success-delete" class="alert alert-success alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'Records Deleted Successfully.' + 
         '</div>');  
                                                 setTimeout(function() {   
                                               $("#modal-delete-level").modal('hide');
                								window.location.replace("view-courses");
}, 1000);
                                        break;
                                    }
                              }
                      });    
          });
	  
	  $("#delete-yes-year").click(function(){
		  var id = $("#delete-id-year").val();
                $.ajax({
                            url: 'actions/delete_scripts.php',
                            type: 'POST',
                            data: {id:id,choice:"SchoolYear"},
                             success: function (result) {
                                    switch(result)
                                    {
                                      case "Error":
                                               $("#deresultyear").html(
        '<div id="error-delete" class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'There Was An Error Processing Your Request.' + 
         '</div>'); 
											setTimeout(function() {   
												$("#deresultyear").html("");
                                               $("#modal-delete-year").modal('hide');
}, 1000);
                                        break;
                                        case "Success":
                                                 $("#deresultyear").html(
        '<div id="success-delete" class="alert alert-success alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'Records Deleted Successfully.' + 
         '</div>');  
                                                 setTimeout(function() {   
                                               $("#modal-delete-year").modal('hide');
                								window.location.replace("view-courses");
}, 1000);
                                        break;
                                    }
                              }
                      });    
          });
	  
	  $("#delete-yes-course").click(function(){
		  var id = $("#delete-id-course").val();
                $.ajax({
                            url: 'actions/delete_scripts.php',
                            type: 'POST',
                            data: {id:id,choice:"SchoolCourse"},
                             success: function (result) {
                                    switch(result)
                                    {
                                      case "Error":
                                               $("#deresultcourse").html(
        '<div id="error-delete" class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'There Was An Error Processing Your Request.' + 
         '</div>'); 
											setTimeout(function() {  
												$("#deresultcourse").html("");
                                               $("#modal-delete-course").modal('hide');
}, 1000);
                                        break;
                                        case "Success":
                                                 $("#deresultcourse").html(
        '<div id="success-delete" class="alert alert-success alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'Records Deleted Successfully.' + 
         '</div>');  
                                                 setTimeout(function() {   
                                               $("#modal-delete-course").modal('hide');
                								window.location.replace("view-courses");
}, 1000);
                                        break;
                                    }
                              }
                      });    
          });
	  
	  $("#delete-yes-scourse").click(function(){
		  var id = $("#delete-id-scourse").val();
                $.ajax({
                            url: 'actions/delete_scripts.php',
                            type: 'POST',
                            data: {id:id,choice:"deleteSchoolCourse"},
                             success: function (result) {
                                    switch(result)
                                    {
                                      case "Error":
                                               $("#deresultscourse").html(
        '<div id="error-delete" class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'There Was An Error Processing Your Request.' + 
         '</div>'); 
											setTimeout(function() {  
												$("#deresultscourse").html("");
                                               $("#modal-delete-scourse").modal('hide');
}, 1000);
                                        break;
                                        case "Success":
                                                 $("#deresultscourse").html(
        '<div id="success-delete" class="alert alert-success alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'Records Deleted Successfully.' + 
         '</div>');  
                                                 setTimeout(function() {   
                                               $("#modal-delete-scourse").modal('hide');
                								window.location.replace("view-courses");
}, 1000);
                                        break;
                                    }
                              }
                      });    
          });
	  
	  /* Edit Record Modal */
	  $( document ).on( "click", "a.edit-record-type", function(e) {
        $('#typeedit-form')[0].reset();
        //$('select').prop('selectedIndex',0);
          
        run_waitMe($('#view-schooltypes'), 1, "timer");
            var id = $(this).attr("data-id");
            $.ajax({
                            url: 'actions/select_scripts.php',
                            type: 'POST',
                            data: {id:id, choice:"SchoolType"},
                            dataType: "JSON",
                             success: function (result) {
                                    switch(result["stat"])
                                    {
                                      case "Error":
                                        setTimeout(function() {   
                                               $('#view-schooltypes').waitMe('hide');   
}, 500); 
                                        break;

                                         case "Success":

                                        setTimeout(function() { 
                                                  $("#modal-typeedit  #type-id").val(result["ID"]);
                                                  $("#modal-typeedit  #type-name").val(result["Name"]);
                                                  $('#view-schooltypes').waitMe('hide');
                                                  $("#modal-typeedit").modal('show');                                                            
}, 500); 
                                        break;

                                    
                                    }
                              }
                      });

        });
	  
	   $( document ).on( "click", "a.edit-record-level", function(e) {
        $('#leveledit-form')[0].reset();
        //$('select').prop('selectedIndex',0);
          
        run_waitMe($('#view-schoollevels'), 1, "timer");
            var id = $(this).attr("data-id");
            $.ajax({
                            url: 'actions/select_scripts.php',
                            type: 'POST',
                            data: {id:id, choice:"SchoolLevel"},
                            dataType: "JSON",
                             success: function (result) {
                                    switch(result["stat"])
                                    {
                                      case "Error":
                                        setTimeout(function() {   
                                               $('#view-schoollevels').waitMe('hide');   
}, 500); 
                                        break;

                                         case "Success":

                                        setTimeout(function() { 
                                                  $("#modal-leveledit  #level-id").val(result["ID"]);
                                                  $("#modal-leveledit  #level-name").val(result["Name"]);
                                                  $('#view-schoollevels').waitMe('hide');
                                                  $("#modal-leveledit").modal('show');                                                            
}, 500); 
                                        break;

                                    
                                    }
                              }
                      });

        });
	  
	  $( document ).on( "click", "a.edit-record-year", function(e) {
        $('#yearedit-form')[0].reset();
        //$('select').prop('selectedIndex',0);
          
        run_waitMe($('#view-schoolyears'), 1, "timer");
            var id = $(this).attr("data-id");
            $.ajax({
                            url: 'actions/select_scripts.php',
                            type: 'POST',
                            data: {id:id, choice:"SchoolYear"},
                            dataType: "JSON",
                             success: function (result) {
                                    switch(result["stat"])
                                    {
                                      case "Error":
                                        setTimeout(function() {   
                                               $('#view-schoolyears').waitMe('hide');   
}, 500); 
                                        break;

                                         case "Success":

                                        setTimeout(function() { 
                                                  $("#modal-yearedit  #year-id").val(result["ID"]);
                                                  $("#modal-yearedit  #year-name").val(result["Name"]);
                                                  $('#view-schoolyears').waitMe('hide');
                                                  $("#modal-yearedit").modal('show');                                                            
}, 500); 
                                        break;

                                    
                                    }
                              }
                      });

        });


  });

</script>
	<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
</body>
</html>
<?php
}
?>
<div class="modal modal-fullscreen fade" id="modal-delete-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Are you sure you want to delete selected record(s)?</h6>
      </div>
      <div class="modal-body">
        <div id="deresulttype"></div>
        <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                          <input type="hidden" id="delete-id-type" name="delete-id-type" />
			<input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                      <div class="form-group">
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                      
                          <button type="button" class="btn btn-success" id="delete-yes-type">Yes</button>
                          <button type="button" class="btn btn-danger" id="delete-no-type">No</button>
                           
                        </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                      </div>
                    </form>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>
<div class="modal modal-fullscreen fade" id="modal-delete-level" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Are you sure you want to delete selected record(s)?</h6>
      </div>
      <div class="modal-body">
        <div id="deresultlevel"></div>
        <form id="delete-form-level" class="form-horizontal form-label-left" method="post" action="">
                          <input type="hidden" id="delete-id-level" name="delete-id-level" />
			<input type="hidden" id="delete-choice" name="delete-choice" value="SchoolLevel"/>
                      <div class="form-group">
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                      
                          <button type="button" class="btn btn-success" id="delete-yes-level">Yes</button>
                          <button type="button" class="btn btn-danger" id="delete-no-level">No</button>
                           
                        </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                      </div>
                    </form>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>

<div class="modal modal-fullscreen fade" id="modal-delete-year" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Are you sure you want to delete selected record(s)?</h6>
      </div>
      <div class="modal-body">
        <div id="deresultyear"></div>
        <form id="delete-form-year" class="form-horizontal form-label-left" method="post" action="">
                          <input type="hidden" id="delete-id-year" name="delete-id-year" />
			<input type="hidden" id="delete-choice" name="delete-choice" value="SchoolYear"/>
                      <div class="form-group">
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                      
                          <button type="button" class="btn btn-success" id="delete-yes-year">Yes</button>
                          <button type="button" class="btn btn-danger" id="delete-no-year">No</button>
                           
                        </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                      </div>
                    </form>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>

<div class="modal modal-fullscreen fade" id="modal-delete-course" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Are you sure you want to delete selected record(s)?</h6>
      </div>
      <div class="modal-body">
        <div id="deresultcourse"></div>
        <form id="delete-form-course" class="form-horizontal form-label-left" method="post" action="">
                          <input type="hidden" id="delete-id-course" name="delete-id-course" />
			<input type="hidden" id="delete-choice" name="delete-choice" value="SchoolCourse"/>
                      <div class="form-group">
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                      
                          <button type="button" class="btn btn-success" id="delete-yes-course">Yes</button>
                          <button type="button" class="btn btn-danger" id="delete-no-course">No</button>
                           
                        </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                      </div>
                    </form>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>
<div class="modal modal-fullscreen fade" id="modal-delete-scourse" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Are you sure you want to delete selected record(s)?</h6>
      </div>
      <div class="modal-body">
        <div id="deresultscourse"></div>
        <form id="delete-form-course" class="form-horizontal form-label-left" method="post" action="">
                          <input type="hidden" id="delete-id-scourse" name="delete-id-scourse" />
			<input type="hidden" id="delete-choice" name="delete-schoice" value="SchoolCourse"/>
                      <div class="form-group">
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                      
                          <button type="button" class="btn btn-success" id="delete-yes-scourse">Yes</button>
                          <button type="button" class="btn btn-danger" id="delete-no-scourse">No</button>
                           
                        </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                      </div>
                    </form>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>

<div class="modal modal-fullscreen fade" id="modal-typeedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Edit School Type</h6>
      </div>
      <div class="modal-body">
        <!-- Form Code Goes Here -->
        <!--<div class="text-center alert alert-success alert-dismissible fade in" id="success-epm" style="color: green;" role="alert"></div>
        <div class="text-center alert alert-danger alert-dismissible fade in" id="error-epm" style="color: red;" role="alert"></div>-->
        <div id="eresulttype">
        </div>
        <form id="typeedit-form" class="form-horizontal form-label-left">
          <div class="form-group">
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">     
                        <input type="hidden" id="choice" name="choice" value="SchoolType">
                          <input type="hidden" id="type-id" name="type-id" />      
                          
              </div>
            </div>
          <div class="form-group">
                        <label for="employee-senttostate" class="control-label col-md-3 col-sm-3 col-xs-12">School Type</label>
                       
                          <input id="type-name" class="form-control col-md-7 col-xs-12" type="text" name="type-name">
                        
                      </div>
            

                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                         
                          
                          <button type="submit" class="btn btn-success">Update</button>
                           
                        </div>
                      </div>   

                    </form>        <!-- -->
      </div>
      
    </div>
  </div>
</div>



<div class="modal modal-fullscreen fade" id="modal-leveledit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Edit School Level</h6>
      </div>
      <div class="modal-body">
        <!-- Form Code Goes Here -->
        <!--<div class="text-center alert alert-success alert-dismissible fade in" id="success-epm" style="color: green;" role="alert"></div>
        <div class="text-center alert alert-danger alert-dismissible fade in" id="error-epm" style="color: red;" role="alert"></div>-->
        <div id="eresultlevel">
        </div>
        <form id="leveledit-form" class="form-horizontal form-label-left">
          <div class="form-group">
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">     
                        <input type="hidden" id="choice" name="choice" value="SchoolType">
                          <input type="hidden" id="level-id" name="level-id" />      
                          
              </div>
            </div>
          <div class="form-group">
                        <label for="employee-senttostate" class="control-label col-md-3 col-sm-3 col-xs-12">School Type</label>
                       
                          <input id="level-name" class="form-control col-md-7 col-xs-12" type="text" name="level-name">
                        
                      </div>
            

                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                         
                          
                          <button type="submit" class="btn btn-success">Update</button>
                           
                        </div>
                      </div>   

                    </form>        <!-- -->
      </div>
      
    </div>
  </div>
</div>


<div class="modal modal-fullscreen fade" id="modal-yearedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Edit School Year</h6>
      </div>
      <div class="modal-body">
        <!-- Form Code Goes Here -->
        <!--<div class="text-center alert alert-success alert-dismissible fade in" id="success-epm" style="color: green;" role="alert"></div>
        <div class="text-center alert alert-danger alert-dismissible fade in" id="error-epm" style="color: red;" role="alert"></div>-->
        <div id="eresultyear">
        </div>
        <form id="yearedit-form" class="form-horizontal form-label-left">
          <div class="form-group">
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">     
                        <input type="hidden" id="choice" name="choice" value="SchoolType">
                          <input type="hidden" id="year-id" name="year-id" />      
                          
              </div>
            </div>
          <div class="form-group">
                        <label for="employee-senttostate" class="control-label col-md-3 col-sm-3 col-xs-12">School level</label>
                       
                          <input id="year-name" class="form-control col-md-7 col-xs-12" type="text" name="year-name">
                        
                      </div>
            

                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                         
                          
                          <button type="submit" class="btn btn-success">Update</button>
                           
                        </div>
                      </div>   

                    </form>        <!-- -->
      </div>
      
    </div>
  </div>
</div>

<div class="modal modal-fullscreen fade" id="modal-yearedit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Edit School Year</h6>
      </div>
      <div class="modal-body">
        <!-- Form Code Goes Here -->
        <!--<div class="text-center alert alert-success alert-dismissible fade in" id="success-epm" style="color: green;" role="alert"></div>
        <div class="text-center alert alert-danger alert-dismissible fade in" id="error-epm" style="color: red;" role="alert"></div>-->
        <div id="eresultyear">
        </div>
        <form id="yearedit-form" class="form-horizontal form-label-left">
          <div class="form-group">
                        
                        <div class="col-md-6 col-sm-6 col-xs-12">     
                        <input type="hidden" id="choice" name="choice" value="SchoolType">
                          <input type="hidden" id="year-id" name="year-id" />      
                          
              </div>
            </div>
          <div class="form-group">
                        <label for="employee-senttostate" class="control-label col-md-3 col-sm-3 col-xs-12">School Year</label>
                       
                          <input id="year-name" class="form-control col-md-7 col-xs-12" type="text" name="year-name">
                        
                      </div>
            

                      <div class="form-group">
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
                         
                          
                          <button type="submit" class="btn btn-success">Update</button>
                           
                        </div>
                      </div>   

                    </form>        <!-- -->
      </div>
      
    </div>
  </div>
</div>