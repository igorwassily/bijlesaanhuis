<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>License Information</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>License Information
               
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">License Information</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
			 <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="card">
                    <div class="body">
						<div><p>Clicking the button below forces the immediate license check.<br>You can use this following the purchase of an upgrade or addon product to have it take immediate effect </p></div>
						<div>
						 <button type="button" class="btn btn-raised btn-primary btn-round waves-effect">Force Update License</button>
						</div>
                    </div>
                </div>
            </div>
			 <div class="col-lg-1"></div>
        </div>
		<div class="row clearfix">
			 <div class="col-lg-1"></div>
            <div class="col-lg-10">
                <div class="card">
                    <div class="body">
						  <div class="body table-responsive">
                        <table class="table table-striped">
                            <thead>
                                
                            </thead>
                            <tbody>
                                <tr>
                                    
                                    <td>Registered to</td>
                                    <td>Kevin Kennegieter</td>
                                </tr>
								<tr>
                                    
                                    <td>License Key</td>
                                    <td></td>
                                </tr>
								<tr>
                                    
                                    <td>License Type</td>
                                    <td>Owned License No Branding</td>
                                </tr>
								<tr>
                                    
                                    <td>Valid Domain</td>
                                    <td>client.we.think.eu, www.client.we.think.eu</td>
                                </tr>
								<tr>
                                    
                                    <td>Valid IP</td>
                                    <td>185.85.16.19</td>
                                </tr>
								<tr>
                                    
                                    <td>Valid Directory</td>
                                    <td>/var/www/vhosts/we.think.eu/www.we.think.eu</td>
                                </tr>
								<tr>
                                    
                                    <td>Branding Removal</td>
                                    <td>Yes</td>
                                </tr>
								<tr>
                                    
                                    <td>Addons</td>
                                    <td>Support and Updates<br>
									iPhone App<br>
									Project Management Addon<br>
									Branding Removal
									
									</td>
                                </tr>
								<tr>
                                    
                                    <td>Created</td>
                                    <td>Tuesday,11th February 2014</td>
                                </tr>
								<tr>
                                    
                                    <td>Expires</td>
                                    <td>Never</td>
                                </tr>
                               
                            </tbody>
                        </table>
                    </div>
                    </div>
                </div>
            </div>
			 <div class="col-lg-1"></div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
</body>
</html>
<?php
}
?>