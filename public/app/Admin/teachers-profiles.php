<?php
$thisPage="Teachers Profile";
session_start();
if(!isset($_SESSION['Student']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Teachers Profiles</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
    color: black !important;
}
	
	#result{
        position: absolute;
z-index: 999;
top: 90%;
left: 22px;
background-color: white;
    }
    .search-box input[type="text"], #result{
        width: 100%;
        box-sizing: border-box;
    }
    /* Formatting result items */
   #result a:hover{
        
	   color: cyan !Important;
    }
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-12">
                <h2>Hi, <?php echo $_SESSION['StudentFName']." ".$_SESSION['StudentLName']; ?>
                </h2>
            </div>
			<div class="col-lg-3 col-md-6 col-sm-12">
                <div class="input-group search-box">                
                <input type="text" class="form-control" placeholder="Search Teacher By Name" name="teachername" id="teachername" autocomplete="off" />
					
                <span class="input-group-addon">
                    <i class="zmdi zmdi-search"></i>
                </span>
						
            </div>
			<div id="result"></div>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="logout"><i class="zmdi zmdi-home"></i>&nbsp;&nbsp; Logout</a></li>
                    
                </ul>
            </div>
        </div>
    </div>
    <div class="container-fluid">
		<div class="row clearfix">
			<?php
	
		$stmt = $con->prepare("SELECT u.userID, u.username, u.email from user u inner join usergroup ug on (u.usergroupID = ug.usergroupID) where u.active = ? AND ug.usergroupname = ?");
				$status = 1;
				$group = "Teacher";
				$stmt->bind_param("is",$status,$group);
				$stmt->execute();
				$stmt->bind_result($userid,$username,$useremail);
				$stmt->store_result();
				while($stmt->fetch())
				{
	?>
            <div class="col-lg-3 col-md-4 col-sm-12 text-center">
                <div class="card product_item">
                    <div class="body">
                        <div class="cp_img">
                            <img src="img/dummy-profile-image-png-6.png" alt="Product" class="img-fluid text-center" />
                        
                        </div>
                        <div class="product_details">
							<form method="post" action="reserve-slot">
								<input type="hidden" name="teacherid" id="teacherid" value = "<?php echo $userid; ?>" />
                            <h5><?php echo $username; ?></h5>
                            <ul class="product_price list-unstyled">
                                <li class="new_price"><?php echo $useremail; ?></li>
                            </ul>
							<button type="submit" name="reserve-slot" class="btn btn-default">Reserve Slot</button>
							</form>
                        </div>
                    </div>
                </div>                
            </div>
			<?php
				}
	?>
        </div>
    </div> 
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
	<!-- Jquery steps Wizard Code --> 
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
	<script type="text/javascript">
$(document).ready(function(){
    $('.search-box input[type="text"]').on("keyup input", function(){
        /* Get input value on change */
        var inputVal = $(this).val();
        var resultDropdown = $("#result");
        if(inputVal.length){
            $.get("actions/teacher-find.php", {term: inputVal}).done(function(data){
                // Display the returned data in browser
                resultDropdown.html(data);
            });
        } else{
            resultDropdown.empty();
        }
    });
    
    // Set search input value on click of result item
    $(document).on("click", "#result a", function(){
        $(this).parents(".search-box").find('input[type="text"]').val($(this).text());
        $(this).parent("#result").empty();
    });
});
</script>
</body>
</html>
<?php
}
?>