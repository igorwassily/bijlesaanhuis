<?php

require_once('includes/connection.php');
require_once('../../../inc/Email.php');

if(isset($_POST['page'])){
	if($_POST['page'] == "delete_tlevel"){

		$q = "UPDATE teacherlevel_rate SET isdeleted=1 WHERE ID=".$_POST['id'];
		$r = mysqli_query($con, $q);
		if($r){
			echo "success";
		}else{
			echo "failed";
		}
	}
	elseif($_POST['page'] == "update_levelrate"){
		$rate = str_replace(",", ".", $_POST['Erate']);

		$q = "UPDATE teacherlevel_rate SET rate=$rate, cost_per_km='$_POST[cpkm]' , internal_rate='$_POST[Irate]', level='$_POST[Elevel]', next_teacherlevelID='$_POST[ntlevel]' WHERE ID=".$_POST['id'];

		$r = mysqli_query($con, $q);
		if($r){
			echo "success";
		}else{
			echo "failed";
		}
	}
	elseif($_POST['page'] == "insert_level"){

		$q = "INSERT INTO  teacherlevel_rate (internal_level, internal_rate, rate) VALUES ('{$_POST['level']}', 0, 0); ";
		$r = mysqli_query($con, $q);
		if($r){
			echo "success";
		}else{
			echo "failed";
		}
	}
	elseif ($_POST['page'] == "update_boundaries") {
		if (is_numeric($_POST['boundary1'])) {
			if (mysqli_query($con, "INSERT INTO variables (name, value) VALUES ('freeTravelRange', '{$_POST['boundary1']}') ON DUPLICATE KEY UPDATE value='{$_POST['boundary1']}'")) {
				echo "success";
				return;
			}
		}
	}
	elseif($_POST['page'] == "accept_level"){
		$_q = "SELECT firstname, lastname, email, image, t.teacherlevel_rate_ID as oldLevel, (SELECT level FROM teacherlevel_rate tlr WHERE tlr.ID=t.requested_teacherlevel_rate_ID)newLevel  FROM `contact` c, `teacher` t, `profile` p WHERE t.userID=c.userID AND t.profileID=p.profileID AND c.userID=".$_POST['tid'];
		$_r = mysqli_query($con, $_q);
		$_f = mysqli_fetch_assoc($_r);

		$q = "UPDATE teacher SET requested_teacherlevel_rate_ID=NULL , teacherlevel_rate_ID=$_POST[rtlID], permission=NULL WHERE userID=".$_POST['tid'];
		$r = mysqli_query($con, $q);
		if($r){

			if($_POST['rtlID'] == 3) {
				$email = new Email($db, 'accepted_type_change_S1_to_S2');
				$email->Prepare($_POST['tid']);
				$email->Send('teacher');
			}else{
				$data = [
					'teacher_forename' => $_f['firstname'],
					'teacher_surname' => $_f['lastname'],
					'teacher_name' => $_f['firstname'] . ' ' . $_f['lastname'],
					'teacher_img' => $_f['image'],
				];
				$_q = "SELECT DISTINCT cb.studentID FROM calendarbooking cb WHERE cb.teacherID={$_POST['tid']}";
				$_r = mysqli_query($con, $_q);
				while($r = mysqli_fetch_assoc($_r)){
					if ($_f['oldLevel'] == 1) {
						$email = new Email($db, 'accepted_type_change_J_to_S1_for_student');
					} else {
						$email = new Email($db, 'accepted_type_change_S2_to_SUP_to_student');
					}
					$email->Prepare($r['studentID'], $data);
					$email->Send('student');
					$email->Send('parent');
				}
				if ($_f['oldLevel'] == 1) {
					$email = new Email($db, 'accepted_type_change_J_to_S1_for_tutor');
				} else {
					$email = new Email($db, 'accepted_type_change_S2_to_SUP_to_tutor');
				}
				$email->Prepare($_POST['tid']);
				$email->Send('teacher');
			}


			echo "success";
		}else{
			echo "failed";
		}
	}

	elseif($_POST['page'] == "cancel_level"){

	 	$q = "UPDATE teacher SET permission='Rejected' WHERE userID=".$_POST['tid'];
		$r = mysqli_query($con, $q);
		if($r){
			echo "success";
		}else{
			echo "failed";
		}
	}

	elseif($_POST['page'] == "accept_subject"){

		$q = "UPDATE teacher_courses SET permission='Accepted' WHERE  teacher_courseID=$_POST[tid]; ";
		$r = mysqli_query($con, $q);
		if($r){
//			$_q = "SELECT firstname, email FROM `contact`c, teacher_courses tc WHERE c.userID=tc.teacherID AND tc.teacher_courseID=".$_POST['tid'];
//			$_r = mysqli_query($con, $_q);
//			$_f = mysqli_fetch_assoc($_r);

			$email = new Email($db, 'admin_accepted_extra_course_level_year_to_tutor');
			$email->Prepare($_POST['tid']);
			$email->Send('teacher');
//			admin_accepted_extra_course_level_year_to_tutor($_f['email'], $_f['firstname']);
			echo "success";
		}else{
			echo "failed";
		}
	}

	elseif($_POST['page'] == "cancel_subject"){

		$q = "UPDATE teacher_courses SET permission='Rejected' WHERE  teacher_courseID=$_POST[tid]; ";
		$r = mysqli_query($con, $q);
		if($r){
			echo "success";
		}else{
			echo "failed";
		}
	}

	elseif($_POST['page'] == "delete_subj"){

		$q = "DELETE FROM teacher_courses WHERE  teacherID=$_POST[tid] AND courseID=$_POST[cid] AND schoollevelID=$_POST[clid]; ";
		$r = mysqli_query($con, $q);
		if($r){
			echo "success";
		}else{
			echo "failed";
		}
	}
	elseif($_POST['page'] == "change_subj"){
		if(!$_POST['value'])
			return;
		$v = explode("_", $_POST['value']);

	 	$q = "UPDATE teacher_courses SET permission='".$v[3]."' WHERE  teacherID=$v[0] AND courseID=$v[1] AND schoollevelID=$v[2]; ";
		$r = mysqli_query($con, $q);
		if($r){
			echo "success";
		}else{
			echo "failed";
		}
	}








}

?>