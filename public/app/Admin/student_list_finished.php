<?php
$thisPage="Student list finished";
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Students finished first lesson</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	div.card>div.header{color:white;}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
	require_once('includes/connection.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="header">
                        <h3>Students finished first lesson</h3>
                    </div>
					
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px;" id="teachers_approve" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>First Name</th>
                                        <th>Last Name</th>
                                        <th>Email</th>
                                        <th>Phone number</th>
                                        <th>Tutor</th>
                                        <th>Date</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
											$stmt = $con->prepare("SELECT b.teacherID, tc.firstname 'teacher_firstname', tc.lastname 'teacher_lastname', b.datee, u.userID, c.firstname, c.lastname, u.email, c.telephone, c.place_name, u.active
FROM `user` u,
     contact c
JOIN (SELECT DISTINCT studentID, teacherID, datee, starttime FROM calendarbooking WHERE isClassTaken=1 group by studentID ORDER BY datee, starttime ASC) b ON b.studentID=c.userID
LEFT JOIN teacher t ON t.userID=b.teacherID
LEFT JOIN `user` tu ON tu.userID=t.userID AND tu.disabled=0
LEFT JOIN contact tc ON tc.userID=t.userID AND tc.contacttypeID=1
LEFT JOIN admin_msg_read m ON m.type=1 AND m.userID=c.userID
WHERE u.usergroupID = 1
  AND u.disabled = 0
  AND u.userID = c.userID
  AND c.contacttypeID = 2
  AND u.active = 1
  AND m.userID IS NULL");
	
					$stmt->execute();
					$stmt->bind_result($tid, $tFirstName, $tLastName, $date, $id,$firstname,$lastname,$email,$phone,$address, $active);
					$stmt->store_result();
					while($stmt->fetch())
					{
					?>
									<tr>
									
										<?php echo "<td><a href='../student-profile?sid=$id'>$firstname </a></td>"; ?>
										<td><?php echo $lastname; ?></td>
										<td><?php echo $email; ?></td>		
										<td><?php echo $phone; ?></td>
										<td><?php echo $tFirstName . ' ' . $tLastName; ?></td>
										<td><?php echo $date; ?></td>
										<td><a  href="/app/student-edit-profile?sid=<?php echo $id?>" >Edit</a>
											&nbsp;&nbsp;&nbsp;&nbsp;
											<a  href="#" class="delete" data-id="<?php echo $id?>">Dismiss</a></td>
									</tr>
				<?php }	?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
	
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="assets/js/pages/ui/notifications.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
		
		
    $('#teachers_approve').DataTable();
	});
		
	$(".delete").click(function(){
		var id = $(this).attr("data-id");
		
		$.ajax({
        type: "POST", 
        url: "admin-ajaxcalls.php",
        data: {subpage:"read_student_first_lesson",id:id}, //here delete_teacher == delete_student
        success: function(response) { 
                      if(response == "success"){
                         
                         showNotification("alert-primary", "Successfully marked as read", "bottom", "center", "", "");
						  location.reload(); 
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
                  },
        error: function(xhr, ajaxOptions, thrownError){
                      if(xhr.response == "success"){
                         
                         showNotification("alert-primary", "Successfully marked as read", "bottom", "center", "", "");
						  location.reload(); 
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
				}
      });// ends ajax
	});
	
</script>

</body>
</html>
<?php
}
?>