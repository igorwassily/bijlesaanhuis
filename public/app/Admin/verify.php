<?php

require_once('includes/connection.php');
$email = test_input($_GET['email']);
$hash = test_input($_GET['hash']);

$stmt01 = $con->prepare("SELECT userID, registered_from from user where email = ? AND verificationcode = ?");
$stmt01->bind_param("ss", $email, $hash);
$stmt01->execute();
$stmt01->bind_result($userID, $registered_from);
$stmt01->store_result();
$stmt01->fetch();
$stmt01->close();
$stmt = $con->prepare("UPDATE user set active = ?, verificationcode = NULL where userID = ?");
$active = 1;
$stmt->bind_param("ii", $active, $userID);
if ($stmt->execute()) {
    header( 'Location: ' . $registered_from);
} else {
    header('Location: successAccount.php?registration_from=' . $registered_from);
}
