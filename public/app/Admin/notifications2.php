<?php
$thisPage = "notifications2";
session_start();

if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');

}
else
{
	?>

	<!DOCTYPE html>
	<html class="no-js " lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
		<title>Notifications</title>
		<?php
		require_once('includes/connection.php');
		require_once('includes/mainCSSFiles.php');
		?>
		<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
		<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
		<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
		<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
		<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
		<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
		<style type="text/css">

			/*Placeholder Color */
			input{
				border: 1px solid #bdbdbd !important;

				color: #486066 !important;
			}

			select{
				border: 1px solid #bdbdbd !important;

				color: #486066 !important;
			}

			input:focus{
				background:transparent !Important;
			}

			select:focus{
				background:transparent !Important;
			}

			.wizard .content
			{
				/*overflow-y: hidden !important;*/
			}

			.wizard .content label {

				color: white !important;

			}
			.wizard>.steps .current a
			{
				background-color: #029898 !Important;
			}
			.wizard>.steps .done a
			{
				background-color: #828f9380 !Important;
			}
			.wizard>.actions a
			{
				background-color: #029898 !Important;
			}
			.wizard>.actions .disabled a
			{
				background-color: #eee !important;
			}

			.btn.btn-simple{
				border-color: #486066 !important;
			}
			.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
				color: white;
			}

			table
			{
				color: white;
			}
			.multiselect.dropdown-toggle.btn.btn-default
			{
				display: none !important;
			}

			.navbar.p-l-5.p-r-5
			{
				display: none !important;
			}

			input[type="text"] {
				height: 40px !important;
			}
			.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
				background-color: transparent !important;
			}

			.bootstrap-select[disabled] button
			{
				color: gray !important;
				border: 1px solid gray !important;
			}

			.bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
				color: black;
			}

			.bootstrap-select.form-control:not([class*="col-"]){
				width: auto !important;
			}
		</style>


		<script>

            function deleteEmail(e){
                var url = "/api/Calendar/CancelTutor";
                var id = e.attr('name');

                if(confirm("Gebruiker verwijderen?")) {
                    $.ajax({
                        type: "POST",
                        url: '/app/includes/chat-ajax.php',
                        data: {id: id, filename: 'deleteMessage'},
                        dataType: "json",
                        success: function (response) {
                            showNotification("alert-success", "Succesfully deleted email.", "bottom", "center", "", "", "");
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                        }
                    });// ends ajax
                }
                else {
                    return 0;
                }
            }
		</script>

	</head>
	<body class="theme-green">
	<!-- Page Loader -->
	<div class="page-loader-wrapper">
		<div class="loader">
			<div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
			<p>Please wait...</p>
		</div>
	</div>
	<!-- Overlay For Sidebars-->
	<div class="overlay"></div>


	<?php
	require_once('includes/header.php');
	require_once('includes/sidebarAdminDashboard.php');
	require_once('includes/connection.php');
	?>

	<?php
	$activePage = basename($_SERVER['PHP_SELF']);
	?>

	<!-- Main Content -->
	<section class="content page-calendar" style="margin-top: 0px !important;">
		<div class="block-header">
			<?php require_once('includes/adminTopBar.php'); ?>
		</div>
		<div class="container-fluid">
			<div class="row clearfix">
				<div class="col-lg-12">
					<div class="card">
						<!-- <div class="header">
							<h3>Tutor Appointment List</h3>
						</div> -->
						<?php
						$query = "SELECT value FROM variables WHERE name='respondTimeChat'";
						$result = mysqli_query($con, $query);
						$respondTimeDB = $result->fetch_assoc()['value'];

						if (!isset($respondTimeDB) && empty($respondTimeDB)){
							$respondTimeDB = "24";
						}

						?>


						<div class="body">
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px; color: #486066" id="teachers_approve" cellspacing="0" width="100%">
									<thead>
									<tr class="text-center">
										<th class="color">TUTOR NAME</th>
										<th class="color">CUSTOMER NAME</th>
										<th class="color">DATE OF MESSAGE</th>
										<th class="color">LAST MESSAGE</th>
										<th class="color">IS READ</th>
										<th class="color">Status</th>
										<th class="color">DELETE</th>
									</tr>
									</thead>

									<tbody>
									<?php
									$sql = "SELECT DISTINCT e.emailID,
                e.receiverID,
                c.firstname,
                c.lastname,
                e.senderID,
                c2.firstname,
                c2.lastname,
                e.message,
                e.is_read,
                MIN(DATE_FORMAT(date_time, '%Y-%m-%d %H:%i:%s')) daatetime,
                e.status
FROM email e
         LEFT JOIN contact c ON e.receiverID = c.userID
         LEFT JOIN contact c2 ON e.senderID = c2.userID
WHERE c.contacttypeID = 1
  AND e.senderID != 0
  AND c2.contacttypeID = 2
  AND c2.prmary = 1
  AND e.message != 'Test'
  AND e.message != ' '
  AND e.message NOT LIKE '%Bijlesverzoek ingediend%'
  AND e.message NOT LIKE '%Bijles geannuleerd%'
  AND e.message NOT LIKE '%Bijles geannuleerd / geweigerd%'
  AND e.message NOT LIKE '%Afspraak ingeboekt%'
  AND e.firstMessage = 1 
GROUP BY e.emailID DESC";

									$stmt = $con->prepare($sql);
									$stmt->execute();

									$stmt->bind_result($eid, $tid,  $teacherFirstName, $teacherLastName, $sid, $studentFirstName, $studentLastName, $message, $is_read, $timestamp, $status);
									$stmt->store_result();


									while($stmt->fetch())
									{
										$q2 = $con->prepare( 'INSERT INTO admin_msg_read(type,userID) VALUES("'.NOTIFICATION_MESSAGE_NO_REACT.'", "'.$eid.'")');
										$q2->execute();
										?>
										<tr>

											<td><a href="./admin_edit_tutor_profile.php?tid=<?php echo $tid;?>"><?php echo $teacherFirstName . " " . $teacherLastName; ?></a></td>
											<td class="color"><a href="./admin_edit_student_profile.php?sid=<?php echo $sid;?>"><?php echo $studentFirstName . " " . $studentLastName; ?></a></td>
											<td class="color"><?php echo $timestamp; ?></td>
											<td class="color"><a href="../chat.php?sid=<?php echo $sid; ?>&tid=<?php echo $tid; ?>&aread=true">Chat</a></td>
											<td class="color"><?php echo ($is_read == 0) ? "False" : "True"; ?></td>
											<td class="color">
                                                <select class="selectpicker form-control show-tick change-teacher-level" data-id="<?php echo $eid; ?>" onchange="updateStatus($(this));">
                                                    <option value="1" <?php echo ($status == 1) ? "selected" : "" ?>>1</option>
                                                    <option value="1.1" <?php echo ($status == 1.1) ? "selected" : "" ?>>1.1</option>
                                                    <option value="2" <?php echo ($status == 2) ? "selected" : "" ?>>2</option>
                                                    <option value="3" <?php echo ($status == 3) ? "selected" : "" ?>>3</option>
                                                    <option value="4" <?php echo ($status == 4) ? "selected" : "" ?>>4</option>
                                                    <option value="5" <?php echo ($status == 5) ? "selected" : "" ?>>5</option>
                                                    <option value="5.1" <?php echo ($status == 5.1) ? "selected" : "" ?>>5.1</option>
                                                    <option value="5.2" <?php echo ($status == 5.2) ? "selected" : "" ?>>5.2</option>
                                                    <option value="6" <?php echo ($status == 6) ? "selected" : "" ?>>6</option>
                                                    <option value="7" <?php echo ($status == 7) ? "selected" : "" ?>>7</option>
                                                </select>
                                            </td>
											<td class="color"><a href="#" onclick="deleteEmail($(this))" name="<?php echo $eid; ?>">Delete</a></td>
										</tr>
									<?php }	?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
	require_once('includes/footerScripts.php');
	?>
	<script type="text/javascript">
        $(document).ready(function() {

            $('#teachers_approve').DataTable({
                "language": {
                    "aria": {
                        "sortAscending":  ": Oplopend sorteren",
                        "sortDescending": ": Aflopend sorteren"
                    },
                    "emptyTable":     "Geen gegevens beschikbaar in de tabel",
                    "info": "Tonen van _START_ tot _END_ van _TOTAL_ inzendingen",
                    "infoEmpty":      "Tonen van 0 tot 0 van 0 inzendingen",
                    "infoFiltered":   "(Gefilterd van in totaal _MAX_ inzendingen)",
                    "lengthMenu": "Toon _MENU_ inzendingen",
                    "loadingRecords": "Laden...",
                    "processing": "Verwerken...",
                    "paginate": {
                        "first":"Eerste",
                        "last":"Laatste",
                        "previous": "Vorige",
                        "next": "Volgende"
                    },
                    "search":"Zoeken:",
                    "zeroRecords": "Geen overeenkomende gegevens gevonden",
                }
            });



        });

        function updateStatus(e){
            var inp = e.find(':selected').val();
            var eID = e.attr("data-id");

            if(inp != " "){

                $.ajax({
                    url: 'admin-ajaxcalls.php',
                    data: {subpage:"change_notification_status_2", inp:inp, eID: eID},
                    type: "POST",
                    //dataType: "json",
                    success: function (result) {
                        showNotification("alert-success", "Student Status is Successfully updated..!", "bottom", "center", "", "");
                    }
                });
            }
        }

	</script>
	<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
	<script src="assets/js/pages/ui/notifications.js"></script>

	</body>
	</html>
	<?php
}
?>

