<?php
require_once('includes/connection.php');
require_once '../../../api/endpoint/Calendar.php';

$thisPage = "Teacher appointment detail";
session_start();

if (!isset($_SESSION['AdminUser'])) {
	header('Location: index.php');
} else {
	$userID = (isset($_GET['tid'])) ? $_GET['tid'] : 0;
	$year = (isset($_GET['year']) ? $_GET['year'] : date('Y'));



	$q = "SELECT c.firstname, c.lastname FROM contact c WHERE c.userID=" . $userID;

	$r = mysqli_query($con, $q);
	$user = mysqli_fetch_assoc($r);
	?>

	<!doctype html>
	<html class="no-js " lang="en">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge">
		<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
		<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
		<title>Teacher Appointment Detail</title>
		<?php
		require_once('includes/connection.php');
		require_once('includes/mainCSSFiles.php');
		?>
		<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
		      rel="stylesheet"/>
		<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
		<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
		<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet'/>
		<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print'/>
		<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
		<style type="text/css">

			/*Placeholder Color */
			input {
				border: 1px solid #bdbdbd !important;

				color: white !important;
			}

			select {
				border: 1px solid #bdbdbd !important;

				color: white !important;
			}

			input:focus {
				background: transparent !Important;
			}

			select:focus {
				background: transparent !Important;
			}

			.wizard .content {
				/*overflow-y: hidden !important;*/
			}

			.wizard .content label {

				color: white !important;

			}

			.wizard > .steps .current a {
				background-color: #029898 !Important;
			}

			.wizard > .steps .done a {
				background-color: #828f9380 !Important;
			}

			.wizard > .actions a {
				background-color: #029898 !Important;
			}

			.wizard > .actions .disabled a {
				background-color: #eee !important;
			}

			.btn.btn-simple {
				border-color: white !important;
			}

			.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
				color: white;
			}

			table {
				color: white;
			}

			.multiselect.dropdown-toggle.btn.btn-default {
				display: none !important;
			}

			.navbar.p-l-5.p-r-5 {
				display: none !important;
			}

			input[type="text"] {
				height: 40px !important;
			}

			.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
				background-color: transparent !important;
			}

			.bootstrap-select[disabled] button {
				color: gray !important;
				border: 1px solid gray !important;
			}

			.bootstrap-select.btn-group.show-tick .dropdown-menu li a span.text {
				color: black;
			}

			.bootstrap-select.form-control:not([class*="col-"]) {
				width: auto !important;
			}

			#search-button {
				width: auto !important;
			}

            #customRateTD {
                color: black !important;
            }
		</style>
		<?php
		$activePage = basename($_SERVER['PHP_SELF']);
		?>
	</head>
    <script type="text/javascript">
        function showCustomInput(e) {
            console.log("show");
            $('#customRateTD').css('display', 'block');

        }

        function hideCustomInput(e){
            console.log("hide");
            $('#customRateTD').css('display', 'none');
        }
    </script>

	<body class="theme-green">
	<!-- Page Loader
	<div class="page-loader-wrapper">
		<div class="loader">
			<div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
			<p>Please wait...</p>
		</div>
	</div>-->
	<!-- Overlay For Sidebars
	<div class="overlay"></div>-->


	<?php
	require_once('includes/header.php');
	require_once('includes/sidebarAdminDashboard.php');
	require_once('includes/connection.php');
	?>

	<!-- Main Content -->
	<section class="content page-calendar" style="margin-top: 0px !important;">
		<div class="block-header">
			<?php require_once('includes/adminTopBar.php'); ?>

		</div>
		<div class="container-fluid">
			<div class="row clearfix">
				<div class="col-lg-12">
					<div class="card">
						<div class="header">
							<h3><b><?php

									echo @$user['firstname'] . " " . @$user['lastname']; ?></b> Student List</h3>
						</div>

						<div class="body">


							<div class="row">
								<div class="col-md-12 col-lg-12 col-xl-12">
									<form id="search_date">

										<div class="card" style="background: transparent !important;">
											<div class="header">
												<h2><strong>Assign different Travel Cost Calculations</strong></h2>

											</div>


											<div class="body">
												<div class="row">
												</div>
											</div>

										</div>

									</form>
								</div>
							</div>
							<div class="table-responsive">
								<table class="table table-bordered table-striped table-hover dataTable dt-responsive"
								       style="font-size: 13px;" id="teachers_approve" cellspacing="0" width="100%">
									<thead>
									<tr class="text-center">
										<th class="color">Student Name</th>
										<th class="color">Customer since</th>
										<th class="color">Travel Cost Calculation</th>
                                        <th class="color" id="customRateHead">Custom Rate</th>
									</tr>
									</thead>

									<tbody>
									<?php

									$params = [
										'year' => "2020"
									];

									$calendar = new API\Endpoint\Calendar($db, ['teacherID' => $userID]);
									$result = $calendar->ReportTutor($params);
									$data = $result->getResult();

									$qb = $db->createQueryBuilder()
										->select('c.firstname, c.lastname, cb.datee, c.userID, ctc.customRate')
										->from('calendarbooking', 'cb')
										->leftJoin('cb', 'contact', 'c', 'cb.studentID  = c.userID')
                                        ->leftJoin('cb', 'custom_travel_costs', 'ctc', 'cb.studentID = ctc.studentID')
										->where('cb.teacherID = ?')
										->andWhere('cb.datee LIKE "2020%"')
										->andWhere('c.prmary = 1')
										->setParameter(0, $userID)
										->distinct()
										->execute();


									while ($row = $qb->fetch(PDO::FETCH_ASSOC)) {
									    $showInput = 'block';
										if(is_null($row['customRate'])){
											$showInput = 'none';
										}
										echo "
                                            <tr>
                                                <td><a href = '../student-profile?sid={$row['userID']}'>{$row['firstname']} {$row['lastname']}</a ></td >
                                                <td class='color'>{$row['datee']}</td>
                                                <td class='color'>
                                                	<fieldset>
    													<input type=\"radio\" id=\"default\" name=\"cost_calc\" value=\"Default\" onclick='hideCustomInput($(this));'>
    													<label for=\"default\"> Default</label> 
    													<input type=\"radio\" id=\"custom\" name=\"cost_calc\" value=\"Custom\" onclick='showCustomInput($(this));'>
    													<label for=\"custom\"> Custom</label>
  													</fieldset>
												</td>
												<td class='color'>
													<input type='text' name='{$row["userID"]}' value='{$row['customRate']}' class='customRate' id='customRateTD' style='display: {$showInput};' placeholder='Custom Rate'>
												</td>
                                          
                                            </tr>
                                        ";
									} ?>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php
	require_once('includes/footerScripts.php');
	?>
	<script type="text/javascript">

        $(document).ready(function () {
            $(".page-loader-wrapper").css("display", "none");

            $('#teachers_approve').DataTable();
        });

        $('.customRate').on('focusout', function(e){
            console.log("focus out");
            let studentID = e.currentTarget.name;
            let customRate = e.currentTarget.value;
            let teacherID = '<?php echo $userID; ?>';
            console.log(customRate);

            // make ajax and send teacherID + studentID + rate

            $.ajax({
                type: "POST",
                url: "admin-ajaxcalls.php",
                data: {subpage:"change_custom_travelcosts",customRate:customRate, studentID: studentID, teacherID: teacherID}, //here delete_teacher == delete_students
                success: function(response) {
                    if(response == "success"){
                        showNotification("alert-primary", "Successfully updated travel costs", "bottom", "center", "", "");
                    }else{
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                },
                error: function(xhr, ajaxOptions, thrownError){
                    if(xhr.response == "success"){

                        showNotification("alert-primary", "Successfully updated travel costs", "bottom", "center", "", "");
                        location.reload();
                    }else{
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                }
            });// ends ajax
        })



	</script>
	</body>
	</html>
	<?php
}
?>