<?php
$thisPage="Teachers Dashboard";
session_start();
if(!isset($_SESSION['Teacher']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Teachers Dashboard</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: white !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
    color: black !important;
}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
   <div class="block-header">
       <?php require_once('includes/teacherTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="card">
            <div class="row">
                <div class="col-md-8 col-lg-8 col-xl-8">
                    <div class="body">
                        <button class="btn btn-primary btn-round waves-effect" id="change-view-today">today</button>
                        <button class="btn btn-default btn-simple btn-round waves-effect" id="change-view-day" >Day</button>
                        <button class="btn btn-default btn-simple btn-round waves-effect" id="change-view-week">Week</button>
                        <button class="btn btn-default btn-simple btn-round waves-effect" id="change-view-month">Month</button>
                        <div id="calendar" class="m-t-20" style="max-width: 100% !important;"></div>
                    </div>
                </div>
				<div class="col-md-4 col-lg-4 col-xl-4">
                    <div class="body">
                        <button type="button" class="btn btn-round btn-info waves-effect" data-toggle="modal" data-target="#addevent">List of Appointments</button>
                        <button class="btn btn-default hidden-lg-up m-t-0 float-right" data-toggle="collapse" data-target="#open-events" aria-expanded="false" aria-controls="collapseExample"><i class="zmdi zmdi-chevron-down"></i></button>
                        <div class="collapse-xs collapse-sm collapse" id="open-events">
                            <hr>
                            <div class="event-name b-primary row">
                                <div class="col-2 text-center">
                                    <h4>11<span>Dec</span><span>2017</span></h4>
                                </div>
                                <div class="col-10">
                                    <h6>Conference</h6>
                                    <p>It is a long established fact that a reader will be distracted</p>
                                    <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                </div>
                            </div>                            
                            <div class="event-name b-primary row">
                                <div class="col-2 text-center">
                                    <h4>13<span>Dec</span><span>2017</span></h4>
                                </div>
                                <div class="col-10">
                                    <h6>Birthday</h6>
                                    <p>It is a long established fact that a reader will be distracted</p>
                                    <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                </div>
                            </div>
                            <hr>
                            <div class="event-name b-lightred row">
                                <div class="col-2 text-center">
                                    <h4>09<span>Dec</span><span>2017</span></h4>
                                </div>
                                <div class="col-10">
                                    <h6>Repeating Event</h6>
                                    <p>It is a long established fact that a reader will be distracted</p>
                                    <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                </div>
                            </div>
                            <hr>
                            <div class="event-name b-greensea row">
                                <div class="col-2 text-center">
                                    <h4>16<span>Dec</span><span>2017</span></h4>
                                </div>
                                <div class="col-10">
                                    <h6>Repeating Event</h6>
                                    <p>It is a long established fact that a reader will be distracted</p>
                                    <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                </div>
                            </div>
                            <div class="event-name b-greensea row">
                                <div class="col-2 text-center">
                                    <h4>28<span>Dec</span><span>2017</span></h4>
                                </div>
                                <div class="col-10">
                                    <h6>Google</h6>
                                    <p>It is a long established fact that a reader will be distracted</p>
                                    <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                </div>
                            </div>
							<hr>
							<div class="event-name b-lightred row">
                                <div class="col-2 text-center">
                                    <h4>09<span>Dec</span><span>2017</span></h4>
                                </div>
                                <div class="col-10">
                                    <h6>Repeating Event</h6>
                                    <p>It is a long established fact that a reader will be distracted</p>
                                    <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                </div>
                            </div>
							<hr>
							<div class="event-name b-greensea row">
                                <div class="col-2 text-center">
                                    <h4>28<span>Dec</span><span>2017</span></h4>
                                </div>
                                <div class="col-10">
                                    <h6>Google</h6>
                                    <p>It is a long established fact that a reader will be distracted</p>
                                    <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                </div>
                            </div>
							<hr>
							<div class="event-name b-primary row">
                                <div class="col-2 text-center">
                                    <h4>13<span>Dec</span><span>2017</span></h4>
                                </div>
                                <div class="col-10">
                                    <h6>Birthday</h6>
                                    <p>It is a long established fact that a reader will be distracted</p>
                                    <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                </div>
                            </div>
							<div class="event-name b-primary row">
                                <div class="col-2 text-center">
                                    <h4>13<span>Dec</span><span>2017</span></h4>
                                </div>
                                <div class="col-10">
                                    <h6>Birthday</h6>
                                    <p>It is a long established fact that a reader will be distracted</p>
                                    <address><i class="zmdi zmdi-pin"></i> 123 6th St. Melbourne, FL 32904</address>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<hr/>
			<div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <ul class="row profile_state list-unstyled">
                        <li class="col-lg-4 col-md-4 col-4">
                            <div class="body">
                                <i class="zmdi zmdi-eye col-amber"></i>
                                <h4>2,365</h4>
                                <span>Hours this month</span>
                            </div>
                        </li>
                        <li class="col-lg-4 col-md-4 col-4">
                            <div class="body">
                                <i class="zmdi zmdi-thumb-up col-blue"></i>
                                <h4>365</h4>
                                <span>Saldo this month</span>
                            </div>
                        </li>
                        <li class="col-lg-4 col-md-4 col-4">
                            <div class="body">
                                <i class="zmdi zmdi-comment-text col-red"></i>
                                <h4>65</h4>
                                <span>Active Students</span>
                            </div>
                        </li>                    
                    </ul>
                </div>
            </div>
        </div>
			<hr/>
			<div class="row clearfix">
            <div class="col-xl-12 col-lg-12 col-md-12">
                <div class="card">
                    <div class="header">
                        <h2>Reviews</h2>
                        
                    </div>
                    <div class="body">
                        <ul class="row list-unstyled c_review">
                            <li class="col-12">
                                <div class="avatar">
                                    <a href="javascript:void(0);"><img class="rounded" src="assets/images/sm/avatar2.jpg" alt="user" width="60"></a>
                                </div>                                
                                <div class="comment-action">
                                    <h6 class="c_name">Hossein Shams</h6>
                                    <p class="c_msg m-b-0">Cras sit amet nibh libero, in gravida nulla. Nulla vel metus scelerisque ante sollicitudin commodo. </p>
                                    <div class="badge badge-info">iPhone 8</div>
                                    <span class="m-l-10">
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                    </span>
                                    <small class="comment-date float-sm-right">Dec 21, 2017</small>
                                </div>                                
                            </li>
                            <li class="col-12">
                                <div class="avatar">
                                    <a href="javascript:void(0);"><img class="rounded" src="assets/images/sm/avatar3.jpg" alt="user" width="60"></a>
                                </div>                                
                                <div class="comment-action">
                                    <h6 class="c_name">Tim Hank</h6>
                                    <p class="c_msg m-b-0">It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout</p>
                                    <div class="badge badge-info">Nokia 8</div>
                                    <span class="m-l-10">
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star-outline text-muted"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star-outline text-muted"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star-outline text-muted"></i></a>
                                    </span>
                                    <small class="comment-date float-sm-right">Dec 18, 2017</small>
                                </div>                                
                            </li>
                            <li class="col-12">
                                <div class="avatar">
                                    <a href="javascript:void(0);"><img class="rounded" src="assets/images/sm/avatar4.jpg" alt="user" width="60"></a>
                                </div>                                
                                <div class="comment-action">
                                    <h6 class="c_name">Maryam Amiri</h6>
                                    <p class="c_msg m-b-0">There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour,</p>
                                    <div class="badge badge-info">Samsung Galaxy S8</div>
                                    <span class="m-l-10">
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star-outline text-muted"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star-outline text-muted"></i></a>
                                    </span>
                                    <small class="comment-date float-sm-right">Dec 18, 2017</small>
                                </div>                                
                            </li>
                            <li class="col-12">
                                <div class="avatar">
                                    <a href="javascript:void(0);"><img class="rounded" src="assets/images/sm/avatar5.jpg" alt="user" width="60"></a>
                                </div>                                
                                <div class="comment-action">
                                    <h6 class="c_name">Gary Camara</h6>
                                    <p class="c_msg m-b-0">Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                                    <div class="badge badge-info">HTC U11</div>
                                    <span class="m-l-10">
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star col-amber"></i></a>
                                        <a href="javascript:void(0);"><i class="zmdi zmdi-star-outline text-muted"></i></a>
                                    </span>
                                    <small class="comment-date float-sm-right">Dec 13, 2017</small>
                                </div>                                
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            
        </div>    
			
        </div>
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


	
	<!-- Jquery steps Wizard Code -->
	<script type="text/javascript">
	$(function () {
    //Advanced form with validation
    var form = $('#teacherRegistration').show();
    form.steps({
        headerTag: 'h4',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
          //  $.AdminOreo.input.activate();
            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
			
			
            if (currentIndex > newIndex) { return true; }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
			//Jquery AJAX code to add client upon  insertion
            
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            //Code for Forms Data Submission
			run_waitMe($('body'), 1, "timer");
			$.ajax({
                            url: 'actions/add_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {


                          
                                   switch(result)
                                    {

                                      case "Error":
                                    
                                        
                                        setTimeout(function() {   
                                                 
                                               $('body').waitMe('hide');
                                               $('#teacherRegistration')[0].empty(); 
                                                showNotification("alert-danger","Sorry!!! Your account cannot be created right now.","top","center","","");
                                               //$("#error-epm").html("There Was An Error Inserting The Record."); 
                                                  
}, 500); 
                                        break;

                                         case "Success":
                                    
                                        
                                       setTimeout(function() { 

                                                  $('body').waitMe('hide');
                                                  $('#teacherRegistration')[0].reset();
                                                  showNotification("alert-success","Kindly, check your email to activate your account","top","center","","");
                                                 
                                                                                                    
}, 500);
                                        break;

                                    
                                    }
                              }
                      });
		
        }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            'confirm': {
                equalTo: '#password'
            }
        }
    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}
		
		
		function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
        message: text
    },
        {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
}
		
	
	</script>  
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
<script src='assets/plugins/fullcalendar/lib/moment.min.js'></script>
<script src='assets/plugins/fullcalendar/fullcalendar.min.js'></script>
<script src='assets/plugins/fullcalendar/lib/moment.min.js'></script>
<!--<script src="assets/js/pages/calendar/calendar.js"></script>-->
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
<script>

  $(document).ready(function() {
	var today = moment().day();
    $('#calendar').fullCalendar({
      header: {
        left: 'prev,next today',
        center: 'title',
        right: 'agendaWeek'
      },
		    dayRender: function(date, cell) {
                    var today = $.fullCalendar.moment();
                    
                    if (date.get('date') == today.get('date')) {
                        cell.css("background-color", "transparent");
         }
    },
	editable:true,
      defaultDate: moment().format('YYYY-MM-DD'),
      defaultView: 'agendaWeek',
	  Duration: '00:30:00',
      navLinks: true, // can click day/week names to navigate views
      selectable: true,
      selectHelper: true,
	  events : 
		{
			url : 'includes/showSlots.php',
			cache: true
		},
	
		eventClick: function(calEvent, jsEvent, view) {
				if(calEvent.backgroundColor == 'red')
				{
				var teacher = <?php echo json_encode($_SESSION['TeacherID']) ?>;
				var start = calEvent.start;
				var end = calEvent.end;
				$.ajax({
                url: 'actions/students_search.php',
                data: {'teacher': teacher, 'start': start.toJSON(), 'end':end.toJSON()},
                type: "POST",
                success: function (response) {
				var res = JSON.parse(response);
                var fname1 = res.FirstName;
                var lname1 = res.LastName;
                var email = res.Email;
                var address = res.Address;
				var telephone = res.Telephone;
					$('#modalTitle').html("Slot Booking Information");
            $('#modalBody').html("Student Full Name: "+ fname1+" "+lname1+"<br/>Email: "+email+"<br/>Address: "+address+"<br/>Telephone: "+telephone);
					$('#calendar').fullCalendar( 'refetchEvents' );
					$('#fullCalModal').modal();
				}
             
            });
					
				}
  },
      select: function(start, end) {
        var title = prompt('Event Title:');
        var eventData;
		var choice = "teacherslot";
		var teacher = <?php echo json_encode($_SESSION['TeacherID']) ?>;
        if (title) {
          eventData = {
            title: title,
            start: start,
            end: end
          };
          $('#calendar').fullCalendar('renderEvent', eventData, true); // stick? = true
        }
		  $.ajax({
                url: 'actions/add_scripts.php',
                data: {'choice': choice, 'teacher': teacher, 'title': title, 'start': start.toJSON(), 'end':end.toJSON()},
                type: "POST",
                success: function (json) {
                    alert("Slot Added Successfully");
					$('#calendar').fullCalendar( 'refetchEvents' );
                }
            });
        $('#calendar').fullCalendar('unselect');
      },
      editable: true,
      eventLimit: true // allow "more" link when too many events
      /*events: [
        {
          title: 'All Day Event',
          start: '2019-01-01'
        },
        {
          title: 'Long Event',
          start: '2019-01-07',
          end: '2019-01-10'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2019-01-09T16:00:00'
        },
        {
          id: 999,
          title: 'Repeating Event',
          start: '2019-01-16T16:00:00'
        },
        {
          title: 'Conference',
          start: '2019-01-11',
          end: '2019-01-13'
        },
        {
          title: 'Meeting',
          start: '2019-01-12T10:30:00',
          end: '2019-01-12T12:30:00'
        },
        {
          title: 'Lunch',
          start: '2019-01-12T12:00:00'
        },
        {
          title: 'Meeting',
          start: '2019-01-12T14:30:00'
        },
        {
          title: 'Happy Hour',
          start: '2019-01-12T17:30:00'
        },
        {
          title: 'Dinner',
          start: '2019-01-12T20:00:00'
        },
        {
          title: 'Birthday Party',
          start: '2019-01-13T07:00:00'
        },
        {
          title: 'Click for Google',
          url: 'http://google.com/',
          start: '2019-01-28'
        }
      ]*/
    });
    $('#courses').multiselect();

  });

</script>
</body>
</html>
<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="margin-top: 0px !important;
padding-top: 0px !important;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php
}
?>