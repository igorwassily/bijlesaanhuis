<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
session_start();
if (!isset($_SESSION['AdminID']) || !isset($_SESSION['AdminUser'])) {
    header('Location: index.php');
    exit();
}

use Template\Cache;

require_once '../includes/connection.php';

require_once '../../../inc/template/Cache.php';

//echo '<pre>' . print_r($_POST, true) . '</pre>';
//global $con;

//$template = new API\ENDPOINT\Template($con);

// page attributes
$_POST = array_map("cleanArray", $_POST);
//echo '<pre>'.print_r($_POST).'</pre>';

$isLayout = (isset($_POST['template-file-directory']) && $_POST['template-file-directory'] === "layout");

$dir = $_POST['template-file-directory'] ? str_replace('/', '', $_POST['template-file-directory']) . "/" : null;
$filename = $_POST['template-file-name'];
$url = $dir . $filename;

$parentName = $con->real_escape_string(isset($_POST['template-name']) ? $_POST['template-name'] : "");
$templateTitle = $con->real_escape_string($_POST['header-page-title']);
$templateTitleSocialMedia = $con->real_escape_string($_POST['header-social-media-title']);
$templateDescription = $con->real_escape_string($_POST['header-page-description']);
$templateDescriptionSocialMedia = $con->real_escape_string($_POST['header-social-media-description']);
$query = $con->query("SELECT * FROM `page` WHERE `url`='$url'");
$errorMessage = "";
if (!$_POST['page-id'] && $query->fetch_assoc()) {
    redirectToBijlesTemplate(0, "Filename already exists.", null);
} else {
    $pageId = (isset($_POST['page-id']) && $_POST['page-id'] && $_POST['page-id'] !== 1) ? $_POST['page-id'] : null;
    if (isset($pageId)) {
        $query = $con->query("UPDATE `page` SET template='$parentName', `url`='$url', title='$templateTitle', `description`='$templateDescription', socialMediaTitle='$templateTitleSocialMedia', socialMediaDescription='$templateDescriptionSocialMedia' WHERE id=$pageId");
    } else {
        $query = $con->query("INSERT INTO `page` (template, `url`, title, `description`, socialMediaTitle, socialMediaDescription) VALUES ('$parentName','$url','$templateTitle','$templateDescription','$templateTitleSocialMedia','$templateDescriptionSocialMedia')");
    }
    // init sections
    try {
        $query = $con->query("SELECT id FROM `page` WHERE `url`='$url'");
        if ($pageId = $query->fetch_assoc()) {

            $sectionName = "card";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = [
                    "position" => $_POST["$sectionName-position"][$i]
                ];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "card-numbers";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = [
                    "position" => $_POST["$sectionName-position"][$i]
                ];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "card-price";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = [
                    "position" => $_POST["$sectionName-position"][$i]
                ];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "cardResponsiveSlider";
            for ($i = 0; isset($_POST["$sectionName"]) && $i < count($_POST["$sectionName"]); $i++) {
                $data = $_POST["$sectionName"][$i];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "card-slider";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $contents = array();
                for ($j = 0; $j < count($_POST["$sectionName-title"]); $j++) {
                    if ($_POST["$sectionName-title"][$i] && $_POST["$sectionName-subtitle"][$j] && $_POST["$sectionName-description"][$j]) {
                        $contents[] = array(
                            "title" => isset($_POST["$sectionName-title"][$j]) ? $_POST["$sectionName-title"][$j] : null,
                            "subtitle" => isset($_POST["$sectionName-subtitle"][$j]) ? $_POST["$sectionName-subtitle"][$j] : null,
                            "description" => isset($_POST["$sectionName-description"][$j]) ? $_POST["$sectionName-description"][$j] : null
                        );
                    };
                };
                $data = array(
                    "contents" => (isset($contents[0]) ? $contents : null),
                    "position" => $_POST["$sectionName-position"][$i]
                );
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "cardTopic";
            for ($i = 0; isset($_POST["$sectionName"]) && $i < count($_POST["$sectionName"]); $i++) {
                $data = $_POST["$sectionName"][$i];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "cardWide";
            for ($i = 0; isset($_POST["$sectionName"]) && $i < count($_POST["$sectionName"]); $i++) {
                $data = $_POST["$sectionName"][$i];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "contact-form";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = [
                    "position" => $_POST["$sectionName-position"][$i]
                ];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "contact-form-small";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = [
                    "position" => $_POST["$sectionName-position"][$i]
                ];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "contact-header";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = [
                    "position" => $_POST["$sectionName-position"][$i]
                ];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "counting-number";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = array(
                    "title" => isset($_POST["$sectionName-title"][$i]) ? $_POST["$sectionName-title"][$i] : null,
                    "unit" => isset($_POST["$sectionName-unit"][$i]) ? $_POST["$sectionName-unit"][$i] : null,
                    "subtitle" => isset($_POST["$sectionName-subtitle"][$i]) ? $_POST["$sectionName-subtitle"][$i] : null,
                    "position" => $_POST["$sectionName-position"][$i]
                );
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "followup-button";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = [
                    "title" => isset($_POST["$sectionName-title"][$i]) ? $_POST["$sectionName-title"][$i] : null,
                    "description" => isset($_POST["$sectionName-description"][$i]) ? $_POST["$sectionName-description"][$i] : null,
                    "buttonText" => isset($_POST["$sectionName-button-text"][$i]) ? $_POST["$sectionName-button-text"][$i] : null,
                    "buttonLink" => isset($_POST["$sectionName-button-link"][$i]) ? $_POST["$sectionName-button-link"][$i] : null,
                    "position" => $_POST["$sectionName-position"][$i]
                ];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "searchbar-header";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = array(
                    "title" => isset($_POST["$sectionName-title"][$i]) ? $_POST["$sectionName-title"][$i] : null,
                    "position" => $_POST["$sectionName-position"][$i]
                );
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "textInfoSection";
            for ($i = 0; isset($_POST["$sectionName"]) && $i < count($_POST["$sectionName"]); $i++) {
                $data = $_POST["$sectionName"][$i];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "tutoring-info";
            for ($i = 0; isset($_POST["$sectionName-position"]) && $i < count($_POST["$sectionName-position"]); $i++) {
                $data = array(
                    "title" => isset($_POST["$sectionName-title"][$i]) ? $_POST["$sectionName-title"][$i] : null,
                    "subtitle" => isset($_POST["$sectionName-subtitle"][$i]) ? $_POST["$sectionName-subtitle"][$i] : null,
                    "description" => isset($_POST["$sectionName-description"][$i]) ? $_POST["$sectionName-description"][$i] : null,
                    "hideButton" => getCheckboxValue("$sectionName-hide-button", $i),
                    "mediaLeft" => getCheckboxValue("$sectionName-media-left", $i),
                    "showVideo" => getCheckboxValue("$sectionName-show-video", $i),
                    "showTutors" => getCheckboxValue("$sectionName-show-tutors", $i),
                    "showImage" => getCheckboxValue("$sectionName-show-image", $i),
                    "videoSrcSet" => [["src" => (isset($_POST["$sectionName-video-src-set"][$i]) ? $_POST["$sectionName-video-src-set"][$i] : null)]],
                    "imageSrc" => isset($_POST["$sectionName-image-src"][$i]) ? $_POST["$sectionName-image-src"][$i] : null,
                    "buttonText" => isset($_POST["$sectionName-button-text"][$i]) ? $_POST["$sectionName-button-text"][$i] : null,
                    "buttonLink" => isset($_POST["$sectionName-button-link"][$i]) ? $_POST["$sectionName-button-link"][$i] : null,
                    "position" => $_POST["$sectionName-position"][$i]
                    //"imageSrcSet" => 
                );
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };

            $sectionName = "questionCollection";
            for ($i = 0; isset($_POST["$sectionName"]) && $i < count($_POST["$sectionName"]); $i++) {
                $data = $_POST["$sectionName"][$i];
                insertOrUpdate($con, $sectionName, $pageId['id'], $data, $i);
            };
        };
    } catch (Exception $e) {
        $errorMessage .= $e->getTraceAsString();
    }
    if (empty($errorMessage)) {
        redirectToBijlesTemplate(1, null, "$url");
    } else {
        redirectToBijlesTemplate(0, $errorMessage, null);
    }
}

function getCheckboxValue($key, $uniqueId)
{
    if (isset($_POST[$key])) {
        if (in_array($key . $uniqueId, $_POST[$key])) {
            return true;
        }
    }
    return false;
}

function insertOrUpdate($con, $sectionName, $pageId, $data, $i)
{
    //print_r($data);
    //$position = isset($_POST["$sectionName-position"][$i]) ? $_POST["$sectionName-position"][$i] : null;
    $position = $data['position'];
    $sectionId = (isset($_POST["$sectionName-id"]) && isset($_POST["$sectionName-id"][$i]) && $_POST["$sectionName-id"][$i]) ? $_POST["$sectionName-id"][$i] : null;
    if (!isset($sectionId)) {
        $position = isset($data['position']) ? $data['position'] : null;
        $sectionId = (isset($data['id']) && !empty($data['id'])) ? $data['id'] : null;
        if (count(explode('-', $sectionName)) < 2) {
            preg_match_all('/((?:^|[A-Z])[a-z]+)/', $sectionName, $matches);
            $sectionName = strtolower(implode("-", $matches[0]));
        }
    }
    $data = json_encode($data, JSON_UNESCAPED_UNICODE);
    if (isset($sectionId)) {
        if (!dbUpdateSection($con, $sectionName, $sectionId, $data, $position)) {
            printf("Error message: %s\n", $con->error);
        }
    } else {
        if (!dbInsertSection($con, $sectionName, $pageId, $data, $position)) {
            printf("Error message: %s\n", $con->error);
        }
    }
}

function dbInsertSection($con, $sectionName, $pageId, $data, $position)
{
    return $con->query("INSERT INTO section (component, `page`, position, `data`) VALUES ('$sectionName',$pageId,$position,'$data')");
}

function dbUpdateSection($con, $sectionName, $sectionId, $data, $position)
{
    return $con->query("UPDATE section SET position=$position, `data`='$data' WHERE id=$sectionId");
}

function redirectToBijlesTemplate($status, $message, $successLink)
{
    if (isset($successLink) && !empty($successLink)) {
        Cache::clear(str_replace('/', '_', $successLink));
    }
    header("Location: bijles-create-page.php?status=$status&message=$message&successLink=/$successLink");
}

function cleanArray($param)
{
    if (is_array($param)) {
        array_walk_recursive($param, function (&$a, $b) {
            $a = htmlspecialchars($a, ENT_QUOTES);
        });
    } else {
        $param = htmlspecialchars($param, ENT_QUOTES);
    }
    return $param;
}
function mySanitizer(&$value)
{
    $value = htmlspecialchars(trim($value));
}
/**/
