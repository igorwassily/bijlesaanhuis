<?php
$thisPage="new  Teachers registered";
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{
	
		
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>New Teacher Registeration</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	input:focus{	
background:transparent !important;
	}
	
	select:focus{	
background:transparent !important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	div.card>div.header{color:white;}
	select:focus{	
	border: 1px solid #bdbdbd !important;
	color: black !important;
}
select{	
	border: 1px solid #bdbdbd !important;
	color: #bdbdbd !important;
}

.text{
	color: black !important;
}
button.btn.dropdown-toggle.btn-round.btn-simple {
    width: 100px;
}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
	    require_once('includes/connection.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="header">
                        <h3>New Registered Teacher List </h3>
                    </div> -->
					
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px; color: #486066" id="teachers_approve" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th class="color">Voornaam</th>
                                        <th class="color">Achternaam</th>
                                        <th class="color">E-mailadres</th>
                                        <th class="color">Telefoonnummer</th>
										<!-- <th>Teacher Type</th> -->
                                        <th class="color">Adres</th>
                                        <th class="color">Skype</th>
                                        <th class="color">Kennismaking</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
	
											$enum_val = array();
											$enums_query = "SELECT * FROM `teacherlevel_rate` WHERE isdeleted=0";
											$e_result = mysqli_query($con,$enums_query);
											$rate_arr = ""; 
											while($row = mysqli_fetch_array($e_result)){
												$arr = array($row['ID'],$row['internal_level'], $row['internal_rate']);
												array_push($enum_val, $arr);
												$rate_arr .=  $row['internal_rate'].",";
											}	
											$rate_arr = rtrim($rate_arr,",");

											//$stmt = $con->prepare("SELECT u.userID, c.firstname, c.lastname, u.email, c.telephone, c.place_name FROM `user` u , contact c, teacher t WHERE usergroupID=2 AND DATEDIFF(CURDATE(), creation_datetime)<=".TUTOR_REGISTERED_IN_N_DAYS." AND disabled<>1 AND t.isConfirmed=0 AND t.teacherlevel_rate_ID IS NULL  AND u.userID=c.userID AND t.userID=c.userID");
											$stmt = $con->prepare('SELECT u.userID, c.firstname, c.lastname, u.email, c.telephone, c.place_name, c.skypeID, CONCAT(acb.datee, ", ", acb.starttime,"-",acb.endtime)ctime FROM `user` u LEFT JOIN contact c ON c.userID=u.userID LEFT JOIN teacher t ON t.userID=u.userID LEFT JOIN admincalendarbooking acb ON acb.teacherID=u.userID WHERE usergroupID=2 AND disabled<>1 AND t.isConfirmed=0 AND t.teacherlevel_rate_ID IS NULL');
	
					$stmt->execute();
					$stmt->bind_result($id,$firstname,$lastname,$email,$phone,$address,$skypeID, $ctime);
					$stmt->store_result();
					while($stmt->fetch())
					{
					    $q2 = $con->prepare( 'INSERT INTO admin_msg_read(type,userID) VALUES("'.NOTIFICATION_TEACHER_REGISTER.'", "'.$id.'")');
					    $q2->execute();


					?>
									<tr>
									
										<?php echo "<td><a href='../teacher-detailed-view.php?tid=$id'>$firstname </a></td>"; ?>
										<td class="color"><?php echo $lastname; ?></td>
										<td class="color"><?php echo $email; ?></td>		
										<td class="color"><?php echo $phone; ?></td>
								<!--		<td>
										 <select id="change-teacher-level" class="form-control show-tick" >
												<?php
												/*	echo "<option value=' '>Select Level</option>";
													$flag = false;
													$rate = " ";
													foreach($enum_val as $item) {
												      if($item[0] == $teacher_level)
													  {
													  	echo "<option value='".$item[0]."_$id' selected>".$item[1]."</option>";
														  $rate = $item[2];
													  }else 
													  {
													  	echo "<option value='".$item[0]."_$id'>".$item[1]."</option>";
													  }
													}	//foreach ends												
												*/
												?>												
											</select>
											<div class="alert alert-info" id="level-update-message" style="display:none; font-size:10px; border-radius:10px"></div>
										</td> -->
										<td class="color"><?php echo $address; ?></td>
                                        <td class="color"> <?php echo $skypeID; ?> </td>
										<td class="color"><?php echo $ctime; ?></td>						
										<td class="color">
											<a  href="actions/confirm_consultation?tid=<?php echo $id?>" >Goedkeuren</a> &nbsp;&nbsp;&nbsp;&nbsp;
											<a  href="../admin_edit_tutor_profile?tid=<?php echo $id?>" >Profiel</a> &nbsp;&nbsp;&nbsp;&nbsp;
											<a  href="#" class="delete" data-id="<?php echo $id?>">Verwijder</a>
										</td>				
									</tr>
				<?php }	?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
	
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="assets/js/pages/ui/notifications.js"></script>
<script type="text/javascript">
	$(document).ready(function() {
		
		
		
    $('#teachers_approve').DataTable({
            "language": {
                "aria": {
                    "sortAscending":  ": Oplopend sorteren",
                    "sortDescending": ": Aflopend sorteren"
                },
                "emptyTable":     "Geen gegevens beschikbaar in de tabel",
                "info": "Tonen van _START_ tot _END_ van _TOTAL_ inzendingen",
                "infoEmpty":      "Tonen van 0 tot 0 van 0 inzendingen",
                "infoFiltered":   "(Gefilterd van in totaal _MAX_ inzendingen)",
                "lengthMenu": "Toon _MENU_ inzendingen",
                "loadingRecords": "Laden...",
                "processing": "Verwerken...",
                "paginate": {
                    "first":"Eerste",
                    "last":"Laatste",
                    "previous": "Vorige",
                    "next": "Volgende"
                },
                "search":"Zoeken:",
                "zeroRecords": "Geen overeenkomende gegevens gevonden",
            }
        });
	});
		
	$(".delete").click(function(){
		var id = $(this).attr("data-id");
        if(confirm("Gebruiker verwijderen?")) {
            $.ajax({
                type: "POST",
                url: "admin-ajaxcalls.php",
                data: {subpage:"delete_teacher",id:id},
                success: function(response) {
                    if(response == "success"){

                        showNotification("alert-primary", "Successfully Deleted the tutor", "bottom", "center", "", "");
                        location.reload();
                    }else{
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                },
                error: function(xhr, ajaxOptions, thrownError){
                    if(xhr.response == "success"){

                        showNotification("alert-primary", "Successfully Deleted the tutor", "bottom", "center", "", "");
                        location.reload();
                    }else{
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                }
            });// ends ajax
        }
        else {
            return 0;
        }

	});
	
	
	var __tid = " ";
	var __level = " ";
	// fetching the teacher rate from 
	var teacher_levelRAte = Array(<?php echo $rate_arr; ?>);
$('select').on('change', function() {
	  var inp = this.value;
      if(inp != " "){
	  var t_level_id = inp.split("_");
		  __tid = t_level_id[1];
		  __level = (t_level_id[0])-1;
		  
  			$.ajax({
				url: 'actions/admin-ajaxcalls.php',
				data: {t_level:t_level_id[0], t_id:t_level_id[1]},
				type: "POST",
				//dataType: "json",
				success: function (result) {
					$(".rate_"+__tid).text(teacher_levelRAte[__level]);
					showNotification("alert-success", "Teacher Level is Successfully updated..!", "bottom", "center", "", "");
				}
			});	
	  }
});	
	
</script>

</body>
</html>
<?php
}
?>