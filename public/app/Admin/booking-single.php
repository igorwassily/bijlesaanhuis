<?php
session_start();
if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>Booking Single</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
	<style type="text/css">
		.form-control.form-control-sm:placeholder
		{
			color: white !important;
		}
	</style>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
	require_once('includes/connection.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Booking Single
              
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Booking Single</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
					
					  <div class="body">
                        <div class="table-responsive">
							<?php
					
	if(isset($_POST['booking-single-submit']))
	{
		$bookingID = test_input($_POST['booking-id']);
										$stmt = $con->prepare("select bookingID, bookingcode, branchID, clientID, agentID, hotelbookingid, transferbookingid, departuretransferbookingid, eventbookingid, arrival_date, arrival_by, arrival_flight, arrival_flight_time, departure_date, departure_by, departure_flight, departure_flight_time, adults, children, enfants, booking_files, guest_names, administration_note, operation_note, accounting_note, representative_note from booking where bookingID = ?");
		$stmt->bind_param("i",$bookingID);
										if($stmt->execute())
										{
											
											 $stmt->bind_result($bID,$bCode,$brID,$clID,$agID,$hbID,$tbID,$dtbID,$ebID,$adate,$aby,$aflight,$aflighttime,$ddate,$dby,$dflight,$dflighttime,$adults,$children,$enfants,$bFiles,$gNames,$anote,$onote,$acnote,$rnote);
											$stmt->store_result();
											$stmt->fetch();
										}
	
	?>
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" id="booking-single" cellspacing="0" width="100%">
                              <thead>
            <tr>
				<th>Booking ID</th>
				<th>Booking Code</th>
                <th>Branch ID</th>
                <th>Client ID</th>
                <th>Agent ID</th>
                <th>Hotel Booking ID</th>
				<th>Arrival Transfer Booking ID</th>
				<th>Departure Transfer Booking ID</th>
				<th>Event Booking ID</th>
				<th>Arrival Date</th>
				<th>Arrival By</th>
				<th>Arrival Flight</th>
				<th>Arrival Flight Time</th>
				<th>Departure Date</th>
				<th>Departure By</th>
				<th>Departure Flight</th>
				<th>Departure Flight Time</th>
				<th>Adults</th>
				<th>Children</th>
				<th>Enfants</th>
				<th>Booking Files</th>
				<th>Guest Names</th>
				<th>Administration Note</th>
				<th>Operation Note</th>
				<th>Accounting Note</th>
				<th>Representative Note</th>
				
				
            </tr>
        </thead>
        <tbody>
			<tr>
			<td><?php echo $bID; ?></td>
			<td><?php echo $bCode; ?></td>
			<td><?php echo $brID; ?></td>
			<td><?php echo $clID; ?></td>
			<td><?php echo $agID; ?></td>
			<td><?php echo $hbID; ?></td>
			<td><?php echo $tbID; ?></td>
			<td><?php echo $dtbID; ?></td>
			<td><?php echo $ebID; ?></td>
			<td><?php echo $adate; ?></td>
			<td><?php echo $aby; ?></td>
			<td><?php echo $aflight; ?></td>
			<td><?php echo $aflighttime; ?></td>
				<td><?php echo $ddate; ?></td>
			<td><?php echo $dby; ?></td>
			<td><?php echo $dflight; ?></td>
			<td><?php echo $dflighttime; ?></td>
			<td><?php echo $adults; ?></td>
			<td><?php echo $children; ?></td>
			<td><?php echo $enfants; ?></td>
			<td><?php echo $bFiles; ?></td>
			<td><?php echo $gNames; ?></td>
			<td><?php echo $anote; ?></td>
			<td><?php echo $onote; ?></td>
			<td><?php echo $acnote; ?></td>
			<td><?php echo $rnote; ?></td>
			</tr>
        </tbody>
   
    
                            </table>
							<?php
	}
	?>
                        </div>
                    </div>
					
					
					
                   
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
	<script type="text/javascript">
    $(document).ready(function() {
    $('#booking-single').DataTable();
} );
</script>
</body>
</html>
<?php
}
?>