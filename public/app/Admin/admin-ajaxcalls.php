<?php

require_once('includes/connection.php');
session_start();

error_reporting(E_ALL);
ini_set('display_errors', 'On');



if(isset($_POST['subpage'])){

		if($_POST['subpage'] == "delete_teacher"){
				$q = [];
				$q[] = "DELETE FROM applications WHERE userID = ".mysqli_real_escape_string($con, $_POST['id']);
				$q[] = "DELETE FROM teacher WHERE userID =".mysqli_real_escape_string($con, $_POST['id']);
				$q[] = "DELETE FROM student WHERE userID =".mysqli_real_escape_string($con, $_POST['id']);
				$q[] = "DELETE FROM contact WHERE userID =".mysqli_real_escape_string($con, $_POST['id']);
				$q[] = "DELETE FROM user WHERE userID = ".mysqli_real_escape_string($con, $_POST['id']);

				$succ = "";

				foreach ($q as $query){
					if(mysqli_query($con, $query)){
						$succ = "success";
					}
					else{
						$succ = "Failed" . $con->error;
						break;
					}
				}

				echo $succ;

			
		} //if $_POST subpage ends

		if($_POST['subpage'] == "activate_user"){
				$q1 = "UPDATE `user` SET `active`=$_POST[newActiveState] WHERE `userID` = '$_POST[id]'";
			
				if(mysqli_query($con, $q1)) 
					echo "success";
					   
				else
					echo "Failed" . $con->error; 
			
		} //if $_POST subpage ends
		
	
		if($_POST['subpage'] == "slotCreation"){
			
 			if($_POST['mon_stime'] != "" && $_POST['mon_etime'] != ""){
				$mon_time = $_POST['mon_stime']."-".$_POST['mon_etime'];
			}else
				$mon_time= "";

			if($_POST['tue_stime'] != "" && $_POST['tue_etime'] != ""){
 				$tue_time = $_POST['tue_stime']."-".$_POST['tue_etime'];
 			}else
 				$tue_time= "";

 			if($_POST['wed_stime'] != "" && $_POST['wed_etime'] != ""){
 				$wed_time = $_POST['wed_stime']."-".$_POST['wed_etime'];
 			}else
 				$wed_time= "";

 			if($_POST['thur_stime'] != "" && $_POST['thur_etime'] != ""){
 				$thur_time = $_POST['thur_stime']."-".$_POST['thur_etime'];
 			}else
 				$thur_time= "";

 			if($_POST['fri_stime'] != "" && $_POST['fri_etime'] != ""){			
				$fri_time = $_POST['fri_stime']."-".$_POST['fri_etime'];
 			}else
 				$fri_time= "";
			
			if($_POST['sat_stime'] != "" && $_POST['sat_etime'] != ""){
 				$sat_time = $_POST['sat_stime']."-".$_POST['sat_etime'];
 			}else
 				$sat_time= "";

 			if($_POST['sun_stime'] != "" && $_POST['sun_etime'] != ""){
 				$sun_time = $_POST['sun_stime']."-".$_POST['sun_etime'];
 			}
 			else
 				$sun_time	= "";

 			$months = $_POST['month'];
 			$date = $_POST['date'];


 			// return;

 			//to add months in current date
 			$end_date = date('Y-m-d', strtotime("+$months", strtotime($date)));

    		// $months = intval($_POST['month'])*30;

			// $dates = createRange($months);

			// $time = timeDiff($stime, $etime);

     		$q1 = "SELECT COUNT(slotID)total FROM adminslots WHERE adminID=".$_SESSION['AdminID'];
    		$r1 = mysqli_query($con, $q1);

    		$d = mysqli_fetch_assoc($r1);

    		if($d['total'] == 0){
    			$q = "INSERT INTO  adminslots (adminID, datee, `mon_time`,`tue_time`,`wed_time`,`thur_time`,`fri_time`,`sat_time`,`sun_time`, noofmonths) VALUES (".$_SESSION['AdminID'].", '".$date."', '".$mon_time."', '".$tue_time."', '".$wed_time."', '".$thur_time."', '".$fri_time."', '".$sat_time."', '".$sun_time."', '".$months."');";
				//echo $q;
				
				$stmt = mysqli_query($con, $q);
			
				if($stmt)
					echo "success";
				else
					echo "nothing";

    		}else{
    			$q = "UPDATE `adminslots` SET mon_time='".$mon_time."', tue_time='".$tue_time."', wed_time='".$wed_time."', thur_time='".$thur_time."', fri_time='".$fri_time."', sat_time='".$sat_time."', sun_time='".$sun_time."' , noofmonths='".$months."' WHERE adminID=".$_SESSION['AdminID'];
				//echo $q;
				
				$stmt = mysqli_query($con, $q);
			
				if($stmt)
					echo "update";
				else
					echo "nothing";


    		}
			
		}// ends if "slotCreation"
	
		
	else if($_POST['subpage'] == "cancelBook"){
			$query = "DELETE FROM `admincalendarbooking` WHERE `admincalendarbooking`.`admincalendarbookingID`=".$_POST['cbID'];
			$stmt = $con->prepare($query);
		
			if($stmt->execute())
				echo "success";
			else
				echo "fail";
		}
	
	
		else if($_POST['subpage'] == "cancelPending"){
			$query = "DELETE FROM `admincalendarbooking` WHERE `admincalendarbooking`.`admincalendarbookingID`=".$_POST['cbID'];
			$stmt = $con->prepare($query);
		
			if($stmt->execute())
				echo "success";
			else
				echo "fail";

		}
	
		else if($_POST['subpage'] == "slotCancellation"){
			$query = "DELETE FROM `admincalendarbooking` WHERE `admincalendarbooking`.`admincalendarbookingID`=".$_POST['cbID'];
			$stmt = $con->prepare($query);
		
			if($stmt->execute()){
				$_SESSION['isSlotBooked']=false;
				echo "success";
			}else
				echo "fail";

		}
	
	
		else if($_POST['subpage'] == "cancelfreeSlot"){
			$query = "INSERT INTO `admincalendarbooking` (`adminID`, `datee`, `starttime`, `endtime`, `isSlotCancelled`) VALUES ('".@$_SESSION['AdminID']."', '".@$_POST['ddate']."', '".$_POST['stime']."', '".$_POST['etime']."', 1);";
			$stmt = $con->prepare($query);
		
			if($stmt->execute())
				echo "success";
			else
				echo "fail";

		} //cancelfreeSlot ends
	
	
		else if($_POST['subpage'] == "reactivatefreeSlot"){
			$query = "DELETE FROM admincalendarbooking WHERE admincalendarbookingID=".$_POST['slotid'];
			$stmt = $con->prepare($query);
		
			if($stmt->execute())
				echo "success";
			else
				echo "fail";

		}// reactivatefreeSlot ends
		
		else if($_POST['subpage'] == "confrimPending"){
			$query = "UPDATE `admincalendarbooking` SET `accepted` = '1' WHERE `admincalendarbooking`.`admincalendarbookingID`=".$_POST['cbID'];
			$stmt = $con->prepare($query);
		
			if($stmt->execute())
				echo "success";
			else
				echo "fail";

		}// confrimPending ends
	
	
		else if($_POST['subpage'] == "teacher_pendingList"){
			$query = "SELECT ts.starttime AS stime, ts.endtime AS etime, ts.datee AS daatee, ts.slotID, cb.admincalendarbookingID AS cbid, cb.accepted, (SELECT CONCAT(c.firstname,\")*(\" ,c.lastname, \")*(\" , c.address) FROM contact c WHERE cb.teacherID=c.userID ORDER BY `daatee` DESC LIMIT 1)std FROM adminslots ts LEFT JOIN admincalendarbooking cb ON cb.slotID=ts.slotID where cb.accepted<>1 AND ts.adminID=".$_SESSION['AdminID'];

			$result = mysqli_query($con, $query);

			$str = "";
			while($row = mysqli_fetch_assoc($result)){
				$std = explode(")*(", $row['std']);

				$str .= "<td></td>
						<td>$std[0] $std[1]</td>
						<td>$row[daatee] $row[stime] - $row[etime]</td>
						<td>$std[2]</td>
						<td>
							<button type=\"button\" class=\"inline-elem-button btn btn-raised btn-primary btn-round waves-effect\" value=\"confirm\" onclick=\"confirmSlot(this, ".$row['cbid'].")\" >Confirm</button>
							<button type=\"button\" class=\"inline-elem-button btn btn-raised btn-primary btn-round waves-effect\" value=\"cancel\" onclick=\"cancelSlot(this, ".$row['cbid'].")\" >Cancel</button>
						</td>
						";
			}
		
			echo $str;

		}//ends if post teacher_pendingList

		else if($_POST['subpage'] == 'change_student_status'){
			$input = test_input($_POST['inp']);
			$userID = test_input($_POST['userID']);

			$sql = "UPDATE student SET status={$input} WHERE userID={$userID}";
			$result = mysqli_query($con, $sql);

			$sql2 = "UPDATE student SET label_date_set=CURDATE() WHERE userID={$userID}";
			mysqli_query($con, $sql2);



			if($result !== false){
				echo "Success";
			}
		}

		else if($_POST['subpage'] == 'change_notification_status_1'){
			$input = test_input($_POST['inp']);
			$cbID = test_input($_POST['cbID']);

			$sql = "UPDATE calendarbooking SET status={$input} WHERE calendarbookingID={$cbID}";
			$result = mysqli_query($con, $sql);

			if($result !== false){
				echo "Success";
			}
		}

		else if($_POST['subpage'] == 'change_notification_status_2'){
			$input = test_input($_POST['inp']);
			$eID = test_input($_POST['eID']);

			$sql = "UPDATE email SET status={$input} WHERE emailID={$eID}";
			$result = mysqli_query($con, $sql);

			if($result !== false){
				echo "Success";
			}
		}

		else if($_POST['subpage'] == 'change_label_docenten_1'){
			$input = test_input($_POST['inp']);
			$tID = test_input($_POST['tID']);

			$sql = "UPDATE teacher SET label_docenten_1={$input} WHERE userID={$tID}";
			$result = mysqli_query($con, $sql);

			$sql2 = "UPDATE teacher SET date_label_set_1=CURDATE() WHERE userID={$tID}";
			mysqli_query($con, $sql2);

			if($result !== false){
				echo "Success";
			}
		}

		else if($_POST['subpage'] == 'change_label_docenten_2'){
			$input = test_input($_POST['inp']);
			$tID = test_input($_POST['tID']);

			$sql = "UPDATE teacher SET label_docenten_2={$input} WHERE userID={$tID}";
			$result = mysqli_query($con, $sql);

			$sql2 = "UPDATE teacher SET date_label_set_2=CURDATE() WHERE userID={$tID}";
			mysqli_query($con, $sql2);

			if($result !== false){
				echo "Success";
			}
		}

		else if($_POST['subpage'] == 'change_custom_travelcosts'){
			$studentID = test_input($_POST['studentID']);
			$teacherID = test_input($_POST['teacherID']);
			$customRate = test_input($_POST['customRate']);

			$sql = "INSERT INTO custom_travel_costs (teacherID, studentID, customRate) VALUES ({$teacherID}, {$studentID}, {$customRate})";
			$result = mysqli_query($con, $sql);

			$sql = "UPDATE custom_travel_costs SET customRate = {$customRate} WHERE teacherID = {$teacherID} AND studentID = {$studentID}";
			$result2= mysqli_query($con, $sql);

			if($result2 !== false){
				echo "Success";
			}
		}



	
	if($_POST['subpage'] == "slotBooking"){
		
			if($_SESSION['isSlotBooked']){
				echo "Maximum One Booking can be made.";
				return;
			}
		
			$slotData = explode("&", $_POST['slotData']);
			 $teacherid = $_POST['teacherid'];

			$insertion_query = "INSERT INTO admincalendarbooking (adminID, calendarstatusID, datee, starttime, endtime,   accepted) VALUES ";
			foreach ($slotData as $data) {
				$d = explode(",", $data);
				$t = explode("-", $d[1]);
				
				$endtime = trim($t[1]);
				$starttime = trim($t[0]); 
				$insertion_query .= "(".ADMIN_ID.", ".$teacherid.", '".trim($d[0])."', '".$starttime."', '".$endtime."', 0),";
			}
			$insertion_query = rtrim($insertion_query, ",");
			// echo $insertion_query;

			$stmt = $con->prepare($insertion_query);
		
			if($stmt->execute()){
				$_SESSION['isSlotBooked'] = true;
				echo "success";
			}else
				echo "failed";

		}// ends if "slotBooking"
	
	elseif ($_POST['subpage'] == "read_student_first_lesson") {
		if (empty($_POST['id']) || !is_numeric($_POST['id'])) {
			die("error");
		}

		$id = $_POST['id'];
		if ($con->query("INSERT INTO admin_msg_read (`type`, userID) VALUES (1, $id)") !== false) {
			die("success");
		}
		die("error");
	}
}//if subpage ends


elseif(isset($_GET['tid'])){

	
		$reservedSlot = array();

		$q = "SELECT (if(calendarstatusID='$_GET[tid]', 1, 0))ownself,cb.* FROM admincalendarbooking cb WHERE cb.adminID=".ADMIN_ID." ORDER BY starttime DESC";

		$r = mysqli_query($con, $q);
		while($d = mysqli_fetch_assoc($r)){
				$index = $d['datee']." ".$d['starttime']." ".$d['endtime'];
				$reservedSlot[$index] = $d;
			}

		$query = "SELECT * FROM adminslots WHERE adminID=".ADMIN_ID." LIMIT 1";

		$result = mysqli_query($con, $query);

		$dd = mysqli_fetch_assoc($result);

		$mon_time = explode("-", $dd['mon_time']);
 			$tue_time = explode("-", $dd['tue_time']);
 			$wed_time = explode("-", $dd['wed_time']);
 			$thur_time = explode("-", $dd['thur_time']);
 			$fri_time = explode("-", $dd['fri_time']);
 			$sat_time = explode("-", $dd['sat_time']);
 			$sun_time = explode("-", $dd['sun_time']);

			$wkdays = getWeekdays($dd['mon_time'], $dd['tue_time'], $dd['wed_time'], $dd['thur_time'], $dd['fri_time'], $dd['sat_time'], $dd['sun_time']);


			// $dd['noofmonths']
			$dateRange = createRange(date('Y-m-d'), "31 Day", $wkdays);
			//var_dump($dateRange);

			$arrTimeDiff = array();
			$arrTimeDiff[1] = timeDiff($mon_time[0], $mon_time[1]);
			$arrTimeDiff[2] = timeDiff($tue_time[0], $tue_time[1]);
			$arrTimeDiff[3] = timeDiff($wed_time[0], $wed_time[1]);
			$arrTimeDiff[4] = timeDiff($thur_time[0], $thur_time[1]);
			$arrTimeDiff[5] = timeDiff($fri_time[0], $fri_time[1]);
			$arrTimeDiff[6] = timeDiff($sat_time[0], $sat_time[1]);
			$arrTimeDiff[7] = timeDiff($sun_time[0], $sun_time[1]);
			
			$str = "{
						\"success\": 1,
						\"result\":[";
			
			$current_date = new DateTime();
			$current_date->modify('+48 hour');
			$timeRange = "";
			//all time range for calender
			$checkBoxTimeArray = array();
	
	foreach ($dateRange as $day) {	
		//temp time range for loop-specific day
		$timeArray = array();
		$tempDay = new DateTime($day);
		$timeDiff = (isset($arrTimeDiff[$tempDay->format("N")]['stime']))? $arrTimeDiff[$tempDay->format("N")] : 0;
		if ($timeDiff == 0) continue;
		
		// first and last index of array
		$timeRange = $timeDiff['stime'][0]." - ".end($timeDiff['etime']);
		
		for($i=0; $i<count($timeDiff['stime']); $i++){
			$index = $day." ".$timeDiff['stime'][$i]." ".$timeDiff['etime'][$i];

			// Default time(dropdown) is unavailable, it will be set as available if slot is free.
					$timeArray[$timeDiff['stime'][$i]] = "unavailable";
			
					if(isset($reservedSlot[$index])){
// echo "reservedSlot[index]['ownself']".$reservedSlot[$index]['ownself'];
// echo "reservedSlot[ index]['accepted']".$reservedSlot[$index]['accepted'];
// echo "<br/>";

				$id=  $reservedSlot[$index]["admincalendarbookingID"];
				$status= $reservedSlot[$index]["isSlotCancelled"];
				$sdate = $day." ".$timeDiff['stime'][$i];
				$edate = $day." ".$timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i]." - ".$timeDiff['etime'][$i];

				if($reservedSlot[$index]["isSlotCancelled"] == 1){
					continue;
				}
				if($reservedSlot[$index]['ownself'] == 1 && $reservedSlot[$index]["accepted"] == 1){ 
					$title_desc = $title = "Slot Booked";
					$class = "event-info";
					$onHoverBtn = "(this, $id)";

				}elseif($reservedSlot[$index]['ownself'] == 1 && $reservedSlot[$index]["accepted"] != 1){ 
					$title_desc = $title = "Confirmation Pending";
					$class = "event-warning";
					$onHoverBtn = "(this, $id)";
				}
				elseif($reservedSlot[$index]['ownself'] == 0){
					$title_desc = $title = "Reversed";
					$class = "event-danger";
					$onHoverBtn = "";
					$title_desc = "Reserved Slot";
					continue;
				}
			}
			else{
				
				$timeArray[$timeDiff['stime'][$i]] = "available";
				
				$title = "Beschikbaar";
				$title_desc = "Beschikbaar";
				$class = "event-inverse";

				$id= '';
				$status=0;
				$sdate = $day." ".$timeDiff['stime'][$i];
				$edate = $day." ".$timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i]." - ".$timeDiff['etime'][$i];
				$onHoverBtn = "";

			}

			if($day <= $current_date->format("Y-m-d")){
						continue;
						$onHoverBtn = "";
					}

			$str .= "{
					\"id\": \"$id\",
					\"cbID\": \"$id\",
					\"start\": \"".strtotime($sdate)."000\",
					\"end\": \"".strtotime($edate)."000\",
					\"time\": \"$time\",
					\"timeRange\": \"$timeRange\",
					\"date\": \"$day\",
					\"status\": \"$status\",
					\"class\": \"".$class."\",
					\"title\": \"".$title."\",
					\"title_desc\": \"".$title_desc."\",
					\"onHoverBtn\": \"".$onHoverBtn."\",
					\"url\": \"\"
				},";

		}//for loop ends
		
		//inserting all time range
		$checkBoxTimeArray[$day] = $timeArray;
		
	}// for each loop ends

			$checkBoxTimeArray = json_encode($checkBoxTimeArray);

			$str = rtrim($str, ",");
			$str .= "], \"timeRange\": $checkBoxTimeArray}";    //$checkBoxTimeArray
	
			echo $str;

	}//if $_GET adminID


elseif(isset($_GET['dashboard'])){
			$reservedSlot = array();
			
	 	$q = "SELECT cb.*, (SELECT CONCAT(c.firstname,\" \" ,c.lastname) FROM contact c WHERE cb.teacherID=c.userID ORDER BY `datee` DESC LIMIT 1)std FROM admincalendarbooking cb WHERE cb.adminID=".@$_SESSION['AdminID']." ORDER BY datee, starttime DESC";

			$r = mysqli_query($con, $q);
			while($d = mysqli_fetch_assoc($r)){
				$index = $d['datee']." ".$d['starttime']." ".$d['endtime'];
				// echo $index;

				$reservedSlot[$index] = $d;
			}

			$query = "SELECT * FROM adminslots WHERE adminID=".$_SESSION['AdminID']." LIMIT 1";
			$result = mysqli_query($con, $query);

			$dd = mysqli_fetch_assoc($result);
	
			$mon_time = explode("-", $dd['mon_time']);
 			$tue_time = explode("-", $dd['tue_time']); 
 			$wed_time = explode("-", $dd['wed_time']);
 			$thur_time = explode("-", $dd['thur_time']);
 			$fri_time = explode("-", $dd['fri_time']);
 			$sat_time = explode("-", $dd['sat_time']);
 			$sun_time = explode("-", $dd['sun_time']);

			$wkdays = getWeekdays($dd['mon_time'], $dd['tue_time'], $dd['wed_time'], $dd['thur_time'], $dd['fri_time'], $dd['sat_time'], $dd['sun_time']);
			
			$dateRange = createRange($dd['datee'], $dd['noofmonths'], $wkdays);

			$arrTimeDiff = array();
			$arrTimeDiff[1] = timeDiff($mon_time[0], $mon_time[1]);
			$arrTimeDiff[2] = timeDiff($tue_time[0], $tue_time[1]);
			$arrTimeDiff[3] = timeDiff($wed_time[0], $wed_time[1]);
			$arrTimeDiff[4] = timeDiff($thur_time[0], $thur_time[1]);
			$arrTimeDiff[5] = timeDiff($fri_time[0], $fri_time[1]);
			$arrTimeDiff[6] = timeDiff($sat_time[0], $sat_time[1]);
			$arrTimeDiff[7] = timeDiff($sun_time[0], $sun_time[1]);
			
			$str = "{
						\"success\": 1,
						\"result\":[";
			
			$current_date = new DateTime();
			
			foreach ($dateRange as $day) {	
				
				$tempDay = new DateTime($day);
				
				// if admin has not set any available slot on this day then move to next day
				if($arrTimeDiff[$tempDay->format("N")] == 0)
					continue;
	
				
				$timeDiff = (isset($arrTimeDiff[$tempDay->format("N")]['stime']))? $arrTimeDiff[$tempDay->format("N")] : 0;
				
				for($i=0; $i<count($timeDiff['stime']); $i++){
					$index = $day." ".$timeDiff['stime'][$i]." ".$timeDiff['etime'][$i];
					// echo $index;
					if(isset($reservedSlot[$index])){


						$id=  $reservedSlot[$index]["admincalendarbookingID"];
						$status= $reservedSlot[$index]["isSlotCancelled"];
						$sdate = $day." ".$timeDiff['stime'][$i];
						$edate = $day." ".$timeDiff['etime'][$i];
						$time = $timeDiff['stime'][$i]." - ".$timeDiff['etime'][$i];

						if($reservedSlot[$index]["isSlotCancelled"] == 1){
							$title = "Cancelled Slot";
							$title_desc = "Slot Free";
							$class = "event-disabled";
							$onHoverBtn = "(this, $id, 3)";
							
						}
						elseif($reservedSlot[$index]["accepted"] == 1){
							$title = "Slot Booked";
							$title_desc = "<a href='teacher-profile?tid=".$reservedSlot[$index]['teacherID']."'>".$reservedSlot[$index]['std']."</a> ";
							$class = "event-info";
							$onHoverBtn = "(this, $id, 0)";

						}elseif($reservedSlot[$index]["accepted"] != 1){
							$title = "Confirmation Pending";
							$title_desc = "<a href='teacher-profile?tid=".$reservedSlot[$index]['teacherID']."'>".$reservedSlot[$index]['std']."</a> ";
							$class = "event-warning";
							$onHoverBtn = "(this, $id, 1)";
						}
					}
					else{
						$title = "Beschikbaar";
						$title_desc = "Beschikbaar";
						$class = "event-inverse";
						$onHoverBtn = "(this, '', 2)";

						$id= '';
						$status=0;
						$sdate = $day." ".$timeDiff['stime'][$i];
						$edate = $day." ".$timeDiff['etime'][$i];
						$time = $timeDiff['stime'][$i]." - ".$timeDiff['etime'][$i];
						$onHoverBtn = "(this, 0, 2)";
					}
					
					if($day < $current_date->format("Y-m-d")){
						$onHoverBtn = "";
					}

					$str .= "{
						\"id\": \"$id\",
						\"cbID\": \"$id\",
						\"start\": \"".strtotime($sdate)."000\",
						\"end\": \"".strtotime($edate)."000\",
						\"time\": \"$time\",
						\"date\": \"$day\",
						\"status\": \"$status\",
						\"class\": \"".$class."\",
						\"title\": \"".$title."\",
						\"title_desc\": \"".$title_desc."\",
						\"onHoverBtn\": \"".$onHoverBtn."\",
						\"designation\": \"Teacher\",
						\"url\": \"\"
					},";

				}//for loop ends
			}// for each loop ends

			$str = rtrim($str, ",");
			$str .= "]}";

	echo $str;

		}//if GET dashboard ends


elseif(isset($_GET['teacherdashboard'])){ 
			$reservedSlot = array();
			
			
			$q = "SELECT cb.*, (SELECT CONCAT(MIN(ccb.starttime),\" - \",MAX(ccb.endtime)) FROM calendarbooking ccb WHERE ccb.slot_group_id=cb.slot_group_id AND ccb.isgapslot=0 GROUP BY ccb.slot_group_id )timee,(SELECT CONCAT(c.firstname,\" \" ,c.lastname) FROM contact c WHERE cb.studentID=c.userID AND contacttypeID=2 ORDER BY `datee` DESC LIMIT 1)std FROM calendarbooking cb WHERE cb.teacherID=".$_GET['ttid']."  ORDER BY datee, starttime ASC";

			$r = mysqli_query($con, $q);
			while($d = mysqli_fetch_assoc($r)){
				$index = $d['datee']." ".$d['starttime']." ".$d['endtime'];
				$reservedSlot[$index] = $d;
			}

			$query = "SELECT * FROM teacherslots WHERE teacherID=".$_GET['ttid']." LIMIT 1";
			$result = mysqli_query($con, $query);

			$dd = mysqli_fetch_assoc($result);
	
			$mon_time = explode("-", $dd['mon_time']);
 			$tue_time = explode("-", $dd['tue_time']);
 			$wed_time = explode("-", $dd['wed_time']);
 			$thur_time = explode("-", $dd['thur_time']);
 			$fri_time = explode("-", $dd['fri_time']);
 			$sat_time = explode("-", $dd['sat_time']);
 			$sun_time = explode("-", $dd['sun_time']);

			$wkdays = getWeekdays($dd['mon_time'], $dd['tue_time'], $dd['wed_time'], $dd['thur_time'], $dd['fri_time'], $dd['sat_time'], $dd['sun_time']);
			
			$dateRange = createRange($dd['datee'], $dd['noofmonths'], $wkdays);

			$arrTimeDiff = array();
			$arrTimeDiff[1] = timeDiff($mon_time[0], $mon_time[1]);
			$arrTimeDiff[2] = timeDiff($tue_time[0], $tue_time[1]);
			$arrTimeDiff[3] = timeDiff($wed_time[0], $wed_time[1]);
			$arrTimeDiff[4] = timeDiff($thur_time[0], $thur_time[1]);
			$arrTimeDiff[5] = timeDiff($fri_time[0], $fri_time[1]);
			$arrTimeDiff[6] = timeDiff($sat_time[0], $sat_time[1]);
			$arrTimeDiff[7] = timeDiff($sun_time[0], $sun_time[1]);
			
			$str = "{
						\"success\": 1,
						\"result\":[";
			
			$current_date = new DateTime();
			
			$slotGID = 0;
			$startTime = "";
			$endTime ="";
			$toPrint = false;
			$time ="";
			
			foreach ($dateRange as $day) {	
				$tempDay = new DateTime($day);
				$timeDiff = (isset($arrTimeDiff[$tempDay->format("N")]['stime']))? $arrTimeDiff[$tempDay->format("N")] : 0;
				
				for($i=0; $i<count($timeDiff['stime']); $i++){
					$index = $day." ".$timeDiff['stime'][$i]." ".$timeDiff['etime'][$i];

					if(isset($reservedSlot[$index])){



						$id=  $reservedSlot[$index]["calendarbookingID"];
						$status= $reservedSlot[$index]["isSlotCancelled"];
						$sdate = $day." ".$timeDiff['stime'][$i];
						$edate = $day." ".$timeDiff['etime'][$i];
						$time = $timeDiff['stime'][$i]." - ".$timeDiff['etime'][$i];
						
									
				if($reservedSlot[$index]["isgapslot"] == 1 ){
					continue;
				}
						
				if($reservedSlot[$index]["slot_group_id"] == $slotGID){
					continue;
				}
					
				if($reservedSlot[$index]["slot_group_id"] != $slotGID){
					$time = $reservedSlot[$index]["timee"];
					$slotGID = $reservedSlot[$index]["slot_group_id"];
					$toPrint = true;
				}
										

						if($reservedSlot[$index]["isSlotCancelled"] == 1){
							$title = "Cancelled Slot";
							$title_desc = "Slot Free";
							$class = "event-disabled";
							$onHoverBtn = "(this, $slotGID, 3)";
							
						}
						elseif($reservedSlot[$index]["accepted"] == 1){
							$title = "Slot Booked";
							$title_desc = "<a href='student-profile?sid=".$reservedSlot[$index]['studentID']."'>".$reservedSlot[$index]['std']."</a>, ".$reservedSlot[$index][place_name].".";
							$class = "event-info";
							$onHoverBtn = "(this, $slotGID, 0)";

						}elseif($reservedSlot[$index]["accepted"] != 1){
							$title = "Confirmation Pending";
							$title_desc = "<a href='student-profile?sid=".$reservedSlot[$index]['studentID']."'>".$reservedSlot[$index]['std']."</a>, ".$reservedSlot[$index][place_name].".";
							$class = "event-warning";
							$onHoverBtn = "(this, $slotGID, 1)";
						}
					}
					else{
						
						if($day < $current_date->format("Y-m-d")){
							continue;
						}
						
						$title = "Slot Free";
						$title_desc = "Slot Free";
						$class = "event-inverse";
						$onHoverBtn = "(this, '', 2)";

						$id= '';
						$status=0;
						$sdate = $day." ".$timeDiff['stime'][$i];
						$edate = $day." ".$timeDiff['etime'][$i];
						$time = $timeDiff['stime'][$i]." - ".$timeDiff['etime'][$i];
						$onHoverBtn = "(this, 0, 2)";
					}
					
					if($day < $current_date->format("Y-m-d")){
						//echo $day ." < ".$current_date->format("Y-m-d"). "\n";
						$onHoverBtn = "";
					}
					
					
						$str .= "{
							\"id\": \"$slotGID\",
							\"cbID\": \"$slotGID\",
							\"start\": \"".strtotime($sdate)."000\",
							\"end\": \"".strtotime($edate)."000\",
							\"time\": \"$time\",
							\"date\": \"$day\",
							\"status\": \"$status\",
							\"class\": \"".$class."\",
							\"title\": \"".$title."\",
							\"title_desc\": \"".$title_desc."\",
							\"onHoverBtn\": \"".$onHoverBtn."\",
							\"designation\": \"Teacher\",
							\"url\": \"\"
						},";
					
				}//for loop ends
				
					$startTime = "";					
					$endTime = "";
					$toPrint = false;
			}// for each loop ends
			

			$str = rtrim($str, ",");
			$str .= "]}";

			echo $str;

		}//if GET teacher-dashboard ends

elseif(isset($_POST['consul'])){
	$consulType = str_replace("0", "", $_POST['consul']);
	$teacherID = $_POST['teacherID'];


	$q = "UPDATE teacher SET consulationType =" . $consulType . " WHERE userID=" . $teacherID;
	$r = mysqli_query($con, $q);


}
elseif(isset($_POST['hType'])){
	$hType = $_POST['hType'];
	$teacherID = $_POST['teacherID'];


	$q = "UPDATE teacher SET reminder =" . $hType . " WHERE userID=" . $teacherID;
	$r = mysqli_query($con, $q);
}

//-------------------
//-----Function------
//-------------------


function timeDiff($t1, $t2){
	//if time is same, it means admin is not available
	if($t1 == $t2){
		return 0;
	}
    $time1 =  strtotime($t1);
    $time2 =  strtotime($t2);
    $diff = intval(($time2-$time1)/3600); //from sec to min
    $slot_time = SLOT_TIME_IN_MINIUTES;//45;
    $gap = SLOT_TIME_GAP_IN_MINIUTES + SLOT_TIME_IN_MINIUTES; //15
    $sArray = array();
    $eArray = array();
    $new = $time1;
    $tmp_etime = strtotime("+$slot_time minute", $new);
    do {
        $sArray[] = date("h:i A", $new);
        $eArray[] = date("h:i A", $tmp_etime);
        $new = strtotime("+$gap minute", $new);
        $tmp_etime = strtotime("+$gap minute", $tmp_etime);
		$diff--;
		$newEndTime = strtotime("+$gap minute", $tmp_etime);
    } while ($time2 >= $newEndTime);
	
	$sArray[] = date("h:i A", $new); 
	$eArray[] = date("h:i A", $tmp_etime);

    return array("diff"=>$diff, "stime"=>$sArray, "etime"=>$eArray);
}

function createRange($s, $n, $wkdays) {
    $tmpDate = new DateTime($s);	
	$tmpDate->modify("-0 Month");
	$tmpEndDate =  new DateTime();
	$tmpEndDate->modify("+$n");
   
    $outArray = array();
    do {

    	//add selected day
    	if(strpos($wkdays, $tmpDate->format('D')) > -1){
    		// echo $tmpDate->format('D')." : ".strpos($wkdays, $tmpDate->format('D'));
        	$outArray[] = $tmpDate->format('Y-m-d');
    	}
 
    } while ($tmpDate->modify('+1 day') <= $tmpEndDate);
    return $outArray;
}


function getWeekdays($m, $t, $w, $th, $f, $sat, $sun){
	$str = "";
	$str .= ($m)? "Mon" : "";
	$str .= ($t)? ",Tue" : "";
	$str .= ($w)? ",Wed" : "";
	$str .= ($th)? ",Thur" : "";
	$str .= ($f)? ",Fri" : "";
	$str .= ($sat)? ",Sat" : "";
	$str .= ($sun)? ",Sun" : "";
	return $str;
}

?>