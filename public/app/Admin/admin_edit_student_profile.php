<?php
$thisPage = "Edit Profile";
session_start();
if (!isset($_SESSION['AdminUser'])) {
	header('Location: index.php');
	die();
}

function translateToDutch($val)
{
	//$permission = array("Accepted"=>"Geaccepteerd", "Rejected"=>"Afgewezen", "Requested"=>"In behandeling");
	//return (isset($permission[$val]))? $permission[$val]) : "";
	if ($val == "Accepted")
		return "Geaccepteerd";
	else if ($val == "Rejected")
		return "Afgewezen";
	else if ($val == "Requested")
		return "In behandeling";
	return $val;
}

?>

<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>Wijzig profiel | Bijles Aan Huis</title>

    
	<?php
	require_once('includes/connection.php');
	require_once('includes/mainCSSFiles.php');
	?>
    <!-- Custom Css -->
    <link rel="stylesheet" href="/app/assets/css/main.css">
    
    <link href="/app/Admin/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="/app/Admin/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="/app/Admin/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='/app/Admin/assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='/app/Admin/assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="/app/Admin/assets/css/bootstrap-multiselect.css" type="text/css">
	
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>

    <link rel="stylesheet" href="/app/assets/css/chatapp.css">

<style type="text/css">
    	.mapboxgl-ctrl-attrib, .mapboxgl-ctrl-logo{display:none !important;}	
		.mapboxgl-ctrl-geocoder li > a {color: black !important;}
		.mapboxgl-ctrl-geocoder input {color: black !important}
	
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;
	border-radius:6px !important;

	}
	label
	{
	font-size: 16px !important;
	margin-left: 2px !important;
	}
	select{	
border: 1px solid #bdbdbd !important;

/*color: white !important; */
	}
	.fa, .far, .fas {
    font-family: "Font Awesome 5 Free" !important;
	font-size:16px !important;
	color: #bbb !important;
}
	.header-radius
	{
		border-top-left-radius: 6px !important;
		border-top-right-radius: 6px !important;		
	}
	.body-radius
	{
		border-bottom-left-radius: 6px !important;
		border-bottom-right-radius: 6px !important;	
	}
/*	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	*/
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}
	
	.body{border: 2px solid #f1f1f1;}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	
	
	.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
    color: black !important;
}
	textarea
	{
		color: white !important;
height: 100px !important;
border: 1px solid #465059 !important;
	}
	
	textarea:focus
	{
		color: black !important;
	}
		
	.btn-block
	{
		width: 40% !Important;
	}
	.errorLabel, .terrorLabel, .terrorLabel2{
		color: red;
	}
	.hide{
		display:none;
	}
	

	.theme-green .btn-primary {margin:auto;}
.card .body {
    background-color: #FAFBFC !important;
}

        
.multiselect.dropdown-toggle.btn.btn-default {
            display: none !important;
        }
        .edit-profile-content__group input {
            max-width:100%;
        }
        /* ADMIN CSS */

        .navbar.p-l-5.p-r-5 {
            display: none !important;
        }

        .card {
            border-radius: 10px !important;
            background-color: transparent;
        }

        .chat .chat-header {
            border-bottom: 0 solid #486066;
        }

        .chat {
            margin-left: 0 !important;
            margin-bottom: 0 !important;
            border-left: none !important;
        }

        #adminNote ~ label {
            display: none;
        }

        input {
            color: black!important;
        }

        .sidebar .menu .list li:not(:first-child) a {
            color: #fff;
        }

        .theme-green .sidebar .menu .list li.active > :first-child i, .theme-green .sidebar .menu .list li.active > :first-child span {
           color: #50d38a;
        }

        .m-l-0, .ls-toggle-menu section.content, .ls-closed section.content, .overlay-open .sidebar {
            margin-left: 0px !important;
        }
        /* .theme-green section.content::before {
            background-color: #5cdb94;
        } */
        .edit-profile-form {
            position: relative;
        }
        .navbar-header h2 {
            margin-left: 30px !important;
        }
        .block-header {
            padding: 0;
            margin-top: -20px;
        }
        .block-header {
            margin-bottom: 20px;
        }
        .block-header h2 {
            padding-top:20px;
        }
        .nav.navbar-nav li:first-child {
            z-index:1;
        }
    </style>
	<?php
	$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                 alt="Oreo"></div>
        <p>Please wait...</p>
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php
require_once('includes/header.php');
require_once('includes/sidebarAdminDashboard.php');
?>

<style type="text/css">
    body section.edit-profile-content {
        margin: 0px 0 0 250px !important;
    }

    /* section.content::before {
        background-color: transparent !important;
    } */

    section.content form {
        width: 100%;
    }

    #pbe-footer-wa-wrap {
        display: none;
    }

    table, .btn, .btn:hover, .btn:focus {
        color: rgb(72, 96, 102)!important;
    }

    input {
        background: #fafbfc !important;
    }
</style>
    <?php
        require_once('../includes/edit_student_profile.php');
    ?>

<?php
    require_once('includes/footerScripts.php');
?>
	
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="assets/js/pages/ui/notifications.js"></script>
</body>
</html>