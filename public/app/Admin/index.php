﻿<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
    <title>BIJLES AAN HUIS Login</title>
    <!-- Favicon-->
    <link rel="icon" href="favicon.ico" type="image/x-icon">
    <!-- Custom Css -->
    <link rel="stylesheet" href="assets/css/color_skins.css">
    <link rel="stylesheet" href="assets/css/authentication.css">
    <link rel="stylesheet" href="assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="assets/css/main.css">
</head>

<body class="theme-green authentication sidebar-collapse">
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
    <div class="container">        
        <div class="navbar-translate n_logo">
           
           
        </div>
        
    </div>
</nav>
<!-- End Navbar -->
<div class="page-header">
    <div class="page-header-image" ></div>
    <div class="container">
        <div class="col-md-12 content-center">
				
            <div class="card-plain">
				<!-- Error Message -->
			<?php
				require_once("includes/connection.php");
								if(isset($_GET['msg']))
								{
									?>
							<div class="bs-example" style="font-size:10px;">
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo test_input($_GET['msg']); ?>
    </div>
</div>
							<?php
								}
				
								if(isset($_GET['successMsg']))
								{
									?>
							<div class="bs-example" style="font-size:10px;">
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo test_input($_GET['successMsg']); ?>
    </div>
</div>
				<?php
								}
							?>
            
			    <form class="form" method="post" action="actions/login">
                    <div class="header">
                        <div class="logo-container">
                        	<h2>BIJLES AAN HUIS</h2>
                            <!--<img src="img/Logo01.png" alt="">-->
                        </div>
                        <h5>Admin Log in</h5>
                    </div>
                    <div class="content">                                                
                        <div class="input-group input-lg">
                            <input type="email" class="form-control" placeholder="Email" name="username" required/>
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                        <div class="input-group input-lg">
                            <input type="password" placeholder="Password" class="form-control" name="password" required maxlength="20" minlength="8" title="Password must contains Minimum 8 characters" />
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                    </div>
                    <div class="footer text-center">
						<button type="submit" name="login" class="btn btn-primary btn-round btn-lg btn-block">SIGN IN</button>
                      
                        <h5><a href="forgotPassword" class="link">Forgot Password?</a></h5>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <footer class="footer">
      
    </footer>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>
</body>
</html>