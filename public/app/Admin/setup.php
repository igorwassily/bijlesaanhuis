﻿<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

<title>Setup</title>
<?php
        require_once('includes/mainCSSFiles.php');
?>
</head>
<body class="theme-orange">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>

<!-- Overlay For Sidebars -->
<div class="overlay"></div>

<?php
        require_once('includes/header.php');
        require_once('includes/sidebar.php');
?>

<!-- Main Content -->
<section class="content">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-7 col-md-6 col-sm-12">
                <h2>Setup
                
                </h2>
            </div>
            <div class="col-lg-5 col-md-6 col-sm-12">                
                <button class="btn btn-white btn-icon btn-round hidden-sm-down float-right m-l-10" type="button">
                    <i class="zmdi zmdi-plus"></i>
                </button>
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i></a></li>
                    <li class="breadcrumb-item active">Setup</li>
                </ul>                
            </div>
        </div>
    </div>
    <div class="container-fluid">
		<div class="input-group">                
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-addon">
                    <i class="zmdi zmdi-search"></i>
                </span>
            </div>
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <div class="body">
							
								<div class="row clearfix">
								
							
							<div class="col-lg-6 text-center">
								<div class="body m-b-10" style="background-color:white !important;color:black!important;">
                                    <div class="demo-google-material-icon" > <i class="material-icons">settings</i><br/><span class="icon-name">GENERAL SETTINGS</span> </div>
                                </div>
							</div>
									<div class="col-lg-6 text-center">
								<div class="body m-b-10" style="background-color:white !important;color:black!important;">
                                    <div class="demo-google-material-icon" > <i class="material-icons">query_builder</i><br/><span class="icon-name">AUTOMATION SETTINGS</span> </div>
                                </div>
							</div>
							
							
						
						
						</div>
								<div class="row clearfix">
								
							
							<div class="col-lg-6 text-center">
								<div class="body m-b-10" style="background-color:white !important;color:black!important;">
                                    <div class="demo-google-material-icon" > <i class="material-icons">notifications</i><br/><span class="icon-name">NOTIFICATIONS</span> </div>
                                </div>
							</div>
									<div class="col-lg-6 text-center">
								<div class="body m-b-10" style="background-color:white !important;color:black!important;">
                                    <div class="demo-google-material-icon" > <i class="material-icons">person</i><br/><span class="icon-name">ADMINISTRATOR USER</span> </div>
                                </div>
							</div>
							
							
						
						
						</div>	<div class="row clearfix">
								
							<div class="col-lg-6 text-center">
								<div class="body m-b-10" style="background-color:white !important;color:black!important;">
                                    <div class="demo-google-material-icon" > <i class="material-icons">phone_iphone</i><br/><span class="icon-name">TWO-FACTOR AUTHENTICATION</span> </div>
                                </div>
							</div>
									<div class="col-lg-6 text-center">
								<div class="body m-b-10" style="background-color:white !important;color:black!important;">
                                    <div class="demo-google-material-icon" > <i class="material-icons">lock</i><br/><span class="icon-name">ADMINISTRATOR RULES AND PERMISIONS</span> </div>
                                </div>
							</div>
							
							
						
						
						</div>
					
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
</body>
</html>