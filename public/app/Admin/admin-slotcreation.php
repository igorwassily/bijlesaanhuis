<?php

    require_once('includes/connection.php');
$thisPage="Admin SlotCreation";
session_start();
if(!isset($_SESSION['AdminID']))
{
    header('Location: index.php');
}
else
{
    $query  = "SELECT * FROM adminslots WHERE adminID=".$_SESSION['AdminID'];
    $result = mysqli_query($con, $query);

    $data   = mysqli_fetch_assoc($result);
	
	$mon_time = explode("-", $data['mon_time']);
	$tue_time = explode("-", $data['tue_time']);
	$wed_time = explode("-", $data['wed_time']);
	$thur_time = explode("-", $data['thur_time']);
	$fri_time = explode("-", $data['fri_time']);
	$sat_time = explode("-", $data['sat_time']);
	$sun_time = explode("-", $data['sun_time']);

?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Admin Slot Creation</title>
<?php
        require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">

<!-- New  Style -->

<!-- New  Style -->
<style type="text/css">
    .inline-elem-button{margin-top: 20px;} 
    .form-control, .btn-round.btn-simple{color: #ece6e6;}
    .input-group-addon{color:#ece6e6;}
    .text{color:black;}
    .margin-top{margin-top:10px;}
    .checkbox{float: left;margin-right: 20px;}
    .margintop{margin-top: 20px; color:#5cc75f;}
    /*Placeholder Color */
    .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover { color: #486066; }
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
    ?>
</head>
<body class="theme-green">
<!-- Page Loader -->
 <div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        /*require_once('includes/header.php');*/
        require_once('includes/sidebarAdminDashboard.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
       
            <div class="col-xl-12 col-lg-12 col-md-12">
                <form id="myform" name="myform">
                <div class="card demo-masked-input">  
                    <div class="header">
                        <h2><strong>Schedule</strong> Slot <small> Set Your Available Time </small> </h2>
                    </div>  
                    <div class="body">
                        <div class="row clearfix margintop">
                            <div class="col-lg-12 col-md-12"> 
                                <b>Monday : </b>
                                <hr/>
                            </div>
							<div class="col-lg-2 col-md-2"> 
                                <b>Start Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="mon_stime" id="mon_stime" placeholder="Ex: 11:59 pm"  value="<?php echo $mon_time[0]; ?>">
                                </div>
                            </div>    
                            <div class="col-lg-2 col-md-2"> <b>End Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="mon_etime" id="mon_etime" placeholder="Ex: 11:59 pm"  value="<?php echo  $mon_time[1]; ?>">
                                </div>
                            </div>   
                        </div>  
						  <div class="row clearfix margintop">
                            <div class="col-lg-12 col-md-12"> 
                                <b>Tuesday : </b>
                                <hr/>
                            </div>
							<div class="col-lg-2 col-md-2"> 
                                <b>Start Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="tue_stime" id="tue_stime" placeholder="Ex: 11:59 pm"  value="<?php echo  $tue_time[0]; ?>">
                                </div>
                            </div>    
                            <div class="col-lg-2 col-md-2"> <b>End Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="tue_etime" id="tue_etime" placeholder="Ex: 11:59 pm"  value="<?php echo $tue_time[1]; ?>">
                                </div>
                            </div>   
                        </div>  
						  <div class="row clearfix margintop">
                            <div class="col-lg-12 col-md-12"> 
                                <b>Wednessday : </b>
                                <hr/>
                            </div>
							<div class="col-lg-2 col-md-2"> 
                                <b>Start Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="wed_stime" id="wed_stime" placeholder="Ex: 11:59 pm"  value="<?php echo $wed_time[0]; ?>">
                                </div>
                            </div>    
                            <div class="col-lg-2 col-md-2"> <b>End Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="wed_etime" id="wed_etime" placeholder="Ex: 11:59 pm"  value="<?php echo $wed_time[1] ?>">
                                </div>
                            </div>   
                        </div>  
						<div class="row clearfix margintop">
                            <div class="col-lg-12 col-md-12"> 
                                <b>Thursday : </b>
                                <hr/>
                            </div>
							<div class="col-lg-2 col-md-2"> 
                                <b>Start Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="thur_stime" id="thur_stime" placeholder="Ex: 11:59 pm"  value="<?php echo $thur_time[0]; ?>">
                                </div>
                            </div>    
                            <div class="col-lg-2 col-md-2"> <b>End Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="thur_etime" id="thur_etime" placeholder="Ex: 11:59 pm"  value="<?php echo $thur_time[1] ?>">
                                </div>
                            </div>   
                        </div>
						<div class="row clearfix margintop">
                            <div class="col-lg-12 col-md-12"> 
                                <b>Firday : </b>
                                <hr/>
                            </div>
							<div class="col-lg-2 col-md-2"> 
                                <b>Start Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="fri_stime" id="fri_stime" placeholder="Ex: 11:59 pm"  value="<?php echo $fri_time[0]; ?>">
                                </div>
                            </div>    
                            <div class="col-lg-2 col-md-2"> <b>End Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="fri_etime" id="fri_etime" placeholder="Ex: 11:59 pm"  value="<?php echo $fri_time[1]; ?>">
                                </div>
                            </div>   
                        </div>
						<div class="row clearfix margintop">
                            <div class="col-lg-12 col-md-12"> 
                                <b>Saturday : </b>
                                <hr/>
                            </div>
							<div class="col-lg-2 col-md-2"> 
                                <b>Start Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="sat_stime" id="sat_stime" placeholder="Ex: 11:59 pm"  value="<?php echo $sat_time[0]; ?>">
                                </div>
                            </div>    
                            <div class="col-lg-2 col-md-2"> <b>End Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="sat_etime" id="sat_etime" placeholder="Ex: 11:59 pm"  value="<?php echo $sat_time[1]; ?>">
                                </div>
                            </div>   
                        </div>
						<div class="row clearfix margintop">
                            <div class="col-lg-12 col-md-12"> 
                                <b>Sunday : </b>
                                <hr/>
                            </div>
							<div class="col-lg-2 col-md-2"> 
                                <b>Start Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="sun_stime" id="sun_stime" placeholder="Ex: 11:59 pm"  value="<?php echo $sun_time[0]; ?>">
                                </div>
                            </div>    
                            <div class="col-lg-2 col-md-2"> <b>End Time : </b>
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="zmdi zmdi-time"></i></span>
                                    <input type="text" class="form-control time12" name="sun_etime" id="sun_etime" placeholder="Ex: 11:59 pm"  value="<?php echo $sun_time[1]; ?>">
                                </div>
                            </div>   
                        </div>
                         <div class="row clearfix margintop">
                            <div class="col-lg-7 col-md-7"> 
                                <b>Repeat above Schedule for number of months : </b>
                                <hr/>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-6">
                                     <b>Valid Recursively for  : </b> 
                                <select class="form-control show-tick" name="month" id="month" >
                                    <option value="7 Day" <?php echo ($data['noofmonths'] == "7 Day")? "selected" : ""; ?>>1 Week</option>
                                    <option value="14 Day" <?php echo ($data['noofmonths'] == "14 Day")? "selected" : ""; ?>>2 Weeks</option>
                                    <option value="30 Day" <?php echo ($data['noofmonths'] == "30 Day")? "selected" : ""; ?>>1 Month</option>
                                    <option value="60 Day" <?php echo ($data['noofmonths'] == "60 Day")? "selected" : ""; ?>>2 Months</option>
                                    <option value="365 Day" <?php echo ($data['noofmonths'] == "365 Day")? "selected" : ""; ?>>Infinite</option>
                                </select>
                            </div>
                        </div>
                        <div class="row clearfix">
                            <div class="col-lg-3 col-md-6"><button type="submit" class="btn btn-raised m-b-10 bg-green" value="confirm" id="submitBtn">Create Time Slots</button></div>
                            <div class="col-lg-3 col-md-6"></div>
                        </div>
                    </div>
                    </div>
                </form>
                </div>
            
        </div>    
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->



<?php
    require_once('includes/footerScriptsAddForms.php');
?>


<!-- New  JS -->

<script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js --> 
<script src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js --> 
<script src="assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js --> 
<script src="assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js --> 
<script src="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js --> 
<script src="assets/plugins/nouislider/nouislider.js"></script> <!-- noUISlider Plugin Js -->

<script src="assets/js/pages/forms/advanced-form-elements.js"></script> 
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
<!-- New  JS -->

<script>

    $(document).ready(function(){


    $(".page-loader-wrapper").css("display", "none");

        $(document).ajaxStart(function() {
            $(document).css({'cursor' : 'wait'});
        }).ajaxStop(function() {
            $(document.body).css({'cursor' : 'default'});
        });
        $("#myform").submit(function(e){
            e.preventDefault();

             $(document).css({'cursor' : 'wait'});
             $("#submitBtn").prop("disabled", true);

            var btn = $(this).val();
			var mon_etime = $("#mon_etime").val();
 			var tue_etime = $("#tue_etime").val();
 			var wed_etime = $("#wed_etime").val();
 			var thur_etime = $("#thur_etime").val();
 			var fri_etime = $("#fri_etime").val();
 			var sat_etime = $("#sat_etime").val();
 			var sun_etime = $("#sun_etime").val();
			
			var mon_stime = $("#mon_stime").val();
 			var tue_stime = $("#tue_stime").val();
 			var wed_stime = $("#wed_stime").val();
 			var thur_stime = $("#thur_stime").val();
 			var fri_stime = $("#fri_stime").val();
 			var sat_stime = $("#sat_stime").val();
 			var sun_stime = $("#sun_stime").val();
			
            var month= $("#month").val(); 
            var wkday = "";

            $(".wkday").each(function(index, obj){
                if($(obj).is(":checked"))
                    wkday += $(obj).val() + ",";
            });
            wkday = wkday.substr(0, wkday.length-1);
/*
            if(stime > etime){
                alert("Wrong Time !");
                $(document.body).css({'cursor' : 'default'});
                $("#submitBtn").prop("disabled", false);
                return;
            }
*/
            var d           = new Date();
            var currentDate = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();
            // console.log(parseTime(stime));
            $.ajax({
                  type: "POST", 
                  url: "admin-ajaxcalls.php",
                  data: { subpage: 'slotCreation', mon_stime:mon_stime, tue_stime:tue_stime, wed_stime:wed_stime, thur_stime:thur_stime, fri_stime:fri_stime, sat_stime:sat_stime, sun_stime:sun_stime, mon_etime:mon_etime, tue_etime:tue_etime, wed_etime:wed_etime, thur_etime:thur_etime, fri_etime:fri_etime, sat_etime:sat_etime, sun_etime:sun_etime,  date:currentDate, month:month},
                  dataType: "json",
                  success: function(response) { 
                                if(response == "success"){
                                    console.log(response); 
                                    showNotification("alert-success", "Slots Successfully created", "bottom", "center", "", "");
                                }
                                 else if(response == "update"){
                                    console.log(response); 
                                    showNotification("alert-success", "Slots Update created", "bottom", "center", "", "");
                                }
                                else {
                                    showNotification("alert-danger", "Slots Update created", "bottom", "center", "", "");
                                }
                                $(document.body).css({'cursor' : 'default'});
                                $("#submitBtn").prop("disabled", false);
                            },
                  error: function(xhr, ajaxOptions, thrownError) { 
                                if(xhr.responseText == "success"){
                                    console.log(xhr.responseText); 
                                    showNotification("alert-success", "Slots Successfully created", "bottom", "center", "", "");
                                }
                                 else if(xhr.responseText == "update"){
                                    console.log(xhr.responseText); 
                                    showNotification("alert-success", "Slots Update Successfully", "bottom", "center", "", "");
                                }
                                else {
                                    showNotification("alert-danger", "Something went wrong !", "bottom", "center", "", "");
                                }
                                $(document.body).css({'cursor' : 'default'});
                                $("#submitBtn").prop("disabled", false);
                           }
                });// ends ajax
        });// click
    });

function parseTime(s) {
    var part = s.match(/(\d+):(\d+)(?: )?(am|pm)?/i);
    var hh = parseInt(part[1], 10);
    var mm = parseInt(part[2], 10);
    var ap = part[3] ? part[3].toUpperCase() : null;
    if (ap === "AM") {
        if (hh == 12) {
            hh = 0;
        }
    }
    if (ap === "PM") {
        if (hh != 12) {
            hh += 12;
        }
    }
    return { hh: hh, mm: mm };
}

</script>
</body>
</html>
<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="margin-top: 0px !important;
padding-top: 0px !important;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php
}
?>