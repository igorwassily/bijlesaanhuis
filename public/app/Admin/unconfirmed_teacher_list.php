<?php
$thisPage="unconfirmed Teachers list";
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else {

	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Accepted Consultation</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');

?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
	<?php
	require_once('includes/footerScripts.php');
	?>

    <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
    <script src="assets/js/pages/ui/notifications.js"></script>

<style type="text/css">
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	div.card>div.header{color:white;}
	
		div.card>div.header{color:white;}
	select:focus{	
	border: 1px solid #bdbdbd !important;
	color: black !important;
}

.text{
	color: black !important;
}
button.btn.dropdown-toggle.btn-round.btn-simple {
    width: 100px;
}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);

	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
    
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
	require_once('includes/connection.php');

?>




<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="header">
                        <h3>Accepted Consultation Teachers List</h3>
                    </div> -->

	                <?php

                    $consulType = (isset($_POST['consulFilter']) ? $_POST['consul'] : "%");


	                ?>



                    <div class="body">
                        <form method="POST">
                           <!-- <select id="labelConsul" name="consul" class="form-control show-tick">
                                <option selected disabled value="">Selecteer</option>
                                <option value='0'>Label 1</option>
                                <option value='1'>Label 2</option>
                                <option value='2'>Label 3</option>
                            </select>
                            <input type="submit" name="consulFilter" value="Zoeken">-->

                        </form>
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px; color: #486066" id="teachers_approve" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th class="color">Voornaam</th>
                                        <th class="color">Achternaam</th>
                                        <th class="color">E-mailadres</th>
                                        <th class="color">Telefoonnummer</th>
                                        <th class="color">Docenttype</th>
                                        <th class="color">Adres</th>
                                        <th class="color">Kennismaking</th>
                                        <th class="color">Consultation Type</th>
                                        <th class="color">Label</th>
                                        <th class="color">Label Date</th>
                                        <th></th>
                                    </tr>
                                </thead>


                                <tbody>
									<?php
											//$stmt = $con->prepare("SELECT u.userID, c.firstname, c.lastname, u.email, c.telephone, c.place_name FROM `user` u , contact c, teacher t,  WHERE usergroupID=2 AND disabled=0 AND u.active=1 AND u.userID=c.userID AND t.userID=u.userID AND t.isConfirmed=0 AND t.teacherlevel_rate_ID IS NOT NULL");
											
											$enum_val = array();
											$enums_query = "SELECT * FROM `teacherlevel_rate` WHERE isdeleted=0";
											$e_result = mysqli_query($con,$enums_query);
											$rate_arr = ""; 
											while($row = mysqli_fetch_array($e_result)){
												$arr = array($row['ID'],$row['internal_level'], $row['internal_rate']);
												array_push($enum_val, $arr);
												$rate_arr .=  $row['internal_rate'].",";
											}
											//echo "<pre>". print_r($enum_val, true) . "</pre>";

											$rate_arr = rtrim($rate_arr,",");
	

									$stmt = $con->prepare('SELECT u.userID, c.firstname, c.lastname, u.email, c.telephone, c.place_name, t.teacherlevel_rate_ID, t.consulationType, t.reminder, CONCAT(acb.datee, ", ", acb.starttime,"-",acb.endtime)ctime, t.label_docenten_2, t.date_label_set_2  FROM `user` u LEFT JOIN contact c ON c.userID=u.userID LEFT JOIN teacher t ON t.userID=u.userID LEFT JOIN admincalendarbooking acb ON acb.teacherID=u.userID WHERE usergroupID=2 AND disabled=0 AND u.active=1 AND t.isConfirmed=1 AND t.teacherlevel_rate_ID IS NULL AND t.consulationType LIKE"'.$consulType.'"');
					$stmt->execute();
					$stmt->bind_result($id,$firstname,$lastname,$email,$phone,$address, $teacherLevel, $consulationtype, $reminder, $ctime, $status, $date2);
					$stmt->store_result();


					while($stmt->fetch())
					{
					?>
									<tr>
									
										<?php echo "<td><a href='../teacher-detailed-view.php?tid=$id'>$firstname </a></td>"; ?>
										<td class="color"><?php echo $lastname; ?></td>
										<td class="color"><?php echo $email; ?></td>		
										<td class="color"><?php echo $phone; ?></td>
										<td class="color">
										 <select class="form-control show-tick change-teacher-level" onchange="changeLevelTeacher(this);">
												<?php
													// echo "<option value=' '>Select Level</option>";
													$flag = false;
													$rate = " ";
													$flag2 = false;

												    if($teacherLevel == NULL && $flag2 == false){
													    echo "<option value='selecteer' selected>Selecteer</option>";
													    $flag2 = true;
													    $rate = 0;
												    }

													foreach($enum_val as $item) {
													  //Has nothing
                                                      if($item[0] == $teacherLevel && $teacherLevel != NULL)
													  {
													  	echo "<option value='".$item[0]."_$id' selected>".$item[1]."</option>";
														  $rate = $item[2];
														  $flag = true;
													  }else 
													  {
													  	echo "<option value='".$item[0]."_$id'>".$item[1]."</option>";
													  }
													}	//foreach ends												
												
												?>												
											</select>
											<div class="alert alert-info" id="level-update-message" style="display:none; font-size:10px; border-radius:10px"></div>
										</td>
										<td class="color"><?php echo $address; ?></td>
										<td class="color"><?php echo $ctime; ?></td>
										<td class="color">
                                            <!-- <select class="form-control show-tick change-consul-type" onchange="changeLabel($(this));"> -->
	                                            <?php
	                                                // 0 - not happen yet
	                                                // 1 - in the past and contract not signed
	                                                // 2 - in the past and contract signed
	                                            $date = substr($ctime, 0, strpos($ctime, ","));
	                                            $startTime = substr($ctime, strpos($ctime, ",")+2, 5);


	                                            $finalDateDB = $date . " " . $startTime . ":00";

	                                            try {
		                                            $currentTime = new DateTime('now', new DateTimeZone("Europe/Amsterdam"));
		                                            $currentDateTime = $currentTime->format('Y-m-d H:i:s');
	                                            } catch (Exception $e) {
		                                            $currentDateTime = date('Y-m-d H:i:s');
	                                            }



	                                            if($finalDateDB <= $currentDateTime){   // PAST

	                                                    $isChecked = "";
	                                                    $isChecked2 = "";
	                                                    $isChecked3 = "";

		                                            if ($consulationtype >= 4) {
			                                            $isChecked = "checked";
			                                            $consulationtype -= 4;
		                                            }

		                                            if ($consulationtype >= 2) {
			                                            $isChecked2 = "checked";
			                                            $consulationtype -= 2;
		                                            }

		                                            if ($consulationtype >= 1) {
			                                            $isChecked3 = "checked";
			                                            $consulationtype -= 1;
		                                            }

		                                            ?>

                                                        <label>
                                                            <input type="checkbox" class="messageCheckbox" data-id="<?php echo $id; ?>" <?php echo $isChecked2; ?> value="2" id="gType" onclick="changeLabel($(this));">G
                                                        </label>
                                                        <label>
                                                            <input type="checkbox" class="messageCheckbox" data-id="<?php echo $id; ?>" <?php echo $isChecked3; ?> value="1" id="pType" onclick="changeLabel($(this));">P
                                                        </label>

                                                <?php
                                                }
	                                            ?>

                                            <!--  <option <?php if($consulationtype == "0") echo "selected"; ?> name="<?php echo $id; ?>" disabled value='0'>Label 1</option>
                                                <option <?php if($consulationtype == "1") echo "selected"; ?> name="<?php echo $id; ?>" disabled value='1'>Label 2</option>
                                                <option <?php if($consulationtype == "2") echo "selected"; ?> name="<?php echo $id; ?>" value='2'>Label 3</option> -->



                                            <!-- </select> -->


                                        </td>
                                        <td class="color">
                                            <select class="selectpicker form-control show-tick change-teacher-level" data-id="<?php echo $id; ?>" onchange="updateLabel($(this));">
                                                <option value="0" <?php echo ($status == "0") ? "selected" : "" ?>>Selecteer</option>
                                                <option value="1" <?php echo ($status == "1") ? "selected" : "" ?>>1</option>
                                                <option value="2.1" <?php echo ($status == "2.1") ? "selected" : "" ?>>2.1</option>
                                                <option value="3.1" <?php echo ($status == "3.1") ? "selected" : "" ?>>3.1</option>
                                                <option value="6" <?php echo ($status == "6") ? "selected" : "" ?>>6</option>
                                                <option value="7" <?php echo ($status == "7") ? "selected" : "" ?>>7</option>
                                            </select>
                                        </td>
                                        <td class="color"><?php echo $date2; ?></td>
										<td class="color"><a  href="../admin_edit_tutor_profile?tid=<?php echo $id?>" >Profiel</a> &nbsp;&nbsp;&nbsp;&nbsp;
											<a  href="#" class="delete" onclick="deleteEntry($(this));" data-id="<?php echo $id?>">Verwijder</a></td>
									</tr>
				<?php }	?>
                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script type="text/javascript">
    $(document).ready(function() {
        $('#teachers_approve').DataTable({
            "order": [[ 6, "asc" ]],
            "language": {
                "aria": {
                    "sortAscending":  ": Oplopend sorteren",
                    "sortDescending": ": Aflopend sorteren"
                },
                "emptyTable":     "Geen gegevens beschikbaar in de tabel",
                "info": "Tonen van _START_ tot _END_ van _TOTAL_ inzendingen",
                "infoEmpty":      "Tonen van 0 tot 0 van 0 inzendingen",
                "infoFiltered":   "(Gefilterd van in totaal _MAX_ inzendingen)",
                "lengthMenu": "Toon _MENU_ inzendingen",
                "loadingRecords": "Laden...",
                "processing": "Verwerken...",
                "paginate": {
                    "first":"Eerste",
                    "last":"Laatste",
                    "previous": "Vorige",
                    "next": "Volgende"
                },
                "search":"Zoeken:",
                "zeroRecords": "Geen overeenkomende gegevens gevonden",
            }
        });
    });

    function updateLabel(e){
        var inp = e.find(':selected').val();
        var tID = e.attr("data-id");

        if(inp != " "){

            $.ajax({
                url: 'admin-ajaxcalls.php',
                data: {subpage:"change_label_docenten_2", inp:inp, tID: tID},
                type: "POST",
                //dataType: "json",
                success: function (result) {
                    showNotification("alert-success", "Student Status is Successfully updated..!", "bottom", "center", "", "");
                }
            });
        }
    }


            function deleteEntry(e){
                var id = e.attr("data-id");

                if(confirm("Gebruiker verwijderen?")) {
                    $.ajax({
                        type: "POST",
                        url: "admin-ajaxcalls.php",
                        data: {subpage: "delete_teacher", id: id},
                        success: function (response) {
                            if (response == "success") {

                                showNotification("alert-primary", "Successfully Deleted the tutor", "bottom", "center", "", "");
                                location.reload();
                            } else {
                                showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                            }
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            if (xhr.response == "success") {

                                showNotification("alert-primary", "Successfully Deleted the tutor", "bottom", "center", "", "");
                                location.reload();
                            } else {
                                showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                            }
                        }
                    });// ends ajax
                } else {
                    return 0;
                }
            }

            console.log("init");
            var __tid = " ";
            var __level = " ";
            // fetching the teacher rate from
            var teacher_levelRAte = Array(<?php echo $rate_arr; ?>);


            function changeLevelTeacher(e){
                var inp = e.value;
                console.log(inp);

                if(inp != " "){
                    var t_level_id = inp.split("_");
                    __tid = t_level_id[1];
                    __level = (t_level_id[0])-1;

                    $.ajax({
                        url: 'actions/admin-ajaxcalls.php',
                        data: {t_level:t_level_id[0], t_id:t_level_id[1]},
                        type: "POST",
                        //dataType: "json",
                        success: function (result) {
                            $(".rate_"+__tid).text(teacher_levelRAte[__level]);
                            showNotification("alert-success", "Teacher Level is Successfully updated..!", "bottom", "center", "", "");
                        }
                    });
                }
            }


            function changeH(e){
                let hType = 0;
                let t_level_id = e.attr('data-id');
                console.log(t_level_id);

                $('.herinnering:checked[data-id='+t_level_id+']').each(function (i, element) {
                    console.log("checked with id: " + t_level_id);

                    hType = 1;
                });

                $.ajax({
                    url: 'admin-ajaxcalls.php',
                    data: {hType:hType,teacherID: t_level_id},
                    type: "POST",
                    //dataType: "json",
                    success: function (result) {
                        showNotification("alert-success", "Herinnering Type is Successfully updated..!", "bottom", "center", "", "");
                    }
                });



                console.log(hType);
                return 0;
            }



            function changeLabel(e){
                let cType = 0;
                let t_level_id = e.attr('data-id');

             //   console.log("T-ID: " + t_level_id);

                // CHECK HOW MANY ARE UNCHECKED / HOW MANY CHECKED

                $('.messageCheckbox:checked[data-id='+t_level_id+']').each(function (i, element) {
                    console.log("checked with id: " + t_level_id);

                    cType = cType | element.value;
                });
              //  console.log("cType after checked each: " + cType);

                /*
                if (e.is(':not(checked)')) {
                    console.log("not checked");
                    console.log(e.attr('value'));
                    //cType -= e.value;
                }
                */


                /*
                $('.messageCheckbox:not(checked)[data-id='+t_level_id+']').each(function (i, element) {
                    console.log("unchecked with id " + t_level_id);

                });
                */


                $.ajax({
                    url: 'admin-ajaxcalls.php',
                    data: {consul:cType,teacherID: t_level_id},
                    type: "POST",
                    //dataType: "json",
                    success: function (result) {
                        showNotification("alert-success", "Teacher Consul Type is Successfully updated..!", "bottom", "center", "", "");
                    }
                });
            }


</script>

</body>
</html>
<?php
}
?>