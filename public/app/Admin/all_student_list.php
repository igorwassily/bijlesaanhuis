<?php
$thisPage="all Student list";
session_start();
if(!isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>All Students List</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<style type="text/css">
    .dropdown-menu a span {
        color: black;
    }
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;

color: #486066 !important;
	}
	

	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	input[type="text"] {
    height: 40px !important;
}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	

	div.card>div.header{color:white;}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarAdminDashboard.php');
	require_once('includes/connection.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
       <?php require_once('includes/adminTopBar.php'); ?>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card">
                    <!-- <div class="header">
                        <h3>All Students List</h3>
                    </div> -->


					
                    <div class="body">
                        <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable dt-responsive" style="font-size: 13px; color: #486066" id="teachers_approve" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th class="color">Vornaam</th>
                                        <th class="color">Achternaam</th>
                                        <th class="color">E-mailadres</th>
                                        <th class="color">Telefoonnummer</th>
                                        <th class="color">Adres</th>
                                        <th class="color">Geactiveerd?</th>
                                        <th class="color">Datum registratie</th>
                                        <th></th>
                                        <th class="color">Label</th>
                                        <th class="color">Date</th>

                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
											$stmt = $con->prepare("SELECT u.userID,
       c.firstname,
       c.lastname,
       u.email,
       c.telephone,
       c.place_name,
       u.active,
       u.creation_datetime,
       s.status,
       s.label_date_set
FROM user u,
     contact c,
     student s
WHERE usergroupID = 1
  AND disabled = 0
  AND u.userID = c.userID
  AND c.contacttypeID = 2
  AND s.contactID = c.contactID");
	
					$stmt->execute();
					$stmt->bind_result($id,$firstname,$lastname,$email,$phone,$address, $active, $creation_datetime, $status, $dateSet);
					$stmt->store_result();
					while($stmt->fetch())
					{
						$q2 = $con->prepare( 'INSERT INTO admin_msg_read(type,userID) VALUES("'.NOTIFICATION_ALL_STUDENTS.'", "'.$id.'")');
						$q2->execute();
					?>
									<tr>
									
										<?php echo "<td><a href='../student-profile?sid=$id'>$firstname </a></td>"; ?>
										<td class="color"><?php echo $lastname; ?></td>
										<td class="color"><?php echo $email; ?></td>
										<td class="color"><?php echo $phone; ?></td>
										<td class="color"><?php echo $address; ?></td>
										<td class="color">
											<?php echo ($active)? "Ja" : "Nee"; ?>
											<br>
											<a  href="#" class="toggle-active-item" data-id="<?php echo $id;?>" data-active="<?php echo $active;?>"><?php echo ($active)? "Deactiveren" : "Activeren"; ?></a>
										</td>
                                        <td class="color"><?php echo $creation_datetime; ?></td>
										<td>
											<a  href="/app/Admin/admin_edit_student_profile?sid=<?php echo $id?>">Profiel</a>
											<br>
											<a  href="#" class="delete" data-id="<?php echo $id?>">Verwijder</a>
										</td>
                                        <td>
                                            <select class="selectpicker form-control show-tick change-teacher-level" data-id="<?php echo $id; ?>" onchange="updateStatus($(this));">
                                                <option value="1" <?php echo ($status == "1") ? "selected" : "" ?>>1</option>
                                                <option value="1.1" <?php echo ($status == "1.1") ? "selected" : "" ?>>1.1</option>
                                                <option value="2" <?php echo ($status == "2") ? "selected" : "" ?>>2</option>
                                                <option value="3" <?php echo ($status == "3") ? "selected" : "" ?>>3</option>
                                                <option value="4" <?php echo ($status == "4") ? "selected" : "" ?>>4</option>
                                                <option value="5" <?php echo ($status == "5") ? "selected" : "" ?>>5</option>
                                                <option value="5.1" <?php echo ($status == "5.1") ? "selected" : "" ?>>5.1</option>
                                                <option value="5.2" <?php echo ($status == "5.2") ? "selected" : "" ?>>5.2</option>
                                                <option value="6" <?php echo ($status == "6") ? "selected" : "" ?>>6</option>
                                                <option value="7" <?php echo ($status == "7") ? "selected" : "" ?>>7</option>
                                            </select>
                                        </td>
                                        <td><?php echo $dateSet; ?></td>
									</tr>
				<?php }	?>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php
    require_once('includes/footerScripts.php');
?>
	
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="assets/js/pages/ui/notifications.js"></script>
<script type="text/javascript">
	$(document).ready(function() {



        $('#teachers_approve').DataTable({
            "language": {
                "aria": {
                    "sortAscending":  ": Oplopend sorteren",
                    "sortDescending": ": Aflopend sorteren"
                },
                "emptyTable":     "Geen gegevens beschikbaar in de tabel",
                "info": "Tonen van _START_ tot _END_ van _TOTAL_ inzendingen",
                "infoEmpty":      "Tonen van 0 tot 0 van 0 inzendingen",
                "infoFiltered":   "(Gefilterd van in totaal _MAX_ inzendingen)",
                "lengthMenu": "Toon _MENU_ inzendingen",
                "loadingRecords": "Laden...",
                "processing": "Verwerken...",
                "paginate": {
                    "first":"Eerste",
                    "last":"Laatste",
                    "previous": "Vorige",
                    "next": "Volgende"
                },
                "search":"Zoeken:",
                "zeroRecords": "Geen overeenkomende gegevens gevonden",
            }
        });

	});
		
	$(".delete").click(function(){
		var id = $(this).attr("data-id");

		if(confirm("Gebruiker verwijderen?")){
            $.ajax({
                type: "POST",
                url: "admin-ajaxcalls.php",
                data: {subpage:"delete_teacher",id:id}, //here delete_teacher == delete_students
                success: function(response) {
                    if(response == "success"){

                        showNotification("alert-primary", "Successfully Deleted the tutor", "bottom", "center", "", "");
                        location.reload();
                    }else{
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                },
                error: function(xhr, ajaxOptions, thrownError){
                    if(xhr.response == "success"){

                        showNotification("alert-primary", "Successfully Deleted the Student", "bottom", "center", "", "");
                        location.reload();
                    }else{
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                }
            });// ends ajax
        }
		else {
		    return 0;
        }


	});


	$(".toggle-active-item").click(function(){
		var id = $(this).attr("data-id");
		var active = $(this).attr("data-active");

		if(confirm("Gebruiker " + (active==1 ? 'de': "") + "activeren?")){
            $.ajax({
                type: "POST",
                url: "admin-ajaxcalls.php",
                data: {subpage:"activate_user",id:id,newActiveState:(active==1 ? 0:1)},
                success: function(response) {
                    if(response == "success"){

                        showNotification("alert-primary", "Successfully " + (active==1 ? 'de': "") + "activated the user", "bottom", "center", "", "");
                        location.reload();
                    }else{
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                },
                error: function(xhr, ajaxOptions, thrownError){
                    if(xhr.response == "success"){

                        showNotification("alert-primary", "Successfully " + (active==1 ? 'de': "") + "activated the user", "bottom", "center", "", "");
                        location.reload();
                    }else{
                        showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                    }
                }
            });// ends ajax
        }
		else {
		    return 0;
        }


	});


    function updateStatus(e){
        var inp = e.find(':selected').val();
        var userID = e.attr("data-id");

        if(inp != " "){

            $.ajax({
                url: 'admin-ajaxcalls.php',
                data: {subpage:"change_student_status", inp:inp, userID: userID},
                type: "POST",
                //dataType: "json",
                success: function (result) {
                    showNotification("alert-success", "Student Status is Successfully updated..!", "bottom", "center", "", "");
                }
            });
        }
    }

	
</script>

</body>
</html>
<?php
}
?>