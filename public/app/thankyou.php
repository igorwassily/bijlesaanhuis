<?php 
require_once("includes/connection.php");


?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Goede bijlesdocenten: altijd welkom! | Verdien meer dan bij onze grootste concurrenten | Bepaal zelf aan wie, wanneer en waar je bijles geeft.">
    <title>Registratie | Bijles Aan Huis</title>
    <?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
	?>
    <!-- Custom Css -->
    
    <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
	<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
	<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
	<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
	<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />	
	<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	
    <link rel="stylesheet" href="calendar/css/calendar.css">	
    
		<!-- New Calendar Style -->
    <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
    
    <style>
        *{color: #000;}

        .msg-error {
          color: #c65848;
        }
        .g-recaptcha.error {
          border: solid 2px #c64848;
          padding: .2em;
          width: 19em;
        }       
        /* {
            color: #486066!important;
            font-family: Poppins light !important;
            background-color: #f1f1f1 !important;
        }*/
        div.container{width: 100%;}
        .zmdi{font: normal normal normal 14px/1 'Material-Design-Iconic-Font'!important;}
        input::placeholder {color: #486066!important;
        }
        .border{
                border: 2px solid rgb(72, 96, 102) !important;
                border-radius: 50px;
        }
        .page-header{
            background-color : #FAFBFC !important;
        }
        
        /* login */
        .ma-heading{
            color: #5CC75F;
            border-bottom: 1px solid #bbb;
           
    		padding: 10px 4px;
        }
        .authentication .card-plain.card-plain .form-control:focus{
            color: #486066 !important;
        }
        .authentication .card-plain.card-plain .form-control, .authentication .card-plain.card-plain  .input-group-focus .input-group-addon{
            color: #486066 !important;
        }
        
        .authentication .card-plain .form{
            background: #f1f1f1 !important;
            padding: 30px;
            border-radius: 6px;
        }
        .authentication .card-plain{
            max-width: 50%;
        }
        .authentication .link, h6{
            color: #000;
        }
        #recaptcha{
            margin: 0 auto;
        }
        .authentication .card-plain.card-plain .input-group-addon{
            color: rgb(0, 0, 0);
        }
        
        @media only screen and (max-width: 768px){
                .authentication .card-plain{
                    max-width: 100%;
            }
        }
*{
        color:#486066!important;
        font-size: 16px;
        font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important; 
    }
span.input-group-addon {
    border: 2px solid #486066 !important;
    border-left: 0px !important;
}
        
        .centeer{width:80%; margin:auto;}
        .input-group, .input-group input[type=email]{border-radius: 6px !important;}
        .input-group input[type=email]{border: 0px !important;}
        .input-group-addon{border:0px !important;}
        .h5_text{padding:0 !important;}

    </style>
</head>

<body class="theme-green authentication login-body">
<!-- Navbar -->
    <?php	
        require_once('includes/header.php');
        
    ?>
<!-- End Navbar -->
<div class="page-header">
    <div class="container content-container">
        <div class="col-md-12 login-container login-container--md-padding">
           <div class="card-plain">
                <form id="login_form" class="form login-form" method="post" action="actions/forgotpassword">
                    <div class="header">
                        <h5 class="ma-heading login-form-header">Goed werk, de eerste stap is gezet!</h5>
                    </div>
                    <div class="login-form-content">
                       <p class="login-form__text">We hebben je een mail gestuurd met extra informatie om je profiel compleet te maken. Zou je tevens info@bijlesaanhuis.nl in je contactenlijst te zetten, zodat onze mails nooit in spam belanden? Heb je geen mail ontvangen? Stuur ons dan even een appje 😄</p>
                    </div>
                    <div class="login-form__button-container">
                        <a href="/" class="btn login-form-button">Naar de hoofdpagina</a>
                    </div>
                    <span class="msg-error"></span>
                </form>
            </div>
        </div>
    </div>
    <?php
   		require_once('includes/footerScriptsAddForms.php');
	?>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

</body>
</html>