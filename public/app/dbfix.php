<?php

use API\Endpoint\Geo;

require_once 'app/includes/connection.php';

$notFound = [];
$invalid = [];
$failed = [];
$updated = [];

$types = [];
$query = $con->query("SELECT ID, internal_level FROM teacherlevel_rate");
while ($row = $query->fetch_array()) {
	$types[strtolower($row[1])] = $row[0];
}

if (($handle = fopen("user.csv", "r")) === FALSE) {
	die("failed to open file user.csv");
}
while (($data = fgetcsv($handle, 0, ",")) !== FALSE) {
	$email = trim($data[2]);
	$type = strtolower(str_replace(' ', '', trim($data[5])));

	if (!isset($types[$type])) {
		$invalid[] = $email;
		continue;
	}

	$query = $con->query("SELECT userID FROM user WHERE email='$email'");
	if (!$query || $query->num_rows < 1) {
		$notFound[] = $email;
		continue;
	}
	$userID = $query->fetch_array()[0];

	if (!$con->query("UPDATE teacher SET teacherlevel_rate_ID={$types[$type]} WHERE userID=$userID")) {
		$failed[] = $email;
	} else {
		$updated[] = $email;
	}
}
fclose($handle);

echo json_encode($updated);
echo '<hr>';
echo json_encode($failed);
echo '<hr>';
echo json_encode($notFound);
echo '<hr>';
echo json_encode($invalid);

/*$query = $con->query("SELECT c.* FROM contact c WHERE (c.address IS NULL OR c.address = '') AND c.place_name IS NOT NULL AND c.place_name <> ''");
while ($row = $query->fetch_assoc()) {
	$placeName = $row['place_name'];
	$placeName = str_replace('Netherlands', '', $placeName);
	$placeName = trim($placeName, ', ');
	$parts = explode(',', $placeName);
	if (sizeof($parts) < 1) continue;
	$address = $con->real_escape_string(trim($parts[0]));
	$parts[1] = trim($parts[1]);
	$pos = strpos($parts[1], ' ');
	if ($pos === false) {
		continue;
	}
	$postCode = $con->real_escape_string(trim(substr($parts[1], 0, $pos)));
	$city = $con->real_escape_string(trim(substr($parts[1], $pos)));
	$country = 'Netherlands';
	$con->query("UPDATE contact SET address='$address', postalcode='$postCode', city='$city', country='$country' WHERE contactID={$row['contactID']}");
}*/
$geo = new Geo($con, $db);
$query = $con->query("SELECT c.* FROM contact c WHERE LENGTH(postalcode) < 5 AND latitude <> '' AND latitude IS NOT NULL AND longitude <> '' AND longitude IS NOT NULL");
while ($row = $query->fetch_assoc()) {
	$data = $geo->Coordinate($row);
	if ($data->getStatus() !== 200) {
		continue;
	}
//	$placeName = $row['place_name'];
//	$placeName = str_replace('Netherlands', '', $placeName);
//	$placeName = trim($placeName, ', ');
//	$parts = explode(',', $placeName);
//	if (sizeof($parts) < 1) continue;
//	$address = $con->real_escape_string(trim($parts[0]));
//	$parts[1] = trim($parts[1]);
//	$pos = strpos($parts[1], ' ');
//	if ($pos === false) {
//		continue;
//	}
//	$postCode = $con->real_escape_string(trim(substr($parts[1], 0, $pos)));
//	$city = $con->real_escape_string(trim(substr($parts[1], $pos)));
	$address = $data['address']['street'] . ' ' . $data['address']['houseNumber'];
	$postCode = $data['address']['postcode'];
	$city = $data['city'];
	$country = 'Netherlands';
	$con->query("UPDATE contact SET address='$address', postalcode='$postCode', city='$city', country='$country' WHERE contactID={$row['contactID']}");
}

/*$query = $con->query("SELECT u.userID, u.email FROM teacher t LEFT JOIN user u ON t.userID = u.userID LEFT JOIN contact c on t.userID = c.userID WHERE c.userID IS NULL");
while ($row = $query->fetch_assoc()) {
	$result = $con->query("INSERT INTO contact (contacttypeID, userID, prmary, email) VALUES (1, {$row['userID']}, 1, '{$row['email']}')");
	if ($result === false) {
		die("Update failed: " . $con->error);
	}
	$result = $con->query("SELECT contactID FROM contact WHERE userID={$row['userID']}")->fetch_assoc();
	$result = $con->query("UPDATE teacher SET contactID={$result['contactID']} WHERE userID={$row['userID']}");
	if ($result === false) {
		die("Failed to set contact ID (userID: {$row['userID']})");
	}
}*/
