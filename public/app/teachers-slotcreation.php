<?php
    require_once('includes/connection.php');
    $thisPage="Teachers SlotCreation";
    session_start();

    if(!isset($_SESSION['Teacher']))
    {
        header('Location: index.php');
    }
    else
    {
        $query  = "SELECT * FROM teacherslots WHERE teacherID=".$_SESSION['TeacherID'];
        $result = mysqli_query($con, $query);
        $data   = mysqli_fetch_assoc($result);
        $mon_time = empty($data) ? [] : explode("-", $data['mon_time']);
        $tue_time = empty($data) ? [] : explode("-", $data['tue_time']);
        $wed_time = empty($data) ? [] : explode("-", $data['wed_time']);
        $thur_time = empty($data) ? [] : explode("-", $data['thur_time']);
        $fri_time = empty($data) ? [] : explode("-", $data['fri_time']);
        $sat_time = empty($data) ? [] : explode("-", $data['sat_time']);
        $sun_time = empty($data) ? [] : explode("-", $data['sun_time']);
        // PARSE JSON FOR ADDITIONAL TIME SLOTS
        $mon_add = empty($data) ? [] : json_decode($data['mon_additional'], true);
        $tue_add = empty($data) ? [] : json_decode($data['tue_additional'], true);
        $wed_add = empty($data) ? [] : json_decode($data['wed_additional'], true);
        $thu_add = empty($data) ? [] : json_decode($data['thur_additional'], true);
        $fri_add = empty($data) ? [] : json_decode($data['fri_additional'], true);
        $sat_add = empty($data) ? [] : json_decode($data['sat_additional'], true);
        $sun_add = empty($data) ? [] : json_decode($data['sun_additional'], true);

        function getmins($time) {
            if(!$time)
                return 0;
                $time = explode(":", $time);
                $min = explode(" ", $time[1]);
            if(isset($min[1]) && $min[1] == "PM")
                $time[0] = intval($time[0]) + 12;
                return ((intval($time[0])* 60 )+ intval($min[0]));
        }
?>

<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Je algemene beschikbaarheid aangeven: dat doe je hier | Verdien meer dan bij onze grootste concurrenten | Bepaal zelf aan wie, wanneer en waar je bijles geeft.">
    <title>Beschikbaarheid aangeven | Bijles Aan Huis</title>
    <?php
            require_once('includes/connection.php');
            require_once('includes/mainCSSFiles.php');
    ?>
    <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
    <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
    <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
    <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet"/>

    <style type="text/css">
        .inline-elem-button{margin-top: 20px;}
        .form-control, .btn-round.btn-simple{color: #ece6e6;}
        .input-group-addon{color:#ece6e6;}
        .text{color:black;}
        .margin-top{margin-top:10px;}
        .checkbox{float: left;margin-right: 20px;}
        .margintop{margin-top: 20px; color:#50d38a;}
        .time-range p {
            font-family:"Arial", sans-serif;
            font-size:14px;
            color:#333;
        }
        .ui-slider-horizontal {
            height: 20px;
            background: #D7D7D7;
            border: 0;
            box-shadow: none;
            clear: both;
            margin: 8px 0;
            /* border-radius: 6px; */
        }
        .ui-slider {
            position: relative;
            text-align: left;
            border: 1px solid #D7D7D7 !important;
        }
        .ui-slider-horizontal .ui-slider-range {
            top: -1px;
            height: 100%;
        }
        #minus {
        content: "\e082";
        color: #ffbd6b !important;
        border-radius: 50%;
        float:right;
        font-size: 23px;
        /* position: absolute; */
        }

        #plus {
        content: "\e081";
        color: rgb(92, 199, 95) !important;
        border-radius: 50%;
        float:right;
        font-size: 23px;
        /* position: absolute;
        right: -30px;
        top:50%;
        transform: translate(0,-50%); */
        }
        .plus-center {
        float: none !important;
        }

        .sliders_step{
        position: relative;
        }

        .ui-slider .ui-slider-range {
        position: absolute;
        z-index: 1;
        height: 20px;
        font-size: .7em;
        display: block;
        /* border: 1px solid #5BA8E1; */
        box-shadow: 0 1px 0 #A7EF9F inset;
        /* -moz-border-radius: 6px;
        -webkit-border-radius: 6px;
        -khtml-border-radius: 6px; */
        /* border-radius: 6px; */
        background: #A7EF9F;
        /* background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
        background-size: 100%;
        background-image: -webkit-gradient(linear, 50% 0, 50% 100%, color-stop(0%, #A0D4F5), color-stop(100%, #81B8F3));
        background-image: -webkit-linear-gradient(top, #A0D4F5, #81B8F3);
        background-image: -moz-linear-gradient(top, #A0D4F5, #81B8F3);
        background-image: -o-linear-gradient(top, #A0D4F5, #81B8F3);
        background-image: linear-gradient(top, #A0D4F5, #81B8F3); */
        }
        .ui-slider .ui-slider-handle {
        border-radius: 50%;
        background: #F9FBFA;
        background-image: url('data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgi…pZHRoPSIxMDAlIiBoZWlnaHQ9IjEwMCUiIGZpbGw9InVybCgjZ3JhZCkiIC8+PC9zdmc+IA==');
        background-size: 100%;
        background-image: -webkit-gradient(linear, 50% 0, 50% 100%, color-stop(0%, #C7CED6), color-stop(100%, #F9FBFA));
        background-image: -webkit-linear-gradient(top, #C7CED6, #F9FBFA);
        background-image: -moz-linear-gradient(top, #C7CED6, #F9FBFA);
        background-image: -o-linear-gradient(top, #C7CED6, #F9FBFA);
        background-image: linear-gradient(top, #C7CED6, #F9FBFA);
        width: 22px;
        height: 22px;
        -webkit-box-shadow: 0 2px 3px -1px rgba(0, 0, 0, 0.6), 0 -1px 0 1px rgba(0, 0, 0, 0.15) inset, 0 1px 0 1px rgba(255, 255, 255, 0.9) inset;
        -moz-box-shadow: 0 2px 3px -1px rgba(0, 0, 0, 0.6), 0 -1px 0 1px rgba(0, 0, 0, 0.15) inset, 0 1px 0 1px rgba(255, 255, 255, 0.9) inset;
        box-shadow: 0 2px 3px -1px rgba(0, 0, 0, 0.6), 0 -1px 0 1px rgba(0, 0, 0, 0.15) inset, 0 1px 0 1px rgba(255, 255, 255, 0.9) inset;
        -webkit-transition: box-shadow .3s;
        -moz-transition: box-shadow .3s;
        -o-transition: box-shadow .3s;
        transition: box-shadow .3s;
        }
        .ui-slider .ui-slider-handle {
        position: absolute;
        z-index: 2;
        width: 7px;
        height: 30px;
        top: 50%;
        transform: translate(50%,-50%);
        cursor: default;
        border: none;
        cursor: pointer;
        background: lightgreen;
        border-radius: 0;
        box-shadow: none;
        background-color:#5CC75F;
        }
        .ui-slider .ui-slider-handle:after {
        content:none;
        position: absolute;
        width: 8px;
        height: 8px;
        border-radius: 50%;
        top: 50%;
        margin-top: -4px;
        left: 50%;
        margin-left: -4px;
        background: #30A2D2;
        -webkit-box-shadow: 0 1px 1px 1px rgba(22, 73, 163, 0.7) inset, 0 1px 0 0 #FFF;
        -moz-box-shadow: 0 1px 1px 1px rgba(22, 73, 163, 0.7) inset, 0 1px 0 0 white;
        box-shadow: 0 1px 1px 1px rgba(22, 73, 163, 0.7) inset, 0 1px 0 0 #FFF;
        }
        .ui-slider-horizontal .ui-slider-handle {
        /* top: -.5em; */
        margin-left: -6px;
        }
        .ui-slider a:focus {
        outline:none;
        }
        span.caret {
        display: none;
        }
        .errorlabel{
        color:red !important;
        font-weight: 700;
        }
        .card .body {
        background-color: #FAFBFC !important;
        border-radius:0;
        }
        .demo-masked-input {
        border: 2px solid #f1f1f1 !important;
        border-radius:6px !important;
        }
        .card .header h2 small {
        margin-top:15px;
        }
        .theme-green .card .header h2:before {
        background: transparent !important;
        }
        .header .ma-main-title {
        font-weight: 600 !important;
        font-size: 21px !important;
        }
        .btn.btn--save {
        background: #5CC75F !important;
        border: solid 2px #5CC75F !important;
        color: #fafbfc !important;
        transition-delay: 0s;
        transition-duration: 0.2s;
        transition-property: all;
        font-weight: 600 !important;
        font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important;
        text-transform: uppercase !important;
        border-radius: 6px !important;
        padding: 15px 20px;
        width: 200px !important;
        float: left;
        }
        .slideTable--text-center {
        text-align: center;
        }
    </style>
    <?php
        $activePage = basename($_SERVER['PHP_SELF']);
    ?>
</head>

<body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <?php
        require_once('includes/header.php');
    ?>

    <!-- Main Content -->
    <section class="content page-calendar">
        <div class="container-fluid ma-sidebar-container">
            <div class="row clearfix">
                <div class="col-lg-12">
                    <form id="myform" name="myform">
                        <div class="card demo-masked-input">
                            <div class="header">
                                <h2 class="ma-main-title">Beschikbaarheid aangeven<small>Deze tijden vul je vaak voor een lange periode in. Het geeft de leerling een globaal idee van je beschikbaarheid, dus je zit hier nooit aan vast. Afwijkingen pas je eenvoudig aan in je kalender.</small></h2>
                            </div>
                            <div class="body">
                                <div class="row clearfix">
                                    <div class="col-lg-3 col-md-12">
                                        <div class="row clearfix margintop">
                                            <div class="col-lg-12 col-md-12">
                                                <b>Maandag :</b>
                                                <hr/>
                                            </div>
                                            <div class="col-md-12 slideTable-mon<?php echo (empty($mon_time[0])? ' slideTable--text-center':'');?>">
                                                <input type="hidden" name="mon_stime" id="mon_stime" value="<?php echo  isset($mon_time[0]) ? $mon_time[0] : ''; ?>">
                                                <input type="hidden" name="mon_etime" id="mon_etime" value="<?php echo  isset($mon_time[1]) ? $mon_time[1] : ''; ?>">
                                                <?php if(empty($mon_time[0])): ?>
                                                    <span class="glyphicon glyphicon glyphicon-plus-sign acplusmon plus-center" id="plus" name="mon"></span>
                                                <?php elseif(!empty($mon_time[0])): ?>
                                                    <div class="time-range">
                                                        <span class="glyphicon glyphicon glyphicon-plus-sign acplusmon" id="plus" name="mon"></span>
                                                        <span class="glyphicon glyphicon glyphicon-minus-sign acminusmon" '="" id="minus" day="mon"></span>
                                                        <p>Beschikbaarheid : <br/>
                                                        <span class="mon-slider-time"><?php echo  $mon_time[0]; ?></span> - <span class="mon-slider-time2"><?php echo  $mon_time[1]; ?></span>
                                                        </p>
                                                        <div class="sliders_step mon_sliders_step">
                                                        <!-- <span class="glyphicon glyphicon glyphicon-plus-sign acplusmon" id="plus" name="mon"></span> -->
                                                        <div class="slider-range" day="mon"></div>
                                                        </div>
                                                        <p id="mon-error" class="errorlabel saveError"></p>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-12">
                                                <p id="mon-area-error" class="errorlabel saveError"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <div class="row clearfix margintop">
                                            <div class="col-lg-12 col-md-12">
                                                <b>Dinsdag :</b>
                                                <hr/>
                                            </div>
                                            <div class="col-md-12 slideTable-din<?php echo (empty($tue_time[0])? ' slideTable--text-center':'');?>">
                                                <input type="hidden" name="tue_stime" id="tue_stime" value="<?php echo  isset($tue_time[0]) ? $tue_time[0] : ''; ?>">
                                                <input type="hidden" name="tue_etime" id="tue_etime" value="<?php echo  isset($tue_time[1]) ? $tue_time[1] : ''; ?>">
                                                <?php if(empty($tue_time[0])): ?>
                                                    <span class="glyphicon glyphicon glyphicon-plus-sign acplusdin plus-center" id="plus" name="din"></span>
                                                <?php elseif(!empty($tue_time[0])): ?>
                                                    <div class="time-range-din[]">
                                                        <span class="glyphicon glyphicon glyphicon-plus-sign acplusdin" id="plus" name="din"></span>
                                                        <span class="glyphicon glyphicon glyphicon-minus-sign acminusdin" '="" id="minus" day="din"></span>
                                                        <p>Beschikbaarheid : <br/>
                                                            <span class="tue-slider-time"><?php echo  $tue_time[0]; ?></span> - <span class="tue-slider-time2"><?php echo  $tue_time[1]; ?></span>
                                                        </p>
                                                        <div class="sliders_step tue_sliders_step">
                                                            <div class="slider-range" day="tue"></div>
                                                        </div>
                                                        <p id="tue-error" class="errorlabel saveError"></p>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-12">
                                                <p id="din-area-error" class="errorlabel saveError"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <div class="row clearfix margintop">
                                            <div class="col-lg-12 col-md-12">
                                                <b>Woensdag :</b>
                                                <hr/>
                                            </div>
                                            <div class="col-md-12 slideTable-woe<?php echo (empty($wed_time[0])? ' slideTable--text-center':'');?>">
                                    <input type="hidden" name="wed_stime" id="wed_stime" value="<?php echo  isset($wed_time[0]) ? $wed_time[0] : ''; ?>">
                                    <input type="hidden" name="wed_etime" id="wed_etime" value="<?php echo  isset($wed_time[1]) ? $mon_time[1] : ''; ?>">


                                    <?php if(empty($wed_time[0])): ?>

                                    <span class="glyphicon glyphicon glyphicon-plus-sign acpluswoe plus-center" id="plus" name="woe"></span>

                                    <?php elseif(!empty($wed_time[0])): ?>


                                    <div class="time-range">
                                    <span class="glyphicon glyphicon glyphicon-plus-sign acpluswoe" id="plus" name="woe"></span>
                                    <span class="glyphicon glyphicon glyphicon-minus-sign acminuswoe" '="" id="minus" day="woe"></span>
                                    <p>Beschikbaarheid : <br/>
                                    <span class="wed-slider-time"><?php echo  $wed_time[0]; ?></span> - <span class="wed-slider-time2"><?php echo  $wed_time[1]; ?></span>
                                    </p>
                                    <div class="sliders_step">
                                    <div class="slider-range" day="wed"></div>
                                    </div>
                                    <p id="wed-error" class="errorlabel saveError"></p>
                                    </div>
                                    <?php endif; ?>
                                    </div>
                                            <div class="col-12">
                                                <p id="woe-area-error" class="errorlabel saveError"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <div class="row clearfix margintop">
                                            <div class="col-lg-12 col-md-12">
                                                <b>Donderdag :</b>
                                                <hr/>
                                            </div>
                                            <div class="col-md-12 slideTable-don<?php echo (empty($thur_time[0])? ' slideTable--text-center':'');?>">
                                    <input type="hidden" name="thur_stime" id="thur_stime" value="<?php echo  isset($thur_time[0]) ? $thur_time[0] : ''; ?>">
                                    <input type="hidden" name="thur_etime" id="thur_etime" value="<?php echo  isset($thur_time[1]) ? $thur_time[1] : ''; ?>">

                                    <?php if(empty($thur_time[0])): ?>

                                    <span class="glyphicon glyphicon glyphicon-plus-sign acplusdon plus-center" id="plus" name="don"></span>

                                    <?php elseif(!empty($thur_time[0])): ?>

                                    <div class="time-range">
                                    <span class="glyphicon glyphicon glyphicon-plus-sign acplusdon" id="plus" name="don"></span>
                                    <span class="glyphicon glyphicon glyphicon-minus-sign acminusdon" '="" id="minus" day="don"></span>
                                    <p>Beschikbaarheid : <br/>
                                    <span class="thur-slider-time"><?php echo  $thur_time[0]; ?></span> - <span class="thur-slider-time2"><?php echo  $thur_time[1]; ?></span>
                                    </p>
                                    <div class="sliders_step">
                                    <div class="slider-range" day="thur"></div>
                                    </div>
                                    <p id="thur-error" class="errorlabel saveError"></p>
                                    </div>
                                    <?php endif; ?>
                                    </div>
                                            <div class="col-12">
                                                <p id="don-area-error" class="errorlabel saveError"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row clearfix ">
                                    <div class="col-lg-3 col-md-12">
                                        <div class="row clearfix margintop">
                                            <div class="col-lg-12 col-md-12">
                                                <b>Vrijdag :</b>
                                                <hr/>
                                            </div>
                                            <div class="col-md-12 slideTable-vri<?php echo (empty($fri_time[0])? ' slideTable--text-center':'');?>">
                                        <input type="hidden" name="fri_stime" id="fri_stime" value="<?php echo  isset($fri_time[0]) ? $fri_time[0] : ''; ?>">
                                        <input type="hidden" name="fri_etime" id="fri_etime" value="<?php echo  isset($fri_time[1]) ? $fri_time[1] : ''; ?>">

                                        <?php if(empty($fri_time[0])): ?>

                                        <span class="glyphicon glyphicon glyphicon-plus-sign acplusvri plus-center" id="plus" name="vri"></span>

                                        <?php elseif(!empty($fri_time[0])): ?>


                                        <div class="time-range">
                                        <span class="glyphicon glyphicon glyphicon-plus-sign acplusvri" id="plus" name="vri"></span>
                                        <span class="glyphicon glyphicon glyphicon-minus-sign acminusvri" '="" id="minus" day="vri"></span>
                                        <p>Beschikbaarheid : <br/>
                                        <span class="fri-slider-time"><?php echo  $fri_time[0]; ?></span> - <span class="fri-slider-time2"><?php echo  $fri_time[1]; ?></span>
                                        </p>
                                        <div class="sliders_step1">
                                        <div class="slider-range" day="fri"></div>
                                        </div>

                                        <p id="fri-error" class="errorlabel saveError"></p>
                                        </div>
                                        <?php endif; ?>
                                        </div>
                                            <div class="col-12">
                                                <p id="vri-area-error" class="errorlabel saveError"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <div class="row clearfix margintop">
                                            <div class="col-lg-12 col-md-12">
                                                <b>Zaterdag :</b>
                                                <hr/>
                                            </div>
                                            <div class="col-md-12 slideTable-zat<?php echo (empty($sat_time[0])? ' slideTable--text-center':'');?>">
                                            <input type="hidden" name="sat_stime" id="sat_stime" value="<?php echo  isset($sat_time[0]) ? $sat_time[0] : ''?>">
                                            <input type="hidden" name="sat_etime" id="sat_etime" value="<?php echo  isset($sat_time[1]) ? $sat_time[1] : ''; ?>">

                                            <?php if(empty($sat_time[0])): ?>

                                            <span class="glyphicon glyphicon glyphicon-plus-sign acpluszat plus-center" id="plus" name="zat"></span>

                                            <?php elseif(!empty($sat_time[0])): ?>


                                            <div class="time-range">
                                            <span class="glyphicon glyphicon glyphicon-plus-sign acpluszat" id="plus" name="zat"></span>
                                            <span class="glyphicon glyphicon glyphicon-minus-sign acminuszat" '="" id="minus" day="zat"></span>
                                            <p>Beschikbaarheid : <br/>
                                            <span class="sat-slider-time"><?php echo  $sat_time[0]; ?></span> - <span class="sat-slider-time2"><?php echo  $sat_time[1]; ?></span>
                                            </p>
                                            <div class="sliders_step1">
                                            <div class="slider-range" day="sat"></div>
                                            </div>
                                            <p id="sat-error" class="errorlabel saveError"></p>
                                            </div>
                                            <?php endif; ?>
                                            </div>
                                            <div class="col-12">
                                                <p id="zat-area-error" class="errorlabel saveError"></p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-3 col-md-12">
                                        <div class="row clearfix margintop">
                                            <div class="col-lg-12 col-md-12">
                                                <b>Zondag :</b>
                                                <hr/>
                                            </div>
                                            <div class="col-md-12 slideTable-zon<?php echo (empty($sun_time[0])? ' slideTable--text-center':'');?>">
                                                <input type="hidden" name="sun_stime" id="sun_stime" value="<?php echo  isset($sun_time[0]) ? $sun_time[0] : ''; ?>">
                                                <input type="hidden" name="sun_etime" id="sun_etime" value="<?php echo  isset($sun_time[1]) ? $sun_time[1] : ''; ?>">
                                                <?php if(empty($sun_time[0])): ?>
                                                    <span class="glyphicon glyphicon glyphicon-plus-sign acpluszon plus-center" id="plus" name="zon"></span>
                                                <?php elseif(!empty($sun_time[0])): ?>
                                                    <div class="time-range">
                                                        <span class="glyphicon glyphicon glyphicon-plus-sign acpluszon" id="plus" name="zon"></span>
                                                        <span class="glyphicon glyphicon glyphicon-minus-sign acminuszon" '="" id="minus" day="zon"></span>
                                                        <p>Beschikbaarheid : <br/>
                                                            <span class="sun-slider-time"><?php echo  $sun_time[0]; ?></span> - <span class="sun-slider-time2"><?php echo  $sun_time[1]; ?></span>
                                                        </p>
                                                        <div class="sliders_step1">
                                                            <div class="slider-range" day="sun"></div>
                                                        </div>
                                                        <p id="sun-error" class="errorlabel saveError"></p>
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                            <div class="col-12">
                                                <p id="zon-area-error" class="errorlabel saveError"></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input name="month" id="month" type="hidden" value="365 Day">
                                <div class="row clearfix" style="padding-top:20px;margin-top:10px;border-top: 2px solid #F1F1F1 !important;">
                            <div class="col-lg-3 col-md-6"><button type="submit" class="btn btn--save waves-effect" value="confirm" id="submitBtn">OPSLAAN</button></div>
                            <div class="col-lg-3 col-md-6"></div>
                            </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    <script src="assets/bundles/libscripts.bundle.js"></script>
    <script src="assets/bundles/vendorscripts.bundle.js"></script>
    <?php
        require_once('includes/footerScriptsAddForms.php');
    ?>
    <script src="assets/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.js"></script> <!-- Bootstrap Colorpicker Js -->
    <script src="assets/plugins/jquery-inputmask/jquery.inputmask.bundle.js"></script> <!-- Input Mask Plugin Js -->
    <script src="assets/plugins/multi-select/js/jquery.multi-select.js"></script> <!-- Multi Select Plugin Js -->
    <script src="assets/plugins/jquery-spinner/js/jquery.spinner.js"></script> <!-- Jquery Spinner Plugin Js -->
    <script src="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script> <!-- Bootstrap Tags Input Plugin Js -->
    <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
    <script src="assets/js/pages/forms/advanced-form-elements.js"></script>
    <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
    <script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
    <script src="assets/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>
    <script src="assets/js/pages/ui/jquery.ui.touch-punch.js"></script> <!-- Custom Js -->

<script>
    var sliderCounter = 1; // TODO Must be dynamic CHECK WHATS THE LAST ID IN CASE ITS FROM DB
    var sliderCounterCoreDin = 1;
    var sliderCounterCoreWoe = 1;
    var sliderCounterCoreDon = 1;
    var sliderCounterCoreVri = 1;
    var sliderCounterCoreZat = 1;
    var sliderCounterCoreZon = 1;

    $(document).ready(function(){

        initAdder(sliderCounter, sliderCounterCoreDin, sliderCounterCoreWoe, sliderCounterCoreDon, sliderCounterCoreVri, sliderCounterCoreZat, sliderCounterCoreZon);

        // ADD TIME SLIDER FUNCTION
        function addTimeSlotv2(e, startTimeParam = "10:30", endTimeParam = "13:15", day2 = null, initCall = false) {
            let id;
            let dayParam;
            let day;
            let dayId;

            if(e) {
                id = e.currentTarget.attributes[2].value;
                dayParam = id;
                day = id.replace(/[0-9]/g, ''); // id without number
            } else if(day2 != null){
                dayId = day2;
                day2 = day2.replace(/[0-9]/g, '');
                day = day2;
                //id = day2;
                dayParam = day2;
            }

            let startTime = startTimeParam;
            let endTime = endTimeParam;

            // DEPRACATED?
            let englishWeek = {
                "mon":  "mon",
                "din":  "tue",
                "woe":  "wed",
                "don":  "thur",
                "vri":  "fri",
                "zat":  "sat",
                "zon":  "sun"
            };

            $('.acplus'+day).remove();

            let sliderTemplate = '';

            if($('.slideTable-'+day).children('div').length === 0) {
                // first adding

                sliderTemplate = $("<span  class='glyphicon glyphicon glyphicon-plus-sign acplus"+day+"' id='plus' name='"+day+"'></span><div class='time-range-" + day + "[]'>\n" +
                "<span class='glyphicon glyphicon glyphicon-minus-sign acminus"+day+"'' id='minus' day='"+day+"'></span>\n" +
                "\t\t\t\t\t\t\t\t\t<p>Beschikbaarheid : <br/>\n" +
                "\t\t\t\t\t\t\t\t\t\t<span class='" + englishWeek[day] +"-slider-time'>" + startTime + "</span> - <span class='" + englishWeek[day] +"-slider-time2'>" + endTime + "</span>\n" +
                "\t\t\t\t\t\t\t\t\t</p>\n" +
                "\n" +
                "\t\t\t\t\t\t\t\t\t<div class='sliders_step " + day + "_sliders_step'>\n" +
                "\t\t\t\t\t\t\t\t\t\t<div class=\"slider-range\" day='" + englishWeek[day] +"'></div>\n" +
                "\t\t\t\t\t\t\t\t\t</div>\n" +
                "\n" +
                "\t\t\t\t\t\t\t\t   <p id='" + englishWeek[day] +"-error' class=\"errorlabel\"></p>\n" +
                "\t\t\t\t\t\t\t\t</div>");

                $('.slideTable-' + day).append(sliderTemplate);
                setTime(".slider-range[day=" + englishWeek[day] + "]", 630, 795);
            } else {
                if(day == 'mon') {
                    sliderTemplate = $("<span  class='glyphicon glyphicon glyphicon-plus-sign acplus"+day+"' id='plus' name='"+day+"'></span><div class='time-range-" + day + "[]'>\n" +
                    "<span class='glyphicon glyphicon glyphicon-minus-sign acminus"+day+"'' id='minus' day='"+day+"'></span>\n" +
                    "\t\t\t\t\t\t\t\t\t<p>Beschikbaarheid : <br/>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<span class='" + day + sliderCounter + "-slider-time'>" + startTime + "</span> - <span class='" + day + sliderCounter + "-slider-time2'>" + endTime + "</span>\n" +
                    "\t\t\t\t\t\t\t\t\t</p>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='sliders_step " + dayParam + "_sliders_step'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<div class=\"slider-range\" day='" + day + sliderCounter + "'></div>\n" +
                    "\t\t\t\t\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t   <p id='" + dayParam + sliderCounter + "-error' class=\"errorlabel\"></p>\n" +
                    "\t\t\t\t\t\t\t\t</div>");

                    $('.slideTable-' + day).append(sliderTemplate);
                    setTime(".slider-range[day=" + day + sliderCounter + "]", 630, 795);
                    sliderCounter++;
                } else if(day == 'din') {
                    sliderTemplate = $("<span  class='glyphicon glyphicon glyphicon-plus-sign acplus"+day+"' id='plus' name='"+day+"'></span><div class='time-range-" + day + "[]'>\n" +
                    "<span class='glyphicon glyphicon glyphicon-minus-sign acminus"+day+"'' id='minus' day='"+day+"'></span>\n" +
                    "\t\t\t\t\t\t\t\t\t<p>Beschikbaarheid : <br/>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<span class='" + day + sliderCounterCoreDin + "-slider-time'>" + startTime + "</span> - <span class='" + day + sliderCounterCoreDin + "-slider-time2'>" + endTime + "</span>\n" +
                    "\t\t\t\t\t\t\t\t\t</p>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='sliders_step " + dayParam + "_sliders_step'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<div class=\"slider-range\" day='" + day + sliderCounterCoreDin + "'></div>\n" +
                    "\t\t\t\t\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t   <p id='" + dayParam + sliderCounterCoreDin + "-error' class=\"errorlabel\"></p>\n" +
                    "\t\t\t\t\t\t\t\t</div>");

                    $('.slideTable-' + day).append(sliderTemplate);
                    setTime(".slider-range[day=" + day + sliderCounterCoreDin + "]", 630, 795);
                    sliderCounterCoreDin++;
                } else if(day == 'woe') {
                    sliderTemplate = $("<span  class='glyphicon glyphicon glyphicon-plus-sign acplus"+day+"' id='plus' name='"+day+"'></span><div class='time-range-" + day + "[]'>\n" +
                    "<span class='glyphicon glyphicon glyphicon-minus-sign acminus"+day+"'' id='minus' day='"+day+"'></span>\n" +
                    "\t\t\t\t\t\t\t\t\t<p>Beschikbaarheid : <br/>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<span class='" + day + sliderCounterCoreWoe + "-slider-time'>" + startTime + "</span> - <span class='" + day + sliderCounterCoreWoe + "-slider-time2'>" + endTime + "</span>\n" +
                    "\t\t\t\t\t\t\t\t\t</p>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='sliders_step " + dayParam + "_sliders_step'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<div class=\"slider-range\" day='" + day + sliderCounterCoreWoe + "'></div>\n" +
                    "\t\t\t\t\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t   <p id='" + dayParam + sliderCounterCoreWoe + "-error' class=\"errorlabel\"></p>\n" +
                    "\t\t\t\t\t\t\t\t</div>");

                    $('.slideTable-' + day).append(sliderTemplate);
                    setTime(".slider-range[day=" + day + sliderCounterCoreWoe + "]", 630, 795);
                    sliderCounterCoreWoe++;
                } else if(day == 'don') {
                    sliderTemplate = $("<span  class='glyphicon glyphicon glyphicon-plus-sign acplus"+day+"' id='plus' name='"+day+"'></span><div class='time-range-" + day + "[]'>\n" +
                    "<span class='glyphicon glyphicon glyphicon-minus-sign acminus"+day+"'' id='minus' day='"+day+"'></span>\n" +
                    "\t\t\t\t\t\t\t\t\t<p>Beschikbaarheid : <br/>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<span class='" + day + sliderCounterCoreDon + "-slider-time'>" + startTime + "</span> - <span class='" + day + sliderCounterCoreDon + "-slider-time2'>" + endTime + "</span>\n" +
                    "\t\t\t\t\t\t\t\t\t</p>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='sliders_step " + dayParam + "_sliders_step'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<div class=\"slider-range\" day='" + day + sliderCounterCoreDon + "'></div>\n" +
                    "\t\t\t\t\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t   <p id='" + dayParam + sliderCounterCoreDon + "-error' class=\"errorlabel\"></p>\n" +
                    "\t\t\t\t\t\t\t\t</div>");

                    $('.slideTable-' + day).append(sliderTemplate);
                    setTime(".slider-range[day=" + day + sliderCounterCoreDon + "]", 630, 795);
                    sliderCounterCoreDon++;
                } else if(day == 'vri'){
                    sliderTemplate = $("<span  class='glyphicon glyphicon glyphicon-plus-sign acplus"+day+"' id='plus' name='"+day+"'></span><div class='time-range-" + day + "[]'>\n" +
                    "<span class='glyphicon glyphicon glyphicon-minus-sign acminus"+day+"'' id='minus' day='"+day+"'></span>\n" +
                    "\t\t\t\t\t\t\t\t\t<p>Beschikbaarheid : <br/>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<span class='" + day + sliderCounterCoreVri + "-slider-time'>" + startTime + "</span> - <span class='" + day + sliderCounterCoreVri + "-slider-time2'>" + endTime + "</span>\n" +
                    "\t\t\t\t\t\t\t\t\t</p>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='sliders_step " + dayParam + "_sliders_step'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<div class=\"slider-range\" day='" + day + sliderCounterCoreVri + "'></div>\n" +
                    "\t\t\t\t\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t   <p id='" + dayParam + sliderCounterCoreVri + "-error' class=\"errorlabel\"></p>\n" +
                    "\t\t\t\t\t\t\t\t</div>");

                    $('.slideTable-' + day).append(sliderTemplate);
                    setTime(".slider-range[day=" + day + sliderCounterCoreVri + "]", 630, 795);
                    sliderCounterCoreVri++;
                }
                else if(day == 'zat') {
                    sliderTemplate = $("<span  class='glyphicon glyphicon glyphicon-plus-sign acplus"+day+"' id='plus' name='"+day+"'></span><div class='time-range-" + day + "[]'>\n" +
                    "<span class='glyphicon glyphicon glyphicon-minus-sign acminus"+day+"'' id='minus' day='"+day+"'></span>\n" +
                    "\t\t\t\t\t\t\t\t\t<p>Beschikbaarheid : <br/>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<span class='" + day + sliderCounterCoreZat + "-slider-time'>" + startTime + "</span> - <span class='" + day + sliderCounterCoreZat + "-slider-time2'>" + endTime + "</span>\n" +
                    "\t\t\t\t\t\t\t\t\t</p>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='sliders_step " + dayParam + "_sliders_step'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<div class=\"slider-range\" day='" + day + sliderCounterCoreZat + "'></div>\n" +
                    "\t\t\t\t\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t   <p id='" + dayParam + sliderCounterCoreZat + "-error' class=\"errorlabel\"></p>\n" +
                    "\t\t\t\t\t\t\t\t</div>");

                    $('.slideTable-' + day).append(sliderTemplate);
                    setTime(".slider-range[day=" + day + sliderCounterCoreZat + "]", 630, 795);
                    sliderCounterCoreZat++;
                } else if(day == 'zon'){
                    sliderTemplate = $("<span  class='glyphicon glyphicon glyphicon-plus-sign acplus"+day+"' id='plus' name='"+day+"'></span><div class='time-range-" + day + "[]'>\n" +
                    "<span class='glyphicon glyphicon glyphicon-minus-sign acminus"+day+"'' id='minus' day='"+day+"'></span>\n" +
                    "\t\t\t\t\t\t\t\t\t<p>Beschikbaarheid : <br/>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<span class='" + day + sliderCounterCoreZon + "-slider-time'>" + startTime + "</span> - <span class='" + day + sliderCounterCoreZon + "-slider-time2'>" + endTime + "</span>\n" +
                    "\t\t\t\t\t\t\t\t\t</p>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='sliders_step " + dayParam + "_sliders_step'>\n" +
                    "\t\t\t\t\t\t\t\t\t\t<div class=\"slider-range\" day='" + day + sliderCounterCoreZon + "'></div>\n" +
                    "\t\t\t\t\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\t\t\t\t\t\t\t\t   <p id='" + dayParam + sliderCounterCoreZon + "-error' class=\"errorlabel\"></p>\n" +
                    "\t\t\t\t\t\t\t\t</div>");

                    $('.slideTable-' + day).append(sliderTemplate);
                    setTime(".slider-range[day=" + day + sliderCounterCoreZon + "]", 630, 795);
                    sliderCounterCoreZon++;
                }
            }

            // if parent slideTable had only one plus button,
            // then remove text-align:center from this parent
            if($('.slideTable-' + day).find('.time-range') && $('.slideTable-' + day).hasClass('slideTable--text-center')) {
                $('.slideTable-' + day).removeClass('slideTable--text-center');
            }

            // ADD CLICK EVENTS TO PLUS / MINUS BUTTONS
            $('.acplus'+day).on('click', function (e) {
                addTimeSlotv2(e);
            });
            $('.acminus'+day).on('click', function (e) {
                centerPlusIconOnEmptySlideTable($(this));
                deleteTimeSlot($(this), e);
            });

            if(!initCall && ('#'+day+'-area-error').length > 0) {
                let errorLabelsOfWeekDayTimeRange = $(".errorlabel[id*="+day+"]:not([id$=-area-error]):not(:empty), .errorlabel[id*="+englishWeek[day]+"]:not([id$=-area-error]):not(:empty)");

                if(errorLabelsOfWeekDayTimeRange.length > 0) {
                    $('#'+day+'-area-error').text("");
                } else {
                    $('#'+day+'-area-error').text('Klik op Opslaan om je veranderingen op te slaan.');
                }
            } else if(!initCall) {
                $('#'+dayId+'-area-error').text('Klik op Opslaan om je veranderingen op te slaan.');
            }
        }

        // if deleting a time-range and the parent slideTable is empty,
        // then make the slideTable text-align:center
        function centerPlusIconOnEmptySlideTable(minusIconButton) {
            let slideTable = $('.slideTable-'+minusIconButton.attr('day')).first();

            if(slideTable.find('.sliders_step, .time-range').length < 2) {
                slideTable.addClass('slideTable--text-center');
            }
        }

        // DELETE TIME SLOT FUNCTION
        function deleteTimeSlot(f, e) {
            var id = e.currentTarget.attributes[3].value;
            let newDay = e.currentTarget.attributes['day'].nodeValue;
            newDay = newDay.replace(/[0-9]/g, '');

            if($('.slideTable-' + id).children('div').length === 1){
                $('.slideTable-' + id).empty();
                var adder = $("<span class='glyphicon glyphicon glyphicon-plus-sign plus-center acplus"+newDay+"' id='plus' name='"+ newDay + sliderCounter + "'>");
                $('.slideTable-' + id).prepend(adder);
            }
            else {
            //id = id.replace(/^\D+/g, '');
                let p = f.parents().eq(0);
                let container = f.parents().eq(1);

                p[0].remove();
                // p.remove();

                $('.acplus'+newDay).remove();
                var adder = $("<span class='glyphicon glyphicon glyphicon-plus-sign acplus"+newDay+"' id='plus' name='"+ newDay + sliderCounter + "'>");

                if($('.slideTable-' + id).children('div').length === 1) {
                    $('.slideTable-' + id).prepend(adder);
                }
                else {
                    $('.slideTable-' + id).children().last().before(adder);
                }
            }

            adder.on('click', function (e) {
                addTimeSlotv2(e);
            });

            if($('#'+e.currentTarget.attributes['day'].nodeValue+'-area-error').length > 0) {
                let englishWeek = {
                    "mon": "mon",
                    "din": "tue",
                    "woe": "wed",
                    "don": "thur",
                    "vri": "fri",
                    "zat": "sat",
                    "zon": "sun"
                };
                let day = e.currentTarget.attributes['day'].nodeValue;
                let errorLabelsOfWeekDayTimeRange = $(".errorlabel[id*=" + day + "]:not([id$=-area-error]):not(:empty), .errorlabel[id*=" + englishWeek[day] + "]:not([id$=-area-error]):not(:empty)");

                if (errorLabelsOfWeekDayTimeRange.length > 0) {
                    $('#' + day + '-area-error').text("");
                } else {
                    $('#' + day + '-area-error').text('Klik op Opslaan om je veranderingen op te slaan.');
                }
            } else {
                $('#'+newDay+'-area-error').text('Klik op Opslaan om je veranderingen op te slaan.');
            }
            exit;       // MAKES PROBLEMS WHEN LEAVING OUT
        }

        $(document).ready(function(){
            <?php
                foreach ($mon_add as $key => $value){
                    foreach ($value as $key2 => $value2){
                        //echo 'value: ' . $value2 . 'key ' . $key2 . '<br><br>';
                        $pos = strpos($value2, ':', strpos($value2, ':') + 1);
                        $firstPart = substr($value2, 0, $pos);
                        $secondPart = substr($value2, $pos + 1, strlen($value2));
                        $command2 = 'addTimeSlotv2(null,"' . $firstPart . '","' . $secondPart . '","' . $key2 . '",true);';
                        echo  $command2;
                        $command = 'setTime(".slider-range[day='. $key2. ']",' . getmins($firstPart) . ',' . getmins($secondPart) . ');';
                        echo $command;
                    }
                }
                foreach ($tue_add as $key => $value){
                    foreach ($value as $key2 => $value2){
                        //echo 'value: ' . $value2 . 'key ' . $key2 . '<br><br>';
                        $pos = strpos($value2, ':', strpos($value2, ':') + 1);
                        $firstPart = substr($value2, 0, $pos);
                        $secondPart = substr($value2, $pos + 1, strlen($value2));
                        $command2 = 'addTimeSlotv2(null,"' . $firstPart . '","' . $secondPart . '","' . $key2 . '",true);';
                        echo  $command2;
                        $command = 'setTime(".slider-range[day='. $key2. ']",' . getmins($firstPart) . ',' . getmins($secondPart) . ');';
                        echo $command;
                    }
                }
                foreach ($wed_add as $key => $value){
                    foreach ($value as $key2 => $value2){
                        //echo 'value: ' . $value2 . 'key ' . $key2 . '<br><br>';
                        $pos = strpos($value2, ':', strpos($value2, ':') + 1);
                        $firstPart = substr($value2, 0, $pos);
                        $secondPart = substr($value2, $pos + 1, strlen($value2));
                        $command2 = 'addTimeSlotv2(null,"' . $firstPart . '","' . $secondPart . '","' . $key2 . '",true);';
                        echo  $command2;
                        $command = 'setTime(".slider-range[day='. $key2. ']",' . getmins($firstPart) . ',' . getmins($secondPart) . ');';
                        echo $command;
                    }
                }
                foreach ($thu_add as $key => $value){
                    foreach ($value as $key2 => $value2){
                        //echo 'value: ' . $value2 . 'key ' . $key2 . '<br><br>';
                        $pos = strpos($value2, ':', strpos($value2, ':') + 1);
                        $firstPart = substr($value2, 0, $pos);
                        $secondPart = substr($value2, $pos + 1, strlen($value2));
                        $command2 = 'addTimeSlotv2(null,"' . $firstPart . '","' . $secondPart . '","' . $key2 . '",true);';
                        echo  $command2;
                        $command = 'setTime(".slider-range[day='. $key2. ']",' . getmins($firstPart) . ',' . getmins($secondPart) . ');';
                        echo $command;
                    }
                }
                foreach ($fri_add as $key => $value){
                    foreach ($value as $key2 => $value2){
                        //echo 'value: ' . $value2 . 'key ' . $key2 . '<br><br>';
                        $pos = strpos($value2, ':', strpos($value2, ':') + 1);
                        $firstPart = substr($value2, 0, $pos);
                        $secondPart = substr($value2, $pos + 1, strlen($value2));
                        $command2 = 'addTimeSlotv2(null,"' . $firstPart . '","' . $secondPart . '","' . $key2 . '",true);';
                        echo  $command2;
                        $command = 'setTime(".slider-range[day='. $key2. ']",' . getmins($firstPart) . ',' . getmins($secondPart) . ');';
                        echo $command;
                    }
                }
                foreach ($sat_add as $key => $value){
                    foreach ($value as $key2 => $value2){
                        //echo 'value: ' . $value2 . 'key ' . $key2 . '<br><br>';
                        $pos = strpos($value2, ':', strpos($value2, ':') + 1);
                        $firstPart = substr($value2, 0, $pos);
                        $secondPart = substr($value2, $pos + 1, strlen($value2));
                        $command2 = 'addTimeSlotv2(null,"' . $firstPart . '","' . $secondPart . '","' . $key2 . '",true);';
                        echo  $command2;
                        $command = 'setTime(".slider-range[day='. $key2. ']",' . getmins($firstPart) . ',' . getmins($secondPart) . ');';
                        echo $command;
                    }
                }
                foreach ($sun_add as $key => $value){
                    foreach ($value as $key2 => $value2){
                        //echo 'value: ' . $value2 . 'key ' . $key2 . '<br><br>';
                        $pos = strpos($value2, ':', strpos($value2, ':') + 1);
                        $firstPart = substr($value2, 0, $pos);
                        $secondPart = substr($value2, $pos + 1, strlen($value2));
                        $command2 = 'addTimeSlotv2(null,"' . $firstPart . '","' . $secondPart . '","' . $key2 . '",true);';
                        echo  $command2;
                        $command = 'setTime(".slider-range[day='. $key2. ']",' . getmins($firstPart) . ',' . getmins($secondPart) . ');';
                        echo $command;
                    }
                }
            ?>
        });

        // add slider function
        function initAdder(sliderCounter, sliderCounterCoreDin, sliderCounterCoreWoe, sliderCounterCoreDon, sliderCounterCoreVri, sliderCounterCoreZat, sliderCounterCoreZon) {
            $(".glyphicon-plus-sign").unbind('click');
            //    $('.glyphicon-plus-sign').off('click');
            //   $('.glyphicon-minus-sign').off('click');
            $('.glyphicon-minus-sign').unbind('click');

            // plus sign add timeslot functionality
            $('.glyphicon-plus-sign').click(function (e) {
                addTimeSlotv2(e);
            });

            // minus sign remove timeslot functionality
            $('.glyphicon-minus-sign').click(function (e) {
                centerPlusIconOnEmptySlideTable($(this));
                deleteTimeSlot($(this), e);
                return;
            });
        }

        $(".page-loader-wrapper").css("display", "none");

        $(document).ajaxStart(function() {
            $(document).css({'cursor' : 'wait'});
        }).ajaxStop(function() {
            $(document.body).css({'cursor' : 'default'});
        });

        /* SUBMIT BUTTON / FUNCTION */
        $("#myform").submit(function(e) {
            e.preventDefault();
            $(document).css({'cursor': 'wait'});
            $("#submitBtn").prop("disabled", true);
            $('.errorOverlap').remove();

            // MONDAY
            let monday_additional = [];
            $('.slideTable-mon div[class*=time-range]').each(function (count, html) {
                if (count > 0) {
                    let startTime = $(this).find('span[class*=slider-time]').eq(0).html();
                    let endTime = $(this).find('span[class*=slider-time]').eq(1).html();
                    let jsonObj = {};
                    jsonObj["mon" + count] = startTime + ":" + endTime;
                    monday_additional.push(jsonObj);
                }
            });
            monday_additional = JSON.stringify(monday_additional);

            // set existing times
            var mon_existing_list = $('.slideTable-mon div[class*=time-range]');
            var mon_time_list = [];

            for (let i = 0; i < mon_existing_list.length; i++) {
                let timeObj = {};
                let startTime;
                let EndTime;
                let startMins;
                let EndMins;
                timeObj['day'] = 'mon';

                let timeSpanFirst = $(mon_existing_list[i]).find('span[class*=slider-time]').eq(0).text();
                let timeSpanSecond = $(mon_existing_list[i]).find('span[class*=slider-time]').eq(1).text();

                startTime = timeSpanFirst.split(":").shift();
                startMins = timeSpanFirst.split(":").pop();

                EndTime = timeSpanSecond.split(":").shift();
                EndMins = timeSpanSecond.split(":").pop();

                let Stime = "";
                let Ftime = "";
                timeObj['id'] = i;
                timeObj['start'] = +(Stime.concat(startTime, startMins));
                timeObj['finish'] = +(Ftime.concat(EndTime, EndMins));
                timeObj['element'] = mon_existing_list[i];
                mon_time_list.push(timeObj);
            }

            if (mon_time_list.length > 1) {
                // loop through time list - i < mon_time_list.length
                for (let i = 0; i < mon_time_list.length; i++) {

                    // loop through time list and compare each item aginst all other items - i < mon_time_list.length
                    for (let j=0; j < mon_time_list.length; j++){

                        // only compare items that aren't duplicates
                        if(mon_time_list[i]['id'] !== mon_time_list[j]['id']) {

                            // does j start before i finish and does j finish before i start
                            if(mon_time_list[j]['start'] < mon_time_list[i]['finish'] && mon_time_list[j]['finish'] > mon_time_list[i]['start']) {
                                $(document).css({'cursor': 'default'});
                                $("#submitBtn").prop("disabled", false);
                                $(mon_time_list[j]['element']).append('<p class="errorOverlap">Zorg ervoor dat beschikbaarheden niet overlappen.</p>');
                                $('.errorlabel').empty();
                                return;
                            }
                        }
                    }
                }
            }

            // TUESDAY
            let tuesday_additional = [];
            $('.slideTable-din div[class*=time-range]').each(function (count, html) {
                if (count > 0) {
                    let startTime = $(this).find('span[class*=slider-time]').eq(0).html();
                    let endTime = $(this).find('span[class*=slider-time]').eq(1).html();
                    let jsonObj = {};
                    jsonObj["din" + count] = startTime + ":" + endTime;
                    tuesday_additional.push(jsonObj);
                }
            });
            tuesday_additional = JSON.stringify(tuesday_additional);

            // set existing times
            var tue_existing_list = $('.slideTable-din div[class*=time-range]');
            var tue_time_list = [];

            for (let i = 0; i < tue_existing_list.length; i++) {
                let timeObj = {};
                let startTime;
                let EndTime;
                let startMins;
                let EndMins;
                timeObj['day'] = 'din';

                let timeSpanFirst = $(tue_existing_list[i]).find('span[class*=slider-time]').eq(0).text();
                let timeSpanSecond = $(tue_existing_list[i]).find('span[class*=slider-time]').eq(1).text();

                startTime = timeSpanFirst.split(":").shift();
                startMins = timeSpanFirst.split(":").pop();

                EndTime = timeSpanSecond.split(":").shift();
                EndMins = timeSpanSecond.split(":").pop();

                let Stime = "";
                let Ftime = "";
                timeObj['id'] = i;
                timeObj['start'] = +(Stime.concat(startTime, startMins));
                timeObj['finish'] = +(Ftime.concat(EndTime, EndMins));
                timeObj['element'] = tue_existing_list[i];
                tue_time_list.push(timeObj);
            }

            if (tue_time_list.length > 1) {

                // loop through time list - i < mon_time_list.length
                for (let i = 0; i < tue_time_list.length; i++) {

                    // loop through time list and compare each item aginst all other items - i < mon_time_list.length
                    for (let j=0; j < tue_time_list.length; j++){

                        // only compare items that aren't duplicates
                        if(tue_time_list[i]['id'] !== tue_time_list[j]['id']) {

                            // does j start before i finish and does j finish before i start
                            if(tue_time_list[j]['start'] < tue_time_list[i]['finish'] && tue_time_list[j]['finish'] > tue_time_list[i]['start']) {
                                $(document).css({'cursor': 'default'});
                                $("#submitBtn").prop("disabled", false);
                                $(tue_time_list[j]['element']).append('<p class="errorOverlap">Zorg ervoor dat beschikbaarheden niet overlappen.</p>');
                                $('.errorlabel').empty();
                                return;
                            }
                        }
                    }
                }
            }

            // WEDNESDAY
            let wednesday_additional = [];
            $('.slideTable-woe div[class*=time-range]').each(function (count, html) {
                if (count > 0) {
                    let startTime = $(this).find('span[class*=slider-time]').eq(0).html();
                    let endTime = $(this).find('span[class*=slider-time]').eq(1).html();
                    let jsonObj = {};
                    jsonObj["woe" + count] = startTime + ":" + endTime;
                    wednesday_additional.push(jsonObj);
                }
            });
            wednesday_additional = JSON.stringify(wednesday_additional);

            // set existing times
            var wed_existing_list = $('.slideTable-woe div[class*=time-range]');
            var wed_time_list = [];

            for (let i = 0; i < wed_existing_list.length; i++) {
                let timeObj = {};
                let startTime;
                let EndTime;
                let startMins;
                let EndMins;
                timeObj['day'] = 'woe';

                let timeSpanFirst = $(wed_existing_list[i]).find('span[class*=slider-time]').eq(0).text();
                let timeSpanSecond = $(wed_existing_list[i]).find('span[class*=slider-time]').eq(1).text();

                startTime = timeSpanFirst.split(":").shift();
                startMins = timeSpanFirst.split(":").pop();

                EndTime = timeSpanSecond.split(":").shift();
                EndMins = timeSpanSecond.split(":").pop();

                let Stime = "";
                let Ftime = "";
                timeObj['id'] = i;
                timeObj['start'] = +(Stime.concat(startTime, startMins));
                timeObj['finish'] = +(Ftime.concat(EndTime, EndMins));
                timeObj['element'] = wed_existing_list[i];
                wed_time_list.push(timeObj);
            }

            if (wed_time_list.length > 1) {

                // loop through time list - i < mon_time_list.length
                for (let i = 0; i < wed_time_list.length; i++) {

                    // loop through time list and compare each item aginst all other items - i < mon_time_list.length
                    for (let j=0; j < wed_time_list.length; j++){

                        // only compare items that aren't duplicates
                        if(wed_time_list[i]['id'] !== wed_time_list[j]['id']) {

                            // does j start before i finish and does j finish before i start
                            if(wed_time_list[j]['start'] < wed_time_list[i]['finish'] && wed_time_list[j]['finish'] > wed_time_list[i]['start']) {
                                $(document).css({'cursor': 'default'});
                                $("#submitBtn").prop("disabled", false);
                                $(wed_time_list[j]['element']).append('<p class="errorOverlap">Zorg ervoor dat beschikbaarheden niet overlappen.</p>');
                                $('.errorlabel').empty();
                                return;
                            }
                        }
                    }
                }
            }

            // THURSDAY
            let thursday_additional = [];
            $('.slideTable-don div[class*=time-range]').each(function (count, html) {
                if (count > 0) {
                    let startTime = $(this).find('span[class*=slider-time]').eq(0).html();
                    let endTime = $(this).find('span[class*=slider-time]').eq(1).html();
                    let jsonObj = {};
                    jsonObj["don" + count] = startTime + ":" + endTime;
                    thursday_additional.push(jsonObj);
                }
            });
            thursday_additional = JSON.stringify(thursday_additional);

            // set existing times
            var thu_existing_list = $('.slideTable-don div[class*=time-range]');
            var thu_time_list = [];

            for (let i = 0; i < thu_existing_list.length; i++) {
                let timeObj = {};
                let startTime;
                let EndTime;
                let startMins;
                let EndMins;
                timeObj['day'] = 'don';

                let timeSpanFirst = $(thu_existing_list[i]).find('span[class*=slider-time]').eq(0).text();
                let timeSpanSecond = $(thu_existing_list[i]).find('span[class*=slider-time]').eq(1).text();

                startTime = timeSpanFirst.split(":").shift();
                startMins = timeSpanFirst.split(":").pop();

                EndTime = timeSpanSecond.split(":").shift();
                EndMins = timeSpanSecond.split(":").pop();

                let Stime = "";
                let Ftime = "";
                timeObj['id'] = i;
                timeObj['start'] = +(Stime.concat(startTime, startMins));
                timeObj['finish'] = +(Ftime.concat(EndTime, EndMins));
                timeObj['element'] = thu_existing_list[i];
                thu_time_list.push(timeObj);
            }

            if (thu_time_list.length > 1) {

                // loop through time list - i < mon_time_list.length
                for (let i = 0; i < thu_time_list.length; i++) {

                    // loop through time list and compare each item aginst all other items - i < mon_time_list.length
                    for (let j=0; j < thu_time_list.length; j++){

                        // only compare items that aren't duplicates
                        if(thu_time_list[i]['id'] !== thu_time_list[j]['id']) {

                            // does j start before i finish and does j finish before i start
                            if(thu_time_list[j]['start'] < thu_time_list[i]['finish'] && thu_time_list[j]['finish'] > thu_time_list[i]['start']) {
                                $(document).css({'cursor': 'default'});
                                $("#submitBtn").prop("disabled", false);
                                $(thu_time_list[j]['element']).append('<p class="errorOverlap">Zorg ervoor dat beschikbaarheden niet overlappen.</p>');
                                $('.errorlabel').empty();
                                return;
                            }
                        }
                    }
                }
            }

            // FRIDAY
            let friday_additional = [];
            $('.slideTable-vri div[class*=time-range]').each(function (count, html) {
                if (count > 0) {
                    let startTime = $(this).find('span[class*=slider-time]').eq(0).html();
                    let endTime = $(this).find('span[class*=slider-time]').eq(1).html();
                    let jsonObj = {};
                    jsonObj["vri" + count] = startTime + ":" + endTime;
                    friday_additional.push(jsonObj);
                }
            });
            friday_additional = JSON.stringify(friday_additional);

            // set existing times
            var fri_existing_list = $('.slideTable-vri div[class*=time-range]');
            var fri_time_list = [];

            for (let i = 0; i < fri_existing_list.length; i++) {
                let timeObj = {};
                let startTime;
                let EndTime;
                let startMins;
                let EndMins;
                timeObj['day'] = 'vri';

                let timeSpanFirst = $(fri_existing_list[i]).find('span[class*=slider-time]').eq(0).text();
                let timeSpanSecond = $(fri_existing_list[i]).find('span[class*=slider-time]').eq(1).text();

                startTime = timeSpanFirst.split(":").shift();
                startMins = timeSpanFirst.split(":").pop();

                EndTime = timeSpanSecond.split(":").shift();
                EndMins = timeSpanSecond.split(":").pop();

                let Stime = "";
                let Ftime = "";
                timeObj['id'] = i;
                timeObj['start'] = +(Stime.concat(startTime, startMins));
                timeObj['finish'] = +(Ftime.concat(EndTime, EndMins));
                timeObj['element'] = fri_existing_list[i];
                fri_time_list.push(timeObj);
            }

            if (fri_time_list.length > 1) {

                // loop through time list - i < mon_time_list.length
                for (let i = 0; i < fri_time_list.length; i++) {

                    // loop through time list and compare each item aginst all other items - i < mon_time_list.length
                    for (let j=0; j < fri_time_list.length; j++){

                        // only compare items that aren't duplicates
                        if(fri_time_list[i]['id'] !== fri_time_list[j]['id']) {

                            // does j start before i finish and does j finish before i start
                            if(fri_time_list[j]['start'] < fri_time_list[i]['finish'] && fri_time_list[j]['finish'] > fri_time_list[i]['start']) {
                                $(document).css({'cursor': 'default'});
                                $("#submitBtn").prop("disabled", false);
                                $(fri_time_list[j]['element']).append('<p class="errorOverlap">Zorg ervoor dat beschikbaarheden niet overlappen.</p>');
                                $('.errorlabel').empty();
                                return;
                            }
                        }
                    }
                }
            }

            // SATRUDAY
            let saturday_additional = [];
            $('.slideTable-zat div[class*=time-range]').each(function (count, html) {
                if (count > 0) {
                    let startTime = $(this).find('span[class*=slider-time]').eq(0).html();
                    let endTime = $(this).find('span[class*=slider-time]').eq(1).html();
                    let jsonObj = {};
                    jsonObj["zat" + count] = startTime + ":" + endTime;
                    saturday_additional.push(jsonObj);
                }
            });
            saturday_additional = JSON.stringify(saturday_additional);

            // set existing times
            var sat_existing_list = $('.slideTable-zat div[class*=time-range]');
            var sat_time_list = [];

            for (let i = 0; i < sat_existing_list.length; i++) {
                let timeObj = {};
                let startTime;
                let EndTime;
                let startMins;
                let EndMins;
                timeObj['day'] = 'zat';

                let timeSpanFirst = $(sat_existing_list[i]).find('span[class*=slider-time]').eq(0).text();
                let timeSpanSecond = $(sat_existing_list[i]).find('span[class*=slider-time]').eq(1).text();

                startTime = timeSpanFirst.split(":").shift();
                startMins = timeSpanFirst.split(":").pop();

                EndTime = timeSpanSecond.split(":").shift();
                EndMins = timeSpanSecond.split(":").pop();

                let Stime = "";
                let Ftime = "";
                timeObj['id'] = i;
                timeObj['start'] = +(Stime.concat(startTime, startMins));
                timeObj['finish'] = +(Ftime.concat(EndTime, EndMins));
                timeObj['element'] = sat_existing_list[i];
                sat_time_list.push(timeObj);
            }

            if (sat_time_list.length > 1) {

                // loop through time list - i < mon_time_list.length
                for (let i = 0; i < sat_time_list.length; i++) {

                    // loop through time list and compare each item aginst all other items - i < mon_time_list.length
                    for (let j=0; j < sat_time_list.length; j++){

                        // only compare items that aren't duplicates
                        if(sat_time_list[i]['id'] !== sat_time_list[j]['id']) {

                            // does j start before i finish and does j finish before i start
                            if(sat_time_list[j]['start'] < sat_time_list[i]['finish'] && sat_time_list[j]['finish'] > sat_time_list[i]['start']) {
                                $(document).css({'cursor': 'default'});
                                $("#submitBtn").prop("disabled", false);
                                $(sat_time_list[j]['element']).append('<p class="errorOverlap">Zorg ervoor dat beschikbaarheden niet overlappen.</p>');
                                $('.errorlabel').empty();
                                return;
                            }
                        }
                    }
                }
            }

            // SUNDAY
            let sunday_additional = [];
            $('.slideTable-zon div[class*=time-range]').each(function (count, html) {
                if (count > 0) {
                    let startTime = $(this).find('span[class*=slider-time]').eq(0).html();
                    let endTime = $(this).find('span[class*=slider-time]').eq(1).html();
                    let jsonObj = {};
                    jsonObj["zon" + count] = startTime + ":" + endTime;
                    sunday_additional.push(jsonObj);
                }
            });
            sunday_additional = JSON.stringify(sunday_additional);

            // set existing times
            var sun_existing_list = $('.slideTable-zon div[class*=time-range]');
            var sun_time_list = [];

            for (let i = 0; i < sun_existing_list.length; i++) {
                let timeObj = {};
                let startTime;
                let EndTime;
                let startMins;
                let EndMins;
                timeObj['day'] = 'zon';

                let timeSpanFirst = $(sun_existing_list[i]).find('span[class*=slider-time]').eq(0).text();
                let timeSpanSecond = $(sun_existing_list[i]).find('span[class*=slider-time]').eq(1).text();

                startTime = timeSpanFirst.split(":").shift();
                startMins = timeSpanFirst.split(":").pop();

                EndTime = timeSpanSecond.split(":").shift();
                EndMins = timeSpanSecond.split(":").pop();

                let Stime = "";
                let Ftime = "";
                timeObj['id'] = i;
                timeObj['start'] = +(Stime.concat(startTime, startMins));
                timeObj['finish'] = +(Ftime.concat(EndTime, EndMins));
                timeObj['element'] = sun_existing_list[i];
                sun_time_list.push(timeObj);
            }

            if (sun_time_list.length > 1) {

                // loop through time list - i < mon_time_list.length
                for (let i = 0; i < sun_time_list.length; i++) {

                    // loop through time list and compare each item aginst all other items - i < mon_time_list.length
                    for (let j=0; j < sun_time_list.length; j++){

                        // only compare items that aren't duplicates
                        if(sun_time_list[i]['id'] !== sun_time_list[j]['id']) {

                            // does j start before i finish and does j finish before i start
                            if(sun_time_list[j]['start'] < sun_time_list[i]['finish'] && sun_time_list[j]['finish'] > sun_time_list[i]['start']) {
                                $(document).css({'cursor': 'default'});
                                $("#submitBtn").prop("disabled", false);
                                $(sun_time_list[j]['element']).append('<p class="errorOverlap">Zorg ervoor dat beschikbaarheden niet overlappen.</p>');
                                $('.errorlabel').empty();
                                return;
                            }
                        }
                    }
                }
            }

            var month= $("#month").val();
            var wkday = "";

            $(".wkday").each(function(index, obj){
            if($(obj).is(":checked"))
            wkday += $(obj).val() + ",";
            });
            wkday = wkday.substr(0, wkday.length-1);
            /*
            if(stime > etime){
            alert("Wrong Time !");
            $(document.body).css({'cursor' : 'default'});
            $("#submitBtn").prop("disabled", false);
            return;
            }
            */
            var d           = new Date();
            var currentDate = d.getFullYear()+"-"+(d.getMonth()+1)+"-"+d.getDate();

            let mon_stime = $('.slideTable-mon div[class*=time-range]:first span[class*=slider-time]').eq(0).html();
            let mon_etime = $('.slideTable-mon div[class*=time-range]:first span[class*=slider-time]').eq(1).html();

            let tue_stime = $('.slideTable-din div[class*=time-range]:first span[class*=slider-time]').eq(0).html();
            let tue_etime = $('.slideTable-din div[class*=time-range]:first span[class*=slider-time]').eq(1).html();

            let wed_stime = $('.slideTable-woe div[class*=time-range]:first span[class*=slider-time]').eq(0).html();
            let wed_etime = $('.slideTable-woe div[class*=time-range]:first span[class*=slider-time]').eq(1).html();

            let thur_stime = $('.slideTable-don div[class*=time-range]:first span[class*=slider-time]').eq(0).html();
            let thur_etime = $('.slideTable-don div[class*=time-range]:first span[class*=slider-time]').eq(1).html();

            let fri_stime = $('.slideTable-vri div[class*=time-range]:first span[class*=slider-time]').eq(0).html();
            let fri_etime = $('.slideTable-vri div[class*=time-range]:first span[class*=slider-time]').eq(1).html();

            let sat_stime = $('.slideTable-zat div[class*=time-range]:first span[class*=slider-time]').eq(0).html();
            let sat_etime = $('.slideTable-zat div[class*=time-range]:first span[class*=slider-time]').eq(1).html();

            let sun_stime = $('.slideTable-zon div[class*=time-range]:first span[class*=slider-time]').eq(0).html();
            let sun_etime = $('.slideTable-zon div[class*=time-range]:first span[class*=slider-time]').eq(1).html();

            let postObj = {
                subpage:"slotCreation",
                mon_stime:mon_stime,
                tue_stime:tue_stime,
                wed_stime:wed_stime,
                thur_stime:thur_stime,
                fri_stime:fri_stime,
                sat_stime:sat_stime,
                sun_stime:sun_stime,
                mon_etime:mon_etime,
                tue_etime:tue_etime,
                wed_etime:wed_etime,
                thur_etime:thur_etime,
                fri_etime:fri_etime,
                sat_etime:sat_etime,
                sun_etime:sun_etime,
                date:currentDate,
                month:month,
                mon_add:monday_additional,
                tue_add:tuesday_additional,
                wed_add:wednesday_additional,
                thu_add:thursday_additional,
                fri_add:friday_additional,
                sat_add:saturday_additional,
                sun_add:sunday_additional
            };

            console.log('postObj');
            console.log(postObj);

            $.ajax({
                type: "POST",
                url: "teacher-ajaxcalls.php",
                data: postObj,
                success: function(response) {

                    if(response.trim() == "success"){
                        showNotification("alert-success", "Je beschikbaar is aangepast", "bottom", "center", "", "");
                        $('.errorlabel').text("");
                    }
                    else if(response.includes("update")){
                        showNotification("alert-success", "Je beschikbaar is aangepast", "bottom", "center", "", "");
                        $('.errorlabel').text("");
                    }
                    else {
                        showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                    }

                    $(document.body).css({'cursor' : 'default'});
                    $("#submitBtn").prop("disabled", false);
                },
                error: function(xhr, ajaxOptions, thrownError) {

                    showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");

                    $(document.body).css({'cursor' : 'default'});
                    $("#submitBtn").prop("disabled", false);
                }
            });
        });
    });

    // DEPRECATED?
    function parseTime(s) {
        var part = s.match(/(\d+):(\d+)(?: )?(am|pm)?/i);
        var hh = parseInt(part[1], 10);
        var mm = parseInt(part[2], 10);
        var ap = part[3] ? part[3].toUpperCase() : null;

        if (ap === "AM") {
            if (hh == 12) {
                hh = 0;
            }
        }
        if (ap === "PM") {
            if (hh != 12) {
                hh += 12;
            }
        }
        return { hh: hh, mm: mm };
    }

    function setTime(selectorName, starttime, endtime){
        $(selectorName).slider({
            range: true,
            min: 360,
            max: 1425,
            step: 15,
            values: [starttime, endtime],
            change: function( event, ui ) {
                $('.errorOverlap').remove();
            },
            slide: function (e, ui) {
                //mon2
                var _day =  $(this).attr("day");
                if(ui.values[1]-ui.values[0] < 45){
                    $("#"+_day+"-error").text("Een bijles duurt minimaal 45 minuten, waardoor deze beschikbaarheid niet meetelt.");
                    $("#"+_day+"_stime").val("");
                    $("#"+_day+"_etime").val("");
                    return;
                }else{
                    let dutchWeek = {
                    "mon":  "mon",
                    "tue":  "din",
                    "wed":  "woe",
                    "thur":  "don",
                    "fri":  "vri",
                    "sat":  "zat",
                    "sun":  "zon"
                    };

                    $('#'+_day.replace(/[0-9]/g, '')+'-area-error, #'+dutchWeek[_day.replace(/[0-9]/g, '')]+'-area-error').text("");
                    $('#'+_day+'-error').text('Klik op Opslaan om je veranderingen op te slaan.');
                }
                var hours1 = Math.floor(ui.values[0] / 60);
                var minutes1 = ui.values[0] - (hours1 * 60);
                if (hours1.length == 1) hours1 = '0' + hours1;
                if (minutes1.length == 1) minutes1 = '0' + minutes1;
                if (minutes1 == 0) minutes1 = '00';
                /*if (hours1 >= 12) {
                if (hours1 == 12) {
                hours1 = hours1;
                minutes1 = minutes1 + " PM";
                } else {
                hours1 = hours1 - 12;
                minutes1 = minutes1 + " PM";
                }
                } else {
                hours1 = hours1;
                minutes1 = minutes1 + " AM";
                }
                if (hours1 == 0) {
                hours1 = 12;
                minutes1 = minutes1;
                }
                */
                $('.'+_day+'-slider-time').html(hours1 + ':' + minutes1);
                $("#"+_day+"_stime").val(hours1 + ':' + minutes1);
                var hours2 = Math.floor(ui.values[1] / 60);
                var minutes2 = ui.values[1] - (hours2 * 60);
                if (hours2.length == 1) hours2 = '0' + hours2;
                if (minutes2.length == 1) minutes2 = '0' + minutes2;
                if (minutes2 == 0) minutes2 = '00';
                /*if (hours2 >= 12) {
                if (hours2 == 12) {
                hours2 = hours2;
                minutes2 = minutes2 + " PM";
                } else if (hours2 == 24) {
                hours2 = 11;
                minutes2 = "59 PM";
                } else {
                hours2 = hours2 - 12;
                minutes2 = minutes2 + " PM";
                }
                } else {
                hours2 = hours2;
                minutes2 = minutes2 + " AM";
                }
                */
                $('.'+_day+'-slider-time2').html(hours2 + ':' + minutes2);
                $("#"+_day+"_etime").val(hours2 + ':' + minutes2);
            }
        });
    }//setTime() ends

    $(document).ready(function(){
        setTime(".slider-range[day=mon]", <?php echo isset($mon_time[0]) ? getmins($mon_time[0]) : 0; ?>, <?php echo isset($mon_time[1]) ? getmins($mon_time[1]) : 0; ?>);
        setTime(".slider-range[day=tue]", <?php echo (isset($tue_time[0]) ?getmins($tue_time[0]) : 0); ?>, <?php echo isset($tue_time[1]) ? getmins($tue_time[1]) : 0; ?>);
        setTime(".slider-range[day=wed]", <?php echo (isset($wed_time[0]) ?getmins($wed_time[0]) : 0); ?>, <?php echo isset($wed_time[1]) ? getmins($wed_time[1]) : 0; ?>);
        setTime(".slider-range[day=thur]", <?php echo (isset($thur_time[0]) ?getmins($thur_time[0]) : 0); ?>, <?php echo isset($thur_time[1]) ? getmins($thur_time[1]) : 0; ?>);
        setTime(".slider-range[day=fri]", <?php echo (isset($fri_time[0]) ?getmins($fri_time[0]) : 0); ?>, <?php echo isset($fri_time[1]) ? getmins($fri_time[1]) : 0; ?>);
        setTime(".slider-range[day=sat]", <?php echo (isset($sat_time[0]) ?getmins($sat_time[0]) : 0); ?>, <?php echo isset($sat_time[1]) ? getmins($sat_time[1]) : 0; ?>);
        setTime(".slider-range[day=sun]", <?php echo (isset($sun_time[0]) ?getmins($sun_time[0]) : 0); ?>, <?php echo isset($sun_time[1]) ? getmins($sun_time[1]) : 0; ?>);
    });
</script>

</body>
</html>
<div id="fullCalModal" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="margin-top: 0px !important;
padding-top: 0px !important;">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal-title"></h4>
            </div>
            <div id="modalBody" class="modal-body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<?php
    }
?>