<?php

use API\Endpoint\Calendar;

require_once('includes/connection.php');
require_once('../../api/endpoint/Calendar.php');

$thisPage = "Appointment";
session_start();
if (!isset($_SESSION['Teacher'])) {
	header('Location: index.php');
} else {
	$query4 = "DELETE FROM `calendarbooking` WHERE `datee` <= CURDATE() AND `teacherID` = " . $_SESSION['TeacherID'] . " AND `accepted` = 0";
	mysqli_query($con, $query4);

	$cal = new Calendar($db, ['teacherID' => $_SESSION['TeacherID']]);
	$result = $cal->GetAppointments(['history' => 1]);
	$outputArray = $result->getResult();

	$str = "";
	$str2 = "";
	$str3 = "";

	$c1 = 1;
	$c2 = 1;
    $c3 = 1;
    

	foreach ($outputArray as $key => $value) {
        $pending = isset($value['pending']) ? $value['pending'] : null;
        $accepted = isset($value['accepted']) ? $value['accepted'] : null;
        $history = isset($value['history']) ? $value['history'] : null;
		if ($pending) {
			foreach ($pending as $key2 => $value2) {
                $userID = isset($value2['userID']) ? $value2['userID'] : null;
                $forename = isset($value2['forename']) ? $value2['forename'] : null;
                $begin = isset($value2['begin']) ? $value2['begin'] : null;
                $end = isset($value2['end']) ? $value2['end'] : null;
                $telephone = isset($value2['telephone']) ? $value2['telephone'] : null;
                $email = isset($value2['email']) ? $value2['email'] : null;
                $address = isset($value2['address']) ? $value2['address'] : null;
                $value_id = isset($value2['id']) ? $value2['id'] : null;
				$deletable = isset($value2['deletable']) ? $value2['deletable'] : null;
				$str .= "
                        <tr>
                                <td class='hide-on-mobile'>$c1</td>
                                <td><a href='student-profile?sid=" . $userID . "'>" . $forename . "</a></td>
                                <td>" . $key . " " . $begin . " - " . $end . "</td>
                                <td>" . $telephone . "</td>
                                <td>" . $email . "</td>
                                <td>" . $address . "</td>
                                <td>
                                    <button type='button' class='action-button inline-elem-button btn btn-raised btn-primary btn-round waves-effect' value='accept' onclick='acceptPending(this, " . $value2['id'] . ")' >VERZOEK ACCEPTEREN</button>
                                    <button type='button' class='action-button inline-elem-button btn btn-raised btn-warning btn-round waves-effect' value='cancel' onclick='cancelSlot($(this), " . $value2['id'] . ",1)' >VERZOEK WEIGEREN</button>
                                </td>
                        </tr>
                        ";
				$c1++;
			}
		}

		if ($accepted) {
			foreach ($accepted as $key2 => $value2) {
                $userID = isset($value2['userID']) ? $value2['userID'] : null;
                $forename = isset($value2['forename']) ? $value2['forename'] : null;
                $begin = isset($value2['begin']) ? $value2['begin'] : null;
                $end = isset($value2['end']) ? $value2['end'] : null;
                $telephone = isset($value2['telephone']) ? $value2['telephone'] : null;
                $email = isset($value2['email']) ? $value2['email'] : null;
                $address = isset($value2['address']) ? $value2['address'] : null;
                $value_id = isset($value2['id']) ? $value2['id'] : null;
				$deletable = isset($value2['deletable']) ? $value2['deletable'] : null;
				$str2 .= "
                                <tr>
                                        <td class='hide-on-mobile'>$c2</td>
                                        <td><a href='student-profile?sid=" . $userID . "'>" . $forename . "</a></td>
                                        <td>" . $key . " " . $begin . " - " . $end . "</td>
                                        <td>" . $telephone . "</td>
                                        <td>" . $email . "</td>
                                        <td>" . $address . "</td>
                                        <td>
                                            <button type='button' class='action-button inline-elem-button btn btn-raised btn-warning btn-round waves-effect' value='cancel' ";
				if ($deletable) $str2 .= "onclick='cancelSlot($(this), " . $value_id . ",3)'";
				else $str2 .= "disabled";
				$str2 .= ">AFSPRAAK ANNULEREN</button>
                                        </td>
                                </tr>
                                ";
				$c2++;
			}
		}

		if ($history) {
			foreach ($history as $item) {
                $userID = isset($item['userID']) ? $item['userID'] : null;
                $forename = isset($item['forename']) ? $item['forename'] : null;
                $begin = isset($item['begin']) ? $item['begin'] : null;
                $end = isset($item['end']) ? $item['end'] : null;
                $telephone = isset($item['telephone']) ? $item['telephone'] : null;
                $email = isset($item['email']) ? $item['email'] : null;
                $address = isset($item['address']) ? $item['address'] : null;
                $value_id = isset($item['id']) ? $item['id'] : null;
				$deletable = isset($value2['deletable']) ? $value2['deletable'] : null;
				$str3 .= "<tr>
                    <td class='hide-on-mobile'>$c3</td>
                    <td><a href='student-profile?sid={$userID}'>{$forename}</a></td>
                    <td>{$key} {$begin} - {$end}</td>
                    <td>{$telephone}</td>
                    <td>{$email}</td>
                    <td>{$address}</td>
                    <td>
                        <button type=\"button\" class=\"action-button inline-elem-button btn btn-raised btn-warning btn-round waves-effect\" value=\"cancel\" ";
				if ($deletable) $str3 .= "onclick=\"cancelSlot($(this), {$value_id},2)\"";
				else $str3 .= "disabled";
				$str3 .= ">AFSPRAAK VERWIJDEREN</button>";
				$str3 .= "</td></tr>";
				$c3++;
			}
		}
	}
	?>

    <!doctype html>
    <html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description"
              content="Een overzicht van alle geboekte bijlessen per maand | Verdien meer dan bij onze grootste concurrenten | Bepaal zelf aan wie, wanneer en waar je bijles geeft.">
        <title>Afspraken | Bijles Aan Huis</title>
		<?php
		require_once('includes/connection.php');
		require_once('includes/mainCSSFiles.php');
		?>
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
              rel="stylesheet"/>
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet'/>
        <link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print'/>
        <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">

        <!-- New  Style -->
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <!--        <script src="assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>-->
        <!--        <script src="assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>-->
        <!--        <script src="assets/js/pages/tables/jquery-datatable.js"></script>-->
        <!--        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>-->

        <!-- new script --><!-- Jquery DataTable Plugin Js -->
        <script src="assets/bundles/datatablescripts.bundle.js"></script>
        <script src="assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
        <script src="assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
        <script src="assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
        <script src="assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
        <script src="assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">

        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


        <!-- New  Style -->

        <style type="text/css">

            .card.calendar-card {
                background-color: #FFFFFF;
                border: 2px solid #f1f1f1;
                border-radius: 6px;
            }

            .card.calendar-card .header {
                background-color: #F1f1f1;
                padding: 20px 40px;

            }

            .card.calendar-card .header .ma-main-title {
                font-family: 'Poppins', sans-serif !important;
                font-weight: 600;
                font-size: 21px !important;
            }

            .card.calendar-card .body {
                padding: 0;
            }

            .card.calendar-card .tab-content {
                padding: 20px 40px;
            }

            .card.calendar-card .tab-content .card .body .col-sm-12 {
                overflow-x: auto;
            }

            .card.calendar-card .tab-content .active {
                background: transparent !important;
                /* padding:20px 40px; */
            }

            .card.calendar-card .nav-tabs {
                padding: 20px 40px;
            }

            .card.calendar-card .ma-main-title::before {
                content: none;
            }


            .card.calendar-card .card {
                background-color: #FFFFFF;
            }

            .card.calendar-card .card table {
                background-color: #F1F1F1;
            }

            .theme-green .action-button {
                width: 180px !important;
                margin-bottom: 10px !important;
            }

            .theme-green .action-button.btn-primary {
                color: #FFFFFF !important;
                background-color: #5CC75F !important;
                font-weight: normal !important;
            }

            .theme-green .action-button.btn-primary:hover {
                /* background: transparent !important;
                color: #5CC75F !important;
                border: 2px solid #5CC75F !important; */
                box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.17);
            }

            .theme-green .action-button.btn-warning {
                color: #FFFFFF !important;
                background-color: #FFBD6B !important;
                font-weight: normal !important;
            }

            .theme-green .action-button.btn-warning:hover {
                /* background: transparent !important;
                color: #FFBD6B  !important;
                border: 2px solid #FFBD6B !important; */
                box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.17);
            }

            /*Placeholder Color */
            .modal-dialog {
                /*margin-top: 70px;*/

            }

            input {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            select {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            input:focus {
                background: transparent !important;
            }

            select:focus {
                background: transparent !important;
            }

            .wizard .content {
                /*overflow-y: hidden !important;*/
            }

            .wizard .content label {

                color: white !important;

            }

            .wizard > .steps .current a {
                background-color: #029898 !important;
            }

            .wizard > .steps .done a {
                background-color: #828f9380 !important;
            }

            .wizard > .actions a {
                background-color: #029898 !important;
            }

            .wizard > .actions .disabled a {
                background-color: #eee !important;
            }

            .btn.btn-simple {
                border-color: white !important;
            }

            .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
                color: white;
            }

            table {
                color: white;
            }

            .multiselect.dropdown-toggle.btn.btn-default {
                display: none !important;
            }

            div.show .dropdown-menu {
                display: block;
            }


            .theme-green .nav-tabs .nav-link.active {
                color: white !important;
                background-color: #5cc75f !important;
                border: 2px solid #5cc75f !important;
            }

            .fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
                color: black !important;
            }


            .nav-tabs {
                border: 0;
                padding: 15px .7rem;
                border-bottom: 1px solid #486066 !important;


            }

            button.btn.btn-simple {
                border-color: #486066 !important;
            }

            .theme-green .page-loader-wrapper {
                background: #FAFBFC;
            }

            span.caret {
                display: none;
            }

            div.dataTables_length {
                margin-bottom: 15px;
            }


            select.form-control.form-control-sm {
                color: #486066 !important;
                padding: 0;
            }

            .btn, .theme-green .btn-primary, .pagination .page-item .page-link {
                border-radius: 6px !important;
            }

            .btn-warning {
                color: #FFBD6B !important;
                border: 2px solid #FFBD6B !important;
            }

            .btn-warning:hover {
                background-color: #FFBD6B !important;
            }

            td.dataTables_empty {
                color: transparent !important;
            }

            .custom-padding {
                padding: 0 0 1rem 0 !important;
                margin-top: 1rem;
            }

            /* extra popup modal styling */
            .modal h6 {
                text-transform: lowercase;
            }

            .modal .modal-title {
                font-size: 18px !important;
                font-weight: bold !important;
            }

            .modal .modal-text {
                padding: 0 20px 27px 20px;
            }

            .modal-content .modal-header button {
                right: 12px !important;
                top: 6px !important;
            }

            .row:before, .row:after {
                display: none !important;
            }

            .tab-content .dataTables_wrapper div.row .dataTables_filter input {
                height: 29px;
            }

            @media screen and (max-width: 870px) {
                .tab-content .dataTables_wrapper div.row .dataTables_filter input {
                    max-width: 80%;
                }

                .tab-content .dataTables_wrapper div.row .col-6, .tab-content .dataTables_wrapper div.row .col-sm-12.col-md-6 {
                    max-width: 50%;
                }
            }

            @media screen and (max-width: 768px) {
                .hide-on-mobile {
                    display: none !important;
                }

                .tab-content .dataTables_wrapper div.row .dataTables_filter input {
                    max-width: 60%;
                }

                .card .body .nav-tabs {
                    padding-left: 10px;
                    padding-right: 10px;
                }

                .card .body .nav-tabs .nav-item {
                    width: 33.333%;
                }

                .nav-tabs > .nav-item > .nav-link {
                    padding: 11px 7px;
                }
            }

            @media screen and (max-width: 576px) {
                .card .body .nav-tabs {
                    padding-left: 3px;
                    padding-right: 0px;
                }

                .card .body .nav-tabs .nav-item a {
                    letter-spacing: -.7px;
                }
            }

            @media screen and (max-width: 480px) {
                .card .body .nav-tabs {
                    padding-left: 15px;
                    padding-right: 15px;
                }

                .card .body .nav-tabs .nav-item {
                    width: 90%;
                    margin-left: 5%;
                }
            }

            .modal__content {
                top: 25% !important;
                width: 60% !important;
            }

            .modal__body {
                padding: 5% !important;
            }

            .modal-content {
                width: 100%;
            }

            .modal-dialog-centered {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                min-height: calc(100% - (.5rem * 2));
            }

            @media (min-width: 576px) {
                .modal-dialog-centered {
                    min-height: calc(100% - (1.75rem * 2));
                }
            }

            .modal .modal-text {
                padding: 0 20px 27px 20px;
                font-family: 'Poppins', Helvetica, Arial, Lucida, sans-serif !important;
                font-size: 16px !important;
            }
        </style>

		<?php
		$activePage = basename($_SERVER['PHP_SELF']);
		?>
    </head>
    <body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper" style="background: #FAFBFC !important;">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                     alt="Oreo"></div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>


	<?php
	require_once('includes/header.php');
	?>
    <style>
        @media screen and (min-width:576px) {
            section.content {
                padding-right:4%;
                padding-left:4%;
            }
        }
    </style>
    <!-- Main Content -->
    <section class="content page-calendar">


        <div class="container ma-sidebar-container" style="margin-top:0 !important;">
            <div class="row clearfix">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="card calendar-card">
                        <div class="header">
                            <h2 class="ma-main-title">Afspraken</h2>
                        </div>
                        <div class="body" style="padding:0">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="nav-item"><a class="nav-link  btn-primary active" data-toggle="tab"
                                                        href="#pending" style="white-space: nowrap;">IN AFWACHTING</a>
                                </li>
                                <li class="nav-item"><a class="nav-link  btn-primary " data-toggle="tab"
                                                        href="#confirm">GEACCEPTEERD</a></li>
                                <li class="nav-item"><a class="nav-link  btn-primary" data-toggle="tab"
                                                        href="#complete">GEWEEST</a></li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane in active" id="pending">
                                    <!-- Exportable Table -->
                                    <div class="row clearfix">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="body">
                                                    <table class="table table-bordered table-striped js-basic-example table-hover dataTable">
                                                        <thead>
                                                        <tr>
                                                            <th class="hide-on-mobile">#</th>
                                                            <th>Naam student</th>
                                                            <th>Datum en tijd</th>
                                                            <th>Telefoonnummer</th>
                                                            <th>E-mailadres</th>
                                                            <th>Adres</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
														<?php echo $str; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- #END# Exportable Table -->
                                </div>
                                <div role="tabpanel" class="tab-pane" id="confirm">
                                    <!-- Exportable Table -->
                                    <div class="row clearfix">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="body">
                                                    <table class="table table-bordered table-striped js-basic-example table-hover dataTable">
                                                        <thead>
                                                        <tr>
                                                            <th class="hide-on-mobile">#</th>
                                                            <th>Naam student</th>
                                                            <th>Datum en tijd</th>
                                                            <th>Telefoonnummer</th>
                                                            <th>E-mailadres</th>
                                                            <th>Adres</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
														<?php echo $str2; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- #END# Exportable Table -->
                                </div>

                                <div role="tabpanel" class="tab-pane" id="complete">
                                    <!-- Exportable Table -->
                                    <div class="row clearfix">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="body">
                                                    <table class="table table-bordered table-striped js-basic-example table-hover dataTable">
                                                        <thead>
                                                        <tr>
                                                            <th class="hide-on-mobile">#</th>
                                                            <th>Naam student</th>
                                                            <th>Datum en tijd</th>
                                                            <th>Telefoonnummer</th>
                                                            <th>E-mailadres</th>
                                                            <th>Adres</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
														<?php echo $str3; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- #END# Exportable Table -->
                                </div>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


	<?php
	require_once('includes/footerScriptsAddForms.php');
	?>
    <!-- <script src='assets/plugins/fullcalendar/lib/moment.min.js'></script> -->
    <!-- <script src='assets/plugins/fullcalendar/fullcalendar.min.js'></script> -->
    <!-- <script src='assets/plugins/fullcalendar/lib/moment.min.js'></script> -->
    <!--<script src="assets/js/pages/calendar/calendar.js"></script>-->
    <!-- <script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script> -->

    <script>


    </script>

    <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
    <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
    <script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
    <script type="text/javascript">


		var _obj = null;
		var _id = null;
		var _type = null;
		var _name = null;
		var _notificationMsg = "";
		var _timer = 1000;

		$(document).ready(function () {
			// $(".inline-elem-button").click(function(){
			//     var btn = $(this).val();
			//     alert("Ajax call for "+ btn);
			// });


			$("#delete-yes-type").click(function () {
				// cancelSlott(_obj, _id);
				cancel(_obj, _id);
				$("#modal-delete-type").modal('hide');
			});

			$("#delete-no-type").click(function () {
				$("#modal-delete-type").modal('hide');
				_obj = null;
				_id = null;
			});


			setTimeout(function () {
				var temporaryHTML = $('.tab-content .dataTables_wrapper div.row .dataTables_length label').html().replace(' weergeven', '<span class="hide-on-mobile"> weergeven</span>');
				$('.tab-content .dataTables_wrapper div.row .dataTables_length label').html(temporaryHTML);
				$('.tab-content .dataTables_wrapper .dataTables_filter, .tab-content .dataTables_wrapper .dataTables_length').parent().addClass('col-6');
				$('.tab-content .dataTables_wrapper .dataTables_filter, .tab-content .dataTables_wrapper .dataTables_length').parent().removeClass('col-sm-12 col-md-6');
			}, 1000);
		});

		function cancel(obj, id) {
			var url = "/api/Calendar/CancelTutor";
			$.ajax({
				type: "POST",
				url: url,
				data: {id: id},
				dataType: "json",
				success: function (response) {
					if (_notificationMsg || _name == '3') {
						showNotification("alert-success", _notificationMsg, "bottom", "center", "", "", _timer);
					} else {
						$('#modal-information-type .modal-title').text('Je hebt het bijlesverzoek afgewezen!');
						$('#modal-information-type .modal-text').html("De leerling wordt hierover door ons standaard per e-mail geïnformeerd, of persoonlijk per telefoon indien het een nieuwe leerling betreft. Hierdoor hoef jij de leerling dus niet meer te informeren over deze afwijzing. Indien je helemaal niet meer beschikbaar bent voor leerlingen, geef dat dan aan op de <a href='/app/edit-profile' style='color:#5CC75F !important;'>Profiel aanpassen</a> pagina.");
						$('#modal-information-type').appendTo("body").modal('show');
					}
					obj.closest('tr').remove();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
				}
			});// ends ajax
		}

		function acceptPending(obj, id) {
			var url = "/api/Calendar/AcceptTutor"
			$.ajax({
				type: "POST",
				url: url,
				data: {id: id},
				dataType: "json",
				success: function (response) {
					showNotification("alert-success", "De bijles is bevestigd", "bottom", "center", "", "", 1000);
					document.location.reload();
				},
				error: function (xhr, ajaxOptions, thrownError) {
					showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
				}
			});// ends ajax
		}

		function completeSlot(obj, id) {
			var data_opt;

			data_opt = {subpage: "classTaken", cbID: id};

			$.ajax({
				type: "POST",
				url: "teacher-ajaxcalls.php",
				data: data_opt,
				dataType: "json",
				success: function (response) {
					if (response.trim() == "success") {

						showNotification("alert-success", "Class has been taken", "bottom", "center", "", "");


						//insert into confirm table
						$("#confirm tbody").prepend("<tr>" + $(obj).parent().parent().html() + "</tr>");
						$("#confirm tbody tr:first-child").find(".inline-elem-button:first-child").remove();
						//remove tr
						$(obj).parent().parent().remove();


						$("#confirm tbody").find(".dataTables_empty").parent().remove();

						//recounting
						$("#pending tbody tr td:first-child").each(function (index, obj) {
							$(this).html(index + 1);
						});
						$("#confirm tbody tr td:first-child").each(function (index, obj) {
							$(this).html(index + 1);
						});

						//recounting
						$("#pending tbody tr td:first-child").each(function (index, obj) {
							$(this).html(index + 1);
						});
						$("#confirm tbody tr td:first-child").each(function (index, obj) {
							$(this).html(index + 1);
						});

						//setTimeout(function () {
						//    document.location.reload();
						//}, 4000);
					} else {
						showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
					}
					$(document.body).css({'cursor': 'default'});
					$("#submitBtn").prop("disabled", false);
				},
				error: function (xhr, ajaxOptions, thrownError) {
					if ((xhr.responseText).trim() == "success") {

						showNotification("alert-success", "Class has been taken", "bottom", "center", "", "");


						//insert into confirm table
						$("#confirm tbody").prepend("<tr>" + $(obj).parent().parent().html() + "</tr>");
						$("#confirm tbody tr:first-child").find(".inline-elem-button:first-child").remove();
						//remove tr
						$(obj).parent().parent().remove();

						$("#confirm tbody").find(".dataTables_empty").parent().remove();

						//recounting
						$("#pending tbody tr td:first-child").each(function (index, obj) {
							$(this).html(index + 1);
						});
						$("#confirm tbody tr td:first-child").each(function (index, obj) {
							$(this).html(index + 1);
						});

						//recounting
						$("#pending tbody tr td:first-child").each(function (index, obj) {
							$(this).html(index + 1);
						});
						$("#confirm tbody tr td:first-child").each(function (index, obj) {
							$(this).html(index + 1);
						});

						//setTimeout(function () {
						//    document.location.reload();
						//}, 4000);
					} else {
						showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
					}
					$(document.body).css({'cursor': 'default'});
					$("#submitBtn").prop("disabled", false);

				}
			});// ends ajax
		}

		function cancelSlot(obj, id, name) {
			_id = id;
			_obj = obj;
			_name = name;
			let modelLabel = "";

			if (name == 2) {
				modelLabel = "Weet je zeker dat je de bijles wilt verwijderen?";
				_notificationMsg = "De bijles is verwijderd";
				_timer = 3000;
			} else if (name == 3) { // cancel accepted appointment
                _notificationMsg = "De bijles is geannuleerd";
				modelLabel = "Weet je zeker dat je de bijles wilt annuleren?";
				_notficationMsg = "Afspraak is geannuleerd";
				_timer = 1000;
			} else { // cancel pending appointment
				modelLabel = "Het bijlesverzoek is geweigerd";
				_timer = 1000;
			}

			$("#modal-delete-type .modal__title").html(modelLabel);
			$('#modal-delete-type').appendTo("body").modal('show');
		}

    </script>

    </body>
    </html>

    <div class="modal" id="modal-delete-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal__content">
                <div class="modal__header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h6 class="modal__title" id="myModalLabel">Weet je zeker dat je de afspraak wilt verwijderen?</h6>
                </div>
                <div class="modal__body">
                    <div id="deresulttype"></div>
                    <form id="delete-form-type" method="post" action="">
                        <input type="hidden" id="delete-id-type" name="delete-id-type"/>
                        <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                        <button type="button" class="btn btn-success" id="delete-yes-type">Ja</button>
                        <button type="button" class="btn btn-danger" id="delete-no-type">Nee</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="modal-information-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal__content">
                <div class="modal__header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h6 class="modal-title modal__title" id="myModalLabel"></h6>
                </div>
                <div class="modal__body">
                    <div id="deresulttype"></div>
                    <form id="delete-form-type" method="post" action="">
                        <input type="hidden" id="delete-id-type" name="delete-id-type"/>
                        <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                        <h6 class="modal-text modal__text--appointment-cancel"></h6>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="fullCalModal" class="modal">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal__content">
                <div class="modal__header">
                    <button type="button" class="inline-elem-button close" data-dismiss="modal"><span
                                aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                    <h4 id="modalTitle" class="modal-title modal__title"></h4>
                </div>
                <div id="modalBody" class="modal__body"></div>
                <button type="button" class="inline-elem-button btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
	<?php
}
