<?php

require_once('includes/connection.php');
$thisPage="Student Profile";
session_start();

if(!isset($_SESSION['Teacher']) && !isset($_SESSION['Student']) && !isset($_SESSION['AdminUser']))
{
	header('Location: index.php');
} 
else
{
      if(!isset($_GET['sid']))
         header('Location: index.php');
?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

	<title>Leerlingen | Bijles Aan Huis</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
 <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />		
    <link rel="stylesheet" href="calendar/css/calendar.css">
    <link rel="stylesheet" href="assets/css/nigel.css">

    <style type="text/css">
           .custom__btn.action-button{

color: #FFFFFF !important;
background-color: #5CC75F !important;
font-weight:normal !important;
border-radius: 6px !important;
}

.custom__btn.action-button:hover{
box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.17);
}

.card.profile-header{
background-color: #FFFFFF;
border: 2px solid #f1f1f1;
border-radius: 6px;
}

.card.profile-header .header{
background-color: #F1f1f1;
padding: 20px 40px !important;


}
.card.profile-header .header h2{
padding-bottom: 0 !important;
}
.card.profile-header .header h2::before{
content: none;
}
.card.profile-header .body{
border: none !important;
}
.card.calendar-card{
border: 2px solid #F1F1F1;
border-top-left-radius: 6px;
border-top-right-radius: 6px;
}

.card.calendar-card .header{
padding: 20px 50px 20px !important;
}


    #calendar { background-color:#fafafa !important; }
    .cal-month-day{color:black;}
    .cal-row-head {background-color: #fafafa !important;}
    .empty {  background-color: white !important; border: 2px solid black;}    
    .inline_time { color: #50d38a !important; }  
    .inline_time_disabled { color: #6b7971 !important; }
    #cal-slide-content{font-family:inherit; color:#bdbdbd;}
    .page-header{height: 0 !important;padding-bottom: 0px;margin: 0 0 20px;border-bottom: 0px;}
    .page-calendar #calendar{max-width: 100%;}
		
	.mapboxgl-ctrl-attrib, .mapboxgl-ctrl-logo{display:none !important;}
		
.input-group{
    background-color: transparent !important;
}
.input-group-focus span{background-color: transparent !important;}
.input-group-focus #chatsender{     
    border:1px solid #ff5200c2 !important;
}
		.mapboxgl-ctrl-geocoder
		{
		   display:none !important;
		}
.card .body {
    background-color: #FAFBFC !important;
}		
	.body-radius
	{
		border-bottom-left-radius: 6px !important;
		border-bottom-right-radius: 6px !important;	
    }

    html { scroll-behavior: smooth; } 
    
    .header {
        padding: 15px 20px 5px 5px !important;
    }

    .custom__btn {
        margin-top: 2rem;
		color: #FFFFFF !important;
		border: 0.125rem solid #5CC75F;
		border-radius: 1.5rem;
		font-family: 'Poppins', Helvetica, Arial, Lucida, sans-serif !important;
		font-weight: 600;
		text-transform: uppercase;
		background-color: #5CC75F;
		width: 100%;
        transition: 0.3s;
        padding: .5rem 1rem;
	}
	.custom__btn:hover {
		/* color: #5CC75F !important; */
		background-image: initial;
		background-color: #FFFFFF;
    }
    
    .size__color {
        font-size: 1.3125rem !important;
        color: #5CC75F !important;
    }   

    @media screen and (max-width: 760px) {
        .padding {
            padding-top: 2rem;
        }
    }
    .custom__btn.action-button {
        white-space: nowrap;
        width: auto;
    }

    </style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green student-profile-calendar">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
			
?>

<style>
        .dashboard-calendar #calendar {
            padding:0;
            width:100% !important;
            margin: 10px auto;
        }
        .appointment-block h3.appointment-block__title {
            margin-top: 0;
        }
        .appointment-block p.appointment-block__text {
            font-size: 14px;
            margin: 0;
        }
        .appointment-block p.appointment-block__top-right {
            margin-bottom: 10px;
        }
        .appointment-block p.appointment-block__text:last-child {
            padding-bottom: 0;
        }
        #calender_startTime, #calender_endTime {
            color: #486066 !important;
            border-color: black !important;
        }
        #cal-slide-content ul.unstyled.list-unstyled {
            padding-bottom: 0;
        }
        .fa, .far, .fas {
            margin-right: 10px !important;
        }
        @media screen and (max-width:576px) {
            .appointment-block__startTime {
                margin: 0 !important;
            }
            .profile-detail-calendar {
                margin: 10px -3%;
            }
        }

        .student-profile-header {
            overflow: hidden;
            background-color: #f1f1f1;
            padding: 20px;
            height: auto !important;
        }
        .ma-subtitle {
            font-size: 21px !important;
            color: #5cc75f !important;
            font-family: 'Poppins', sans-serif !important;
            font-weight: 600;
            font-style: normal;
            margin-top: 0;
            margin-bottom: 0;
        }
.student-profile-header__heading {
    padding: 0;
}
.student-profile-content__section {
    border-radius: 6px;
    border: solid 2px 
    #f1f1f1;
    margin-bottom: 30px;
}
.edit-profile-form, .dashboard-content-container, .student-profile-content-container {
    display: flow-root;
    width: 90%;
    margin: 0 auto;
    padding-bottom: 90px;
}
.student-profile-body {
    padding: 20px;
}
.student-profile-body__text {
    padding: 0 0 5px 0;
}
.student-profile-body__text--title {
    font-weight: 700;
}
.student-profile-header__heading--date {
    padding: 0 0 10px 0;
}
.student-profile-header .colorLegend {
    padding-left: 0;
}
.profile-detail-header .colorLegend, .student-profile-header .colorLegend {
    padding-left: 0;
    display: inline-block;
}
.profile-detail-header .colorLegend .colorLegend-p, .student-profile-header .colorLegend .colorLegend-p {
    padding: 0 25px 0 0;
    margin: 0;
    display: inline-block;
}

div.colorLegend span.available {
display: inline-block !important;
width: 20px !important;
height: 20px !important;
border-radius: 6px !important;
background-color: #a7ef9f !important;
margin-left: 10px;
}
div.colorLegend span.color {
display: inline-block !important;
width: 20px !important;
height: 20px !important;
border-radius: 6px !important;
}
.profile-detail-header .colorLegend .colorLegend-p .available, .student-profile-header .colorLegend .colorLegend-p .available {
border-radius: 6px !important;
background-color: #a7ef9f !important;
}
.profile-detail-header .colorLegend .colorLegend-p .color, .student-profile-header .colorLegend .colorLegend-p .color {
padding: 0;
margin: 0px 15px 0 0 !important;
width: 30px !important;
height: 24px !important;
display: inline-block !important;
vertical-align: middle;
}
.btn.student-profile__button {
background-color: #5CC75F !important;
border: solid 2px #5CC75F !important;
color: #fafbfc !important;
transition-delay: 0s;
transition-duration: 0.2s;
transition-property: all;
font-weight: 600 !important;
font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important;
text-transform: uppercase !important;
border-radius: 6px !important;
width: 100% !important;
margin: 20px 0 0 0 !important;
}
.btn {
display: block !important;
margin: auto !important;
width: 90% !important;
}
.btn {
display: inline-block;
padding: 6px 12px;
margin-bottom: 0;
font-size: 14px;
font-weight: 400;
line-height: 1.42857143;
text-align: center;
white-space: nowrap;
vertical-align: middle;
-ms-touch-action: manipulation;
touch-action: manipulation;
cursor: pointer;
-webkit-user-select: none;
-moz-user-select: none;
-ms-user-select: none;
user-select: none;
background-image: none;
border: 1px solid transparent;
border-radius: 4px;
}
@media screen and (min-width: 992px) {
    .btn.student-profile__button {
        width: 300px !important;
    }
}
    </style>
<!-- Main Content -->
<section class="content page-calendar student-profile-content">
    <div class="student-profile-content-container">
        <?php
            //	if(isset($_SESSION['TeacherID']))
            // $query = "SELECT (SELECT 111.045 * DEGREES(ACOS(LEAST(COS(RADIANS(c.latitude)) * COS(RADIANS(cc.latitude)) * COS(RADIANS(c.longitude - cc.longitude)) + SIN(RADIANS(c.latitude)) * SIN(RADIANS(cc.latitude)), 1.0) )) FROM contact cc WHERE cc.userID=$_SESSION[TeacherID] LIMIT 1)distance, (SELECT firstname FROM `contact`c1 WHERE c1.userID = 2 AND c1.contacttypeID = '3')parent_name, c.*,s.*,p.* FROM contact c, student s LEFT JOIN `profile` p ON p.profileID=s.profileID WHERE c.userID=s.userID AND c.userID=2 LIMIT 1";
            //else
            $query = "SELECT (SELECT firstname FROM `contact`c1 WHERE c1.userID = $_GET[sid] AND c1.contacttypeID = '3')parent_name, c.*,s.*,p.* FROM `contact`c, student s LEFT JOIN `profile` p ON p.profileID=s.profileID WHERE c.userID=s.userID AND c.userID=$_GET[sid] LIMIT 1";
            $result = mysqli_query($con, $query);

            $row = mysqli_fetch_assoc($result);

            $flag = true;
            $userID = 0;
            $isList = false;
            $isList = true;
        ?>
        <div class="student-profile-content__section">
            <div class="student-profile-header">
                <h3 class="ma-subtitle student-profile-header__heading" data-firstname="<?php echo $row['firstname'];?>">Leerling: <?php echo $row['firstname']; ?></h3>
            </div>
            <div class="student-profile-body">
                <p class="student-profile-body__text"><span class="student-profile-body__text--title">Adres: </span>
                    <?php echo $row['address'] . ", " . $row['postalcode'] . " " . $row['city'] ; ?>
                </p>
                <p class="student-profile-body__text"><span class="student-profile-body__text--title">Tel.nummer contactpersoon: </span><?php echo $row['telephone'];?></p>
                <p class="student-profile-body__text"><span class="student-profile-body__text--title">E-mailadres contactpersoon: </span><?php echo $row['email'];?></p>
                <p class="student-profile-body__text"><span class="student-profile-body__text--title">Naam ouder: </span><?php echo $row['parent_name'];?></p>
                <p class="student-profile-body__text"><span class="student-profile-body__text--title">Naam leerling: </span><?php echo $row['firstname'];?></p>
                <?php
                    $stdid = (@$_SESSION['StudentID'])? @$_SESSION['StudentID'] : 0;
                    if( isset($_GET['sid']) && isset($_SESSION['Teacher']) ) {
                    ?>
                    <a href="<?php echo "chat?sid=$row[userID]";?>"><button class="btn student-profile__button" data-type="prompt">Bericht versturen</button></a>
                <?php }	?>
            </div>
        </div>
        <?php
            if(!$isList){
                echo '<li class=" no-message text-center no-hover" style="">Geen Student<br/>
                <div style="font-size:16px;margin-top: 8px;">tutoring is a great step to better grades</div></i>';
            }
        ?>
        <div class="student-profile-content__section">
            <div class="student-profile-header">
                <h3 class="ma-subtitle student-profile-header__heading student-profile-header__heading--date"></h3>
                <div class="colorLegend">
                    <p class="colorLegend-p"><span class="color available"></span>Beschikbaar</p>
                    <p class="colorLegend-p"><span class="color request"></span> Bijlesverzoek</p>
                    <p class="colorLegend-p"><span class="color appointment"></span>Afspraak</p>
                </div>
                <div class="pull-right form-inline">
                    <div class="btn-group">
                        <button class="btn btn-primary" data-calendar-nav="prev"><<</button>
                        <button class="btn btn-default" data-calendar-nav="today">Today</button>
                        <button class="btn btn-primary" data-calendar-nav="next">>></button>
                    </div>
                </div>
            </div>
            <div id="calendar"></div>
        </div>
    </div>
</section>

	<div class="modal modal-fullscreen fade" id="modal-delete-course" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		  <h6 class="modal-title" id="myModalLabel"><b class="popupMsg">Berichtje versturen</b></h6>
      </div>
      <div class="modal-body">
        <div id="deresultcourse"></div>
                          <input type="hidden" id="tid" name="tid" value="<?php echo $_POST['tid']; ?>" />
                      <div class="form-group">
						  <div class="col-md-12 col-sm-12 col-xs-12" >
							  <div class="chat-message clearfix">
                            <div class="input-group p-t-15">
                                <input type="text" class="form-control" id="chatsender" style="height:40px;" autocomplete="off" placeholder="Typ bericht...">
                                <span class="input-group-addon sendMail">
                                    <i class="zmdi zmdi-mail-send"></i>
                                </span>
                            </div>                          
                        </div>
						  </div>
                      </div>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<?php
    require_once('includes/footerScriptsAddForms.php');
?>

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
    <script type="text/javascript" src="calendar/js/calendar.js"></script>
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->

<script>
     // this function is added
Date.prototype.setTimezoneOffset = function(minutes) { 
    var _minutes;
    if (this.timezoneOffset == _minutes) {
        _minutes = this.getTimezoneOffset();
    } else {
        _minutes = this.timezoneOffset;
    }
    if (arguments.length) {
        this.timezoneOffset = minutes;
    } else {
        this.timezoneOffset = minutes = this.getTimezoneOffset();
    }
    return this.setTime(this.getTime() + (_minutes - minutes) * 6e4);
};


 var calendar;
 var options;

    var teacherid = <?php echo (isset($_SESSION['TeacherID']))? $_SESSION['TeacherID'] : 0; ?>;
    var studentid = <?php echo (isset($_GET['sid']))? $_GET['sid'] : 0; ?>;
	var std_lat = "<?php echo (isset($_SESSION['std_lat']))? $_SESSION['std_lat']: $row['latitude']; ?>";
	var std_lng = "<?php echo (isset($_SESSION['std_lng']))? $_SESSION['std_lng']: $row['longitude']; ?>";
	var place_name = "<?php echo (isset($_SESSION['std_place_name']))? $_SESSION['std_place_name']: $row['place_name']; ?>";
	var travelDistance = "<?php echo (isset($row['distance']))? $row['distance']: 0; ?>";
	
    var calendar;
    var options;
	
	
var _obj = null;
var  _id = null;
var _name = null;
	
var _ddate = null;
var _stime = null;
 var _etime = null;

var __calender_date = null;


  $(document).ready(function() {
      
	  $("#sendMsg").click(function(){
          $('#modal-delete-course').appendTo("body").modal('show');   		  
		  $("#chatsender").val("");
	  });
	  
	  $(".cal-month-day").dblclick(function(e){e.preventDefault();});
  
  	$(".page-loader-wrapper").css("display", "none");
	  
    "use strict";
    var firstname = $(".firstname").attr('data-firstname');
      <?php
          $url = '/api/Calendar/';
          if (isset($_GET['sid']) && isset($_SESSION['TeacherID'])) {
              $url .= "GetFreeSlotsTutor";
          } else {
	          $url .= "GetAppointments";
          }
          $url .= '?studentID=';
      ?>
    var url = "<?php echo $url; ?>"+studentid;

    options = {
        events_source: url,
        view: 'month',
        tmpl_path: 'calendar/tmpls/student-profile/',
        tmpl_cache: false,
         // day: '2013-03-21', 
        // day: 'now',

        merge_holidays: false,
        display_week_numbers: false,
        weekbox: false,
        //shows events which fits between time_start and time_end
        show_events_which_fits_time: true,

        onAfterEventsLoad: function(events) {
            if(!events) {
                return;
            }
            var list = $('#eventlist');
            list.html('');

            $.each(events, function(key, obj) {
                //converting the timeZone to UTC
                obj.start = new Date(parseInt(obj.start)).setTimezoneOffset(0);
                obj.end = new Date(parseInt(obj.end)).setTimezoneOffset(0);

                $(document.createElement('li'))
                    .html('<a href="' + obj.url + '">' + obj.title + '</a>')
                    .appendTo(list);
            });
        },
        onAfterViewLoad: function(view) {
            $('.student-profile-header__heading--date').text(this.getTitle());
            $('.btn-group button').removeClass('active');
            $('button[data-calendar-view="' + view + '"]').addClass('active');
        },
        classes: {
            months: {
                general: 'label'
            }
        }
    };

     calendar = $('#calendar').calendar(options);

    $('.btn-group button[data-calendar-nav]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.navigate($this.data('calendar-nav'));
        });
    });

    $('.btn-group button[data-calendar-view]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.view($this.data('calendar-view'));
        });
    });

   $('#first_day').change(function(){
        var value = $(this).val();
        value = value.length ? parseInt(value) : null;
        calendar.setOptions({first_day: value});
        calendar.view();
    });

    $('#language').change(function(){
        calendar.setLanguage($(this).val());
        calendar.view();
    });

    $('#events-in-modal').change(function(){
        var val = $(this).is(':checked') ? $(this).val() : null;
        calendar.setOptions({modal: val});
    });
    $('#format-12-hours').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({format12: val});
        calendar.view();
    });
    $('#show_wbn').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({display_week_numbers: val});
        calendar.view();
    });
    $('#show_wb').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({weekbox: val});
        calendar.view();
    });
    $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
        //e.preventDefault();
        //e.stopPropagation();
    });

    $(".cal-month-day").on("click",function(){
        console.log($(this).find(".cal-slide-content").html());
  
    });


    $(".list-item").on("mouseover",function(e){
        e.preventDefault(); 
        console.log($(this).html());
    });

        // var x = 15; //minutes interval
        // var timeArr = []; // time array
        // var tt = 0; // start time
        // var ap = ['AM', 'PM']; // AM-PM

        // //loop to increment the time and push results in array
        // for (var i=0;tt<24*60; i++) {
        //     var hh = Math.floor(tt/60); // getting hours of day in 0-24 format
        //     var mm = (tt%60); // getting minutes of the hour in 0-55 format
        //     timeArr[i] = ("0" + hh ).slice(-2) + ':' + ("0" + mm).slice(-2) ;
        //     tt = tt + x; 
        // }

        function diff_hours(dt2, dt1) 
	{
		var diff =(dt2.getTime() - dt1.getTime()) / 1000;
		diff /= (60 * 60);
		return Math.abs(Math.round(diff));
	}

	var add_minutes =  function (dt, minutes) {
    	return new Date(dt.getTime() + minutes*60000); 
	};


	  $("body").on("change", "#calender_startTime", function () {
		  let selStart = $("#calender_startTime");
		  let stime = selStart.val();
		  const minTime = parseInt(selStart.find(':selected').attr("data-min"));
		  const maxTime = parseInt(selStart.find(':selected').attr("data-max"));
		  const startTime = new Date("01/01/2007 " + stime);

		  let endTime;
		  let tempEndTime;

		  let str = "";
		  for (let i = minTime; i <= maxTime; i += 15) {
			  //starttime + x minutes
			  endTime = add_minutes(startTime, i);
			  tempEndTime = ((endTime.getHours() > 9) ? endTime.getHours() : "0" + endTime.getHours()) + ":" + ((endTime.getMinutes() > 9) ? endTime.getMinutes() : "0" + endTime.getMinutes());
			  str += "<option value='" + i + "'" + (i === 90 ? "selected" : "") + ">" + tempEndTime + "</option>";
		  }
		  $("#calender_endTime").html(str);
	  });
	  
	  
	  $(".sendMail").click(function(e){ 
 
       

        var msg = $("#chatsender").val();
        var subj = 'null';
		var senderID = null;
		  
		  <?php 

	if(isset($_SESSION['StudentID']))
		echo "senderID = $_SESSION[StudentID];";
	else if(isset($_SESSION['TeacherID']))
		echo "senderID = $_SESSION[TeacherID];";
	else if(isset($_GET['sid']))			  
		echo "senderID = $_GET[sid];";

		  ?>
		  
		
		if(!msg.trim())
			return;
		
        $.ajax({
            type: "POST", 
            url: "includes/chat-ajax.php",
            data: { filename: "chat_text_msg_by_sender", msg: msg, senderId: senderID, recvId:"<?php echo $_GET['sid']; ?>", subj:subj},
            dataType: "text",
            success: function(response) { 
                if(res.status == "success"){
                          showNotification("alert-success", "Bericht verzonden!", "bottom", "center", "", "");
                }else{					
                          showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
				}
				$(".close").click();
            },
            error: function(xhr, ajaxOptions, thrownError) { 
                if(xhr.responseText == "success"){
                          showNotification("alert-success", "Bericht verzonden!", "bottom", "center", "", "");
                }else{					
                          showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
				}
				$(".close").click();
            }
        });  // ends ajax
    });     // () sendMail ends
	  
	  $("#chatsender").keyup(function(e){

        if(e.keyCode != 13)
            return;

        var msg = $(this).val();
        var subj = 'null';
		var senderID = null;
		  <?php 
		
			if(isset($_SESSION['StudentID']))
				echo "senderID = $_SESSION[StudentID];";
			else if(isset($_SESSION['TeacherID']))
				echo "senderID = $_SESSION[TeacherID];";
			else if(isset($_GET['sid']))			  
				echo "senderID = $_GET[sid];";
			
		  ?>
		  
		
		if(!msg.trim())
			return;
		
        $.ajax({
            type: "POST", 
            url: "includes/chat-ajax.php",
            data: { filename: "chat_text_msg_by_sender", msg: msg, senderId: senderID, recvId:"<?php echo $_GET['sid']; ?>", subj:subj},
            dataType: "text",
            success: function(response) { 
                var res = JSON.parse(response);
                if(res.status == "success"){
                          showNotification("alert-success", "Bericht verzonden!", "bottom", "center", "", "");
                }else{					
                          showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
				}
				$(".close").click();
            },
            error: function(xhr, ajaxOptions, thrownError) { 
                if(xhr.responseText == "success"){
                          showNotification("alert-success", "Bericht verzonden!", "bottom", "center", "", "");
                }else{					
                          showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
				}
				$(".close").click();
            }
        });  // ends ajax
    });     // () chatsender ends

	  
	  $("#delete-yes-type").click(function(){
        console.log('delete yes');
 
		  	cancelSlott(_obj, _id);		  
	  		$("#modal-delete-type").modal('hide');
	  });
	  
	  $("#delete-no-type").click(function(){
	  	 $("#modal-delete-type").modal('hide');  	
		   _obj = null;
		  _id = null;
		  _name = null; 
		  
		  _ddate = null;
		  _stime = null;
		  _etime = null;
	  });


  }); //main $()


  function removeButton(obj){
    $(obj).find(".inline-elem-buttons-box").remove();
  }


  function addButton(obj, id, slotType){
    var cases = $(obj).attr("data-event");
    var buttonText = (cases == "event-info")? "ANNULEREN" : "ANNULEREN";
	var button = '<span class="inline-elem-buttons-box">'
				   +'<button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect"'
				   +'value="cancel" onclick="cancelSlot(this, '+id+')">'
				   + buttonText
				   +'</button></span>';
	  
     $(obj).append(button);
	  
  }// addButton function


  function cancelSlot(obj, id){	  
	  
	$('#modal-delete-type').appendTo("body").modal('show'); 	  
	  _obj = obj;
	  _id = id;
 }
	
function cancelSlott(obj, id){
    console.log('call cancel');
    var sid = <?php $_GET['sid']; ?>
      $.ajax({ 
        type: "POST",  
        url: "student-ajaxcall.php",
        data: { subpage:"slotCancellation", cbID: id, sid: sid},
        dataType: "json",
        success: function(response) { 
                      if(response == "success"){ 
                          showNotification("alert-success", "Verzoek is geannuleerd", "bottom", "center", "", "");
                      }else{
                         showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                       calendar = $('#calendar').calendar(options);
                  },
        error: function(xhr, ajaxOptions, thrownError) { 
                      if(xhr.responseText == "success"){ 
                          showNotification("alert-success", "Verzoek is geannuleerd", "bottom", "center", "", "");
                      }else{
                         showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                       calendar = $('#calendar').calendar(options);

                      }
      });// ends ajax
  }
 
  function confirmSlot(obj, id){
    var data_opt;

    data_opt = { subpage:"confrimPending", cbID: id};
    
      $.ajax({
        type: "POST", 
        url: "teacher-ajaxcalls.php",
        data: data_opt,
        dataType: "json",
        success: function(response) { 
                      if(response.trim() == "success"){

                          showNotification("alert-success", "De bijles is bevestigd", "bottom", "center", "", "",4000);
                          
                           
                            //insert into accept table
                             $("#confirm tbody").prepend("<tr>"+$(obj).parent().parent().html()+"</tr>");
                             $("#confirm tbody tr:first-child").find(".inline-elem-button:first-child").remove();
                             //remove tr
                            $(obj).parent().parent().remove();


                            $("#confirm tbody").find(".dataTables_empty").parent().remove();

                            //recounting
                            $("#pending tbody tr td:first-child").each(function(index, obj){
                                $(this).html(index+1);
                            });
                             $("#confirm tbody tr td:first-child").each(function(index, obj){
                                $(this).html(index+1);
                            });

                            //recounting
                            $("#pending tbody tr td:first-child").each(function(index, obj){
                                $(this).html(index+1);
                            });
                             $("#confirm tbody tr td:first-child").each(function(index, obj){
                                $(this).html(index+1);
                            });
						  setTimeout(function(){ document.location.reload(); }, 4000);
                      }else{
                         showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                  },
        error: function(xhr, ajaxOptions, thrownError) { 
                      if((xhr.responseText).trim() == "success"){

                          showNotification("alert-success", "De bijles is bevestigd", "bottom", "center", "", "",4000);
                          
                            
                            //insert into confirm table
                             $("#confirm tbody").prepend("<tr>"+$(obj).parent().parent().html()+"</tr>");
                             $("#confirm tbody tr:first-child").find(".inline-elem-button:first-child").remove();
                             //remove tr
                            $(obj).parent().parent().remove();

                            $("#confirm tbody").find(".dataTables_empty").parent().remove();

                            //recounting
                            $("#pending tbody tr td:first-child").each(function(index, obj){
                                $(this).html(index+1);
                            });
                             $("#confirm tbody tr td:first-child").each(function(index, obj){
                                $(this).html(index+1);
                            });

                            //recounting
                            $("#pending tbody tr td:first-child").each(function(index, obj){
                                $(this).html(index+1);
                            });
                             $("#confirm tbody tr td:first-child").each(function(index, obj){
                                $(this).html(index+1);
                            });
						  setTimeout(function(){ document.location.reload(); }, 4000);
						  
                      }else{
                         showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);

                      }
      });// ends ajax
  }

  
  function bookTutor(){
        var stime = $("#calender_startTime").val();  
        var duration = $("#calender_endTime").val(); 
        var date = $("#calender_startTime").find(':selected').attr("data-date");
        var url = "/api/Calendar/BookTutor";
        $.ajax({
            type: "POST", 
            url: url,
            data: {studentID:studentid, teacherID:teacherid, date:date, time:stime, duration:duration},
            dataType: "json",
            success: function(response) {
	            trackEvent('Book Tutor', 'Click', 'Appointment' + response.level, response.rate, response.duration);
                showNotification("alert-success", "De bijles is ingeboekt", "bottom", "center", "", "");
                calendar = $('#calendar').calendar(options);
                    },
            error: function(xhr, ajaxOptions, thrownError) { 
                showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                        }
        });// ends ajax
}
	
	function booking(){

        var quarterHours = ["00", "15", "30", "45"];
        var times = [];
        for(var i = 0; i < 24; i++){
            for(var j = 0; j < 4; j++){
                var time = i + ":" + quarterHours[j];
                if(i < 10){
                time = "0" + time;
                }
                times.push(time);
            }
        }

	  
	  //check if teacher is login
       
	  if(true == <?php echo (isset($_SESSION['TeacherID']))? "false": "true"; ?>){
		$("#myModalLabel2").text("TO BOOK A Student SLOT, YOU HAVE TO LOGIN AS A Teacher.");
		$('#modal-login-type').appendTo("body").modal('show'); 
	  	 return;
  	  }
	
	 if(teacherid==0 || studentid ==0){
		$('#modal-login-type').appendTo("body").modal('show'); 
	  	 return;
  	  }

	  
	  var stime = $("#calender_startTime").val();
	  var etime = $("#calender_endTime").val();
	  
	  if(!stime || !etime)
		  return;
	  
	  //converting into date, so we can get the minutes after subtracting
	  var timeStart = new Date("01/01/2007 " + stime);
	  var timeEnd = new Date("01/01/2007 " + etime);

	  var minutes = 60*1000; //sec * millisec
	  
	  if(((timeEnd - timeStart)/minutes) < <?php echo MINIMUM_TIME_FOR_APPOINTMENT_SLOT; ?>){
			 showNotification("alert-danger", "Minimaal 45 minuten opeenvolgende slots kunnen worden geboekt", "bottom", "center", "", "");
		  return;
	  }
	  var timeArr = Global_Variable_timeRange[__calender_date];
	  
	  var slotTime = Array();
	  var slotDetail = Array();
	  var milliseconds = window.performance.now();
	  var isTimeStart = false;
	  var isTimeEnd = false;
	  var prevTime = null;
	  var strTime = null;
	  var tempTime = null;
      var firstname = $(".firstname").attr('data-firstname');

	  for(var time in times){
		  //adding slot_time to get end_time 
		  tempTime = new Date("01/01/2007 " + times[time]);
		  tempTime = new Date(tempTime.getTime() + <?php echo SLOT_TIME_IN_MINIUTES*60*1000; //millisec?>);
		  tempTime = ((tempTime.getHours()>9)? tempTime.getHours() : "0"+tempTime.getHours())+":"+((tempTime.getMinutes()>9)? tempTime.getMinutes() : "0"+tempTime.getMinutes());
	
		  strTime = times[time]+"-"+tempTime;
		  //adding slot_time to get end_time 
		  
		 /* if(isTimeEnd){
			  if(timeArr[time]== "available")
		  	  	slotTime.push({isGapSlot:1,  time:strTime , date:__calender_date});
			  break;
		  }
		  
		  if(time == etime){
			  slotTime.push({isGapSlot:0,  time:strTime , date:__calender_date});
			  isTimeEnd = true;
			  continue;
		  }
		  */ 
		  
		   if(times[time] == etime){
		  	  	slotTime.push({isGapSlot:1,  time:strTime , date:__calender_date});
			  break;
		  }
		  
		   
		  if(isTimeStart){
		  	  slotTime.push({isGapSlot:0,  time:strTime , date:__calender_date});
			  continue;
		  }  
		  
		  if(times[time] == stime){			  
			  if(prevTime){
		  	  	slotTime.push({isGapSlot:1,  time:prevTime+"-"+times[time] , date:__calender_date});
			  }
			  slotTime.push({isGapSlot:0,  time:strTime , date:__calender_date});
			  isTimeStart = true;
			  continue;
		  }
          if(!isTimeStart)
			  prevTime = times[time];
		  else
			  prevTime = null;
		  
		  
	  } //ends for()
	  
	  slotDetail.push({slotGID:(teacherid+ milliseconds), slotDetail:slotTime});
      $.ajax({ 
        type: "POST", 
        url: "student-ajaxcall.php",
        data: { subpage:"tslotBooking",firstname:firstname, studentid:studentid, teacherid: teacherid, slotData:slotDetail, std_lat:std_lat, std_lng:std_lng, place_name:place_name, travelDistance:travelDistance},
        dataType: "json",
        success: function(response) {  
                      if(response == "success"){ 
                          showNotification("alert-success", "Bijles ingeboekt", "bottom", "center", "", "");
                       calendar = $('#calendar').calendar(options);
                      }else{
						  if(response == "Maximum One Booking can be made.")
                         	showNotification("alert-danger", "Maximaal één boeking kan worden gemaakt.", "bottom", "center", "", "");
						  else if(response == "100hours Maximum time for classes is already occupied.")
                         	showNotification("alert-danger", "100 uur Maximale tijd voor lessen is al bezet.", "bottom", "center", "", "");
						  else 							  
                         	showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                  },
        error: function(xhr, ajaxOptions, thrownError) { 
                      if(xhr.responseText == "success"){
                          showNotification("alert-success", "Bijles ingeboekt", "bottom", "center", "", "");
                       	calendar = $('#calendar').calendar(options);
                      }else{						  
                         	showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
		 }// func ends 
      });// ends ajax
    }  

</script>
<!-----------------------------map ------------------------->
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>	
	
	<script>
		console.log("array...");
		var userIDs = '<?php echo  $userID ?>';
	    var _userIDs = userIDs.split(",");
		for(var i=1; i < _userIDs.length ; i++)
			{
				$("#map"+_userIDs[i]).trigger("click");
			}
	        
	function intializeLatLng(_lat, _lng, _place_name, userID)
		{	
	var map;
	var canvas;
	var lat = "";
	var lng = "";
	var place_name="";
	var mapCheck = true;
			 lat = _lat;
			 lng = _lng;
			 place_name= _place_name;		
				
				
mapboxgl.accessToken = 'pk.eyJ1Ijoic2JpbGFsc2hhaCIsImEiOiJjanU3cmw3aXIwMmE5NDl0Zjd6Mm56Nmg4In0.iobs2-o479pXYRd_zon95g';
map = new mapboxgl.Map({
container: 'map'+userID,
style: 'mapbox://styles/mapbox/streets-v11',
center: [-79.4512, 43.6568],
zoom: 13
});
 
var geocoder = new MapboxGeocoder({
accessToken: mapboxgl.accessToken, limit:1
});

map.addControl(geocoder);
 
// After the map style has loaded on the page, add a source layer and default
// styling for a single point.
map.on('load', function() {
	//$(".mapboxgl-ctrl-geocoder input").prop('disabled', true);
	//$(".mapboxgl-ctrl-geocoder").css('display', 'none');	 	
	geocoder.query(lng+","+lat);
map.resize();
// Listen for the `result` event from the MapboxGeocoder that is triggered when a user
// makes a selection and add a symbol that matches the result.
geocoder.on('result', function(ev) {
	console.log("Here: ",ev);
	lat = ev.result.center[1];
	lng = ev.result.center[0];
	place_name = ev.result.place_name;
	isAddressChanged = true;

map.getSource('point').setData(ev.result.geometry);
});
});








canvas = map.getCanvasContainer();
 
var geojson = {
"type": "FeatureCollection",
"features": [{
"type": "Feature",
"geometry": {
"type": "Point",
"coordinates": [-79.4512, 43.6568]
}
}]
};
 
 
map.on('load', function() {
 
// Add a single point to the map
map.addSource('point', {
"type": "geojson",
"data": geojson
});
 
map.addLayer({
"id": "point",
"type": "circle",
"source": "point",
"paint": {
"circle-radius": 10,
"circle-color": "#3887be"
}
});
 
// When the cursor enters a feature in the point layer, prepare for dragging.


 
map.on('mousedown', 'point', function(e) {
// Prevent the default map drag behavior.
e.preventDefault();
 
canvas.style.cursor = 'grab';
 
});
 
map.on('touchstart', 'point', function(e) {
if (e.points.length !== 1) return;
 
// Prevent the default map drag behavior.
e.preventDefault();
 
map.on('touchmove', onMove);
map.once('touchend', onUp);
});

});
			
} // end intilize function

</script>			
</body>
</html>
<div class="modal modal-fullscreen fade" id="modal-login-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel2">Log in of registreer om deze docent te boeken</h6>
      </div>
      <div class="modal-body">
        <div id="deresulttype"></div>
        <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                      <div class="form-group">
                        <div class="col-md-12 col-sm-6 col-xs-12  text-center">
                      
                          <button type="button" class="btn btn-success" id="login-yes-type">Login as Teacher</button>
                          <button type="button" class="btn btn-danger" id="login-no-type">Register as a Teacher</button>
                           
                        </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                      </div>
                    </form>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>
<div class="modal modal-fullscreen fade" id="modal-delete-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Weet je zeker dat je het verzoek wilt annuleren?</h6>
      </div>
      <div class="modal-body">
        <div id="deresulttype"></div>
        <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                          <input type="hidden" id="delete-id-type" name="delete-id-type" />
			<input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                      <div class="form-group">
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                      
                          <button type="button" class="btn btn-success" id="delete-yes-type">Ja</button>
                          <button type="button" class="btn btn-danger" id="delete-no-type">Nee</button>
                           
                        </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                      </div>
                    </form>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>
<?php
}
?>