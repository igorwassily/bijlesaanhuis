<?php
$thisPage = "Edit Profile";
session_start();
if (!isset($_SESSION['Teacher'])) {
    header('Location: index.php');
} else {
    function translateToDutch($val)
    {
        if ($val == "Accepted")
            return "Geaccepteerd";
        else if ($val == "Rejected")
            return "Afgewezen";
        else if ($val == "Requested")
            return "In behandeling";
    }
?>
    <!doctype html>
    <html class="no-js " lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Veranderingen doorvoeren: dat doe je hier | Verdien meer dan bij onze grootste concurrenten | Bepaal zelf aan wie, wanneer en waar je bijles geeft.">
        <title>Profiel aanpassen | Bijles Aan Huis</title>
        <?php
        require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
        ?>
        <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link rel="stylesheet" type="text/css" href="/app/assets/plugins/bootstrap-select/css/bootstrap-select.css">
        <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-multiselect.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.min.css">
        <link rel="stylesheet" type="text/css" href="assets/css/pages/edit-profile.css">
        <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="/app/assets/plugins/croppie/demo.css">
        <link rel="stylesheet" type="text/css" href="/app/assets/plugins/croppie/croppie.css">
        <?php
        $activePage = basename($_SERVER['PHP_SELF']);
        ?>
    </head>

    <body class="edit-profile">
        <!-- Page Loader -->
        <!-- <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo">
                </div>
                <p>Please wait...</p>
            </div>
        </div> -->
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>
    <?php
    require_once('includes/header.php');
    include('includes/edit_teacher_profile.php');
}
    ?>
    </body>

    </html>