<?php
$thisPage = "Teachers Registration";
session_start();
$_SESSION['isSlotBooked'] = false;
/*if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{*/
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta property="og:title" content="Registreren als docent">
	<meta name="twitter:title" content="Registreren als docent">
	<meta property="og:description" content="Kom bijles geven aan leerlingen van de middelbare school of basisschool | Verdien meer dan bij andere bedrijven | Bepaal zelf welke leerling jij accepteert.">
	<meta property="twitter:description" content="Kom bijles geven aan leerlingen van de middelbare school of basisschool | Verdien meer dan bij andere bedrijven | Bepaal zelf welke leerling jij accepteert.">
    <meta name="description"
          content="Kom bijles geven aan leerlingen van de middelbare school of basisschool | Verdien meer dan bij andere bedrijven | Bepaal zelf welke leerling jij accepteert.">
    <title>Registreren als docent | Bijles Aan Huis</title>
	<?php
	require_once('includes/connection.php');
	require_once('includes/mainCSSFiles.php');

	?>
    <link rel="stylesheet" type="text/css" href="/app/assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="/app/assets/plugins/bootstrap-select/css/bootstrap-select.css">
    <link rel="stylesheet" type="text/css" href="/app/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" type="text/css" href="/app/assets/css/bootstrap-multiselect.css">
    <link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/app/assets/css/nigel.css">
    <link rel="stylesheet" type="text/css" href="calendar/css/calendar.css">
	<?php
	$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green teacher-registration">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30">
                <img class="zmdi-hc-spin" src="/app/assets/images/logo.svg" width="48" height="48" alt="Oreo">
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <div class="overlay"></div>
    <?php
    require_once('includes/header.php');
	?>
	
<style>
section.content > .container-fluid {
    width: 100%;
	max-width: unset;
}
.block-header {
	margin-left: 10px;
}
.js-school-level--helper-message label {
	font-size:14px;
}
.school-type div.dropdown-menu {
	min-width: 175px !important;
}
.calender_register_text h3 {
	padding-top: 0;
}
#calender_startTime {
	font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important;
}
</style>
    <section class="content">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <h2 class="tutor_registration_main_title">Aanmelden als docent</h2>
                </div>
            </div>
        </div>
        <div class="container-fluid">
            <div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">
                    <form id="teacherRegistration" enctype="multipart/form-data">
                        <h4>Inloggegevens</h4>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6">
                                    <input type="hidden" class="form-control" placeholder="Skype ID" name="skypeid"
                                           id="skypeid"/>

                                    <script type="text/javascript">
                                        function emailCheck() {
                                            var email = $("#email[data-id='first']").val();
                                            var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                                            if (regex.test(email)) {
                                                $.ajax({
                                                    url: 'actions/email.php',
                                                    data: {'email': email},
                                                    type: 'POST',
                                                    success: function (result) {
                                                        $("#checkEmail").val(result);
                                                    }
                                                });
                                            }
                                        }
                                    </script>

                                    <div class="form-group form-float">
                                        <label for="username">E-mailadres *</label>
                                        <input type="hidden" class="form-control" placeholder="E-mailadres" name="choice"
                                               id="choice" value="Teacher-Registration"/>
                                        <input type="hidden" class="form-control" placeholder="User Group" name="usergroup"
                                               id="usergroup" value="Teacher"/>
                                        <input type="email" required class="form-control registration_form_input"
                                               placeholder="E-mailadres" onkeyup="emailCheck();" name="email" id="email"
                                               data-id="first" data-required="true"/>
                                        <input type="hidden" name="checkEmail" id="checkEmail" value=""/>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for="username">Wachtwoord *</label>
                                        <input type="password" class="form-control registration_form_input"
                                               placeholder="Wachtwoord" name="password" id="password" minlength="8"
                                               required/>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for="username">Bevestig wachtwoord *</label>
                                        <input type="password" class="form-control registration_form_input"
                                               placeholder="Bevestig wachtwoord" name="confirm_password"
                                               id="confirm_password" minlength="8" required/>
                                    </div>
                                    <div class="form-group form-float">
                                        <button type="button" class="btn registration_form_button btn-next">Volgende</button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h4>Persoonlijke informatie</h4>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6">
                                    <div class="form-group form-float">
                                        <label for="booking-code">Voornaam *</label>
                                        <input type="text" class="form-control registration_form_input"
                                               placeholder="Voornaam" name="first-name" id="first-name" required>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for="booking-code">Achternaam *</label>
                                        <input type="text" class="form-control registration_form_input"
                                               placeholder="Achternaam" name="last-name" id="last-name" required>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for="booking-code">Geboortedatum * (dd/mm/yyyy)</label>
                                        <input style="display:none">
                                        <input type="text" class="datepicker-here registration_form_input form-control"
                                               name="dob" id="dob" placeholder="Geboortedatum" autocomplete="off"
                                               required>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for="enfants">Telefoonnummer *</label>
                                        <input type="text"
                                               class="form-control registration_form_input"
                                               placeholder="Telefoonnummer"
                                               name="telephone"
                                               id="telephone"
                                               maxlength="19"
                                               required>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for='enfants'>Adres vanaf waar je naar de bijles reist</label>
                                        <label for="error" id="errorMessage2">Wij hebben geen adres gevonden</label>
                                        <label for="error" id="errorMessage3">Voer uw huisnummer in</label>
                                        <input class="form-control registration_form_input registration_form_input--address" type="text"
                                               name="postCode" placeholder="Postcode" id="postcode2" required>
                                        <input class="form-control registration_form_input registration_form_input--address" type="text"
                                               name="houseNumber" placeholder="Huisnummer" id="huisnummer2" required>
                                        <input class="form-control registration_form_input" type="text" name="street"
                                               placeholder="Straatnaam" id="straatnaam2"
                                               class="form-control registration_form_input">
                                        <input type="hidden" class="form-control registration_form_input registration_form_input--spaced"
                                               name="_place_name" id="place_name2">
                                        <input class="form-control registration_form_input" type="text" name="city"
                                               placeholder="Woonplaats" id="woonplaats2">
                                    </div>
                                    <div class="form-group form-float registration_progress">
                                        <button type="button" class="btn btn-prev registration_progress_button">
                                            Vorige
                                        </button>
                                        <button type="button" class="btn btn-next registration_progress_button">
                                            Volgende
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h4>Vakken en reisafstand</h4>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-2"></div>
                                <div class="col-sm-8">
                                    <div class="form-group form-float">
                                        <label for="booking-code">Beschikbaar voor online bijles? *</label>
                                        <label class="registration-radio">
                                            <input type="radio" class="registration_radio_tick green" placeholder=""
                                                   name="online-courses" id="online-courses" value="Yes" required>
                                            <span>Beschikbaar</span>
                                        </label>
                                        <label class="registration-radio">
                                            <input type="radio" class="registration_radio_tick red" placeholder=""
                                                   name="online-courses" id="online-courses" value="No" required>
                                            <span>Niet beschikbaar</span>
                                        </label>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for="booking-code1">Huidige opleiding / studie en leerjaar *</label>
                                        <input type="text" name="current_edu" id="current_edu"
                                               class="form-control registration_form_input registration_current_edu"
                                               placeholder="Bijv: 3e jaar bedrijfskunde of VWO 6" required/>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-lg-12 col-sm-12">
                                            <label for="booking-code22">Selecteer in welke vakken je bijles wilt geven *</label>
                                            <div class="card">
                                                <div class="body courses-div">
                                                    <div id="newCourse">
                                                        <div class="row clearfix" id="course-row1">
                                                            <input type="hidden" name="counter" class="counter" value="1"/>
                                                            <input type="hidden" id="__fieldcounter" name="fieldcounter"
                                                                   value="1"/>
                                                            <div class="col-lg-3 col-md-6" style="z-index:10 !important; padding-left: 0;">
                                                                <p> Schooltype </p>
                                                                <select class="form-control show-tick school-type dynamic-course"
                                                                        data-actions-box="true" data-dropup-auto="false"
                                                                        data-size="3" id="school-type1" data-id="1"
                                                                        name="select-school-type1[]">
                                                                    <option value="" selected>Selecteer</option>
                                                                    <?php
                                                                    $stmt = $con->prepare("SELECT DISTINCT typeID, typeName from schooltype group by typeName");
                                                                    $stmt->execute();
                                                                    $stmt->bind_result($typeID, $typeName);
                                                                    $stmt->store_result();
                                                                    while ($stmt->fetch()) {
                                                                        ?>
                                                                        <option value="<?php echo $typeID; ?>"><?php echo $typeName; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-lg-3 col-md-6 form-float" style="z-index:9 !important">
                                                                <p>Schoolvak </p>
                                                                <select class="form-control show-tick school-course"
                                                                        name="school-course1[]" data-size="15"
                                                                        data-dropup-auto="false" id="school-course1"
                                                                        data-id="1" required>
                                                                    <option value="" selected>Selecteer</option>
                                                                </select>
                                                            </div>
                                                            <div class="d-md-none col-12 js-school-level--helper-message js-mobile">
                                                                <label>
                                                                    Selecteer voor elk niveau alle leerjaren waarin je
                                                                    bijles wilt geven
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-3 col-md-6 school-level-wrapper" style="z-index:8 !important">
                                                                <p>Niveau </p>
                                                                <select class="form-control show-tick school-level"
                                                                        data-actions-box="true" data-dropup-auto="false"
                                                                        multiple="multiple" id="school-level1" data-id="1"
                                                                        data-size="13" name="school-level1[]"
                                                                        data-deselect-all-text="Verwijderen"
                                                                        data-select-all-text="ALLES SELECTEREN">
                                                                    <option value="" selected>Selecteer</option>
                                                                    <?php
                                                                    $stmt = $con->prepare("SELECT * from schoollevel");
                                                                    $stmt->execute();
                                                                    $stmt->bind_result($levelID, $levelName);
                                                                    $stmt->store_result();
                                                                    while ($stmt->fetch()) {
                                                                        ?>
                                                                        <option value="<?php echo $levelID; ?>"><?php echo $levelName; ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-5 col-lg-2 js-school-level--helper-message js-desktop">
                                                                <label>
                                                                    Selecteer voor elk niveau alle leerjaren waarin je bijles wilt geven
                                                                </label>
                                                            </div>
                                                            <div class="col-lg-1 col-md-1 d-md-block d-flex align-items-center" style="padding-right:0;">
                                                                <span class="deleteRow glyphicon glyphicon-remove" style="display:none;"></span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="row">
                                                        <div class="col-md-6 col-sm-12 offset-md-3 d-flex justify-content-center">
                                                            <button type="button" class="registration_progress_button btn" id="add-more-courses">
                                                                EXTRA VAK TOEVOEGEN
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="form-group form-float">
                                                <label for="booking-code">Hoeveel km ben je maximaal bereid te reizen? *
                                                    <i class="fa alignment tippy" data-toggle="tooltip"
                                                       data-placement="bottom"
                                                       data-tippy-content="Alle afstand zijn hemelsbreed. De eerste 3,5 km voor een enkele reis is verwerkt in de vergoeding die je krijgt voor een bijles. Hierna krijg je (ongeveer) 1 euro voor elke kilometer per enkele reis vergoed. Leerlingen die verder wonen dan de hier ingevulde afstand zullen jou niet vinden, ook niet met reiskosten."><img
                                                                src="/app/assets/images/info-icon.png"></i>
                                                </label>
                                                <div class="registration-distance">
                                                    <input type="number" class="registration_form_input registration-distance__input"
                                                           placeholder="Vul minimaal 3,5 km en maximaal 20 km in." name="distance" id="distance"
                                                           min="3.5" max="50" step="0.1" required
                                                           title="Vul minimaal 3,5 km en maximaal 20 km in.">
                                                    <div class="registration-distance__label">km</div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group form-float">
                                            <label for="booking-code">Optioneel: een korte motivatie waarom je bijles wilt geven</label>
                                            <textarea class="form-control registration_form_input" placeholder="Motivatie"
                                                      name="motivation" id="motivation"></textarea>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-lg-12 col-md-12 col-sm-12">
                                            <button type="button" class="btn btn-prev registration_progress_button">
                                                Vorige
                                            </button>
                                            <button type="button" class="btn btn-next registration_progress_button">
                                                Volgende
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h4>Kennismaking</h4>
                        <fieldset>
                            <div class="registration-calendar">
                                <div class="registration-calendar-header">
                                    <p class="registration-calendar-header__text">Reserveer hier een tijdstip voor een
                                        kennismakingsgesprek via Skype van maximaal 20 minuten. Mocht je achteraf toch niet
                                        kunnen, mail ons dan minimaal 24 uur voor het hieronder gewenste tijdstip. Vermeld
                                        hierbij je nieuwe beschikbaarheid voor dit gesprek.</p>
                                    <h3 class="registration-calendar-header__title"></h3>
                                    <div class="calendar_legend">
                                        <div class="calendar_legend_block"></div>
                                        <p class="calendar_legend_p">Beschikbaar</p>
                                    </div>
                                    <div class="pull-right form-inline">
                                        <div class="btn-group">
                                            <button class="btn btn-primary" data-calendar-nav="prev"><<</button>
                                            <button class="btn btn-default" data-calendar-nav="today">Today</button>
                                            <button class="btn btn-primary" data-calendar-nav="next">>></button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="calendar"></div>
                            <div class="teacher-registration-skype">
                                <label class="teacher-registration-skype__title">Skype naam (optioneel)</label>
                                <input type="text" id="skype" name="skype"
                                       class="teacher-registration-skype__input form-control registration_form_input"
                                       placeholder="Skype naam"
                                       title="Optioneel: vul hier je unieke Skype naam in. Deze vind je binnen Skype bij ‘Instellingen -> Account &amp; Profiel’ en begint altijd met ‘Live’.">
                            </div>
                            <div class="form-group form-float">
                                <input type="hidden" name="teacherid" id="teacherid" value=""/>
                                <input type="hidden" name="interview_datetime" id="interview_datetime" value=""/>
                                <input type="hidden" name="interview_date" id="interview_date" value=""/>
                            </div>
                            <div class="form-group form-float registration-calendar-buttons">
                                <button type="button" class="btn btn-prev registration_progress_button">Vorige</button>
                                <button type="button" class="btn btn-submit registration_progress_button">Aanmelden</button>
                            </div>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </section>

<script src="/app/assets/bundles/libscripts.bundle.js"></script>
<script src="/app/assets/bundles/vendorscripts.bundle.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>
<script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script type="text/javascript">
	$(function () {

		$(window).keydown(function (event) {
			if (event.keyCode == 13) {
				event.preventDefault();
				return false;
			}
		});


		//Advanced form with validation
		var form = $('#teacherRegistration').show();
		form.steps({
			headerTag: 'h4',
			bodyTag: 'fieldset',
			onInit: function (event, currentIndex) {
				//  $.AdminOreo.input.activate();
				//Set tab width
				var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
				var tabCount = $tab.length;
				$tab.css('width', (100 / tabCount) + '%');

				//set button waves effect
				setButtonWavesEffect(event);
			},
			onStepChanging: function (event, currentIndex, newIndex) {

				if (newIndex == 1 && mapCheck) {
					initMap();
					mapCheck = false;
				}


//			else if (newIndex > currentIndex){
//				$("#map").html(" ");
//			initMap();
				//		}
				//		else {
				//alert("m1"+currentIndex+" "+newIndex);
				//			$("#map").html(" ");

				//		}

				if (currentIndex > newIndex) {
					mapCheck = false;
					return true;
				}


				if (currentIndex < newIndex) {
					mapCheck = false;
					form.find('.body:eq(' + newIndex + ') label.error').remove();
					form.find('.body:eq(' + newIndex + ') .error').removeClass('error');

				}


				if (currentIndex === 1) {
					var huisnummer = $('#huisnummer2').val();

					if (huisnummer.length == 0) {
						// console.log("huis empty");
						$('#errorMessage3').css('display', 'block');
						return false;
					} else {
						$('#errorMessage3').css('display', 'none');

					}
				}

				form.validate().settings.ignore = ':disabled,:hidden';
				return form.valid();
			},
			onStepChanged: function (event, currentIndex, priorIndex) {
				//Jquery AJAX code to add client upon  insertion

			},
			onFinishing: function (event, currentIndex) {
				form.validate().settings.ignore = ':disabled';
				return form.valid();
			},
			onFinished: function (event, currentIndex) {
				$("#teacherid").val(teacherid);


				$('.page-loader-wrapper').css('display', 'block');
				$('.page-loader-wrapper').css('background-color','rgba(255,255,255,.5)');
				$.ajax({
					url: 'actions/add_scripts.php',
					type: 'POST',
					data: new FormData(this),
					contentType: false,
					cache: false,
					processData: false,
					success: function (result) {

						switch (true) {

							case result.indexOf("Error") >= 0:

								$('.page-loader-wrapper').css('display', 'none');
								setTimeout(function () {

									$('body').waitMe('hide');
									$('#teacherRegistration')[0].empty();
									showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen", "top", "center", "", "");
									//$("#error-epm").html("There Was An Error Inserting The Record.");

								}, 500);
								break;

							case result.indexOf("Succes") >= 0:


								setTimeout(function () {
									// console.log('test voor succes');
									$('#teacherRegistration')[0].reset();
									window.location.replace("/app/thankyou");


								}, 500);
								break;


						}
					}
				});

			}
		});
		$.validator.addMethod("notEqual", function (value, element, param) {
			return this.optional(element) || value != $(param).val();
		}, "Dit e-mailadres heeft al een account. Log in met dit account.");

		$.validator.addMethod("telephone", function (value, element) {
			return this.optional(element) || /^[\+ ]?[( ]?[0-9 ]{3}[) ]?[-\s\.]?[0-9 ]{3}[-\s\.]?[0-9 ]{4,12}$/im.test(value);
		}, "Geen geldig telefoonnummer");

		$.validator.addMethod("dob", function (value, element) {
			return this.optional(element) || /^([0-2][0-9]|(3)[0-1])(\/|-)(((0)[0-9])|((1)[0-2]))(\/|-)\d{4}$/im.test(value.replace(" ", ""));
		}, "Vul een geldige datum in");

		$.validator.addMethod("huisnummer", function (value, element) {
			// console.log("in here");
			return /^[0-9]+$/im.test(value);
		}, "Please teype in huisnummer");

		form.validate({
			highlight: function (input) {
				$(input).parents('.form-line').addClass('error');
			},
			unhighlight: function (input) {
				$(input).parents('.form-line').removeClass('error');
			},
			errorPlacement: function (error, element) {
				$(element).parents('.form-group').append(error);
			},
			rules: {
				confirm_password: {
					equalTo: '#password'
				},
				"select-course": "required",
				email:
					{
						notEqual: '#checkEmail',
					},
				telephone: {
					telephone: "#telephone"
				},
				dob: {
					dob: "#dob"
				},
				huisnummer: {
					huisnummer: "#huisnummer2"
				},
				'parent-telephone': {
					telephone: "#parent-telephone"
				}
			} // end of rules

		});

	});

	function setButtonWavesEffect(event) {
		$(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
		$(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
	}


	function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
		if (colorName === null || colorName === '') {
			colorName = 'bg-black';
		}
		if (text === null || text === '') {
			text = 'Turning standard Bootstrap alerts';
		}
		if (animateEnter === null || animateEnter === '') {
			animateEnter = 'animated fadeInDown';
		}
		if (animateExit === null || animateExit === '') {
			animateExit = 'animated fadeOutUp';
		}
		var allowDismiss = true;

		$.notify({
				message: text
			},
			{
				type: colorName,
				allow_dismiss: allowDismiss,
				newest_on_top: true,
				timer: 1000,
				placement: {
					from: placementFrom,
					align: placementAlign
				},
				animate: {
					enter: animateEnter,
					exit: animateExit
				},
				template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
					'<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
					'<span data-notify="icon"></span> ' +
					'<span data-notify="title">{1}</span> ' +
					'<span data-notify="message">{2}</span>' +
					'<div class="progress" data-notify="progressbar">' +
					'<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
					'</div>' +
					'<a href="{3}" target="{4}" data-notify="url"></a>' +
					'</div>'
			});
	}


</script>
<?php
require_once('includes/footerScriptsAddForms.php');
?>
<script type="text/javascript" src="/app/assets/js/bootstrap-multiselect.js"></script>
<script>

	function delete_btn(id) {
		var c_id;
		if (id == "1") {
			c_id = "#id-card";
			//alert(c_id);
		} else if (id == "2") {
			c_id = "#school-docs";
			//alert(c_id);
		}

		$(c_id).replaceWith($(c_id).val('').clone(true));
	}


	function showSchoolLevelPickerMessage(e, courseRowId) {
		setTimeout(function () {
			if ($(e.currentTarget).parent().hasClass('open') || $(e.currentTarget).parent().find('.dropdown-menu').css('display') == 'block' || $(e.currentTarget).attr('aria-expanded') === "true") {
				$(courseRowId).find('.js-school-level--helper-message').show();
			} else {
				$(courseRowId).find('.js-school-level--helper-message').hide();
			}
		}, 200);
	}

	$(document).ready(function () {


		//Prev and Next Button code
		$(".btn-next").click(function () {
			$(".wizard > .actions ul li:nth-child(2) a").click();

		});
		$(".btn-prev").click(function () {
			$(".wizard > .actions ul li:nth-child(1) a").click();
		});
		$(".btn-submit").click(function () {
			$(".wizard > .actions ul li:nth-child(3) a").click();
		});

		//HTML Date Changer
		//   $(".error-date").hide();

		$(document).on("click", ".deleteRow", function () {
			$(this).parent().parent().remove();
		});

		setTimeout($('#course-row1 .school-level .dropdown-toggle, #course-row1 .school-level .bootstrap-select,#course-row1 .school-level.bootstrap-select,#course-row1 .school-level .dropdown-backdrop').on('click', function (e) {
			showSchoolLevelPickerMessage(e, '#course-row1')
		}), 300);

		$("#dob").on("change", function () {
			this.setAttribute(
				"data-date",
				moment(this.value, "YYYY-MM-DD")
					.format(this.getAttribute("data-date-format"))
			)

			// var reDate = /(?:0[1-9]|[12][0-9]|3[01])\/(?:0[1-9]|1[0-2])\/(?:19|20\d{2})/;
			// var dateElement = $(".datepicker-here");

			// if(reDate.test(dateElement.val())){
			// 	console.log("date is valid");   
			// 	$(".error-date").hide(); 
			// }else{
			// 	console.log("date is not valid");
			// 	$(".error-date").show(); 
			// }
		});


		/* Fetch Pre Information for courses */

		function fetch_preinfo(cat, className, rowID) {

			$.ajax({
				url: 'actions/fetch_preinfo.php',
				data: {'cat': cat},
				type: "POST",
				//dataType: "json",
				success: function (result) {
					//$(this).parent().parent().siblings().find(".school-course").html("");
					var rowselector = "#";
					var selector = ".";
					//var separator ">";
					var classNam = rowselector.concat(rowID);
					var classNam1 = selector.concat(className);
					$(classNam).children().find(classNam1).not(".btn-group", ".bootstrap-select").html("");
					var json_obj = $.parseJSON(result);
					var output = '';
					var added = false;

					for (var i in json_obj) {

						if (String(json_obj[i].Name) !== 'Basisschool' && String(json_obj[i].Name) !== 'Middelbare school') {
							$(classNam).children().find(classNam1).not(".btn-group", ".bootstrap-select").append('<option value="' + String(json_obj[i].ID) + '">' + String(json_obj[i].Name) + '</option>');
						} else {
							if (added === false) {
								$(classNam).children().find(classNam1).not(".btn-group", ".bootstrap-select").append('<option value="">Selecteer</option>');
								added = true;
							}

							$(classNam).children().find(classNam1).not(".btn-group", ".bootstrap-select").append('<option value="' + String(json_obj[i].ID) + '">' + String(json_obj[i].Name) + '</option>');
						}


					}

					$(classNam).children().find(classNam1).not(".btn-group", ".bootstrap-select").selectpicker('refresh');
				}
			});

		}

		// date function...
		/*	function parseDMY(value) {
			var date = value.split("/");
			var d = parseInt(date[0], 10),
				m = parseInt(date[1], 10),
				y = parseInt(date[2], 10);
			return new Date(y, m - 1, d);
			}  */

		fetch_preinfo("SchoolType", "school-type", "course-row1");
		fetch_preinfo("SchoolLevel", "school-level", "course-row1");
		fetch_preinfo("SchoolYear", "school-year", "course-row1");
		fetch_preinfo("SchoolCourse", "school-course", "course-row1");


		var __obj = null;

		$(document).on("change", ".school-type", function (e) {
			// console.log("type loaded");
			e.stopImmediatePropagation();

			__obj = $(this).parent().parent().parent();

			var id = $(__obj).find(".school-type:eq(1)").attr("id");
			var stype = $("#" + id).val();
			stype = (typeof (stype) == "object") ? stype.join(",") : stype;


			var lid = $(__obj).find(".school-level:eq(1)").attr("id");

			var yid = $(__obj).find(".school-year:eq(1)").attr("id");

			if (stype == "<?php echo ELEMENTRY_TYPE_ID; ?>") {
				$("#" + yid).parent().parent().css("display", "none");
				$("#" + yid).prop("disabled", true);
				$("#" + yid + " [value='']").attr('selected', 'true');

				$("#" + lid).parent().parent().css("display", "none");
				$("#" + lid).prop("disabled", true);
				$("#" + lid + " [value='']").attr('selected', 'true');
			} else {

				$("#" + yid).parent().parent().css("display", "block");
				$("#" + yid).prop("disabled", false);
				$("#" + yid).selectpicker('val', '');

				$("#" + lid).parent().parent().css("display", "block");
				$("#" + lid).prop("disabled", false);
				$("#" + lid).selectpicker('refresh');
			}
			$.ajax({
				url: 'actions/fetch_courses2.php',
				data: {'typeID': stype, 'levelID': '', 'yearID': '', tid: false},
				type: "POST",
				//dataType: "json",
				success: function (result) {
					id = $(__obj).find(".school-course:eq(1)").attr("id");
					$("#" + id).html('<option value="">Selecteer</option>' + result);
					$("#" + id).selectpicker('refresh');
				}
			});

		});

		//show-tick ends

		$(document).on("change", ".school-course", function (e) {
			e.stopImmediatePropagation();

			__obj = $(this).parent().parent().parent();

			var cid = $(this).val();

			$.ajax({
				url: 'actions/course_filter.php',
				data: {'subpage': "courselevelyear", cid: cid},
				type: "POST",
				success: function (result) {
					id = $(__obj).find(".school-level:eq(1)").attr("id");
					$("#" + id).html(result);
					$("#" + id).selectpicker('refresh');
				}
			}); //ajax ends

		}); // $("#school-course1").change() ends

		/*
   $(document).on("change", ".school-level" , function(e){
	  e.stopImmediatePropagation();

	   __obj = $(this).parent().parent().parent();

	   var id = $(__obj).find(".school-course:eq(1)").attr("id");
	  var scourse = $("#"+id).val();
	  scourse = (typeof(scourse) == "object")? scourse.join(",") : scourse;

	  var clid = $(this).val();

	  $.ajax({
		  url: 'actions/course_filter.php',
		  data: {'subpage': "courseyear", clid:clid, cid:scourse},
		  type: "POST",
		  success: function (result) {
			  id = $(__obj).find(".school-year:eq(1)").attr("id");
								$("#"+id).html(result);
								$("#"+id).selectpicker('refresh');
		  }
	  }); //ajax ends

  }); // $(".school-level").change() ends

 */

		/* Add More Courses */
		/* Courses AJAX Calls */
		$("#add-more-courses").click(function () {

			var count = $(".body").children().find(".row").length - 1;
			__fieldcounter++;
			var te = __fieldcounter;
			$("#newCourse").append('<div class="row clearfix marginTop" id="course-row' + te + '" ><input type="hidden" name="counter" class="counter" value="' + te + '" /><div class="col-lg-3 col-md-6" style="z-index:10 !important;"><p> Schooltype </p><select data-size="3" data-dropup-auto="false" class="form-control show-tick dynamic-course school-type" id="school-type' + te + '" data-id="' + te + '" name="select-school-type' + te + '[]" ></select></div><div class="col-lg-3 col-md-6" style="z-index:9 !important;"><p> Schoolvak </p><select data-dropup-auto="false" class="form-control show-tick school-course" data-size="15" name="school-course' + te + '[]" id="school-course' + te + '" data-id="' + te + '"></select></div>' +

				'<div class="d-md-none col-12 js-school-level--helper-message js-mobile">' +
				'<label>Selecteer voor elk niveau alle leerjaren waarin je bijles wilt geven</label>' +
				'</div>' +

				'<div class="col-lg-3 col-md-6 school-level-wrapper" style="z-index:8 !important;"><p> Niveau </p><select data-actions-box="true" class="form-control show-tick school-level" data-dropup-auto="false" multiple="multiple" id="school-level' + te + '" data-id="' + te + '" name="school-level' + te + '[]" data-deselect-all-text="Verwijderen" data-select-all-text="ALLES SELECTEREN"></select></div>' +
				'<div class="col-md-5 col-lg-2 js-school-level--helper-message js-desktop">' +
				'<label>Selecteer voor elk niveau alle leerjaren waarin je bijles wilt geven</label>' +
				'</div>' +
				"<div class='col-lg-1 col-md-1 d-md-block d-flex align-items-center'>" +
				"<span class='deleteRow glyphicon glyphicon-remove'></span>" +
				"</div></div>");
			fetch_preinfo("SchoolType", "school-type", "course-row" + te);
			$("#school-level" + te).selectpicker('refresh');
			$("#school-course" + te).selectpicker('refresh');
			//$("#school-year"+te).selectpicker('refresh');

			$("#__fieldcounter").val(__fieldcounter);

			$('#course-row' + te + ' .school-level .dropdown-toggle,#course-row' + te + ' .school-level .bootstrap-select,#course-row' + te + ' .school-level .bootstrap-select span,#course-row' + te + ' .school-level .dropdown-backdrop').on('click', function (e) {
				showSchoolLevelPickerMessage(e, '#course-row' + te)
			});


		});

	});


</script>
<script>
	var map;
	var canvas;
	var lat = 0;
	var lng = 0;
	var place_name = "";
	var mapCheck = false;

	function initMap() {


		mapboxgl.accessToken = "<?php echo ACCESS_TOKEN; ?>";
// map = new mapboxgl.Map({
// container: 'map',
// style: 'mapbox://styles/mapbox/streets-v11',
// //center: [-79.4512, 43.6568],
// 	center: [5.17917, 52.10333],
// zoom: 7
// });

// var geocoder = new MapboxGeocoder({
// accessToken: mapboxgl.accessToken, limit:10, mode:'mapbox.places', types:'address' ,countries: 'nl'
// });

// map.addControl(geocoder);

// // After the map style has loaded on the page, add a source layer and default
// // styling for a single point.
// map.on('load', function() {
// // Listen for the `result` event from the MapboxGeocoder that is triggered when a user
// // makes a selection and add a symbol that matches the result.
// geocoder.on('result', function(ev) {

// 	lat = ev.result.center[1];
// 	lng = ev.result.center[0];
// 	place_name = ev.result.place_name;
// 	isAddressChanged = true;
// map.getSource('point').setData(ev.result.geometry);
// });
// });


// canvas = map.getCanvasContainer();

// var geojson = {
// "type": "FeatureCollection",
// "features": [{
// "type": "Feature",
// "geometry": {
// "type": "Point",
// "coordinates": [5.17917, 52.10333]
// }
// }]
// };

// function onMove(e) {
// var coords = e.lngLat;

// // Set a UI indicator for dragging.
// canvas.style.cursor = 'grabbing';

// // Update the Point feature in `geojson` coordinates
// // and call setData to the source layer `point` on it.
// geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
// map.getSource('point').setData(geojson);

// }

// function onUp(e) {

// var coords = e.lngLat;

// 	lat = e.lngLat.lat;
// 	lng = e.lngLat.lng;	
// 	isAddressChanged = true;

// // Print the coordinates of where the point had
// // finished being dragged to on the map.
// coordinates.style.display = 'none';
// //coordinates.innerHTML = 'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat;
// canvas.style.cursor = '';
// 	geocoder['eventManager']['limit'] = 1;	
// 	geocoder['options']['limit'] = 1;	
//    geocoder.query(lng+","+lat);


// // Unbind mouse/touch events
// map.off('mousemove', onMove);
// map.off('touchmove', onMove);
// 	geocoder['eventManager']['limit'] = 10;	
// 	geocoder['options']['limit'] = 10;	
// }

// map.on('load', function() {
// map.resize();
// // Add a single point to the map
// map.addSource('point', {
// "type": "geojson",
// "data": geojson
// });

// map.addLayer({
// "id": "point",
// "type": "circle",
// "source": "point",
// "paint": {
// "circle-radius": 10,
// "circle-color": "#3887be"
// }
// });

// // When the cursor enters a feature in the point layer, prepare for dragging.
// map.on('mouseenter', 'point', function() {
// map.setPaintProperty('point', 'circle-color', '#3bb2d0');
// canvas.style.cursor = 'move';
// });

// map.on('mouseleave', 'point', function() {
// map.setPaintProperty('point', 'circle-color', '#3887be');
// canvas.style.cursor = '';
// });

// map.on('mousedown', 'point', function(e) {
// // Prevent the default map drag behavior.
// e.preventDefault();

// canvas.style.cursor = 'grab';

// map.on('mousemove', onMove);
// map.once('mouseup', onUp);
// });

// map.on('touchstart', 'point', function(e) {
// if (e.points.length !== 1) return;

// // Prevent the default map drag behavior.
// e.preventDefault();

// map.on('touchmove', onMove);
// map.once('touchend', onUp);
// });

// });

// $(".mapboxgl-ctrl-geocoder input").prop("required", true);
	}


</script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
<script type="text/javascript" src="calendar/js/calendar.js"></script>
<script src="/app/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="/app/assets/js/pages/ui/notifications.js"></script>
<script src="/app/assets/js/parsley.min.js"></script>
<script src="/app/assets/js/nl.js"></script>
<script>
	//window.ParsleyValidator.setLocale('nl');
	window.Parsley.setLocale('nl');
	$("#teacherRegistration").parsley();


	var __counter = 0;
	var __fieldcounter = 1;
	// this function is added
	Date.prototype.setTimezoneOffset = function (minutes) {
		var _minutes;
		if (this.timezoneOffset == _minutes) {
			_minutes = this.getTimezoneOffset();
		} else {
			_minutes = this.timezoneOffset;
		}
		if (arguments.length) {
			this.timezoneOffset = minutes;
		} else {
			this.timezoneOffset = minutes = this.getTimezoneOffset();
		}
		return this.setTime(this.getTime() + (_minutes - minutes) * 6e4);
	};


	var calendar;
	var options;

	var hourlyCost = <?php echo (@$data['customhourly']) ? @$data['customhourly'] : 0; ?>;
	var std_lat = "<?php echo (@$_SESSION['std_lat']) ? @$_SESSION['std_lat'] : 30.00003434343; ?>";
	var std_lng = "<?php echo (@$_SESSION['std_lng']) ? @$_SESSION['std_lng'] : 72.00343333333; ?>";
	//var place_name = "<?php echo (@$_SESSION['std_place_name']) ? @$_SESSION['std_place_name'] : ''; ?>";
	var place_name = "";
	var calendar;
	var options;


	var _ddate = null;
	var _stime = null;
	var _etime = null;

	var __calender_date = null;


	$(document).ready(function () {
		$("body").on("focusout", "#calender_startTime", function () {
			var _stime = $("#calender_startTime").val();
			var __calender_date = $("#calender_startTime").find(':selected').attr("data-date");

			$("#interview_datetime").val(_stime);
			$("#interview_date").val(__calender_date);

		});

		$("#huisnummer2, #postcode2").on("change paste", function () {
			// console.log("IN THAT FIELD POSTAL");


			var houseNumber = $('#huisnummer2').val();

			// POSTALCODE 2012ES
			// HOUSE NUMBER 30
			var foo = $('#postcode2').val().split(" ").join("");
			if (foo.length > 0) {
				foo = foo.match(new RegExp('.{1,4}', 'g')).join(" ");
			}
			$('#postcode2').val(foo);


			if (houseNumber.length > 0 && houseNumber.length <= 4) {
				// console.log(houseNumber);
				var postalCode = $('#postcode2').val().replace(" ", "");

				if (postalCode.length >= 4 && postalCode.length <= 6) {
					// console.log(postalCode);

					$.ajax
					({
						type: "GET",
						url: "/api/Geo/Address?postalCode=" + postalCode + "&streetNumber=" + houseNumber,
						success: function (response) {
							$('#errorMessage2').css("display", "none");
							// console.log(obj);
							let street = response.streetName;
							let place = response.locality;

							$('#straatnaam2').val(street);
							$('#woonplaats2').val(place);

							let lat = response.latitude;
							let long = response.longitude;

							$('#lat2').val(lat);
							$('#lng2').val(long);

							let placeName = street + " " + response.streetNumber + ", " + response.postalCode + " " + place + " Nederland";
							$('#place_name2').val(placeName);
						},
						error: function (error) {
							console.log("error");
							console.log(error);

							$('#errorMessage2').css("display", "block");
							$('#straatnaam2').val("");
							$('#woonplaats2').val("");

							$('#lat2').val("");
							$('#lng2').val("");
							$('#place_name2').val("");
							$('#huisnummer2').val("");
						}
					});
				}
			}
		});


		$(".cal-month-day").dblclick(function (e) {
			e.preventDefault();
		});

		$(".page-loader-wrapper").css("display", "none");

		"use strict";
		<?php $url = "/api/Calendar/GetFreeSlotsConsultation"; ?>
		options = {
			//events_source: "student-ajaxcall.php?teacherid=<?php //echo @$_GET[tid]; ?>",
			events_source: '<?= $url ?>',
			view: 'month',
			tmpl_path: 'calendar/tmpls/docenten-registratie/',
			tmpl_cache: false,
			// day: '2013-03-21',
			// day: 'now',

			merge_holidays: false,
			display_week_numbers: false,
			weekbox: false,
			//shows events which fits between time_start and time_end
			show_events_which_fits_time: true,

			onAfterEventsLoad: function (events) {
				if (!events) {
					return;
				}
				var list = $('#eventlist');
				list.html('');

				$.each(events, function (key, obj) {
					//converting the timeZone to UTC
					obj.start = new Date(parseInt(obj.start)).setTimezoneOffset(0);
					obj.end = new Date(parseInt(obj.end)).setTimezoneOffset(0);

					$(document.createElement('li'))
						.html('<a href="' + obj.url + '">' + obj.title + '</a>')
						.appendTo(list);
				});
			},
			onAfterViewLoad: function (view) {
				$('.registration-calendar-header__title').text(this.getTitle());
				$('.btn-group button').removeClass('active');
				$('button[data-calendar-view="' + view + '"]').addClass('active');
			},
			classes: {
				months: {
					general: 'label'
				}
			}
		};

		calendar = $('#calendar').calendar(options);
		$('.btn-group button[data-calendar-nav]').click(function (e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			var $this = $(this);
			calendar.navigate($this.data('calendar-nav'));
			/*$this.click(function() {

			});*/
		});

		$('.btn-group button[data-calendar-view]').click(function (e) {
			e.preventDefault();
			e.stopImmediatePropagation();
			var $this = $(this);
			calendar.view($this.data('calendar-view'));
			/*$this.click(function() {

			});*/
		});

		$('#first_day').change(function () {
			var value = $(this).val();
			value = value.length ? parseInt(value) : null;
			//calendar.setOptions({first_day: value});
			//calendar.view();
		});

		$('#language').change(function () {
			calendar.setLanguage($(this).val());
			calendar.view();
		});

		$('#events-in-modal').change(function () {
			var val = $(this).is(':checked') ? $(this).val() : null;
			//  calendar.setOptions({modal: val});
		});
		$('#format-12-hours').change(function () {
			var val = $(this).is(':checked') ? true : false;
			calendar.setOptions({format12: val});
			calendar.view();
		});
		$('#show_wbn').change(function () {
			var val = $(this).is(':checked') ? true : false;
			calendar.setOptions({display_week_numbers: val});
			calendar.view();
		});
		$('#show_wb').change(function () {
			var val = $(this).is(':checked') ? true : false;
			calendar.setOptions({weekbox: val});
			calendar.view();
		});

		$('#events-modal .modal-header, #events-modal .modal-footer').click(function (e) {
			//e.preventDefault();
			//e.stopPropagation();
		});

		// $(".cal-month-day").on("click",function(){
		//     console.log($(this).find(".cal-slide-content").html());
		//
		// });


		$(".list-item").on("mouseover", function (e) {
			e.preventDefault();
			// console.log($(this).html());
		});

		function add_minutes(dt, minutes) {
			var add = new Date(dt.getTime() + minutes * 60000);
			if (add.getMinutes() == 0) {
				if (add.getHours() == 0) {
					return '00' + ':' + '00';
				} else {
					return add.getHours() + ':' + '00';
				}
			} else {
				if (add.getHours() == 0) {
					return '00' + ':' + add.getMinutes();
				} else {
					return add.getHours() + ':' + add.getMinutes();
				}
			}
		}

		function formatTime(dt) {
			var add = new Date(dt.getTime());
			if (add.getMinutes() == 0) {
				if (add.getHours() == 0) {
					return '00' + ':' + '00';
				} else {
					return add.getHours() + ':' + '00';
				}
			} else {
				if (add.getHours() == 0) {
					return '00' + ':' + add.getMinutes();
				} else {
					return add.getHours() + ':' + add.getMinutes();
				}
			}
		}

		// $("body").on("click",".cal-month-day",function(){
		//     __calender_date = $(this).find(".pull-right").attr("data-cal-date");
		// 	var timeArr = Global_Variable_timeRange[__calender_date];
		// 	var str = "";
		// 	var counter = 1;
		// 	for(var time in timeArr){
		// 		var endTimeNew = add_minutes(new Date("01/01/2007 " + time), 20).toString();

		// 		var timeFormated = formatTime(new Date("01/01/2007 " + time)).toString();

		// 		if(counter == 1){
		// 			str += "<option value='' selected>Selecteer</option>";
		// 		}
		// 		counter++;
		// 		if(timeArr[time] == "available")
		// 			str += "<option value='"+timeFormated+" - "+endTimeNew+"'>"+timeFormated+" - "+endTimeNew+"</option>";
		// 	}
		// 	setTimeout(function(){
		// 			$("#calender_startTime").html(str);
		// 			$("#calender_startTime").change();
		// 			$(".updateButton").removeClass("_hide");
		// 			}, 250);

		// });


		// $("body").on("change", "#calender_startTime",function(){
		// 	var stime = $("#calender_startTime").val();
		// 	var timeArr = Global_Variable_timeRange[__calender_date];
		// 	var slotDiff= <?php echo 20 * 60 * 1000; //millisec ?>;

		// 	//adding slot_time to get end_time
		// 	var tempTime = new Date("01/01/2007 " + stime);
		// 	tempTime = new Date(tempTime.getTime() + slotDiff);
		// 	//tempTime = tempTime.getHours()+":"+tempTime.getMinutes();
		// 	//tempTime = formatAMPM(tempTime);


		// 	//adding slot_time to get end_time

		// 	var str = "";

		// 	for(var time in timeArr){
		// 		var t =  new Date("01/01/2007 " + time);

		// 		if(timeArr[time] == "available" && tempTime.getTime() <= t.getTime())
		// 			str += "<option value='"+time+"'>"+time+"</option>";
		// 	}
		// 	$("#calender_endTime").html(str);
		// });

	}); //main $()


	function removeButton(obj) {
		$(obj).find(".inline-elem-buttons-box").remove();
	}


	function addButton(obj, id) {
		// console.log("addButton : ", obj);
		var cases = $(obj).attr("data-event");
		// if(cases == "empty"){
		//     $(obj).append('');
		// }
		$(obj).append('<span class="inline-elem-buttons-box">\
   <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, ' + id + ')">Cancel Slot</button>\
</span>');

	}// addButton function


	function cancelSlot(obj, id) {
		$.ajax({
			type: "POST",
			url: "Admin/admin-ajaxcalls.php",
			data: {subpage: "slotCancellation", cbID: id},
			dataType: "json",
			success: function (response) {
				if (response == "success") {
					// console.log(response);
					showNotification("alert-success", "Je verzoek is geanuleerd", "bottom", "center", "", "");
					interviewBookingValidation(--__counter);
				} else {
					showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen", "bottom", "center", "", "");
				}
				$(document.body).css({'cursor': 'default'});
				$("#submitBtn").prop("disabled", false);
				calendar = $('#calendar').calendar(options);
			},
			error: function (xhr, ajaxOptions, thrownError) {
				if (xhr.responseText == "success") {
					// console.log(xhr.responseText);
					showNotification("alert-success", "Je verzoek is geanuleerd", "bottom", "center", "", "");
					interviewBookingValidation(--__counter);
				} else {
					showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen", "bottom", "center", "", "");
				}
				$(document.body).css({'cursor': 'default'});
				$("#submitBtn").prop("disabled", false);
				calendar = $('#calendar').calendar(options);

			}
		});// ends ajax
	}

	/*

   function booking(){

		$(document).css({'cursor' : 'wait'});
		$("#submitBtn").prop("disabled", true);

	   var slotData = "";
	   var time = "";
	   var date = "";
	   $("#cal-slide-content").find(".inline-elem-check").each(function(index, obj){
		   if($(this).prop("checked")){
			 var time = $(this).attr("data-time");
			 var date = $(this).attr("data-date");
			 slotData += date+","+time+"&";

		   }
	   });


		 slotData = slotData.substring(0, slotData.length-1);

		 $.ajax({
		   type: "POST",
		   url: "Admin/admin-ajaxcalls.php",
		   data: { subpage:"slotBooking", teacherid: teacherid, slotData:slotData, hourlyCost:hourlyCost, std_lat:std_lat, std_lng:std_lng, place_name:place_name},
		   dataType: "json",
		   success: function(response) {
						 if(response == "success"){
							 console.log(response);
							 showNotification("alert-success", "Je verzoek is verzonden", "bottom", "center", "", "");
								calendar = $('#calendar').calendar(options);
							 interviewBookingValidation(++__counter);
						 }else if(response == "Maximum One Booking can be made."){
							 showNotification("alert-danger", "Je kan maximaal 1 gesprek inplannen", "bottom", "center", "", "");
						 }else{
							showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen", "bottom", "center", "", "");
						 }
						 $(document.body).css({'cursor' : 'default'});
						 $("#submitBtn").prop("disabled", false);
					 },
		   error: function(xhr, ajaxOptions, thrownError) {
						 if(xhr.responseText == "success"){
							 console.log(xhr.responseText);
							 showNotification("alert-success", "Je verzoek is verzonden", "bottom", "center", "", "");
								calendar = $('#calendar').calendar(options);
							  interviewBookingValidation(++__counter);
						 }else if(xhr.responseText == "Maximum One Booking can be made."){
							 showNotification("alert-danger", "Je kan maximaal 1 gesprek inplannen", "bottom", "center", "", "");
						 }else{
							showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen", "bottom", "center", "", "");
						 }
						 $(document.body).css({'cursor' : 'default'});
						 $("#submitBtn").prop("disabled", false);

						 }
		 });// ends ajax

	 }// func ends

   */

	//dummy function

	function __onclick(obj) {
		return 1;
	}

	function interviewBookingValidation(count) {
		if (count > 0)
			$("#interview").val(count);
		else
			$("#interview").val("");
	}

	function formatAMPM(date) {
		var hours = date.getHours();
		var minutes = date.getMinutes();
		var ampm = hours >= 12 ? 'PM' : 'AM';
		hours = hours % 12;
		hours = hours ? hours : 12; // the hour '0' should be '12'
		hours = hours < 10 ? '0' + hours : hours;
		minutes = minutes < 10 ? '0' + minutes : minutes;
		var strTime = hours + ':' + minutes + ' ' + ampm;
		return strTime;
	}
</script>
</body>
</html>
<?php
//}
?>





