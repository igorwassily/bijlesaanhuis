<?php
$thisPage = "Students Registration";
/*session_start();
if(!isset($_SESSION['Admin']))
{ 
	header('Location: index.php');
}
else
{*/
?>

    <!doctype html>
    <html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description"
              content="Door in te loggen, boek je direct de bijlesdocent naar jouw wensen | Laat je gerust eerst adviseren door onze specialisten.">
        <title>Registreren | Bijles Aan Huis</title>
        <?php
        require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
        ?>
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
              rel="stylesheet"/>
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">

        <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet'/>
        <link rel='stylesheet'
              href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css'
              type='text/css'/>
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <style type="text/css">

            .mapboxgl-ctrl-attrib, .mapboxgl-ctrl-logo {
                display: none !important;
            }

            .mapboxgl-ctrl-geocoder li > a {
                color: black !important;
            }

            .mapboxgl-ctrl-geocoder input {
                color: black !important
            }

            #map {
                position: absolute;
                top: 0;
                bottom: 0;
                width: 100%;
                transition: all 0.3s;
            }

            .button {
                color: #fff;
                background-color: #555;
                padding: 1em;
                margin: 1em;
                position: absolute;
                right: 1em;
                top: 1em;
                border-radius: 0.5em;
                border-bottom: 2px #222 solid;
                cursor: pointer;
            }

            #resizeMap {
                top: 5em;
            }

            /*Placeholder Color */
            input {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            select {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            input:focus {
                background: transparent !Important;
            }

            select:focus {
                background: transparent !Important;
            }

            .wizard .content {
                overflow: visible !important;
                /*height: 370px !important; */
                min-height: 500px !important;
                margin-top: 50px !important;
            }

            .wizard .content label {

                color: #486066 !important;
                font-weight: bold;
            }

            .wizard > .steps .current a {
                background-color: #5CC75F !Important;
                border-radius: 6px;
            }

            .wizard > .steps .done a {
                background-color: #E8E8E8 !Important;
                border-radius: 6px;
            }

            .wizard .steps a, .wizard .steps a:hover {
                border-radius: 6px;
            }

            .wizard > .actions a {
                background-color: #029898 !Important;
            }

            .wizard > .actions .disabled a {
                background-color: #FAFBFC !important;
            }


            .btn.btn-simple {
                border-color: white !important;
            }

            .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
                color: white;
            }

            .navbar.p-l-5.p-r-5 {
                display: none !important;
            }

            .mapboxgl-canvas {
                position: absolute;
                width: 610px;
                height: 200px;
            }

            .wizard .content label.error {
                color: #e45e5e !important;
            }

            #-error {
                display: block;
                margin-top: 223px;
            }

            }
            section.content {
                padding-top: 29px;
            }

            #fc_frame, #fc_frame.fc-widget-normal {
                bottom: 100px !important;
            }

            .custom-error label.error {
                color: #e45e5e !important;
                position: inherit;
                top: -21px;

            }

            #-error {
                float: right;
            }

            .mapboxgl-canvas {
                left: 0;
            }

            .theme-green section.content:before {
                background-color: transparent !important;
            }


        </style>
        <?php
        $activePage = basename($_SERVER['PHP_SELF']);
        ?>
    </head>
    <body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                     alt="Oreo"></div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>


    <?php
    require_once('includes/header.php');

    //require_once('includes/sidebar.php');
    ?>

    <style> .mapboxgl-ctrl-geocoder li > a {
            color: black !important;
        }</style>
    <!-- Main Content -->
    <section class="content" style="margin: 0px; padding-top:29px; position: relative !important;">
        <div class="block-header">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-12">
                    <h2>De eerste stap richting goede begeleiding!
                    </h2>
                </div>
                <!-- <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                     <ul class="breadcrumb float-md-right">
                         <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i>&nbsp;&nbsp;&nbsp;Back to Home</a></li>

                     </ul>
                 </div>-->
            </div>
        </div>
        <div class="container-fluid">
            <div class="row content clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12">


                    <form id="studentRegistration" enctype="multipart/form-data">

                        <h4>Account aanmaken</h4>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-4"></div>
                                <div class="col-sm-4">
                                    <div class="form-group form-float">
                                        <!--<label for="username">Username</label>-->
                                        <input type="hidden" class="form-control" placeholder="User Group" name="choice"
                                               id="choice" value="Student-Registration"/>
                                        <input type="hidden" class="form-control" placeholder="User Group"
                                               name="usergroup" id="usergroup" value="Student"/>
                                        <input type="hidden" class="form-control" placeholder="Username" name="username"
                                               id="username"/>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for="username">E-mailadres *</label>
                                        <input type="email" class="form-control" placeholder=" E-mailadres" name="email"
                                               id="email" required/>
                                        <input type="hidden" name="checkEmail" id="checkEmail" value=""/>
                                    </div>


                                    <div class="form-group form-float">
                                        <label for="username">Wachtwoord *</label>
                                        <input type="password" class="form-control" placeholder="Wachtwoord"
                                               name="password" id="password" minlength="8" required/>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for="username">Wachtwoord bevestigen *</label>
                                        <input type="password" class="form-control" placeholder="Wachtwoord bevestigen"
                                               name="confirm_password" id="confirm_password" minlength="8" required/>
                                    </div>
                                    <div class="form-group form-float">
                                        <label for="enfants">Telefoonnummer</label>
                                        <input type="text" class="form-control" placeholder="Telefoonnummer"
                                               name="telephone" id="telephone" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                        </fieldset>

                        <h4>Persoonlijke gegevens</h4>
                        <fieldset>
                            <div class="row">
                                <div class="col-sm-3"></div>
                                <div class="col-sm-6">

                                    <div class="form-group form-float">
                                        <label for="booking-code">Voornaam leerling *</label>
                                        <input type="text" class="form-control" placeholder="Voornaam leerling"
                                               name="first-name" id="first-name" required>

                                    </div>


                                    <div class="form-group form-float">
                                        <label for="booking-code">Achternaam leerling *</label>
                                        <input type="text" class="form-control" placeholder="Achternaam leerling"
                                               name="last-name" id="last-name" required>

                                    </div>

                                    <!--
                                                                    <div class="form-group form-float">
                                                                        <label for="booking-code">Address</label>
                                                                        <input type="text" class="form-control" placeholder="Address" name="address" id="address">

                                                                    </div>


                                                                    <div class="form-group form-float">
                                                                        <div class="row">
                                                                         <div class="col-lg-3 col-md-3 col-sm-12">
                                                                             <label for="adults">Postal Code</label>
                                                                            <input type="text" class="form-control" placeholder="Postal Code" name="postal-code" id="postal-code">
                                                                        </div>

                                                                         <div class="col-lg-3 col-md-3 col-sm-12">
                                                                            <label for="children">City</label>
                                                                        <input type="text" class="form-control" placeholder="City" name="city" id="city">
                                                                        </div>

                                                                         <div class="col-lg-3 col-md-3 col-sm-12">
                                                                             <label for="enfants">Telephone</label>
                                                                        <input type="text" class="form-control" placeholder="Telephone" name="telephone" id="telephone">
                                                                        </div>
                                                                             <div class="col-lg-3 col-md-3 col-sm-12">
                                                                             <label for="enfants">Country</label>
                                                                        <input type="text" class="form-control" placeholder="Country" name="country" id="country">
                                                                        </div>
                                                                        </div>

                                                                    </div>

                                    -->
                                    <!--
                                        <div class="row">
                                            <div class="col-lg-12 col-md-12 col-sm-12 form-group form-float">
                                                     <label for="enfants">Telephone *</label>
                                                <input type="text" class="form-control" placeholder="Telephone" name="telephone" id="telephone" required>
                                                </div>

                                        </div>
    -->

                                    <div class="row">

                                        <div class="col-sm-12">
                                            <div class="form-group form-float">
                                                <label for="booking-code">Voornaam ouder *</label>
                                                <input type="text" class="form-control" placeholder="Voornaam ouder"
                                                       name="parent-first-name" id="parent-first-name" required>

                                            </div>


                                            <div class="form-group form-float">
                                                <label for="booking-code">Achternaam ouder *</label>
                                                <input type="text" class="form-control" placeholder="Achternaam ouder"
                                                       name="parent-last-name" id="parent-last-name" required>

                                            </div>

                                            <div class="">
                                                <div class="row">

                                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group form-float">
                                                        <label for="parent-email">Ouderlijk e-mailadres *</label>
                                                        <input type="email" class="form-control"
                                                               placeholder="E-mailadres" name="parent-email"
                                                               id="parent-email" required/>
                                                    </div>

                                                </div>

                                            </div>
                                        </div>

                                        <label for="maps" style="margin-left: 17px;">Adres waar de bijles moet
                                            plaatsvinden *</label>
                                    </div>
                                    <div class="row" style="width:100%;height:220px">
                                        <div class="col-lg-12 col-md-12 col-sm-12 form-group form-float custom-error">
                                            <input type="hidden" class="form-control" name="lat" id="lat">
                                            <input type="hidden" class="form-control" name="lng" id="lng">
                                            <input type="hidden" class="form-control" name="place_name" id="place_name">
                                            <div id='map'
                                                 style="position:absolute; top: 0;bottom: 0;width: 100%; height: 100%; overflow:visible;"></div>
                                        </div>
                                    </div>
                                    <pre id='coordinates' class='coordinates' style="display:none;"></pre>

                                    <!--<div class="form-group form-float">
                                         <label for="agent">Choose Arrival By</label>
                                       <select class="form-control show-tick" id="booking-arrival-by" name="booking-arrival-by">
                                        <option value="">-- Please select --</option>
                                        <option value="aeroplane">Aeroplane</option>
                                        <option value="bus">Bus</option>

                                    </select>
                                    </div>-->

                                </div>
                            </div>


                        </fieldset>


                    </form>


                </div>
            </div>
            <!-- #END# Basic Examples -->
        </div>
    </section>
    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

    <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
    <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>

    <script>


        var lat = 52.1326, lng = 5.2913;
        var place_name = "";
        var isMapInitialise = true;
        var map = null;
        var canvas = null;


        function initMap() {


            mapboxgl.accessToken = "<?php echo ACCESS_TOKEN; ?>";
            map = new mapboxgl.Map({
                container: 'map',
                style: 'mapbox://styles/mapbox/streets-v11',
                center: [5.55, 52.31667],
                zoom: 7
            });

            var geocoder = new MapboxGeocoder({
                accessToken: mapboxgl.accessToken,
                limit: 10,
                mode: 'mapbox.places',
                countries: 'nl',
                placeholder: 'Zoeken'
            });

            map.addControl(geocoder);

// After the map style has loaded on the page, add a source layer and default
// styling for a single point.
            var biggerSmaller;
            map.on('load', function () {

                map.resize();


// Listen for the `result` event from the MapboxGeocoder that is triggered when a user
// makes a selection and add a symbol that matches the result.
                geocoder.on('result', function (ev) {
                    console.log("Here: ", ev);
                    lat = ev.result.center[1];
                    lng = ev.result.center[0];
                    place_name = ev.result.place_name;
                    isAddressChanged = true;
                    map.getSource('point').setData(ev.result.geometry);
                });
            });


            canvas = map.getCanvasContainer();

            var geojson = {
                "type": "FeatureCollection",
                "features": [{
                    "type": "Feature",
                    "geometry": {
                        "type": "Point",
                        "coordinates": [5.2913, 52.1326]
                    }
                }]
            };

            function onMove(e) {
                var coords = e.lngLat;

// Set a UI indicator for dragging.
                canvas.style.cursor = 'grabbing';

// Update the Point feature in `geojson` coordinates
// and call setData to the source layer `point` on it.
                geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
                map.getSource('point').setData(geojson);

            }

            function onUp(e) {

                var coords = e.lngLat;
                console.log(e.lngLat);
                lat = e.lngLat.lat;
                lng = e.lngLat.lng;
                isAddressChanged = true;

// Print the coordinates of where the point had
// finished being dragged to on the map.
                coordinates.style.display = 'block';
//coordinates.innerHTML = 'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat;
                canvas.style.cursor = '';
                geocoder['eventManager']['limit'] = 1;
                geocoder['options']['limit'] = 1;
                geocoder.query(lng + "," + lat);


// Unbind mouse/touch events
                map.off('mousemove', onMove);
                map.off('touchmove', onMove);
                geocoder['eventManager']['limit'] = 10;
                geocoder['options']['limit'] = 10;
            }

            map.on('load', function () {

// Add a single point to the map
                map.addSource('point', {
                    "type": "geojson",
                    "data": geojson
                });

                map.addLayer({
                    "id": "point",
                    "type": "circle",
                    "source": "point",
                    "paint": {
                        "circle-radius": 10,
                        "circle-color": "#3887be"
                    }
                });

// When the cursor enters a feature in the point layer, prepare for dragging.
                map.on('mouseenter', 'point', function () {
                    map.setPaintProperty('point', 'circle-color', '#3bb2d0');
                    canvas.style.cursor = 'move';
                });

                map.on('mouseleave', 'point', function () {
                    map.setPaintProperty('point', 'circle-color', '#3887be');
                    canvas.style.cursor = '';
                });

                map.on('mousedown', 'point', function (e) {
// Prevent the default map drag behavior.
                    e.preventDefault();

                    canvas.style.cursor = 'grab';

                    map.on('mousemove', onMove);
                    map.once('mouseup', onUp);
                });

                map.on('touchstart', 'point', function (e) {
                    if (e.points.length !== 1) return;

// Prevent the default map drag behavior.
                    e.preventDefault();

                    map.on('touchmove', onMove);
                    map.once('touchend', onUp);
                });

            });

            $(".mapboxgl-ctrl-geocoder input").prop("required", true);
        }
    </script>


    <!-- Jquery steps Wizard Code -->
    <script type="text/javascript">
        $(function () {

            $(window).keydown(function (event) {
                if (event.keyCode == 13) {
                    event.preventDefault();
                    return false;
                }
            });

            //Advanced form with validation
            var form = $('#studentRegistration').show();
            form.steps({
                headerTag: 'h4',
                bodyTag: 'fieldset',

                onInit: function (event, currentIndex) {
                    //  $.AdminOreo.input.activate();
                    //Set tab width
                    var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
                    var tabCount = $tab.length;
                    $tab.css('width', (100 / tabCount) + '%');

                    //set button waves effect
                    setButtonWavesEffect(event);
                },
                onStepChanging: function (event, currentIndex, newIndex) {
                    if (newIndex == 1 && isMapInitialise) {
                        initMap();
                        isMapInitialise = false;
                    }

                    if (currentIndex > newIndex) {
                        isMapInitialise = false;
                        return true;
                    }

                    if (currentIndex < newIndex) {
                        isMapInitialise = false;
                        form.find('.body:eq(' + newIndex + ') label.error').remove();
                        form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
                    }


                    form.validate().settings.ignore = ':disabled,:hidden';
                    return form.valid();
                },
                onStepChanged: function (event, currentIndex, priorIndex) {
                    //Jquery AJAX code to add client upon  insertion

                },
                onFinishing: function (event, currentIndex) {
                    form.validate().settings.ignore = ':disabled';
                    return form.valid();
                },
                onFinished: function (event, currentIndex) {
                    //Code for Forms Data Submission
                    $("#lat").val(lat);
                    $("#lng").val(lng);
                    $("#place_name").val(place_name);
                    console.log("im here");


                    run_waitMe($('body'), 1, "timer");
                    $.ajax({
                        url: 'actions/add_scripts.php',
                        type: 'POST',
                        data: new FormData(this),
                        contentType: false,
                        cache: false,
                        processData: false,
                        success: function (result) {


                            switch (result) {

                                case "Error":


                                    setTimeout(function () {

                                        $('body').waitMe('hide');
                                        $('#studentRegistration')[0].empty();
                                        showNotification("alert-danger", "Sorry!!! Your account cannot be created right now.", "top", "center", "", "");
                                        //$("#error-epm").html("There Was An Error Inserting The Record.");

                                    }, 500);
                                    break;

                                case "Success":


                                    setTimeout(function () {

                                        $('body').waitMe('hide');
                                        $('#studentRegistration')[0].reset();
                                        showNotification("alert-success", "Kindly, check your email to activate your account", "top", "center", "", "");


                                    }, 500);
                                    window.location.replace("<?php echo SITE_URL;?>student-thankyou");
                                    break;


                            }
                        }
                    });

                }
            });

            $.validator.addMethod("notEqual", function (value, element, param) {
                return this.optional(element) || value != $(param).val();
            }, "Dit e-mailadres wordt al gebruikt");


            $.validator.addMethod("telephone", function (value, element) {
                return this.optional(element) || /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(value);
            }, "Invalid Number");

            form.validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                },
                rules: {
                    confirm_password: {
                        equalTo: '#password'
                    },
                    "select-course": "required",
                    email:
                        {
                            notEqual: '#checkEmail',
                        },
                    telephone: {
                        telephone: "#telephone"
                    },
                    'parent-telephone': {
                        telephone: "#parent-telephone"
                    }
                }
            });

            $("#email").keyup(function () {
                var email = $("#email").val();
                var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                if (regex.test(email)) {
                    $.ajax({
                        url: 'actions/email.php',
                        data: {'email': email},
                        type: 'POST',
                        success: function (result) {
                            $("#checkEmail").val(result);
                        }
                    });
                }
            });

        });

        function setButtonWavesEffect(event) {
            $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
            $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
        }

        function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
            if (colorName === null || colorName === '') {
                colorName = 'bg-black';
            }
            if (text === null || text === '') {
                text = 'Turning standard Bootstrap alerts';
            }
            if (animateEnter === null || animateEnter === '') {
                animateEnter = 'animated fadeInDown';
            }
            if (animateExit === null || animateExit === '') {
                animateExit = 'animated fadeOutUp';
            }
            var allowDismiss = true;

            $.notify({
                    message: text
                },
                {
                    type: colorName,
                    allow_dismiss: allowDismiss,
                    newest_on_top: true,
                    timer: 1000,
                    placement: {
                        from: placementFrom,
                        align: placementAlign
                    },
                    animate: {
                        enter: animateEnter,
                        exit: animateExit
                    },
                    template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
                        '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
                        '<span data-notify="icon"></span> ' +
                        '<span data-notify="title">{1}</span> ' +
                        '<span data-notify="message">{2}</span>' +
                        '<div class="progress" data-notify="progressbar">' +
                        '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
                        '</div>' +
                        '<a href="{3}" target="{4}" data-notify="url"></a>' +
                        '</div>'
                });
        }

    </script>

    <script>
        /* Condition for Arrival & Departure By Value */
        $(document).ready(function () {

            $("#copy-address").change(function () {
                if (this.checked) {


                    // var address = $("#address").val();
                    //var postalcode = $("#postal-code").val();
                    //var city = $("#city").val();
                    var telephone = $("#telephone").val();
                    //	var country = $("#country").val();
                    $("#parent-address").val(address);
                    //$("#parent-postal-code").val(postalcode);
                    //$("#parent-city").val(city);
                    $("#parent-telephone").val(telephone);
                    //$("#parent-country").val(country);
                }
            });
        });

    </script>


    <?php
    require_once('includes/footerScriptsAddForms.php');
    ?>
    <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
    </body>
    </html>
<?php
//}
?>