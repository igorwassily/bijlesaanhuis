<?php

use API\Endpoint\Calendar;

require_once('includes/connection.php');
require_once('../../api/endpoint/Calendar.php');
$thisPage = "Students Dashboard";
session_start();


if (!isset($_SESSION['StudentID'])) {
    header('Location: index.php');
} else {


    function getDutchMonth($mon)
    {
        $months = array(
            "Jan" => "Jan",
            "Feb" => "Feb",
            "Mar" => "Mrt",
            "Apr" => "Apr",
            "May" => "Mei",
            "Jun" => "Jun",
            "Jul" => "Jul",
            "Aug" => "Aug",
            "Sep" => "Sept",
            "Sept" => "Sept",
            "Oct" => "Okt",
            "Nov" => "Nov",
            "Dec" => "Dec"
        );
        return $months[$mon];
    }

    ?>

    <!doctype html>
    <html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description"
              content="Een overzicht van alle geboekte bijlessen in je kalender | Bijles Aan Huis; kwaliteit, innovatie en betrouwbaarheid.">
        <title>Kalender | Bijles Aan Huis</title>
        <?php
        require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
        ?>
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
              rel="stylesheet"/>
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet'/>
        <link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print'/>
        <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">

        <!-- New Calendar Style -->
        <link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="calendar/css/calendar.css">
        <link rel="stylesheet" href="assets/css/nigel.css">


        <style type="text/css">
            section.content {
                margin: 60px 60px 45px 60px;
            }

            .page-calendar .b-primary {
                border-color: #5CC75F !important;
            }

            .b-warning {
                border-color: #ffbd6b !important;
            }

            #calendar {
                background-color: #fafafa !important;
            }

            .cal-month-day {
                color: black;
            }

            .cal-row-head {
                background-color: #fafafa !important;
            }

            .empty {
                background-color: white !important;
                border: 2px solid black;
            }

            .inline_time {
                color: #50d38a !important;
            }

            #cal-slide-content, .cal-day-outmonth {
                font-family: inherit;
                color: #bdbdbd !important;
            }

            .page-header {
                height: 0 !important;
                padding-bottom: 0px;
                margin: 0 0 20px !important;
                border-bottom: 0px;
            }
        </style>
        <!-- New Calendar Style -->

        <style type="text/css">

            /*Placeholder Color */
            input {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            select {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            input:focus {
                background: transparent !Important;
            }

            select:focus {
                background: transparent !Important;
            }

            .wizard .content {
                /*overflow-y: hidden !important;*/
            }

            .wizard .content label {

                color: white !important;

            }

            .wizard > .steps .current a {
                background-color: #029898 !Important;
            }

            .wizard > .steps .done a {
                background-color: #828f9380 !Important;
            }

            .wizard > .actions a {
                background-color: #029898 !Important;
            }

            .wizard > .actions .disabled a {
                background-color: #eee !important;
            }

            .btn.btn-simple {
                border-color: white !important;
            }

            .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
                color: white;
            }

            table {
                color: white;
            }

            .multiselect.dropdown-toggle.btn.btn-default {
                display: none !important;
            }

            .card .body {
                background-color: #FAFBFC !important;
            }

            div#modal-delete-type > div.modal-dialog {
                margin-top: 119px !important;
            }

            .calender_dropdownTime {
                display: none;
            }

            /* extra popup modal styling */
            .modal h6 {
                text-transform: lowercase;
            }

            .modal .modal-title {
                font-size: 18px !important;
                font-weight: bold !important;
            }

            .modal .modal-text {
                padding: 0 20px 27px 20px;
            }

            .modal-content .modal-header button {
                right: 12px !important;
                top: 6px !important;
            }

            .custom-padding {
                padding: 0 0 1rem 0 !important;
                margin-top: 1rem;
            }

            .kalendar-left-title {
                overflow: initial !important;
            }

            body .kalendar-left .header .page-header .kalendar-left-title h3 {
                text-transform: initial !important;
                margin-left: 0;
            }

            body .kalendar-left .header {
                padding: 20px
            }

            body .kalendar-right .header {
                padding: 20px;
            }

            @media (min-width: 992px) {
                body .kalendar-right {
                    padding-left: 0;
                }
            }

            @media (min-width: 992px) and (max-width: 1100px) {
                body .kalendar-right .header .ma-subtitle {
                    margin-left: -9%;
                    margin-right: -9%;
                    text-align: center;
                }
            }

            @media (max-width: 991px) {
                body .kalendar-right {
                    padding-left: 15px;
                }
            }

            .kalendar-body-title {
                padding-bottom: 17px;
            }

            .colorLegend {
                position: relative;
                min-height: 36px;
            }

            .colorLegend .colorLegend--item {
                width: 120px;
            }

            .colorLegend--color-title {
                position: absolute;
                margin-left: 5px;
            }

            .main-row {
                display: flex !important;
                margin-bottom: 25px;
            }

            .kalendar-left {
                flex: 1 1 0;
            }

            @media (max-width: 991px) {
                .main-row .kalendar-left, .main-row .kalendar-right {
                    margin-left: 15px;
                    margin-right: 15px;
                    padding: 0;
                }

                .kalendar-left {
                    flex: none;
                }
            }

            .row.main-row, .row.w-100 {
                display: block;
                max-width: 100%;
                /* margin-right: auto;
				margin-left: auto; */
                /* padding-left: 20px;
				padding-right: 20px; */
                box-sizing: border-box;
            }

            /* !!!BEWARE!!!: kalendar-right.card instead of kalendar-right .card */
            body .kalendar-left .card, body .kalendar-right.card {
                padding: 0;
                border: 2px solid #F1F1F1;
                border-top-left-radius: 6px;
                border-top-right-radius: 6px;
                height: auto;
                overflow: hidden;
                background: transparent;
            }

            .kalendar-right.card .body {
                overflow: auto !important;
                border-bottom: 2px solid #F1F1F1;
            }

            .calendar-month-buttons {
                position: absolute;
                right: 0;
                bottom: 0;
            }

            .btn.btn--save {
                background: #5CC75F !important;
                border: solid 2px #5CC75F !important;
                color: #fafbfc !important;
                transition-delay: 0s;
                transition-duration: 0.2s;
                transition-property: all;
                font-weight: 600 !important;
                font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important;
                text-transform: uppercase !important;
                border-radius: 6px !important;
                padding: 15px 20px;
                width: 200px !important;
                margin-top: 20px !important;
                margin-bottom: 20px !important;
            }

            @media screen and (max-width: 576px) {
                section.content {
                    margin: 0 !important;
                }
            }

            @media (max-width: 768px) {
                #mobile-halve .dashboard-list{
                    height: 300px !important;
                }
            }
            .dashboard-content-container {
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
                margin: 0 5%;
            }
            .dashboard-content__section {
                float: none;
                background-color: white;
                border-radius: 6px;
                margin-bottom: 30px;
                border: 1px #f1f1f1 solid;
            }
            section.dashboard-content .dashboard-header {
                background-color: #f1f1f1;
                border-radius: 6px 6px 0 0;
            }
            .dashboard-content__section--cal .dashboard-header {
                padding: 0 15px 10px 15px;
            }
        </style>
        <?php
        $activePage = basename($_SERVER['PHP_SELF']);
        ?>
    </head>
    <body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                     alt="Oreo"></div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>


    <?php
    require_once('includes/header.php');
    ?>

    <!-- Main Content -->
    <section class="content page-calendar dashboard-content">

        <div class="dashboard-content-container">
            <div class="dashboard-content__section dashboard-content__section--cal">
                <div class="dashboard-header">
                    <div class="dashboard-header__titles">
                        <h2 class="ma-subtitle dashboard-header__heading">Kalender</h2><h3 class="ma-subtitle dashboard-header__heading"></h3>
                    </div>
                    <div class="colorLegend">
                        <p class="colorLegend-p"><span class="color request"></span> Bijlesverzoek</p>
                        <p class="colorLegend-p"><span class="color appointment"></span>Afspraak</p>
                    </div>
                    <div class="pull-right form-inline">
                        <div class="btn-group">
                            <button class="btn btn-primary" data-calendar-nav="prev"><<</button>
                            <button class="btn btn-default" data-calendar-nav="today">Today</button>
                            <button class="btn btn-primary" data-calendar-nav="next">>></button>
                        </div>
                    </div>
                </div>
                <div class="dashboard-calendar">
                    <div id="calendar"></div>
                </div>
            </div>
            <?php                         
                $cal = new Calendar($db, ['studentID' => $_SESSION['StudentID']]);
                $result = $cal->GetAppointments();
                $outputArray = $result->getResult();
                foreach ($outputArray as $key => $value) { 
                    if (isset($value['pending']) && $value['pending']) {
                        $data = true;
                    }
                    if (isset($value['accepted']) && $value['accepted']) { 
                        $data = true;
                    }
                } 
            ?>
            <div class="dashboard-content__section dashboard-content__section--list" <?php if (!isset($data)){?> id="mobile-halve" <?php } ?>>
                <div class="dashboard-header">
                    <h4 class="ma-subtitle">Toekomstige afspraken</h4>
                </div>
                <div class="body dashboard-list">
                    <div id="reload">
                        <?php
                        $limit = 3;
                        $i = 0;

                        $data = false;
                        foreach ($outputArray as $key => $value) {
                            if ($i > $limit) {
                                echo "<hr/> <p style='text-align:right;'> <a style='color:white !important; background:#5CC75F;' class='btn future-btn' href='teachers-appointment' >Toon alle</a></p>";
                                break;
                            }
                            $year = DateTime::createFromFormat('Y-m-d', $key)->format('Y');
                            $m = DateTime::createFromFormat('Y-m-d', $key)->format('M');
                            $day = DateTime::createFromFormat('Y-m-d', $key)->format('d');
                            if ($value['pending']) {
                                foreach ($value['pending'] as $key2 => $value2) {
                                    $data = true;
                                    $userID = isset($value2['userID']) ? $value2['userID'] : '';
                                    $forename = isset($value2['forename']) ? $value2['forename'] : '';
                                    $surname = isset($value2['surename']) ? $value2['surename'] : '';
                                    $begin = isset($value2['begin']) ? $value2['begin'] : '';
                                    $end = isset($value2['end']) ? $value2['end'] : '';
                                    $address = isset($value2['address']) ? $value2['address'] : '';
                                    $email = isset($value2['email']) ? $value2['email'] : '';
                                    $telephone = isset($value2['telephone']) ? $value2['telephone'] : '';
                                    echo '<hr id="h" /><div id="cl" class="event-name b-warning row">
												 
											 <div class="col-2 p-0 m-0 text-center">
											 	<h4>' . $day . '<span>' . $m . '</span><span>' . $year . '</span></h4>
											 </div>
											 <div class="col-10 p-0 m-0 ">
												 <h6>' . "<a href='teacher-detailed-view?tid={$userID}'>{$forename}  {$surname}</a>" . '</h6>
												 <address><i class="zmdi zmdi-time"></i> ' . $begin . ' - ' . $end . '</address>
												 <address><i class="zmdi zmdi-phone"></i> ' . $telephone . '</address>
												 <address><i class="zmdi zmdi-email"></i> ' . $email . '</address> 
												 <address style="display:none;"><i class="zmdi zmdi-pin"></i>' . $address . '</address>
											 
											 </div>
										 </div>';
                                    $i++;
                                }
                            }
                            if (isset($value['accepted']) && $value['accepted']) {
                                foreach ($value['accepted'] as $key2 => $value2) {
                                    $data = true;
                                    echo '<hr id="h" /><div id="cl" class="event-name b-primary row">
										 
									 <div class="col-2 p-0 m-0 text-center">
									 	<h4>' . $day . '<span>' . $m . '</span><span>' . $year . '</span></h4>
									 </div>
									 <div class="col-10 p-0 m-0 ">
										 <h6>' . "<a href='teacher-detailed-view?tid={$value2['userID']}'>{$value2['forename']}  {$value2['surename']}</a>" . '</h6>
										 <address><i class="zmdi zmdi-time"></i> ' . $value2['begin'] . ' - ' . $value2['end'] . '</address>
										 <address><i class="zmdi zmdi-phone"></i> ' . $value2['telephone'] . '</address>
										 <address><i class="zmdi zmdi-email"></i> ' . $value2['email'] . '</address> 
										 <address style="display:none;"><i class="zmdi zmdi-pin"></i>' . $value2['address'] . '</address>
									 
									 </div>
								 </div>';
                                    $i++;
                                }
                            }
                        }
                        if (!$data) {
                            echo '<p style="text-align:center;">Geen toekomstige afspraken</p>';
                            echo '<hr><a href="/app/teachers-profile2"><button type="submit" style="width:15%;margin-top: 12px;" class="btn btn--save" id="" name="">  Vind docent </button></a>';
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>


        <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
        <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


        <?php
        require_once('includes/footerScriptsAddForms.php');
        ?>
        <!-- <script src='assets/plugins/fullcalendar/lib/moment.min.js'></script> -->
        <!-- <script src='assets/plugins/fullcalendar/fullcalendar.min.js'></script> -->
        <!-- <script src='assets/plugins/fullcalendar/lib/moment.min.js'></script> -->
        <!--<script src="assets/js/pages/calendar/calendar.js"></script>-->
        <!-- <script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script> -->

        <!-- New Calendar JS -->
        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script type="text/javascript"
                src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript"
                src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
        <script type="text/javascript" src="calendar/js/calendar.js"></script>
        <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
        <script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
        <!-- <script type="text/javascript" src="calendar/js/app.js"></script> -->
        <!-- New Calendar JS -->

        <script>

            var calendar;
            var options;


            var _obj = null;
            var _id = null;
            var _name = null;

            var _ddate = null;
            var _stime = null;
            var _etime = null;

            $(document).ready(function () {
                <?php

                if(isset($_SESSION['popup_msg'])){
                ?>
                    showNotification("alert-success", "Je account is geactiveerd", "bottom", "center", "", "");
                    <?php
                        unset($_SESSION['popup_msg']);
                    }
                ?>

                // $(window).resize(function(){$(".zmdi-swap").click();});

                $(".page-loader-wrapper").css("display", "none");

                "use strict";
                var url = "/api/Calendar/GetAppointments?studentID=" +<?php echo @$_SESSION['StudentID']; ?>;
                options = {
                    events_source: url,
                    view: 'month',
                    tmpl_path: 'calendar/tmpls/student-dashboard/',
                    tmpl_cache: false, day: 'now',

                    merge_holidays: false,
                    display_week_numbers: false,
                    weekbox: false,
                    //shows events which fits between time_start and time_end
                    show_events_which_fits_time: true,

                    onAfterEventsLoad: function (events) {

                        if (!events) {
                            return;
                        }
                        var list = $('#eventlist');
                        list.html('');

                        $.each(events, function (key, obj) {
                            //converting the timeZone to UTC
                            obj.start = new Date(parseInt(obj.start)).setTimezoneOffset(0);
                            obj.end = new Date(parseInt(obj.end)).setTimezoneOffset(0);

                            $(document.createElement('li'))
                                .html('<a href="' + obj.url + '">' + obj.title + '</a>')
                                .appendTo(list);
                        });

                    },
                    onAfterViewLoad: function (view) {
                        $('h3.ma-subtitle').text(this.getTitle());

                        $('.page-header h3').text(this.getTitle());
                        $('.btn-group button').removeClass('active');
                        $('button[data-calendar-view="' + view + '"]').addClass('active');
                    },
                    classes: {
                        months: {
                            general: 'label'
                        }
                    }
                };

                calendar = $('#calendar').calendar(options);

                $('.btn-group button[data-calendar-nav]').each(function () {
                    var $this = $(this);
                    $this.click(function () {
                        calendar.navigate($this.data('calendar-nav'));
                    });
                });

                $('.btn-group button[data-calendar-view]').each(function () {
                    var $this = $(this);
                    $this.click(function () {
                        calendar.view($this.data('calendar-view'));
                    });
                });

                $('#first_day').change(function () {
                    var value = $(this).val();
                    value = value.length ? parseInt(value) : null;
                    calendar.setOptions({first_day: value});
                    calendar.view();
                });

                $('#language').change(function () {
                    calendar.setLanguage($(this).val());
                    calendar.view();
                });

                $('#events-in-modal').change(function () {
                    var val = $(this).is(':checked') ? $(this).val() : null;
                    calendar.setOptions({modal: val});
                });
                $('#format-12-hours').change(function () {
                    var val = $(this).is(':checked') ? true : false;
                    calendar.setOptions({format12: val});
                    calendar.view();
                });
                $('#show_wbn').change(function () {
                    var val = $(this).is(':checked') ? true : false;
                    calendar.setOptions({display_week_numbers: val});
                    calendar.view();
                });
                $('#show_wb').change(function () {
                    var val = $(this).is(':checked') ? true : false;
                    calendar.setOptions({weekbox: val});
                    calendar.view();
                });
                $('#events-modal .modal-header, #events-modal .modal-footer').click(function (e) {
                    //e.preventDefault();
                    //e.stopPropagation();
                });

                $(".cal-month-day").on("click", function () {
                    console.log($(this).find(".cal-slide-content").html());

                });

                $(".list-item").on("mouseover", function (e) {
                    e.preventDefault();
                    alert("heree");
                    console.log($(this).html());
                });


                $("#delete-yes-type").click(function () {
                    cancelSlott(_obj, _id);
                    $("#modal-delete-type").modal('hide');
                });

                $("#delete-no-type").click(function () {
                    $("#modal-delete-type").modal('hide');
                    _obj = null;
                    _id = null;
                    _name = null;

                    _ddate = null;
                    _stime = null;
                    _etime = null;
                });


                $("#delete-yes-type2").click(function () {
                    rejectt(_id);
                    $("#appointment-delete-type").modal('hide');
                });

                $("#delete-no-type2").click(function () {
                    $("#appointment-delete-type").modal('hide');
                    _id = null;
                });

                <?php
                if (!empty($_GET['login'])) {
                    trackEvent('Login Student', 'Click', 'Success');
                }
                ?>

            }); //main $()


            function removeButton(obj) {
                $(obj).find(".inline-elem-buttons-box").remove();
            }


            //   function addButton(obj, id, slotType){

            //     var cases = $(obj).attr("data-event");

            // 	  if(slotType == 2){
            //      		$(obj).append('<span class="inline-elem-buttons-box">\
            // 		   <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, '+id+', '+slotType+')">VERZOEK ANNULEREN</button>\
            // 		</span>');
            // 	  }
            // 	  else if (slotType == 1){
            // 			$(obj).append('<span class="inline-elem-buttons-box">\
            // 		   <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, '+id+', '+slotType+')">AFSPRAAK ANNULEREN</button>\
            // 		</span>');
            // 	  }
            //   }// addButton function


            function cancelSlot(obj, id, slotType) {

                if (slotType == 1) {
                    $("#myModalLabel").text("Weet je zeker dat je de bijles wilt annuleren?");
                    _notficationMsg = "Afspraak is geannuleerd";
                } else {
                    $("#myModalLabel").text("Weet je zeker dat je het verzoek wilt annuleren?");
                    _notficationMsg = "Verzoek is geannuleerd";
                }
                $('#modal-delete-type').appendTo("body").modal('show');
                _obj = obj;
                _id = id;
            }

            function cancelSlott(obj, id) {

                $.ajax({
                    type: "POST",
                    url: "student-ajaxcall.php",
                    data: {subpage: "slotCancellation", cbID: id},
                    dataType: "json",
                    success: function (response) {
                        if (response == "success") {
                            reload(<?php echo $_SESSION['StudentID'];?>);
                            calendar = $('#calendar').calendar(options);
                            showNotification("alert-success", _notficationMsg, "bottom", "center", "", "");
                        } else {
                            showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                        }
                        $(document.body).css({'cursor': 'default'});
                        $("#submitBtn").prop("disabled", false);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        if (xhr.responseText == "success") {
                            reload(<?php echo $_SESSION['StudentID'];?>);
                            calendar = $('#calendar').calendar(options);
                            showNotification("alert-success", _notficationMsg, "bottom", "center", "", "");
                        } else {
                            showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                        }
                        $(document.body).css({'cursor': 'default'});
                        $("#submitBtn").prop("disabled", false);

                    }
                });// ends ajax
            }

            function booking() {

                $(document).css({'cursor': 'wait'});
                $("#submitBtn").prop("disabled", true);

                var slotData = "";
                var stime = $(this).attr("data-stime");
                var etime = $(this).attr("data-etime");
                var date = $(this).attr("data-date");

                $("#cal-slide-content ").find(".inline-elem-check").each(function (index, obj) {
                    if ($(this).prop("checked")) {
                        var stime = $(this).attr("data-stime");
                        var etime = $(this).attr("data-etime");
                        var date = $(this).attr("data-date");
                        slotData += $(this).val() + "," + date + "," + stime + "," + etime + "&";
                    }
                });

                slotData = slotData.substring(0, slotData.length - 1);

                $.ajax({
                    type: "POST",
                    url: "student-ajaxcall.php",
                    data: {subpage: "slotBooking", teacherid: teacherid, slotData: slotData, hourlyCost: hourlyCost},
                    dataType: "json",
                    success: function (response) {
                        if (response == "success") {
                            reload(<?php echo $_SESSION['StudentID'];?>);
                            calendar = $('#calendar').calendar(options);
                            showNotification("alert-success", "Je verzoek is verzonden", "bottom", "center", "", "");
                        } else {
                            showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                        }
                        $(document.body).css({'cursor': 'default'});
                        $("#submitBtn").prop("disabled", false);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        if (xhr.responseText == "success") {
                            reload(<?php echo $_SESSION['StudentID'];?>);
                            calendar = $('#calendar').calendar(options);
                            showNotification("alert-success", "Je verzoek is verzonden", "bottom", "center", "", "");
                        } else {
                            showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                        }
                        $(document.body).css({'cursor': 'default'});
                        $("#submitBtn").prop("disabled", false);


                    }
                });// ends ajax

            }// func ends


            function reject(id, name) {
                _id = id;

                var modelLabel = "";

                if (name == 1) {
                    modelLabel = "WEET JE ZEKER DAT JE DE AFSPRAAK WILT ANNULEREN?";
                    _notficationMsg = "Afspraak is geannuleerd";
                    _timer = 3000;
                } else if (name == 2) {
                    modelLabel = "WEET JE ZEKER DAT JE HET VERZOEK WILT ANNULEREN?";
                    _notficationMsg = "Verzoek is geannuleerd";
                    _timer = 99999999;
                }

                $("#myModalLabel2").text(modelLabel);
                $('#appointment-delete-type').appendTo("body").modal('show');
            }

            function rejectt(id) {
                var data_opt;
                var str_id = id + "";
                //data_opt = { subpage:"acceptPendingAppoi", cbID: id};

                data_opt = {subpage: "cancelPending", cbID: id, decide_action: 9};
                var _id = str_id.split(".");
                //console.log($("#cl"+_id[0]).html());
                $.ajax({
                    type: "POST",
                    url: "teacher-ajaxcalls.php",
                    data: data_opt,
                    success: function (response) {

                        //alert(id);
                        if (response.trim() == "success") {

                            $("#cl" + _id[0]).css("display", "none");
                            $("#h" + _id[0]).css("display", "none");
                            reload(id);
                            calendar = $('#calendar').calendar(options);
                            showNotification("alert-success", _notficationMsg, "bottom", "center", "", _timer);
                        } else {
                            showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                        }
                        // $(document.body).css({'cursor' : 'default'});
                        // $("#submitBtn").prop("disabled", false);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(id + " error: " + xhr.responseText);
                    }
                });
            }


            function reload(id) {
                var data_opt;
                var str_id = id + "";
                //data_opt = { subpage:"acceptPendingAppoi", cbID: id};

                data_opt = {subpage: "reloadDiv", id:<?php echo $_SESSION['StudentID']; ?>};
                var _id = str_id.split(".");
                //console.log($("#cl"+_id[0]).html());
                $.ajax({
                    type: "POST",
                    url: "student-ajaxcall.php",
                    data: data_opt,
                    success: function (response) {

                        //alert(id);
                        $("#reload").html(response);
                        //console.log(response);
                        // calendar = $('#calendar').calendar(options);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(id + " error: " + xhr.responseText);
                    }
                });
            }

        </script>

    </body>
    </html>

    <div class="modal modal-fullscreen fade" id="appointment-delete-type" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 100% !important">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h6 class="modal-title" id="myModalLabel2">WEET JE ZEKER DAT JE HET VERZOEK WILT WEIGEREN?</h6>
                </div>
                <div class="modal-body">
                    <div id="deresulttype"></div>
                    <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                        <input type="hidden" id="delete-id-type" name="delete-id-type"/>
                        <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">

                                <button type="button" class="btn btn-success" id="delete-yes-type2">Ja</button>
                                <button type="button" class="btn btn-danger" id="delete-no-type2">Nee</button>

                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        </div>
                    </form>
                    <!-- -->
                </div>

            </div>
        </div>
    </div>


    <div class="modal modal-fullscreen fade" id="modal-delete-type" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content" style="width: 100% !important">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h6 class="modal-title" id="myModalLabel">Weet je zeker dat je het verzoek wilt annuleren?</h6>
                </div>
                <div class="modal-body">
                    <div id="deresulttype"></div>
                    <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                        <input type="hidden" id="delete-id-type" name="delete-id-type"/>
                        <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">

                                <button type="button" class="btn btn-success" id="delete-yes-type">Ja</button>
                                <button type="button" class="btn btn-danger" id="delete-no-type">Nee</button>

                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        </div>
                    </form>
                    <!-- -->
                </div>

            </div>
        </div>
    </div>
    <?php

}

?>