<?php
session_start();
if(isset($_SESSION['Teacher']))
{
	unset($_SESSION['Teacher']);
	unset($_SESSION['TeacherID']);
	unset($_SESSION['TeacherFName']);
	unset($_SESSION['TeacherLName']);
	unset($_SESSION);
}

if(isset($_SESSION['Student']))
{
	unset($_SESSION['Student']);
	unset($_SESSION['StudentID']);
	unset($_SESSION['StudentFName']);
	unset($_SESSION['StudentLName']);	
	unset($_SESSION);
}

session_destroy();
header('Location: /app/');

?>