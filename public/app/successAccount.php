<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Door in te loggen, boek je direct de bijlesdocent naar jouw wensen | Laat je gerust eerst adviseren door onze specialisten.">

    <title>Gelukt! | Bijles Aan Huis</title>
    <?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
	?>
    <!-- Custom Css -->
    <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
	<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
	<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
	<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
	<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />	
	<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	
	<link rel="stylesheet" href="calendar/css/calendar.css">	
		<!-- New Calendar Style -->
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">
	<style>
		
	</style>
</head>

<body class="theme-green authentication login-body">
<!-- Navbar -->
    <?php	
        require_once('includes/header.php');
    ?>
<!-- End Navbar -->
<div class="page-header">
    <div class="container content-container">
        <div class="col-md-12 login-container login-container--lg-padding">
            <div class="card-plain">
                <form class="form login-form" method="" action="">
                    <div class="header">
                        <h5 class="ma-heading login-form-header">Goed werk, de eerste stap is gezet! </h5>
                    </div>
                    <div class="login-form-content">
                        <p class="login-form__text">Je kan vanaf nu inloggen en de meest geschikte docent boeken. Laat je hierbij gerust eerst door ons adviseren!</p>
                    </div>
                    <div class="login-form__button-container">
                        <a href="/" class="btn login-form-button">Naar de hoofdpagina</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
   		require_once('includes/footerScriptsAddForms.php');
	?>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
</script>
</body>
</html>