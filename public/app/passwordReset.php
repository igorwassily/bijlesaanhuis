<?php

require_once('includes/connection.php');
$email = test_input($_GET['email']);
$hash = test_input($_GET['hash']);

$stmt01 = $con->prepare("SELECT userID from user where email = ? AND pwreset_token = ?");
$stmt01->bind_param("ss",$email,$hash);
$stmt01->execute();
$stmt01->bind_result($userIDReset);
$stmt01->store_result();
$stmt01->fetch();
$stmt01->close();
$stmt = $con->prepare("UPDATE user set pwreset_token = NULL where userID = ?");
$active = 1;
$stmt->bind_param("i",$userIDReset);
if($stmt->execute())
{
	?>
	
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Wachtwoord vergeten | Het overkomt de besten | Bijles Aan Huis; kwaliteit, innovatie en betrouwbaarheid.">
	<title>Wachtwoord vergeten | Bijles Aan Huis</title>
	<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
	?>
    <!-- Favicon-->
	<link rel="shortcut icon" href="img/favico.png" type="image/png"> <!-- Favicon-->
    <!-- Custom Css -->

	<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
	<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
	<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
	<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
	<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
	<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />	
	<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	
	<link rel="stylesheet" href="calendar/css/calendar.css">	
		<!-- New Calendar Style -->
	<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

	
	<style>
		.msg-error {
		  color: #c65848;
		}
		.g-recaptcha.error {
		  border: solid 2px #c64848;
		  padding: .2em;
		  width: 19em;
		}		
		/* {
			color: #486066!important;
			font-family: Poppins light !important;
			background-color: #f1f1f1 !important;
		}*/
		div.container{width: 100%;}
		.zmdi{font: normal normal normal 14px/1 'Material-Design-Iconic-Font'!important;}
		input::placeholder {color: #486066!important;
		}
		.border{
			    border: 2px solid rgb(72, 96, 102) !important;
    			border-radius: 50px;
		}
		
		/* login */
		.ma-heading{
			color: #000;
		}
		.authentication .card-plain.card-plain .form-control:focus{
			color: #486066 !important;
		}
		.authentication .card-plain.card-plain .form-control, .authentication .card-plain.card-plain  .input-group-focus .input-group-addon{
			color: #486066 !important;
		}

		.authentication .card-plain{
			max-width: 50%;
		}
		.authentication .link, h6{
			color: #000;
		}
		/* #recaptcha{
			margin: 0 auto;
		} */
		.authentication .card-plain.card-plain .input-group-addon{
			color: rgb(0, 0, 0);
		}
		
		.form-group.input-lg .form-control, .input-group.input-lg .form-control {
   			padding: 21px 18px !important;
		}
		@media only screen and (max-width: 768px){
				.authentication .card-plain{
					max-width: 100%;
			}
		}
*{
		color:#486066!important;
		font-size: 16px;
		font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important; 
	}


	</style>
</head>

<body class="theme-green authentication login-body">
<!-- Navbar --> 
	<?php	
		require_once('includes/header.php');
	?>
<!-- End Navbar -->
<div class="page-header">
    <div class="container">
        <div class="col-md-12 login-container login-container--md-padding">
				
            <div class="card-plain">
				<!-- Error Message -->
			<?php
				require_once("includes/connection.php");
								if(isset($_GET['msg']))
								{
									?>
							<div class="bs-example" style="font-size:10px;">
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo test_input($_GET['msg']); ?>
    </div>
</div>
							<?php
								}
							?>
            
			    <form class="form login-form" method="post" action="actions/passwordReset" onsubmit="return checkPassword(this);">
                    <div class="header">
                        <h5 class="ma-heading login-form-header">Wachtwoord veranderen</h5>
                    </div>
                    <div class="login-form-content">
                        <div class="login-form-content__group">
                            <label class="login-form-label">Nieuw wachtwoord</label>
                            <input type="password" placeholder="Nieuw wachtwoord" class="login-form-input" name="password" required maxlength="20" minlength="8" title="Password must contains Minimum 8 characters" id="password"/>
							<input type="hidden" placeholder="Password" class="form-control" name="userID" required value="<?php echo $userIDReset; ?>" />
                            <span class="input-group-addon inloggen-input-icon">
                                <i class="zmdi zmdi-lock"></i> 
                            </span>
                        </div>
						<div class="login-form-content__group">
                            <label class="login-form-label">Bevestig wachtwoord</label>
                            <input type="password" placeholder="Bevestig wachtwoord" class="login-form-input" name="cpassword" required maxlength="20" minlength="8" title="Password must contains Minimum 8 characters" id="cpassword"/>
                            <span class="input-group-addon inloggen-input-icon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                    </div>
                    <div class="login-form__button-container">
						<button type="submit" name="reset" class="btn login-form-button">BEVESTIGEN</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
   		require_once('includes/footerScriptsAddForms.php');
	?>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
	
function checkPassword(theForm) {
	if (theForm.password.value != theForm.cpassword.value)
	{
		alert('Password Must Be Same As Confirm Password');
		return false;
	} else {
		return true;
	}
}
</script>
</body>
</html>
<?php
}
else
{
	header('Location: expiredLink.php');
}

?>
