<?php
require_once('includes/connection.php');
session_start();

$email = test_input($_GET['email']);
$hash = test_input($_GET['hash']);


$stmt01 = $con->prepare("SELECT userID, 
                                       usergroupID, 
                                       username, 
                                       registered_from
                                    FROM user 
                                    WHERE email = ? 
                                    AND verificationcode = ?");
$stmt01->bind_param("ss", $email, $hash);
$stmt01->execute();
$stmt01->bind_result($userID, $userGroupID, $username, $registered_from);
$stmt01->store_result();
$stmt01->fetch();
$stmt01->close();

/**
 * Check to see if we have a userId
 */
if( $userID > 0) {
    // Ok we have a valid email address
    $stmt = $con->prepare("UPDATE user SET active = ?, 
                                    verificationcode = NULL 
                                    WHERE userID = ?");

    /**
     * Set popup message flag so we can trigger on on some pages
     */
    $_SESSION['popup_msg'] = true;

    $active = 1;

    $stmt->bind_param("ii", $active, $userID);
    if ($stmt->execute()) {
        if ($userGroupID != 1) {
            if ($registered_from > '') {
                header('Location: ' . $registered_from);
            } else {
                header('Location: successAccount.php?registration_from=' . $registered_from);
            }
            exit;
        }

        $_SESSION['Student'] = $username;
        $_SESSION['StudentID'] = $userID;
        $contactType = 'Student';
        $stmt01 = $con->prepare("SELECT c.firstname, c.lastname 
                                            FROM contact c 
                                            INNER JOIN contacttype ct ON c.contacttypeID = ct.contacttypeID 
                                            WHERE c.userID = ? 
                                            AND ct.contacttype = ?");
        $stmt01->bind_param("is", $userID, $contactType);
        $stmt01->execute();
        $stmt01->bind_result($firstname, $lastname);
        $stmt01->store_result();
        $stmt01->fetch();
        $_SESSION['StudentFName'] = $firstname;
        $_SESSION['StudentLName'] = $lastname;
        $_SESSION['image_letter'] = ucfirst($firstname[0]) . "" . ucfirst($lastname[0]);
        $_SESSION['isSlotBooked'] = false;

        if ($registered_from > '') {
            header('Location: ' . $registered_from);
        } else {
            header('Location: students-dashboard?registration_from=' . $registered_from);
        }
        exit;
    }
} else {
    header('Location: expiredLink.php');
    exit;
}