<?php

require_once('includes/connection.php');
$thisPage="email";
session_start();

$totalMail =0;

$pageLimit = 10;
$limit_from = 0;


// $limit_to = $pageLimit;
$page = (isset($_GET['p']))? $_GET['p'] : 0;

if($page !=0){
    $limit_from = ($page-1)*$pageLimit;
    // $limit_to = ($page * $pageLimit);
}
$totalPages = 0;

if(!isset($_SESSION['TeacherID']) && !isset($_SESSION['StudentID']))
{
  header('Location: index.php');
}
else
{
    $userID   =   (isset($_SESSION['TeacherID']))? $_SESSION['TeacherID'] : $_SESSION['StudentID'];


	$query = 'SELECT  e.senderID, e.receiverID, c.userID, MAX(e.date_time) AS date_time, (SELECT message FROM email ee WHERE (ee.senderID = c.userID AND ee.receiverID='.$userID.') OR (ee.senderID = '.$userID.' AND ee.receiverID=c.userID) ORDER BY ee.emailID DESC LIMIT 1)message, (SELECT DATE_FORMAT(date_time, "%H:%i, %d %M %Y") FROM email ee WHERE (ee.senderID = c.userID AND ee.receiverID='.$userID.') OR (ee.senderID = '.$userID.' AND ee.receiverID=c.userID)  ORDER BY ee.emailID DESC LIMIT 1)dtime, (SELECT COUNT(emailID)email FROM `email` WHERE email.senderID=e.senderID AND email.receiverID=e.receiverID AND is_read=0)unreadMail, (SELECT image FROM `profile` p , teacher t WHERE p.profileID=t.profileID AND t.userID=c.userID)image, (SELECT contacttype FROM `contacttype` WHERE `contacttype`.contacttypeID=c.contacttypeID)contacttype, c.firstname, c.lastname, c.contacttypeID FROM  email e  RIGHT JOIN contact c ON c.userID=e.senderID  LEFT JOIN calendarbooking cb ON cb.studentID=c.userID WHERE (e.receiverID='.$userID.' OR cb.teacherID='.$userID.')  AND contacttypeID IN (1,2) GROUP BY c.userID ORDER BY date_time DESC';

        $result = mysqli_query($con, $query);



    // to count number of sender fro pagination
    $query2 = 'SELECT COUNT(DISTINCT senderID)total FROM email e WHERE e.receiverID='.$userID.' OR e.senderID='.$userID;
  
    $r = mysqli_query($con, $query2);
    $d = mysqli_fetch_assoc($r);
    $totalPages = ceil($d['total']/$pageLimit);
 

function getDutchMonth($date){
	
	if(!$date)
		return "";
	
	$x = explode(",", $date);
	$m = explode(" ", trim($x[1]));
	
	$months = array(
		"January"   => "Januari",
		"February"  => "Februari",
		"March" => "Maart",
		"April"     => "April",
		"May"       => "Mei",
		"June"      => "Juni",
		"July"      => "Juli",
		"August"    => "Augustus",
		"September" => "September",
		"October"   => "Oktober",
		"November"  => "November",
		"December"  => "December"
	);
	
	return $x[0].", ".$m[0]." ".$months[$m[1]]." ".$m[2];
}

    if(isset($_SESSION['Teacher']))
    	$desc = "Een berichtje: persoonlijk contact maakt bijles geven leuk | Verdien meer dan bij onze grootste concurrenten | Bepaal zelf aan wie, wanneer en waar je bijles geeft.";
    else if ($_SESSION['StudentID'])		
    	$desc = "Een berichtje; een eerste mooie stap richting betere schoolresultaten | Niet goed? Geld terug! | Alles voor het beste resultaat!"; 
    else 
        $desc = "Log in om de chat te bekijken";
?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="<?php echo $desc ?>">
<title>Berichten | Bijles Aan Huis</title>

<?php require_once('includes/mainCSSFiles.php'); ?>


<link rel="stylesheet" href="assets/css/inbox.css">
 <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


<style type="text/css"> 
    body{
        background-color: #fafbfc !important;
    } 
    .card.action_bar, .card.profile-header, .card.demo-masked-input{
        background: #fafbfc !important;
    } 
    .theme-green .navbar, .theme-green .page-loader-wrapper{
        background-color: #fafbfc !important;
        background: #fafbfc !important;
    }
	.no-message
	{
		font-size:21px;
	}

    
.unread{background-color: goldenrod;}
.list-group-item{cursor:pointer;}
.greenHeading{color: #5cc75f;font-size: 24px;}
.inbox .mail_list .list-group-item .thumb img {
    width: 60px;
    height: 60px;
    image-rendering: pixelated;
}
.inbox .mail_list .list-group-item .media-heading a {
    font-weight: bold;
}
.inbox .mail_list .list-group-item:hover {
    background: #ededed !important;  /*rgba(58, 59, 60, 0.49)*/
}
	.no-hover:hover
	{
	  background: transparent !important;
	}
.list-group-item  {
    border-left: 3px solid #5cc75f !important;	
    margin-bottom: 7px !important; 
}
	.badge.bg-amber{border:0px; color:white !important;}
	
div.media-p > p {
    display: inline-block;
    text-align: center;
    word-break: break-all;
    overflow: hidden;
}
.page-link.btn-primary.active{
    color: white !important;
    background-color: #5cc75f !important;
    border: 2px solid #50d38a !important;
}
.btn.btn--save {
    background: #5CC75F !important;
    border: solid 2px #5CC75F !important;
    color: #fafbfc !important;
    transition-delay: 0s;
    transition-duration: 0.2s;
    transition-property: all;
    font-weight: 600 !important;
    font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important;
    text-transform: uppercase !important;
    border-radius: 6px !important;
    padding: 15px 20px;
    width: 200px !important;
    margin-top: 20px !important;
    margin-bottom: 20px !important;
}
.card .body {
    background-color: #FAFBFC !important;    
	border: 2px solid #FAFBFC;
    border-radius: 6px;
    min-height: 50vh !important;
}
    .card .header{
         background-color: #e8e8e8 !important;  
    }
	.mail_list.list-group.list-unstyled li {
    
}
    .ma-sidebar-container{ background-color: #FAFBFC !important; }
    .content{
        background-color: #ffffff !important;
        
    }
    .content.inbox{
        padding:0;
    }
    .content.inbox .message-card{
        background-color: #FFFFFF;
        border: 2px solid #f1f1f1;
        border-radius: 6px;
    }
    .content.inbox .message-card .header{
        background-color: #F1f1f1 !important;
        padding: 20px 40px;
        
    }

    .content.inbox .message-card .header .ma-main-title{
        font-family: 'Poppins', sans-serif !important;
        font-weight: 600;
        font-size: 21px !important;
    }

    .content.inbox .message-card .header .ma-main-title::before{
        content: none;
    }
   
    #profileImageMini{
        width: 60px;
        height: 60px;
        background-size: 60px 60px;
        background-image: url(assets/images/profile_av.jpg);
        background-repeat: no-repeat;
    } 
    .bg-amber{
        background-color: #5cc75f !important;
    }
    .card{
        background-color: #e8e8e8 !important;
    }
    .card.action_bar, .card.profile-header, .card.demo-masked-input{
        background-color: #e8e8e8;
    }
     
    @media screen and (max-width: 600px){
        body section.content{
            padding: 0 !important;
            margin: 0px !important;
        }
        .ma-sidebar-container{
            margin-top: 0px !important;
        }
        .navbar{
            margin-bottom: 0px !important;
        }
        .msg_text{
            display: inline-block;
            width: 100%;
        }
    }
    .float-right {
        float:right;
    }
    .btn, .theme-green .btn-primary, .pagination .page-item .page-link {
        display:none;
    }
    .btn, .theme-green .btn-primary, .pagination .page-item:first-child .page-link, .btn, .theme-green .btn-primary, .pagination .page-item:nth-child(2) .page-link, .btn, .theme-green .btn-primary, .pagination .page-item:last-child .page-link {
    	border-radius: 6px !important;
        display: block;
    }
</style>

<?php
$activePage = basename($_SERVER['PHP_SELF']);
  ?>


</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div> 
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php 
        require_once('includes/header.php');
?>

<!-- Main Content -->

   <section class="content inbox">
    <div class="block-header" style="display:none;">
        
       <?php 
       		if(isset($_SESSION['Student']))
	        	require_once('includes/studentTopBar.php'); 
	        else
	        	require_once('includes/teacherTopBar.php'); 
        ?>
    </div>
    <div class="container-fluid ma-sidebar-container" style="background-color: transparent !important;">
        <div class="row clearfix">
            <div class="col-lg-12">
                <div class="card message-card action_bar">
					<div class="header">
                        <h2 class="ma-main-title">Berichten</h2>
                    </div>
					
                    <div class="body">
                        
						<div class="row clearfix">
							<div class="col-md-12 col-lg-12 col-xl-12">
								<ul class="mail_list list-group list-unstyled">
								<?php 
									$i =0 ;
									while($data = mysqli_fetch_assoc($result)){
                                        if (empty($data['senderID'])) $data['senderID'] = $data['userID'];
 										//break;
										$img = ($data['image'])? $data['image'] : constant("DEFAULT_PIC");

									   // $isUnread = ($data['unreadMail'] > 0)? "unread" : "";
										$isUnread = ($data['unreadMail'] > 0)? '<span class="badge bg-amber">Nieuw</span>' : '';
										$contactType = ($data['contacttypeID'] == 1)? "tid" : "sid";
										$path = "$contactType=".$data['senderID'];
                                        echo '<a class="mail_list_link" href="chat?'.$path.'">';
										echo '<li class="list-group-item"  href="chat?'.$path.'">
												<div class="media">
													<div class="pull-left">';                                
												if(!isset($_SESSION['image_letter']))
														echo '<div class="image" id="profileImageMini">'.(isset($data['reciverName']) ? ucfirst($data['reciverName']) : '').'</div>';
												else
														echo '<div class="thumb hidden-sm-down m-r-20"> <img src="'.$img.'" class="rounded-circle" alt=""> </div>';
										
										echo ' </div>
													<div class="media-body">
														<div class="media-heading">
															<span href="chat?'.$path.'" class="media-heading__name m-r-10">'.$data['firstname'].'</span>
															'.$isUnread.'
															<small class="float-right text-muted"><time class="hidden-sm-down">'.getDutchMonth($data['dtime']).'</time></small>
														</div>
														<p class="msg">'.(($data['contacttype'] == "Teacher")? "Bijlesdocent": "Leerling").'</p>
														<p class="msg_text">'.(($data['message'] == "0") ? " " : $data['message']).'</p>
													</div>
												</div>
                                            </li>';
                                            echo '<a/>';
											$i++;
									}// while ends
                                    
									if($i==0){
										
										if(isset($_SESSION['StudentID'])){
											echo '<li class=" no-message text-center no-hover" style="">Geen berichten<br/>
											 <div style="font-size:16px;margin-top: 8px;">Bijles is de eerste stap naar betere cijfers!<hr/>
											 <a href="/app/teachers-profile2"><button type="submit" style="width:15%;margin-top: 12px;" class="btn btn--save" id="" name="">  Vind docent </button>	</a>
											 </div></li>
											 ';
										}else if(isset($_SESSION['Teacher'])){										 
											echo '<li class=" no-message text-center no-hover no-message--edit" style="">Geen berichten</li>';
										}
									}



								 ?>

									</ul>
								<?php if($totalPages > 1){ ?>	
                                    <div class="dataTables_wrapper">
                                        <div class="dataTables_paginate">
                                            <ul class="pagination pagination-primary m-b-0">
                                                <?php
                                                    $pre = 0;
                                                    $next= 0;
                                                    $str = "";

                                                    for ($i=1; $i <= $totalPages ; $i++) {
                                                        if(($limit_from+1) == $i){
                                                            $pre = ($i == 0)? "" : $i-1;
                                                            $next = ($i == $totalPages)? "" : $i+1;
                                                            $str .= '<li class="page-item active"><a href="message?p='.$i.'" class="page-link btn-primary active">'.$i.'</a></li>';
                                                        }else{
                                                            $str .= '<li class="page-item"><a href="message?p='.$i.'" class="page-link btn-primary">'.$i.'</a></li>';
                                                        }
                                                    }//for ends
                                                    if($totalPages > 1){
                                                 ?>

                                                <li class="page-item"><a href="message?p=<?php echo $pre; ?>" class="page-link btn-primary">VORIGE</a></li>
                                                    <?php //echo $str; ?>
                                                <li class="page-item"><a href="message?p=<?php echo $next; ?>" class="page-link btn-primary">VOLGENDE</a></li>
                                                <?php } ?>
                                            </ul>
                                        </div>
                                    </div>
								<?php } ?>
								
							</div>
						</div>
						
                    </div>
                </div>
            </div>           
        </div>        

    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


<?php
    require_once('includes/footerScriptsAddForms.php');
?>

<!-- New Calendar JS -->
    
    <!-- <script type="text/javascript" src="calendar/js/app.js"></script> -->
<!-- New Calendar JS -->

<script>


  $(document).ready(function() {

  	$(".page-loader-wrapper").css("display", "none");
   /* $(".list-group-item").click(function(e){
            e.preventDefault();
            window.location =$(this).attr("href");
    });*/
    // In case ready function is not working in safari: timeout
    setTimeout($(".pagination a:empty").hide(),300);
  });
</script>

</body>
<style>
#fc_frame, #fc_frame.fc-widget-normal{
        bottom: 15px !important;
    }
  </style>
</html>
<?php
}
?>