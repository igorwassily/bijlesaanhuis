<?php

require_once('includes/connection.php');
session_start();

if ($_GET['subpage'] == "student-profile") {
	$reservedSlot = array();

	$stdID = $_GET['studentid'];
	$tID = $_GET['tid'];
	$firstname = $_GET['firstname'];
	$place_name = $_GET['place_name'];

	//Query Modified, now only student own slots will be fetched
	$q = "SELECT if(cb.studentID=$stdID, 1, 0)ownself, cb.*, (SELECT CONCAT(MIN(ccb.starttime),\" - \",MAX(ccb.endtime)) FROM calendarbooking ccb WHERE ccb.slot_group_id=cb.slot_group_id AND ccb.isgapslot=0 GROUP BY ccb.slot_group_id )timee  FROM calendarbooking cb WHERE cb.teacherID=" . $tID . " AND cb.studentID=$stdID ORDER BY starttime DESC";

	$r = mysqli_query($con, $q);
	while ($d = mysqli_fetch_assoc($r)) {
		$index = $d['datee'] . " " . $d['starttime'] . " " . $d['endtime'];
		$reservedSlot[$index] = $d;
	}

	$query = "SELECT * FROM teacherslots WHERE teacherID=" . $tID . " LIMIT 1";
	$result = mysqli_query($con, $query);

	$dd = mysqli_fetch_assoc($result);
	$wholeDay = "00:00-24:00"; 

	$mon_time = explode("-", $wholeDay);
	$tue_time = explode("-", $wholeDay);
	$wed_time = explode("-", $wholeDay);
	$thur_time = explode("-", $wholeDay);
	$fri_time = explode("-", $wholeDay);
	$sat_time = explode("-", $wholeDay);
	$sun_time = explode("-", $wholeDay);

	$wkdays = getWeekdays($dd['mon_time'], $dd['tue_time'], $dd['wed_time'], $dd['thur_time'], $dd['fri_time'], $dd['sat_time'], $dd['sun_time']);

	$dateRange = createRange($dd['datee'], '60 Day', "Mon,Tue,Wed,Thur,Fri,Sat,Sun");
	$arrTimeDiff = array();
	$arrTimeDiff[1] = timeDiff($mon_time[0], $mon_time[1]);
	$arrTimeDiff[2] = timeDiff($tue_time[0], $tue_time[1]);
	$arrTimeDiff[3] = timeDiff($wed_time[0], $wed_time[1]);
	$arrTimeDiff[4] = timeDiff($thur_time[0], $thur_time[1]);
	$arrTimeDiff[5] = timeDiff($fri_time[0], $fri_time[1]);
	$arrTimeDiff[6] = timeDiff($sat_time[0], $sat_time[1]);
	$arrTimeDiff[7] = timeDiff($sun_time[0], $sun_time[1]); 

	$str = "{
						\"success\": 1,
						\"result\":[";

	$current_date = new DateTime();
	$slotGID = 0;
	$startTime = "";
	$endTime = "";
	$toPrint = false;
	$timeRange = "";
	//all time range for calender
	$checkBoxTimeArray = array();

	foreach ($dateRange as $day) {

		//temp time range for loop-specific day
		$timeArray = array();

		$tempDay = new DateTime($day);
		$timeDiff = (isset($arrTimeDiff[$tempDay->format("N")]['stime'])) ? $arrTimeDiff[$tempDay->format("N")] : 0;

		// first and last index of array
		$timeRange = $timeDiff['stime'][0] . " - " . end($timeDiff['etime']);

		for ($i = 0; $i < count($timeDiff['stime']); $i++) {
			$index = $day . " " . $timeDiff['stime'][$i] . " " . $timeDiff['etime'][$i];

			// Default time(dropdown) is unavailable, it will be set as available if slot is free.
			$timeArray[$timeDiff['stime'][$i]] = "unavailable";

			if (isset($reservedSlot[$index])) {

				$id = $reservedSlot[$index]["slot_group_id"];
				$status = $reservedSlot[$index]["isSlotCancelled"];
				$sdate = $day . " " . $timeDiff['stime'][$i];
				$edate = $day . " " . $timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i] . " - " . $timeDiff['etime'][$i];

				if ($reservedSlot[$index]["isSlotCancelled"] == 1) {
					continue;
				}

				if ($reservedSlot[$index]["isgapslot"] == 1) {
					continue;
				}

				if ($reservedSlot[$index]["slot_group_id"] == $slotGID) {
					continue;
				}

				if ($reservedSlot[$index]["slot_group_id"] != $slotGID) {
					$time = $reservedSlot[$index]["timee"];
					$slotGID = $reservedSlot[$index]["slot_group_id"];
					$toPrint = true;
				}

				if ($reservedSlot[$index]["isSlotCancelled"] == 1) {
					continue;
				}

				if ($reservedSlot[$index]['ownself'] == 1 && $reservedSlot[$index]["accepted"] == 1) {
					$title_desc = $title = "Bijles is geaccepteerd";
					$class = "event-info";
					$onHoverBtn = "(this, $id, 1)";

				} elseif ($reservedSlot[$index]['ownself'] == 1 && $reservedSlot[$index]["accepted"] != 1) {
					$title_desc = $title = "In afwachting";
					$class = "event-warning";
					$onHoverBtn = "(this, $id, 2)";
				} elseif ($reservedSlot[$index]['ownself'] == 0) {
					$title_desc = $title = "Reversed";
					$class = "event-danger";
					$onHoverBtn = "";
				}
			} else {

				$timeArray[$timeDiff['stime'][$i]] = "available";

				$title = "Beschikbaar";
				$title_desc = "Beschikbaar";
				$class = "event-inverse";
				$id = '';
				$status = 0;
				$sdate = $day . " " . $timeDiff['stime'][$i];
				$edate = $day . " " . $timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i] . " - " . $timeDiff['etime'][$i];
				$onHoverBtn = "";
				$toPrint = true;
				$reservedSlot[$index]["isgapslot"] = 1;

			}

			$month = new DateTime($day);

			if ($month->format("m") < $current_date->format("m")) {
				$onHoverBtn = "";
			}

			if ($month->format("m") < $current_date->format("m") && $title == "Beschikbaar") {
				continue;
			}

			$formattedMonth = getBetween($day, '-', '-');
			$dateObj = DateTime::createFromFormat('!m', $formattedMonth);
			$monthName = $dateObj->format('F'); // March

			$daystring = strtotime($day);
			$dayOnly = date('d', $daystring);

			$streetName = getBetween('/' . $place_name, '/', ',');


			$str .= "{ 
					\"id\": \"$id\",
					\"cbID\": \"$id\",
					\"start\": \"" . strtotime($sdate) . "000\",
					\"end\": \"" . strtotime($edate) . "000\",
					\"time\": \"$time\",
					\"timeRange\": \"$timeRange\",
					\"date\": \"$day\",
					\"status\": \"$status\",
					\"class\": \"" . $class . "\",
					\"title\": \"" . $title . "\", 
					\"title_desc\": \"" . $title_desc . "\", 
					\"onHoverBtn\": \"" . $onHoverBtn . "\",
					\"day\": \"" . $dayOnly . "\", 
					\"month\": \"" . $monthName . "\",
					\"firstname\": \"" . $firstname . "\", 
					\"place_name\": \"" . $streetName . "\", 
					\"past\": \"" . $past . "\", 

					\"url\": \"\"
				},";

		}//for loop ends					

		//inserting all time range
		$checkBoxTimeArray[$day] = $timeArray;

	}// for each loop ends

	$checkBoxTimeArray = json_encode($checkBoxTimeArray);

	$str = rtrim($str, ",");
	$str .= "], \"timeRange\": $checkBoxTimeArray}";

	echo $str;


}// student-profile  ends
elseif (isset($_POST['subpage'])) {
	if ($_POST['subpage'] == "slotBooking") {
		$qq = "SELECT COUNT(calendarbookingID)noOfmins FROM calendarbooking WHERE datee >= CURRENT_DATE() AND isgapslot=0 AND isClassTaken=0 AND studentID=$_SESSION[StudentID]";
		$rr = mysqli_query($con, $qq);
		$dd = mysqli_fetch_assoc($rr);
		$mins = $dd['noOfmins'] * SLOT_TIME_IN_MINIUTES;

		if ($mins >= MAXIMUM_TIME_SLOT_ALLOWED_TO_STUDENT) {
			echo "100 hours maximum time for classes is already occupied.";
			return;
		}

		/*	if($_SESSION['isSlotBooked']){
				echo "Maximum One Booking can be made.";
				return;
			}
			*/

		$teacherid = $_POST['teacherid'];

		$qq = "SELECT level, rate, internal_level, internal_rate FROM teacher t, teacherlevel_rate tlr WHERE tlr.ID=t.teacherlevel_rate_ID AND t.userID=$teacherid LIMIT 1 ";
		$r2 = mysqli_query($con, $qq);
		$d2 = mysqli_fetch_assoc($r2);
		$external_level = $d2['level'];
		$external_rate = $d2['rate'];
		$internal_level = $d2['internal_level'];
		$internal_rate = $d2['internal_rate'];


		$slotData = $_POST['slotData'];
		$studentid = (isset($_SESSION['StudentID'])) ? $_SESSION['StudentID'] : $_POST['studentid'];
		$time = explode("-", $_POST['time']);
		$internal_amount = calculateCostPerMintues($time[0], $time[1], $internal_rate);
		$external_amount = calculateCostPerMintues($time[0], $time[1], $external_rate);
		$travelCost = ($_POST['travelCost']) ? $_POST['travelCost'] : 0;
		$travelDistance = ($_POST['travelDistance']) ? $_POST['travelDistance'] : 0;
		$std_lat = (isset($_SESSION['std_lat'])) ? $_SESSION['std_lat'] : $_POST['std_lat'];
		$std_lng = (isset($_SESSION['std_lng'])) ? $_SESSION['std_lng'] : $_POST['std_lng'];
		$place_name = (isset($_SESSION['std_place_name'])) ? $_SESSION['std_place_name'] : $_POST['std_place_name'];

		$insertion_query = "INSERT INTO calendarbooking (studentID, teacherID, slot_group_id, isgapslot, datee, starttime, endtime, internal_bookingamount, external_bookingamount,  accepted, `latitude`, `longitude`, `place_name`, internal_teacher_level , external_teacher_level , `travel_distance`, `travel_cost`) VALUES ";
		foreach ($slotData as $data) {
			$slotGID = $data['slotGID'];

			foreach ($data['slotDetail'] as $d) {
				$t = explode("-", $d['time']);
				$endtime = trim($t[1]);
				$starttime = trim($t[0]);

				$insertion_query .= " (" . $studentid . ", " . $teacherid . ", '" . $slotGID . "', '" . $d['isGapSlot'] . "', '" . trim($d['date']) . "', '" . $starttime . "', '" . $endtime . "', '" . $internal_amount . "', '" . $external_amount . "', 0, '" . $std_lat . "', '" . $std_lng . "', '" . $place_name . "', '" . $internal_level . "', '" . $external_level . "', '" . $travelDistance . "', '" . $travelCost . "'),";
			}//inner foreach ends

		}//outer foreach ends
		$insertion_query = rtrim($insertion_query, ",");
		//echo $insertion_query;

		$stmt = $con->prepare($insertion_query);

		if ($stmt->execute()) {
			$_SESSION['isSlotBooked'] = true;

			$_q = "SELECT c.email, c.telephone, c.place_name, c.firstname, c.lastname, cb.datee, MIN(cb.starttime)starttime, MAX(cb.endtime)endtime, cb.teacherID, (SELECT CONCAT(firstname, '**', email)tdata FROM contact WHERE userID=cb.teacherID)tname, (SELECT CONCAT(firstname,' ', lastname) FROM contact cc WHERE cc.userID=cb.studentID AND cc.contacttypeID=3 LIMIT 1 )pname, p.image timage, cb.travel_cost , tlr.level  FROM  calendarbooking cb , contact c, `profile` p, teacher t, teacherlevel_rate tlr WHERE slot_group_id='$slotGID' AND cb.isgapslot=0 AND c.userID=cb.studentID AND  p.profileID=t.profileID AND t.userID=cb.teacherID AND tlr.ID=t.teacherlevel_rate_ID AND c.contacttypeID IN (1,2)  GROUP BY slot_group_id LIMIT 1";
			$_r = mysqli_query($con, $_q);
			$_f = mysqli_fetch_assoc($_r);

			$pname = explode(" ", $_f['pname']);
			$sname = $_f['firstname'] . " " . $_f['lastname'];
			$semail = $_f['email'];
			$dateTime = $_f['datee'] . ", " . $_f['starttime'] . " - " . $_f['endtime'];
			$tdata = explode("**", $_f['tname']);
			$temail = $tdata[1];

			$pname = explode(" ", $_f['pname']);
			$addres = explode(",", $_f['place_name']);
			$city = explode(" ", $addres[1]);
			$city[0] = "";
			$city = implode(" ", $city);

			$tcost = ($_f['travel_cost']) ? $_f['travel_cost'] : "geen";

			received_tutoring_request_for_tutor($temail, $tdata[0], $dateTime, $sname, $_f['pname'], $_f['place_name'], $city, $semail, $_f['telephone'], $tcost);

			email_confirm_tutoring_request_for_student($semail, $tdata[0], $_f['firstname'] . " / " . $pname[0], $_f['timage'], $dateTime, $tcost, $_f['level']);

			echo "success";
		} else
			echo "failed";

	}// ends if "slotBooking"

	elseif ($_POST['subpage'] == "tslotBooking") {
		$qq = "SELECT COUNT(calendarbookingID)noOfmins FROM calendarbooking WHERE datee >= CURRENT_DATE() AND isgapslot=0 AND isClassTaken=0 AND studentID={$_SESSION['StudentID']}";
		$rr = mysqli_query($con, $qq);
		$dd = mysqli_fetch_assoc($rr);
		$mins = $dd['noOfmins'] * SLOT_TIME_IN_MINIUTES;

		if ($mins >= MAXIMUM_TIME_SLOT_ALLOWED_TO_STUDENT) {
			echo "100 hours maximum time for classes is already occupied.";
			return;
		}

		/*	if($_SESSION['isSlotBooked']){
				echo "Maximum One Booking can be made.";
				return;
			}
			*/


		$teacherid = $_POST['teacherid'];

		$qq = "SELECT level, rate, internal_level, internal_rate, cost_per_km FROM teacher t, teacherlevel_rate tlr WHERE tlr.ID=t.teacherlevel_rate_ID AND t.userID=$teacherid LIMIT 1 ";
		$r2 = mysqli_query($con, $qq);
		$d2 = mysqli_fetch_assoc($r2);
		$external_level = $d2['level'];
		$external_rate = $d2['rate'];
		$internal_level = $d2['internal_level'];
		$internal_rate = $d2['internal_rate'];

		$slotData = $_POST['slotData'];
		$studentid = $_POST['studentid'];
		$time = explode("-", $_POST['time']);
		$internal_amount = calculateCostPerMintues($time[0], $time[1], $internal_rate);
		$external_amount = calculateCostPerMintues($time[0], $time[1], $external_rate);
		$travelDistance = ($_POST['travelDistance']) ? round((float)$_POST['travelDistance']) : 0;

		$travelDistance = ($travelDistance >= TUTOR_NO_TRAVEL_COST_KILOMETER_LIMIT && $travelDistance <= TUTOR_MAX_TRAVEL_KILOMETER_LIMIT) ?
			($travelDistance - TUTOR_NO_TRAVEL_COST_KILOMETER_LIMIT) : 0;

		$travelCost = $travelDistance * $d2['cost_per_km'];


		$std_lat = (isset($_SESSION['std_lat'])) ? $_SESSION['std_lat'] : $_POST['std_lat'];
		$std_lng = (isset($_SESSION['std_lng'])) ? $_SESSION['std_lng'] : $_POST['std_lng'];
		$place_name = (isset($_SESSION['std_place_name'])) ? $_SESSION['std_place_name'] : $_POST['place_name'];
		$distance = $_POST['travelDistance'];

		$insertion_query = "INSERT INTO calendarbooking (studentID, teacherID, slot_group_id, isgapslot, datee, starttime, endtime, internal_bookingamount, external_bookingamount,  accepted, `latitude`, `longitude`, `place_name`, internal_teacher_level , external_teacher_level , `travel_distance`, `travel_cost`) VALUES ";
		foreach ($slotData as $data) {
			$slotGID = $data['slotGID'];

			foreach ($data['slotDetail'] as $d) {
				$t = explode("-", $d['time']);
				$endtime = trim($t[1]);
				$starttime = trim($t[0]);

				$insertion_query .= " (" . $studentid . ", " . $teacherid . ", '" . $slotGID . "', '" . $d['isGapSlot'] . "', '" . trim($d['date']) . "', '" . $starttime . "', '" . $endtime . "', '" . $internal_amount . "', '" . $external_amount . "', 1, '" . $std_lat . "', '" . $std_lng . "', '" . $place_name . "', '" . $internal_level . "', '" . $external_level . "', '" . $travelDistance . "', '" . $travelCost . "'),";
			}//inner foreach ends

		}//outer foreach ends
		$insertion_query = rtrim($insertion_query, ",");
		//echo $insertion_query;

		$stmt = $con->prepare($insertion_query);

		if ($stmt->execute()) {
			$_SESSION['isSlotBooked'] = true;

			$_q = "SELECT c.email, cb.external_teacher_level, c.telephone, c.place_name, c.firstname, c.lastname, c.telephone , cb.datee, cb.starttime, cb.endtime, cb.teacherID, (SELECT CONCAT(firstname, '**', email)tdata FROM contact WHERE userID=cb.teacherID)tname, (SELECT CONCAT(firstname,' ', lastname) FROM contact cc WHERE cc.userID=cb.studentID AND cc.contacttypeID=3 LIMIT 1 )pname,(SELECT p.image FROM profile p, teacher t WHERE t.userID=cb.teacherID AND t.profileID=p.profileID LIMIT 1)image  FROM `calendarbooking`cb , contact c WHERE slot_group_id='$slotGID' AND c.userID=cb.studentID AND c.contacttypeID IN (1,2) LIMIT 1";
			$_r = mysqli_query($con, $_q);
			$_f = mysqli_fetch_assoc($_r);

			$pname = explode(" ", $_f['pname']);
			$sname = $_f['firstname'] . " " . $_f['lastname'];
			$semail = $_f['email'];
			$dateTime = $_f['datee'] . ", " . $_f['starttime'] . " - " . $_f['endtime'];
			$tdata = explode("**", $_f['tname']);
			$tname = $tdata[0];
			$temail = $tdata[1];

			$addres = explode(",", $_f['place_name']);
			$city = explode(" ", $addres[1]);
			$city[0] = "";
			$city = implode(" ", $city);

			tutor_books_a_tutoring_session_himself_for_tutor($temail, $tname, $dateTime, $sname, $_f['pname'], $_f['place_name'], $city, $semail, $_f['telephone']);
			email_tutor_books_a_session_himself_for_student($semail, $sname, $dateTime, $tname, $_f['external_teacher_level'], $_f['image']);

			echo "success";
		} else
			echo "failed";

	}// ends if "tslotBooking"
	elseif ($_POST['subpage'] == "reloadDiv") {
		$query = "SELECT   cb.slot_group_id, MIN(cb.starttime)starttime, MAX(cb.endtime)endtime, c.userID, c.firstname, c.lastname, c.address, cb.calendarbookingID, cb.teacherID, cb.datee, cb.accepted  FROM contact c RIGHT JOIN calendarbooking cb ON cb.teacherID=c.userID AND cb.isSlotCancelled=0 where datee >= CURDATE() AND cb.studentID=$_SESSION[StudentID] AND isgapslot=0 AND cb.isClassTaken=0 AND (cb.accepted=0 OR cb.accepted=1) GROUP BY slot_group_id ORDER BY cb.datee ASC, cb.starttime ASC LIMIT 6";

		$result = mysqli_query($con, $query);

		$isEmpty = true;
		$i = 0;
		$limit = 4;
		while ($row = mysqli_fetch_assoc($result)) {
			$i++;
			if ($i > $limit) {
				echo "<hr/> <p style='text-align:right;'> <a href='students-appointment'>Toon alle</a></p>";
				break;
			}

			$color = ($row['accepted'] == 1) ? "b-primary" : "b-warning";
			$status = ($row['accepted'] == 1) ? "Geaccepteerd" : "In afwachting";
			$accept_delete_button = ($row['accepted'] == 1) ? '<button style="color:white !important; background:#ffbd6b" class="btn m-b-10 w-100 btn-primary-chat" id="" name="" onclick="reject(' . $row['slot_group_id'] . ', 1)">AFSPRAAK ANNULEREN</button>'
				: '<button style="color:white !important; background:#ffbd6b" class="btn m-b-10 w-100 btn-primary-chat" id="" name="" onclick="reject(' . $row['slot_group_id'] . ',2)">VERZOEK ANNULEREN</button>';

			$day = date("d", strtotime($row['datee']));
			$m = getDutchMonth(date("M", strtotime($row['datee'])));
			$year = date("Y", strtotime($row['datee']));

			echo '<hr/><div class="event-name ' . $color . ' row">
                                        <div class="col-3 text-center">
                                            <h4>' . $day . '<span>' . $m . '</span><span>' . $year . '</span></h4>
                                        </div>
                                        <div class="col-9">
                                            <h6>' . "<a href='teacher-detailed-view.php?tid=$row[userID]'>$row[firstname]</a>" . '</h6>
                                            <address><i class="zmdi zmdi-time"></i> ' . $row['starttime'] . " - " . $row['endtime'] . '</address>
                                            <address>' . $status . '</address>' . $accept_delete_button . '
                                        </div>
                                    </div>';

			$isEmpty = false;

		}

		if ($isEmpty) {
			//  echo '<hr/> <center><a href="/app/teachers-profile2"><button style="width:50%;color:white !important; background:#5CC75F" class="btn m-b-10  btn-primary-chat" id="" name="">Vind docent</button></a></center>';
			echo '<hr/><center>Geen toekomstige afspraken</center>';
		}

	}// ends if "reloadDiv"
	elseif ($_POST['subpage'] == "slotCancellation") {
		$_q = "SELECT p.image, cb.accepted,cb.teacherID, c.email, c.telephone, c.place_name, c.firstname, c.lastname, cb.datee, MIN(cb.starttime)starttime, MAX(cb.endtime)endtime, cb.teacherID, (SELECT CONCAT(firstname, '**', email)tdata FROM contact WHERE userID=cb.teacherID)tname, (SELECT CONCAT(firstname,' ', lastname) FROM contact cc WHERE cc.userID=cb.studentID AND cc.contacttypeID=3 LIMIT 1 )pname, p.image timage, cb.travel_cost , tlr.level FROM calendarbooking cb , contact c, `profile` p, teacher t, teacherlevel_rate tlr WHERE slot_group_id='$_POST[cbID]' AND cb.isgapslot=0 AND c.userID=cb.studentID AND p.profileID=t.profileID AND t.userID=cb.teacherID AND tlr.ID=t.teacherlevel_rate_ID AND c.contacttypeID IN (1,2) GROUP BY slot_group_id";
		$_r = mysqli_query($con, $_q);
		$_f = mysqli_fetch_assoc($_r);

		$query_str = "DELETE FROM `calendarbooking` WHERE slot_group_id=" . $_POST['cbID'];
		$stmt = $con->prepare($query_str);

		if ($stmt->execute()) {
			$_SESSION['isSlotBooked'] = false;

			$pname = explode(" ", $_f['pname']);
			$sname = $_f['firstname'] . " / " . $pname[0];
			$semail = $_f['email'];
			$dateTime = $_f['datee'] . ", " . $_f['starttime'] . " - " . $_f['endtime'];
			$tdata = explode("**", $_f['tname']);
			$temail = $tdata[1];

			$path = $_f['image'];

			$addres = explode(",", $_f['place_name']);
			$city = explode(" ", $addres[1]);
			$city[0] = "";
			$city = implode(" ", $city);

			if ($_f['accepted'] == 0) {
				//cancelled_tutoring_request_by_student($semail, $sname, $dateTime, $_f['teacherID'], $tdata[0]);
				email_cancelled_tutoring_request_by_customer_for_tutor($temail, $tdata[0], $dateTime, $_f['firstname'], $pname[0], $_f['place_name'], $city, $semail, $_f['telephone']);
				email_cancelled_tutoring_request_by_student_for_student($semail, $sname, $path, $tdata[0], $dateTime, $_f['teacherID']);
			} elseif ($_f['accepted'] == 1) {
				cancelled_tutoring_session_by_customer($temail, $tdata[0], $dateTime, $_f['firstname'] . " " . $_f['lastname'], $_f['pname'], $_f['place_name'], $city, $semail, $_f['telephone']);
				email_cancelled_tutoring_session_by_student_for_student($semail, $sname, $path, $dateTime, $_f['teacherID']);
			}

			echo "success";
		} else
			echo "failed";
	}//ends if subpage =="slotCancellation"
}//ends if(isset($_POST['subpage']))
elseif (isset($_GET['teacherid'])) {
	$reservedSlot = array();

	//Query Modified, Now only student own slots will be fetched
	$q = "SELECT $std cb.*, (SELECT CONCAT(MIN(ccb.starttime),\" - \",MAX(ccb.endtime)) FROM calendarbooking ccb WHERE ccb.slot_group_id=cb.slot_group_id AND ccb.isgapslot=0 GROUP BY ccb.slot_group_id )timee  FROM calendarbooking cb WHERE cb.teacherID=" . $_GET['teacherid'] . " AND  cb.studentID=$_SESSION[StudentID] ORDER BY starttime DESC";

	$r = mysqli_query($con, $q);
	while ($d = mysqli_fetch_assoc($r)) {
		$index = $d['datee'] . " " . $d['starttime'] . " " . $d['endtime'];
		$reservedSlot[$index] = $d;
	}

	$query = "SELECT * FROM teacherslots WHERE teacherID=" . $_GET['teacherid'] . " LIMIT 1";
	$result = mysqli_query($con, $query);

	$dd = mysqli_fetch_assoc($result);

	$name = $dd[0];
	$mon_time = explode("-", $dd['mon_time']);
	$tue_time = explode("-", $dd['tue_time']);
	$wed_time = explode("-", $dd['wed_time']);
	$thur_time = explode("-", $dd['thur_time']);
	$fri_time = explode("-", $dd['fri_time']);
	$sat_time = explode("-", $dd['sat_time']);
	$sun_time = explode("-", $dd['sun_time']);

	$matches = null;
	$mon_add = preg_match('/(([0-9]){2}:){3}([0-9]){2}/', $dd['mon_additional'], $matches);
	$mon_add = explodeNth($matches[0], ":", 2);

	$mon_add_array = array();
	$mon_add_array[1] = timeDiff($mon_add[0], $mon_add[1]);
 
	$wkdays = getWeekdays($dd['mon_time'], $dd['tue_time'], $dd['wed_time'], $dd['thur_time'], $dd['fri_time'], $dd['sat_time'], $dd['sun_time']);

	$dateRange = createRange($dd['datee'], $dd['noofmonths'], $wkdays);
	
	$arrTimeDiff = array(); 
	$arrTimeDiff[1] = timeDiff($mon_time[0], $mon_time[1]);
	$arrTimeDiff[2] = timeDiff($tue_time[0], $tue_time[1]);
	$arrTimeDiff[3] = timeDiff($wed_time[0], $wed_time[1]);
	$arrTimeDiff[4] = timeDiff($thur_time[0], $thur_time[1]);
	$arrTimeDiff[5] = timeDiff($fri_time[0], $fri_time[1]);
	$arrTimeDiff[6] = timeDiff($sat_time[0], $sat_time[1]);
	$arrTimeDiff[7] = timeDiff($sun_time[0], $sun_time[1]);

	$str = "{
						\"success\": 1,
						\"result\":[";
			
			$current_date = new DateTime();
			$slotGID = 0;
			$startTime = "";
			$endTime ="";
			$toPrint = false;
			$timeRange = "";
			//all time range for calender
			$checkBoxTimeArray = array();
	
	foreach ($dateRange as $day) {	
		$q1 = "SELECT * FROM `teacher_absent` WHERE `datee` = '".$day."'";
		$result = mysqli_query($con, $q1);
		if(mysqli_num_rows($result) < 1){

		//temp time range for loop-specific day
		$timeArray = array();

		$tempDay = new DateTime($day);
		$timeDiff = (isset($arrTimeDiff[$tempDay->format("N")]['stime'])) ? $arrTimeDiff[$tempDay->format("N")] : 0;

		// Default time(dropdown) is unavailable, it will be set as available if slot is free.


		// first and last index of array
		$timeRange = $timeDiff['stime'][0] . " - " . end($timeDiff['etime']);

		for ($i = 0; $i < count($timeDiff['stime']); $i++) {
			$index = $day . " " . $timeDiff['stime'][$i] . " " . $timeDiff['etime'][$i];

			$timeArray[$timeDiff['stime'][$i]] = "unavailable";

			if (isset($reservedSlot[$index])) {

				$id = $reservedSlot[$index]["slot_group_id"];
				$status = $reservedSlot[$index]["isSlotCancelled"];
				$sdate = $day . " " . $timeDiff['stime'][$i];
				$edate = $day . " " . $timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i] . " - " . $timeDiff['etime'][$i];


				if ($reservedSlot[$index]["isSlotCancelled"] == 1) {
					continue;
				}

				if ($reservedSlot[$index]["isgapslot"] == 1) {
					continue;
				}

				if ($reservedSlot[$index]["slot_group_id"] == $slotGID) {
					continue;
				}

				if ($reservedSlot[$index]["slot_group_id"] != $slotGID) {
					$time = $reservedSlot[$index]["timee"];
					$slotGID = $reservedSlot[$index]["slot_group_id"];
					$toPrint = true;
				}

				if ($reservedSlot[$index]["isSlotCancelled"] == 1) {
					continue;
				}

				if ($reservedSlot[$index]['ownself'] == 1 && $reservedSlot[$index]["isClassTaken"] == 1) {
					//$title_desc = $title = "Completed Appointment";
					//$class = "event-special";
					$title_desc = $title = "Bijles is geaccepteerd";
					$class = "event-info";
					$onHoverBtn = "";

				} elseif ($reservedSlot[$index]['ownself'] == 1 && $reservedSlot[$index]["accepted"] == 1) {
					$title_desc = $title = "Bijles is geaccepteerd";
					$class = "event-info";
					$onHoverBtn = "(this, $id,1)";

				} elseif ($reservedSlot[$index]['ownself'] == 1 && $reservedSlot[$index]["accepted"] != 1) {
					$title_desc = $title = "In afwachting";
					$class = "event-warning";
					$onHoverBtn = "(this, $id,2)";
				} elseif ($reservedSlot[$index]['ownself'] == 0) {
					$title_desc = $title = "Reversed";
					$class = "event-danger";
					$onHoverBtn = "";
					$title_desc = "Reserved Slot";
				}
			} else {

				$timeArray[$timeDiff['stime'][$i]] = "available";

				$title = "Beschikbaar";
				$title_desc = "Beschikbaar";
				$class = "event-inverse";
				$id = '';
				$status = 0;
				$sdate = $day . " " . $timeDiff['stime'][$i];
				$edate = $day . " " . $timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i] . " - " . $timeDiff['etime'][$i];
				$onHoverBtn = "";
				$toPrint = true;
				$reservedSlot[$index]["isgapslot"] = 1;

			}

			if ($day < $current_date->format("Y-m-d")) {
				$onHoverBtn = "";
			}

			if ($day < $current_date->format("Y-m-d") && $title == "Beschikbaar") {
				continue;
			}

			$str .= "{
					\"id\": \"$id\",
					\"cbID\": \"$id\",
					\"start\": \"".strtotime($sdate)."000\", 
					\"end\": \"".strtotime($edate)."000\", 
					\"time\": \"$time\",
					\"timeRange\": \"$timeRange\",
					\"date\": \"$day\",
					\"status\": \"$status\",
					\"class\": \"" . $class . "\",
					\"title\": \"" . $title . "\",
					\"title_desc\": \"" . $title_desc . "\",
					\"onHoverBtn\": \"" . $onHoverBtn . "\",
					\"name\": \"" . $name . "\",

					\"url\": \"\"
				},";


		}//for loop ends
		//inserting all time range
		$checkBoxTimeArray[$day] = $timeArray;
	}// for each loop ends
	
}
			$checkBoxTimeArray = json_encode($checkBoxTimeArray);
	
			$str = rtrim($str, ",");
			$str .= "], \"timeRange\": $checkBoxTimeArray}";

	echo $str;

}//if $_GET teacherID
elseif (isset($_GET['dashboard'])) {
	//$query = "SELECT c.firstname, c.lastname, c.address, cb.calendarbookingID, cb.teacherID, cb.datee, cb.starttime, cb.endtime, cb.accepted, cb.isgapslot, cb.slot_group_id, (SELECT CONCAT(MIN(ccb.starttime),\" - \",MAX(ccb.endtime)) FROM calendarbooking ccb WHERE ccb.slot_group_id=cb.slot_group_id AND ccb.isgapslot=0 GROUP BY ccb.slot_group_id )timee FROM contact c RIGHT JOIN calendarbooking cb ON cb.teacherID=c.userID AND contacttypeID=1 where cb.studentID=".$_SESSION['StudentID']."  ORDER BY cb.starttime DESC";

	$query = "SELECT c.firstname, c.lastname, c.place_name, cb.calendarbookingID, cb.teacherID, cb.datee, cb.starttime, cb.endtime, cb.accepted, cb.isgapslot, cb.slot_group_id, cb.isClassTaken , (SELECT CONCAT(MIN(ccb.starttime),' - ',MAX(ccb.endtime)) FROM calendarbooking ccb WHERE ccb.slot_group_id=cb.slot_group_id AND ccb.isgapslot=0 GROUP BY ccb.slot_group_id )timee FROM contact c RIGHT JOIN calendarbooking cb ON cb.teacherID=c.userID AND contacttypeID=1 where cb.studentID=" . $_SESSION['StudentID'] . " AND cb.isgapslot=0 ORDER BY  datee, starttime ASC";
	$result = mysqli_query($con, $query);

	$str = "{
					\"success\": 1,
					\"result\":[";

	$current_date = new DateTime();
	$slotGID = 0;
	$startTime = "";
	$endTime = "";
	$toPrint = false;
	$time = "";

	while ($row = mysqli_fetch_assoc($result)) {
		$title = "";
		$title_desc = "";
		$class = "";
		$onHoverBtn = "";

		if ($row["isgapslot"] == 1) {
			continue;
		}

		if ($row["slot_group_id"] == $slotGID) {
			continue;
		}

		if ($row["slot_group_id"] != $slotGID) {
			$time = $row["timee"];
			$slotGID = $row["slot_group_id"];
			$toPrint = true;
		}


		if ($row["slot_group_id"]) {
			if ($row["isClassTaken"] == 1) {
				//$title = "Completed Appointment";
				//$title_desc = "<a href='teacher-profile?tid=$row[teacherID]'>$row[firstname]</a>";
				//$class = "event-special";
				$title = "Bijles is geaccepteerd";
				$title_desc = "<a href='teacher-profile?tid=$row[teacherID]'>$row[firstname]</a>";
				$class = "event-info";
				$onHoverBtn = "(this, $row[slot_group_id],1)";
				$eventType = "Afspraak";
			} elseif ($row["accepted"] == 1) {
				$title = "Bijles is geaccepteerd";
				$title_desc = "<a href='teacher-profile?tid=$row[teacherID]'>$row[firstname]</a>";
				$class = "event-info";
				$onHoverBtn = "(this, $row[slot_group_id],1)";
				$eventType = "Afspraak";
			} elseif ($row["accepted"] != 1) {
				$title = "In afwachting";
				$title_desc = "<a href='teacher-profile?tid=$row[teacherID]'>$row[firstname]</a>";
				$class = "event-warning";
				$onHoverBtn = "(this, $row[slot_group_id],2)";
				$eventType = "Verzoek";
			}
		}


		if ($row['datee'] < $current_date->format("Y-m-d")) {
			$onHoverBtn = "";
		}

		$formattedMonth = getBetween($row['datee'], '-', '-');
		$dateObj = DateTime::createFromFormat('!m', $formattedMonth);
		$monthName = $dateObj->format('F'); // March

		$daystring = strtotime($row['datee']);
		$dayOnly = date('d', $daystring);

		$streetName = getBetween('/' . $row['place_name'], '/', ',');
		$cityWithNumbers = getBetween($row['place_name'], ',', ',');
		$city = preg_replace('/[0-9]+/', '', $cityWithNumbers);

		$str .= "{
						\"id\": \"$row[slot_group_id]\",
						\"cbID\": \"$row[slot_group_id]\",
						\"start\": \"" . strtotime("$row[datee] $row[starttime]") . "000\",
						\"end\": \"" . strtotime("$row[datee] $row[endtime]") . "000\",
						\"time\": \"$time\",
						\"date\": \"$row[datee]\",
						\"class\": \"" . $class . "\",
						\"title\": \"" . $title . "\",
						\"title_desc\": \"" . $title_desc . "\",
						\"onHoverBtn\": \"" . $onHoverBtn . "\",
						\"teacherName\": \"$row[firstname]\",   
						\"classTaken\": \"$row[isClassTaken]\",  
						\"location\": \"$row[place_name]\",

						\"day\": \"$dayOnly\", 
						\"month\": \"$monthName\", 
						\"streetName\": \"$streetName\", 
						
						\"city\": \"$city\",  
						\"eventType\": \"$eventType\", 

						\"url\": \"\"
					},";

	}
	$str = rtrim($str, ",");
	$str .= "]}";
	echo $str;

}//if GET dashboard ends

//--------------------------
//		Functions
//--------------------------
function getBetween($content, $start, $end)
{
	$r = explode($start, $content);
	if (isset($r[1])) {
		$r = explode($end, $r[1]);
		return $r[0];
	}
	return '';
}


function timeDiff($t1, $t2)
{
	$time1 = strtotime($t1);
	$time2 = strtotime($t2);
	$diff = intval(($time2 - $time1) / 3600); //from sec to min
	$slot_time = SLOT_TIME_IN_MINIUTES;//45;
	$gap = SLOT_TIME_GAP_IN_MINIUTES + SLOT_TIME_IN_MINIUTES; //15
	$sArray = array();
	$eArray = array();
	$new = $time1;
	$tmp_etime = strtotime("+$slot_time minute", $new);
	do {
		$sArray[] = date("H:i", $new);
		$eArray[] = date("H:i", $tmp_etime);
		$new = strtotime("+$gap minute", $new);
		$tmp_etime = strtotime("+$gap minute", $tmp_etime);
		$diff--;
	} while ($time2 > $tmp_etime);

	return array("diff" => $diff, "stime" => $sArray, "etime" => $eArray);
}

function createRange($s, $n, $wkdays)
{
	$tmpDate = new DateTime($s);
	$tmpDate->modify("-2 Month");
	$tmpEndDate = new DateTime();
	$tmpEndDate->modify("+$n");

	$outArray = array();
	do {

		//add selected day
		if (strpos($wkdays, $tmpDate->format('D')) > -1) {
			// echo $tmpDate->format('D')." : ".strpos($wkdays, $tmpDate->format('D'));
			$outArray[] = $tmpDate->format('Y-m-d');
		}

	} while ($tmpDate->modify('+1 day') <= $tmpEndDate);
	return $outArray;
}


function getWeekdays($m, $t, $w, $th, $f, $sat, $sun)
{
	$str = "";
	$str .= ($m) ? "Mon" : "";
	$str .= ($t) ? ",Tue" : "";
	$str .= ($w) ? ",Wed" : "";
	$str .= ($th) ? ",Thur" : "";
	$str .= ($f) ? ",Fri" : "";
	$str .= ($sat) ? ",Sat" : "";
	$str .= ($sun) ? ",Sun" : "";
	return $str;
}

function calculateCostPerMintues($t1, $t2, $rate)
{
	$start_date = new DateTime("2009-09-01 $t1");
	$since_start = $start_date->diff(new DateTime("2009-09-01 $t2"));

	//minutes + (hour*60)
	$minutesOfDiff = $since_start->i + ($since_start->h * 60);
	$cost = ($minutesOfDiff / UNIT_TIME_FOR_COST_CALCULATION) * $rate;
	return round($cost, 2);
}

function getDutchMonth($mon)
{
	$months = array(
		"Jan" => "Jan",
		"Feb" => "Feb",
		"Mar" => "Mrt",
		"Apr" => "Apr",
		"May" => "Mei",
		"Jun" => "Jun",
		"Jul" => "Jul",
		"Aug" => "Aug",
		"Sep" => "Sept",
		"Sept" => "Sept",
		"Oct" => "Okt",
		"Nov" => "Nov",
		"Dec" => "Dec"
	);
	return $months[$mon];
}

function explodeNth(string $source, string $separator = ';', int $nth = 2) {
    if (trim($source) === '' || trim($separator) === '' || $nth <= 1) {
      return [];
    }
  
    $results = [];
  
    $parts = explode($separator, $source);
    for ($i = 0; $i < count($parts); $i = $i + $nth) {
        $results[] = implode($separator, array_slice($parts, $i, $i + $nth));
    }
  
    return $results;
}

?>