<?php
$thisPage="Teachers Registration";
session_start();
$_SESSION['isSlotBooked']=false;
/*if(!isset($_SESSION['Admin']))
{
	header('Location: index.php');
}
else
{*/
	?>



<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Teachers Registration</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
	
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />	
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	
 <link rel="stylesheet" href="calendar/css/calendar.css">	
<style type="text/css">
    #calendar { background-color:#fafafa !important; }
    .cal-month-day{color:black;}
    .cal-row-head {background-color: #fafafa !important;}
    .empty {  background-color: white !important; border: 2px solid black;}    
    .inline_time { color: #50d38a !important; }  
    .inline_time_disabled { color: #6b7971 !important; }
    #cal-slide-content{font-family:inherit; color:#bdbdbd;}
    .page-header{height: 0 !important;padding-bottom: 0px;margin: -25px 0 50px;border-bottom: 0px;}
    .page-calendar #calendar{max-width: 100%;}
	
    .inline-elem-button{margin-top: 20px;} 
    .form-control, .btn-round.btn-simple{color: #ece6e6 !important;}
    .form-control{height: auto !important; }
	.input-group-addon{color:#ece6e6;}
    .text{color:black;}
    .margin-top{margin-top:10px;}
    .checkbox{float: left;margin-right: 20px;}
    .margintop{margin-top: 20px; color:#50d38a;}  
    	.mapboxgl-ctrl-attrib, .mapboxgl-ctrl-logo{display:none !important;}	
		.mapboxgl-ctrl-geocoder li > a {color: black !important;}
		.mapboxgl-ctrl-geocoder input {color: black !important}
	li.list-item {clear: both;}
	.inline-elem-updateButton{clear:both; display: block;}
	.marginTop{margin-top: 45px;}
	.btn-group
	{
	  background:#30373e;
	}
button.btn.dropdown-toggle {
    background: #30373e;
}	
.wizard .content label.error {
    color: #e45e5e !important;
}	
	/*Placeholder Color */
	
pre#coordinates {
    display: none;
}
._hide{display:none;}

input{	
border: 1px solid #bdbdbd !important;
color: white !important;
	background: #30373e !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;
color: white !important;
		background: #30373e !important;
	}
	
	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
		overflow: hidden !important;
		min-height:400px !importan;
		margin-top: 50px !important;
		
	}
	
	.wizard .content label {
    color: #486066 !important;
}
	.wizard>.steps .current a 
	{
		background-color: #5CC75F !Important;
		border-radius:6px;
	}
	.wizard>.steps .done a
	{
		background-color: #E8E8E8 !Important;
		border-radius:6px;
	}
	.wizard .steps a, .wizard .steps a:hover {border-radius:6px;}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #FAFBFC  !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	.navbar.p-l-5.p-r-5
	{
		display: none !important;
	}
	
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	label#school-course1-error {
    margin-top: 25px;
}
	ul.unstyled.list-unstyled {
    padding-bottom: 47px;
}
	section.content{
		padding-top: 29px;
	}
#fc_frame, #fc_frame.fc-widget-normal {
    bottom: 48px !important;
}
.btn-group {
    background: transparent;
}

.form-control, .btn-round.btn-simple {
    color: #486066 !important;
}
button.btn.dropdown-toggle {
    background: transparent;
}
.btn.btn-simple {
     border-color: #486066 !important; 
}

span.caret{display:none;}
	
	.custom-error label.error
	{
		color: #e45e5e !important;
		position: inherit;	
		top: -21px;
	
	}
#-error {
    float: right;
}	
	.mapboxgl-canvas {
  left: 0;
}
label#interview-error {
    float: right;
    margin-right: 56px;
} 
.theme-green section.content:before {
     background-color: transparent !important; 
}
input[type=date].form-control, input[type=time].form-control, input[type=datetime-local].form-control, input[type=month].form-control{
	line-height: 1.42857143 !important; 
}
.dropdown-menu {
    margin-top: 46px !important;
}
.glyphicon{font-family: 'Glyphicons Halflings' !important;}
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('Admin/includes/header.php');
       // require_once('Admin/includes/sidebarAdminDashboard.php');
?>

<!-- Main Content -->
<section class="content" style="margin: 0px; position: relative !important;">
    <div class="block-header">
        <div class="row">
            <div class="col-lg-8 col-md-8 col-sm-12">
                <h2>Registreren als bijlesdocent</h2>
            </div>            
        <!--    <div class="col-lg-4 col-md-4 col-sm-12 text-right">
                <ul class="breadcrumb float-md-right">
                    <li class="breadcrumb-item"><a href="index.php"><i class="zmdi zmdi-home"></i>&nbsp;&nbsp;&nbsp;Back to Home</a></li>
                   
                </ul>
            </div> -->
        </div>
    </div>
    <div class="container-fluid">
        <div class="row clearfix">
            <div class="col-lg-12 col-md-12 col-sm-12">
                
                    
                        <form id="teacherRegistration" enctype="multipart/form-data">
							
														
						<h4>Inloggegevens</h4>


							<fieldset>
								
								<!--<div class="form-group form-float">
									<label for="username">Username</label>-->

<div class="row">
	<div  class="col-sm-4"></div>
	<div class="col-sm-4">


								<!--	<div class="form-group form-float">
										<label for="username">Skype ID *</label>-->
										<input type="hidden" class="form-control" placeholder="Skype ID" name="skypeid" id="skypeid"/>
							<!--		</div>  -->
							
							
								
									<div class="form-group form-float">
									<label for="username">E-mailadres *</label>
                                    <input type="hidden" class="form-control" placeholder="E-mailadres" name="choice" id="choice" value="Teacher-Registration" />    
									<input type="hidden" class="form-control" placeholder="User Group" name="usergroup" id="usergroup" value="Teacher" />										
									<input type="email" class="form-control" placeholder="E-mailadres" name="email" id="email" required/>
									<input type="hidden"  name="checkEmail" id="checkEmail" value="" />
									</div>	




							

								<!--	<input type="text" class="form-control" placeholder="Username" name="username" id="username" required/>
									</div>-->




									<div class="form-group form-float">
									<label for="username">Wachtwoord *</label>




									<input type="password" class="form-control" placeholder="Wachtwoord" name="password" id="password" minlength="8"  required/>


									 
									</div>
									<div class="form-group form-float">
									<label for="username">Bevestig wachtwoord *</label>
									<input type="password" class="form-control" placeholder="Bevestig wachtwoord" name="confirm_password" id="confirm_password" minlength="8" required/>
	
									</div>	

</div>															
</div>	

							</fieldset>




                            <h4>Persoonlijke informatie</h4>
                            <fieldset>
								
                            <div class="row">
	<div  class="col-sm-3"></div>
	<div class="col-sm-6">
								
								<div class="form-group form-float">
									<label for="booking-code">Voornaam *</label>
									<input type="text" class="form-control" placeholder="Voornaam" name="first-name" id="first-name" required>
                                    
                                </div>


								<div class="form-group form-float">
									<label for="booking-code">Achternaam *</label>
									<input type="text" class="form-control" placeholder="Achternaam" name="last-name" id="last-name" required>
                                    
                                </div>
								
								 
								<div class="">
									<div class="row">
									
									<div class="col-lg-6 col-md-6 col-sm-12 form-group form-float">
                                    <label for="booking-code">Geboortedatum *</label>
									
									
                                    <input type="date" class="form-control" min="1900-01-01" max="2010-12-31" placeholder="dd/mm/yyyy" name="dob" id="dob" required style="background: transparent !important;">

									
									</div>
										
									 <div class="col-lg-6 col-md-6 col-sm-12 h-25 form-group form-float">
										 <label for="enfants">Telefoonnummer *</label>
                                    <input type="text" class="form-control" placeholder="Telephone" name="telephone" id="telephone" required>
									</div>
									 										
									</div>




								<div class="row" style="width:100%;height:220px">
										
                                    <div class="col-lg-12 col-md-12 col-sm-12 form-group form-float custom-error">
										<label for='enfants' style="color: white !important;">My Address *</label>
										 <div id='map' style='position:absolute; top: 0;bottom: 0;width: 100%; height: 100%;  overflow:visible;'></div>
									</div>
									
									</div>
                                </div>




                                	



										
						 
								
							<pre id='coordinates' class='coordinates' style="display:none"></pre>
							<input type="hidden" class="form-control" placeholder="lat" name="lat" id="lat" value=""/>
							<input type="hidden" class="form-control" placeholder="lng" name="lng" id="lng" value=""/>
							<input type="hidden" class="form-control" placeholder="place_name" name="place_name" id="place_name" value=" "/>
								
							</div>
							</div>	
						
								
                               
							</fieldset>
                			
							<h4>Vakken en reisafstand</h4>
                            <fieldset>

     <div class="row">
	<div  class="col-sm-2"></div>
	<div class="col-sm-8">



                                <div class="form-group form-float">
                                    <label for="booking-code">Ik ben bereid om, naast aan huis, ook online videobijles te geven: *</label>
                                   <label style="font-weight:normal;"> <input type="radio" class="" placeholder="" name="online-courses" id="online-courses" value="Yes" required> <span style="color: white;">Ja</span></label>
                                    <label style="font-weight:normal;"><input type="radio" class="" placeholder="" name="online-courses" id="online-courses" value="No" required> <span style="color: white;">Nee</span></label>
                                </div> 

                                <div class="row">
                                    
                                    <div class="col-md-12 col-lg-12 col-sm-12">
                                <div class="card" style="background: transparent !important;">
                    <div class="header">
                        <h2> <small>Selecteer hier alle vakken waarin je bijles wilt geven. Indien je minder sterk bent in een bepaald vak, kan je overwegen om dit vak alsnog te selecteren op een lager niveau. Dit kan waardevol zijn voor onze jongere leerling indien jij goede didactische vaardigheden hebt.</small> </h2>
                        
                    </div>
                    <div class="body courses-div">
						<div id="newCourse">
                        <div class="row clearfix" id="course-row1">
							<input type="hidden" name="counter" class="counter" value="1" />
                          	<input type="hidden" id="__fieldcounter" name="fieldcounter" value="1"/>
                            <div class="col-lg-3 col-md-6">
                                <p> <b>Schooltype  </b> </p>
                                    <select class="form-control show-tick school-type dynamic-course" id="school-type1" data-id="1" name="select-school-type1[]" >
                                  
										
                                    <?php
									
										$stmt = $con->prepare("SELECT DISTINCT typeID, typeName from schooltype group by typeName");
										$stmt->execute();
										$stmt->bind_result($typeID,$typeName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $typeID; ?>"><?php echo $typeName; ?></option>
									<?php
										}
									?>

                                </select>
									   
								
                            </div>
							
							<div class="col-lg-3 col-md-6 form-group form-float">
								<p> <b>Schoolvak *</b> </p>
                                <select class="form-control show-tick school-course" name="school-course1[]" id="school-course1" data-id="1" required>
								
                                </select>
								
                            </div>
							<div class="col-lg-3 col-md-6">
                                <p> <b>Niveau </b> </p>
                                <select class="form-control show-tick school-level" id="school-level1" data-id="1" name="school-level1[]">
									
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoollevel");
										$stmt->execute();
										$stmt->bind_result($levelID,$levelName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $levelID; ?>"><?php echo $levelName; ?></option>
									<?php
										}
									?>
                                </select>
                            </div>
							<div class="col-lg-3 col-md-6">
                                <p> <b>Leerjaar </b> </p>
                                <select class="form-control show-tick school-year " id="school-year1" multiple="multiple" data-id="1"  name="school-year1[]">
									
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoolyear");
										$stmt->execute();
										$stmt->bind_result($yearID,$yearName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $yearID; ?>"><?php echo $yearName; ?></option>
									<?php
										}
									?>
                                </select>
                            </div>
                        </div>
						</div>
						<br/><br/><br/>
						<div class="row">
							<div class="col-md-4 col-sm-12"></div>
							<div class="col-md-4 col-sm-12">
							<button type="button" class="btn btn-raised m-b-10 bg-green btn-block btn-primary waves-effect text-center" id="add-more-courses" style="
padding: 20px 20px 20px 20px;
margin: 0 auto !important;"> EXTRA VAK TOEVOEGEN </button>	 
							</div>
							<div class="col-md-4 col-sm-12"></div>
						</div>
							
                    </div>
									
                </div>      
                            </div>
								</div>
								<div class="row">
                                <div class="col-sm-12">
                                    
                                    <div class="form-group form-float">
                                    <label for="booking-code">Welke afstand ben je maximaal bereid te reizen? *</label>
                                    <input type="number" class="form-control" placeholder="Distance" name="distance" id="distance" min="3" max="50" required>
                                    
                                </div>
                                </div>

                                  <div class="col-lg-12 col-md-12 col-sm-12 form-group form-float">
                                    <label for="booking-code">Een korte motivatie *</label>
                                    <input type="text" class="form-control" placeholder="Motivatie" name="motivation" id="motivation" required>
									</div>
                                </div>   


                                </div>
                                </div>

                            </fieldset>

                            
                        </form>
                   
                
            </div>
        </div>
        <!-- #END# Basic Examples --> 
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


	
	<!-- Jquery steps Wizard Code -->
	<script type="text/javascript">
	$(function () {
		
		 $(window).keydown(function(event){
			if(event.keyCode == 13) {
			  event.preventDefault();
			  return false;
			}
		  });
		
		
		
    //Advanced form with validation
    var form = $('#teacherRegistration').show();
    form.steps({
        headerTag: 'h4',
        bodyTag: 'fieldset',
        onInit: function (event, currentIndex) {
          //  $.AdminOreo.input.activate();
            //Set tab width
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            //set button waves effect
            setButtonWavesEffect(event);
        },
        onStepChanging: function (event, currentIndex, newIndex) {
			
			if(newIndex == 1 && mapCheck)
			{
			    initMap();
				mapCheck = false;
			}
			
			
//			else if (newIndex > currentIndex){
//				$("#map").html(" ");
//			initMap();
	//		}
	//		else {
				//alert("m1"+currentIndex+" "+newIndex);
	//			$("#map").html(" ");
				
	//		}
			
            if (currentIndex > newIndex) {mapCheck = false; return true;  }
			
			

            if (currentIndex < newIndex) {
				mapCheck = false;				
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');

            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
			//Jquery AJAX code to add client upon  insertion
            
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
			$("#lat").val(lat);
			$("#lng").val(lng);
			$("#place_name").val(place_name);
			$("#teacherid").val(teacherid);
					
            //Code for Forms Data Submission
			//run_waitMe($('body'), 1, "timer");
			$.ajax({
                            url: 'actions/admin_edit_scripts.php',
                            type: 'POST',
                            data: new FormData(this),
                            contentType: false,       
                            cache: false,           
                            processData:false,
                             success: function (result) {


                          
                                   switch(result)
                                    {

                                      case "Error":
                                    
                                        
                                        setTimeout(function() {   
                                                 
                                               $('body').waitMe('hide');
                                               $('#teacherRegistration')[0].empty(); 
                                                showNotification("alert-danger","Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen","top","center","","");
                                               //$("#error-epm").html("There Was An Error Inserting The Record."); 
                                                  
}, 500); 
                                        break;

                                         case "Success":
                                    
                                       showNotification("alert-success","Successfully ! Tutor is registered","bottom","center","","");
                                       setTimeout(function() { 

                                               //   $('body').waitMe('hide');
                                                 $('#teacherRegistration')[0].reset();
										  		document.location.reload();
                                              
                                                 
                                                                                                    
}, 500);
                                        break;

                                    
                                    }
                              }
                      });
		
        }
    });
	$.validator.addMethod("notEqual", function(value, element, param) {
	 return this.optional(element) || value != $(param).val();
	}, "Email, Already Exist.");
		
		
	 $.validator.addMethod("telephone",function(value,element){
                return this.optional(element) || /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(value);
            },"Invalid Number");
		
    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            confirm_password: {
                equalTo: '#password'
            },
			"select-course": "required",
			email:
			{
				notEqual: '#checkEmail',
			},
			telephone:{
				telephone: "#telephone"
			},
			'parent-telephone':{
				telephone: "#parent-telephone"
			}
        } // end of rules
		
    });
	$("#email").keyup(function(){
		  var email = $("#email").val();
		  var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
		  if(regex.test(email))
		  {
				$.ajax({
					url:'actions/email.php',
					data: {'email': email},
					type:'POST',
					success:function(result)
					{
						$("#checkEmail").val(result);
					}
				});
		  }		
	});
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}
		
		
		function showNotification(colorName, text, placementFrom, placementAlign, animateEnter, animateExit) {
    if (colorName === null || colorName === '') { colorName = 'bg-black'; }
    if (text === null || text === '') { text = 'Turning standard Bootstrap alerts'; }
    if (animateEnter === null || animateEnter === '') { animateEnter = 'animated fadeInDown'; }
    if (animateExit === null || animateExit === '') { animateExit = 'animated fadeOutUp'; }
    var allowDismiss = true;

    $.notify({
        message: text
    },
        {
            type: colorName,
            allow_dismiss: allowDismiss,
            newest_on_top: true,
            timer: 1000,
            placement: {
                from: placementFrom,
                align: placementAlign
            },
            animate: {
                enter: animateEnter,
                exit: animateExit
            },
            template: '<div data-notify="container" class="bootstrap-notify-container alert alert-dismissible {0} ' + (allowDismiss ? "p-r-35" : "") + '" role="alert">' +
            '<button type="button" aria-hidden="true" class="close" data-notify="dismiss">×</button>' +
            '<span data-notify="icon"></span> ' +
            '<span data-notify="title">{1}</span> ' +
            '<span data-notify="message">{2}</span>' +
            '<div class="progress" data-notify="progressbar">' +
            '<div class="progress-bar progress-bar-{0}" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>' +
            '</div>' +
            '<a href="{3}" target="{4}" data-notify="url"></a>' +
            '</div>'
        });
}
		
	
	</script>  
<?php
    require_once('Admin/includes/footerScriptsAddForms.php');
?>
<!--<script src="assets/js/pages/calendar/calendar.js"></script>-->
<script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script>
<script>
	  
	  function delete_btn(id)
	  {
		  var c_id;
		  if(id == "1")
		  {
		   c_id = "#id-card";
			  //alert(c_id);
		  } else if(id == "2")
		  {
		  	c_id = "#school-docs";
			  //alert(c_id);
		  }
		  
		  $(c_id).replaceWith($(c_id).val('').clone(true));
	  }
  $(document).ready(function() {
	
	  
	  /* Fetch Pre Information for courses */
	  
	   function fetch_preinfo(cat, className, rowID) { 

         $.ajax({
                            url: 'actions/fetch_preinfo.php',
                            data: {'cat': cat},
				  			type: "POST",
							//dataType: "json",
                             success: function (result) {
								//$(this).parent().parent().siblings().find(".school-course").html("");
								 var rowselector = "#";
								 var selector = ".";
								 //var separator ">";
								 var classNam = rowselector.concat(rowID);
								 var classNam1 = selector.concat(className);
								 $(classNam).children().find(classNam1).not(".btn-group",".bootstrap-select").html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                	$(classNam).children().find(classNam1).not(".btn-group",".bootstrap-select").append('<option value="'+String(json_obj[i].ID)+'">'+String(json_obj[i].Name)+'</option>');
								}
					
								  $(classNam).children().find(classNam1).not(".btn-group",".bootstrap-select").selectpicker('refresh');
							}
                      });

    }
	  // date function...
/*	function parseDMY(value) {
    var date = value.split("/");
    var d = parseInt(date[0], 10),
        m = parseInt(date[1], 10),
        y = parseInt(date[2], 10);
    return new Date(y, m - 1, d);
	}  */
	  
	  fetch_preinfo("SchoolType","school-type","course-row1");
	  fetch_preinfo("SchoolLevel","school-level","course-row1");
	  fetch_preinfo("SchoolYear","school-year","course-row1");
	  fetch_preinfo("SchoolCourse","school-course","course-row1");

	  	
	  var __obj = null;
	
	$(document).on("change", ".school-type" , function(e) {
		  e.stopImmediatePropagation();
		
		  __obj = $(this).parent().parent().parent();
									 
		  var id = $(__obj).find(".school-type:eq(1)").attr("id");		
		  var stype = $("#"+id).val();
		  stype = (typeof(stype) == "object")? stype.join(",") : stype;
		   
		
		  var lid = $(__obj).find(".school-level:eq(1)").attr("id");
	
		  var yid = $(__obj).find(".school-year:eq(1)").attr("id");
			
			if(stype == "<?php echo ELEMENTRY_TYPE_ID; ?>"){
				$("#"+yid).parent().parent().css("display", "none");
				$("#"+yid).prop("disabled", true);
				$("#"+yid +" [value='']").attr('selected', 'true');
				
				$("#"+lid).parent().parent().css("display", "none");
				$("#"+lid).prop("disabled", true);
				$("#"+lid +" [value='']").attr('selected', 'true');
			}else{
				
				$("#"+yid).parent().parent().css("display", "block");
				$("#"+yid).prop("disabled", false);
				
				$("#"+lid).parent().parent().css("display", "block");
				$("#"+lid).prop("disabled", false);
			}
		   $.ajax({
                            url: 'actions/fetch_courses2.php',
                            data: {'typeID': stype, 'levelID':'', 'yearID':'', tid:false},
				  			type: "POST",
							//dataType: "json",
                            success: function (result) {
									id = $(__obj).find(".school-course:eq(1)").attr("id");
                					$("#"+id).html(result);
									$("#"+id).selectpicker('refresh');
							}
                      }); 
		  
	  }); //show-tick ends
	  
	  $(document).on("change", ".school-course" , function(e){		  
		  e.stopImmediatePropagation();
		  
		   __obj = $(this).parent().parent().parent();
	  		
		  var cid = $(this).val();
		  
		  $.ajax({
			  url: 'actions/course_filter.php',
			  data: {'subpage': "courselevel", cid:cid},
			  type: "POST",
			  success: function (result) {
				  id = $(__obj).find(".school-level:eq(1)").attr("id");
                					$("#"+id).html(result);
									$("#"+id).selectpicker('refresh');
			  }
		  }); //ajax ends
		  
	  }); // $("#school-course1").change() ends
	  
	  
	   $(document).on("change", ".school-level" , function(e){		  
		  e.stopImmediatePropagation();
		  
		   __obj = $(this).parent().parent().parent();
		   
		   var id = $(__obj).find(".school-course:eq(1)").attr("id");		
		  var scourse = $("#"+id).val();
		  scourse = (typeof(scourse) == "object")? scourse.join(",") : scourse;
	  		
		  var clid = $(this).val();
		  
		  $.ajax({
			  url: 'actions/course_filter.php',
			  data: {'subpage': "courseyear", clid:clid, cid:scourse},
			  type: "POST",
			  success: function (result) {
				  id = $(__obj).find(".school-year:eq(1)").attr("id");
                					$("#"+id).html(result);
									$("#"+id).selectpicker('refresh');
			  }
		  }); //ajax ends
		  
	  }); // $(".school-level").change() ends

  
	  
	  /* Add More Courses */
	   /* Courses AJAX Calls */
      $( "#add-more-courses" ).click(function() {
		  
		  var count = $(".body").children().find(".row").length-1;
		  __fieldcounter++;
		  var te =  __fieldcounter;
		  $("#newCourse").append('<div class="row clearfix marginTop" id="course-row'+te+'" ><input type="hidden" name="counter" class="counter" value="'+te+'" /><div class="col-lg-3 col-md-6"><p> <b>Select School Type *</b> </p><select class="form-control show-tick dynamic-course school-type" id="school-type'+te+'" data-id="'+te+'" name="select-school-type'+te+'[]" ></select></div><div class="col-lg-3 col-md-6"><p> <b>Select School Course *</b> </p><select class="form-control show-tick school-course" name="school-course'+te+'[]" id="school-course'+te+'" data-id="'+te+'"></select></div><div class="col-lg-3 col-md-6"><p> <b>Select School Level *</b> </p><select class="form-control show-tick school-level"  id="school-level'+te+'" data-id="'+te+'" name="school-level'+te+'[]"></select></div><div class="col-lg-3 col-md-6"><p> <b>Select School Year *</b> </p><select class="form-control show-tick school-year" data-id="'+te+'"  id="school-year'+te+'" multiple="multiple" name="school-year'+te+'[]"></select></div></div>');
		  fetch_preinfo("SchoolType","school-type","course-row"+te);
		  $("#school-level"+te).selectpicker('refresh');
		  $("#school-course"+te).selectpicker('refresh');
		  $("#school-year"+te).selectpicker('refresh');
		  
		  $("#__fieldcounter").val(__fieldcounter);
		  
	  });

  });

	
	
	
	
	
</script>
	
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>	
	
	<script>
	var map;
	var canvas;
	var lat = 0;
	var lng =0;
	var place_name="";
	var mapCheck = true;		
function initMap()
		{
			
				
mapboxgl.accessToken = "<?php echo ACCESS_TOKEN; ?>";
map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11',
//center: [-79.4512, 43.6568],
	center: [5.17917, 52.10333],
zoom: 7
});
 
var geocoder = new MapboxGeocoder({
accessToken: mapboxgl.accessToken, limit:10, mode:'mapbox.places', types:'address' ,countries: 'nl'
});
 
map.addControl(geocoder);
 
// After the map style has loaded on the page, add a source layer and default
// styling for a single point.
map.on('load', function() {
// Listen for the `result` event from the MapboxGeocoder that is triggered when a user
// makes a selection and add a symbol that matches the result.
geocoder.on('result', function(ev) {
	
	lat = ev.result.center[1];
	lng = ev.result.center[0];
	place_name = ev.result.place_name;
	isAddressChanged = true;
map.getSource('point').setData(ev.result.geometry);
});
});








canvas = map.getCanvasContainer();
 
var geojson = {
"type": "FeatureCollection",
"features": [{
"type": "Feature",
"geometry": {
"type": "Point",
"coordinates": [5.17917, 52.10333]
}
}]
};
 
function onMove(e) {
var coords = e.lngLat;
 
// Set a UI indicator for dragging.
canvas.style.cursor = 'grabbing';
 
// Update the Point feature in `geojson` coordinates
// and call setData to the source layer `point` on it.
geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
map.getSource('point').setData(geojson);
	
}
 
function onUp(e) {
	
var coords = e.lngLat;
 
	lat = e.lngLat.lat;
	lng = e.lngLat.lng;	
	isAddressChanged = true;

// Print the coordinates of where the point had
// finished being dragged to on the map.
coordinates.style.display = 'none';
//coordinates.innerHTML = 'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat;
canvas.style.cursor = '';
	geocoder['eventManager']['limit'] = 1;	
	geocoder['options']['limit'] = 1;	
   geocoder.query(lng+","+lat);
  
	
// Unbind mouse/touch events
map.off('mousemove', onMove);
map.off('touchmove', onMove);
	geocoder['eventManager']['limit'] = 10;	
	geocoder['options']['limit'] = 10;	
}

map.on('load', function() {
map.resize();
// Add a single point to the map
map.addSource('point', {
"type": "geojson",
"data": geojson
});

map.addLayer({
"id": "point",
"type": "circle",
"source": "point",
"paint": {
"circle-radius": 10,
"circle-color": "#3887be"
}
});
 
// When the cursor enters a feature in the point layer, prepare for dragging.
map.on('mouseenter', 'point', function() {
map.setPaintProperty('point', 'circle-color', '#3bb2d0');
canvas.style.cursor = 'move';
});
 
map.on('mouseleave', 'point', function() {
map.setPaintProperty('point', 'circle-color', '#3887be');
canvas.style.cursor = '';
});
 
map.on('mousedown', 'point', function(e) {
// Prevent the default map drag behavior.
e.preventDefault();
 
canvas.style.cursor = 'grab';
 
map.on('mousemove', onMove);
map.once('mouseup', onUp);
});
 
map.on('touchstart', 'point', function(e) {
if (e.points.length !== 1) return;
 
// Prevent the default map drag behavior.
e.preventDefault();
 
map.on('touchmove', onMove);
map.once('touchend', onUp);
});

});

$(".mapboxgl-ctrl-geocoder input").prop("required", true);
}

</script>	
<!-----------------------------------reserve calendar---------------------->	
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
    <script type="text/javascript" src="calendar/js/calendar.js"></script>
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->

<script>
	
	var __counter = 0;
	var __fieldcounter = 1;
     // this function is added
Date.prototype.setTimezoneOffset = function(minutes) { 
    var _minutes;
    if (this.timezoneOffset == _minutes) {
        _minutes = this.getTimezoneOffset();
    } else {
        _minutes = this.timezoneOffset;
    }
    if (arguments.length) {
        this.timezoneOffset = minutes;
    } else {
        this.timezoneOffset = minutes = this.getTimezoneOffset();
    }
    return this.setTime(this.getTime() + (_minutes - minutes) * 6e4);
};


 var calendar;
 var options;

   var teacherid = window.performance.now() +""+Math.floor(100000000 + Math.random() * 99999999999999);
    var hourlyCost = <?php echo (@$data['customhourly'])? @$data['customhourly'] : 0; ?>;
	var std_lat = "<?php echo (@$_SESSION['std_lat'])? @$_SESSION['std_lat'] : 30.00003434343; ?>";
	var std_lng = "<?php echo (@$_SESSION['std_lng'])? @$_SESSION['std_lng'] : 72.00343333333; ?>";
	//var place_name = "<?php echo (@$_SESSION['std_place_name'])? @$_SESSION['std_place_name'] : 'islamabad pakistan'; ?>";
    var place_name = "islamabad, pakistan";
    var calendar;
    var options;
  
	
</script>

</body>
</html>
<?php
//}
?>





