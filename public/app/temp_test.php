<?php
$thisPage="Edit Profile";
session_start();
if(!isset($_SESSION['Teacher']))
{
	header('Location: index.php');
} 
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Wijzig profiel | Bijles Aan Huis</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">	
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />	
	
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	
<!-- Image Crop-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.css" type="text/css" />
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/css/Jcrop.min.css" type="text/css" />
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>		
<style type="text/css">
/*.jcrop-selection.jcrop-current  {
 -webkit-border-radius: 50% !important;
 -moz-border-radius: 50% !important;
  border-radius: 50% !important;
	border: 1px solid #f1f1f1 !important;
  margin: -1px;
}*/
.fa, .far, .fas {
    font-family: "Font Awesome 5 Free" !important;
	font-size:16px !important;
	color: #bbb !important;
}
	.header-radius
	{
		border-top-left-radius: 6px !important;
		border-top-right-radius: 6px !important;		
	}
	.body-radius
	{
		border-bottom-left-radius: 6px !important;
		border-bottom-right-radius: 6px !important;	
	}	
/* jquery.Jcrop.min.css v0.9.12 (build:20130126) */
.jcrop-holder{direction:ltr;text-align:left;}
.jcrop-vline,.jcrop-hline{background:#FFF url(Jcrop.gif);font-size:0;position:absolute;}
.jcrop-vline{height:100%;width:1px!important;}
.jcrop-vline.right{right:0;}
.jcrop-hline{height:1px!important;width:100%;}
.jcrop-hline.bottom{bottom:0;}
.jcrop-tracker{-webkit-tap-highlight-color:transparent;-webkit-touch-callout:none;-webkit-user-select:none;height:100%;width:100%;}
.jcrop-handle{background-color:#333;border:1px #EEE solid;font-size:1px;height:7px;width:7px;}
.jcrop-handle.ord-n{left:50%;margin-left:-4px;margin-top:-4px;top:0;}
.jcrop-handle.ord-s{bottom:0;left:50%;margin-bottom:-4px;margin-left:-4px;}
.jcrop-handle.ord-e{margin-right:-4px;margin-top:-4px;right:0;top:50%;}
.jcrop-handle.ord-w{left:0;margin-left:-4px;margin-top:-4px;top:50%;}
.jcrop-handle.ord-nw{left:0;margin-left:-4px;margin-top:-4px;top:0;}
.jcrop-handle.ord-ne{margin-right:-4px;margin-top:-4px;right:0;top:0;}
.jcrop-handle.ord-se{bottom:0;margin-bottom:-4px;margin-right:-4px;right:0;}
.jcrop-handle.ord-sw{bottom:0;left:0;margin-bottom:-4px;margin-left:-4px;}
.jcrop-dragbar.ord-n,.jcrop-dragbar.ord-s{height:7px;width:100%;}
.jcrop-dragbar.ord-e,.jcrop-dragbar.ord-w{height:100%;width:7px;}
.jcrop-dragbar.ord-n{margin-top:-4px;}
.jcrop-dragbar.ord-s{bottom:0;margin-bottom:-4px;}
.jcrop-dragbar.ord-e{margin-right:-4px;right:0;}
.jcrop-dragbar.ord-w{margin-left:-4px;}
.jcrop-light .jcrop-vline,.jcrop-light .jcrop-hline{background:#FFF;filter:alpha(opacity=70)!important;opacity:.70!important;}
.jcrop-light .jcrop-handle{-moz-border-radius:3px;-webkit-border-radius:3px;background-color:#000;border-color:#FFF;border-radius:3px;}
.jcrop-dark .jcrop-vline,.jcrop-dark .jcrop-hline{background:#000;filter:alpha(opacity=70)!important;opacity:.7!important;}
.jcrop-dark .jcrop-handle{-moz-border-radius:3px;-webkit-border-radius:3px;background-color:#FFF;border-color:#000;border-radius:3px;}
.solid-line .jcrop-vline,.solid-line .jcrop-hline{background:#FFF;}
.jcrop-holder img,img.jcrop-preview{max-width:none;}   
	
    	.mapboxgl-ctrl-attrib, .mapboxgl-ctrl-logo{display:none !important;}	
		.mapboxgl-ctrl-geocoder li > a {color: black !important;}
		.mapboxgl-ctrl-geocoder input {color: black !important}
	

	
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;
	}
	label
	{
    	font-size: 16px !important;	
		margin-left: 14px !important
	}
	
	select{	
border: 1px solid #bdbdbd !important;

/*color: white !important; */
	}
	.body-overflow	
	{
	
	}
/*	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	*/
	.modal-dialog{
	   margin-top:71px !important;
	}
	.fade{
	  background: #66666694;
	   opacity:1;
	}
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	/***********datatable content**********/
	th.sorting_asc:before{
	  content: none !important;
	} 
	th.sorting:before{
	  content: none !important;
	}
	
	th.sorting_asc:after{
	  content: none !important;
	}	
	
	th.sorting:after{
	  content: none !important;
	}	
	.dataTables_length	
	{
		display:none !important;
	}
	.dataTables_filter
	{
	 display:none !important;
	}
	.dataTables_info
	{
	display:none !important;
	}
	.dataTables_paginate 
	{
	 display:none !important;
	}
	/***********************************************************/
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}

	
	.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
    color: black !important;
}
	textarea
	{
		color: #5a777b  !important;
height: 100px !important;
border: 1px solid #465059 !important;
	}
	
	textarea:focus
	{
		color: #5a777b  !important;
	}
		
	.btn-block
	{
		width: 40% !Important;
	}
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}
	
	.dropdown-menu > ul > li > a
	{
		color: #555 !important;
	}
	.errorLabel, .terrorLabel{
		color: red;
	}
	.hide{
		display:none;
	}
	ul.dropdown-menu.inner {
    display: block;
}
#radioBtn .notActive {
    color: #5cc75f !important;
    background-color: #fff !important;
    border: 1px solid #bbb !important;
}
	#radioBtn a.active
	{
		background-color: #5CC75F;
		border:1px solid #bbb;
	}	

	.form-control{
		    background-color: transparent;	
	}
	span.bs-caret {
	    display: none;
	}
	#coordinates{display:none;}
	button#update-more-courses {margin: auto;}
	.btn-group.bootstrap-select.show-tick.show{display:inline-block !important;}
	section{overflow:hidden;}
	.body{border: 2px solid #f1f1f1;}
	select.form-control.form-control-sm {
    height: 30px !important;
}
/*profile selection*/
.grid{
	padding-bottom: 4%;
}
div#pbe-footer-wa-wrap {
    padding-left: 40px;
}
span.maxlabel {
    font-size: 12px !important;
	float:right !important;
}
	.card .body {
   background-color: #FAFBFC !important;
}

</style>
<link href="assets/plugins/croppie/demo.css" rel="stylesheet">
<link href="assets/plugins/croppie/croppie.css" rel="stylesheet">
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
        require_once('includes/sidebarTeachersDashboard.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 85px !important;">
   <div class="block-header">
       <?php require_once('includes/teacherTopBar.php'); ?>
    </div>
	<form action = "actions/edit_scripts" method="post" id="info" enctype='multipart/form-data'>
     <br/>
						<div class="row">
				
							<div class="col-md-12 col-sm-12">
								
						<!--	<button type="button" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-more-courses"> ADD MORE</button> -->
								<div style="text-align:center;"> 
									</div>
							</div>
							
							
							
						</div>	
	<!--<hr style="border-top: 4px solid #f1f1f1 !important;"/>-->
		
			<div class="row">

				<div class="col-md-12 col-lg-12 col-xl-12">
                   <div class="card" style="background: transparent !important;">
                   
                    <div class="header header-radius" style="background-color:#F1F1F1;">
                        <h2 class="ma-subtitle">Persoonlijke informatie</h2>
                        
					   </div>
					   <div class="body body-radius">
						   <?php
			
					
									$teacherid = $_SESSION['TeacherID'];
									$stmt = $con->prepare("SELECT firstname, lastname, dateofbirth, telephone, email, address, postalcode, city, country, latitude, longitude, place_name from contact inner join contacttype on (contacttype.contacttypeID = contact.contacttypeID) where contact.userID = ? AND contacttype.contacttype = ?");
									$contacttype = 'Teacher';
	$stmt->bind_param("is",$teacherid,$contacttype);
	$stmt->execute();
	$stmt->bind_result($fname,$lname,$dob,$telephone,$email,$address,$postalcode,$city,$country, $lat, $lng, $place_name);
	$stmt->store_result();
	$stmt->fetch();
	
	?>
						   
						   
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6">
							   <div class="form-group form-float">
									<label for="booking-code">Voornaam</label>
								   <input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="TeacherPersonalInfo">
								   <input type="hidden" class="form-control" placeholder="First Name" name="teacher-id" id="teacher-id" value="<?php echo $teacherid; ?>">
									<input type="text" class="form-control" placeholder="Voornaam" name="first-name" id="first-name" value="<?php echo $fname; ?>">
                                    
                                </div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group form-float">
									<label for="booking-code">Achternaam</label>
									<input type="text" class="form-control" placeholder="Achternaam" name="last-name" id="last-name" value="<?php echo $lname; ?>">
                                    
                                </div>
								</div>
							</div>
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-6">
                                <div class="form-group form-float">
                                    <label for="booking-code">Geboortedatum </label>
                                    <input type="date" class="form-control" placeholder="Geboortedatum " name="dob" id="dob"  min="1900-01-01" max="2010-01-01" value="<?php echo $dob; ?>">
                                    
                                </div>								
								 </div>
								 <div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group form-float">
										 <label for="enfants">Telefoonnummer</label>
                                    <input type="text" class="form-control" placeholder="Telefoonnummer" name="telephone" id="telephone" value="<?php echo $telephone; ?>">
										<span class="terrorLabel hide"> </span>

									
                                </div>
								</div>
							</div>								   
							   <?php
			
					
									$teacherid = $_SESSION['TeacherID'];
									$stmt = $con->prepare("select username, email from user inner join usergroup on (user.usergroupID = usergroup.usergroupID) where user.userID = ? and usergroup.usergroupname = ?");
									$contacttype = 'Teacher';
									$stmt->bind_param("is",$teacherid,$contacttype);
									$stmt->execute();
									$stmt->bind_result($username,$email);
									$stmt->store_result();
									$stmt->fetch();
	
	?>
						   

								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group form-float">
										<label for="booking-code">E-mailadres</label>
										<input type="email" class="form-control" placeholder="E-mailadres" name="email" id="email" value="<?php echo $email; ?>" readonly>
										
									</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-6">
									<div class="form-group form-float">
										<label for="booking-code">Wachtwoord</label>
										<input type="password" class="form-control password" placeholder="Wachtwoord" name="password" id="password"> 

										
										<input name="toggle" type="checkbox"  id="toggle"  value='0' onchange='togglePassword(this);'>
										<label style="font-weight: 100; margin-top:6px;" for="toggle" id='toggleText'>&nbsp; Laat wachtwoord zien</label>

										
									</div>	
									</div>
								</div>
								
							<div class="row">
							<?php
									$stmt = $con->prepare("SELECT onlineteaching, allownewstudents FROM teacher where userID = ?");
									$stmt->bind_param("i",$teacherid);
									$stmt->execute();
									$stmt->bind_result($online_courses,$acc_new_std);
									$stmt->store_result();
									$stmt->fetch();								
									echo $onl
								?>	
										
										<div class="col-lg-6 col-md-6 col-sm-6">
											<div class="input-group" style="margin-left: 14px;">
												<label for="Online Courses" class="control-label text-right" style="margin-right:15px;">											
												  Beschikbaar voor online bijles? <i style=" color:#bbb !important;" class="fa" >&#xf05a;</i>
												</label>
												<div id="radioBtn" class="btn-group">
													<a class="btn btn-success btn-sm <?php if($online_courses == 1){ echo 'active';} else { echo 'notActive';} ?>" data-toggle="online_courses" data-title="1">Ja</a>
													<a class="btn btn-success btn-sm <?php if($online_courses == 0){ echo 'active';} else { echo 'notActive';} ?>" data-toggle="online_courses" data-title="0">Nee</a>
												</div>
												<input type="hidden" name="online_courses" id="online_courses" value="<?php echo $online_courses; ?>">
											</div>
										</div>
									
									
									
										
										<div class="col-lg-6 col-md-6 col-sm-6">
											
											<div class="input-group"  style="margin-left: 14px;">
												<label for="Accepting New Students" class="control-label text-right" style="margin-right:15px;">Beschikbaar voor nieuwe leerlingen?
												<i style=" color:#bbb !important;" class="fa" >&#xf05a;</i></label>
												<div id="radioBtn" class="btn-group">
													<a class="btn btn-success btn-sm <?php if($acc_new_std == 1){ echo 'active';} else { echo 'notActive';} ?>"  data-toggle="acc_new_std" data-title="1">Ja</a>
													<a class="btn btn-success btn-sm <?php if($acc_new_std == 0){ echo 'active';} else { echo 'notActive';} ?>" data-toggle="acc_new_std" data-title="0">Nee</a>
												</div>
												<input type="hidden" name="acc_new_std" id="acc_new_std" value="<?php echo $acc_new_std; ?>">
											</div>
										</div>
									
							</div>		
							
							   
									<!--<label for="booking-code">Username</label> -->
									<input type="hidden" class="form-control" placeholder="Username" name="username" id="username" value="<?php echo $username; ?>" readonly>
                                    
                               							
								
						<!--	  <div class="footer" style="text-align:center;"> 
						    <button type="submit" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block hide" id="submit-btn2" name="update-teacher"></button>
						    <button type="button" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block " id="add-level-btn2" name="update-teacher"> UPDATE </button>
							   </div>
							   
						   </form> -->
								<button type="submit" style="margin-left:0px ; width:15% !important; margin-top:16px !important;" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="update-more-courses" name="update-teacher"> OPSLAAN </button>							   
					   </div>
									   
                </div>

                
			</div>	
		</div>
								


		<!--PROFILE INFORMATION-->
			<div class="row">

				<div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="card" style="background: transparent !important;">
                   
                    <div class="header header-radius" style="background-color:#F1F1F1;">
                        <h2 class="ma-subtitle">Profiel informatie</h2>
                        
					   </div>
					   <div class="body body-radius">
						   <?php
			
					
									$teacherid = $_SESSION['TeacherID'];
									$stmt = $con->prepare("select profile.title, profile.shortdescription, profile.description, profile.image, profile.profileID from profile inner join teacher ON (teacher.profileID = profile.profileID) inner join user on (user.userID = teacher.userID) inner join usergroup on (user.usergroupID = usergroup.usergroupID) where user.userID = ? and usergroup.usergroupname = ?");
									$contacttype = 'Teacher';
	$stmt->bind_param("is",$teacherid,$contacttype);
	$stmt->execute();
	$stmt->bind_result($title_profile, $short,$desc,$profile,$profileID);
	$stmt->store_result();
	$stmt->fetch();
	
	?>
						  
						   
                                <div class="form-group form-float">
									<!--profile section-->
									<div class="row clearfix">
										<div class="col-md-12 col-lg-12">
											<div class="demo-wrap upload-demo">
														<div class="col-lg-5">
															<label for="booking-code">Profielfoto</label>
															<input type="hidden" name="current-image" value="<?php echo $profile; ?>" />
															<div class="image">
																
																	<!--<div class="" id="jcrop" style="margin-top: 50px; border: 1px solid #f1f1f1;">-->
																	<div class="" id="jcrop" style="margin-top: 10px;">													
																	
																			<img style="width: 246px;" src="<?php echo $profile; ?>" id="jcrop-image" alt="User">
																			
																	</div>

																	<input id="png" type="hidden" name="imgb" />
																	<div class="upload-demo-wrap">
																		<div id="upload-demo"></div>
																	</div>
																
															</div>
															<div class="actions">
																<label style="display:block;">Upload New Picture</label>
																<a class="file-btn btn btn-warning btn-md " style="margin-left: 12px;">
																	kies een foto
																	<input type="file" id="file" name="profile-image" value="Choose a file" accept="image/*" />
																</a>
																<!--<button class="upload-result btn btn-primary btn-md">Crop</button>-->
															
																
															</div>

															
														</div>
														<div class="col-lg-5">
															<canvas id="canvas" style="margin-top:37px;margin-left:80px;max-width:200px; max-height:200px"></canvas>
														<!--	<img id="ma-img" style="margin:10%;"/> 
															<input type="hidden" name="ma-profile-image" id="ma-profile" value="" /> -->
															
														</div>
														<div class="col-1-3">

														</div>
														
											</div>
										</div>
									</div>
									<!--profile section ends-->
										
								</div>						   
								
							   <div class="form-group form-float">
								   <label for="booking-code">Profieltitel </label>&nbsp &nbsp  <span id="title_profile"><?php echo (75 - strlen($title_profile)); ?></span> characters left <span class="maxlabel"> (Gebruik maximaal 75 tekens)</span>
								 <textarea class="form-control" placeholder="Gebruik maximaal 75 tekens. Deze tekst wordt weergeven voordat een leerling op je profiel klikt. Benoem je educatieve achtergrond en verkoop jezelf gerust!" maxlength="75" name="title_profile" id="title_profile" onkeyup="countChar(this, 75, 'title_profile')"><?php echo $title_profile; ?></textarea>
									
                                    
                                </div>
						   
						   <div class="form-group form-float">
								   <label for="booking-code">Korte profielbeschrijving </label>&nbsp &nbsp  <span id="short_profile"><?php echo (180 - strlen($short)); ?></span> characters left <span class="maxlabel"> (Gebruik maximaal 180 tekens)</span>
								   <!-- <input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="TeacherProfileInfo"> -->
								   
								   <input type="hidden" class="form-control" placeholder="First Name" name="profile-id" id="profile-id" value="<?php echo $profileID; ?>">
								   
								   
								   <textarea class="form-control" placeholder="Gebruik maximaal 180 tekens. Deze tekst wordt weergeven voordat een leerling op je profiel klikt. Benoem je educatieve achtergrond en verkoop jezelf gerust!" maxlength="180" name="short-description" id="short-description" onkeyup="countChar(this, 180, 'short_profile')"><?php echo $short; ?></textarea>
									
                                    
                                </div>


								<div class="form-group form-float">
									<label for="booking-code">Lange profielbeschrijving </label> &nbsp &nbsp<span id="long_profile"><?php echo (2000 - strlen($desc)); ?></span> characters left <span class="maxlabel"> (Gebruik maximaal 2,000 tekens)</span>
								   <textarea class="form-control" placeholder="Gebruik maximaal 2,000 tekens. Deze tekst wordt weergeven in je profiel. Zorg dat de leerling alle informatie krijgt die het nodig heeft, zoals: Vertel uitgebreider over je studie of opleiding. Hoe geef je bijles? Waarom kan jij de leerling goed helpen? Benoem het niet als je geen ervaring hebt, maar benadruk het als je dat wel hebt. En vertel eventueel aanvullende informatie die zinvol kan zijn voor de leerling. Overigens weet de leerling jouw locatie niet precies, maar wel of jij reiskosten MAG rekenen. Daarom kan het handig zijn om aan te geven in welke gebieden je zonder reiskosten bij de leerling aan huis komt. Je kan hier ook meer over je algemene beschikbaarheid vertellen, omdat de kalender wellicht niet altijd representatief is." maxlength="2000" name="full-description" id="full-description" onkeyup="countChar(this, 2000, 'long_profile')"><?php echo $desc; ?></textarea>
									
									
					    <button type="submit" style="margin-left:0px ; width:15% !important; margin-top:16px !important;" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="add-level-btn"  name="update-teacher">  OPSLAAN </button>  			
                                    
                                </div>
                              
                                    
                               
 <!--<div style="text-align:center"> 
						    <button type="submit" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="add-level-btn"  name="update-teacher"> UPDATE </button>
	 </div>
							   
						   </form> -->
						   
					   </div>
						
                </div>

                </div>
			</div>		

		<div class="row">
		<div class="card" style="background: transparent !important;">
			 <div class="col-lg-6 col-md-6">
                   <div class="card" style="background: transparent !important;">
                   
						<div class="header header-radius" style="background-color:#F1F1F1;">
							<h2 class="ma-subtitle">Pas je locatie aan</h2>
							
						   </div>
						   <div class="body body-radius">
							  
							  
								   <input type="hidden" class="form-control" name="lat" id="lat" value="<?php echo ($lat)? $lat : 33.719361; ?>">
											<input type="hidden" class="form-control"  name="lng" id="lng" value="<?php echo ($lng)? $lng : 73.074144; ?>">
											<input type="hidden" class="form-control"  name="place_name" id="place_name" value="<?php echo ($place_name)? $place_name : ""; ?>">
							   <input type="hidden"   name="teacher-id" id="teacher-id" value="<?php echo $_SESSION['TeacherID']; ?>">
								   <div class="form-group form-float">
												<div id='map' style="height: 200px;  overflow:visible;"></div>	
									</div>
										<pre id='coordinates' class='coordinates'></pre>
						<!--	<div style="text-align:center;"> 			
								   
								<button type="submit" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="update-button-map" name="update-teacher"> UPDATE </button>
								</div>
							   </form> -->
							   <button type="submit" style="margin-left:20px;margin-top:16px ; width:32% !important" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="update-more-courses" name="update-teacher"> OPSLAAN </button>	
						   </div>
													   
					</div>
					
					</div>	


				<div class="col-md-6 col-lg-6 col-xl-6">
                    <div class="card" style="background: transparent !important;">
                   
                    <div class="header header-radius" style="background-color:#F1F1F1;">
                        <h2 class="ma-subtitle">Bewijs van geschikt niveau</h2>
					   </div>
					   <div class="body body-radius">
						   <?php
			
					
									$teacherid = $_SESSION['TeacherID'];
									$stmt = $con->prepare("select  `applicationID`, `IDdocument`, `Educationdocument` from applications where userID = ?");
									$contacttype = 'Teacher';
									$stmt->bind_param("i",$teacherid);
									$stmt->execute();
									$stmt->bind_result($appid,$iddoc,$educationdoc);
									$stmt->store_result();
									$stmt->fetch();
	
	?>
						  
						   
							   



                   <!--             <div class="form-group form-float">
									<div class="row clearfix">
										<div class="col-md-6 col-lg-6 col-xl-6">
										<label for="booking-code">ID Document</label>
										<!--	 <input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="TeacherApplicationInfo">
								   <input type="hidden" class="form-control" placeholder="First Name" name="application-id" id="application-id" value="<?php //echo $appid; ?>">
											<input type="hidden" name="current-image-id" value="<?php //echo $iddoc; ?>" />
                                    <div class="image">
										<?php //if($iddoc){ ?>
											<a target="_blank" href="<?php //echo $iddoc; ?>">
												  <span class="fa fa-download"></span> <?php// $x = explode("/",$iddoc); echo $x[0]; ?>
												</a>
										<a  href="actions/doc_delete?file=doc&id=<?php //echo $appid; ?>" style="float: right;">
												  <span class="fa fa-remove"></span>
												</a>
										<?php// } ?>
											</div>
											
										</div>
										<div class="col-md-6 col-lg-6 col-xl-6">
										<label for="booking-code">Upload New File</label>
									<input type="file" class="form-control" placeholder="Profile Image" name="iddoc-image" id="iddoc-image">
										</div>
									</div>
									</div>
							   -->
						   
							   <div class="form-group form-float">
									<div class="row clearfix">
										<div class="col-md-6 col-lg-6 col-xl-6">
										<label for="booking-code">Schoolcijfers</label>
											 <input type="hidden" name="current-image-education" value="<?php echo $educationdoc; ?>" />
                                    <div class="image">
										<?php if($educationdoc){ ?>
											<a target="_blank" href="<?php echo $educationdoc; ?>">
												  <span class="fa fa-download"></span> <?php $x = explode("/",$educationdoc); echo $x[0]; ?>
												</a>
										<a  href="actions/doc_delete?file=edu&id=<?php echo $appid; ?>" style="float: right;">
												  <span class="fa fa-remove"></span>
												</a>
										<?php } ?>
									</div>
											
										</div>
										<div class="col-md-6 col-lg-6 col-xl-6">

													<div class="actions">
														<label for="booking-code">Nieuw bestand uploaden</label>
														<a class="file-btn btn btn-warning btn-md " style="margin-left:10px;">
																	Kies een bestand
														<input type="file" class="form-control" placeholder="Profile Image" name="educationdoc-image" id="educationdoc-image">
														</a>
														<!--<button class="upload-result btn btn-primary btn-md">Crop</button>-->
															
																
													</div>											
										</div>
									</div>
									</div>
                                    
                                    
                           <!--    <div class="footer" style="text-align:center; margin-top: 41px;"> 

						    <button type="submit" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="add-level-btn"  name="update-teacher"> UPDATE </button>
								   </div>
						   </form>-->
					   	
							<button type="submit" style="margin-left:20px;margin-top:16px ; width:32% !important" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="update-more-courses" name="update-teacher"> OPSLAAN </button>
					   </div>
					
						
                </div>
					
					<!--------------------------map update --------------------------->

					<!--------------------------------------------------------------->
					
                </div>		
		
		  </div>
		
		</div>
		
		<div class="row">
				<div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="card" style="background: transparent !important;">
                   
                    <div class="header header-radius" style="background-color:#F1F1F1;">
                        <h2 class="ma-subtitle">Teacher Level</h2>
                        	
					   </div>
						<div class="body body-radius">
							<?php 
								//$q = "SELECT requested_teacherlevel_rate_ID rtID, teacherlevel_rate_ID tID, tlr.level FROM teacher, teacherlevel_rate tlr WHERE userID=$_SESSION[TeacherID] AND (tlr.ID=teacherlevel_rate_ID || tlr.ID=teacher.requested_teacherlevel_rate_ID) LIMIT 1";
	
								$q = "SELECT requested_teacherlevel_rate_ID, (SELECT internal_level FROM teacherlevel_rate stlr WHERE stlr.ID=teacher.requested_teacherlevel_rate_ID)requested_tlevel, (SELECT internal_level FROM teacherlevel_rate sstlr WHERE sstlr.ID=tlr.next_teacherlevelID)ntlevel , tlr.next_teacherlevelID ntlevelID, tlr.internal_level AS `level` FROM teacher, teacherlevel_rate tlr WHERE userID=$_SESSION[TeacherID] AND tlr.ID=teacherlevel_rate_ID";
						
								$r = mysqli_query($con, $q);
								$f = mysqli_fetch_array($r);
								$rtID = $f['rtID'];
								$tID = $f['tID'];
								$tLevel = $f['level'];
								
								//fetching level name
								$q1 = "SELECT * FROM teacherlevel_rate tlr";
						
								$r1 = mysqli_query($con, $q1);
							
								
							?>
							<div class="row">
							<div class="col-md-4 col-lg-4 col-xl-4">
								<label>Current Level : </label>
								<span class="clevel"><?php echo $f['level']; ?></span>
							</div>
							<div class="col-md-8 col-lg-8 col-xl-8">
								<label>Desiring Level : </label>
								<div id="dlevel_place" style="display: inline-block;">
								<?php if(!$f['requested_teacherlevel_rate_ID']){ ?>
								<input type="hidden" id="requestedlevel" name="requestedlevel" value="<?php echo $f['ntlevelID']; ?>"/>
								<button type="button" style="margin-top: 0px;" class="btn btn-primary btn-md " id="sendRequest">Resquest to become <?php echo $f['ntlevel']; ?> tutor</button>
								<?php
										  }else{
									echo "Already Applied for ".$f['requested_tlevel'];
								}	   
								?>
								</div>
								</div>
							</div>
						</div>
					</div>
			</div>
		</div>

		<div class="row">
				<div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="card" style="background: transparent !important;">
                   
                    <div class="header header-radius" style="background-color:#F1F1F1;">
                        <h2 class="ma-subtitle">Vakken waarin je bijles geeft</h2>
                        
					   </div>
					   <div class="body body-radius">
						   <?php
			
					
									$teacherid = $_SESSION['TeacherID'];
									$stmt = $con->prepare("select applicationID, IDdocument, Educationdocument from applications where userID = ?");
									$contacttype = 'Teacher';
	$stmt->bind_param("i",$teacherid);
	$stmt->execute();
	$stmt->bind_result($appid,$iddoc,$educationdoc);
	$stmt->store_result();
	$stmt->fetch();
	
	?>
						   <div class="table-responsive">
                            <table class="table table-bordered table-striped table-hover dataTable js-basic-example1 dt-responsive" style="font-size: 13px;" id="view-schoolcourses" cellspacing="0" width="100%">
                                <thead>
                                    <tr class="text-center">
                                        <th>Schoolvak</th>
										<th>Schooltype</th>
										<th>Leerjaar</th>
										<th>Niveau</th>
										<th>Geaccepteerd?</th>                                        
                                        <th></th>
                                    </tr>
                                </thead>
                                
                                <tbody>
									<?php
	$teacherid = $_SESSION['TeacherID'];
											$stmt = $con->prepare("SELECT tc.teacherID, tc.teacher_courseID, courses.courseID, courses.coursename, tc.permission, (SELECT GROUP_CONCAT(typeName) FROM schooltype WHERE FIND_IN_SET(typeID, courses.coursetype))typeName, (SELECT GROUP_CONCAT(schoollevel.levelName) FROM schoollevel WHERE FIND_IN_SET(levelID, tc.schoollevelID))levelName, (SELECT GROUP_CONCAT(schoolyear.yearName) FROM schoolyear WHERE FIND_IN_SET(yearID, tc.schoolyearID))yearName FROM courses, teacher_courses tc WHERE tc.courseID=courses.schoolcourseID AND tc.teacherID=$teacherid  AND courses.courselevel=tc.schoollevelID  ORDER BY coursename");
	
	
					$stmt->bind_param("i",$teacherid);
					$stmt->execute();
					$stmt->bind_result($assignteacher, $assigncourse, $id,$name, $permission, $type,$level, $year);
					$stmt->store_result();
					$ii=1;
					while($stmt->fetch())
					{
	
									?>
									<tr>
				
									<td><?php echo $name; ?></td>
										<td><?php echo $type; ?></td>
									<td><?php echo $level; ?></td>
										<td><?php echo $year; ?></td>
										<td><?php echo $permission; ?></td>									
										<td>
									<b><a href="javascript:;" class="delete-record-course" data-id="<?php echo $assignteacher.",".$assigncourse; ?>" style="color: #5CC75F !important;"> Verwijderen </a></b>  
										</td>
									</tr>
									<?php
					}
	?>
                                </tbody>
                            </table>
                        </div>
						   <br/><br/><br/>
						  
								<!-- <input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="TeacherCoursesInfo"> -->
								 

							<!--			<button type="submit" style="margin-left:0px;margin-top:5px ; width:15% !important" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="update-more-courses" name="update-teacher"> OPSLAAN </button>-->
					   </div> <!-- last body end-->
										
                </div>
                </div>		
		
		</div>
		
		<div class="row">
				<div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="card" style="background: transparent !important;">
                   
                    <div class="header header-radius" style="background-color:#F1F1F1;">
                        <h2 class="ma-subtitle">Een nieuw vak aanvragen</h2>
                        
					   </div>
					   <div class="body body-radius body-overflow" style="overflow: -webkit-paged-y">
						   <div class="courses-div">
						<div id="newCourse" style="margin-bottom: 102px;">
                        <div class="row clearfix" id="course-row1">
							<input type="hidden" name="counter" class="counter" value="1" />
                            <div class="col-lg-3 col-md-6">
                                <p styl="color: white !important;"> <b>Schooltype *</b> </p>
                                 
                                    <select class="form-control show-tick dynamic-course school-type" name="select-school-type[]" id="school-type">
                                    
										
                                    <?php
									
										$stmt = $con->prepare("SELECT DISTINCT typeID, typeName from schooltype group by typeName");
										$stmt->execute();
										$stmt->bind_result($typeID,$typeName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $typeID; ?>"><?php echo $typeName; ?></option>
									<?php
										}
									?>
-->
                                </select>
									   
								
                            </div>
							<div class="col-lg-3 col-md-6">
								<p styl="color: white !important;"> <b>Schoolvak *</b> </p>
                                <select class="form-control show-tick school-course" name="school-course[]" id="school-course1">
					
                                </select>
								
                            </div>
							<div class="col-lg-2 col-md-4">
                                <p styl="color: white !important;"> <b>Niveau *</b> </p>
                                <select class="form-control show-tick school-level" id="school-level"  name="school-level[]">
									<option>Select</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoollevel");
										$stmt->execute();
										$stmt->bind_result($levelID,$levelName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $levelID; ?>"><?php echo $levelName; ?></option>
									<?php
										}
									?>
									
                                </select>
                            </div>
							<div class="col-lg-2 col-md-4">
                                <p styl="color: white !important;"> <b>Leerjaar123 *</b> </p>
                                <select class="form-control show-tick school-year" id="school-year"  multiple="multiple" name="school-year[]">
								
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoolyear");
										$stmt->execute();
										$stmt->bind_result($yearID,$yearName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $yearID; ?>"><?php echo $yearName; ?></option>
									<?php
										}
									?>
									
                                </select>
                            </div>
							<div class="col-lg-2 col-md-4">
										<button type="button" style="width: 100% !important; margin-top: 27px;" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="add-courses" name="update-teacher"> OPSLAAN </button>								
								</div>
                        </div>
						</div>
							
                    </div>	
							   
					   </div>

					</div>
				</div>
				
			</div>	

				
				
				
		
	 </form>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> <!-- Latest compiled and minified JavaScript -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
	<script type="text/javascript">

  $(document).ready(function() {
	  
	  $('.js-basic-example1').DataTable({
		  'iDisplayLength': 100,
		language: {
			url:'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Dutch.json'
		}
	});
	  
	  /* Fetch Pre Information for courses */
	  
	   function fetch_preinfo(cat, className, rowID) { 

         $.ajax({
                            url: 'actions/fetch_preinfo.php',
                            data: {'cat': cat},
				  			type: "POST",
							//dataType: "json",
                             success: function (result) {
								//$(this).parent().parent().siblings().find(".school-course").html("");
								 var rowselector = "#";
								 var selector = ".";
								 //var separator ">";
								 var classNam = rowselector.concat(rowID);
								 var classNam1 = selector.concat(className);
								 $(classNam).children().find(classNam1).not(".btn-group",".bootstrap-select").html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                	$(classNam).children().find(classNam1).not(".btn-group",".bootstrap-select").append('<option value="'+String(json_obj[i].ID)+'">'+String(json_obj[i].Name)+'</option>');
								}
					
								  $(classNam).children().find(classNam1).not(".btn-group",".bootstrap-select").selectpicker('refresh');
}
                      });

    }
	  
	  
	  fetch_preinfo("SchoolType","school-type","course-row1");
	  
	//fetch_preinfo("SchoolCourse","school-course","course-row1");
	  
	  //$('#view-schoolcourses').DataTable();
	  
	  $("#school-course1").change(function(){
	  		var cid = $(this).val();
		  $.ajax({
			  url: 'actions/course_filter.php',
			  data: {'subpage': "teacherCourselevel", cid:cid},
			  type: "POST",
			  success: function (result) {
				  $("#school-level").html(result);
				  $("#school-level").selectpicker('refresh');
			  }
		  }); //ajax ends
		  
	  }); // $("#school-course1").change() ends
	  
	   $("#school-level").change(function(){
	  		var cid = $("#school-course1").val();
	  		var clid = $(this).val();
		  $.ajax({
			  url: 'actions/course_filter.php',
			  data: {'subpage': "teachercourseyear", cid:cid, clid:clid},
			  type: "POST",
			  success: function (result) {
				  $("#school-year").html(result);
				  $("#school-year").selectpicker('refresh');
			  }
		  }); //ajax ends
		  
	  }); // $("#school-course1").change() ends
	  
	  $("#sendRequest").click(function(){
	  
	  	var rlevel = $("#requestedlevel").val();
		  
		 $.ajax({
			 url: 'teacher-ajaxcalls.php',
			 data: {'subpage': "teacherlevel_requested", 'levelID':rlevel},
			 type: "POST",
			 success: function (result) {
				 if(result.trim() == "success"){
						showNotification("alert-success", "Requested has been sent successfully", "bottom", "center", "", "");	
					 	$("#dlevel_place").html("Applied for the desired level");
				 }else{
						showNotification("alert-danger", "Failed to send Request, Try later.", "bottom", "center", "", "");	
				 }
			 }
		 }); 
	  
	  });
	  
	  	var _type= "";
		var _course= "";
		var _level= "";
		var _year= "";
	  	var _courseID = "";

	  $("#add-courses").click(function(){
	  	var type= $("#school-type").val();
	  	var course= $("#school-course1").val();
	  	var level= $("#school-level").val();
	  	var year= $("#school-year").val();
		  
		 if(type == <?php echo ELEMENTRY_TYPE_ID; ?>){
			if(!(type && course)){
		 		showNotification("alert-danger", "Select all options of courses, then try again.", "bottom", "center", "", "");
			 	return 1;
		 	}
		  	_type = $('#school-type option:selected').text();
			_course = $('#school-course1 option:selected').text();
		 	
		}else{
			if(!(type && course && level && year)){
		 		showNotification("alert-danger", "Select all options of courses, then try again.", "bottom", "center", "", "");
			 	return 1;
		 	}
	  		_type = $('#school-type option:selected').text();
			_course = $('#school-course1 option:selected').text();
	  		_level = $('#school-level option:selected').text();
			_year = $("#school-year option:selected").map(function () {
						return $(this).text();
					}).get().join(',');
  			}			
		  
		 $.ajax({
			 url: 'teacher-ajaxcalls.php',
			 data: {'subpage': "teachercourse_request", type:type,course:course,level:level,year:year},
			 type: "POST",
			 success: function (result) {
	  				result = (result.trim()).split("*");
				 if(result[0] == "success"){
						showNotification("alert-success", "Requested has been sent successfully", "bottom", "center", "", "");	
					 	_courseID = result[1];
					 
					 //appending the new last inserted course into table
					 
$("table#view-schoolcourses tbody").append("<tr><td>"+_course+"</td><td>"+_type+"</td><td>"+_level+"</td><td>"+_year+"</td><td>Requested</td><td><b><a href='javascript:;' class='delete-record-course' data-id='"+_courseID+"' style='color: #5CC75F !important;'> Verwijderen </a></b></td></tr>");
					 
					 //refreshing the level and year dropdownlist
					  $('#school-level').val('').trigger('change');
					  $("#school-level").selectpicker('refresh');
					  $('#school-year').selectpicker('deselectAll');
					  $("#school-year").selectpicker('refresh');
					 
				 }else{
						showNotification("alert-danger", "Failed to send Request, Try later.", "bottom", "center", "", "");	
				 }
	  			 _type= "";
				 _course= "";
				 _level= "";
				 _year= "";
				 _courseID = "";
			 }
		 }); 
	  });
	  
	/* 
	   $.validator.addMethod("telephone",function(value,element){
                return this.optional(element) || /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(value);
            },"Invalid Number");
		
    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
        rules: {
            confirm_password: {
                equalTo: '#password'
            },
			"select-course": "required",
			email:
			{
				notEqual: '#checkEmail',
			},
			telephone:{
				telephone: "#telephone"
			}
        }
    });
	*/
	  
	 $("#add-level-btn2").click(function(e){
		  	e.preventDefault();
		  
	  		var phone = $("#telephone").val();
		 
			if(!(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(phone))){
					$(".terrorLabel").text("Invalid telephone number").removeClass("hide");			
					return;
			}
				
		   $("#submit-btn2").click();
			
	  });
	  
	  
	  $("#add-level-btn1").click(function(e){
		  	e.preventDefault();
		  
	  		var pass = $("#password").val();
		  	var cpass = $("#cpassword").val();
		  
		  	if(pass.length < 8){
					$(".errorLabel").text("Password must be minimum 8 characters").removeClass("hide");
					return;
			}
				
			if(pass != cpass){
					$(".errorLabel").text("Password does not matched").removeClass("hide");			
					return;
			}
				
		   $("#submit-btn").click();
			
	  });
	  
	  $( document ).on( "click", "a.delete-record-course", function(e) {
        run_waitMe($('#view-schoolcourses'), 1, "timer");
            var id = $(this).attr("data-id");
//  alert(id);          
            
                                                 $('#view-schoolcourses').waitMe('hide');
                                                 $("#delete-id-course").val(id);
                                                  //$("#modal-delete").modal('show');    
                                                  $('#modal-delete-course').appendTo("body").modal('show');                                                              
        });
	  
	  $("#delete-yes-course").click(function(){
		  var id = $("#delete-id-course").val();
                $.ajax({
                            url: 'actions/delete_scripts.php',
                            type: 'POST',
                            data: {id:id,choice:"TeacherAssignedCourse"},
                             success: function (result) {
                                    switch(result)
                                    {
                                      case "Error":
                                               $("#deresultcourse").html(
        '<div id="error-delete" class="alert alert-danger alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'There Was An Error Processing Your Request.' + 
         '</div>'); 
											setTimeout(function() {  
												$("#deresultcourse").html("");
                                               $("#modal-delete-course").modal('hide');
}, 1000);
                                        break;
                                        case "Success":
                                                 $("#deresultcourse").html(
        '<div id="success-delete" class="alert alert-success alert-dismissable">'+
            '<button type="button" class="close" ' + 
                    'data-dismiss="alert" aria-hidden="true">' + 
                '&times;' + 
            '</button>' + 
            'Records Deleted Successfully.' + 
         '</div>');  
                                                 setTimeout(function() {   
                                               $("#modal-delete-course").modal('hide');
                								window.location.replace("edit-profile");
}, 1000);
                                        break;
                                    }
                              }
                      });    
          });
	  
	   $("#delete-no-course").click(function(){
		  $("#modal-delete-course").modal('hide');
                  
          });
	   
	 /*
      $(document).on("click", ".school-type" , function(e) {
		  e.stopImmediatePropagation();
          var text = $(this).find("option:selected").text();
		  var id = $(this).find("option:selected").val();
		  if(text == "Elementary")
		  {
			  var schoolType = $(this).find("option:selected").val();
			  $(this).parent().parent().children().find(".school-level").attr('disabled','disabled');
			  $(this).parent().parent().children().find(".school-level").parent().css('display','none');
			  $(this).parent().parent().children().find(".school-year").attr('disabled','disabled');
			  $(this).parent().parent().children().find(".school-year").parent().css('display','none');
			  //$(this).parent().parent().children().find(".school-course").attr('disabled','disabled');
			  //$(this).parent().parent().children().find(".school-course").parent().css('display','none');			  
			  var rowId =  $(this).closest("div.row").attr('id');
			  var elementId = $("#"+rowId).children("div").find("select.school-course").attr('id');
			  $("#"+rowId).children("div").find("select.school-level").val('');
			  $("#"+rowId).children("div").find("select.school-level").selectpicker('refresh');
			  $("#"+rowId).children("div").find("select.school-year").val('');
			  $("#"+rowId).children("div").find("select.school-year").selectpicker('refresh');
			  $.ajax({
                            url: 'actions/fetch_courses.php',
                            data: {'type':text,'typeID': schoolType},
				  			type: "POST",
							//dataType: "json",
                             success: function (result) {
								$("#"+elementId).html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                					$("#"+elementId).append('<option value="'+String(json_obj[i].courseID)+'">'+String(json_obj[i].courseName)+'</option>');
								}
					
								 
								 $("#"+elementId).selectpicker('refresh');
}
                      });
			  
	
		  }
		  
		  else
		  {
			  
			
			  
			  $(this).parent().parent().children().find(".school-level").removeAttr('disabled');
			  $(this).parent().parent().children().find(".school-level").parent().css('display','block');
			  $(this).parent().parent().children().find(".school-level").selectpicker('refresh');
			  $(this).parent().parent().children().find(".school-year").removeAttr('disabled');
			  $(this).parent().parent().children().find(".school-year").parent().css('display','block');
			   $(this).parent().parent().children().find(".school-year").selectpicker('refresh');
			  
			  $(this).parent().parent().children().find(".school-course").removeAttr('disabled');
			  $(this).parent().parent().children().find(".school-course").parent().css('display','block');
			   $(this).parent().parent().children().find(".school-course").selectpicker('refresh');			  		  
			  
			  var rowId =  $(this).closest("div.row").attr('id');
			  var elementId = $("#"+rowId).children("div").find("select.school-course").attr('id');
		   	 var text = $(this).find("option:selected").text();
			 var schoolType = $(this).find("option:selected").val();
			 var schoolLevel = $(this).parent().parent().children().find(".school-level option:selected").not(".btn-group",".bootstrap-select").val();
			 var schoolYear = $(this).parent().parent().children().find(".school-year option:selected").not(".btn-group",".bootstrap-select").val();
		   
			   $.ajax({
                            url: 'actions/fetch_courses.php',
                            data: {'type':text,'typeID': schoolType, 'levelID':schoolLevel, 'yearID':schoolYear},
				  			type: "POST",
							//dataType: "json",
                                       success: function (result) {
								$("#"+elementId).html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                					$("#"+elementId).append('<option value="'+String(json_obj[i].courseID)+'">'+String(json_obj[i].courseName)+'</option>');
								}
					
								 
								 $("#"+elementId).selectpicker('refresh');
}
                      });
			  
			  
		  }
      });

	   $(document).on("click", ".school-year" , function(e) {
		  e.stopImmediatePropagation();
          //var text = $(this).parent().parent().siblings().find(".school-type option:selected").not(".btn-group",".bootstrap-select").text();
		  //var id = $(this).find("option:selected").val();
			 var rowId =  $(this).closest("div.row").attr('id');
			  var elementId = $("#"+rowId).children("div").find("select.school-course").attr('id');
		   var text = $(this).parent().parent().children().find(".school-type option:selected").not(".btn-group",".bootstrap-select").text();
			 var schoolType = $(this).parent().parent().children().find(".school-type option:selected").not(".btn-group",".bootstrap-select").val();
			 var schoolLevel = $(this).parent().parent().children().find(".school-level option:selected").not(".btn-group",".bootstrap-select").val();
			 var schoolYear = $(this).find("option:selected").val();
		   
			   $.ajax({
                            url: 'actions/fetch_courses.php',
                            data: {'type':text,'typeID': schoolType, 'levelID':schoolLevel, 'yearID':schoolYear},
				  			type: "POST",
							//dataType: "json",
                                       success: function (result) {
								$("#"+elementId).html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                					$("#"+elementId).append('<option value="'+String(json_obj[i].courseID)+'">'+String(json_obj[i].courseName)+'</option>');
								}
					
								 
								 $("#"+elementId).selectpicker('refresh');
}
                      });
      });
	  
	   $(document).on("click", ".school-level" , function(e) {
		  e.stopImmediatePropagation();
          //var text = $(this).parent().parent().siblings().find(".school-type option:selected").not(".btn-group",".bootstrap-select").text();
		  //var id = $(this).find("option:selected").val();
			 var rowId =  $(this).closest("div.row").attr('id');
			  var elementId = $("#"+rowId).children("div").find("select.school-course").attr('id');
		   var text = $(this).parent().parent().children().find(".school-type option:selected").not(".btn-group",".bootstrap-select").text();
			 var schoolType = $(this).parent().parent().children().find(".school-type option:selected").not(".btn-group",".bootstrap-select").val();
			 var schoolLevel = $(this).find("option:selected").val();
			 var schoolYear = $(this).parent().parent().children().find(".school-year option:selected").not(".btn-group",".bootstrap-select").val();
		  
			   $.ajax({
                            url: 'actions/fetch_courses.php',
                            data: {'type':text,'typeID': schoolType, 'levelID':schoolLevel, 'yearID':schoolYear},
				  			type: "POST",
							//dataType: "json",
                                       success: function (result) {
								$("#"+elementId).html("");
								var json_obj = $.parseJSON(result);
								 var output = '';
								for (var i in json_obj) 
            					{
                					$("#"+elementId).append('<option value="'+String(json_obj[i].courseID)+'">'+String(json_obj[i].courseName)+'</option>');
								}
					
								 
								 $("#"+elementId).selectpicker('refresh');
}
                      });
      });
	  */
	  
	  
	  $(document).on("change", ".dynamic-course" , function(e) {
		  e.stopImmediatePropagation();
		
		  var stype = $("#school-type").val();
		  //stype = (typeof(stype) == "object")? stype.join(",") : "";
		  console.log(stype);
		  var slevel = $("#school-level").val();
		  slevel =  (typeof(slevel) == "object")? slevel.join(",") : slevel;
	
		  var syear = $("#school-year").val();
		  syear =  (typeof(syear) == "object")? syear.join(",") : syear;		
		  
		  if(stype == <?php echo ELEMENTRY_TYPE_ID; ?>){
		  	 $("#school-level").parent().parent().css("display","none");
			  $("#school-level").prop("disbaled", true);
		  	 $("#school-year").parent().parent().css("display","none");
			  $("#school-year").prop("disbaled", true);
			  slevel = "";
			  syear = "";
		  }else{
		  	 $("#school-level").parent().parent().css("display","block");
			  $("#school-level").prop("disbaled", false);
		  	 $("#school-year").parent().parent().css("display","block");
			  $("#school-year").prop("disbaled", false);
		  }
		  
		   $.ajax({
                            url: 'actions/fetch_courses2.php',
                            data: {'typeID': stype, 'levelID':slevel, 'yearID':syear},
				  			type: "POST",
							//dataType: "json",
                            success: function (result) {
                					$("#school-course1").html(result);
								$("#school-course1").selectpicker('refresh');
							}
                      }); 
		  
	  }); //show-tick ends
	  
	  
	  /* Courses AJAX Calls */
      $( "#add-more-courses" ).click(function() {
		  var count = $(".body").children().find(".row").length-1;
		  var te = count;
		  $("#newCourse").append('<div class="row clearfix" id="course-row'+te+'" ><input type="hidden" name="counter" class="counter" value="'+te+'" /><div class="col-lg-3 col-md-6"><p> <b>Select School Type *</b> </p><select class="form-control show-tick school-type" name="select-school-type[]" required></select></div><div class="col-lg-3 col-md-6"><p> <b>Select School Level *</b> </p><select class="form-control show-tick school-level" required name="school-level[]"></select></div><div class="col-lg-3 col-md-6"><p> <b>Select School Year *</b> </p><select class="form-control show-tick school-year" required name="school-year[]"></select></div><div class="col-lg-3 col-md-6"><p> <b>Select School Course *</b> </p><select class="form-control show-tick school-course" required name="school-course[]" id="school-course'+te+'"></select></div></div>');
		  fetch_preinfo("SchoolType","school-type","course-row"+te);
		   fetch_preinfo("SchoolLevel","school-level","course-row"+te);
	  fetch_preinfo("SchoolYear","school-year","course-row"+te);
		  $(".school-course").selectpicker('refresh');
	  });
	  
  });
		

 function countChar(val, totalCharater, elem) {
        var len = val.value.length;
        if (len >= totalCharater) {
          val.value = val.value.substring(0, totalCharater);
        } else {
          $('#'+elem).text(totalCharater - len);
        }
      };
	</script>
</body>
</html>
<?php
}
?>
<div class="modal modal-fullscreen fade" id="modal-delete-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Weet je zeker dat je het verzoek wilt annuleren?</h6>
      </div>
      <div class="modal-body">
        <div id="deresulttype"></div>
        <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                          <input type="hidden" id="delete-id-type" name="delete-id-type" />
			<input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                      <div class="form-group">
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                      
                          <button type="button" class="btn btn-success" id="delete-yes-type">Ja</button>
                          <button type="button" class="btn btn-danger" id="delete-no-type">Nee</button>
                           
                        </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                      </div>
                    </form>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>


<div class="modal modal-fullscreen fade" id="modal-delete-course" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h6 class="modal-title" id="myModalLabel">Are you sure you want to delete selected record(s)?</h6>
      </div>
      <div class="modal-body">
        <div id="deresultcourse"></div>
        <form id="delete-form-course" class="form-horizontal form-label-left" method="post" action="">
                          <input type="hidden" id="delete-id-course" name="delete-id-course" />
			<input type="hidden" id="delete-choice" name="delete-choice" value="TeacherAssignedCourse"/>
                      <div class="form-group">
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                      
                          <button type="button" class="btn btn-success" id="delete-yes-course">Yes</button>
                          <button type="button" class="btn btn-danger" id="delete-no-course">No</button>
                           
                        </div>
						  <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                      </div>
                    </form>
        <!-- -->
      </div>
      
    </div>
  </div>
</div>

<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>
        
	<script>
		
		var lat=<?php echo ($lat)? $lat : 33.719361; ?>;
		var lng =<?php echo ($lng)? $lng : 73.074144; ?>;
		var place_name = <?php echo ($place_name)? "'".$place_name."'" : "''"; ?>;
		var map = null;
		var canvas = null;
		
mapboxgl.accessToken = "<?php echo ACCESS_TOKEN; ?>";
 map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11',
center: [lng, lat],
zoom: 13
});
 
var geocoder = new MapboxGeocoder({
accessToken: mapboxgl.accessToken, limit:10, mode:'mapbox.places', types:'address' ,countries: 'nl'
});
 
map.addControl(geocoder);
	$(".mapboxgl-ctrl-geocoder input").val(place_name);

// After the map style has loaded on the page, add a source layer and default
// styling for a single point.
map.on('load', function() {
// Listen for the `result` event from the MapboxGeocoder that is triggered when a user
// makes a selection and add a symbol that matches the result.
geocoder.on('result', function(ev) {
	console.log("Here: ",ev);
	lat = ev.result.center[1];
	lng = ev.result.center[0];
	place_name = ev.result.place_name;
	
	$("#lat").val(lat);
	$("#lng").val(lng);
	$("#place_name").val(place_name);
	
	isAddressChanged = true;
map.getSource('point').setData(ev.result.geometry);
});
});









 canvas = map.getCanvasContainer();
 
var geojson = {
"type": "FeatureCollection",
"features": [{
"type": "Feature",
"geometry": {
"type": "Point",
"coordinates": [lng, lat]
}
}]
};
 
function onMove(e) {
var coords = e.lngLat;
 
// Set a UI indicator for dragging.
canvas.style.cursor = 'grabbing';
 
// Update the Point feature in `geojson` coordinates
// and call setData to the source layer `point` on it.
geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
map.getSource('point').setData(geojson);
}
 
function onUp(e) {
var coords = e.lngLat;
 console.log(e.lngLat);
	lat = e.lngLat.lat;
	lng = e.lngLat.lng;	
	
	$("#lat").val(lat);
	$("#lng").val(lng);
	isAddressChanged = true;
// Print the coordinates of where the point had
// finished being dragged to on the map.
coordinates.style.display = 'block';
//coordinates.innerHTML = 'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat;
canvas.style.cursor = '';
// geocoder.query("-122.1162864,37.4133183");
	geocoder['eventManager']['limit'] = 1;	
	geocoder['options']['limit'] = 1;	
   geocoder.query(lng+","+lat);
  
	
// Unbind mouse/touch events
map.off('mousemove', onMove);
map.off('touchmove', onMove);
	geocoder['eventManager']['limit'] = 10;	
	geocoder['options']['limit'] = 10;	
}
 
map.on('load', function() {
 
// Add a single point to the map
map.addSource('point', {
"type": "geojson",
"data": geojson
});
 
map.addLayer({
"id": "point",
"type": "circle",
"source": "point",
"paint": {
"circle-radius": 10,
"circle-color": "#3887be"
}
});
 
// When the cursor enters a feature in the point layer, prepare for dragging.
map.on('mouseenter', 'point', function() {
map.setPaintProperty('point', 'circle-color', '#3bb2d0');
canvas.style.cursor = 'move';
});
 
map.on('mouseleave', 'point', function() {
map.setPaintProperty('point', 'circle-color', '#3887be');
canvas.style.cursor = '';
});
 
map.on('mousedown', 'point', function(e) {
// Prevent the default map drag behavior.
e.preventDefault();
 
canvas.style.cursor = 'grab';
 
map.on('mousemove', onMove);
map.once('mouseup', onUp);
});
 
map.on('touchstart', 'point', function(e) {
if (e.points.length !== 1) return;
 
// Prevent the default map drag behavior.
e.preventDefault();
 
map.on('touchmove', onMove);
map.once('touchend', onUp);
});

});

</script>
	
<script>
	 $(document).ready(function() {
			$("#showHide").on('change', function () {
			if ($(this).is(":checked")) {
				$(".password").attr("type", "text");
				//$("#labelforShowHide").html("Hide Password");
			} else {
				$(".password").attr("type", "password");
				//$("#labelforShowHide").html("Show Password");
				
			}
		});
		 var sel = "";
		 var tog = "";
		$('#radioBtn a').on('click', function(){
			//alert(tog+"--"+sel);
				 sel = $(this).data('title');
				 tog = $(this).data('toggle');

			$('#modal-delete-type').appendTo("body").modal('show'); 
			
		});		
		 
			$("#delete-yes-type").click(function(){
				$('#'+tog).prop('value', sel);
				
				$('a[data-toggle="'+tog+'"]').not('[data-title="'+sel+'"]').removeClass('active').addClass('notActive');
				$('a[data-toggle="'+tog+'"][data-title="'+sel+'"]').removeClass('notActive').addClass('active');
				
				$("#modal-delete-type").modal('hide');
			});

			  $("#delete-no-type").click(function(){
				 $("#modal-delete-type").modal('hide');  	
			  });		 
	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.js"></script>
<script>
	/***************************image crop****************************/
var picture_width;
var picture_height;
var crop_max_width = 400;
var crop_max_height = 280;
	

  $(document).ready(function(){
	
	 //$('#jcrop-image').Jcrop({setSelect:   [ 0, 0, crop_max_width, crop_max_height]});
	  
	$("#file").change(function(){
	picture(this);
});
	$("#info").submit(function(){
		e.preventDefault();
		var base64 = $("#png").val();
		console.log("submitting...............");
		//Remove base64 string value from #png input to prevent it form being sent
		$("#png").val("");
		//Get formdata
		//formData = new FormData($(this)[0]);
		//Convert base64 string to file blob
		var blob = dataURLtoBlob(base64); 
		$("#png").val(blob);
		//Add file blob to the form data
		//formData.append("cropped_image[]", blob);	
	});	  


function picture(input) {
	if (input.files && input.files[0]) {
		var reader = new FileReader();
		reader.onload = function (e) {
			$("#jcrop").html("").append("<img  src=\""+e.target.result+"\" alt=\"\" />");
			picture_width = $("#jcrop img").width();
			picture_height = $("#jcrop img").height();
			console.log(picture_width+":::"+picture_height);
			/**************center selection****************/
			var	  x = Math.abs(( (picture_width/2) - (700/2)));
			var y = Math.abs(( (picture_height/2) - (700/2)  ));
			var x1 = x + 700;
			var y1 = y + 700;	
			/**************center selection****************/
			$("#jcrop  img").Jcrop({
				 aspectRatio: 1,
				onChange: canvas,
				onSelect: canvas,
				setSelect:   [ 50, 50,crop_max_height, crop_max_height ],
				boxWidth: crop_max_width,
				boxHeight: crop_max_height
			});
		}
		reader.readAsDataURL(input.files[0]);
	}
}
function canvas(_coords){
	var imageObj = $("#jcrop img")[0];
	var canvas = $("#canvas")[0];
	canvas.width  = _coords.w;
	canvas.height = _coords.h;
	var context = canvas.getContext("2d");
	context.drawImage(imageObj, _coords.x, _coords.y, _coords.w, _coords.h, 0, 0, canvas.width, canvas.height);
	png();
}
function png() {
	var png = $("#canvas")[0].toDataURL('image/png');
	$("#png").val(png);
}
function dataURLtoBlob(dataURL) {
	var BASE64_MARKER = ';base64,';
	if(dataURL.indexOf(BASE64_MARKER) == -1) {
		var parts = dataURL.split(',');
		var contentType = parts[0].split(':')[1];
		var raw = decodeURIComponent(parts[1]);

		return new Blob([raw], {type: contentType});
	}
	var parts = dataURL.split(BASE64_MARKER);
	var contentType = parts[0].split(':')[1];
	var raw = window.atob(parts[1]);
	var rawLength = raw.length;
	var uInt8Array = new Uint8Array(rawLength);
	for(var i = 0; i < rawLength; ++i) {
		uInt8Array[i] = raw.charCodeAt(i);
	}

	return new Blob([uInt8Array], {type: contentType});
}
	  
});	
	</script>
