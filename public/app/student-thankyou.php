<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Door in te loggen, boek je direct de bijlesdocent naar jouw wensen | Laat je gerust eerst adviseren door onze specialisten.">

<title>Registeren | Bijles Aan Huis</title>
	<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
	
	?>
    <!-- Custom Css -->
    <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />	
    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
</head>

<body class="theme-green authentication login-body">

<!-- Navbar -->

<!-- Overlay For Sidebars -->
<div class="overlay"></div>
<?php
        require_once('includes/header.php');
?>
<!-- End Navbar -->
<div class="page-header">
   <!-- <div class="page-header-image" style="background-image:url(img/online-3307293_1920.jpg)"></div> -->
    <div class="container content-container">
        <div class="col-md-12 login-container login-container--md-padding">
            <div class="card-plain">
                <form class="form login-form" method="" action="">
                    <div class="header">
                        <h5 class="ma-heading login-form-header">Welkom!</h5>
                    </div>
                    <div class="login-form-content">
                        <p class="login-form__text">Bedankt voor je registratie! We hebben een e-mail verstuurd, zodat je je account kan activeren. Heb je een vraag? We zijn er graag voor je, dus neem gerust  <a href="https://bijlesaanhuis.nl/contact/">hier</a>  contact met ons op. Of krijg nog sneller reactie via onze live chat rechtsonder!</p>
                    </div>
                    <div class="login-form__button-container">
                        <a href="/app/" class="btn login-form-button">Naar de hoofdpagina</a>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <?php
   		require_once('includes/footerScriptsAddForms.php');
	?>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
</script>
</body>
</html>