<?php
/*
Template Name: Teacher Profile
Template Post Type: post, page, event
*/

//use API\Price;

session_start();

require_once('includes/connection.php');
require_once('../../inc/Price.php');
$thisPage = "Detailed Profile View";

if (!isset($_SESSION['Student']) && !isset($_REQUEST['tid'])) {
    header('Location: index.php');
} else if (isset($_REQUEST['tid'])) {
    $tid = $con->real_escape_string($_REQUEST['tid']);


    $query = "select
       GROUP_CONCAT(DISTINCT crs.coursename) courses_name,
       COUNT(DISTINCT crs.courseID)          courses_count,
       tlr.rate,
       tlr.level,
       tlr.cost_per_km,
       u.active,
       p.image,
       p.shortdescription,
       p.description,
       t.onlineteaching,
       t.travelRange,
       t.allownewstudents,
       c.*
from teacher t
left join teacherlevel_rate tlr on tlr.ID=t.teacherlevel_rate_ID
left join profile p on t.profileID = p.profileID
left join contact c on t.userID = c.userID
left join user u on c.userID = u.userID
left join teacher_courses tc on t.userID = tc.teacherID
left join courses crs on crs.schoolcourseID = tc.courseID
where t.userID={$tid}
group by t.userID";
    $result = mysqli_query($con, $query);
    $_row = mysqli_fetch_assoc($result);

    $allownewStudents = (isset($_row['allownewstudents']) ? $_row['allownewstudents'] : '');
    $costPerKm = (isset($_row['cost_per_km']) ? $_row['cost_per_km'] : null);
    $coursesName = (isset($_row['courses_name']) ? $_row['courses_name'] : '');
    $description = (isset($_row['description']) ? $_row['description'] : '');
    $firstName = (isset($_row['firstname']) ? $_row['firstname'] : '');
    $level = (isset($_row['level']) ? $_row['level'] : '');
    $latitude = (isset($_row['latitude']) ? $_row['latitude'] : null);
    $longitude = (isset($_row['longitude']) ? $_row['longitude'] : null);
    $onlineTeaching = (isset($_row['onlineteaching']) ? $_row['onlineteaching'] : '');
    $profileImage = (isset($_row['image']) ? $_row['image'] : '');
    $rate = (isset($_row['rate']) ? $_row['rate'] : null);
    $shortDescription = (isset($_row['shortdescription']) ? $_row['shortdescription'] : '');
    $travelRange = (isset($_row['travelRange']) ? $_row['travelRange'] : null);


    $query1 = "SELECT  tcrs.*,crs.* FROM `teacher` t,`courses` crs, `teacher_courses` tcrs where tcrs.permission='Accepted' crs.courseID = tcrs.courseID 	AND t.teacherID = tcrs.teacherID AND tcrs.teacherID = '$tid'";
    $courses = mysqli_query($con, $query1);


    $query3 = "SELECT * FROM teacherslots WHERE teacherID = '$tid'";
    $timeslots = mysqli_query($con, $query3);
    $time = mysqli_fetch_assoc($timeslots);
    //if(!$result)
    // header('Location: index.php');
    //	$query4 = "select (SELECT COUNT(DISTINCT slot_group_id)class FROM `calendarbooking` cb where isClassTaken = 0 AND t.userID = cb.teacherID GROUP BY teacherID)completedLessons, t.onlineteaching, t.allownewstudents, tlr.level, tlr.rate from teacher t, teacherlevel_rate tlr  where t.teacherlevel_rate_ID = tlr.ID AND t.userID = '$tid'";
    // 	$rlc = mysqli_query($con, $query4); // rate , level, courses count and online teaching status
    //	$data_rlc = mysqli_fetch_assoc($rlc);
    //print_r($rate_level_courses);
    // $d = mysqli_fetch_assoc($result);

    $r = mysqli_query($con, "SELECT value FROM variables WHERE name='freeTravelRange'");
    $r = mysqli_fetch_array($r);
    $travelBoundary1 = $r[0];

    $distCost = null;
    $online = false;
    $lat = 0;
    $long = 0;
    if (!empty($_SESSION['StudentID'])) {
        $s = mysqli_query($con, "SELECT * FROM contact WHERE userID=" . $_SESSION['StudentID']);
        if ($s) {
            $s = mysqli_fetch_assoc($s);
            $lat = $s['latitude'];
            $long = $s['longitude'];
        }
    } else if (!empty($_SESSION['std_lat']) && !empty($_SESSION['std_lng'])) {
        $lat = $_SESSION['std_lat'];
        $long = $_SESSION['std_lng'];
    }
    if ($lat != 0 && $long != 0) {
        $distance = Price::Distance($latitude, $longitude, $lat, $long);
        if ($distance > $travelBoundary1 && !empty($travelRange) && $distance <= $travelRange) {
            $distCost = Price::TravelCosts($costPerKm, $distance, $travelBoundary1);
        } else if ($distance > $travelRange && $onlineTeaching == 1) {
            $online = true;
        }
    }

    $query = $db->createQueryBuilder()
        ->select('*')
        ->from('teacherslots')
        ->where('teacherID=?')
        ->setParameter(1, $_REQUEST['tid'])
        ->setMaxResults(1)
        ->execute();
    $available = $query && $query->rowCount() > 0;
?>
    <html>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <title><?php echo $firstName ?>'s profiel | Bijles Aan Huis</title>
    <?php
    require_once('includes/mainCSSFiles.php');
    ?>
    <link rel="stylesheet" type="text/css" href="https://cdn.mytutor.co.uk/primefaces/fa/font-awesome.css?1523486454000">
    <link rel="stylesheet" type="text/css" href="assets/css/tutorprofile.css">
    <link rel="stylesheet" type="text/css" href="https://www.jqueryscript.net/css/jquerysctipttop.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel="stylesheet" type="text/css" href="https://use.fontawesome.com/releases/v5.7.0/css/all.css" integrity="sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Poppins">
    <link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="assets/css/nigel.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-select/css/bootstrap-select.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-multiselect.css">
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Nunito:400,600|Source+Sans+Pro:300,400,400i,600,600i,700,900">
    <link rel="stylesheet" type="text/css" href="calendar/css/calendar.css">

    <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script>
        $(document).ready(function() {
            var text = $("#aboutMeShort").text();

            if (text.length > 600) {
                var shortText = $("#aboutMeShort").text();
                $("#aboutMeFull").css("display", "none");
                $("#showMore").css("display", "inline");
            }

            $("#showMore").on("click", function() {
                $("#showLess").css("display", "block");
                $("#aboutMeShort").hide();
                $("#showMore").hide();
                $("#aboutMeFull").css("display", "inline");



            });
            $("#showLess").on("click", function() {
                $("#aboutMeShort").show();
                $("#aboutMeFull").css("display", "none");
                $("#showLess").hide();
                $("#showMore").show();

            });



        });
    </script>
    <?php
    $activePage = basename($_SERVER['PHP_SELF']);
    ?>

    </head>

    <body class="dashboard-calendar">

        <div class="overlay"></div>
        <?php
        require_once('includes/header.php');
        ?>

        <style>
            .dashboard-calendar #calendar {
                padding: 0;
                width: 100% !important;
                margin: 10px auto;
            }

            .appointment-block h3.appointment-block__title {
                margin-top: 0;
            }

            .appointment-block p.appointment-block__text {
                font-size: 14px;
                margin: 0;
            }

            .appointment-block p.appointment-block__top-right {
                margin-bottom: 10px;
            }

            .appointment-block p.appointment-block__text:last-child {
                padding-bottom: 0;
            }

            #calender_startTime,
            #calender_endTime {
                color: #486066 !important;
                border-color: black !important;
            }

            #cal-slide-content ul.unstyled.list-unstyled {
                padding-bottom: 0;
            }

            .fa,
            .far,
            .fas {
                margin-right: 10px !important;
            }

            @media screen and (max-width:576px) {
                .appointment-block__startTime {
                    margin: 0 !important;
                }

                .profile-detail-calendar {
                    margin: 10px -3%;
                }
            }


            @media (max-width: 643px) {
                .tutormeta__avatar {
                    margin-right: 0;
                    margin-top: 20px;
                    display: flex;
                    align-items: center;
                }

                .tutormeta__avatar .profileimage {
                    width: 160px;
                    height: 160px;
                }
            }
        </style>
        <section class="content page-calendar">
            <div id="container1" class="page-container1">
                <div class="container1 container1--public">
                    <div class="row row--full-width-mobile main-row">
                        <?php
                        if (!isset($_SESSION['Student']) && !isset($_SESSION['Teacher']))
                            echo '<div class="profile-info">';
                        else
                            echo '<div class="profile-info">';
                        ?>
                        <div class="container1__card container1__card--tutor container1__card--tutor--profile">
                            <div class="tutorprofile__subsection tutorprofile__subsection--top">
                                <div class="tutormeta__avatar">
                                    <?php $profilepic =  empty($_row['image']) ? 'profile_av.jpg' : basename($_row['image']); ?>
                                    <img src="<?php echo '/s3/profileImages/' . $profilepic; ?>" alt="Tutor-Name M." class="profileimage updateOnSaveCropProfileImage" />
                                </div>
                                <div class="left-right">
                                    <div class="tutorprofile-subtitle">
                                        <div class="tutorprofile-subtitle__name">
                                            <?php echo $firstName . '&nbsp'; ?>
                                        </div>
                                        <div class="tutorprofile-subtitle__level">
                                            <?php if ($_row['level'] == 'Junior') { ?>
                                                <div style="padding-top: 5px;"> <?php echo $_row['level']; ?> docent
                                                    <i class="fa info-icon tippy" data-tippy-content="Voor al onze docenten garanderen wij onze hoge kwaliteit. Een Junior docent heeft de stof recent zeer succesvol gevolgd, waardoor het erg vers in het geheugen zit. <?php echo $firstName; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. Het tarief van een Junior docent is <b>€ <?php echo $rate; ?></b>,- per lesuur van 45 minuten.">&#xf05a;</i>
                                                </div>
                                            <?php } ?>
                                            <?php if ($_row['level'] == 'Senior') { ?>
                                                <div> <?php echo $_row['level']; ?> docent
                                                    <i class="fa info-icon tippy" data-tippy-content="Een Senior docent heeft, vaak door middel van een gerelateerde studie, een uitmuntende kennis in zijn/haar vakgebied. <?php echo $firstName; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. Het tarief van een Senior docent is <b>€ <?php echo $rate; ?></b>,- per lesuur van 45 minuten.">&#xf05a;</i>
                                                </div>
                                            <?php } ?>
                                            <?php if ($_row['level'] == 'Supreme') { ?>
                                                <div> <?php echo $_row['level']; ?> docent
                                                    <i class="fa info-icon tippy" data-tippy-content="Een Supreme docent is een professional is in zijn/haar vakgebied. <?php echo $firstName; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. Het tarief van een Supreme docent is <b>€ <?php echo $rate; ?></b>,- per lesuur van 45 minuten.">&#xf05a;</i>
                                                </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="tutorprofile-info">
                                        <span class="custom_info_bar">
                                            <i class='fas fa-book green-icon'></i>
                                            <?php
                                            $crs = explode(",", $_row['courses_name']); // make the array
                                            for ($i = 1; $i <= count($crs); $i++) {
                                                if ($i == count($crs) && count($crs) > 1) {
                                                    $crs[$i - 1] = 'en ' . $crs[$i - 1];
                                                }
                                            }
                                            $subj = implode(", ", $crs); // convert to string
                                            $lastComma = strrpos($subj, ",");
                                            $subj1 = substr($subj, 0, $lastComma);
                                            $subj2 = substr($subj, $lastComma + 1, strlen($subj) - 1);
                                            /*$count = count($crs);
                                        $subject = "";
                                        for($i=0; $i<$count; $i++){ if($i > 1){ $subject .= " and more "; break;} $subject .= $crs[$i].","; } echo $subject;*/
                                            echo (count($crs) > 1) ? $subj1 . $subj2 : $subj;
                                            ?>
                                        </span>
                                        <?php
                                        if ($_row['onlineteaching']) { ?>
                                            <span class='custom_info_bar'>
                                                <i class='fas green-icon'>&#xf109;</i>
                                                Geeft online bijles<i class='fa info-icon tippy' data-toggle='tooltip' data-placement='right' data-tippy-content='Ons speciaal voor bijles gebouwde online platform is eind december 2019 af. Informatie hierover vind je <a href="/bijles/online">hier</a>. Tot die tijd adviseren wij het gebruik van Skype.'>&#xf05a;</i>
                                            </span>
                                        <?php } ?>
                                        <?php if (isset($_SESSION['StudentID']) && $distCost > 0) { ?>
                                            <span class="custom_info_bar">
                                                <i class="fas orange-icon">&#xf206;</i>
                                                Je betaalt voor deze docent reiskosten <i class="fa info-icon tippy" data-tippy-content="Het zou kunnen dat voor deze docent reiskosten gerekend worden. Om dit te zien, dient u deze docent op de zoeken in de vind docent pagina, waarna dit in het mini profiel wordt aangegeven.">&#xf05a;</i>
                                            </span>
                                        <?php } ?>
                                        <?php echo ($_row['allownewstudents']) ? " " : "<span class='custom_info_bar'><i  class='fas red-icon fa-user'></i> Niet beschikbaar voor nieuwe leerlingen</span>"; ?>
                                        <?php if (!isset($_SESSION['TeacherID'])) : ?>
                                            <span class='custom_info_bar'>
                                                <i class='fas green-icon fa-dollar-sign' style='float:left;'></i>
                                                € <?php echo number_format($_row['rate'], 2, ".", ","); ?> per lesuur <?php echo empty($distCost) ? "" : " + €{$distCost} reiskosten"; ?>
                                                <i class="fa info-icon tippy" data-tippy-content='Een lesuur duurt 45 minuten. De duur van een bijles kan in overeenstemming met de docent bepaald worden. Hierbij hanteert Bijles Aan Huis 90 minuten als richtinglijn. Meer weten over reiskosten? Kik <a href="/reiskosten" target="_blank">hier</a>!'>&#xf05a;</i>
                                            </span>
                                        <?php endif; ?>
                                    </div>
                                </div>
                            </div>
                            <div class="tutorprofile__subsection">
                                <h4 class="tutorprofile__subheader1" style="padding-bottom: 10px;font-size:21px;">Over <?php echo $firstName; ?></h4>
                                <div>
                                    <div>
                                        <span class="tutorprofile__paragraph">
                                            <?php
                                            $shortProfileDescription = $_row['description'];
                                            $maxCharacterLength = 600;
                                            if (strlen($shortProfileDescription) > $maxCharacterLength) {
                                                $shortProfileDescription = wordwrap($shortProfileDescription, $maxCharacterLength);
                                                $shortProfileDescription = substr($shortProfileDescription, 0, strpos($shortProfileDescription, "\n"));
                                            ?>
                                                <p id="aboutMeShort" style="display: inline;"><?php echo nl2br($shortProfileDescription); ?>... </p>
                                                <p id="aboutMeFull"><?php echo (nl2br($_row['description'])) ? nl2br($_row['description']) : " "; ?></p>
                                                <span class="js-truncate-button" style="display:inline-block;">
                                                    <?php if (!empty($_row['description']) && strlen($_row['description']) > $maxCharacterLength) { ?>
                                                        <a id="showMore" class="link__more _link__more">meer tonen</a>
                                                        <a id="showLess" class="link__more _link__more">minder tonen</a>
                                                    <?php } ?>
                                                </span>
                                            <?php
                                            } else {
                                                echo "<p>" . nl2br($shortProfileDescription) . "</p>";
                                            }
                                            ?>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form class="message-box send-message-box desktop" method="post" enctype="application/x-www-form-urlencoded">
                        <h3 class="box-title">Stuur <?php echo $firstName; ?> een berichtje</h3>
                        <div class="tutor-message">
                            <p>Hey ik ben <?php echo $firstName;  ?> Ik sta graag voor je klaar om vragen te beantwoorden. Ik reageer zo snel mogelijk!</p>
                        </div>
                        <textarea class="message-area" id="msgText" name="msgText" placeholder="Hey <?php echo $firstName;  ?>, ik ben op zoek naar een bijlesdocent. Kunnen we kennismaken?" maxlength="2147483647"></textarea>
                        <button type="button" class="send-button" name="tutorprofileContactForm_SUBMIT" id="sendMsg">Bericht versturen</button>
                    </form>
                    <div class="modal-container" id="modal-container">
                        <div class="modal-box">
                            <i class="fas fa-times"></i>
                            <form class="send-message-box mobile" method="post" enctype="application/x-www-form-urlencoded">
                                <h3 class="box-title" style="padding-bottom:10px;">
                                    Stuur <?php echo $firstName; ?> een berichtje
                                </h3>
                                <div class="tutor-message">
                                    <p>
                                        Hey ik ben <?php echo $firstName;  ?> Ik sta graag voor je klaar om vragen te beantwoorden. Ik reageer zo snel mogelijk!
                                    </p>
                                </div>
                                <textarea class="message-area" id="msgTextMobile" name="msgText"></textarea>
                                <button type="button" class="send-button" name="tutorprofileContactForm_SUBMIT" id="sendMsg">Bericht versturen</button>
                            </form>
                        </div>
                    </div>
                    <div class="row w-100">
                        <div class="open-message-modal" id="open-message-modal" style="font-size: 18px;">
                            Bericht versturen
                        </div>
                    </div>
                </div>
                <div id="teacher-detail-view-hash" class="profile-detail-container">
                    <div class="profile-detail-section" style="background-color:white;">
                        <div class="profile-detail-header" style="padding:0;">
                            <h2 class="profile-detail-title ma-subtitle" style="font-size:21px !important;padding-bottom:20px;border-bottom: 1px solid #486066;margin-top:0;">Boek een afspraak met&nbsp;<?php echo $firstName; ?></h2>
                            <div class="profile-detail-date">
                                <h3 class="ma-subtitle profile-detail-date__heading" style="color: #486066 !important;font-size:21px !important;display: inline-block;padding-right:10px;margin-top:0;"></h3>
                                <p class="profile-detail-date__sub-heading" style="display: inline-block;font-size: 14px;">selecteer de dag en de tijd waarop je bijles wilt</p>
                            </div>
                            <div class="colorLegend">
                                <div class="colorLegend-p" style="font-size: 14px;">
                                    <span class="color available" style="margin-left: 0;"></span>
                                    Beschikbaar
                                </div>
                            </div>
                            <div class="pull-right form-inline">
                                <div class="btn-group">
                                    <button class="btn btn-primary" data-calendar-nav="prev">
                                        << </button> <button class="btn btn-default" data-calendar-nav="today">Today
                                    </button>
                                    <button class="btn btn-primary" data-calendar-nav="next">>> </button>
                                </div>
                            </div>
                        </div>
                        <div class="profile-detail-calendar" style="position:relative;">
                            <div id="calendar"></div>
                            <?php if (!$available) : ?>
                                <div class="unavailable-overlay">
                                    <div class="unavailable-modal">
                                        <h4 class="unavailable-modal__title">Stuur <?php echo $firstName; ?> een berichtje</h4>
                                        <p class="unavailable-modal__content">Ondanks dat ik mijn kalender niet heb ingevuld, ben ik graag voor je beschikbaar. Stuur mij gerust een <a class="chat-focus" href="#container1">berichtje</a> en dan kom ik snel bij je terug!</p>
                                    </div>
                                </div>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php
        require_once('includes/footerScriptsAddForms.php');
        ?>

        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
        <script type="text/javascript" src="calendar/js/calendar.js"></script>
        <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
        <script src="assets/js/pages/ui/notifications.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>
            // this function is added
            Date.prototype.setTimezoneOffset = function(minutes) {
                var _minutes;
                if (this.timezoneOffset == _minutes) {
                    _minutes = this.getTimezoneOffset();
                } else {
                    _minutes = this.timezoneOffset;
                }
                if (arguments.length) {
                    this.timezoneOffset = minutes;
                } else {
                    this.timezoneOffset = minutes = this.getTimezoneOffset();
                }
                return this.setTime(this.getTime() + (_minutes - minutes) * 6e4);
            };

            var calendar;
            var options;
            var teacherid = <?php echo @$_GET['tid']; ?>;
            var studentid = "<?php echo @$_SESSION['StudentID']; ?>";
            var hourlyCost = <?php echo (@$data['customhourly']) ? @$data['customhourly'] : 0; ?>;
            var travelCost = <?php echo (@$_GET['c']) ? @$_GET['c'] : 0; ?>;
            var travelDistance = <?php echo (@$_GET['d']) ? @$_GET['d'] : 0; ?>;
            var std_lat = "<?php echo @$_SESSION['std_lat']; ?>";
            var std_lng = "<?php echo @$_SESSION['std_lng']; ?>";
            var place_name = "<?php echo @$_SESSION['std_place_name']; ?>";
            var calendar;
            var options;
            var _obj = null;
            var _id = null;
            var _name = null;
            var _ddate = null;
            var _stime = null;
            var _etime = null;
            var endcounter = null;
            var __calender_date = null;
            // stops multiple pops shown on first book
            var bookingCount = 0;

            $(document).ready(function() {

                $(".cal-month-day").dblclick(function(e) {
                    e.preventDefault();
                });

                $(".page-loader-wrapper").css("display", "none");

                "use strict";

                <?php
                $url = "/api/Calendar/GetFreeSlotsTutor?teacherID=";
                if (isset($_GET['tid']))
                    $url .= $_GET['tid'];
                else
                    $url .= $_SESSION['TeacherID'];
                ?>
                options = {
                    events_source: '<?php echo $url; ?>',
                    view: 'month',
                    tmpl_path: 'calendar/tmpls/teacher-detailed/',
                    tmpl_cache: false,
                    // day: '2013-03-21',
                    // day: 'now',

                    merge_holidays: false,
                    display_week_numbers: false,
                    weekbox: false,
                    //shows events which fits between time_start and time_end
                    show_events_which_fits_time: true,

                    onAfterEventsLoad: function(events) {
                        if (!events) {
                            return;
                        }
                        var list = $('#eventlist');
                        list.html('');

                        $.each(events, function(key, obj) {
                            //converting the timeZone to UTC
                            obj.start = new Date(parseInt(obj.start)).setTimezoneOffset(0);
                            obj.end = new Date(parseInt(obj.end)).setTimezoneOffset(0);

                            $(document.createElement('li'))
                                .html('<a href="' + obj.url + '">' + obj.title + '</a>')
                                .appendTo(list);
                        });
                    },
                    onAfterViewLoad: function(view) {
                        $('.profile-detail-date__heading').text(this.getTitle());
                        $('.btn-group button').removeClass('active');
                        $('button[data-calendar-view="' + view + '"]').addClass('active');
                    },
                    classes: {
                        months: {
                            general: 'label'
                        }
                    }
                };

                calendar = $('#calendar').calendar(options);

                $('.btn-group button[data-calendar-nav]').each(function() {
                    var $this = $(this);
                    $this.click(function() {
                        calendar.navigate($this.data('calendar-nav'));
                    });
                });

                $('.btn-group button[data-calendar-view]').each(function() {
                    var $this = $(this);
                    $this.click(function() {
                        calendar.view($this.data('calendar-view'));
                    });
                });

                $('#first_day').change(function() {
                    var value = $(this).val();
                    value = value.length ? parseInt(value) : null;
                    calendar.setOptions({
                        first_day: value
                    });
                    calendar.view();
                });

                $('#language').change(function() {
                    calendar.setLanguage($(this).val());
                    calendar.view();
                });

                $('#events-in-modal').change(function() {
                    var val = $(this).is(':checked') ? $(this).val() : null;
                    calendar.setOptions({
                        modal: val
                    });
                });
                $('#format-12-hours').change(function() {
                    var val = $(this).is(':checked') ? true : false;
                    calendar.setOptions({
                        format12: val
                    });
                    calendar.view();
                });
                $('#show_wbn').change(function() {
                    var val = $(this).is(':checked') ? true : false;
                    calendar.setOptions({
                        display_week_numbers: val
                    });
                    calendar.view();
                });
                $('#show_wb').change(function() {
                    var val = $(this).is(':checked') ? true : false;
                    calendar.setOptions({
                        weekbox: val
                    });
                    calendar.view();
                });
                $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e) {
                    //e.preventDefault();
                    //e.stopPropagation();
                });

                function diff_hours(dt2, dt1) {
                    var diff = (dt2.getTime() - dt1.getTime()) / 1000;
                    diff /= (60 * 60);
                    return Math.abs(Math.round(diff));
                }

                var add_minutes = function(dt, minutes) {
                    return new Date(dt.getTime() + minutes * 60000);
                };

                function dump(arr, level) {
                    var dumped_text = "";
                    if (!level) level = 0;

                    //The padding given at the beginning of the line.
                    var level_padding = "";
                    for (var j = 0; j < level + 1; j++) level_padding += "    ";

                    if (typeof(arr) == 'object') { //Array/Hashes/Objects 
                        for (var item in arr) {
                            var value = arr[item];

                            if (typeof(value) == 'object') { //If it is an array,
                                dumped_text += level_padding + "'" + item + "' ...\n";
                                dumped_text += dump(value, level + 1);
                            } else {
                                dumped_text += level_padding + "'" + item + "' => \"" + value + "\"\n";
                            }
                        }
                    } else { //Stings/Chars/Numbers etc.
                        dumped_text = "===>" + arr + "<===(" + typeof(arr) + ")";
                    }
                    return dumped_text;
                }

                $("body").on("change", "#calender_startTime", function() {
                    loadEndTimes();
                });


                $("body").on("mouseleave click mouse", ".cal-cell", function() {
                    loadEndTimes();
                });

                function loadEndTimes() {
                    let selStart = $("#calender_startTime");
                    let stime = selStart.val();
                    const minTime = parseInt(selStart.find(':selected').attr("data-min"));
                    const maxTime = parseInt(selStart.find(':selected').attr("data-max"));
                    const startTime = new Date("01/01/2007 " + stime);
                    let endTime;
                    let tempEndTime;
                    let str = "";
                    for (let i = minTime; i <= maxTime; i += 15) {
                        //starttime + x minutes
                        endTime = add_minutes(startTime, i);
                        tempEndTime = ((endTime.getHours() > 9) ? endTime.getHours() : "0" + endTime.getHours()) + ":" + ((endTime.getMinutes() > 9) ? endTime.getMinutes() : "0" + endTime.getMinutes());
                        str += "<option value='" + i + "'" + (i === 90 ? "selected" : "") + ">" + tempEndTime + "</option>";
                    }
                    $("#calender_endTime").html(str);
                }

                $(".list-item").on("mouseover", function(e) {
                    e.preventDefault();
                    console.log($(this).html());
                });

                $("body").on("click", ".PopupBtn", function() {
                    console.log('click send');
                    $(".modal-content").addClass('modal--no-scroll');
                    console.log('add class to: ');
                    console.log($(".modal-content"));
                    $('.registreren-modal-btn').on('click', function() {
                        $(".modal-content").removeClass('modal--no-scroll');
                    });
                });

                $("body").on("click", "#sendMsg", function() {
                    console.log('click send');
                    $(".modal-content").addClass('modal--no-scroll');
                    console.log('add class to: ');
                    console.log($(".modal-content"));
                    $('.registreren-modal-btn').on('click', function() {
                        $(".modal-content").removeClass('modal--no-scroll');
                    });
                    let msg = '';
                    let windowsize = $(window).width();
                    if (windowsize < 992) {
                        msg = $("#msgTextMobile").val();
                    } else {
                        msg = $("#msgText").val();
                    }

                    console.log(msg.length);

                    let subj = 'null';
                    let student = '<?php echo empty($_SESSION['StudentID']) ? '' : $_SESSION['StudentID']; ?>';
                    if (student.length > 0 && msg.length > 0) {
                        $.ajax({
                            type: "POST",
                            url: "includes/chat-ajax.php",
                            data: {
                                filename: "chat_text_msg_by_sender",
                                msg: msg,
                                senderId: student,
                                recvId: "<?php echo $_GET['tid']; ?>",
                                subj: subj
                            },
                            dataType: "text",
                            success: function(response) {
                                let res = JSON.parse(response);
                                if (res.status == "success") {
                                    $('.modal-container').removeClass('show-modal');
                                    $('body').removeClass('fixed-body');
                                    showNotification("alert-success", "Bericht verzonden !", "bottom", "center", "", "");
                                } else {
                                    $('.modal-container').removeClass('show-modal');
                                    $('body').removeClass('fixed-body');
                                    showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                                }
                                $("#msgText").val('');
                            },
                            error: function(xhr, ajaxOptions, thrownError) {
                                console.log("error", xhr.responseText);
                                if (xhr.responseText == "success") {
                                    $('.modal-container').removeClass('show-modal');
                                    $('body').removeClass('fixed-body');
                                    showNotification("alert-success", "Bericht verzonden !", "bottom", "center", "", "");
                                } else {
                                    $('.modal-container').removeClass('show-modal');
                                    $('body').removeClass('fixed-body');
                                    showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                                }
                            }
                        }); // ends ajax
                    } else if (msg.length < 1) {
                        $('.modal-container').removeClass('show-modal');
                        $('body').removeClass('fixed-body');
                        showNotification("alert-danger", "Vul wat in om een berichtje te sturen!", "bottom", "center", "", "");
                    }
                }); // () chatsender ends



                $("#delete-yes-type").click(function() {
                    cancelSlott(_obj, _id);
                    $("#modal-delete-type").modal('hide');
                });

                $("#delete-no-type").click(function() {
                    $("#modal-delete-type").modal('hide');
                    _obj = null;
                    _id = null;
                    _name = null;

                    _ddate = null;
                    _stime = null;
                    _etime = null;
                });

                $("#login-yes-type").click(function() {
                    $("#modal-login-type").modal('hide');
                    window.location = "/app/";
                });

                $("#login-no-type").click(function() {
                    $("#modal-login-type").modal('hide');
                    window.location = "/app/students-registration";
                });


            }); //main $()


            function removeButton(obj) {
                $(obj).find(".inline-elem-buttons-box").remove();
            }


            //   function addButton(obj, id){
            //     var cases = $(obj).attr("data-event");
            //     var buttonText = (cases == "event-info")? "BIJLES ANNULEREN" : "VERZOEK ANNULEREN";
            // 	var button = '<span class="inline-elem-buttons-box">'
            // 				   +'<button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect"'
            // 				   +'value="cancel" onclick="cancelSlot(this, '+id+')">'
            // 				   + buttonText
            // 				   +'</button></span>';

            //      $(obj).append(button);

            //   }// addButton function


            function cancelSlot(obj, id) {

                $('#modal-delete-type').appendTo("body").modal('show');
                _obj = obj;
                _id = id;
            }

            function cancelSlott(obj, id) {

                $.ajax({
                    type: "POST",
                    url: "student-ajaxcall.php",
                    data: {
                        subpage: "slotCancellation",
                        cbID: id
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response == "success") {
                            console.log(response);
                            showNotification("alert-success", "Verzoek is geannuleerd", "bottom", "center", "", "");
                            calendar = $('#calendar').calendar(options);
                        } else {
                            showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                        }
                        $(document.body).css({
                            'cursor': 'default'
                        });
                        $("#submitBtn").prop("disabled", false);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        if (xhr.responseText == "success") {
                            console.log(xhr.responseText);
                            showNotification("alert-success", "Verzoek is geannuleerd", "bottom", "center", "", "");
                            calendar = $('#calendar').calendar(options);
                        } else {
                            showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                        }
                        $(document.body).css({
                            'cursor': 'default'
                        });
                        $("#submitBtn").prop("disabled", false);

                    }
                }); // ends ajax
            }

            function bookTutor() {

                if (<?php echo (isset($_SESSION['Student']) || isset($_SESSION['Teacher'])) ? "false" : "true"; ?>) {
                    var modal = document.getElementById("login-register-modal");
                    modal.style.display = "block";
                    $(".keuze1").fadeIn();
                    $(".modal-content").addClass('modal--no-scroll');
                    $('.registreren-modal-btn').on('click', function() {
                        $(".modal-content").removeClass('modal--no-scroll');
                    });
                    $(".modal-inloggen").hide();
                    $(".registeren").hide();
                    // Resize recaptcha
                    $('.g-recaptcha').css('transform-origin', '0 0');
                    if ($('#login-email').outerWidth() <= 304) {
                        $('.g-recaptcha').css({
                            transform: 'scale(' + ($('#login-email').outerWidth() / $('.g-recaptcha').width()) + ')',
                            paddingLeft: '8px'
                        });
                    } else {
                        $('.g-recaptcha').css({
                            transform: 'scale(1.0)',
                            paddingLeft: '0px'
                        });
                    }

                    return;
                }

                var stime = $("#calender_startTime").val();
                var duration = $("#calender_endTime").val();
                var date = $("#calender_startTime").find(':selected').attr("data-date");
                var url = "/api/Calendar/BookTutor";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {
                        studentID: studentid,
                        teacherID: teacherid,
                        date: date,
                        time: stime,
                        duration: duration
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response.firstBook === true && bookingCount < 1) {
                            $('#student-first-book').appendTo("body").modal('show');
                            bookingCount++;
                        }
                        trackEvent('Book Tutor', 'Click', 'Appointment' + response.level, response.rate, response.duration);
                        showNotification("alert-success", "Het bijlesverzoek is verzonden!", "bottom", "center", "", "");
                        calendar = $('#calendar').calendar(options);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                    }
                }); // ends ajax

            }

            function booking() {

                // Show register modal if user is not logged in
                if (<?php echo (isset($_SESSION['Student']) || isset($_SESSION['Teacher'])) ? "false" : "true"; ?>) {
                    var modal = document.getElementById("login-register-modal");
                    modal.style.display = "block";
                    $(".keuze1").fadeIn();
                    $(".modal-content").addClass('modal--no-scroll');
                    console.log('add class to: ');
                    console.log($(".modal-content"));
                    $('.registreren-modal-btn').on('click', function() {
                        console.log('here');
                        $(".modal-content").removeClass('modal--no-scroll');
                    });
                    $(".modal-inloggen").hide();
                    $(".registeren").hide();
                    // Resize recaptcha
                    $('.g-recaptcha').css('transform-origin', '0 0');
                    if ($('#login-email').outerWidth() <= 304) {
                        $('.g-recaptcha').css({
                            transform: 'scale(' + ($('#login-email').outerWidth() / $('.g-recaptcha').width()) + ')',
                            paddingLeft: '8px'
                        });
                    } else {
                        $('.g-recaptcha').css({
                            transform: 'scale(1.0)',
                            paddingLeft: '0px'
                        });
                    }

                    return;
                }

                // check if user is logged in as student, otherwise end this booking function 
                if (<?php echo (isset($_SESSION['Student']) ? "false" : "true"); ?>) {
                    showNotification("alert-danger", "Registreer je als student om deze functie te gebruiken.", "bottom", "center", "", "");
                    return;
                }

                var stime = $("#calender_startTime").val();
                var etime = $("#calender_endTime").val();

                if (!stime || !etime)
                    return;

                //converting into date, so we can get7 the minutes after subtracting
                var timeStart = new Date("01/01/2007 " + stime);
                var timeEnd = new Date("01/01/2007 " + etime);

                var minutes = 60 * 1000; //sec * millisec

                if (((timeEnd - timeStart) / minutes) < <?php echo MINIMUM_TIME_FOR_APPOINTMENT_SLOT; ?>) {
                    showNotification("alert-danger", "Minimaal 45 minuten opeenvolgende slots kunnen worden geboekt", "bottom", "center", "", "");
                    return;
                }
                var timeArr = Global_Variable_timeRange[__calender_date];

                var slotTime = Array();
                var slotDetail = Array();
                var milliseconds = window.performance.now();
                var isTimeStart = false;
                var isTimeEnd = false;
                var prevTime = null;
                var strTime = null;
                var tempTime = null;

                for (var time in timeArr) {
                    //adding slot_time to get end_time 
                    tempTime = new Date("01/01/2007 " + time);
                    tempTime = new Date(tempTime.getTime() + <?php echo SLOT_TIME_IN_MINIUTES * 60 * 1000; //millisec
                                                                ?>);
                    tempTime = ((tempTime.getHours() > 9) ? tempTime.getHours() : "0" + tempTime.getHours()) + ":" + ((tempTime.getMinutes() > 9) ? tempTime.getMinutes() : "0" + tempTime.getMinutes());

                    strTime = time + "-" + tempTime;
                    //adding slot_time to get end_time 

                    /* if(isTimeEnd){
			  if(timeArr[time]== "available")
		  	  	slotTime.push({isGapSlot:1,  time:strTime , date:__calender_date});
			  break;
		  }
		  
		  if(time == etime){
			  slotTime.push({isGapSlot:0,  time:strTime , date:__calender_date});
			  isTimeEnd = true;
			  continue;
		  }
		  */

                    if (time == etime) {
                        if (timeArr[time] == "available")
                            slotTime.push({
                                isGapSlot: 1,
                                time: strTime,
                                date: __calender_date
                            });
                        break;
                    }

                    if (isTimeStart && timeArr[time] == "unavailable") {
                        showNotification("alert-danger", "Deze combinatie van begin- en eindtijd is niet mogelijk, omdat door een andere leerling een bijles is geboekt tussen deze twee tijden. Vraag ons gerust om hulp.", "bottom", "center", "", "");
                        return;
                    }

                    if (isTimeStart && timeArr[time] == "available") {
                        slotTime.push({
                            isGapSlot: 0,
                            time: strTime,
                            date: __calender_date
                        });
                        continue;
                    }

                    if (time == stime) {
                        if (prevTime) {
                            slotTime.push({
                                isGapSlot: 1,
                                time: prevTime + "-" + time,
                                date: __calender_date
                            });
                        }
                        slotTime.push({
                            isGapSlot: 0,
                            time: strTime,
                            date: __calender_date
                        });
                        isTimeStart = true;
                        continue;
                    }

                    if (!isTimeStart && timeArr[time] == "available")
                        prevTime = time;
                    else if (timeArr[time] == "unavailable")
                        prevTime = null;

                } //ends for()

                slotDetail.push({
                    slotGID: (teacherid + milliseconds),
                    slotDetail: slotTime
                });

                $.ajax({
                    type: "POST",
                    url: "student-ajaxcall.php",
                    data: {
                        subpage: "slotBooking",
                        teacherid: teacherid,
                        slotData: slotDetail,
                        hourlyCost: hourlyCost,
                        std_lat: std_lat,
                        std_lng: std_lng,
                        place_name: place_name,
                        travelCost: travelCost,
                        travelDistance: travelDistance
                    },
                    dataType: "json",
                    success: function(response) {
                        if (response == "success") {
                            console.log(response);
                            showNotification("alert-success", "Het bijlesverzoek is verzonden!", "bottom", "center", "", "");
                            calendar = $('#calendar').calendar(options);
                        } else {
                            if (response == "Maximum One Booking can be made.")
                                showNotification("alert-danger", "Maximaal één boeking kan worden gemaakt.", "bottom", "center", "", "");
                            else if (response == "100hours Maximum time for classes is already occupied.")
                                showNotification("alert-danger", "100 uur Maximale tijd voor lessen is al bezet.", "bottom", "center", "", "");
                            else
                                showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                        }
                        $(document.body).css({
                            'cursor': 'default'
                        });
                        $("#submitBtn").prop("disabled", false);
                    },
                    error: function(xhr, ajaxOptions, thrownError) {
                        if (xhr.responseText == "success") {
                            console.log(xhr.responseText);
                            showNotification("alert-success", "Verzoek is verzonden", "bottom", "center", "", "");
                            calendar = $('#calendar').calendar(options);
                        } else {
                            if (xhr.responseText == "Maximum One Booking can be made.")
                                showNotification("alert-danger", "Maximaal één boeking kan worden gemaakt.", "bottom", "center", "", "");
                            else if (xhr.responseText == "100hours Maximum time for classes is already occupied.")
                                showNotification("alert-danger", "100 uur Maximale tijd voor lessen is al bezet.", "bottom", "center", "", "");
                            else
                                showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                        }
                        $(document.body).css({
                            'cursor': 'default'
                        });
                        $("#submitBtn").prop("disabled", false);
                    } // func ends 
                }); // ends ajax
            }

            //dummy function

            function __onclick(obj) {
                return 1;
            }
        </script>
        <script src="https://unpkg.com/popper.js@1"></script>
        <script src="https://unpkg.com/tippy.js@4"></script>
        <script>
            $(document).ready(function() {
                $('#open-message-modal').click(() => {
                    if (!$('.open-message-modal').hasClass('PopupBtn')) {
                        $('#modal-container').toggleClass('show-modal');
                        $('body').toggleClass('fixed-body');
                    }
                });

                $('.modal-container').click((event) => {
                    if (event.target == $('#modal-container')[0] || event.target == $('#modal-container').find('i')[0]) {
                        $('#modal-container').toggleClass('show-modal');
                        $('body').toggleClass('fixed-body');
                    }
                });


                $(".pull-right").each(function() {
                    var calender_date = $(this).attr("data-cal-date");
                    if (new Date() > new Date(calender_date)) {
                        $(this).parent().find(".events-list").remove();
                    }
                });

                // tutor unavailable overlay
                let unavailableOverlay = $('.unavailable-overlay');
                if (unavailableOverlay) {
                    console.log($('.unavailable-overlay'));
                    //unavailableOverlay.detach().appendTo('#calendar');
                    $('.chat-focus').on('click', function() {
                        console.log($('.send-message-box'));
                        $('.send-message-box').css({
                            border: '2px solid #f1f1f1'
                        }).animate({
                            borderColor: '#486066'
                        }, 1500).animate({
                            borderColor: '#f1f1f1'
                        }, 1500);
                    });
                }
            });




            $(document).ready(function() {
                tippy('.tippy', {
                    placement: 'bottom-start',
                    animation: 'fade',
                    theme: 'light',
                    interactive: true,
                    interactiveBorder: 20
                })

            });

            $(document).ready(function() {
                $('[data-toggle="tooltip"]').tooltip({});
            });
        </script>
    </body>

    </html>

    <div class="modal modal-fullscreen fade" id="modal-delete-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h6 class="modal-title" id="myModalLabel">Weet je zeker dat je het verzoek wilt annuleren?</h6>
                </div>
                <div class="modal-body">
                    <div id="deresulttype"></div>
                    <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                        <input type="hidden" id="delete-id-type" name="delete-id-type" />
                        <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType" />
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">
                                <button type="button" class="btn btn-success" id="delete-yes-type">Ja</button>
                                <button type="button" class="btn btn-danger" id="delete-no-type">Nee</button>
                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

    <div class="modal modal-dialog-centered fade" id="student-first-book" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal__content">
            <div class="modal__header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h6 class="modal__title" id="myModalLabel2">Gefeliciteerd, <span class="modal__title--name"><?php echo $s['firstname']; ?></span></h6>
            </div>
            <div class="modal__body">
                <p class="modal__text--first-book">Leuk dat je je eerste bijlesverzoek hebt ingediend! <?= $firstName ?> reageert doorgaans binnen 24 uur op je verzoek en zodra de docent bevestigd heeft, krijg je hierover bericht via de mail. Meer informatie over de manier van betalen vind je overigens <a href="/veelgestelde-vragen/" class="modal__text--link">hier</a>. Mocht je een vraag hebben, dan kan je altijd persoonlijk contact met ons opnemen!</p>
            </div>
        </div>
    </div>

<?php } ?>

<script type="application/javascript">
    $(document).ready(function() {
        <?php
        //$_SESSION['popup_msg'] = true;

        if (isset($_SESSION['popup_msg'])) {
        ?>
            showNotification("alert-success", "Je account is geactiveerd", "bottom", "center", "", "");
        <?php
            unset($_SESSION['popup_msg']);
        }
        ?>
    });
</script>