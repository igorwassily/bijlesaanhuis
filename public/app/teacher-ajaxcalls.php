<?php
// error_reporting(E_ALL);
// ini_set('display_errors',1);
// if(!isset($_SESSION['Teacher']))
//     header('Location: index.php');

//use API\Email;

require_once('includes/connection.php');
require_once '../../inc/Email.php';
session_start();

if (isset($_POST['subpage'])) {
	if ($_POST['subpage'] == "slotCreation") {
		if ($_POST['mon_stime'] != "" && $_POST['mon_etime'] != "") {
			$mon_time = $_POST['mon_stime'] . "-" . $_POST['mon_etime'];
		} else
			$mon_time = "";

		if ($_POST['tue_stime'] != "" && $_POST['tue_etime'] != "") {
			$tue_time = $_POST['tue_stime'] . "-" . $_POST['tue_etime'];
		} else
			$tue_time = "";

		if ($_POST['wed_stime'] != "" && $_POST['wed_etime'] != "") {
			$wed_time = $_POST['wed_stime'] . "-" . $_POST['wed_etime'];
		} else
			$wed_time = "";

		if ($_POST['thur_stime'] != "" && $_POST['thur_etime'] != "") {
			$thur_time = $_POST['thur_stime'] . "-" . $_POST['thur_etime'];
		} else
			$thur_time = "";

		if ($_POST['fri_stime'] != "" && $_POST['fri_etime'] != "") {
			$fri_time = $_POST['fri_stime'] . "-" . $_POST['fri_etime'];
		} else
			$fri_time = "";

		if ($_POST['sat_stime'] != "" && $_POST['sat_etime'] != "") {
			$sat_time = $_POST['sat_stime'] . "-" . $_POST['sat_etime'];
		} else
			$sat_time = "";

		if ($_POST['sun_stime'] != "" && $_POST['sun_etime'] != "") {
			$sun_time = $_POST['sun_stime'] . "-" . $_POST['sun_etime'];
		} else
			$sun_time = "";

		/* ADDITIONAL WEEKDAYS */

		if (isset($_POST['mon_add']) && !empty($_POST['mon_add'])) {
			$mon_add = $_POST['mon_add'];
		} else {
			$mon_add = "";
		}

		if (isset($_POST['tue_add']) && !empty($_POST['tue_add'])) {
			$tue_add = $_POST['tue_add'];
		} else {
			$tue_add = "";
		}

		if (isset($_POST['wed_add']) && !empty($_POST['wed_add'])) {
			$wed_add = $_POST['wed_add'];
		} else {
			$wed_add = "";
		}

		if (isset($_POST['thu_add']) && !empty($_POST['thu_add'])) {
			$thu_add = $_POST['thu_add'];
		} else {
			$thu_add = "";
		}

		if (isset($_POST['fri_add']) && !empty($_POST['fri_add'])) {
			$fri_add = $_POST['fri_add'];
		} else {
			$fri_add = "";
		}

		if (isset($_POST['sat_add']) && !empty($_POST['sat_add'])) {
			$sat_add = $_POST['sat_add'];
		} else {
			$sat_add = "";
		}

		if (isset($_POST['sun_add']) && !empty($_POST['sun_add'])) {
			$sun_add = $_POST['sun_add'];
		} else {
			$sun_add = "";
		}


		$wkday = $_POST['wkday'];
		$months = $_POST['month'];
		$date = $_POST['date'];


		// return;

		//to add months in current date
		$end_date = date('Y-m-d', strtotime("+$months months", strtotime($date)));

		// $months = intval($_POST['month'])*30;

		// $dates = createRange($months);

		// $time = timeDiff($stime, $etime);

		$q1 = "SELECT COUNT(slotID)total FROM teacherslots WHERE teacherID=" . $_SESSION['TeacherID'];
		$r1 = mysqli_query($con, $q1);

		$d = mysqli_fetch_assoc($r1);

		if ($d['total'] == 0) {
			$q = "INSERT INTO  teacherslots (teacherID, datee, `mon_time`,`tue_time`,`wed_time`,`thur_time`,`fri_time`,`sat_time`,`sun_time`, noofmonths, mon_additional, tue_additional, wed_additional, thur_additional, fri_additional, sat_additional, sun_additional ) VALUES (" . $_SESSION['TeacherID'] . ", '" . $date . "', '" . $mon_time . "', '" . $tue_time . "', '" . $wed_time . "', '" . $thur_time . "', '" . $fri_time . "', '" . $sat_time . "', '" . $sun_time . "', '" . $months . "', '" . $mon_add . "', '" . $tue_add . "', '" . $wed_add . "', '" . $thu_add . "', '" . $fri_add . "', '" . $sat_add . "', '" . $sun_add . "');";
			//echo $q;

			$stmt = mysqli_query($con, $q);

			if ($stmt)
				echo "success";
			else
				echo "nothing";

		} else {
			$q = "UPDATE `teacherslots` SET mon_time='" . $mon_time . "', tue_time='" . $tue_time . "', wed_time='" . $wed_time . "', thur_time='" . $thur_time . "', fri_time='" . $fri_time . "', sat_time='" . $sat_time . "', sun_time='" . $sun_time . "' , noofmonths='" . $months . "', mon_additional='" . $mon_add . "', tue_additional='" . $tue_add . "', wed_additional='" . $wed_add . "', thur_additional='" . $thu_add . "', fri_additional='" . $fri_add . "', sat_additional='" . $sat_add . "', sun_additional='" . $sun_add . "' WHERE teacherID=" . $_SESSION['TeacherID'];
			//echo $q;

			$stmt = mysqli_query($con, $q);

			if ($stmt)
				echo "update";
			else
				echo "nothing";


		}

	}// ends if "slotCreation"
	else if ($_POST['subpage'] == "cancelBook") {

		$_q = "SELECT cb.accepted,cb.teacherID, c.email, c.telephone, c.place_name, c.firstname, c.lastname, cb.datee, MIN(cb.starttime)starttime, MAX(cb.endtime)endtime, cb.teacherID, (SELECT CONCAT(firstname, '**', email)tdata FROM contact WHERE userID=cb.teacherID)tname, (SELECT CONCAT(firstname,' ', lastname) FROM contact cc WHERE cc.userID=cb.studentID AND cc.contacttypeID=3 LIMIT 1 )pname, p.image timage, cb.travel_cost , tlr.level FROM calendarbooking cb , contact c, `profile` p, teacher t, teacherlevel_rate tlr WHERE slot_group_id='$_POST[cbID]' AND cb.isgapslot=0 AND c.userID=cb.studentID AND p.profileID=t.profileID AND t.userID=cb.teacherID AND tlr.ID=t.teacherlevel_rate_ID AND c.contacttypeID IN (1,2) GROUP BY slot_group_id";

		$_r = mysqli_query($con, $_q);
		$_f = mysqli_fetch_assoc($_r);

		$query = "DELETE FROM `calendarbooking` WHERE `calendarbooking`.`slot_group_id`=" . $_POST['cbID'];
		$stmt = $con->prepare($query);

		if ($stmt->execute()) {
			$pname = explode(" ", $_f['pname']);
			$sname = $_f['firstname'] . " / " . $pname[0];
			$tname = $_f['tname'];
			$semail = $_f['email'];
			$dateTime = $_f['datee'] . ", " . $_f['starttime'] . " - " . $_f['endtime'];
			$tdata = explode("**", $_f['tname']);
			$temail = $tdata[1];
			$addres = explode(",", $_f['place_name']);
			$city = explode(" ", $addres[1]);
			$city[0] = "";
			$city = implode(" ", $city);
			$path = $_f['timage'];


			if (isset($_SESSION['TeacherID'])) {
				if ($_f['accepted'] == 1) {
					//for student
					cancelled_tutoring_session_by_tutor($semail, $sname, $dateTime, $tdata[0], $_f['teacherID'], $_f['timage']);
					email_cancelled_tutoring_session_by_tutor_for_tutor($temail, $tdata[0], $dateTime, $_f['firstname'] . " " . $_f['lastname'], $_f['pname'], $_f['place_name'], $semail, $_f['telephone']);
				} else {
					email_cancelled_request_by_tutor_for_student($semail, $sname, $tdata[0], $path, $dateTime, $_f['teacherID']);
				}

			}
			if (isset($_SESSION['StudentID'])) {
				if ($_f['accepted'] == 1) {
					email_cancelled_tutoring_session_by_student_for_student($semail, $sname, $tdata[0], $path, $dateTime, $_f['teacherID']);
					email_cancelled_tutoring_request_by_customer_for_tutor($temail, $tdata[0], $dateTime, $sname, $_f['pname'], $addres, $city, $semail, $sphone);
				} else {
					email_cancelled_tutoring_request_by_customer_for_tutor($temail, $tdata[0], $dateTime, $sname, $_f['pname'], $address, $city, $semail, $sphone);
					email_cancelled_tutoring_request_by_student_for_student($semail, $sname, $path, $tdata[0], $dateTime, $_f['teacherID']);
				}
			}

			echo "success";
		} else {
			echo "fail";
		}
	}
	else if ($_POST['subpage'] == "cancelPending") {
		$_q = "SELECT p.image, cb.accepted,cb.teacherID, c.email, c.telephone, c.place_name, c.firstname, c.lastname, cb.datee, MIN(cb.starttime)starttime, MAX(cb.endtime)endtime, cb.teacherID, (SELECT CONCAT(firstname, '**', email)tdata FROM contact WHERE userID=cb.teacherID)tname, (SELECT CONCAT(firstname,' ', lastname) FROM contact cc WHERE cc.userID=cb.studentID AND cc.contacttypeID=3 LIMIT 1 )pname, p.image timage, cb.travel_cost , tlr.level FROM calendarbooking cb , contact c, `profile` p, teacher t, teacherlevel_rate tlr WHERE slot_group_id='$_POST[cbID]' AND cb.isgapslot=0 AND c.userID=cb.studentID AND p.profileID=t.profileID AND t.userID=cb.teacherID AND tlr.ID=t.teacherlevel_rate_ID AND c.contacttypeID IN (1,2) GROUP BY slot_group_id";

		$_r = mysqli_query($con, $_q);
		$_f = mysqli_fetch_assoc($_r);

		$query = "DELETE FROM `calendarbooking` WHERE `calendarbooking`.`slot_group_id`=" . $_POST['cbID'];
		$stmt = $con->prepare($query);

		if ($stmt->execute()) {

			$pname = explode(" ", $_f['pname']);
			$sname = $_f['firstname'] . " / " . $pname[0];
			$semail = $_f['email'];
			$dateTime = $_f['datee'] . ", " . $_f['starttime'] . " - " . $_f['endtime'];
			$tdata = explode("**", $_f['tname']);
			$temail = $tdata[1];

			$path = $_f['image'];

			$addres = explode(",", $_f['place_name']);
			$city = explode(" ", $addres[1]);
			$city[0] = "";
			$city = implode(" ", $city);

			if (isset($_SESSION['TeacherID'])) {
				if ($_f['accepted'] == 1) {
					//for student
					cancelled_tutoring_session_by_tutor($semail, $sname, $dateTime, $tdata[0], $_f['teacherID'], $_f['image']);
					email_cancelled_tutoring_session_by_tutor_for_tutor($temail, $tdata[0], $dateTime, $_f['firstname'] . " " . $_f['lastname'], $_f['pname'], $_f['place_name'], $semail, $_f['telephone']);


				} else {

					email_cancelled_request_by_tutor_for_student($semail, $sname, $tdata[0], $_f['image'], $dateTime, $_f['teacherID']);
					refused_tutoring_request_confirmation_for_tutor($temail, $tdata[0], $dateTime, $_f['firstname'] . " " . $_f['lastname'], $_f['pname'], $city, $_f['email'], $_f['telephone'], $_f['place_name']);
				}

			} else if (isset($_SESSION['StudentID'])) {
				if ($_f['accepted'] == 1) {
					cancelled_tutoring_session_by_customer($temail, $tdata[0], $dateTime, $_f['firstname'] . " " . $_f['lastname'], $_f['pname'], $_f['place_name'], $city, $semail, $_f['telephone']);
					email_cancelled_tutoring_session_by_student_for_student($semail, $sname, $tdata[0], $path, $dateTime, $_f['teacherID']);

				} else {
					email_cancelled_tutoring_request_by_customer_for_tutor($temail, $tdata[0], $dateTime, $_f['firstname'], $pname[0], $_f['place_name'], $city, $semail, $_f['telephone']);
					email_cancelled_tutoring_request_by_student_for_student($semail, $sname, $path, $tdata[0], $dateTime, $_f['teacherID']);
				}
			}

			echo "success";
		} else
			echo "fail";

	}
	else if ($_POST['subpage'] == "cancelfreeSlot") {

		$slotGID = time() . "" . mt_rand(100, 1000000);
		$teacherid = $_POST['teacherid'];
		$slotData = $_POST['slotData'];

		$insertion_query = "INSERT INTO calendarbooking (`teacherID`, `datee`, `starttime`, `endtime`, 	`slot_group_id`, `isSlotCancelled`) VALUES ";
		foreach ($slotData as $data) {

			foreach ($data['slotDetail'] as $d) {
				$t = explode("-", $d['time']);
				$endtime = trim($t[1]);
				$starttime = trim($t[0]);

				$insertion_query .= " (" . $teacherid . ", '" . trim($d['date']) . "', '" . $starttime . "', '" . $endtime . "', '" . $slotGID . "', 1),";
			}//inner foreach ends

		}//outer foreach ends
		$insertion_query = rtrim($insertion_query, ",");
		// $insertion_query;

		$stmt = $con->prepare($insertion_query);

		if ($stmt->execute())
			echo "success";
		else
			echo "fail";

	}
	else if ($_POST['subpage'] == "reactivatefreeSlot") {
		$query = "DELETE FROM calendarbooking WHERE slot_group_id=" . $_POST['cbID'];
		$stmt = $con->prepare($query);

		if ($stmt->execute())
			echo "success";
		else
			echo "fail";

	}
	else if ($_POST['subpage'] == "confrimPending") {
		// accepted=1, deleted=9
		$query = "UPDATE `calendarbooking` SET `accepted` = '1' WHERE `calendarbooking`.`slot_group_id`=" . $_POST['cbID'];
		$stmt = $con->prepare($query);




			if ($stmt->execute()) {
			$_q = "SELECT cb.travel_cost, cb.external_teacher_level, c.email, c.telephone, c.place_name, c.firstname, c.lastname, cb.datee, cb.starttime, cb.endtime, cb.teacherID, (SELECT CONCAT(firstname, '**', email)tdata FROM contact WHERE userID=cb.teacherID)tname, (SELECT CONCAT(firstname,' ', lastname) FROM contact cc WHERE cc.userID=cb.studentID AND cc.contacttypeID=3 LIMIT 1 )pname, (SELECT p.image FROM `profile` p, teacher t WHERE t.userID=cb.teacherID AND t.profileID=p.profileID)pic_path FROM `calendarbooking`cb , contact c WHERE slot_group_id='$_POST[cbID]' AND c.userID=cb.studentID AND c.contacttypeID IN (1,2) LIMIT 1";
			$_r = mysqli_query($con, $_q);
			$_f = mysqli_fetch_assoc($_r);

			$pname = explode(" ", $_f['pname']);
			$sname = $_f['firstname'] . " / " . $pname[0];
			$semail = $_f['email'];
			$dateTime = $_f['datee'] . ", " . $_f['starttime'] . " - " . $_f['endtime'];
			$tdata = explode("**", $_f['tname']);
			$email = $tdata[1];

			/* FOR STUDENT TO HAVE MESSAGE AS WELL */
			$q2 = "SELECT * FROM `calendarbooking` WHERE `calendarbooking`.`slot_group_id`=" . $_POST['cbID'];

			$res = mysqli_query($con, $q2);
			$s = mysqli_fetch_assoc($res);

				$receiverID = $s['studentID'];              // receiver => studentID
				$senderID  = $s['teacherID'];               /// senderID => teacherID           equals in student has ID in profile -> but teacher not!




			$message = "";
			$subject = "null";

			date_default_timezone_set('Europe/Amsterdam');
			$d1 = new DateTime();
			$query = "INSERT INTO `email` (`emailID`, `senderID`, `receiverID`, `subject`, `message`, `date_time`) VALUES (NULL, ".$senderID.", ".$receiverID.", '".$subject."', '".$message."',  '".$d1->format('Y-m-d H:i:s')."');";
			$stmt = $con->prepare($query);
			$stmt->execute();


				date_default_timezone_set('Europe/Amsterdam');
				$d1 = new DateTime();
				$query = "INSERT INTO `email` (`emailID`, `senderID`, `receiverID`, `subject`, `message`, `date_time`) VALUES (NULL, ".$receiverID.", ".$senderID.", '".$subject."', '".$message."',  '".$d1->format('Y-m-d H:i:s')."');";
				$stmt = $con->prepare($query);
				$stmt->execute();





			$addres = explode(",", $_f['place_name']);
			$city = explode(" ", $addres[1]);
			$city[0] = "";
			$city = implode(" ", $city);

			accepted_tutoring_session_by_tutor($semail, $sname, $tname, $dateTime, $_f['pic_path'], $_f['teacherID']);
			email_accepted_tutoring_request_confirmation_for_tutor($email, $tdata[0], $dateTime, $sname, $_f['pname'], $_f['place_name'], $semail, $_f['telephone'], $city);
			// email_confirm_tutoring_request_for_student($semail, $tdata[0], $sname, $_f['pic_path'], $dateTime, $_f['travel_cost'], $f['external_teacher_level']);



			echo "success";
		} else
			echo "fail";

	}
	else if ($_POST['subpage'] == "reloadDiv") {
		$query = "SELECT  MIN(cb.starttime)starttime, MAX(cb.endtime)endtime, c.userID, c.firstname, c.lastname, telephone, email, c.address, cb.slot_group_id, cb.teacherID, cb.datee, cb.accepted, cb.place_name,cb.calendarbookingID FROM contact c RIGHT JOIN calendarbooking cb ON cb.studentID=c.userID AND contacttypeID=2  where datee >= CURDATE() AND cb.studentID is not null AND cb.teacherID=$_REQUEST[id] AND cb.isSlotCancelled=0 AND isgapslot=0 AND cb.isClassTaken=0 AND (cb.accepted=0 OR cb.accepted=1) GROUP BY slot_group_id ORDER BY cb.datee, cb.starttime,cb.accepted ASC LIMIT 4";

		$result = mysqli_query($con, $query);
		$isEmpty = true;
		$i = 0;
		$limit = 3;


		while ($row = mysqli_fetch_assoc($result)) {

			$i++;
			if ($i > $limit) {
				echo "<hr/> <p style='text-align:right;'> <a href='teachers-appointment'>Toon alle</a></p>";
				break;
			}

			$color = ($row['accepted'] == 1) ? "b-primary" : "b-warning";
			$status = ($row['accepted'] == 1) ? "Geaccepteerd" : "In afwachting";
			$accept_delete_button = ($row['accepted'] == 1) ? '<button style="color:white !important; background:#ffbd6b" class="btn m-b-10 w-100 btn-primary-chat" id="" name="" onclick="reject(' . $row['slot_group_id'] . ', 1)">AFSPRAAK ANNULEREN</button>'
				: '<button style="color:white !important; background:#5CC75F" class="btn m-b-10 w-100 btn-primary-chat" id="" name="" onclick="accept(' . $row['slot_group_id'] . ')">VERZOEK ACCEPTEREN</button>
											<button style="color:white !important; background:#ffbd6b" class="btn m-b-10 w-100 btn-primary-chat" id="" name="" onclick="reject(' . $row['slot_group_id'] . ',2)">VERZOEK WEIGEREN</button>';

			$day = date("d", strtotime($row['datee']));
			$m = getDutchMonth(date("M", strtotime($row['datee'])));
			$year = date("Y", strtotime($row['datee']));
			$id = explode(".", $row["slot_group_id"]);
			echo '<hr id="h' . $id[0] . '" /><div id="cl' . $id[0] . '" class="event-name ' . $color . ' row">
								
                                        <div class="col-3 text-center">
                                            <h4>' . $day . '<span>' . $m . '</span><span>' . $year . '</span></h4>
                                        </div>
                                        <div class="col-9">
                                            <h6>' . "<a href='student-profile?sid=$row[userID]'>$row[firstname]  $row[lastname]</a>" . '</h6>
                                            <address><i class="zmdi zmdi-time"></i> ' . $row['starttime'] . " - " . $row['endtime'] . '</address>
                                            <address><i class="zmdi zmdi-phone"></i> ' . $row['telephone'] . '</address>
                                            <address><i class="zmdi zmdi-email"></i> ' . $row['email'] . '</address>
                                            <address><i class="zmdi zmdi-pin"></i>' . $row['place_name'] . '</address>
                                            <address>' . $status . '</address>' . $accept_delete_button . '
										
                                        </div>
                                    </div>';
			$isEmpty = false;


		}

		if ($isEmpty) {
			echo '<hr/> <p style="text-align:center;">Nee Toekomstige afspraken</p>';
		}

	}
	else if ($_POST['subpage'] == "classTaken") {
		$query = "UPDATE `calendarbooking` SET `isClassTaken` = '1' WHERE `calendarbooking`.`slot_group_id`=" . $_POST['cbID'];
		$stmt = $con->prepare($query);

		if ($stmt->execute())
			echo "success";
		else
			echo "fail";

	}
	else if ($_POST['subpage'] == "teacher_pendingList") {
		$query = "SELECT ts.starttime AS stime, ts.endtime AS etime, ts.datee AS daatee, ts.slotID, cb.calendarbookingID AS cbid, cb.accepted, (SELECT CONCAT(c.firstname,\")*(\" ,c.lastname, \")*(\" , c.address) FROM contact c WHERE cb.studentID=c.userID AND contacttypeID=2 ORDER BY `daatee` DESC LIMIT 1)std FROM teacherslots ts LEFT JOIN calendarbooking cb ON cb.slotID=ts.slotID where cb.accepted<>1 AND ts.teacherID=" . $_SESSION['TeacherID'];

		$result = mysqli_query($con, $query);

		$str = "";
		while ($row = mysqli_fetch_assoc($result)) {
			$std = explode(")*(", $row['std']);

			$str .= "<td></td>
						<td>$std[0] $std[1]</td>
						<td>$row[daatee] $row[stime] - $row[etime]</td>
						<td>$std[2]</td>
						<td>
							<button type=\"button\" class=\"inline-elem-button btn btn-raised btn-primary btn-round waves-effect\" value=\"confirm\" onclick=\"confirmSlot(this, " . $row['cbid'] . ")\" >Confirm</button>
							<button type=\"button\" class=\"inline-elem-button btn btn-raised btn-primary btn-round waves-effect\" value=\"cancel\" onclick=\"cancelSlot(this, " . $row['cbid'] . ")\" >Cancel</button>
						</td>
						";
		}

		echo $str;

	}//ends if post teacher_pendingList
	elseif ($_POST['subpage'] == "teacherlevel_requested") {
		if ($_POST['levelID'] == "none")
			$level = "permission=NULL, requested_teacherlevel_rate_ID=NULL";
		else {
			if (isset($_SESSION['AdminID']))
				$level = "permission=NULL, teacherlevel_rate_ID='{$_POST['levelID']}'";
			else
				$level = "permission='Requested', requested_teacherlevel_rate_ID='{$_POST['levelID']}'";
		}
		$teacherid = $_POST['tid'];

		$query = "UPDATE teacher SET  $level WHERE userID=" . $teacherid;

		$result = mysqli_query($con, $query);
		if ($result) {
			if (!isset($_SESSION['AdminID'])) {
				$email = new Email($db, 'confirmation_request_tutor_type_change_for_tutor');
				$email->Prepare($teacherid);
				$email->Send('teacher');
//				$_q = "SELECT firstname, email FROM contact WHERE userID=" . $teacherid;
//				$_r = mysqli_query($con, $_q);
//				$_f = mysqli_fetch_assoc($_r);
//				confirmation_request_tutor_type_change_for_tutor($_f['email'], $_f['firstname']);
			}
			echo "success";
		} else
			echo "failed";

	} //teacherlevel_requested ends
	elseif ($_POST['subpage'] == "teachercourse_request") {

		$teacherID = $_POST['tid'];
		$type = $_POST['type'];
		$course = $_POST['course'];
		$level = @$_POST['level'];
		$year = @$_POST['year'];
		$levels = [];
		$permission = isset($_SESSION['AdminID']) ? "Accepted" : "Requested";

		if ($type == ELEMENTRY_TYPE_ID) {
			$levels = [0];
		} else if (is_array($level) && sizeof($level) > 0) {
			foreach ($level as $value) {
				$tmp = explode("_", $value);
				if (sizeof($tmp) < 2) $tmp[1] = "";
				$levels[$tmp[0]][$tmp[1]] = $tmp[1];
			}
		} else if (is_numeric($level)) {
			$levels[$level] = empty($year) ? "" : $year;
		} else {
			echo "failed*";
			return;
		}

		$courses = [$course];
		if ($course == 25 || $course == 26) { // Natuurkunde and Scheikunde implies Nask
			$courses[] = 32;
		}

		$success = true;
		foreach ($levels as $l => $years) {
			$level = $l;
			foreach ($courses as $c) {
				if ($c == 32 && sizeof($years) > 0) {
					$allowed = [11,12,13];
					if ($level == 5) $allowed[] = 14;
					foreach ($years as $key => $item) {
						if (!in_array($item, $allowed)) {
							unset($years[$key]);
						}
					}
					if (empty($years)) continue;
				}

				$course = $c;
				$foundYear = null;
				$foundPermission = null;
				$stmtSelect = mysqli_prepare($con, "SELECT schoolyearID, permission FROM teacher_courses WHERE teacherID=? AND courseID=? AND schoollevelID=? AND schooltypeID=?");
				mysqli_stmt_bind_param($stmtSelect, "iiii", $teacherID, $course, $level, $type);
				mysqli_stmt_execute($stmtSelect);
				mysqli_stmt_bind_result($stmtSelect, $foundYear, $foundPermission);
				mysqli_stmt_fetch($stmtSelect);
				if ($foundYear && sizeof($years) > 0) {
					if ($foundYear == implode(",", $years)) continue;

					$tmp = explode(",", $foundYear);
					foreach ($tmp as $item) {
						if (isset($years[$item])) {
							unset($years[$item]);
						}
					}
					if (empty($years)) continue;
				}
				mysqli_stmt_close($stmtSelect);
				$year = implode(",", $years);

				$stmtInsert = mysqli_prepare($con, "INSERT INTO teacher_courses(teacherID,courseID,schoollevelID,schooltypeID,schoolyearID,permission)values(?,?,?,?,?,?)");
				mysqli_stmt_bind_param($stmtInsert, "iiiiss", $teacherID, $course, $level, $type, $year, $permission);
				if (mysqli_stmt_execute($stmtInsert)) {
					if (!isset($_SESSION['AdminID'])) {
//						$_q = "SELECT firstname, email FROM contact WHERE userID=" . $teacherID;
//						$_r = mysqli_query($con, $_q);
//						$_f = mysqli_fetch_assoc($_r);
						$email = new Email($db, 'confirmed_request_course_year_level_change_tutor');
						$email->Prepare($teacherID);
						$email->Send('teacher');
					}
				} else {
					mysqli_stmt_close($stmtInsert);
					$success = false;
					break;
				}
				mysqli_stmt_close($stmtInsert);
			}
		}
//		if ($success) confirmed_request_course_year_level_change_tutor($_f['email'], $_f['firstname']);
		echo $success ? "success*" : "failed*";
	}
	elseif ($_POST['subpage'] == "availability") {

		$availability_date = $_POST['availability_date'];
		$availType = $_POST['availType'];
		$tid = $_POST['teacherid'];

		if($availType == 1){
			if (isset($availability_date) && isset($tid)) {
				$str = "INSERT INTO teacher_absent(teacherID,datee)values($tid, CAST('" . $availability_date . "' AS DATE))";

				$q = mysqli_query($con, $str);
				if ($q) {
					echo " success";
				} else {
					echo " failed";
				}
			}
		}
		if($availType == 0){
			if (isset($availability_date) && isset($tid)) {
				$str = "DELETE FROM `teacher_absent` WHERE `datee` = '".$availability_date."' AND `teacherID` = '".$tid."'";

				$q = mysqli_query($con, $str);
				if ($q) {
					echo " success";
				} else {
					echo " failed";
				}
			}
		}
	}//ends if teachercourse_request
}// ends if "isset" POST

elseif (isset($_GET['dashboard'])) {
	$reservedSlot = array();
	$teacherID = isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : -1;
	header("content-type: text/json");

	$q = "SELECT cb.*, (SELECT CONCAT(MIN(ccb.starttime),\" - \",MAX(ccb.endtime)) FROM calendarbooking ccb WHERE ccb.slot_group_id=cb.slot_group_id AND ccb.isgapslot=0 GROUP BY ccb.slot_group_id )timee,(SELECT CONCAT(c.firstname,\" \" ,c.lastname) FROM contact c WHERE cb.studentID=c.userID AND contacttypeID=2 ORDER BY `datee` DESC LIMIT 1)std FROM calendarbooking cb WHERE cb.teacherID=$teacherID ORDER BY datee, starttime ASC";

	$r = mysqli_query($con, $q);
	while ($d = mysqli_fetch_assoc($r)) {
		$index = $d['datee'] . " " . $d['starttime'] . " " . $d['endtime'];
		$reservedSlot[$index] = $d;
	}

	$query = "SELECT * FROM teacherslots WHERE teacherID=$teacherID LIMIT 1";
	$result = mysqli_query($con, $query);

	$dd = mysqli_fetch_assoc($result);

	//change available hours for the calender to 24/7
	$wholeDay = "00:00-24:00";

	$mon_time = explode("-", $wholeDay);
	$tue_time = explode("-", $wholeDay);
	$wed_time = explode("-", $wholeDay);
	$thur_time = explode("-", $wholeDay);
	$fri_time = explode("-", $wholeDay);
	$sat_time = explode("-", $wholeDay);
	$sun_time = explode("-", $wholeDay);

	$wkdays = getWeekdays($dd['mon_time'], $dd['tue_time'], $dd['wed_time'], $dd['thur_time'], $dd['fri_time'], $dd['sat_time'], $dd['sun_time']);

	$dateRange = createRange($dd['datee'], $dd['noofmonths'], $wkdays);

	$arrTimeDiff = array();
	$arrTimeDiff[1] = timeDiff($mon_time[0], $mon_time[1]); 
	$arrTimeDiff[2] = timeDiff($tue_time[0], $tue_time[1]);
	$arrTimeDiff[3] = timeDiff($wed_time[0], $wed_time[1]);
	$arrTimeDiff[4] = timeDiff($thur_time[0], $thur_time[1]);
	$arrTimeDiff[5] = timeDiff($fri_time[0], $fri_time[1]);
	$arrTimeDiff[6] = timeDiff($sat_time[0], $sat_time[1]);
	$arrTimeDiff[7] = timeDiff($sun_time[0], $sun_time[1]);

	$str = "{
						\"success\": 1,
						\"result\":[";

	$current_date = new DateTime();

	$slotGID = 0;
	$startTime = "";
	$endTime = "";
	$toPrint = false;
	$time = "";
	$timeRange = "";
	//all time range for calender
	$checkBoxTimeArray = array();

	foreach ($dateRange as $day) {
		//temp time range for loop-specific day
		$timeArray = array();

		$tempDay = new DateTime($day);
		$timeDiff = (isset($arrTimeDiff[$tempDay->format("N")]['stime'])) ? $arrTimeDiff[$tempDay->format("N")] : 0;

		// first and last index of array
		$timeRange = $timeDiff['stime'][0] . " - " . end($timeDiff['etime']);

		for ($i = 0; $i < count($timeDiff['stime']); $i++) {
			$index = $day . " " . $timeDiff['stime'][$i] . " " . $timeDiff['etime'][$i];

			// Default time(dropdown) is unavailable, it will be set as available if slot is free.
			$timeArray[$timeDiff['stime'][$i]] = "unavailable";

			if (isset($reservedSlot[$index])) {


				$id = $reservedSlot[$index]["calendarbookingID"];
				$status = $reservedSlot[$index]["isSlotCancelled"];
				$sdate = $day . " " . $timeDiff['stime'][$i];
				$edate = $day . " " . $timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i] . " - " . $timeDiff['etime'][$i];


				if ($reservedSlot[$index]["isgapslot"] == 1) {
					continue;
				}

				if ($reservedSlot[$index]["slot_group_id"] == $slotGID) {
					continue;
				}

				if ($reservedSlot[$index]["slot_group_id"] != $slotGID) {
					$time = $reservedSlot[$index]["timee"];
					$slotGID = $reservedSlot[$index]["slot_group_id"];
					$toPrint = true;
				}


				if ($reservedSlot[$index]["isSlotCancelled"] == 1) {
					$title = "Beschikbaar verwijderd";
					$title_desc = "Beschikbaar verwijderd";
					$class = "event-disabled";
					$onHoverBtn = "(this, $slotGID, 3)";

				} elseif ($reservedSlot[$index]["isClassTaken"] == 1) {
					//$title = "Completed Appointment";
					//$title_desc = "<a href='student-profile?sid=".$reservedSlot[$index]['studentID']."'>".$reservedSlot[$index]['std']."</a>, ".$reservedSlot[$index][place_name].".";
					//$class = "event-special";
					$title = "Bijles is geaccepteerd";
					$title_desc = "<a href='student-profile?sid=" . $reservedSlot[$index]['studentID'] . "'>" . $reservedSlot[$index]['std'] . "</a>, " . $reservedSlot[$index][place_name] . ".";
					$class = "event-info";
					$onHoverBtn = "(this, $slotGID, 1)";
					$firstName = "<a href='student-profile?sid=" . $reservedSlot[$index]['studentID'] . "'>" . $reservedSlot[$index]['std'] . "</a>";
					$place_name = $reservedSlot[$index][place_name];
				} elseif ($reservedSlot[$index]["accepted"] == 1) {
					$title = "Bijles is geaccepteerd";
					$title_desc = "<a href='student-profile?sid=" . $reservedSlot[$index]['studentID'] . "'>" . $reservedSlot[$index]['std'] . "</a>, " . $reservedSlot[$index][place_name] . ".";
					$class = "event-info";
					$onHoverBtn = "(this, $slotGID, 1)";
					$eventType = "Afspraak";
					$firstName = "<a href='student-profile?sid=" . $reservedSlot[$index]['studentID'] . "'>" . $reservedSlot[$index]['std'] . "</a>";
					$place_name = $reservedSlot[$index][place_name];


				} elseif ($reservedSlot[$index]["accepted"] != 1) {
					$title = "In afwachting";
					$title_desc = "<a href='student-profile?sid=" . $reservedSlot[$index]['studentID'] . "'>" . $reservedSlot[$index]['std'] . "</a>, " . $reservedSlot[$index][place_name] . ".";
					$class = "event-warning";
					$onHoverBtn = "(this, $slotGID, 2)";
					$eventType = "Verzoek";
					$firstName = "<a href='student-profile?sid=" . $reservedSlot[$index]['studentID'] . "'>" . $reservedSlot[$index]['std'] . "</a>";
					$place_name = $reservedSlot[$index][place_name];

				}
			} else {


				$timeArray[$timeDiff['stime'][$i]] = "available";
				//continue; //avoid concatinating other attribite in 'str'

				$title = "Beschikbaar";
				$title_desc = "Beschikbaar";
				$class = "event-inverse";
				$onHoverBtn = "(this, '', 2)";

				$id = '';
				$status = 0;
				$sdate = $day . " " . $timeDiff['stime'][$i];
				$edate = $day . " " . $timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i] . " - " . $timeDiff['etime'][$i];
				$onHoverBtn = "(this, 0, 3)";
				$eventType = "Afspraak";

			}

			if ($title == "Beschikbaar" && $day < $current_date->format("Y-m-d")) {
				continue;
			}

			$formattedMonth = getBetween($day, '-', '-');
			$dateObj = DateTime::createFromFormat('!m', $formattedMonth);
			$monthName = $dateObj->format('F'); // March

			$daystring = strtotime($day);
			$dayOnly = date('d', $daystring);

			$streetName = getBetween($title_desc, ',', ',');

			$citychanged = preg_replace('/' . ',' . '/', '#', $title_desc, 1);
			$cityWithNumbers = getBetween($citychanged, ',', ', ');
			$city = preg_replace('/[0-9]+/', ' ', $cityWithNumbers);

			if (isset($place_name)) {
				$streetName = getBetween("/" . $place_name, '/', ',');

			} else {
				$streetName = getBetween($title_desc, ',', ',');

			}

			// check if a event is in the past
			if ($day < $current_date->format("Y-m-d")) {
				$past = 'past';
			} else {
				$past = 'future';
			}

			$q2 = "SELECT * FROM teacher_absent WHERE datee ='" . $day . "' AND teacherID= '" . $_SESSION['TeacherID'] . "'";
			$result = mysqli_query($con, $q2);
			if (mysqli_num_rows($result) == 0) {
				$notAvailable = 0;
			} else {
				$notAvailable = 1;
			}


			$str .= "{
							\"id\": \"$slotGID\",
							\"cbID\": \"$slotGID\",
							\"start\": \"" . strtotime($sdate) . "000\",
							\"end\": \"" . strtotime($edate) . "000\",
							\"time\": \"$time\",
							\"timeRange\": \"$timeRange\", 
							\"date\": \"$day\", 
							\"status\": \"$status\",
							\"class\": \"" . $class . "\",
							\"title\": \"" . $title . "\",
							\"title_desc\": \"" . $title_desc . "\",
							\"onHoverBtn\": \"" . $onHoverBtn . "\",
							\"designation\": \"Teacher\",
							\"day\": \"" . $dayOnly . "\", 
							\"month\": \"" . $monthName . "\",
							\"firstname\": \"" . $firstName . "\", 
							\"eventType\": \"" . $eventType . "\",
							\"streetName\": \"" . $streetName . "\",
							\"past\": \"" . $past . "\", 
							\"notAvailable\": \"" . $notAvailable . "\", 
							\"td\": \"" . $query . "\", 
							\"title_desc\": \"" . $title_desc . "\", 
							\"url\": \"\"
						},";


		}//for loop ends
		//inserting all time range
		$checkBoxTimeArray[$day] = $timeArray;

		$startTime = "";
		$endTime = "";
		$toPrint = false;
	}// for each loop ends

	$checkBoxTimeArray = json_encode($checkBoxTimeArray);

	$str = rtrim($str, ",");
	$str .= "], \"timeRange\": $checkBoxTimeArray}";

	echo $str;

}//if GET dashboard ends

elseif (isset($_GET['teachers_calendar'])) {

	// if(!isset($_POST['tid']))
	// 	return;


	$userID = (isset($_POST['tid'])) ? $_POST['tid'] : $_SESSION['TeacherID'];

	$reservedSlot = array();

	$q = "SELECT cb.*, (SELECT CONCAT(c.firstname,\")*(\" ,c.lastname, \")*(\" , c.address) FROM contact c WHERE cb.studentID=c.userID AND contacttypeID=2 ORDER BY `datee` DESC LIMIT 1)std FROM calendarbooking cb WHERE cb.teacherID=" . $userID . " ORDER BY datee, starttime DESC";

	// $q = "SELECT * FROM calendarbooking WHERE teacherID=".$_SESSION['TeacherID'];
	$r = mysqli_query($con, $q);
	while ($d = mysqli_fetch_assoc($r)) {
		$index = $d['datee'] . " " . $d['starttime'] . " " . $d['endtime'];
		$reservedSlot[$index] = $d;
	}
// print_r($reservedSlot);
	$query = "SELECT * FROM teacherslots WHERE teacherID=" . $userID . " LIMIT 1";
	$result = mysqli_query($con, $query);

	$dd = mysqli_fetch_assoc($result);

	$dateRange = createRange($dd['datee'], $dd['datee_end'], $dd['dayofweek']);

	$timeDiff = timeDiff($dd['starttime'], $dd['endtime']);

	$str = "{
						\"success\": 1,
						\"result\":[";

	foreach ($dateRange as $day) {
		for ($i = 0; $i < count($timeDiff['stime']); $i++) {
			$index = $day . " " . $timeDiff['stime'][$i] . " " . $timeDiff['etime'][$i];

			if (isset($reservedSlot[$index])) {

				$std = explode(")*(", $reservedSlot[$index]['std']);

				$id = $reservedSlot[$index]["calendarbookingID"];
				$status = $reservedSlot[$index]["isSlotCancelled"];
				$sdate = $day . " " . $timeDiff['stime'][$i];
				$edate = $day . " " . $timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i] . " - " . $timeDiff['etime'][$i];

				if ($reservedSlot[$index]["isClassTaken"] == 1) {
					//$title = "Completed Appointment";
					//$title_desc = "$std[0] $std[1], $std[2].";
					//$class = "event-special";
					$title = "Bijles is geaccepteerd";
					$title_desc = "$std[0] $std[1], $std[2].";
					$class = "event-info";
					$onHoverBtn = "";

				} elseif ($reservedSlot[$index]["isSlotCancelled"] == 1) {
					$title = "Cancelled Slot";
					$title_desc = "Beschikbaar";
					$class = "event-disabled";
					$onHoverBtn = "(this, $id, 3)";

				} elseif ($reservedSlot[$index]["accepted"] == 1) {
					$title = "Bijles is geaccepteerd";
					$title_desc = "$std[0] $std[1], $std[2].";
					$class = "event-info";
					$onHoverBtn = "(this, $id, 0)";
					$eventType = "Afspraak";


				} elseif ($reservedSlot[$index]["accepted"] != 1) {
					$title = "In afwachting";
					$title_desc = "$std[0] $std[1], $std[2].";
					$class = "event-warning";
					$onHoverBtn = "(this, $id, 1)";
					$eventType = "Verzoek";

				}
			} else {
				$title = "Beschikbaar";
				$title_desc = "Beschikbaar";
				$class = "event-inverse";
				$onHoverBtn = "(this, '', 2)";

				$id = '';
				$status = 0;
				$sdate = $day . " " . $timeDiff['stime'][$i];
				$edate = $day . " " . $timeDiff['etime'][$i];
				$time = $timeDiff['stime'][$i] . " - " . $timeDiff['etime'][$i];
				$onHoverBtn = "(this, 0, 2)";
				$eventType = "Afspraak";

			}

			if ($class != "event-inverse")
				$str .= "{
						\"id\": \"$id\",
						\"cbID\": \"$id\",
						\"start\": \"" . strtotime($sdate) . "000\",
						\"end\": \"" . strtotime($edate) . "000\",
						\"time\": \"$time\",
						\"date\": \"$day\",
						\"status\": \"$status\",
						\"class\": \"" . $class . "\",
						\"title\": \"" . $title . "\",
						\"title_desc\": \"" . $title_desc . "\", 
						\"onHoverBtn\": \"" . $onHoverBtn . "\",
						\"eventType\": \"" . $eventType . "\",
						\"designation\": \"Teacher\",
						\"url\": \"\"
						
					},";

		}//for loop ends
	}// for each loop ends

	$str = rtrim($str, ",");
	$str .= "]}";

	echo $str;
}//Get teachers_calendar ends here


function getBetween($content, $start, $end)
{
	$r = explode($start, $content);
	if (isset($r[1])) {
		$r = explode($end, $r[1]);
		return $r[0];
	}
	return '';
}


function timeDiff($t1, $t2)
{
	$time1 = strtotime($t1);
	$time2 = strtotime($t2);
	$diff = intval(($time2 - $time1) / 3600); //from sec to min
	$slot_time = SLOT_TIME_IN_MINIUTES;//45;
	$gap = SLOT_TIME_GAP_IN_MINIUTES + SLOT_TIME_IN_MINIUTES; //15
	$sArray = array();
	$eArray = array();
	$new = $time1;
	$tmp_etime = strtotime("+$slot_time minute", $new);
	do {
		$sArray[] = date("H:i", $new);
		$eArray[] = date("H:i", $tmp_etime);
		$new = strtotime("+$gap minute", $new);
		$tmp_etime = strtotime("+$gap minute", $tmp_etime);
		$diff--;
	} while ($time2 >= $tmp_etime);
	$sArray[] = date("H:i", $new);

	return array("diff" => $diff, "stime" => $sArray, "etime" => $eArray);
}

function createRange($s, $n, $wkdays)
{
	$tmpDate = new DateTime($s);
	$tmpDate->modify("-2 Month");
	$tmpEndDate = new DateTime();
	$tmpEndDate->modify("+$n");

	$outArray = array();
	do {

		//add selected day
		if (strpos($wkdays, $tmpDate->format('D')) > -1) {
			// echo $tmpDate->format('D')." : ".strpos($wkdays, $tmpDate->format('D'));
			$outArray[] = $tmpDate->format('Y-m-d');
		}

	} while ($tmpDate->modify('+1 day') <= $tmpEndDate);
	return $outArray;
}


function getWeekdays($m, $t, $w, $th, $f, $sat, $sun)
{
	$str = "";
	$str .= ($m) ? "Mon" : "";
	$str .= ($t) ? ",Tue" : "";
	$str .= ($w) ? ",Wed" : "";
	$str .= ($th) ? ",Thur" : "";
	$str .= ($f) ? ",Fri" : "";
	$str .= ($sat) ? ",Sat" : "";
	$str .= ($sun) ? ",Sun" : "";
	return $str;
}

function getDutchMonth($mon)
{
	$months = array(
		"Jan" => "Jan",
		"Feb" => "Feb",
		"Mar" => "Mrt",
		"Apr" => "Apr",
		"May" => "Mei",
		"Jun" => "Jun",
		"Jul" => "Jul",
		"Aug" => "Aug",
		"Sep" => "Sept",
		"Sept" => "Sept",
		"Oct" => "Okt",
		"Nov" => "Nov",
		"Dec" => "Dec"
	);
	return $months[$mon];
}

?>



