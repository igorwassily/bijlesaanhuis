<?php
$thisPage="Edit Profile";
session_start();	
if(!(isset($_SESSION['Student']) || isset($_SESSION['AdminUser'])))
{
	header('Location: index.php');
}
else
{
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Edit Profile</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
	
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>			
	
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="calendar/css/calendar.css">
	
<style type="text/css">
    	.mapboxgl-ctrl-attrib, .mapboxgl-ctrl-logo{display:none !important;}	
		.mapboxgl-ctrl-geocoder li > a {color: black !important;}
		.mapboxgl-ctrl-geocoder input {color: black !important}
	
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;
	border-radius:6px !important;

	}
	label
	{
	font-size: 16px !important;
	margin-left: 2px !important;
	}
	select{	
border: 1px solid #bdbdbd !important;

/*color: white !important; */
	}
	.fa, .far, .fas {
    font-family: "Font Awesome 5 Free" !important;
	font-size:16px !important;
	color: #bbb !important;
}
	.header-radius
	{
		border-top-left-radius: 6px !important;
		border-top-right-radius: 6px !important;		
	}
	.body-radius
	{
		border-bottom-left-radius: 6px !important;
		border-bottom-right-radius: 6px !important;	
	}
/*	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	*/
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.btn.btn-simple{
    border-color: white !important;
}
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}
	
	.body{border: 2px solid #f1f1f1;}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	
	
	
	.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
    color: black !important;
}
	textarea
	{
		color: white !important;
height: 100px !important;
border: 1px solid #465059 !important;
	}
	
	textarea:focus
	{
		color: black !important;
	}
		
	.btn-block
	{
		width: 40% !Important;
	}
	.errorLabel, .terrorLabel, .terrorLabel2{
		color: red;
	}
	.hide{
		display:none;
	}
	

	.theme-green .btn-primary {margin:auto;}
.card .body {
    background-color: #FAFBFC !important;
}
	
	<!-- New Calendar Style -->

.page-calendar .b-primary {
    border-color: #5CC75F !important;
}		
.b-warning {
    border-color: #ffbd6b !important;
}		
    #calendar { background-color:#fafafa !important; }
    .cal-month-day{color:black;}
    .cal-row-head {background-color: #fafafa !important;}
    .empty {  background-color: white !important; border: 2px solid black;}    
    .inline_time { color: #50d38a !important; }
    #cal-slide-content, .cal-day-outmonth{font-family:inherit; color:#bdbdbd !important;}
    .page-header{height: 0 !important;padding-bottom: 0px;margin: 0 0 50px;border-bottom: 0px;}	
	.calender_dropdownTime{display:none;}
    
<!-- New Calendar Style -->
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
?>

<!-- Main Content -->
<section class="content page-calendar">
   <div class="block-header">
       <?php require_once('includes/studentTopBar.php'); ?>
    </div>
    <div class="container-fluid ma-sidebar-container">
       <form action = "actions/admin_edit_scripts" method="post">
		

			<!--<hr style="border-top: 3px solid #f1f1f1 !important; width:95%"/>-->
            <div class="row">
                <div class="col-md-12 col-lg-12 col-xl-12">
                   <div class="card" style="background: transparent !important;">
                   
                    <div class="header header-radius" style="background-color:#F1F1F1;">
                        <h2 class="ma-subtitle">Accountgegevens</h2>
                        
					   </div>
					   <div class="body body-radius">
						   <?php
			
					
									$studentid = (isset($_SESSION['StudentID']))? $_SESSION['StudentID'] : @$_GET['sid'];
	
									$stmt = $con->prepare("SELECT firstname, lastname, dateofbirth, telephone, email, address, postalcode, city, country, latitude, longitude, place_name from contact inner join contacttype on (contacttype.contacttypeID = contact.contacttypeID) where contact.userID = ? AND contact.prmary = ? AND contacttype.contacttype = ?");
									$contacttype = 'Student';
	
									$primary = 1;
									$stmt->bind_param("iis",$studentid,$primary,$contacttype);
									$stmt->execute();
									$stmt->bind_result($fname,$lname,$dob,$telephone,$email,$address,$postalcode,$city,$country, $lat, $lng, $place_name);
									$stmt->store_result();
									$stmt->fetch();
	
	?>
	
								   <?php
			
					
									//$studentid = $_SESSION['StudentID'];
									$stmt1 = $con->prepare("select username, email from user inner join usergroup on (user.usergroupID = usergroup.usergroupID) where user.userID = ? and usergroup.usergroupname = ?");
									$contacttype1 = 'Student';
	$stmt1->bind_param("is",$studentid,$contacttype1);
	$stmt1->execute();
	$stmt1->bind_result($username,$email);
	$stmt1->store_result();
	$stmt1->fetch();
	
	?>
						   	<div class="row">
							 <div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group form-float">
									<label for="booking-code">E-mailadres 
										<i style=" color:#bbb !important;" class="fa" data-toggle="tooltip" data-placement="right" title="Dit e-mailadres wordt gebruikt voor alle vormen van contact, behalve facturering. Voor facturering wordt onderstaand ouderlijk e-mailadres gebruikt">&#xf05a;</i>
									</label>
									<input type="email" class="form-control" placeholder="E-mailadres" name="email" id="email" value="<?php echo $email; ?>" readonly>
                                    
                                </div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
										 <label for="enfants">Telefoonnummer</label>
                                    <input style="padding:16px;" type="text" class="form-control" placeholder="Telefoonnummer" name="stelephone" id="telephone" value="<?php echo $telephone; ?>">
										 <span class="terrorLabel hide"> </span>
								

								</div>
							</div>
							<div class="row">
							 <div class="col-lg-6 col-md-6 col-sm-6">	 
								<div class="form-group form-float">

								    <div class="form-group form-float">
                                    <label for="booking-code">Wachtwoord</label>
                                    <input type="password" class="form-control" placeholder="Wachtwoord" name="password" id="password">
                                    
									<input name="toggle" type="checkbox"  id="toggle"  value='0' onchange='togglePassword(this);'>
									<label style="font-weight: 100; margin-top:5px;" for="toggle" id='toggleText'>&nbsp; Laat wachtwoord zien</label>
									
									
									</div>	
                                </div>
							</div>
							</div>
						   
			<div class="row">
				<div class="col-md-4 col-sm-4">			
					<!--<button type="button" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-more-courses"> ADD MORE</button> -->
					<div class="text-center"> 
						<button style="margin-left:2px !important;" type="submit" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="" name="update-teacher">OPSLAAN</button>
					</div>
				</div>
			</div>
							
							   <div class="form-group form-float">
									<!-- <label for="booking-code">Username</label> -->
								   <!--<input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="StudentAccountInfo">-->
								   <input type="hidden" class="form-control" placeholder="First Name" name="student-id" id="student-id" value="<?php echo $studentid; ?>">
									<input type="hidden" class="form-control" placeholder="Username" name="username" id="username" value="<?php echo $username; ?>" readonly>
                                    
                                </div>



						<!--	<div class="text-center">
                               <button type="submit" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block hide" id="submit-btn2" name="update-teacher"></button>
						    <button type="button" class="btn btn-primary btn-raised m-b-10 bg-green btn-block waves-effect btn-block " id="add-level-btn2" name="update-teacher"> UPDATE </button>
							</div>
						   </form> -->
					   </div>
                </div>
                
				</div>
            </div>
				<div class="row">
				<div class="col-md-12 col-lg-12 col-xl-12">
                    <div class="card" style="background: transparent !important;">
                   
                    <div class="header header-radius" style="background-color:#F1F1F1;">
                        <h2 class="ma-subtitle">Persoonsgegevens</h2>
                        
					   </div>
					    <div class="body body-radius">
						   <?php
			
					
									//$studentid = $_SESSION['StudentID'];
										$stmt11 = $con->prepare("SELECT firstname, lastname, dateofbirth, telephone, email, address, postalcode, city, country from contact where contact.userID = ? AND contact.prmary = ? AND contact.contacttypeID = ?");
																	$contacttype = '2'; //Student


									$primary = 1;
									$stmt11->bind_param("iii",$studentid,$primary,$contacttype);
									$stmt11->execute();
									$stmt11->bind_result($sfname,$slname,$sdob,$stelephone,$semail,$saddress,$spostalcode,$scity,$scountry);
									$stmt11->store_result();
									$stmt11->fetch();
	
									$stmt = $con->prepare("SELECT firstname, lastname, dateofbirth, telephone, email, address, postalcode, city, country from contact where contact.userID = ? AND contact.prmary = ? AND contact.contacttypeID = ?");
									$contacttype = '3'; //Parent
	

	$primary = 0;
	$stmt->bind_param("iii",$studentid,$primary,$contacttype);
	$stmt->execute();
	$stmt->bind_result($pfname,$plname,$pdob,$ptelephone,$pemail,$paddress,$ppostalcode,$pcity,$pcountry);
	$stmt->store_result();
	$stmt->fetch();
	
	?>
						   	<div class="row">
							 <div class="col-lg-6 col-md-6 col-sm-6">
							   <div class="form-group form-float">
									<label for="booking-code">Voornaam leerling</label>
								   <input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="StudentPersonalInfo">
								   <input type="hidden" class="form-control" placeholder="First Name" name="student-id" id="student-id" value="<?php echo $studentid; ?>">
								   <input type="hidden" class="form-control" placeholder="First Name" name="sdob" id="dob" value="<?php echo $sdob; ?>">
								   <input type="hidden" class="form-control" placeholder="First Name" name="saddress" id="address" value="<?php echo $saddress; ?>">
								   <input type="hidden" class="form-control" placeholder="First Name" name="spostal-code" id="postal-code" value="<?php echo $spostalcode; ?>">
								   <input type="hidden" class="form-control" placeholder="First Name" name="scity" id="city" value="<?php echo $scity; ?>">
								   
									<input type="text" class="form-control" placeholder="Voornaam leerling" name="sfirst-name" id="first-name" value="<?php echo $sfname; ?>">
									
                                    
                                </div>
								</div>

								<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group form-float">
									<label for="booking-code">Achternaam leerling</label>
									<input type="text" class="form-control" placeholder="Achternaam leerling" name="slast-name" id="last-name" value="<?php echo $slname; ?>">
                                    
                                </div>
								</div>
							</div>								
						  <div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">
							   <div class="form-group form-float">
									<label for="booking-code">Voornaam ouder</label>
								  <!-- <input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice" value="StudentParentInfo"> -->
								   <input type="hidden" class="form-control" placeholder="First Name" name="student-id" id="student-id" value="<?php echo $studentid; ?>">
									<input type="text" class="form-control" placeholder="Voornaam ouder" name="pfirst-name" id="first-name" value="<?php echo $pfname; ?>">
                                    
                                </div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-6">
								<div class="form-group form-float">
									<label for="booking-code">Achternaam ouder</label>
									<input type="text" class="form-control" placeholder="Achternaam ouder" name="plast-name" id="last-name" value="<?php echo $plname; ?>">
                                    
                                </div>
								</div>

                            </div>
								
						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-6">	 
								<div class="form-group form-float">
									<div class="row">
									
									 <div class="col-lg-12 col-md-12 col-sm-12">
										 <label for="enfants">Telefoonnummer ouder</label>
                                    <input type="text" class="form-control" placeholder="Telefoonnummer ouder" name="ptelephone" id="telephone1" value="<?php echo $ptelephone; ?>">
									<span class="terrorLabel2 hide"> </span>
									</div>
									</div>
									
                                </div>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-6">	 
								<div class="form-group form-float">
									<div class="row">									
									 <div class="col-lg-12 col-md-12 col-sm-12">
										 <label for="enfants">E-mailadres ouder
											 <i style=" color:#bbb !important;" class="fa" data-toggle="tooltip" data-placement="right" title="Dit e-mailadres wordt uitsluitend gebruikt voor " >&#xf05a;</i>
										 </label>
                                    <input type="text" class="form-control" placeholder="E-mailadres ouder" name="pemail" id="pemail" value="<?php echo $pemail; ?>" required>
									</div>
									</div>
									
                                </div>
							</div>
						</div>

			<div class="row">
				<div class="col-md-4 col-sm-4">			
					<!--<button type="button" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-more-courses"> ADD MORE</button> -->
					<div class="text-center"> 
						<button style="margin-left:2px !important;" type="submit" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="" name="update-teacher">OPSLAAN</button>
					</div>
				</div>
			</div>
                               
								
						<!--	   <div class="text-center">
						    <button type="submit" class="btn btn-raised m-b-10 bg-green  hide" id="submit-btn3" name="update-teacher"></button>
							   <button type="button" class="btn btn-primary  btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-level-btn3" name="update-teacher"> UPDATE </button>
								   </div>
						   </form> -->
							   
					   </div>
                </div>
                </div>
				</div>
				<div class="row">
					<div class="col-md-6 col-lg-6 col-xl-6">
						<div class="card" style="background: transparent !important;">
							<div class="header header-radius" style="background-color:#F1F1F1;">
								<h2 class="ma-subtitle">Pas bijleslocatie aan</h2>
						   </div>
						   <div class="body body-radius">
								<input type="hidden" class="form-control" name="lat" id="lat" value="<?php echo ($lat)? $lat : 33.719361; ?>">
								<input type="hidden" class="form-control"  name="lng" id="lng" value="<?php echo ($lng)? $lng : 73.074144; ?>">
								<input type="hidden" class="form-control"  name="place_name" id="place_name" value="<?php echo ($place_name)? $place_name : ""; ?>">
								<input type="hidden" class="form-control" placeholder="First Name" name="student-id" id="student-id" value="<?php echo $studentid; ?>">
								<div id='map' style="height: 200px; overflow: visible;"></div>
						   
					<div class="text-center"> 
						<button style="margin-left:2px !important; width:28% !important; margin-top:15px !important;;" type="submit" class="btn btn-raised m-b-10 bg-green btn-primary btn-block waves-effect btn-block" id="" name="update-teacher">OPSLAAN</button>
					</div>	
							   
						   </div>
							<pre id='coordinates' class='coordinates'></pre>
						</div>
					</div>		
					<!--<button type="button" class="btn btn-raised m-b-10 bg-green btn-block waves-effect btn-block" id="add-more-courses"> ADD MORE</button> -->

						
				</div>	   

	</form>
		<div class="row">
			
            <div class="col-md-12 col-lg-12 col-xl-12">
                <div class="card">
					<div class="header" style="background-color:#F1F1F1;">
                        <div class="calendar-page-header page-header">
							<h3 class="ma-subtitle"></h3>
								 <div class="colorLegend">
									<span class="color available"></span> Beschikbaar
									<span class="color request"></span> Bijlesverzoek
									<span class="color appointment"></span> Afspraak
								</div>
                            <div class="pull-right form-inline" style="margin-top: -40px;">
                                <div class="btn-group">
                                    <button class="btn btn-primary" data-calendar-nav="prev"><< </button>
                                    <button class="btn btn-default" data-calendar-nav="today">Today</button>
                                    <button class="btn btn-primary" data-calendar-nav="next">>> </button>
                                </div>
                            </div>
							
                        </div>
                        
					   </div>
                    <div class="body">
						
                        <div id="calendar" style="max-width:100%;"></div>                        
                    </div>
                </div>
            </div>		
		</div>
		
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js --> 
	

<!-- New Calendar JS -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
    <script type="text/javascript" src="calendar/js/calendar.js"></script>
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
<!-- New Calendar JS -->
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>
<?php
    require_once('includes/footerScriptsAddForms.php');
?>

	 <?php
	
							if(isset($_GET['successMsg']))
							{
								echo '<script type="text/javascript>"';
								echo 'swal("Good job!", "You clicked the button!", "success");';
								echo '</script>';
							}
	
	?>
</body>
	<script>
 var calendar;
 var options;
	
$(".page-loader-wrapper").css("display", "none");
  	
    "use strict";

    options = {
        events_source: "student-ajaxcall.php?dashboard=23892382",
        view: 'month',
        tmpl_path: 'calendar/tmpls/',
        tmpl_cache: false,        day: 'now',

        merge_holidays: false,
        display_week_numbers: false,
        weekbox: false,
        //shows events which fits between time_start and time_end
        show_events_which_fits_time: true,

        onAfterEventsLoad: function(events) {
            if(!events) {
                return;
            }
            var list = $('#eventlist');
            list.html('');

            $.each(events, function(key, obj) {
                //converting the timeZone to UTC
                obj.start = new Date(parseInt(obj.start)).setTimezoneOffset(0);
                obj.end = new Date(parseInt(obj.end)).setTimezoneOffset(0);

                $(document.createElement('li'))
                    .html('<a href="' + obj.url + '">' + obj.title + '</a>')
                    .appendTo(list);
            });
        },
        onAfterViewLoad: function(view) {
            $('.page-header h3').text(this.getTitle());
            $('.btn-group button').removeClass('active');
            $('button[data-calendar-view="' + view + '"]').addClass('active');
        },
        classes: {
            months: {
                general: 'label'
            }
        }
    };

     calendar = $('#calendar').calendar(options);

    $('.btn-group button[data-calendar-nav]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.navigate($this.data('calendar-nav'));
        });
    });

    $('.btn-group button[data-calendar-view]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.view($this.data('calendar-view'));
        });
    });

    $('#first_day').change(function(){
        var value = $(this).val();
        value = value.length ? parseInt(value) : null;
        calendar.setOptions({first_day: value});
        calendar.view();
    });

    $('#language').change(function(){
        calendar.setLanguage($(this).val());
        calendar.view();
    });

    $('#events-in-modal').change(function(){
        var val = $(this).is(':checked') ? $(this).val() : null;
        calendar.setOptions({modal: val});
    });
    $('#format-12-hours').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({format12: val});
        calendar.view();
    });
    $('#show_wbn').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({display_week_numbers: val});
        calendar.view();
    });
    $('#show_wb').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({weekbox: val});
        calendar.view();
    });
    $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
        //e.preventDefault();
        //e.stopPropagation();
    });

    $(".cal-month-day").on("click",function(){
        console.log($(this).find(".cal-slide-content").html());
  
    });

    $(".list-item").on("mouseover",function(e){
        e.preventDefault(); 
    });
		
		
		
		
		
		$(document).ready(function(){terrorLabel2
			
			$("#add-level-btn2").click(function(e){
				e.preventDefault();

				var phone = $("#telephone").val();

				if(!(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(phone))){
						$(".terrorLabel").text("Invalid telephone number").removeClass("hide");			
						return;
				}

			   $("#submit-btn2").click();

		  });
									 
			$("#add-level-btn3").click(function(e){
				e.preventDefault();

				var phone = $("#telephone1").val();
alert(230);
				if(!(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(phone))){
						$(".terrorLabel2").text("Invalid telephone number").removeClass("hide");			
						return;
				}

			   $("#submit-btn3").click();

		  });						 


		  $("#add-level-btn1").click(function(e){
				e.preventDefault();

				var pass = $("#password").val();
				var cpass = $("#cpassword").val();

				if(pass.length < 8){
						$(".errorLabel").text("Password must be minimum 8 characters").removeClass("hide");
						return;
				}

				if(pass != cpass){
						$(".errorLabel").text("Password does not matched").removeClass("hide");			
						return;
				}

			   $("#submit-btn").click();

		  });
	  
		});
		
		
		

		var lat=<?php echo ($lat)? $lat : 33.719361; ?>;
		var lng =<?php echo ($lng)? $lng : 73.074144; ?>;
		var place_name = <?php echo ($place_name)? "'".$place_name."'" : "''"; ?>;
		var map = null;
		var canvas = null;
		

mapboxgl.accessToken = "<?php echo ACCESS_TOKEN; ?>";
 map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11',
center: [lng, lat],
zoom: 13
});
 
var geocoder = new MapboxGeocoder({
accessToken: mapboxgl.accessToken, limit:10, countries: 'nl', placeholder:'Zoeken'
});
 
map.addControl(geocoder);
 
// After the map style has loaded on the page, add a source layer and default
// styling for a single point.
map.on('load', function() {
	$(".mapboxgl-ctrl-geocoder input").val(place_name);
// Listen for the `result` event from the MapboxGeocoder that is triggered when a user
// makes a selection and add a symbol that matches the result.
geocoder.on('result', function(ev) {
	console.log("Here: ",ev);
	lat = ev.result.center[1];
	lng = ev.result.center[0];
	place_name = ev.result.place_name;
	
	$("#lat").val(lat);
	$("#lng").val(lng);
	$("#place_name").val(place_name);
	
	isAddressChanged = true;
map.getSource('point').setData(ev.result.geometry);
});
});









 canvas = map.getCanvasContainer();
 
var geojson = {
"type": "FeatureCollection",
"features": [{
"type": "Feature",
"geometry": {
"type": "Point",
"coordinates": [lng, lat]
}
}]
};
 
function onMove(e) {
var coords = e.lngLat;
 
// Set a UI indicator for dragging.
canvas.style.cursor = 'grabbing';
 
// Update the Point feature in `geojson` coordinates
// and call setData to the source layer `point` on it.
geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
map.getSource('point').setData(geojson);
}
 
function onUp(e) {
var coords = e.lngLat;
 console.log(e.lngLat);
	lat = e.lngLat.lat;
	lng = e.lngLat.lng;	
	
	$("#lat").val(lat);
	$("#lng").val(lng);
	$("#place_name").val(place_name);
	
	isAddressChanged = true;
// Print the coordinates of where the point had
// finished being dragged to on the map.
coordinates.style.display = 'block';
//coordinates.innerHTML = 'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat;
canvas.style.cursor = '';
	geocoder['eventManager']['limit'] = 1;	
	geocoder['options']['limit'] = 1;	
   geocoder.query(lng+","+lat);
  
	
// Unbind mouse/touch events
map.off('mousemove', onMove);
map.off('touchmove', onMove);
	geocoder['eventManager']['limit'] = 10;	
	geocoder['options']['limit'] = 10;	
}
 
map.on('load', function() {
 
// Add a single point to the map
map.addSource('point', {
"type": "geojson",
"data": geojson
});
 
map.addLayer({
"id": "point",
"type": "circle",
"source": "point",
"paint": {
"circle-radius": 10,
"circle-color": "#3887be"
}
});
 
// When the cursor enters a feature in the point layer, prepare for dragging.
map.on('mouseenter', 'point', function() {
map.setPaintProperty('point', 'circle-color', '#3bb2d0');
canvas.style.cursor = 'move';
});
 
map.on('mouseleave', 'point', function() {
map.setPaintProperty('point', 'circle-color', '#3887be');
canvas.style.cursor = '';
});
 
map.on('mousedown', 'point', function(e) {
// Prevent the default map drag behavior.
e.preventDefault();
 
canvas.style.cursor = 'grab';
 
map.on('mousemove', onMove);
map.once('mouseup', onUp);
});
 
map.on('touchstart', 'point', function(e) {
if (e.points.length !== 1) return;
 
// Prevent the default map drag behavior.
e.preventDefault();
 
map.on('touchmove', onMove);
map.once('touchend', onUp);
});

});


</script>
<script>
	$(document).ready(function(){
					$("#showHide").on('change', function () {
			if ($(this).is(":checked")) {
				$(".password").attr("type", "text");
				//$("#labelforShowHide").html("Hide Password");
			} else {
				$(".password").attr("type", "password");
				//$("#labelforShowHide").html("Show Password");
				
			}
		});
	});
$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});		

</script>
</html>
<?php
}
?>