<?php
require_once('includes/connection.php');
$thisPage = "Student Profile";
session_start();

if (!isset($_SESSION['Teacher']) && !isset($_SESSION['Student']) && !isset($_SESSION['AdminUser'])) {
    header('Location: index.php');
} else {


?>

    <!doctype html>
    <html class="no-js " lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">

        <title>Leerlingen | Bijles Aan Huis</title>
        <?php
        require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
        ?>
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
        <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />
        <link rel="stylesheet" href="calendar/css/calendar.css">

        <style type="text/css">
            .custom__btn.action-button {

                color: #FFFFFF !important;
                background-color: #5CC75F !important;
                font-weight: normal !important;
                border-radius: 6px !important;
            }

            .custom__btn.action-button:hover {
                box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.17);
            }

            .card.profile-header .body {
                border-radius: 0 0 6px 6px;
            }

            .card.profile-header .header {
                padding: 15px 20px !important;
            }

            .card.profile-header .header h2 {
                padding-bottom: 0;

            }

            .card.profile-header .header h2 .size__color {
                font-size: 1.2rem !important;

            }

            .card.profile-header .header h2::before {
                content: none;
            }


            #calendar {
                background-color: #fafafa !important;
            }

            .cal-month-day {
                color: black;
            }

            .cal-row-head {
                background-color: #fafafa !important;
            }

            .empty {
                background-color: white !important;
                border: 2px solid black;
            }

            .inline_time {
                color: #50d38a !important;
            }

            .inline_time_disabled {
                color: #6b7971 !important;
            }

            #cal-slide-content {
                font-family: inherit;
                color: #bdbdbd;
            }

            .page-header {
                height: 0 !important;
                padding-bottom: 0px;
                margin: 0 0 50px;
                border-bottom: 0px;
            }

            .page-calendar #calendar {
                max-width: 100%;
            }

            .mapboxgl-ctrl-attrib,
            .mapboxgl-ctrl-logo {
                display: none !important;
            }

            .input-group {
                background-color: transparent !important;
            }

            .input-group-focus span {
                background-color: transparent !important;
            }

            .input-group-focus #chatsender {
                border: 1px solid #ff5200c2 !important;
            }

            .mapboxgl-ctrl-geocoder {
                display: none !important;
            }

            .body-radius {
                border-bottom-left-radius: 6px !important;
                border-bottom-right-radius: 6px !important;
            }

            .custom__btn {
                margin-top: 2rem;
                color: #FFFFFF !important;
                border: 0.125rem solid #5CC75F;
                border-radius: 1.5rem;
                font-family: 'Poppins', Helvetica, Arial, Lucida, sans-serif !important;
                font-weight: 600;
                text-transform: uppercase;
                background-color: #5CC75F;
                width: 100%;
                transition: 0.3s;
                padding: .5rem 1rem;
            }

            .custom__btn:hover {
                /* color: #5CC75F !important; */
                background-image: initial;
                background-color: #FFFFFF;
            }

            .header {
                padding: 15px 5px 5px 5px !important;
            }

            .margin {
                margin-bottom: 1rem !important;
                margin-right: .5rem !important;
            }

            .margin__right {
                margin-right: 1rem;
            }

            .size__color {
                font-size: 1.3125rem !important;
                color: #5CC75F !important;
            }

            @media screen and (min-width: 1360px) and (max-width: 2560px) {
                .container {
                    max-width: calc(49% - 2px) !important;
                }
            }

            @media screen and (min-width: 1024px) and (max-width: 1360px) {
                .container {
                    max-width: calc(100% - 2px) !important;
                }
            }

            button.btn.action-button.student-list__button {
                background-color: #5CC75F;
                border: solid 2px #5CC75F;
                color: #fafbfc !important;
                transition-delay: 0s;
                transition-duration: 0.2s;
                transition-property: all;
                font-weight: 600;
                font-family: 'Poppins', Helvetica, Arial, Lucida, sans-serif;
                text-transform: uppercase;
                border-radius: 6px;
                border-radius: 6px;
                padding: 15px 20px;
                float: none;
                margin: 10px 0 0 0 !important;
                width: 200px !important;
            }
        </style>
        <?php
        $activePage = basename($_SERVER['PHP_SELF']);
        ?>
    </head>

    <body class="theme-green student-list">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>


        <?php
        require_once('includes/header.php');

        ?>

        <!-- Main Content -->
        <section class="content page-calendar inbox">
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12">
                        <div class="card message-card action_bar">
                            <div class="header">
                                <h2 class="ma-main-title">Bijles Inboeken</h2>
                            </div>
                            <div class="body student-list__body">
                                <?php
                                $q1 = "SELECT DISTINCT cs.*, cp.firstname AS parent_name, cp.email AS parent_email
FROM student s
         LEFT JOIN contact cs ON cs.userID = s.userID AND cs.contacttypeID=2
         LEFT JOIN contact cp ON cp.contacttypeID=3 AND cp.userID = cs.userID
         LEFT JOIN calendarbooking a
                   ON s.userID = a.studentID AND a.teacherID = {$_SESSION['TeacherID']}
         LEFT JOIN email e ON (e.senderID = s.userID AND e.receiverID = {$_SESSION['TeacherID']} OR
                               e.senderID = {$_SESSION['TeacherID']} AND e.receiverID = s.userID)
WHERE e.emailID IS NOT NULL OR a.calendarbookingID IS NOT NULL
ORDER BY cs.firstname"; //ORDER BY cb.datee DESC
                                $r1 = mysqli_query($con, $q1);
                                $flag = true;
                                $userID = 0;
                                $isList = false;
                                if ($r1) {
                                    while ($row = mysqli_fetch_assoc($r1)) {
                                        $address = empty($row['address']) ? $row['place_name'] : "{$row['address']}, {$row['postalcode']} {$row['city']}";

                                        $isList = true;
                                ?>
                                        <div class="row clearfix">
                                            <div class="col-md-12 col-lg-12 col-xl-12">
                                                <div class="card profile-header">
                                                    <div class="header">
                                                        <h2 class="student-list__heading"><span class="student-list__heading--span">Leerling:</span> <?php echo "<a class='student-list__heading--link' href='student-profile?sid=$row[userID]'>$row[firstname]</a>"; ?></h2>
                                                    </div>
                                                    <div class="body" style="border: 2px solid #F1F1F1;">
                                                        <div class="row">
                                                            <div class="col-md-6"><strong>Adres:</strong></div>
                                                            <div class="col-md-6"><?php echo $address; ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"><strong>Tel.nummer contactpersoon:</strong></div>
                                                            <div class="col-md-6"><?php echo $row['telephone']; ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"><strong>E-mailadres contactpersoon:</strong></div>
                                                            <div class="col-md-6"><?php echo $row['email']; ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"><strong>Naam ouder:</strong></div>
                                                            <div class="col-md-6"><?php echo $row['parent_name']; ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6"><strong>E-mailadres ouder:</strong></div>
                                                            <div class="col-md-6"><?php echo $row['parent_email']; ?></div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-lg-4 col-md-4 col-sm-6">
                                                                <a href="<?php echo "student-profile?sid=$row[userID]"; ?>&#calendar-top"><button class="btn action-button student-list__button" data-type="prompt">Bijles inboeken</button></a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                <?php }
                                }
                                if (!$isList) {
                                    echo '
                        <div class="row clearfix">
                            <div class="col-md-12 col-lg-12 col-xl-12">
                                <ul class="mail_list list-group list-unstyled">
                                    <li class=" no-message text-center no-hover no-message--edit" style="">Geen leerlingen</li>
                                </ul>
                            </div>
                        </div>
                    ';
                                }
                                ?>


                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </section>

        <div class="modal modal-fullscreen fade" id="modal-delete-course" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h6 class="modal-title" id="myModalLabel"><b class="popupMsg">Send Message to Teacher</b></h6>
                    </div>
                    <div class="modal-body">
                        <div id="deresultcourse"></div>
                        <input type="hidden" id="tid" name="tid" value="<?php echo isset($_POST['tid']) ? $_POST['tid'] : (isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : ''); ?>" />
                        <div class="form-group">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="chat-message clearfix">
                                    <div class="input-group p-t-15">
                                        <input type="text" class="form-control" id="chatsender" style="height:40px;" autocomplete="off" placeholder="Type bericht...">
                                        <span class="input-group-addon sendMail">
                                            <i class="zmdi zmdi-mail-send"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
        <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
        <?php
        require_once('includes/footerScriptsAddForms.php');
        ?>

        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
        <script type="text/javascript" src="calendar/js/calendar.js"></script>
        <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
        <script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->

        <script>
            // this function is added
            Date.prototype.setTimezoneOffset = function(minutes) {
                var _minutes;
                if (this.timezoneOffset == _minutes) {
                    _minutes = this.getTimezoneOffset();
                } else {
                    _minutes = this.timezoneOffset;
                }
                if (arguments.length) {
                    this.timezoneOffset = minutes;
                } else {
                    this.timezoneOffset = minutes = this.getTimezoneOffset();
                }
                return this.setTime(this.getTime() + (_minutes - minutes) * 6e4);
            };


            var calendar;
            var options;

            var teacherid = <?php echo (isset($_SESSION['TeacherID'])) ? $_SESSION['TeacherID'] : 0; ?>;
            var studentid = <?php echo (isset($_GET['sid'])) ? $_GET['sid'] : 0; ?>;
            var std_lat = "<?php echo (isset($_SESSION['std_lat'])) ? $_SESSION['std_lat'] : (isset($d['latitude']) ? $d['latitude'] : ''); ?>";
            var std_lng = "<?php echo (isset($_SESSION['std_lng'])) ? $_SESSION['std_lng'] : (isset($d['longitude']) ? $d['longitude'] : ''); ?>";
            var place_name = "<?php echo (isset($_SESSION['std_place_name'])) ? $_SESSION['std_place_name'] : (isset($d['place_name']) ? $d['place_name'] : ''); ?>";
            var hourlyCost = 0;

            var calendar;
            var options;


            var _obj = null;
            var _id = null;
            var _name = null;

            var _ddate = null;
            var _stime = null;
            var _etime = null;

            $(document).ready(function() {
                $(".page-loader-wrapper").css("display", "none");
            });
        </script>


        <!-----------------------------map ------------------------->
        <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
        <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>

        <script>
            console.log("array...");
            var userIDs = '<?php echo  $userID ?>';
            var _userIDs = userIDs.split(",");
            for (var i = 1; i < _userIDs.length; i++) {
                $("#map" + _userIDs[i]).trigger("click");
            }

            function intializeLatLng(_lat, _lng, _place_name, userID) {
                var map;
                var canvas;
                var lat = "";
                var lng = "";
                var place_name = "";
                var mapCheck = true;
                lat = _lat;
                lng = _lng;
                place_name = _place_name;


                mapboxgl.accessToken = 'pk.eyJ1Ijoic2JpbGFsc2hhaCIsImEiOiJjanU3cmw3aXIwMmE5NDl0Zjd6Mm56Nmg4In0.iobs2-o479pXYRd_zon95g';
                map = new mapboxgl.Map({
                    container: 'map' + userID,
                    style: 'mapbox://styles/mapbox/streets-v11',
                    center: [-79.4512, 43.6568],
                    zoom: 13
                });

                var geocoder = new MapboxGeocoder({
                    accessToken: mapboxgl.accessToken,
                    limit: 1
                });

                map.addControl(geocoder);

                // After the map style has loaded on the page, add a source layer and default
                // styling for a single point.
                map.on('load', function() {
                    //$(".mapboxgl-ctrl-geocoder input").prop('disabled', true);
                    //$(".mapboxgl-ctrl-geocoder").css('display', 'none');	 	
                    geocoder.query(lng + "," + lat);
                    map.resize();
                    // Listen for the `result` event from the MapboxGeocoder that is triggered when a user
                    // makes a selection and add a symbol that matches the result.
                    geocoder.on('result', function(ev) {
                        console.log("Here: ", ev);
                        lat = ev.result.center[1];
                        lng = ev.result.center[0];
                        place_name = ev.result.place_name;
                        isAddressChanged = true;

                        map.getSource('point').setData(ev.result.geometry);
                    });
                });








                canvas = map.getCanvasContainer();

                var geojson = {
                    "type": "FeatureCollection",
                    "features": [{
                        "type": "Feature",
                        "geometry": {
                            "type": "Point",
                            "coordinates": [-79.4512, 43.6568]
                        }
                    }]
                };


                map.on('load', function() {

                    // Add a single point to the map
                    map.addSource('point', {
                        "type": "geojson",
                        "data": geojson
                    });

                    map.addLayer({
                        "id": "point",
                        "type": "circle",
                        "source": "point",
                        "paint": {
                            "circle-radius": 10,
                            "circle-color": "#3887be"
                        }
                    });

                    // When the cursor enters a feature in the point layer, prepare for dragging.



                    map.on('mousedown', 'point', function(e) {
                        // Prevent the default map drag behavior.
                        e.preventDefault();

                        canvas.style.cursor = 'grab';

                    });

                    map.on('touchstart', 'point', function(e) {
                        if (e.points.length !== 1) return;

                        // Prevent the default map drag behavior.
                        e.preventDefault();

                        map.on('touchmove', onMove);
                        map.once('touchend', onUp);
                    });

                });

            } // end intilize function
        </script>
    </body>

    </html>

    <div class="modal modal-fullscreen fade" id="modal-delete-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h6 class="modal-title" id="myModalLabel">Are you sure you want to Cancel this Slot?</h6>
                </div>
                <div class="modal-body">
                    <div id="deresulttype"></div>
                    <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                        <input type="hidden" id="delete-id-type" name="delete-id-type" />
                        <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType" />
                        <div class="form-group">
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3 text-center">

                                <button type="button" class="btn btn-success" id="delete-yes-type">Yes</button>
                                <button type="button" class="btn btn-danger" id="delete-no-type">No</button>

                            </div>
                            <div class="col-md-3 col-sm-3 col-xs-12 col-md-offset-3 text-center"></div>
                        </div>
                    </form>
                    <!-- -->
                </div>

            </div>
        </div>
    </div>
<?php
}
?>