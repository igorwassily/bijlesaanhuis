<?php 
require_once("includes/connection.php");


?>
<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Wachtwoord vergeten | Het overkomt de besten | Bijles Aan Huis; kwaliteit, innovatie en betrouwbaarheid.">
	<title>Wachtwoord vergeten | Bijles Aan Huis</title>
	<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
	
	?>
    <!-- Custom Css -->
	<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />	
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	
 <link rel="stylesheet" href="calendar/css/calendar.css">	
	<!-- New Calendar Style -->
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

	<style>
	h5, .h5 {
	font-size: 1.57em !important;
	}
	.input-lg{
	padding: 0px !important;
	height: 20px !important;
	}
	.input-lg .input-group-addon{
		padding: 0px !important;
	}
		*{color: #000;}

		.msg-error {
		  color: #c65848;
		}
		.g-recaptcha.error {
		  border: solid 2px #c64848;
		  padding: .2em;
		  width: 19em;
		}		
		/* {
			color: #486066!important;
			font-family: Poppins light !important;
			background-color: #f1f1f1 !important;
		}*/
		div.container{width: 100%;}
		.zmdi{font: normal normal normal 14px/1 'Material-Design-Iconic-Font'!important;}
		input::placeholder {color: #486066!important;
		}
		.border
    			border-radius: 50px;
		}
		.page-header{
				/* background-image: url("http://bt.bijlesaanhuis.nl/file_download.php?file_id=96&type=bug"); */
			background-color : #FAFBFC !important;
		}
		
		/* login */
		.ma-heading{
			color: #5CC75F;
			border-bottom: 1px solid #bbb;
			padding-bottom: 10px;
		}
		.authentication .card-plain.card-plain .form-control:focus{
			color: #486066 !important;
		}
		.authentication .card-plain.card-plain .form-control, .authentication .card-plain.card-plain  .input-group-focus .input-group-addon{
			color: #486066 !important;
		}
		
		.authentication .card-plain .form{
			background: #f1f1f1 !important;
			padding: 30px;
			border-radius: 6px;
		}
		.authentication .card-plain{
			max-width: 50%;
		}
		.authentication .link, h6{
			color: #000;
		}
		#recaptcha{
			margin: 0 auto;
		}
		.authentication .card-plain.card-plain .input-group-addon{
			color: rgb(0, 0, 0);
		}
		
		@media only screen and (max-width: 768px){
				.authentication .card-plain{
					max-width: 100%;
			}
		}
*{
		color:#486066!important;
		font-size: 16px;
		font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important; 
	}

		
		.centeer{width:80%; margin:auto;}
		.input-group, .input-group input[type=email]{border-radius: 6px !important;}
		.input-group-addon{border:0px !important;}
		.h5_text{padding:0 !important;}
		button#forgotpassword:hover {
			color: #5CC75F !important;
			background-color: #F1F1F1!important;
		}
		.theme-green .btn-primary:active, .theme-green .btn-primary:hover, .theme-green .btn-primary{
			color: #F1F1F1 !important;
			background-color: #5CC75F !important;
		}
		 
	</style>
</head>

<body class="theme-green authentication login-body">
<!-- Navbar -->

<?php
    require_once('includes/header.php');
?>
<!-- End Navbar -->
<div class="page-header">
    <div class="container content-container">
        <div class="col-md-12 login-container login-container--md-padding">
				
           <div class="card-plain">
				<!-- Error Message -->
			<?php
				require_once("includes/connection.php");
								if(isset($_GET['msg']))
								{
									?>
							<div class="bs-example" style="font-size:10px;">
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo test_input($_GET['msg']); ?>
    </div>
</div>
							<?php } ?>
            
			    <form id="login_form" class="form login-form" method="post" action="actions/forgotpassword" style="padding-left:0; padding-right:0;">
                    <div class="header">
                        <h5 class="ma-heading login-form-header">Wachtwoord aanpassen</h5>
                    </div>
                    <div class="login-form-content">
						<p class="login-form__text">Je krijgt via de mail een link waarmee je het wachtwoord kunt aanpassen</p>
                        <div class="login-form-content__group">
                            <input type="email" class="login-form-input" placeholder="E-mailadres" name="email" required maxlength="60" minlength="5" />
                            <span class="input-group-addon inloggen-input-icon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                    </div>
                    <div class="login-form__button-container">
					    <button type="submit" name="forgotpassword" id="forgotpassword" class="btn login-form-button">VERSTUREN</button>
                    </div>
                    <div class="login-form-footer">
                        <p class="login-form-footer__text"><a class="login-form-footer__link" href="index" class="link">INLOGGEN</a></p>
                    </div>
                    <span class="msg-error"></span>
                </form>
            </div>
        </div>
    </div>
    <?php
    require_once('includes/footerScriptsAddForms.php');
?>
</div>

<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>
</body>
</html>