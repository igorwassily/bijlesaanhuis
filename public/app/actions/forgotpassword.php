<?php

if (isset($_POST['forgotpassword'])) {
	require_once('../includes/connection.php');
	require_once('../../../inc/Email.php');
	session_start();
	$stmt = $con->prepare("SELECT u.userID, username, c.firstname from user u,contact c WHERE c.userID=u.userID AND u.email=?  LIMIT 1");
	$stmt->bind_param('s', $email);
	$email = test_input($_POST['email']);
	if ($stmt->execute()) {
		$stmt->bind_result($userID, $username, $name);
		$stmt->store_result();
		$stmt->fetch();
		$count = $stmt->num_rows;
		if ($count == 1) {
			$stmt->close();
//			$query = $con->query("SELECT firstname FROM contact WHERE prmary=0 AND userID=$userID LIMIT 1");
//			if ($query !== false && $query->num_rows > 0) {
//				$name .= " / " . $query->fetch_array(MYSQLI_NUM)[0];
//			}

			$verificationcode = md5(uniqid(rand(), true));
			$stmt01 = $con->prepare("UPDATE user set pwreset_token = ? where userID = ?");
			$stmt01->bind_param("si", $verificationcode, $userID);
			if ($stmt01->execute()) {
				$link = SITE_URL . "passwordReset?email=$email&hash=$verificationcode";
				$email = new Email($db, 'forgot_password');
				$email->Prepare($userID, ['link' => $link]);
//				$sentmail = email_forget_password($name, $email, $link);

				if ($email->Send('user')) {
					header("Location: ../forgotPasswordSuccess");
				}
			}
		} else {
			header("Location: ../forgotPassword.php?msg=Dit e-mailadres is niet in gebruik");
		}
	} else {
		header("Location: ../forgotPassword.php?msg=There is some technical problem, Kindly try later.");
	}
}
?>