<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

session_start();
require_once("../includes/connection.php");
require_once("../../../inc/AWS.php");
//print_r($_POST);exit;

if (isset($_POST['action'])) {
	header("content-type: text/json");
	if (empty($_SESSION['AdminUser'])) {
		die(json_encode(["success" => false, "admin rights are required"]));
	}

	switch ($_POST['action']) {
		case 'add_note':
			if (empty($_POST['value'])) {
				die(json_encode(['success' => false, "error" => "missing param 'value'"]));
			}
			$table = "";
			$id = null;
			if (!empty($_POST['tid'])) {
				$table = "teacher";
				$id = $_POST['tid'];
			} else if (!empty($_POST['sid'])) {
				$table = "student";
				$id = $_POST['sid'];
			}
			if (empty($id)) {
				die(json_encode(['success' => false, "error" => "missing param 'tid' or 'sid'"]));
			}

			$stmt = $con->prepare("SELECT admin_comments FROM $table WHERE userID=?");
			$stmt->bind_param("s", $id);
			$stmt->execute();
			$stmt->bind_result($comments);
			$stmt->fetch();
			$stmt->close();

			$notes = [];
			if (!empty($comments)) {
				$notes = json_decode($comments);
				if (empty($notes)) {
					$notes = [[
						"author" => "",
						"time" => "",
						"text" => $comments
					]];
				}
			}

			$notes[] = [
				"author" => $_SESSION["AdminUser"],
				"time" => date("H:i, d F Y"),
				"text" => $_POST['value']
			];

			$comments = json_encode($notes);
			$stmt = $con->prepare("UPDATE $table SET admin_comments=? WHERE userID=?");
			$stmt->bind_param("si", $comments, $id);
			$success = $stmt->execute();
			$stmt->close();

			echo json_encode(["success" => $success, "data" => $notes]);
			break;
	}
	die();
}

switch ($_POST['choice']) {

	case "editClient":
		$stmt = $con->prepare("UPDATE client set client_first_name= ?, client_last_name = ?, client_company = ?, client_address = ?, client_city = ?, client_state = ?, client_postalcode = ?, client_country = ?, client_language = ?, client_phonenumber = ?, client_email = ?, client_status = ?, client_insurancenumber = ?, client_insurancecompany = ?, client_emergencycontact = ?, client_wheelchair = ?, updated_by = ?, updated_at = ? where clientID = ?");
		$cid = test_input($_POST['client-id']);
		$cfname = test_input($_POST['client-first-name']);
		$clname = test_input($_POST['client-last-name']);
		$ccompany = test_input($_POST['client-company']);
		$caddress = test_input($_POST['client-address']);
		$ccity = test_input($_POST['client-city']);
		$cstate = test_input($_POST['client-state']);
		$cpostalcode = test_input($_POST['client-postal-code']);
		$clanguage = test_input($_POST['client-language']);
		$cphone = test_input($_POST['client-phone']);
		$cemail = test_input($_POST['client-email']);
		$ccountry = test_input($_POST['client-country']);
		$cstatus = test_input($_POST['client-status']);
		$cinsurancenumber = test_input($_POST['client-insurance-number']);
		$cinsurancecompany = test_input($_POST['client-insurance-company']);
		$cemergencycontact = test_input($_POST['client-emergency-contact']);
		$cwheelchair = test_input($_POST['client-wheel-chair']);
		$updatedby = $_SESSION['Admin'];
		$updatedat = date('Y-m-d H:i:s');
		$stmt->bind_param("sssssssssssssssissi", $cfname, $clname, $ccompany, $caddress, $ccity, $cstate, $cpostalcode, $clanguage, $cphone, $cemail, $ccountry, $cstatus, $cinsurancenumber, $cinsurancecompany, $cemergencycontact, $cwheelchair, $updatedby, $updatedat, $cid);
		if ($stmt->execute()) {
			echo "Success";

		} else {
			echo $stmt->error;
		}
		break;

	case "EditBookings":
		$stmt = $con->prepare("UPDATE booking set bookingcode = ?, arrival_date = ?, arrival_by = ?, arrival_flight = ?, arrival_flight_time = ?, departure_date = ?, departure_by = ?, departure_flight = ?, departure_flight_time = ?, adults = ?, children = ?, enfants = ?, guest_names = ?, updated_by = ?, updated_at = ? where bookingID = ?");
		$bID = test_input($_POST['booking-id']);
		$bCode = test_input($_POST['booking-code']);
		$gNames = test_input($_POST['guest-names']);
		$adults = test_input($_POST['adults']);
		$children = test_input($_POST['children']);
		$enfants = test_input($_POST['enfants']);
		$baDate = test_input($_POST['booking-arrival-date']);
		$baBy = test_input($_POST['booking-arrival-by']);
		$baFlight = test_input($_POST['booking-arrival-flight']);
		$baTime = test_input($_POST['booking-arrival-time']);
		$bdDate = test_input($_POST['booking-departure-date']);
		$bdBy = test_input($_POST['booking-departure-by']);
		$bdFlight = test_input($_POST['booking-departure-flight']);
		$bdTime = test_input($_POST['booking-departure-time']);
		/*$aNote = test_input($_POST['administration-note']);
		$oNote = test_input($_POST['operation-note']);
		$acNote = test_input($_POST['accounting-note']);
		$rNote = test_input($_POST['representative-note']);*/
		$updatedby = $_SESSION['Admin'];
		$updatedat = date('Y-m-d H:i:s');
		$stmt->bind_param("sssssssssiiisssi", $bCode, $baDate, $baBy, $baFlight, $baTime, $bdDate, $bdBy, $bdFlight, $bdTime, $adults, $children, $enfants, $gNames, $updatedby, $updatedat, $bID);
		if ($stmt->execute()) {
			echo "Success";

		} else {
			echo $stmt->error;
		}
		break;

	case "EditUsers":
		$stmt = $con->prepare("update user set username = ?, loginattempts = ?, updated_by = ?, updated_at = ? where userID = ?");
		$uID = test_input($_POST['user-id']);
		$uName = test_input($_POST['user-name']);
		$lattempts = test_input($_POST['login-attempts']);
		$updatedby = $_SESSION['Admin'];
		$updatedat = date('Y-m-d H:i:s');
		$stmt->bind_param("sissi", $uName, $lattempts, $updatedby, $updatedat, $uID);
		if ($stmt->execute()) {
			echo "Success";

		} else {
			echo $stmt->error;
		}
		break;

	case "TeacherPersonalInfo":
		$teacherid = isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : $_POST['teacher-id'];
		$stmt = $con->prepare("SELECT t.profileID FROM teacher t WHERE t.userID=?");
		$stmt->bind_param('i', $teacherid);
		$stmt->bind_result($profileID);
		$stmt->execute();
		$stmt->fetch();
		$stmt->close();

		$flag = true;
		if (true) {//(isset($_POST['update-teacher'])){
			//$flag = false;
			$profile = NULL;
			if (empty($_POST['imgb'])) {
				if (!empty($_POST['current-image'])) {
					$profile = $_POST['current-image'];
				}
			} else {
				$name = round(microtime(true)) . uniqid() . ".png";
				$img = $_POST['imgb'];
				$img = str_replace('data:image/jpeg;base64,', '', $img);
				$img = base64_decode($img);
				$aws = new AWS();
				$aws->upload($name, $img);
				$profile = $name;
			}
			$stmt = $con->prepare("update profile set image = ? where profileID=?");
//			$profileID = test_input($_POST['profile-id']);

			$stmt->bind_param("si", $profile, $profileID);
			if ($stmt->execute()) {
				//echo "path : ".$profile;
				//echo " : : record updated...";
				//header('Location: ../edit-profile');

			} elseif (empty($profileID)) {
				$stmt->close();
//				$teacherid = isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : $_POST['teacher-id'];
				$approved = 1;
				$stmt1 = $con->prepare("INSERT INTO profile (image,approved) values (?,?)");
				$stmt1->bind_param("si", $profile, $approved);
				$stmt1->execute();
				$profileID = $stmt1->insert_id;
				$stmt1->close();
				$stmt2 = $con->prepare("UPDATE teacher set profileID = ? where userID = ?");
				$stmt2->bind_param("ii", $profileID, $teacherid);
				$stmt2->execute();

				//header('Location: ../edit-profile');
			}
		}
		if ($flag) {
			//print_r($_POST);
			//exit;
			$stmt = $con->prepare("update contact set firstname=?, lastname=?, dateofbirth=?,telephone=?, address=?, postalcode=?, city=? where userID = ?");
			$fname = test_input($_POST['first-name']);
			$lname = test_input($_POST['last-name']);
			$dob = test_input($_POST['dob']);
			$address = test_input(@$_POST['address']);
			$postalcode = test_input(@$_POST['postal-code']);
			$city = test_input(@$_POST['city']);
			$telephone = test_input($_POST['telephone']);
			$teacherid = test_input($_POST['teacher-id']);
			$stmt->bind_param("sssssssi", $fname, $lname, $dob, $telephone, $address, $postalcode, $city, $teacherid);
			$stmt->execute();
			/*************online course and accepting students************/
			$online_course = test_input($_POST['online_courses']);;
			$accept_new_std = test_input($_POST['acc_new_std']);;
			$stmt_cours_accept = $con->prepare("update teacher set onlineteaching=?, allownewstudents=? where userID = ?");
			$stmt_cours_accept->bind_param("iii", $online_course, $accept_new_std, $teacherid);
			if ($stmt_cours_accept->execute()) {
//				header('Location: ../edit-profile');
			}
			$stmt_cours_accept->close();
			if (!empty($_POST['travel_range'])) {
				$travelRange = test_input($_POST['travel_range']);
				$stmt_cours_accept = $con->prepare("update teacher set travelRange=? where userID = ?");
				$stmt_cours_accept->bind_param("di", $travelRange, $teacherid);
				$stmt_cours_accept->execute();
				$stmt_cours_accept->close();
			}

//			if ($stmt->execute()) {
			//header('Location: ../edit-profile');

//			} else {
			//header('Location: ../edit-profile');
//			}


			// update account-information inside personal information form
			if (empty(test_input($_POST['password'])) && $flag) {
				$stmt = $con->prepare("update user set username=?, email=? where userID = ?");
				$username = test_input($_POST['username']);
				$email = test_input($_POST['email']);
				$teacherid = test_input($_POST['teacher-id']);
				$stmt->bind_param("ssi", $username, $email, $teacherid);
				if ($stmt->execute()) {
					//header('Location: ../edit-profile');

				} else {
					//header('Location: ../edit-profile');
				}
			} else {
				$stmt = $con->prepare("update user set username=?, email=?, password = ? where userID = ?");
				$username = test_input($_POST['username']);
				$email = test_input($_POST['email']);
				$pass = test_input($_POST['password']);
				$password = md5($pass);
				$teacherid = test_input($_POST['teacher-id']);
				$stmt->bind_param("sssi", $username, $email, $password, $teacherid);
				if ($stmt->execute()) {
					//header('Location: ../edit-profile');

				} else {
					//header('Location: ../edit-profile');
				}
			}
		}
		/**************teacher profile info******************/
		if ($flag) {

			$stmt = $con->prepare("update profile set title=?, shortdescription=?, description=? where profileID=?");
			$sdesc = strip_tags($_POST['short-description']);
			$fdesc = strip_tags($_POST['full-description']);
			$title_profile = strip_tags($_POST['title_profile']);
			//echo "shor descritpion : ".$sdesc;
			//print_r($_REQUEST["imgb"]);

//			$profileID = test_input($_POST['profile-id']);

			$stmt->bind_param("sssi", $title_profile, $sdesc, $fdesc, $profileID);
			if ($stmt->execute()) {
				//echo "path : ".$profile;
				//echo " : : record updated...";
				//header('Location: ../edit-profile');

			} elseif (empty($profileID)) {
				$stmt->close();
//				$teacherid = isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : $_POST['teacher-id'];
				$approved = 1;

				$title_profile = test_input($_POST['title_profile']);
				$stmt1 = $con->prepare("INSERT INTO profile (title, shortdescription,description,approved) values (?,?,?)");
				$stmt1->bind_param("sssi", $title_profile, $sdesc, $fdesc, $approved);
				$stmt1->execute();
				$profileID = $stmt1->insert_id;
				$stmt1->close();
				$stmt2 = $con->prepare("UPDATE teacher set profileID = ? where userID = ?");
				$stmt2->bind_param("ii", $profileID, $teacherid);
				$stmt2->execute();

				//header('Location: ../edit-profile');
			}
		}
		/******************map update*******************/
		$stmt = $con->prepare("update contact set latitude=?, longitude=?, place_name=?, address=?, postalcode=?, city=?, country=? where userID = ?");
		$lat = test_input($_POST['lat']);
		$lng = test_input($_POST['lng']);
		$place_name = test_input($_POST['place_name']);
		$teacherid = test_input($_POST['teacher-id']);
		$address = test_input($_POST['straatnaam'] . " " . $_POST['huisnummer']);
		$postalCode = test_input($_POST['postalCode']);
		$city = test_input($_POST['woonplats']);
		$country = "Nederland";

		$stmt->bind_param("sssssssi", $lat, $lng, $place_name, $address, $postalCode, $city, $country, $teacherid);
		if ($stmt->execute()) {
			//header('Location: ../edit-profile');

		} else {
			//header('Location: ../edit-profile');
		}
		/************Application Info*********************/
		if ($flag) {
			$stmt = $con->prepare("update applications set IDdocument = ?, Educationdocument = ? where applicationID = ?");
			if (empty($_FILES['iddoc-image']['tmp_name'])) {
				if (@$_POST['current-image-id'] != "") {
					$profile = $_POST['current-image-id'];
				} else {
					$profile = NULL;
				}

			} else {
				$target_dir = "../../IDCards/";
				$target_file = $target_dir . basename($_FILES["iddoc-image"]["name"]);
				$file2 = basename($_FILES["iddoc-image"]["name"]);
				while (file_exists($target_file)) {
					$rand = mt_rand(1, 100);
					$target_file = $target_dir . $rand . "-" . basename($_FILES["iddoc-image"]["name"]);
					$file2 = $rand . "-" . basename($_FILES["iddoc-image"]["name"]);
				}
				if (move_uploaded_file($_FILES["iddoc-image"]["tmp_name"], $target_file)) {
					//unlink("../".$_POST['current-image']);
					$profile = "IDCards/" . $file2;
				}

			}

			if (empty($_FILES['educationdoc-image']['tmp_name'])) {
				if (!empty($_POST['current-image-education'])) {
					$profile1 = $_POST['current-image-education'];
				} else {
					$profile1 = NULL;
				}

			} else {
				$target_dir = "../../SchoolDocs/";
				$target_file = $target_dir . basename($_FILES["educationdoc-image"]["name"]);
				$file2 = basename($_FILES["educationdoc-image"]["name"]);
				while (file_exists($target_file)) {
					$rand = mt_rand(1, 100);
					$target_file = $target_dir . $rand . "-" . basename($_FILES["educationdoc-image"]["name"]);
					$file2 = $rand . "-" . basename($_FILES["educationdoc-image"]["name"]);
				}
				if (move_uploaded_file($_FILES["educationdoc-image"]["tmp_name"], $target_file)) {
					//unlink("../".$_POST['current-image']);
					$profile1 = "SchoolDocs/" . $file2;
				}

			}

			$appid = test_input(@$_POST['application-id']);
			$stmt->bind_param("ssi", $profile, $profile1, $appid);
			if ($stmt->execute()) {
				//	header('Location: ../edit-profile');

			} elseif (!empty($profileID)) {
				$stmt->close();
				$teacherid = isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : $_POST['teacher-id'];
				$approved = 1;
				$stmt1 = $con->prepare("INSERT INTO profile (shortdescription,description,image,approved) values (?,?,?,?)");
				$stmt1->bind_param("sssi", $sdesc, $fdesc, $profile, $approved);
				$stmt1->execute();
				$profileID = $stmt1->insert_id;
				$stmt1->close();
				$stmt2 = $con->prepare("UPDATE teacher set profileID = ? where userID = ?");
				$stmt2->bind_param("ii", $profileID, $teacherid);
				$stmt2->execute();

				//	header('Location: ../edit-profile');
			}
		}

		/*******************Teacher course info**************/
		if ($flag) {
			$done = 0;
			$teacherID = isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : $_POST['teacher-id'];
			$course = @$_POST['school-course'];
			$count_courses = empty($course) ? 0 : count($course);
			if ($done == 1) {
				if (isset($_SESSION['Teacher']))
					header('Location: ../edit-profile');
				else
					header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);
			} else {
				if (isset($_SESSION['Teacher']))
					header('Location: ' . $_SERVER['HTTP_REFERER']);
				else if (isset($_POST['teacher-id'])) {
					$link = "http";
					if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
						$link .= "s";
					$link .= "://";
					$link .= $_SERVER['HTTP_HOST'];
					$link .= "/app/Admin/admin_edit_tutor_profile.php?tid=" . $_POST['teacher-id'];
					header("Location: $link");
				} else
					header("Location: {$_SERVER['HTTP_REFERER']}?tid=" . $_POST['teacher-id']);
			}
		}
		break;

	case "TeacherCoursesInfo":
		if (true)//(isset($_POST['update-teacher']))
		{
			$done = 0;
			$teacherID = isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : $_POST['teacher-id'];
			$count_courses = count($_POST['school-course']);

			for ($i = 0; $i < $count_courses; $i++) {
				$course = $_POST['school-course'][$i];
				echo "INSERT INTO teacher_courses(teacherID,courseID)values($teacherID, $course)";
				$stmt10 = $con->prepare("INSERT INTO teacher_courses(teacherID,courseID)values(?,?)");
				$stmt10->bind_param("ii", $teacherID, $course);
				if ($stmt10->execute()) {
					$done = 1;
				} else {
					$done = 0;
				}
			}
			$stmt10->close();
			if ($done == 1) {
				if (isset($_SESSION['Teacher']))
					header('Location: ../edit-profile');
				else
					header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);
			} else {
				if (isset($_SESSION['Teacher']))
					header('Location: ../edit-profile');
				else
					header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);
			}
		}
		break;

	case "StudentPersonalInfo":

		if (isset($_POST['update-teacher'])) {
			$stmt = $con->prepare("update contact set firstname=?, lastname=?, telephone=? where userID = ? AND prmary = ?");
			$primary = 1;
			$fname = test_input($_POST['sfirst-name']);
			$lname = test_input($_POST['slast-name']);
			$telephone = test_input($_POST['stelephone']);
			$studentid = test_input($_POST['student-id']);
			$stmt->bind_param("sssii", $fname, $lname, $telephone, $studentid, $primary);
			if ($stmt->execute()) {
				//header('Location: ../student-edit-profile');
			}

			$password = test_input($_POST['password']);
			if (trim($password) != "") {
				$stmt = $con->prepare("update user set password=? where userID = ?");
				$password = md5(test_input($_POST['password']));
				$stmt->bind_param("si", $password, $studentid);
				if ($stmt->execute()) {
					//header('Location: ../student-edit-profile');
				}
			}

		}

		$stmt = $con->prepare("update contact set latitude=?, longitude=?, place_name=?, address=?, city=?, postalcode=?, country=? where userID = ?");
		$lat = test_input($_POST['lat']);
		$lng = test_input($_POST['lng']);
//		$place_name = test_input($_POST['place_name']);
		$street = test_input($_POST['street']);
		$houseNumber = test_input($_POST['housenumber']);
		$city = test_input($_POST['city']);
		$postCode = test_input($_POST['postcode']);
		$country = test_input($_POST['country']);
		$address = "$street $houseNumber";
		$place_name = "$street $houseNumber, $postCode $city $country";
		$studentid = test_input($_POST['student-id']);
		$stmt->bind_param("sssssssi", $lat, $lng, $place_name, $address, $city, $postCode, $country, $studentid);
		if ($stmt->execute()) {
			if (isset($_SESSION['Student']))
				header('Location: ../student-edit-profile');
			else if (isset($_POST['student-id'])) {
				$link = "http";
				if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
					$link .= "s";
				$link .= "://";
				$link .= $_SERVER['HTTP_HOST'];
				$link .= "/app/Admin/admin_edit_student_profile.php?sid=" . $_POST['student-id'];
				header("Location: $link");
			} else
				header('Location: ../student-edit-profile?sid=' . $_POST['student-id']);

		} else {

			if (isset($_SESSION['Student']))
				header('Location: ../student-edit-profile');
			else if (isset($_POST['student-id'])) {
				$link = "http";
				if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
					$link .= "s";
				$link .= "://";
				$link .= $_SERVER['HTTP_HOST'];
				$link .= "/app/Admin/admin_edit_student_profile.php?sid=" . $_POST['student-id'];
				header("Location: $link");
			} else
				header('Location: ../student-edit-profile?sid=' . $_POST['student-id']);
		}
		/***************student parent info...*************/
		if (isset($_POST['update-teacher'])) {
			//echo "here"; var_dump($_POST);exit;
			$stmt = $con->prepare("update contact set firstname=?, lastname=?, telephone=?,  email=? where userID = ? AND prmary = ?");
			$primary = 0;
			$fname = test_input($_POST['pfirst-name']);
			$lname = test_input($_POST['plast-name']);
			$pemail = test_input($_POST['pemail']);
			$telephone = test_input($_POST['ptelephone']);
			$studentid = test_input($_POST['student-id']);
			$stmt->bind_param("ssssii", $fname, $lname, $telephone, $pemail, $studentid, $primary);
			if ($stmt->execute()) {
				if (isset($_SESSION['Student']))
					header('Location: ../student-edit-profile');
				else if (isset($_POST['student-id'])) {
					$link = "http";
					if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
						$link .= "s";
					$link .= "://";
					$link .= $_SERVER['HTTP_HOST'];
					$link .= "/app/Admin/admin_edit_student_profile.php?sid=" . $_POST['student-id'];
					header("Location: $link");
				} else
					header('Location: ../student-edit-profile?sid=' . $_POST['student-id']);

			} else {
				if (isset($_SESSION['Student']))
					header('Location: ../student-edit-profile');
				else if (isset($_POST['student-id'])) {
					$link = "http";
					if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
						$link .= "s";
					$link .= "://";
					$link .= $_SERVER['HTTP_HOST'];
					$link .= "/app/Admin/admin_edit_student_profile.php?sid=" . $_POST['student-id'];
					header("Location: $link");
				} else
					header('Location: ../student-edit-profile?sid=' . $_POST['student-id']);
			}
		}
		break;

	case "StudentParentInfo":
		if (isset($_POST['update-teacher'])) {
			$stmt = $con->prepare("update contact set firstname=?, lastname=?, dateofbirth=?,telephone=?, address=?, postalcode=?, email=? where userID = ? AND prmary = ?");
			$primary = 0;
			$fname = test_input($_POST['pfirst-name']);
			$lname = test_input($_POST['plast-name']);
			//$dob = test_input($_POST['dob']);
			//$address = test_input($_POST['address']);
			//$postalcode = test_input($_POST['postal-code']);
			$email = test_input($_POST['pemail']);
			$telephone = test_input($_POST['ptelephone']);
			$studentid = test_input($_POST['student-id']);
			$stmt->bind_param("ssssii", $fname, $lname, $telephone, $email, $studentid, $primary);
			if ($stmt->execute()) {
				if (isset($_SESSION['Student']))
					header('Location: ../student-edit-profile');
				else if (isset($_POST['student-id'])) {
					$link = "http";
					if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
						$link .= "s";
					$link .= "://";
					$link .= $_SERVER['HTTP_HOST'];
					$link .= "/app/Admin/admin_edit_student_profile.php?sid=" . $_POST['student-id'];
					header("Location: $link");
				} else
					header('Location: ../student-edit-profile?sid=' . $_POST['student-id']);

			} else {
				if (isset($_SESSION['Student']))
					header('Location: ../student-edit-profile');
				else if (isset($_POST['student-id'])) {
					$link = "http";
					if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
						$link .= "s";
					$link .= "://";
					$link .= $_SERVER['HTTP_HOST'];
					$link .= "/app/Admin/admin_edit_student_profile.php?sid=" . $_POST['student-id'];
					header("Location: $link");
				} else
					header('Location: ../student-edit-profile?sid=' . $_POST['student-id']);
			}
		}
		break;

	case "TeacherAccountInfo":
		if (isset($_POST['update-teacher'])) {
			if (empty(test_input($_POST['password']))) {
				$stmt = $con->prepare("update user set username=?, email=? where userID = ?");
				$username = test_input($_POST['username']);
				$email = test_input($_POST['email']);
				$teacherid = test_input($_POST['teacher-id']);
				$stmt->bind_param("ssi", $username, $email, $teacherid);
				if ($stmt->execute()) {
					if (isset($_SESSION['Teacher']))
						header('Location: ../edit-profile');
					else
						header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);

				} else {
					if (isset($_SESSION['Teacher']))
						header('Location: ../edit-profile');
					else
						header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);
				}
			} else {
				$stmt = $con->prepare("update user set username=?, email=?, password = ? where userID = ?");
				$username = test_input($_POST['username']);
				$email = test_input($_POST['email']);
				$pass = test_input($_POST['password']);
				$password = md5($pass);
				$teacherid = test_input($_POST['teacher-id']);
				$stmt->bind_param("sssi", $username, $email, $password, $teacherid);
				if ($stmt->execute()) {
					if (isset($_SESSION['Teacher']))
						header('Location: ../edit-profile');
					else
						header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);

				} else {
					if (isset($_SESSION['Teacher']))
						header('Location: ../edit-profile');
					else
						header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);
				}
			}
		}
		break;

	case "StudentAccountInfo":
		if (isset($_POST['update-teacher'])) {

			$stmt = $con->prepare("update user set username=?, email=?, password = ? where userID = ?");
			$username = test_input($_POST['username']);
			$email = test_input($_POST['email']);
			$pass = test_input($_POST['password']);
			$password = md5($pass);
			$studentid = test_input($_POST['student-id']);
			$stmt->bind_param("sssi", $username, $email, $password, $studentid);
			if ($stmt->execute()) {
				if (isset($_SESSION['Student']))
					header('Location: ../student-edit-profile');
				else if (isset($_POST['student-id'])) {
					$link = "http";
					if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
						$link .= "s";
					$link .= "://";
					$link .= $_SERVER['HTTP_HOST'];
					$link .= "/app/Admin/admin_edit_student_profile.php?sid=" . $_POST['student-id'];
					header("Location: $link");
				} else
					header('Location: ../student-edit-profile?sid=' . $_POST['student-id']);

			} else {
				if (isset($_SESSION['Student']))
					header('Location: ../student-edit-profile');
				else if (isset($_POST['student-id'])) {
					$link = "http";
					if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on')
						$link .= "s";
					$link .= "://";
					$link .= $_SERVER['HTTP_HOST'];
					$link .= "/app/Admin/admin_edit_student_profile.php?sid=" . $_POST['student-id'];
					header("Location: $link");
				} else
					header('Location: ../student-edit-profile?sid=' . $_POST['student-id']);
			}
		}
		break;

	case "TeacherProfileInfo":
		if (true)//(isset($_POST['update-teacher']))
		{

			$stmt = $con->prepare("update profile set shortdescription=?, description=?, image = ? where profileID=?");
			$sdesc = test_input($_POST['short-description']);
			$fdesc = test_input($_POST['full-description']);
			if (empty($_FILES['profile-image']['tmp_name'])) {
				if ($_POST['current-image'] != "") {
					$profile = $_POST['current-image'];
				} else {
					$profile = NULL;
				}

			} else {
				$target_dir = "../../TeacherProfileImages/";
				$target_file = $target_dir . basename($_FILES["profile-image"]["name"]);
				$file2 = basename($_FILES["profile-image"]["name"]);
				while (file_exists($target_file)) {
					$rand = mt_rand(1, 100);
					$target_file = $target_dir . $rand . "-" . basename($_FILES["profile-image"]["name"]);
					$file2 = $rand . "-" . basename($_FILES["profile-image"]["name"]);
				}
				if (move_uploaded_file($_FILES["profile-image"]["tmp_name"], $target_file)) {
					//unlink("../".$_POST['current-image']);
					$profile = "TeacherProfileImages/" . $file2;
				}

			}
			$teacherid = test_input($_POST['profile-id']);
			$stmt->bind_param("sssi", $sdesc, $fdesc, $profile, $teacherid);
			if ($stmt->execute()) {
				if (isset($_SESSION['Teacher']))
					header('Location: ../edit-profile');
				else
					header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);

			} else {
				$stmt->close();
				$teacherid = isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : $_POST['teacher-id'];
				$approved = 1;
				$stmt1 = $con->prepare("INSERT INTO profile (shortdescription,description,image,approved) values (?,?,?,?)");
				$stmt1->bind_param("sssi", $sdesc, $fdesc, $profile, $approved);
				$stmt1->execute();
				$profileID = $stmt1->insert_id;
				$stmt1->close();
				$stmt2 = $con->prepare("UPDATE teacher set profileID = ? where userID = ?");
				$stmt2->bind_param("ii", $profileID, $teacherid);
				$stmt2->execute();

				if (isset($_SESSION['Teacher']))
					header('Location: ../edit-profile');
				else
					header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);
			}
		}

		break;

	case "TeacherApplicationInfo":

		if (isset($_POST['update-teacher'])) {
			$stmt = $con->prepare("update applications set IDdocument = ?, Educationdocument = ? where applicationID = ?");
			if (empty($_FILES['iddoc-image']['tmp_name'])) {
				if ($_POST['current-image-id'] != "") {
					$profile = $_POST['current-image-id'];
				} else {
					$profile = NULL;
				}

			} else {
				$target_dir = "../../IDCards/";
				$target_file = $target_dir . basename($_FILES["iddoc-image"]["name"]);
				$file2 = basename($_FILES["iddoc-image"]["name"]);
				while (file_exists($target_file)) {
					$rand = mt_rand(1, 100);
					$target_file = $target_dir . $rand . "-" . basename($_FILES["iddoc-image"]["name"]);
					$file2 = $rand . "-" . basename($_FILES["iddoc-image"]["name"]);
				}
				if (move_uploaded_file($_FILES["iddoc-image"]["tmp_name"], $target_file)) {
					//unlink("../".$_POST['current-image']);
					$profile = "IDCards/" . $file2;
				}

			}

			if (empty($_FILES['educationdoc-image']['tmp_name'])) {
				if ($_POST['current-image-education'] != "") {
					$profile1 = $_POST['current-image-education'];
				} else {
					$profile1 = NULL;
				}

			} else {
				$target_dir = "../../SchoolDocs/";
				$target_file = $target_dir . basename($_FILES["educationdoc-image"]["name"]);
				$file2 = basename($_FILES["educationdoc-image"]["name"]);
				while (file_exists($target_file)) {
					$rand = mt_rand(1, 100);
					$target_file = $target_dir . $rand . "-" . basename($_FILES["educationdoc-image"]["name"]);
					$file2 = $rand . "-" . basename($_FILES["educationdoc-image"]["name"]);
				}
				if (move_uploaded_file($_FILES["educationdoc-image"]["tmp_name"], $target_file)) {
					//unlink("../".$_POST['current-image']);
					$profile1 = "SchoolDocs/" . $file2;
				}

			}

			$appid = test_input($_POST['application-id']);
			$stmt->bind_param("ssi", $profile, $profile1, $appid);
			if ($stmt->execute()) {
				if (isset($_SESSION['Teacher']))
					header('Location: ../edit-profile');
				else
					header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);

			} else {
				$stmt->close();
				$teacherid = isset($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : $_POST['teacher-id'];
				$approved = 1;
				$stmt1 = $con->prepare("INSERT INTO profile (shortdescription,description,image,approved) values (?,?,?,?)");
				$stmt1->bind_param("sssi", $sdesc, $fdesc, $profile, $approved);
				$stmt1->execute();
				$profileID = $stmt1->insert_id;
				$stmt1->close();
				$stmt2 = $con->prepare("UPDATE teacher set profileID = ? where userID = ?");
				$stmt2->bind_param("ii", $profileID, $teacherid);
				$stmt2->execute();

				if (isset($_SESSION['Teacher']))
					header('Location: ../edit-profile');
				else
					header('Location: ../edit-profile?tid=' . $_POST['teacher-id']);
			}
		}
		break;

}
