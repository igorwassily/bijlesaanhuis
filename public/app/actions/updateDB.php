<?php

require_once('../includes/connection.php');

$query = mysqli_query($con, "SELECT * FROM teacher_courses WHERE courseID=25 OR courseID=26");
$success = 0;
$failed = 0;
$count = 0;
while ($row = mysqli_fetch_assoc($query)) {
	$year = $row['schoolyearID'];
	if (!empty($year)) {
		$years = explode(",", $year);
		$allowed = [11, 12, 13];
		if ($row['schoollevelID'] == 15) $allowed[] = 14;
		foreach ($years as $key => $value) {
			if (!in_array($value, $allowed)) {
				unset($years[$key]);
			}
		}
		if (empty($years)) continue;
		$year = implode(",", $years);
	}

	$sql = "INSERT INTO teacher_courses(teacherID,courseID,schoollevelID,schooltypeID,schoolyearID,permission)values({$row['teacherID']},32,{$row['schoollevelID']},{$row['schooltypeID']},'{$year}','{$row['permission']}')";
	$result = mysqli_query($con, $sql);
	if ($result === false) {
		$failed ++;
		echo "<pre>" . print_r($row, true) . print_r(mysqli_error($con), true) . "<br/>" . print_r($sql, true) . "</pre><hr/>";
	} else {
		$success ++;
	}
	$count ++;
}

if ($failed > 0)
	echo "Failed to update $failed of $count entries";
elseif ($success > 0)
	echo "Added $success entries (of $count)";
elseif ($count == 0)
	echo "Nothing to do...";
else
	echo "Something went wrong";
