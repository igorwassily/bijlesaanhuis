<?php
require_once('../includes/connection.php');

session_start();

$teacherID = -1;
if (isset($_SESSION['TeacherID'])) {
	$teacherID = $_SESSION['TeacherID'];
} else if (isset($_GET['tid'])) {
	$teacherID = $_GET['tid'];
}

if(isset($_POST['subpage'])){
	if($_POST['subpage']=="course"){
			$stype = (isset($_POST['typeID']))? $_POST['typeID'] : "";
			$slevel = (isset($_POST['levelID']))? $_POST['levelID'] : "";
			$syear = (isset($_POST['yearID']))? $_POST['yearID'] : "";


			$where = "";
			$s="";

			if($stype){
				$s = "";
				$arr = explode(",", $stype);
				foreach($arr as $a){
					$s .= " FIND_IN_SET($a, c.coursetype) OR";
				}
				$s = rtrim($s, "OR");
				$where .= " AND ($s) ";
			}


			if($slevel){
				$s = "";
				$arr = explode(",", $slevel);
				foreach($arr as $a){
					$s .= " FIND_IN_SET($a, c.courselevel) OR";
				}
				$s = rtrim($s, "OR");
				$where .= " AND ($s) ";
			}

			if($syear){
				$s = "";
				$arr = explode(",", $syear);
				foreach($arr as $a){
					$s .= " FIND_IN_SET($a, c.courseyear) OR";
				}
				$s = rtrim($s, "OR");
				$where .= " AND ($s) ";
			}

			$teacher="";
			if(isset($_SESSION['TeacherID']) && (isset($_POST['tid']) && $_POST['tid']!="false")){
				$teacher = " AND c.courseID NOT IN (SELECT tc.courseID FROM teacher_courses tc WHERE tc.teacherID={$_SESSION['TeacherID']}) ";
			}

			$str = "SELECT  c.* FROM courses c WHERE 1 $teacher $where  GROUP BY courseID ORDER BY c.coursename";
			$query = mysqli_query($con, $str); 

			//$options = "<option value='' style='display:none;' selected>Selecteer</option>";
			$options = "";
			while($d = mysqli_fetch_assoc($query)){
				$options .= "<option value='$d[courseID]'>$d[coursename]</option>";
			}

			echo $options;
	}// subpage==course
	
	
	elseif($_POST['subpage']=="courselevel"){
			// TODO MAKE THIS FUNCTION WORK FOR MORE THAN 1 SUBJECT (CID) / CURRENTLY IT CAN ONLY HANDLE ONE SUBJECT

			if(strpos($_POST['cid'], ",") != false){
				$_POST['cid'] = substr($_POST['cid'], 0, strpos($_POST['cid'], ","));
			}

			$str = "SELECT * FROM schoollevel sl, courses cc WHERE cc.courselevel=sl.levelID AND cc.schoolcourseID={$_POST['cid']}";
			$query = mysqli_query($con, $str);

			$options = "<option value='' style='display:none;' selected>Selecteer</option>";
			while($d = mysqli_fetch_assoc($query)){
				$options .= "<option value='$d[levelID]'>$d[levelName]</option>";
			}

			echo $options;
	}// subpage=="courselevel"  ends
	
	elseif($_POST['subpage']=="courselevelyear"){
	
		$str = "SELECT (SELECT GROUP_CONCAT(sy.yearName)sy FROM schoolyear sy WHERE FIND_IN_SET(sy.yearID, cc.courseyear))levelyears, cc.*, sl.* FROM schoollevel sl, courses cc WHERE cc.courselevel=sl.levelID AND cc.schoolcourseID={$_POST['cid']}";
			$query = mysqli_query($con, $str);

			$options = "";
			while($d = mysqli_fetch_assoc($query)){
				
				$options .= "<optgroup label='".$d['levelName']."'>";
				$yearlabels = explode(",", $d['levelyears']);
				$yearIds = explode(",", $d['courseyear']);
				for($i=0; $i<sizeof($yearlabels); $i++){
					$options .= "<option class='$d[levelName]' value='$d[levelID]_$yearIds[$i]'>$yearlabels[$i]</option>";
				}
				$options .= "</optgroup>";
			}

			echo $options;
	}// subpage=="courselevelyear"  ends
	
	elseif($_POST['subpage']=="teacherCourselevel"){
		
		 $str = "SELECT * FROM schoollevel sl, courses cc WHERE cc.courselevel=sl.levelID AND cc.schoolcourseID={$_POST['cid']} AND sl.levelID NOT IN (SELECT tc.schoollevelID FROM `courses` c, teacher_courses tc WHERE tc.courseID=c.schoolcourseID AND tc.schoollevelID=c.courselevel AND tc.courseID={$_POST['cid']} AND tc.teacherID={$teacherID}) ";
			$query = mysqli_query($con, $str);
			var_dump($_SESSION);

			$options = "<option value='' style='display:none;' selected>Selecteer</option>";
			while($d = mysqli_fetch_assoc($query)){
				$options .= "<option value='$d[levelID]'>$d[levelName]</option>";
			}

			echo $options;
	}// subpage=="teacherCourselevel"  ends
	
	elseif($_POST['subpage']=="teacherCourselevelyear"){
		
		 $str = "SELECT (SELECT GROUP_CONCAT(sy.yearName)sy FROM schoolyear sy WHERE FIND_IN_SET(sy.yearID, cc.courseyear))levelyears, sl.*, cc.* FROM schoollevel sl, courses cc WHERE cc.courselevel=sl.levelID AND cc.schoolcourseID={$_POST['cid']} AND sl.levelID NOT IN (SELECT tc.schoollevelID FROM `courses` c, teacher_courses tc WHERE tc.courseID=c.schoolcourseID AND tc.schoollevelID=c.courselevel AND tc.courseID={$_POST['cid']} AND tc.teacherID={$teacherID})";
			$query = mysqli_query($con, $str);

			$options = "";
			while($d = mysqli_fetch_assoc($query)){
				
				$options .= "<optgroup label='".$d['levelName']."'>";
				$yearlabels = explode(",", $d['levelyears']);
				$yearIds = explode(",", $d['courseyear']);
				for($i=0; $i<sizeof($yearlabels); $i++){
					$options .= "<option class='$d[levelName]' value='$d[levelID]_$yearIds[$i]'>$yearlabels[$i]</option>";
				}
				$options .= "</optgroup>";
			}

			echo $options;
	}// subpage=="teacherCourselevelyear"  ends
	
	elseif($_POST['subpage']=="courseyear"){
		// TODO MAKE THIS FUNCTION WORK FOR MORE THAN 1 SUBJECT (CID) / CURRENTLY IT CAN ONLY HANDLE ONE SUBJECT

		if(strpos($_POST['cid'], ",") != false){
			$_POST['cid'] = substr($_POST['cid'], 0, strpos($_POST['cid'], ","));
		}

		$str1 = "SELECT c.courseyear FROM `courses` c WHERE c.schoolcourseID={$_POST['cid']} AND c.courselevel={$_POST['clid']}";
		
		$r1 = mysqli_query($con, $str1);
		$d1 = mysqli_fetch_assoc($r1);
		
		$str = "SELECT * FROM schoolyear sy WHERE sy.yearID IN ({$d1['courseyear']})";
		
			$query = mysqli_query($con, $str);

			$options = "<option value='' style='display:none;' selected>Selecteer</option>";
			while($d = mysqli_fetch_assoc($query)){
				$options .= "<option value='$d[yearID]'>$d[yearName]</option>";
			}

			echo $options;
	}// subpage=="courseyear"  ends
	
	elseif($_POST['subpage']=="teachercourseyear"){
		
		 	$str1 = "SELECT tc.schoolyearID FROM `courses` c, teacher_courses tc WHERE tc.courseID=c.schoolcourseID AND tc.schoollevelID=c.courselevel AND tc.courseID={$_POST['cid']} AND tc.schoollevelID={$_POST['clid']} AND tc.teacherID={$_SESSION['TeacherID']}";
		
		$r1 = mysqli_query($con, $str1);
		$d1 = mysqli_fetch_assoc($r1);
		if($d1['schoolyearID']){
			$years = ($d1['schoolyearID'])? $d1['schoolyearID'] : "0"; 
			$str = "SELECT * FROM schoolyear sy WHERE sy.yearID NOT IN ($years)";
		}else{
			$str1 = "SELECT c.courseyear FROM `courses` c WHERE c.schoolcourseID={$_POST['cid']} AND c.courselevel={$_POST['clid']}";
			$r1 = mysqli_query($con, $str1);
			$d1 = mysqli_fetch_assoc($r1);
			
			$str = "SELECT * FROM schoolyear sy WHERE sy.yearID IN ({$d1['courseyear']})";
		}
		
		
			$query = mysqli_query($con, $str);

			$options = "<option value='' style='display:none;' selected>Selecteer</option>";
			while($d = mysqli_fetch_assoc($query)){
				$options .= "<option value='$d[yearID]'>$d[yearName]</option>";
			}

			echo $options;
	}// subpage=="teachercourseyear"  ends
	
}//isset(subpage) ends

?>