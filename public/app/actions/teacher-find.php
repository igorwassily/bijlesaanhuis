<?php

require_once('../includes/connection.php');

    if(isset($_REQUEST["term"])){
        // create prepared statement
        $stmt = $con->prepare("SELECT userID, username from user where active = ? AND username LIKE ?");
		$status = 1;
		$term = $_REQUEST["term"] . '%';
		$output = "<div class='list-group'>";
        // bind parameters to statement
        $stmt->bind_param("is", $status,$term);
        // execute the prepared statement
        $stmt->execute();
		$stmt->bind_result($userID,$username);
		$stmt->store_result();
		$count = $stmt->num_rows;
		
        if($count > 0){
            while($stmt->fetch()){
				
				$output .= 

						'<a href="reserve-slot.php?teacherid=' . $userID . '" class="list-group-item" style="color: white !important;">'.$username.'</a>';

					;
            }
			$output .= "</div>";
			echo $output;
        } else{
            echo "<p>No matches found</p>";
        }
    }
?>