<?php
$data = array();
require_once('../includes/connection.php');
$cat = $_POST['cat'];
switch ($cat) {
	case "SchoolType":
		$stmt = $con->prepare("SELECT * from schooltype ORDER BY typeName");
		$stmt->execute();
		$stmt->bind_result($ID, $Name);
		$stmt->store_result();
		$data = [];
		while ($stmt->fetch()) {
			$data[] = array(
				'ID' => strval($ID),
				'Name' => $Name
			);
		}
		echo json_encode($data);

		break;

	case "SchoolLevel":
		$stmt = $con->prepare("SELECT * from schoollevel");
		$stmt->execute();
		$stmt->bind_result($ID, $Name);
		$stmt->store_result();
		$data = array();
		while ($stmt->fetch()) {
			$data[] = array(
				'ID' => strval($ID),
				'Name' => $Name
			);
		}
		echo json_encode($data);

		break;
	case "SchoolYear":
		$stmt = $con->prepare("SELECT * from schoolyear");
		$stmt->execute();
		$stmt->bind_result($ID, $Name);
		$stmt->store_result();
		$data = array();
		while ($stmt->fetch()) {
			$data[] = array(
				'ID' => strval($ID),
				'Name' => $Name
			);
		}
		echo json_encode($data);

		break;

	case "SchoolCourse":
		$stmt = $con->prepare("SELECT courseID, coursename from schoolyear");
		$stmt->execute();
		$stmt->bind_result($ID, $Name);
		$stmt->store_result();
		$data = array();
		while ($stmt->fetch()) {
			$data[] = array(
				'ID' => strval($ID),
				'Name' => $Name
			);
		}
		echo json_encode($data);

		break;
}
?>