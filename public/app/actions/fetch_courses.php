<?php
$type = $_POST['type'];
switch ($type) {
	case "Elementary":
		$data = array();
		require_once('../includes/connection.php');
		$typeID = test_input($_POST['typeID']);
		$stmt = $con->prepare("SELECT courseID, coursename from courses where coursetype = ? AND courselevel IS NULL AND courseyear IS NULL");
		$stmt->bind_param("i", $typeID);
		$stmt->execute();
		$stmt->bind_result($courseID, $courseName);
		$stmt->store_result();
		$data[] = array(
			'courseID' => "",
			'courseName' => "Selecteer"
		);
		while ($stmt->fetch()) {
			$data[] = array(
				'courseID' => strval($courseID),
				'courseName' => $courseName
			);
		}
		echo json_encode($data);
		break;

	default:
		$data = array();
		require_once('../includes/connection.php');
		$typeID = test_input($_POST['typeID']);
		$levelID = test_input($_POST['levelID']);
		$yearID = test_input($_POST['yearID']);
		$stmt = $con->prepare("SELECT courseID, coursename from courses where coursetype = ? AND courselevel = ? AND courseyear = ?");
		$stmt->bind_param("iii", $typeID, $levelID, $yearID);
		$stmt->execute();
		$stmt->bind_result($courseID, $courseName);
		$stmt->store_result();
		$data[] = array(
			'courseID' => "",
			'courseName' => "Selecteer"
		);
		while ($stmt->fetch()) {
			$data[] = array(
				'courseID' => strval($courseID),
				'courseName' => $courseName
			);
		}
		echo json_encode($data);
		break;
}
?>