<?php
require_once('../includes/connection.php');
session_start();

$query_string = "";


$stype = (isset($_REQUEST['stype'])) ? mysqli_escape_string($con, $_REQUEST['stype']) : "";
$slevel = (isset($_REQUEST['slevel'])) ? mysqli_escape_string($con, $_REQUEST['slevel']) : "";
$syear = (isset($_REQUEST['syear'])) ? mysqli_escape_string($con, $_REQUEST['syear']) : "";
$scourse = (isset($_REQUEST['scourse'])) ? mysqli_escape_string($con, $_REQUEST['scourse']) : "";
$offset = (isset($_REQUEST['offset'])) ? mysqli_escape_string($con, $_REQUEST['offset']) : "";
$limit = (isset($_REQUEST['limit'])) ? mysqli_escape_string($con, $_REQUEST['limit']) : "";
$postCodeEmpty = (isset($_REQUEST['postCodeEmpty'])) ? $_REQUEST['postCodeEmpty'] : "";
$lat = (isset($_REQUEST['laat'])) ? mysqli_escape_string($con, $_REQUEST['laat']) : "";
$lng = (isset($_REQUEST['lnng'])) ? mysqli_escape_string($con, $_REQUEST['lnng']) : "";

$courseName = [];

$r = mysqli_query($con, "SELECT value FROM variables WHERE name='freeTravelRange'");
$r = mysqli_fetch_array($r);
$travelBoundary1 = $r[0];

if (isset($scourse) && !empty($scourse)) {

    $countCourses = explode(",", $scourse);
    if (sizeof($countCourses) == 1) {
        $query = "SELECT DISTINCT coursename FROM courses WHERE schoolcourseID =" . $scourse;
        $request = mysqli_query($con, $query);
        $result = mysqli_fetch_assoc($request);
        $courseName[] = $result['coursename'];
    } else if (sizeof($countCourses) > 1) {
        foreach ($countCourses as $course) {
            $query = "SELECT DISTINCT coursename FROM courses WHERE schoolcourseID =" . $course;
            $request = mysqli_query($con, $query);
            $result = mysqli_fetch_assoc($request);
            $courseName[] = $result['coursename'];
        }
    }


}


$where = "";
$s = "";
$operator = "AND";


if (!empty(trim($stype))) {
    $where .= " AND crs.coursetype = {$stype}";
}

if (!empty(trim($slevel))) {
    $where .= " AND crs.courselevel = {$slevel}";
}

if (!empty(trim($syear))) {
    $where .= " AND CONCAT(',', tcrs.schoolyearID, ',') LIKE '%,{$syear},%'";
}

if (!empty(trim($scourse))) {
    $subjects = explode(",", $scourse);
    $placeholder = "";

    foreach ($subjects as $sub) {
        if (!empty($placeholder)) {
            $placeholder .= " OR ";
        }
        $placeholder .= "crs.schoolcourseID = {$sub}";
    }

    $where .= " AND " . $placeholder;

}

/*
if($slevel){
	$s = "";
	$arr = explode(",", $slevel);
	foreach($arr as $a){
	    if($stype == 9){
		    $s .= " FIND_IN_SET(0, crs.courselevel) $operator";
	    } else{
		    $s .= " FIND_IN_SET($a, crs.courselevel) $operator";
        }

	}
	$s = rtrim($s, "'$operator'");
	if($s)
		$where .= " AND ($s) ";
}

if(trim($syear) && $stype != 9){
	$s = "";
	$arr = explode(",", $syear);
	foreach($arr as $a){
		$s .= " FIND_IN_SET($a, crs.courseyear) $operator";
	}
	$s = rtrim($s, "'$operator'");
	if($s)
		$where .= " AND ($s) ";
}

if(trim($scourse)){
		//$scourse = trim($scourse)
		$s = " FIND_IN_SET(crs.schoolcourseID, '$scourse') ";

		$where .= " AND ($s) ";
}
*/

$oldTeacherStr = " (null) oldTeacher ";

//var_dump($_SESSION);


if (isset($_SESSION['StudentID'])) {
    $oldTeacherStr = "(SELECT 1 FROM calendarbooking _cb WHERE t.userID=_cb.teacherID AND _cb.studentID=$_SESSION[StudentID] LIMIT 1)oldTeacher ";
}

// MORE THAN FIRST 10
function checkIfThereAreMoreResults($con, $q1, $previousOffset)
{
    $more = false;
    //var_dump($q1);

    // FIND OUT THE PREV. OFFSET
    $offset = $previousOffset + 10;


    // CHANGE THE OFFSET
    $newQuery = substr($q1, 0, strlen($q1) - 1);
    $newQuery .= $offset;

    //RESULTING
    $result = mysqli_query($con, $newQuery);

    //CHECKING
    $more = (mysqli_num_rows($result) > 0 ? true : false);


    return $more;
}

function contains($needles, $haystack)
{
    return count(array_intersect($needles, explode(",", $haystack)));
}

function reSortResults($result, $courseName)
{
    $res = [];
    $resComplete = [];
    $resMedium = [];
    $resElse = [];
    $selected = $courseName;

    if (!empty($selected) && sizeof($selected) >= 2) {
        while ($row = mysqli_fetch_assoc($result)) {

            $i = contains($selected, $row['courses_name']);
            // echo ($i) ? "found ($i)" : "not found";


            if ($i == sizeof($selected)) {
                // FIRST ORDER RESULTS
                $resComplete[] = $row;

            } else if ($i == sizeof($selected) - 1) {
                $resMedium[] = $row;
            } else {
                $resElse[] = $row;
            }
        }

        $res[] = $resComplete;
        $res[] = $resMedium;
        $res[] = $resElse;
    } else {
        // Do nothing return all normal results
        while ($row = mysqli_fetch_assoc($result)) {
            $res[][] = $row;
        }
    }

    return $res;
}

function debugOutput($data)
{
    echo "<br>";
    echo "<br>";
    echo "<pre>" . print_r($data, true) . "</pre>";
    echo "<br>";
    echo "<br>";
}

function subjectsTaughtForType($schoolType, $con)
{
    $subjects = [];
    if (isset($schoolType) && !empty($schoolType)) {
        $sql = "SELECT sc.schoolcourseID, c.coursename
FROM schoolcourse sc,
     courses c
WHERE sc.schoolcourseID = c.schoolcourseID
  AND c.coursetype = $schoolType
GROUP BY sc.schoolcourseID
ORDER BY c.coursename";


        $r1 = mysqli_query($con, $sql);

        while ($row = mysqli_fetch_assoc($r1)) {

            $subjects[] = $row['coursename'];
        }
    }

    return $subjects;
}

// REMOVE FOR
/*
$q1 = "Select d.distance, $oldTeacherStr,
c.*, IF(CEIL(distance) > $travelBoundary2, 2, IF(CEIL(distance) <= t.servicearearange, 0, 1)) AS cat,
(SELECT COUNT(DISTINCT slot_group_id)class FROM `calendarbooking` cb where isClassTaken = 0 AND t.userID = cb.teacherID GROUP BY teacherID)completedLessons,
(SELECT GROUP_CONCAT(DISTINCT(crss.coursename)) FROM courses crss, teacher_courses tcc WHERE tcc.courseID=crss.schoolcourseID AND tcc.teacherID=c.userID)courses_name
,COUNT(crs.courseID)courses_count,tlr.rate,tlr.level, tlr.cost_per_km, tcrs.permission, u.active, p.image,p.shortdescription, p.title, t.onlineteaching, t.servicearearange, t.allownewstudents
 from 
 (SELECT 111.045 * DEGREES(ACOS(LEAST(COS(RADIANS(" . $_REQUEST['laat'] . ")) * COS(RADIANS(cc.latitude)) * COS(RADIANS(" . $_REQUEST['lnng'] . " - cc.longitude)) + SIN(RADIANS(" . $_REQUEST['laat'] . ")) * SIN(RADIANS(cc.latitude)), 1.0) )) AS distance, cc.userID FROM contact cc WHERE cc.contacttypeID=1 GROUP BY cc.userID) AS d,
 
 courses crs, teacher_courses tcrs,teacher t, `user` u, contact c, `profile` p, teacherlevel_rate tlr where t.userID = u.userID AND c.userID = u.userID AND u.userID = tcrs.teacherID AND tcrs.permission='Accepted' AND u.active = 1 AND crs.schoolcourseID = tcrs.courseID AND p.profileID=t.profileID AND t.teacherlevel_rate_ID = tlr.ID AND c.contacttypeID IN (1) AND t.onlineteaching IN( 1, if(d.distance <= $travelBoundary2, 0, '-1' ) ) AND d.userID=t.userID $where
 GROUP BY c.userID  ORDER BY cat, d.distance ASC LIMIT $_POST[limit] OFFSET " . $_POST['offset'];
*/


$q1 = "Select DISTINCT sql_calc_found_rows 
       $oldTeacherStr,
       c.firstname,
       c.lastname,
       c.*,
       (SELECT COUNT(DISTINCT slot_group_id) class
        FROM `calendarbooking` cb
        where isClassTaken = 0
          AND t.userID = cb.teacherID
        GROUP BY teacherID) completedLessons,
       (SELECT GROUP_CONCAT(DISTINCT (crss.coursename))
        FROM courses crss,
             teacher_courses tcc
        WHERE tcc.courseID = crss.schoolcourseID
          AND tcc.teacherID = c.userID) courses_name,
       @d:= 111.045 * DEGREES(ACOS(LEAST(
                       COS(RADIANS($lat)) * COS(RADIANS(c.latitude)) * COS(RADIANS($lng - c.longitude)) +
                       SIN(RADIANS($lat)) * SIN(RADIANS(c.latitude)), 1.0))) AS distance,
       IF(CEIL(@d) > t.travelRange, 2, IF(CEIL(@d) <= $travelBoundary1, 0, 1)) AS cat,
       tlr.rate,
       tlr.level,
       tlr.cost_per_km,
       tcrs.permission,
       u.active,
       p.image,
       p.shortdescription,
       p.title,
       t.onlineteaching,
       t.travelRange,
       t.allownewstudents
FROM contact c
         JOIN teacher t ON c.userID = t.userID
         JOIN teacher_courses tcrs ON tcrs.teacherID = t.userID
         JOIN courses crs ON crs.schoolcourseID = tcrs.courseID
         JOIN teacherlevel_rate tlr ON tlr.ID = t.teacherlevel_rate_ID
         LEFT JOIN profile p ON p.profileID = t.profileID
         JOIN user u on c.userID = u.userID
WHERE
  tcrs.permission = 'Accepted'
  AND u.active = 1
  AND c.contacttypeID = 1
 {$where}
HAVING  t.onlineteaching IN (1, if(distance < t.travelRange, 0, '-1'))
ORDER BY cat, distance ASC LIMIT  {$limit} OFFSET {$offset};
";

$_SESSION["std_lat"] = $lat;
$_SESSION["std_lng"] = $lng;
$_SESSION["std_place_name"] = $_REQUEST['placeName'];
$r1 = mysqli_query($con, $q1);
$moreResults = mysqli_query($con, "SELECT found_rows()");
$moreResults = mysqli_fetch_array($moreResults);
$moreResults = ($moreResults[0] > $offset + $limit);


//$moreResults = checkIfThereAreMoreResults($con, $q1, $offset);

$results = reSortResults($r1, $courseName);
// var_dump(count($results));
//  debugOutput($results);

$subjectsTaught = subjectsTaughtForType($stype, $con);


$flag = true;
$split = true;
$decide = false;
$firstTime = true;
$showSeperator = true;
$c = $d = 0;

if ($r1) { ?>
    <section class="w-100">
        <div class="">
            <div class="row" style="padding:0 38px;">
                <input type="hidden" id="moreResultsChecker" value="<?php echo($moreResults == true ? 1 : 0); ?>">
                <?php //debugOutput($results);

                foreach ($results as $key => $value) {
                    if (is_array($value)) {
                        foreach ($value as $subkey => $row) {

                            /**
                             * Small fix for default images
                             */
                            $row['image'] = str_replace('../assets/', '/app/assets/', $row['image']);


                            $travelBoundary2 = $row['travelRange'];

                            if (!isset($_SESSION['StudentID']) && $row["allownewstudents"] == 0)
                                continue;

                            $d = ceil((float)$row['distance']);
                            $d = ($d < 0) ? 0 : $d;
                            $c = ($d >= $travelBoundary1 && $d <= $travelBoundary2) ?
                                ($d - $travelBoundary1) * $row['cost_per_km'] : 0;
                            $d = ($d >= $travelBoundary1 && $d <= $travelBoundary2) ?
                                ($d - $travelBoundary1) : 0;
                            //  var_dump($row);
                            if ($row['distance'] <= $travelBoundary1) {       // <= 4 => Category 1 [BIKE]


                                ?>

                                <div class="container tutor__profile">
                                    <div class="row">
                                        <div class="col-md-3 col-lg-2">
                                            <img class="img-responsive center-block round__off"
                                                 src="<?php echo(($row['image']) ? substr($row['image'], 0) : constant("DEFAULT_PIC")); ?>"
                                                 class="img-thumbnail border-0"/>
                                        </div>
                                        <div class="col-md-6 col-lg-7">
                                            <div class="text-left">
                                                <div class="pt-4 pb-2">
                                                    <h4 class="text-primary"><?php echo $row['firstname'] . '&nbsp'; ?>

                                                        <div class="_level-md">
                                                            <span><?php echo $row['level']; ?> docent</span>

                                                            <?php if ($row['level'] == 'Senior') { ?>
                                                                <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent, vaak door middel van een gerelateerde studie, midden in de stof zit. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden.">&#xf05a;</i></sup>
                                                            <?php } else if ($row['level'] == 'Junior') { ?>
                                                                <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent erg dicht bij de leerling staat en goed weet waar de struikelblokken liggen. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. ">&#xf05a;</i></sup>
                                                            <?php } else if ($row['level'] == 'Supreme') { ?>
                                                                <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Voor <?php echo $row['level']; ?> docenten garanderen wij onze hoogst mogelijke kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent een jarenlange ervaring heeft in het geven van onderwijs. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van de best mogelijke didactische vaardigheden en vakspecifieke kennis.">&#xf05a;</i></sup>
                                                            <?php } ?>

                                                        </div>
                                                    </h4>
                                                    <div class="card-text">
                                                        <span class="card-text"><p
                                                                    class="card-title"><?php echo $row['title']; ?></p></span>
                                                        <span class="card-text card-des"><?php echo($row['shortdescription'] ? (substr($row['shortdescription'], 0, 180)) : "Stuur mij een bericht voor meer informatie."); ?></span>
                                                        <!-- <span class="custom_info_box"><?php //echo number_format((float)$row['distance'], 2, '.', '').' km Distant';
                                                        ?></span>&nbsp;&nbsp; -->
                                                        <hr id='split-line' class="separator__line"/>
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-5 col-sm-6 center__alignment">
											<span class="online__tutoring">
												<a href="/online-bijles-uitleg" target="_blank">
													<i class="fas online__icon tippy" data-toggle="tooltip"
                                                       data-placement="bottom"
                                                       data-tippy-content='Deze docent is beschikbaar voor online bijles. Meer weten, klik <a href="/online-bijles-uitleg" target="_blank">hier</a>!'>&#xf109;</i>
												</a>
                                                <!--Online videobijles : <?php echo ($row["onlineteaching"]) ? "Ja" : "Nee" ?>-->
											</span>
                                                                    <!--
                                                                    <span class="travelling__tutor">
                                                                        <a href="/reiskosten" target="_blank">
                                                                            <i class="fas travel__icon" data-toggle="tooltip" data-placement="left" title="Gezien je bijleslocatie, betaal je voor deze docent reiskosten. Meer weten? Kik hier!">&#xf206;</i>
                                                                        </a>
                                                                    </span>
                                                                    -->

                                                                    <i class="fas fa-home home__icon tippy"
                                                                       data-toggle="tooltip" data-placement="bottom"
                                                                       data-tippy-content="Deze bijlesdocent komt bij jou aan huis."></i>
                                                                </div>
                                                                <div class="col-md-7 col-sm-6">
											<span class="custom_info_box">


												<?php /*$crs = explode(",", $row['courses_name']);
													$count = count($crs);
													$subject = "";
													for($i=0; $i<$count; $i++){ if($i > 1){ $subject .= " en meer "; break;} $subject .= $crs[$i].", "; } echo $subject; */


                                                $teacherCourses = explode(",", $row['courses_name']);

                                                $notTeached = array_diff($teacherCourses, $courseName);
                                                $teached = array_intersect($courseName, $teacherCourses);   // Schnittmenge
                                                // var_dump($notTeached);
                                                /*
                                                echo "Teached <br>";
                                                echo "<pre>" . print_r($teached, true) . "</pre>";
                                                echo "<br>";

                                                echo "Not Teached <br>";
                                                echo "<pre>" . print_r($notTeached, true) . "</pre>";
 */
                                                //    var_dump($teached);

                                                // SCRATCH AWAY FROM notTeached and teached

                                                if (!empty($subjectsTaught)) {
                                                    $notTeached = array_intersect($subjectsTaught, $notTeached);

                                                    $teached = array_intersect($subjectsTaught, $teached);
                                                }


                                                $outPutString = "";
                                                $counter = 0;
                                                $wordCounter = 0;
                                                $highlighted = false;


                                                // Teached things highlights
                                                foreach ($teached as $teach) {
                                                    $counter++;
                                                    if ($counter == 3) {
                                                        break;
                                                    } else if ($wordCounter <= 3) {

                                                        $outPutString .= "<b>" . $teach . "</b>" . ", ";
                                                        $wordCounter++;
                                                    } else {
                                                        break;
                                                    }
                                                }


                                                // SHOW 2 NON HIGHLIGHTED TOO
                                                $counter = 0;
                                                foreach ($notTeached as $noteach) {
                                                    $counter++;
                                                    if ($counter == 3) {
                                                        break;
                                                    } else if ($wordCounter <= 3) {
                                                        $outPutString .= $noteach . ", ";
                                                        $wordCounter++;
                                                    } else {
                                                        break;
                                                    }
                                                }

                                                echo substr($outPutString, 0, strlen($outPutString) - 2) . " en meer";

                                                ?>
											</span>

                                                                    <?php //echo ($row["allownewstudents"])? " " : "<span class='custom_info_box' style='background:#f8d7da'> Alleen beschikbaar voor mijn huidige bijlesleerlingen</span>"
                                                                    ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 partition">
                                            <div class="pt-4 pb-2 _level_container">
                                                <hr id="split-line" class="partition__separator__line"/>

                                                <!-- <span style='background-color:#dceef1; font-size:14px;' class="badge">type docent : <#?php echo $row["level"]; ?></span>-->
                                                <!--
                                                <div class="_level">  <#?php echo $row["level"]; ?> docent
                                                    <sup><i class="fa alignment" data-toggle="tooltip" data-placement="right" title="Voor de specificaties van onze verschillende typen docenten verwijzen we graag naar de prijzenpagina."></i></p></sup>
                                                </div>
                                                -->

                                                <div class="_level docent_level_bottom <?php echo strtolower($row['level']); ?>">
                                                    <span><?php echo $row['level']; ?> docent</span>

                                                    <?php if ($row['level'] == 'Senior') { ?>
                                                        <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent, vaak door middel van een gerelateerde studie, midden in de stof zit. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden.">&#xf05a;</i></sup>
                                                    <?php } else if ($row['level'] == 'Junior') { ?>
                                                        <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent erg dicht bij de leerling staat en goed weet waar de struikelblokken liggen. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. ">&#xf05a;</i></sup>
                                                    <?php } else if ($row['level'] == 'Supreme') { ?>
                                                        <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Voor <?php echo $row['level']; ?> docenten garanderen wij onze hoogst mogelijke kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent een jarenlange ervaring heeft in het geven van onderwijs. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van de best mogelijke didactische vaardigheden en vakspecifieke kennis.">&#xf05a;</i></sup>
                                                    <?php } ?>

                                                </div>

                                                <!--<p class="mt-25" style="font-weight:bold;"><?php //echo "€".$row['rate']."/hr";
                                                ?></p>-->
                                                <!--<small><span style='background-color:#007b5e; font-size:14px;' class="badge"><?php //echo ($row["completedLessons"])?$row["completedLessons"] : "0"
                                                ?></span> completed lessons</small>-->
                                                <div class="_level tutor__profile__course-price">
                                                    <small><?php echo number_format($row['rate'], 2, ".", ","); ?>€ per
                                                        lesuur*</small><br/>
                                                </div>
                                                <div class="align__bottom pb-2">
                                                    <a href="./teacher-detailed-view.php?tid=<?php echo $row['userID']; ?><?php echo "&c=$c&d=$d"; ?>"
                                                       name="reserve-slot" onclick="saveToStorage();">
                                                        <button class="custom__btn hover__effect f-p"> BEKIJK PROFIEL
                                                        </button>
                                                    </a>
                                                    <a href="./teacher-detailed-view.php?tid=<?php echo $row['userID']; ?><?php echo "&c=$c&d=$d"; ?>#avail-calendar"
                                                       name="reserve-slot">
                                                        <button class="custom__btn margin__top__five calendar-transition f-p">
                                                            BEKIJK BESCHIKBAARHEID
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $decide = true;
                                $showSeperator = true; ?>

                            <?php } else if ($row['distance'] > $travelBoundary1 && $row['distance'] <= $travelBoundary2) {     // CATEGORY 2 [4-12km]
                                if ($showSeperator) {
                                    echo "
					<div class='container margin-top'>
						<div class='row outline center__alignment travel__separator'>
							<div class='col-md-2 neg-margin'>
								<i class='fas travel__icon__large'>&#xf206;</i>	
							</div>
							<div class='col-md-10'>
								<h1 class='text-primary-bold' style='padding-top: 2.5rem'>Onderstaande docenten wonen iets verder weg, waardoor reiskosten van toepassing zijn.</h1>
							</div>
						</div>
					</div>
					<div class='container'><div class='row'><hr id='split-line' class='separator__line'/></div></div>";
                                    $showSeperator = false;
                                }
                                ?>
                                <div class="container tutor__profile">
                                    <div class="row">
                                        <div class="col-md-3 col-lg-2">
                                            <img class="img-responsive center-block round__off"
                                                 src="<?php echo(($row['image']) ? substr($row['image'], 0) : constant("DEFAULT_PIC")); ?>"
                                                 class="img-thumbnail border-0"/>
                                        </div>
                                        <div class="col-md-6 col-lg-7">
                                            <div class="text-left">
                                                <div class="pt-4 pb-2">
                                                    <h4 class="text-primary"><?php echo $row['firstname'] . '&nbsp'; ?>
                                                        <div class="_level-md">
                                                            <span><?php echo $row['level']; ?> docent</span>

                                                            <?php if ($row['level'] == 'Senior') { ?>
                                                                <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent, vaak door middel van een gerelateerde studie, midden in de stof zit. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden.">&#xf05a;</i></sup>
                                                            <?php } else if ($row['level'] == 'Junior') { ?>
                                                                <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent erg dicht bij de leerling staat en goed weet waar de struikelblokken liggen. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. ">&#xf05a;</i></sup>
                                                            <?php } else if ($row['level'] == 'Supreme') { ?>
                                                                <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Voor <?php echo $row['level']; ?> docenten garanderen wij onze hoogst mogelijke kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent een jarenlange ervaring heeft in het geven van onderwijs. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van de best mogelijke didactische vaardigheden en vakspecifieke kennis.">&#xf05a;</i></sup>
                                                            <?php } ?>

                                                        </div>
                                                    </h4>
                                                    <div class="card-text">
                                                        <span class="card-text"><p
                                                                    class="card-title"><?php echo $row['title']; ?></p></span>
                                                        <span class="card-text card-des"><?php echo($row['shortdescription'] ? (substr($row['shortdescription'], 0, 180)) : "Stuur mij een bericht voor meer informatie."); ?></span>
                                                        <!-- <span class="custom_info_box"><?php //echo number_format((float)$row['distance'], 2, '.', '').' km Distant'; ?></span>&nbsp;&nbsp; -->
                                                        <hr id='split-line' class="separator__line"/>
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-5 col-sm-6 center__alignment">
											<span class="online__tutoring">	
												<a href="/online-bijles-uitleg" target="_blank">
													<i class="fas online__icon tippy" data-toggle="tooltip"
                                                       data-placement="bottom"
                                                       data-tippy-content='Deze docent is beschikbaar voor online bijles. Meer weten, klik <a href="/online-bijles-uitleg" target="_blank">hier</a>!'>&#xf109;</i>
												</a>
											</span>
                                                                    <span class="travelling__tutor">
												<a href="/reiskosten" target="_blank">
													<i class="fas travel__icon tippy" data-toggle="tooltip"
                                                       data-placement="bottom"
                                                       data-tippy-content='Gezien je bijleslocatie, betaal je voor deze docent €<?php echo $c ? $c : ''; ?> reiskosten. Meer weten? Kik <a href="/reiskosten" style="color:#FFBD6B !important;" target="_blank">hier</a>!'>&#xf206;</i>
												</a>
											</span>
                                                                    <i class="fas fa-home home__icon tippy"
                                                                       data-toggle="tooltip" data-placement="bottom"
                                                                       data-tippy-content="Deze bijlesdocent komt bij jou aan huis."></i>
                                                                </div>
                                                                <div class="col-md-7 col-sm-6">
											<span class="custom_info_box">
												<?php /*$crs = explode(",", $row['courses_name']);
												$count = count($crs);
												$subject = "";
												for($i=0; $i<$count; $i++){ if($i > 1){ $subject .= " en meer "; break;} $subject .= $crs[$i].", "; } echo $subject;*/


                                                $teacherCourses = explode(",", $row['courses_name']);

                                                $notTeached = array_diff($teacherCourses, $courseName);
                                                $teached = array_intersect($courseName, $teacherCourses);   // Schnittmenge
                                                // var_dump($notTeached);
                                                /*
                                                echo "Teached <br>";
                                                echo "<pre>" . print_r($teached, true) . "</pre>";
                                                echo "<br>";

                                                echo "Not Teached <br>";
                                                echo "<pre>" . print_r($notTeached, true) . "</pre>";
 */
                                                //    var_dump($teached);

                                                if (!empty($subjectsTaught)) {
                                                    $notTeached = array_intersect($subjectsTaught, $notTeached);

                                                    $teached = array_intersect($subjectsTaught, $teached);
                                                }


                                                $outPutString = "";
                                                $counter = 0;
                                                $wordCounter = 0;
                                                $highlighted = false;


                                                // Teached things highlights
                                                foreach ($teached as $teach) {
                                                    $counter++;
                                                    if ($counter == 3) {
                                                        break;
                                                    } else if ($wordCounter <= 3) {

                                                        $outPutString .= "<b>" . $teach . "</b>" . ", ";
                                                        $wordCounter++;
                                                    } else {
                                                        break;
                                                    }
                                                }

                                                // SHOW 2 NON HIGHLIGHTED TOO
                                                $counter = 0;
                                                foreach ($notTeached as $noteach) {
                                                    $counter++;
                                                    if ($counter == 3) {
                                                        break;
                                                    } else if ($wordCounter <= 3) {
                                                        $outPutString .= $noteach . ", ";
                                                        $wordCounter++;
                                                    } else {
                                                        break;
                                                    }
                                                }

                                                echo substr($outPutString, 0, strlen($outPutString) - 2) . " en meer";

                                                ?>
											</span>
                                                                    <?php //echo ($row["allownewstudents"])? " " : "<span class='custom_info_box' style='background:#f8d7da'> Alleen beschikbaar voor mijn huidige bijlesleerlingen</span>" ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 partition tutor__profile-traveller">
                                            <div class="pt-4 pb-2 _level_container">
                                                <hr id="split-line" class="partition__separator__line"/>

                                                <!--<span style='background-color:#dceef1; font-size:14px;' class="badge">type docent : <#?php echo $row["level"]; ?></span>-->
                                                <!--
                                                <div class="_level">  <#?php echo $row["level"]; ?> docent
                                                    <sup><i class="fa alignment" data-toggle="tooltip" data-placement="right" title="Voor de specificaties van onze verschillende typen docenten verwijzen we graag naar de prijzenpagina."></i></sup>
                                                </div>
                                                -->
                                                <div class="_level docent_level_bottom <?php echo strtolower($row['level']); ?>">
                                                    <span><?php echo $row['level']; ?> docent</span>

                                                    <?php if ($row['level'] == 'Senior') { ?>
                                                        <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent, vaak door middel van een gerelateerde studie, midden in de stof zit. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden.">&#xf05a;</i></sup>
                                                    <?php } else if ($row['level'] == 'Junior') { ?>
                                                        <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent erg dicht bij de leerling staat en goed weet waar de struikelblokken liggen. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. ">&#xf05a;</i></sup>
                                                    <?php } else if ($row['level'] == 'Supreme') { ?>
                                                        <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Voor <?php echo $row['level']; ?> docenten garanderen wij onze hoogst mogelijke kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent een jarenlange ervaring heeft in het geven van onderwijs. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van de best mogelijke didactische vaardigheden en vakspecifieke kennis.">&#xf05a;</i></sup>
                                                    <?php } ?>

                                                </div>
                                                <!--<p class="mt-25" style="font-weight:bold;"><#?php //echo "€".$row['rate']."/hr"; ?></p>-->
                                                <!--<small><span style='background-color:#007b5e; font-size:14px;' class="badge"><#?php //echo ($row["completedLessons"])?$row["completedLessons"] : "0" ?></span> completed lessons</small>-->
                                                <div class="_level tutor__profile__course-price">
                                                    <small><?php echo number_format($row['rate'], 2, ".", ","); ?>€ per
                                                        lesuur*</small><br/>
                                                </div>
                                                <?php
                                                if ($c > 0) {
                                                    ?>
                                                    <div class="_level tutor__profile__travel-cost">
                                                        <small> + <?php echo $c; ?>€ travel costs</small>
                                                    </div>
                                                    <?php
                                                }
                                                ?>
                                                <div class="align__bottom pb-2" style="justify-content:space-between;">
                                                    <a href="./teacher-detailed-view.php?tid=<?php echo $row['userID']; ?><?php echo "&c=$c&d=$d"; ?>"
                                                       name="reserve-slot" onclick="saveToStorage();">
                                                        <button class="custom__btn f-p"> BEKIJK PROFIEL</button>
                                                    </a>
                                                    <a href="./teacher-detailed-view.php?tid=<?php echo $row['userID']; ?><?php echo "&c=$c&d=$d"; ?>#avail-calendar"
                                                       name="reserve-slot">
                                                        <button class="custom__btn margin__top__five calendar-transition f-p">
                                                            BEKIJK BESCHIKBAARHEID
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php $decide = true; ?>

                                <?php
                            } else { // if($decide && $firstTime)          // CATEGORY 3 [ONLINE] > 12KM

                                if ($firstTime) {
                                    if ($postCodeEmpty == 'false') {
                                        echo "
					<div class='container margin-top'>
						<div class='row outline center__alignment'>
							<div class='col-md-2 neg-margin'>
								<i class='fas online__icon__large'>&#xf109;</i>	
							</div>
							<div class='col-md-10'>
								<h1 class='text-primary-bold'>Onderstaande docenten zijn voor jou alleen beschikbaar voor online bijles!</h1>
								<p> Wil je een berichtje zodra een nieuwe docent beschikbaar is die bij jou aan huis komt? Laat dan <a href='/bijlesverzoek' target='_blank' style='color:#5CC75F !important'>hier</a> je verzoek achter.</p>
							</div>
						</div>
					</div>
					<div class='container'><div class='row'><hr id='split-line' class='separator__line'/></div></div>";
                                    } else {
                                        echo "
					<div class='container margin-top'>
						<div class='row outline center__alignment'>
							<div class='col-md-2 neg-margin'>
								<i class='fas online__icon__large'>&#xf109;</i>	
							</div>
							<div class='col-md-10'>
								<h1 class='text-primary-bold'>Onderstaande docenten zijn voor jou alleen beschikbaar voor online bijles!</h1>
								<p> Wil je een berichtje zodra een nieuwe docent beschikbaar is die bij jou aan huis komt? Laat dan <a href='/bijlesverzoek' target='_blank' style='color:#5CC75F !important'>hier</a> je verzoek achter.</p>
							</div>
						</div>
					</div>
					<div class='container'><div class='row'><hr id='split-line' class='separator__line'/></div></div>";

                                    }


                                    $firstTime = false;
                                }
                                if ($split) {
                                    echo "";
                                    $split = false;
                                } ?>

                                <div class="container tutor__profile">
                                    <div class="row">
                                        <div class="col-md-3 col-lg-2">
                                            <img class="img-responsive center-block round__off"
                                                 src="<?php echo(($row['image']) ? substr($row['image'], 0) : constant("DEFAULT_PIC")); ?>"
                                                 class="img-thumbnail border-0"/>
                                        </div>
                                        <div class="col-md-6 col-lg-7">
                                            <div class="text-left">
                                                <div class="pt-4 pb-2">
                                                    <h4 class="text-primary"><?php echo $row['firstname'] . '&nbsp'; ?>

                                                        <div class="_level-md">
                                                            <span><?php echo $row['level']; ?> docent</span>

                                                            <?php if ($row['level'] == 'Senior') { ?>
                                                                <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent, vaak door middel van een gerelateerde studie, midden in de stof zit. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden.">&#xf05a;</i></sup>
                                                            <?php } else if ($row['level'] == 'Junior') { ?>
                                                                <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent erg dicht bij de leerling staat en goed weet waar de struikelblokken liggen. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. ">&#xf05a;</i></sup>
                                                            <?php } else if ($row['level'] == 'Supreme') { ?>
                                                                <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                        data-placement="bottom"
                                                                        data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Voor <?php echo $row['level']; ?> docenten garanderen wij onze hoogst mogelijke kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent een jarenlange ervaring heeft in het geven van onderwijs. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van de best mogelijke didactische vaardigheden en vakspecifieke kennis.">&#xf05a;</i></sup>
                                                            <?php } ?>

                                                        </div>
                                                    </h4>
                                                    <div class="card-text">
                                                        <span class="card-text"><p
                                                                    class="card-title"><?php echo $row['title']; ?></p></span>
                                                        <span class="card-text card-des"><?php echo($row['shortdescription'] ? (substr($row['shortdescription'], 0, 180)) : "<span class='text-center'>Stuur mij een bericht voor meer informatie.</span>"); ?></span>
                                                        <!-- <span class="custom_info_box"><?php //echo number_format((float)$row['distance'], 2, '.', '').' km Distant'; ?></span>&nbsp;&nbsp; -->
                                                        <hr id="split-line" class="separator__line"/>
                                                        <div class="container">
                                                            <div class="row">
                                                                <div class="col-md-5 col-sm-6 center__alignment">
											<span class="online__tutoring">	
												<a href="/online-bijles-uitleg" target="_blank">
													<i class="fas online__icon tippy" data-toggle="tooltip"
                                                       data-placement="bottom"
                                                       data-tippy-content='Deze docent is beschikbaar voor online bijles. Meer weten, klik <a href="/online-bijles-uitleg" target="_blank">hier</a>!'>&#xf109;</i>
												</a>
                                                <!-- <i class='fas' style="float:left;">&#xf109;</i> Online videobijles : <?php echo ($row["onlineteaching"]) ? "Ja" : "Nee" ?></span>&nbsp&nbsp -->
											</span>
                                                                </div>
                                                                <div class="col-md-7 col-sm-6">
											<span class="custom_info_box">
                                                <?php

                                                $teacherCourses = explode(",", $row['courses_name']);

                                                $notTeached = array_diff($teacherCourses, $courseName);
                                                $teached = array_intersect($courseName, $teacherCourses);   // Schnittmenge
                                                // var_dump($notTeached);
                                                /*
                                                echo "Teached <br>";
                                                echo "<pre>" . print_r($teached, true) . "</pre>";
                                                echo "<br>";

                                                echo "Not Teached <br>";
                                                echo "<pre>" . print_r($notTeached, true) . "</pre>";
 */
                                                //    var_dump($teached);


                                                if (!empty($subjectsTaught)) {
                                                    $notTeached = array_intersect($subjectsTaught, $notTeached);

                                                    $teached = array_intersect($subjectsTaught, $teached);
                                                }


                                                $outPutString = "";
                                                $counter = 0;
                                                $wordCounter = 0;
                                                $highlighted = false;


                                                // Teached things highlights
                                                foreach ($teached as $teach) {
                                                    $counter++;
                                                    if ($counter == 3) {
                                                        break;
                                                    } else if ($wordCounter <= 3) {

                                                        $outPutString .= "<b>" . $teach . "</b>" . ", ";
                                                        $wordCounter++;
                                                    } else {
                                                        break;
                                                    }
                                                }

                                                // SHOW 2 NON HIGHLIGHTED TOO
                                                $counter = 0;
                                                foreach ($notTeached as $noteach) {
                                                    $counter++;
                                                    if ($counter == 3) {
                                                        break;
                                                    } else if ($wordCounter <= 3) {
                                                        $outPutString .= $noteach . ", ";
                                                        $wordCounter++;
                                                    } else {
                                                        break;
                                                    }
                                                }

                                                echo substr($outPutString, 0, strlen($outPutString) - 2) . " en meer";


                                                ?>
                                                <?php
                                                /*
                                                    $crs = explode(",", $row['courses_name']);
													$count = count($crs);
													$subject = "";
													for($i=0; $i<$count; $i++){
													    if($i > 1) {
													        $subject = rtrim($subject, ", ").	 " en meer ";
													        break;
													    }

													    $subject .= $crs[$i].", ";
													}
													//echo rtrim($subject, ", ");
                                                */
                                                ?>
											</span>

                                                                    <?php //echo ($row["allownewstudents"])? " " : "<span class='custom_info_box' style='background:#f8d7da'>Alleen beschikbaar voor mijn huidige bijlesleerlingen</span>" ?>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-3 partition">
                                            <div class="pt-4 pb-2 _level_container">
                                                <hr id="split-line" class="partition__separator__line"/>

                                                <!--<span style='background-color:#dceef1; font-size:14px;' class="badge">type docent: <?php echo $row["level"]; ?></span>-->
                                                <!--
                                                <div class="_level">  <#?php echo $row["level"]; ?> docent
                                                    <sup><i class="fa alignment" data-toggle="tooltip" data-placement="right" title="Voor de specificaties van onze verschillende typen docenten verwijzen we graag naar de prijzenpagina."></i></sup>
                                                </div>
                                                -->

                                                <div class="_level docent_level_bottom <?php echo strtolower($row['level']); ?>">
                                                    <span><?php echo $row['level']; ?> docent</span>

                                                    <?php if ($row['level'] == 'Senior') { ?>
                                                        <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent, vaak door middel van een gerelateerde studie, midden in de stof zit. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden.">&#xf05a;</i></sup>
                                                    <?php } else if ($row['level'] == 'Junior') { ?>
                                                        <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Ook voor <?php echo $row['level']; ?> docenten garanderen wij onze hoge kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent erg dicht bij de leerling staat en goed weet waar de struikelblokken liggen. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. ">&#xf05a;</i></sup>
                                                    <?php } else if ($row['level'] == 'Supreme') { ?>
                                                        <sup><i class="fa alignment tippy" data-toggle="tooltip"
                                                                data-placement="bottom"
                                                                data-tippy-content="<?php echo $row['firstname']; ?> Is een <?php echo $row['level']; ?> docent met bijbehorend tarief van €<?php echo number_format($row['rate'], 2, ".", ","); ?> per lesuur van 45 minuten. Voor <?php echo $row['level']; ?> docenten garanderen wij onze hoogst mogelijke kwaliteit. Het voordeel van een <?php echo $row['level']; ?> docent is dat de docent een jarenlange ervaring heeft in het geven van onderwijs. <?php echo $row['firstname']; ?> is zorgvuldig geselecteerd op basis van de best mogelijke didactische vaardigheden en vakspecifieke kennis.">&#xf05a;</i></sup>
                                                    <?php } ?>

                                                </div>

                                                <div class="_level tutor__profile__course-price">
                                                    <small><?php echo number_format($row['rate'], 2, ".", ","); ?>€ per
                                                        lesuur*</small><br/>
                                                </div>
                                                <!--<p class="mt-25" style="font-weight:bold;"><?php //echo "€".$row['rate']."/hr"; ?></p>-->
                                                <!--<small><span style='background-color:#007b5e; font-size:14px;' class="badge"><?php //echo ($row["completedLessons"])?$row["completedLessons"] : "0" ?></span> completed lessons</small>-->
                                                <div class="align__bottom pb-2">
                                                    <a href="./teacher-detailed-view.php?tid=<?php echo $row['userID']; ?><?php echo "&c=$c&d=$d"; ?>"
                                                       name="reserve-slot" onclick="saveToStorage();">
                                                        <button class="custom__btn f-p"> BEKIJK PROFIEL</button>
                                                    </a>
                                                    <a href="./teacher-detailed-view.php?tid=<?php echo $row['userID']; ?><?php echo "&c=$c&d=$d"; ?>&t#avail-calendar"
                                                       name="reserve-slot">
                                                        <button class="custom__btn margin__top__five calendar-transition f-p">
                                                            BEKIJK BESCHIKBAARHEID
                                                        </button>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            <?php } ?>

                            <?php $flag = false;
                        }
                    }
                } ?>
            </div>

            <script>
                $(document).ready(function () {
                    console.log('tippy on ready');
                    tippy('.tippy', {
                        placement: 'bottom-start',
                        animation: 'fade',
                        theme: 'light',
                        interactive: true,
                        interactiveBorder: 0
                    });
                    $('[data-toggle="tooltip"]').tooltip({});
                });
            </script>
        </div>
    </section>
    <?php if ($flag) {
        // ob_get_clean();
        echo "<div class='container no__more__tutors'>
				<div class='row'>
					<div class='col-md-1'>
						<i class='fa cross__icon__large'>&#xf00d;</i>
					</div>
					<div class='col-md-11'>
						<h4 class='text-primary'> Geen docenten gevonden! </h4>
							<p class='card-text'> Sorry, wij hebben geen docent die voldoet aan je zoekcriteria.
								Laat je gegevens gerust achter in het contactformulier,
								waarna we contact opnemen zodra we een geschikte docent hebben.
								Natuurlijk is dit geheel vrijblijvend.</p>
					</div>
				</div>
			  </div>";
        #echo "<div class='alert alert-danger' id='msg_not_found' style='border-radius:10px; width:100%; text-align:center'>No Tutor found ...! </div>";
    }
} else {
    echo mysqli_error($con);
}
?>

		