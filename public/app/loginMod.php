<?php 

session_start();

if(isset($_SESSION['TeacherID'])){
	header('Location: teachers-dashboard.php');
	exit;
}
	
if(isset($_SESSION['StudentID'])){
	header('Location: students-dashboard.php');
	exit;
}

?><!doctype html>

<head>
<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
   
	
    <!-- Custom Css -->
 
	<style>

	.page-header:before{
		background-color: rgba(86, 86, 86, 0.4) !important;
	}
		.msg-error {
		  color: #c65848;
		}
		
		.g-recaptcha.error {
		  border: solid 2px #c64848;
		  padding: .2em;
		  width: 19em;
		}
		
	
			//by Aqib
		body {
		  background-color:#FAFBFC!important;
		}
		
		/* {
			color: #486066!important;
			font-family: Poppins light !important;
			background-color: #f1f1f1 !important;
		}*/
		div.container{width: 100%;}
		.zmdi{font: normal normal normal 14px/1 'Material-Design-Iconic-Font'!important;}
		input::placeholder {color: #486066!important;
		}
		.border{
			    border: 2px solid rgb(72, 96, 102) !important;
    			border-radius: 50px;
		}
		.page-header{
			 background-image: url("http://bt.bijlesaanhuis.nl/file_download.php?file_id=96&type=bug");
		}
		
		/* login */
		.ma-heading{
			color: #000;
		}
		.authentication .card-plain.card-plain .form-control:focus{
			color: #486066 !important;
		}
		.authentication .card-plain.card-plain .form-control, .authentication .card-plain.card-plain  .input-group-focus .input-group-addon{
			color: #486066 !important;
		}
		
		.authentication .card-plain .form{
			background: #f1f1f1 !important;
			padding: 30px;
			border-radius: 6px;
		}
		.authentication .card-plain{
			max-width: 50%;
		}
		.authentication .link, h6{
			color: #000;
		}
		#recaptcha{
			/*margin: 0 auto;*/
			margin-left:-30px !important;
		}
		.authentication .card-plain.card-plain .input-group-addon{
			color: rgb(0, 0, 0);
		}
		
		@media only screen and (max-width: 768px){
				.authentication .card-plain{
					max-width: 100%;
			}
		}
		.custom-modal-dialog{
			width: 1100px !important;
			max-width:1100px !important;
		}
		.steps{
			padding:0 !important;
		}
		.modal-title
		{
			color: #5cc75f !important;
		}
		.wizard > .steps .first a {

			border-radius: 6px !Important;
			padding: 3px !Important;

		}		
		.wizard > .steps .current  a {

			background-color: #5CC75F !Important;
			border-radius: 6px !Important;
			padding: 3px !Important;

		}	
		.wizard > .steps .disabled  a {
   			border: 1px solid black;
			border-radius: 6px !Important;
			padding: 3px !Important;

		}		
	</style>
</head>

<!-- <body class="theme-green authentication sidebar-collapse">-->
<!-- Navbar -->
<nav class="navbar navbar-expand-lg fixed-top navbar-transparent">
    <div class="container">        
        <div class="navbar-translate n_logo">
           
           
        </div>
        
    </div>
</nav>
<!-- End Navbar -->

				
            <div class="card-plain">
				<!-- Error Message -->
			<?php
				require_once("includes/connection.php");
								if(isset($_GET['msg']))
								{
									?>
							<div class="bs-example" style="font-size:10px;">
    <div class="alert alert-danger">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo test_input($_GET['msg']); ?>
    </div>
</div>
							<?php
								}
				
								if(isset($_GET['successMsg']))
								{
									?>
							<div class="bs-example" style="font-size:10px;">
    <div class="alert alert-success">
        <a href="#" class="close" data-dismiss="alert">&times;</a>
        <?php echo test_input($_GET['successMsg']); ?>
    </div>
</div>
				<?php
								}
							?>
            
			    <form id="login_form" class="form" method="post" action="actions/login">
                    <!--<div class="header">
                        <div class="logo-container">
                            <img src="img/Logo01.png" alt="">
                        </div>
                        <h5 class="ma-heading">INLOGGEN</h5>
                    </div>-->
                    <div class="content">                                                
                        <div class="input-group input-lg border">
                            <input type="text" class="form-control" placeholder="E-mailadres" name="username" required maxlength="60" minlength="5" />
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-account-circle"></i>
                            </span>
                        </div>
                        <div class="input-group input-lg border">
                            <input type="password" placeholder="Wachtwoord" class="form-control" name="password" required maxlength="20" minlength="8" title="Wachtwoord moet minimaal 8 tekens bevatten" />
                            <span class="input-group-addon">
                                <i class="zmdi zmdi-lock"></i>
                            </span>
                        </div>
                    </div>
					<div class="input-group input-lg">
						<span class="msg-error error"></span>
						<div id="recaptcha" class="g-recaptcha" style="width:312px;" data-sitekey="6LebmZ8UAAAAAJU6v6g1ntuuNZycJMbr8k6Pv5F3"></div>				
					</div>
                    <div class="footer text-center">
						<button type="submit" name="login" id="login" class="btn btn-primary btn-round btn-lg btn-block">INLOGGEN</button>
                      
                        <h5><a href="forgotPassword" class="link">Wachtwoord vergeten?</a></h5>
						<h6> Registreer als <a href="#" class="link student-registration-link"> LEERLING? </a> / <a href="#" class="link tutor-registration-link">DOCENT?</a></h6>
                    </div>
                

  <span class="msg-error"></span>			
                </form>
            </div>





<!-- Jquery Core Js -->
<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<script>
   $(".navbar-toggler").on('click',function() {
    $("html").toggleClass("nav-open");
});
//=============================================================================
$('.form-control').on("focus", function() {
    $(this).parent('.input-group').addClass("input-group-focus");
}).on("blur", function() {
    $(this).parent(".input-group").removeClass("input-group-focus");
});
</script>
	<!----------------------------------recaptcha-------------- -->
	<!----------------------------------recaptcha-------------- -->
	<script src="https://www.google.com/recaptcha/api.js?explicit&hl=nl"  async defer></script>

<script>
$(".modal").on("hidden.bs.modal", function(){	
		$("#login-body").css("display","block");
		$("#login-header").css("display","block");
		
		$(".modal-dialog").removeClass("custom-modal-dialog");
		$("#registration-body").css("display","none");
		$("#registration-header").css("display","none");
		$("#registration-student-body").css("display","none");
		$("#registration-student-header").css("display","none");
});	
	$(".tutor-registration-link").on("click", function(){
		/*$(".modal-body").css("display","none");*/
		$("#login-body").css("display","none");
		$("#login-header").css("display","none");
		
		$(".modal-dialog").addClass("custom-modal-dialog");
		$("#registration-body").css("display","block");
		$("#registration-header").css("display","block");
		
	});	
	$(".student-registration-link").on("click", function(){
		/*$(".modal-body").css("display","none");*/
		$("#login-body").css("display","none");
		$("#login-header").css("display","none");
		
		$(".modal-dialog").addClass("custom-modal-dialog");
		$("#registration-student-body").css("display","block");
		$("#registration-student-header").css("display","block");
		
	});		
$( '#login_form').submit(function(){
  var $captcha = $( '#recaptcha' ),
      response = grecaptcha.getResponse();
     
  if (response.length === 0) {
    $( '.msg-error').text( "" );
    if( !$captcha.hasClass( "error" ) ){
      $captcha.addClass( "error" );
		return false;
    }
  } else {
    $( '.msg-error' ).text('');
    $captcha.removeClass( "error" );
	  return true;
  }
});
	</script>
	<script>
function isIE() {
  ua = navigator.userAgent;
  /* MSIE used to detect old browsers and Trident used to newer ones*/
  var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
  
  return is_ie; 
}
function isEdge(){
	ua = navigator.userAgent;
	 var is_ed = ua.indexOf("Edge") > -1;
	 return is_ed;
}
/* for IE and Edge */
if (isIE()){
    alert('Applicatie is niet compatibel met deze browser.');
	window.location.replace("https://bijlesaanhuis.nl");
}else if(isEdge()){
	alert('Applicatie is niet compatibel met deze browser.');
	window.location.replace("https://bijlesaanhuis.nl");
}
</script>
