<?php

use API\Endpoint\Calendar;

require_once('includes/connection.php');
require_once('../../api/endpoint/Calendar.php');
$thisPage = "Teachers Dashboard";
session_start();

if (!isset($_SESSION['Teacher'])) {
    header('Location: index.php');
} else {


    function getDutchMonth($mon)
    {
        $months = array(
            "Jan" => "Jan",
            "Feb" => "Feb",
            "Mar" => "Mrt",
            "Apr" => "Apr",
            "May" => "Mei",
            "Jun" => "Jun",
            "Jul" => "Jul",
            "Aug" => "Aug",
            "Sep" => "Sept",
            "Sept" => "Sept",
            "Oct" => "Okt",
            "Nov" => "Nov",
            "Dec" => "Dec"
        );
        return $months[$mon];
    }

    ?>

    <!doctype html>
    <html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description"
              content="Een overzicht van alle geboekte lessen in je kalender | Verdien meer dan bij onze grootste concurrenten | Bepaal zelf aan wie, wanneer en waar je bijles geeft.">

        <title>Kalender | Bijles Aan Huis</title>

        <?php
        require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
        ?>
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link rel="stylesheet" href="calendar/css/calendar.css">
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
              rel="stylesheet"/>
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
        <link rel="stylesheet" href="assets/css/nigel.css" type="text/css">

        <style type="text/css">
            .b-warning {
                border-color: #ffbd6b !important;
            }

            #calendar {
                background-color: #fafafa !important;
            }

            .cal-month-day {
                color: black;
            }

            .cal-row-head {
                background-color: #fafafa !important;
            }

            .empty {
                background-color: white !important;
                border: 2px solid black;
            }

            .inline_time {
                color: #50d38a !important;
            }

            .inline_time_disabled {
                color: #6b7971 !important;
            }

            #cal-slide-content {
                font-family: inherit;
                color: #bdbdbd;
            }

            .page-header {
                height: 0 !important;
                padding-bottom: 0px;
                margin: 0 0 50px;
                border-bottom: 0px;
            }

            div.cal-month-day .pull-right {
                pointer-events: none;
            }

            .page-calendar .b-primary {
                border-color: #5CC75F !important;
            }


        </style>

        <style type="text/css">

            .header-radius {
                border-top-left-radius: 6px !important;
                border-top-right-radius: 6px !important;
            }

            .body-radius {
                border-bottom-left-radius: 6px !important;
                border-bottom-right-radius: 6px !important;
            }

            /*Placeholder Color */
            input {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            select {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            input:focus {
                background: transparent !Important;
            }

            select:focus {
                background: transparent !Important;
            }

            .wizard .content {
                /*overflow-y: hidden !important;*/
            }

            .wizard .content label {

                color: white !important;

            }

            .wizard > .steps .current a {
                background-color: #029898 !Important;
            }

            .wizard > .steps .done a {
                background-color: #828f9380 !Important;
            }

            .wizard > .actions a {
                background-color: #029898 !Important;
            }

            .wizard > .actions .disabled a {
                background-color: #eee !important;
            }

            .btn.btn-simple {
                border-color: white !important;
            }

            .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
                color: white;
            }

            table {
                color: white;
            }

            .multiselect.dropdown-toggle.btn.btn-default {
                display: none !important;
            }

            /*.navbar.p-l-5.p-r-5
            {
                display: none !important;
            }*/

            .fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
                color: black !important;
            }

            .content {
                margin-top: 52px !important;
            }

            .card .body {
                background-color: #FAFBFC !important;
            }

            .card .body h2 {
                font-size: 36px !important;
                color: #5CC75F !important;
            }

            .card .header h2::before {
                height: 21px !important;
            }

            select#calender_startTime, select#calender_endTime {
                color: #4e4e4e !important;
            }

            /* extra popup modal styling */

            .modal .modal-title {
                font-size: 18px !important;
                font-weight: bold !important;
            }

            .modal .modal-text {
                padding: 0 20px 27px 20px;
                font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important;
                font-size: 16px !important;
            }

            .modal-content .modal-header button {
                right: 12px !important;
                top: 6px !important;
            }

            .custom-padding {
                padding: 0 0 1rem 0 !important;
                margin-top: 1rem;
            }

            #check + label {
                border: 1.5px solid #486066;
            }

            #check + label svg path {
                stroke: #ffbd6b;
            }

            #cal-slide-content {
                background-color: #f1f1f1;
            }


            .modal-dialog-centered {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                min-height: calc(100% - (.5rem * 2));
            }

            @media (min-width: 576px) {
                .modal-dialog-centered {
                    min-height: calc(100% - (1.75rem * 2));
                }
            }
        
            @media (max-width: 768px) {
                #mobile-halve .dashboard-list{
                    height: 300px !important;
                }
            }
            .dashboard-content-container {
                display: flex;
                flex-wrap: wrap;
                justify-content: space-between;
                margin: 0 5%;
            }
            .dashboard-content__section {
                float: none;
                background-color: white;
                border-radius: 6px;
                margin-bottom: 30px;
                border: 1px #f1f1f1 solid;
            }
            section.dashboard-content .dashboard-header {
                background-color: #f1f1f1;
                border-radius: 6px 6px 0 0;
            }
            .dashboard-content__section--cal .dashboard-header {
                padding: 0 15px 10px 15px;
            }
        </style>
        <?php
        $activePage = basename($_SERVER['PHP_SELF']);
        ?>
    </head>
    <body class="theme-green dashboard">
    <!-- Page Loader -->
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                     alt="Oreo"></div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>

    <?php
    require_once('includes/header.php');
    ?>

    <!-- Main Content -->
    <section class="content page-calendar dashboard-content">

        <div class="dashboard-content-container">
            <div class="dashboard-content__section dashboard-content__section--cal">
                <div class="dashboard-header">
                    <div class="dashboard-header__titles">
                        <h2 class="ma-subtitle dashboard-header__heading">Kalender</h2><h3 class="ma-subtitle dashboard-header__heading"></h3>
                    </div>
                    <div class="colorLegend">
                        <p class="colorLegend-p"><span class="color available"></span>Beschikbaar</p>
                        <p class="colorLegend-p"><span class="color request"></span> Bijlesverzoek</p>
                        <p class="colorLegend-p"><span class="color appointment"></span>Afspraak</p>
                    </div>
                    <div class="pull-right form-inline">
                        <div class="btn-group">
                            <button class="btn btn-primary" data-calendar-nav="prev"><<</button>
                            <button class="btn btn-default" data-calendar-nav="today">Today</button>
                            <button class="btn btn-primary" data-calendar-nav="next">>></button>
                        </div>
                    </div>
                </div>
                <div class="dashboard-calendar">
                    <div id="calendar"></div>
                </div>
            </div>
            <?php 
                $cal = new Calendar($db, ['teacherID' => $_SESSION['TeacherID']]);
                $result = $cal->GetAppointments();
                $outputArray = $result->getResult();
                $data = false;
                foreach ($outputArray as $key => $value) {
                    if (isset($value['pending'])) {
                        $data = true;
                    }
                    if (isset($value['accepted'])) {
                        $data = true;
                    }
                } 
            ?>
            <div class="dashboard-content__section dashboard-content__section--list" <?php if (!$data){?> id="mobile-halve" <?php } ?>>
                <div class="dashboard-header">
                    <h4 class="ma-subtitle">Toekomstige afspraken</h4>
                </div>
                <div class="body dashboard-list">
                    <div id="reload">
                        <?php
                        $limit = 3;
                        $i = 0;

                        $data = false;
                        foreach ($outputArray as $key => $value) {
                            if ($i > $limit) {
                                echo "<hr/> <p style='text-align:right;'> <a style='color:white !important; background:#5CC75F;' class='btn future-btn' href='teachers-appointment' >Toon alle</a></p>";
                                break;
                            }

                            $year = DateTime::createFromFormat('Y-m-d', $key)->format('Y');
                            $m = DateTime::createFromFormat('Y-m-d', $key)->format('M');
                            $day = DateTime::createFromFormat('Y-m-d', $key)->format('d');

                            if (isset($value['pending'])) {
                                foreach ($value['pending'] as $key2 => $value2) {
                                    $data = true;
                                    $userID = isset($value2['userID']) ? $value2['userID'] : '';
                                    $forename = isset($value2['forename']) ? $value2['forename'] : '';
                                    $surname = isset($value2['surename']) ? $value2['surename'] : '';
                                    $begin = isset($value2['begin']) ? $value2['begin'] : '';
                                    $end = isset($value2['end']) ? $value2['end'] : '';
                                    $address = isset($value2['address']) ? $value2['address'] : '';
                                    $email = isset($value2['email']) ? $value2['email'] : '';
                                    $telephone = isset($value2['telephone']) ? $value2['telephone'] : '';
                                    echo '<hr id="h" /><div id="cl" class="event-name b-warning row">
                                            
                                        <div class="col-2 p-0 m-0 text-center">
                                            <h4>' . $day . '<span>' . $m . '</span><span>' . $year . '</span></h4>
                                        </div>
                                        <div class="col-10 p-0 m-0 ">
                                            <h6>' . "<a href='student-profile?sid={$userID}'>{$forename}  {$surname}</a>" . '</h6>
                                            <address><i class="zmdi zmdi-time"></i> ' . $begin . ' - ' . $end . '</address>
                                            <address><i class="zmdi zmdi-phone"></i> ' . $telephone . '</address>
                                            <address><i class="zmdi zmdi-email"></i> ' . $email . '</address> 
                                            <address><i class="zmdi zmdi-pin"></i>' . $address . '</address>
                                        
                                        </div>
                                    </div>';
                                    $i++;
                                }
                            }
                            if (isset($value['accepted'])) {
                                foreach ($value['accepted'] as $key2 => $value2) {
                                    $data = true;
                                    echo '<hr id="h" /><div id="cl" class="event-name b-primary row">
                                    
                                <div class="col-2 p-0 m-0 text-center">
                                    <h4>' . $day . '<span>' . $m . '</span><span>' . $year . '</span></h4>
                                </div>
                                <div class="col-10 p-0 m-0 ">
                                    <h6>' . "<a href='student-profile?sid={$value2['userID']}'>{$value2['forename']}  {$value2['surename']}</a>" . '</h6>
                                    <address><i class="zmdi zmdi-time"></i> ' . $value2['begin'] . ' - ' . $value2['end'] . '</address>
                                    <address><i class="zmdi zmdi-phone"></i> ' . $value2['telephone'] . '</address>
                                    <address><i class="zmdi zmdi-email"></i> ' . $value2['email'] . '</address> 
                                    <address><i class="zmdi zmdi-pin"></i>' . $value2['address'] . '</address>
                                
                                </div>
                            </div>';
                                    $i++;
                                }
                            }
                        }
                        if (!$data) {
                            echo '<p style="text-align:center;">Geen toekomstige afspraken</p>';
                        }
                        ?>

                    </div>
                </div>
            </div>
        </div>
        <?php
        require_once('includes/footerScriptsAddForms.php');
        ?>


        <!-- New Calendar JS -->

        <script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
        <script type="text/javascript"
                src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
        <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script type="text/javascript"
                src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
        <script type="text/javascript" src="calendar/js/calendar.js"></script>
        <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
        <script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
        <!-- <script type="text/javascript" src="calendar/js/app.js"></script> -->
        <!-- New Calendar JS -->

        <script>
            // this function is added
            Date.prototype.setTimezoneOffset = function (minutes) {
                var _minutes;
                if (this.timezoneOffset == _minutes) {
                    _minutes = this.getTimezoneOffset();
                } else {
                    _minutes = this.timezoneOffset;
                }
                if (arguments.length) {
                    this.timezoneOffset = minutes;
                } else {
                    this.timezoneOffset = minutes = this.getTimezoneOffset();
                }
                return this.setTime(this.getTime() + (_minutes - minutes) * 6e4);
            };


            var calendar;
            var options;

            var calendar_day;
            var calandar_view;

            var _obj = null;
            var _id = null;
            var _name = null;

            var _ddate = null;
            var _stime = null;
            var _etime = null;
            var _notficationMsg = "";
            var _timer = 3;

            var _availabilityDate = null;
            var _availType = null;
            var _dateOriginal = null;
            var _checked = null;

            function accept(id) {
                var data_opt;
                //data_opt = { subpage:"acceptPendingAppoi", cbID: id};
                var str_id = id + "";
                data_opt = {subpage: "confrimPending", cbID: id, decide_action: 1};
                var _id = str_id.split(".");
                $.ajax({
                    type: "POST",
                    url: "teacher-ajaxcalls.php",
                    data: data_opt,
                    success: function (response) {

                        //alert(id);
                        if (response.trim() == "success") {

                            $("#cl" + _id[0]).css("display", "none");
                            $("#h" + _id[0]).css("display", "none");
                            reload(id);
                            showNotification("alert-success", "De bijles is bevestigd", "bottom", "center", "", "");
                            //document.location.reload();
                            calendar = $('#calendar').calendar(options);
                        } else {
                            showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                        }
                        $(document.body).css({'cursor': 'default'});
                        $("#submitBtn").prop("disabled", false);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(id + " error: " + xhr.responseText);
                    }
                });
            }


            function reject(id, name) {
                _id = id;

                var modelLabel = "";

                if (name == 1) {
                    modelLabel = "Weet je zeker dat je de bijles wilt annuleren?";
                    _notficationMsg = "Afspraak is geannuleerd";
                    _timer = 1000;
                    $("#myModalLabel2").text(modelLabel);
                    $('#appointment-delete-type').appendTo("body").modal('show');
                    $("#delete-yes-type2").click(function () {
                        //alert('Removed');
                        $('#appointment-delete-type').css('display', 'none');
                        $('.modal-backdrop').hide();
                        $("#modal-information-type").modal('hide');
                        var url = "/api/Calendar/CancelTutor";
                        $.ajax({
                            type: "POST",
                            url: url,
                            data: {id: _id},
                            dataType: "json",
                            success: function (response) {
                                showNotification("alert-success", _notficationMsg, "bottom", "center", "", "", 2000);
                                document.location.reload();
                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                //showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                            }
                        });// ends ajax
                    });

                } else if (name == 2) {

                    modelLabel = "Weet je zeker dat je de afpraak wilt weigeren?";
                    _notficationMsg = "Afspraak is afgewezen";
                    _timer = 1000;
                    $("#myModalLabel2").text(modelLabel);
                    $('#appointment-delete-type').appendTo("body").modal('show');
                    $("#delete-yes-type2").click(function () {
                       cancelPending(_id);
                       $("#appointment-delete-type").modal('hide');
                    });

                } else if (name == 3) {
                    //modelLabel = "Weet je zeker dat je de bijles wilt annuleren?";
                    _notficationMsg = "Beschikbaar is verwijderd";
                    _timer = 1000;
                    var url = "/api/Calendar/CancelTutor";
                    $.ajax({
                        type: "POST",
                        url: url,
                        data: {id: _id},
                        dataType: "json",
                        success: function (response) {
                            showNotification("alert-success", _notficationMsg, "bottom", "center", "", "", 1000);
                            document.location.reload();
                        },
                        error: function (xhr, ajaxOptions, thrownError) {
                            showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                        }
                    });// ends ajax


                }
            }

            function reload(id) {
                var data_opt;
                var str_id = id + "";
                //data_opt = { subpage:"acceptPendingAppoi", cbID: id};

                data_opt = {subpage: "reloadDiv", id:<?php echo $_SESSION['TeacherID']; ?>};
                var _id = str_id.split(".");
                //console.log($("#cl"+_id[0]).html());
                $.ajax({
                    type: "POST",
                    url: "teacher-ajaxcalls.php",
                    data: data_opt,
                    success: function (response) {

                        //alert(id);
                        $("#reload").html(response);
                        //console.log(response);
                        // calendar = $('#calendar').calendar(options);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        console.log(id + " error: " + xhr.responseText);
                    }
                });
            }

            function cancelPending(_id) {
                var url = "/api/Calendar/CancelTutor";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {id: _id},
                    dataType: "json",
                    success: function (response) {

                        $('#modal-information-type .modal-title').text('Je hebt het bijlesverzoek afgewezen!');
                        $('#modal-information-type .modal-text').html("De leerling wordt hierover door ons standaard per e-mail geïnformeerd, of persoonlijk per telefoon indien het een nieuwe leerling betreft. Hierdoor hoef jij de leerling dus niet meer te informeren over deze afwijzing. Indien je helemaal niet meer beschikbaar bent voor leerlingen, geef dat dan aan op de <a href='/app/edit-profile' style='color:#5CC75F !important;'>Profiel Aanpassen</a> pagina.");
                        $('#modal-information-type').appendTo("body").modal('show');
                        //setTimeout(function () {
                        //    document.location.reload();
                        //}, 4000);

                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                    }
                });// ends ajax
            }

            function acceptPending(obj, id) {
                var url = "/api/Calendar/AcceptTutor";
                $.ajax({
                    type: "POST",
                    url: url,
                    data: {id: id},
                    dataType: "json",
                    success: function (response) {
                        showNotification("alert-success", "De bijles is bevestigd", "bottom", "center", "", "", 1000);
                        document.location.reload();
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                    }
                });// ends ajax
            }


            $(document).ready(function () {


                $(".page-loader-wrapper").css("display", "none");


                "use strict";

                options = {
                    events_source: "/api/Calendar/GetAppointments?teacherID=<?php echo $_SESSION['TeacherID']; ?>",
                    view: 'month',
                    tmpl_path: 'calendar/tmpls/teachers-dashboard/',
                    tmpl_cache: false,
                    day: 'now',

                    merge_holidays: false,
                    display_week_numbers: false,
                    weekbox: false,
                    //shows events which fits between time_start and time_end
                    show_events_which_fits_time: false,

                    onAfterEventsLoad: function (events) {
                        if (!events) {
                            return;
                        }
                        var list = $('#eventlist');
                        list.html('');

                        $.each(events, function (key, obj) {
                            //converting the timeZone to UTC
                            obj.start = new Date(parseInt(obj.start)).setTimezoneOffset(0);
                            obj.end = new Date(parseInt(obj.end)).setTimezoneOffset(0);

                            $(document.createElement('li'))
                                .html('<a href="' + obj.url + '">' + obj.title + '</a>')
                                .appendTo(list);
                        });
                    },

                    onAfterViewLoad: function (view) {
                        $('h3.ma-subtitle').text(this.getTitle());
                        $('.btn-group button').removeClass('active');
                        $('button[data-calendar-view="' + view + '"]').addClass('active');
                    },
                    classes: {
                        months: {
                            general: 'label'
                        }
                    }
                };

                calendar = $('#calendar').calendar(options);

                $('.btn-group button[data-calendar-nav]').each(function () {
                    var $this = $(this);
                    $this.click(function () {
                        calendar.navigate($this.data('calendar-nav'));
                    });
                });

                $('.btn-group button[data-calendar-view]').each(function () {
                    var $this = $(this);
                    $this.click(function () {
                        //calendar.view($this.data('calendar-view'));
                    });
                });

                $('#first_day').change(function () {
                    var value = $(this).val();
                    value = value.length ? parseInt(value) : null;
                    //calendar.setOptions({first_day: value});
                    //calendar.view();
                });

                $('#language').change(function () {
                    calendar.setLanguage($(this).val());
                    calendar.view();
                });

                $('#events-in-modal').change(function () {
                    var val = $(this).is(':checked') ? $(this).val() : null;
                    //  calendar.setOptions({modal: val});
                });
                $('#format-12-hours').change(function () {
                    var val = $(this).is(':checked') ? true : false;
                    calendar.setOptions({format12: val});
                    calendar.view();
                });
                $('#show_wbn').change(function () {
                    var val = $(this).is(':checked') ? true : false;
                    calendar.setOptions({display_week_numbers: val});
                    calendar.view();
                });
                $('#show_wb').change(function () {
                    var val = $(this).is(':checked') ? true : false;
                    calendar.setOptions({weekbox: val});
                    calendar.view();
                });
                $('#events-modal .modal-header, #events-modal .modal-footer').click(function (e) {
                    //e.preventDefault();
                    //e.stopPropagation();
                });


                $(".list-item").on("mouseover", function (e) {
                    e.preventDefault();
                    console.log($(this).html());
                });

                $('#first_day').change(function () {
                    var value = $(this).val();
                    value = value.length ? parseInt(value) : null;
                    calendar.setOptions({first_day: value});
                    calendar.view();
                });


                $("#delete-yes-type").click(function () {
                    cancelSlott(_obj, _id, _name);
                    $("#modal-delete-type").modal('hide');
                });

                $("#set-no-availability").click(function () {
                    availabilityy(_dateOriginal, _availType, _checked); 

                    $("#availability-delete-type").modal('hide');
                });

                $("#delete-no-type").click(function () {
                    $("#modal-delete-type").modal('hide');
                    _obj = null;
                    _id = null;
                    _name = null;

                    _ddate = null;
                    _stime = null;
                    _etime = null;
                });


                //$("#delete-yes-type2").click(function () {
                //    cancelPending(_id);
                //    $("#appointment-delete-type").modal('hide');
                //});

                $("#availability-no-type").click(function () {
                    $("#availability-delete-type").modal('hide');
                });

                $("#delete-no-type2").click(function () {
                    $("#appointment-delete-type").modal('hide');
                    _id = null;
                });

                /*$("body").on("click", ".cal-month-day", function () {
                    console.log("in here");
                    __calender_date = $(this).find(".pull-right").attr("data-cal-date");
                    console.log(__calender_date);
                    console.log(Global_Variable_timeRange);
                    var timeArr = Global_Variable_timeRange[__calender_date];
                    console.log(timeArr);
                    var str = "";
                    for (var time in timeArr) {
                        // if(timeArr[time] == "available")
                        str += "<option value='" + time + "'>" + time + "</option>";
                    }
                    //changing stuff after some time
                    setTimeout(function () {
                        $(".inline-elem-updateButton.bookSlots").val("verwijderd");
                        $("#calender_startTime").html(str);
                        $("#calender_startTime").change();
                    }, 250);

                });

                 */



                //$("body").on("change", "#calender_startTime", function () {
                //    var stime = $("#calender_startTime").val();
                //    var timeArr = Global_Variable_timeRange[__calender_date];
                //    var slotDiff = <?php //echo DEFAULT_TIME_FOR_APPOINTMENT_SLOT * 60 * 1000; //millisec ?>//;
                //
                //    //adding slot_time to get end_time
                //    var tempTime = new Date("01/01/2007 " + stime);
                //    tempTime = new Date(tempTime.getTime() + slotDiff);
                //    tempTime = ((tempTime.getHours() > 9) ? tempTime.getHours() : "0" + tempTime.getHours()) + ":" + ((tempTime.getMinutes() > 9) ? tempTime.getMinutes() : "0" + tempTime.getMinutes());
                //
                //    //adding slot_time to get end_time
                //
                //    var str = "";
                //
                //    for (var time in timeArr) {
                //        if (timeArr[time] == "available" && tempTime <= time)
                //            str += "<option value='" + time + "'>" + time + "</option>";
                //    }
                //    $("#calender_endTime").html(str);
                //});

                <?php
                if (!empty($_GET['login'])) {
                    trackEvent('Login Tutor', 'Click', 'Success');
                }
                ?>

            }); //main $()
            function availability(dateOriginal, number, checked) {
                var modelLabel = "";

                var dateArray = dateOriginal.split('-');

                _availabilityDate = dateArray[2] + '/' + dateArray[1] + '/' + dateArray[0];
                _dateOriginal = dateOriginal;
                _checked = checked;

                if (number == 1) {
                    modelLabel = "Weet je zeker dat je niet beschikbaar wilt zijn op " + _availabilityDate;
                    _notficationMsg = "Beschikbaarheid is uitgezet";
                    _availType = 1;
                }
                if (number == 0) {
                    modelLabel = "Weet je zeker dat je weer beschikbaar wilt zijn op " + _availabilityDate;
                    _notficationMsg = "Beschikbaarheid is aangezet";
                    _availType = 0;
                }

                $("#myModalLabel3").text(modelLabel);
                $('#availability-delete-type').appendTo("body").modal('show');
            }


            function availabilityy(date, _availType, checked) {

                var teacherid =<?php echo $_SESSION['TeacherID']; ?>;

                if (checked == 'checked') {
                    var url = '/api/Calendar/RemoveAbsentDay';
                } else {
                    var url = '/api/Calendar/AddAbsentDay';
                }

                $.ajax({
                    type: "POST",
                    url: url,
                    data: {teacherID: teacherid, date: date},
                    dataType: "json",
                    success: function (response) {
                        showNotification("alert-success", _notficationMsg, "bottom", "center", "", _timer);
                        setTimeout(function () {
                           document.location.reload();
                        }, 2500);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                    }
                });// ends ajax


            }
            function removeButton(obj) {
                $(obj).find(".inline-elem-buttons-box").remove();
            }


            function addButton(obj, id, name) {
                var cases = $(obj).attr("data-event");
                var btn = '';

                if (name == 1) {
                    btn = '<span class="inline-elem-buttons-box">\
               <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, ' + id + ', ' + name + ')">AFSPRAAK ANNULEREN</button>\
            </span>';
                } else if (name == 2) {
                    btn = '<span class="inline-elem-buttons-box">\
               <button type="button" class="inline-elem-button btn btn-raised btn-primary btn-round waves-effect" value="confirm" onclick="confirmSlot(this, ' + id + ', ' + name + ')">VERZOEK ACCEPTEREN</button> \
               <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, ' + id + ', ' + name + ')">VERZOEK WEIGEREN</button>\
            </span>';
                } else if (name == 3) {
                    btn = '<span class="inline-elem-buttons-box">\
               <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, ' + id + ', ' + name + ')">Annuleer onbeschikbaarheid</button>\
            </span>';
                }

                $(obj).append(btn);

            }// addButton function

            function cancelSlot(obj, id, name) {

                _obj = obj;
                _id = id;
                _name = name;
                var modelLabel = "";

                if (name == 1) {
                    modelLabel = "Weet je zeker dan je de bijles wilt annuleren?";
                    _notficationMsg = "De bijles is verwijderd";
                    _timer = 3;
                } else if (name == 2) {
                    modelLabel = "WEET JE ZEKER DAT JE HET VERZOEK WILT WEIGEREN?";
                    _notficationMsg = "<b style='color:white !important;'>Je hebt het bijlesverzoek afgewezen!</b><br/>\
Je vindt het verzoek en informatie over de leerling terug bij je geweigerde afspraken. Indien je dat nog niet hebt gedaan, is het soms verstandig om hierover contact met de leerling of ouder op te nemen. Kan jij deze leerling nooit te hulp staan? Verwijs deze dan door naar de 'vind docent' pagina (www.bijlesaanhuis.nl/docenten) of onze helpdesk (www.bijlesaanhuis.nl/contact). Indien je helemaal niet meer beschikbaar bent voor nieuwe leerlingen, geef je dat aan bij <a href='/app/edit-profile' style='color:#5CC75F'>`Profiel Aanpassen`</a>.";
                    _timer = 99999;
                } else if (name == 3) {
                    modelLabel = "Wil je deze beschikbaarheid verwijderen?";
                    _notficationMsg = "Beschikbaar is verwijderd";
                    _timer = 3;
                }

                $("#myModalLabel").text(modelLabel);
                $('#modal-delete-type').appendTo("body").modal('show');


                var elem = $(obj).parent().parent();
                _ddate = elem.find(".event-item").attr("data-date");
                var time = elem.find(".inline_time").text();
                time = (time.trim()).split(" ");
                _stime = time[0];
                _etime = time[2];

            }

            function cancelSlott(obj, id, name) {

                var subpage, cbID = '';
                if (name == 1) {
                    subpage = "cancelBook";
                    cbID = id;
                } else if (name == 2) {
                    subpage = "cancelPending";
                    cbID = id;
                } else if (name == 3) {
                    subpage = "reactivatefreeSlot";
                    cbID = id;
                }

                $.ajax({
                    type: "POST",
                    url: "teacher-ajaxcalls.php",
                    data: {subpage: subpage, cbID: cbID, ddate: _ddate, stime: _stime, etime: _etime},
                    dataType: "json",
                    success: function (response) {
                        if (response.trim() == "success") {
                            if (_notficationMsg) {
                                showNotification("alert-success", _notficationMsg, "bottom", "center", "", "", _timer);
                            } else {
                                $('#modal-information-type .modal-title').text('Je hebt het bijlesverzoek afgewezen!');
                                //$('#modal-information-type .modal-text').html("Je vindt het verzoek en informatie over de leerling terug bij je geweigerde afspraken. Indien je dat nog niet hebt gedaan, is het soms verstandig om hierover contact met de leerling of ouder op te nemen. Kan jij deze leerling nooit te hulp staan? Verwijs deze dan door naar de `vind docent` pagina (www.bijlesaanhuis.nl/docenten) of onze helpdesk (www.bijlesaanhuis.nl/contact). Indien je helemaal niet meer beschikbaar bent voor nieuwe leerlingen, geef je dat aan bij <a href='/app/edit-profile' style='color:#5CC75F'>`Profiel aanpassen`</a>.");
                                $('#modal-information-type .modal-text').html("De leerling wordt hierover door ons standaard per e-mail geïnformeerd, of persoonlijk per telefoon indien het een nieuwe leerling betreft. Hierdoor hoef jij de leerling dus niet meer te informeren over deze afwijzing. Indien je helemaal niet meer beschikbaar bent voor leerlingen, geef dat dan aan op de <a href='/app/edit-profile' style='color:#5CC75F !important;'>Profiel Aanpassen</a> pagina.");
                                $('#modal-information-type').appendTo("body").modal('show');
                            }

                            reload(<?php echo $_SESSION['TeacherID'];?>);
                            calendar = $('#calendar').calendar(options);
                        } else {
                            showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                        }
                        $(document.body).css({'cursor': 'default'});
                        $("#submitBtn").prop("disabled", false);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        if ((xhr.responseText).trim() == "success") {
                            if (_notficationMsg) {
                                showNotification("alert-success", _notficationMsg, "bottom", "center", "", "", _timer);
                            } else {
                                $('#modal-information-type .modal-title').text('Je hebt het bijlesverzoek afgewezen!');
                                //$('#modal-information-type .modal-text').html("Je vindt het verzoek en informatie over de leerling terug bij je geweigerde afspraken. Indien je dat nog niet hebt gedaan, is het soms verstandig om hierover contact met de leerling of ouder op te nemen. Kan jij deze leerling nooit te hulp staan? Verwijs deze dan door naar de `vind docent` pagina (www.bijlesaanhuis.nl/docenten) of onze helpdesk (www.bijlesaanhuis.nl/contact). Indien je helemaal niet meer beschikbaar bent voor nieuwe leerlingen, geef je dat aan bij <a href='/app/edit-profile' style='color:#5CC75F'>`Profiel aanpassen`</a>.");
                                $('#modal-information-type .modal-text').html("De leerling wordt hierover door ons standaard per e-mail geïnformeerd, of persoonlijk per telefoon indien het een nieuwe leerling betreft. Hierdoor hoef jij de leerling dus niet meer te informeren over deze afwijzing. Indien je helemaal niet meer beschikbaar bent voor leerlingen, geef dat dan aan op de <a href='/app/edit-profile' style='color:#5CC75F !important;'>Profiel Aanpassen</a> pagina.");

                                $('#modal-information-type').appendTo("body").modal('show');
                            }
                            reload(<?php echo $_SESSION['TeacherID'];?>);
                            calendar = $('#calendar').calendar(options);
                        } else {
                            showNotification("alert-danger", "Er ging iets mis. Indien dit blijft gebeuren, kan je altijd contact met ons opnemen.", "bottom", "center", "", "");
                        }
                        $(document.body).css({'cursor': 'default'});
                        $("#submitBtn").prop("disabled", false);

                    }
                });// ends ajax
            }


            function booking() {
                cancelfreeSlot();
            }

            function cancelfreeSlot() {

                var stime = $("#calender_startTime").val();
                var etime = $("#calender_endTime").val();

                if (!stime || !etime)
                    return;

                //converting into date, so we can get the minutes after subtracting
                var timeStart = new Date("01/01/2007 " + stime);
                var timeEnd = new Date("01/01/2007 " + etime);

                var minutes = 60 * 1000; //sec * millisec


                var timeArr = Global_Variable_timeRange[__calender_date];

                var slotTime = Array();
                var slotDetail = Array();
                var milliseconds = window.performance.now();
                var isTimeStart = false;
                var isTimeEnd = false;
                var prevTime = null;
                var strTime = null;
                var tempTime = null;

                for (var time in timeArr) {
                    //adding slot_time to get end_time
                    tempTime = new Date("01/01/2007 " + time);
                    tempTime = new Date(tempTime.getTime() + <?php echo SLOT_TIME_IN_MINIUTES * 60 * 1000; //millisec?>);
                    tempTime = ((tempTime.getHours() > 9) ? tempTime.getHours() : "0" + tempTime.getHours()) + ":" + ((tempTime.getMinutes() > 9) ? tempTime.getMinutes() : "0" + tempTime.getMinutes());

                    strTime = time + "-" + tempTime;
                    //adding slot_time to get end_time

                    /* if(isTimeEnd){
                         if(timeArr[time]== "available")
                               slotTime.push({isGapSlot:1,  time:strTime , date:__calender_date});
                         break;
                     }

                     if(time == etime){
                         slotTime.push({isGapSlot:0,  time:strTime , date:__calender_date});
                         isTimeEnd = true;
                         continue;
                     }
                     */

                    if (time == etime) {
                        if (timeArr[time] == "available")
                            slotTime.push({isGapSlot: 1, time: strTime, date: __calender_date});
                        break;
                    }

                    if (isTimeStart && timeArr[time] == "unavailable") {
                        showNotification("alert-danger", "Deze combinatie van begin- en eindtijd is niet mogelijk, omdat door een andere leerling een bijles is geboekt tussen deze twee tijden. Vraag ons gerust om hulp.", "bottom", "center", "", "");
                        return;
                    }

                    if (isTimeStart && timeArr[time] == "available") {
                        slotTime.push({isGapSlot: 1, time: strTime, date: __calender_date});
                        continue;
                    }

                    if (time == stime) {
                        if (prevTime) {
                            slotTime.push({isGapSlot: 1, time: prevTime + "-" + time, date: __calender_date});
                        }
                        slotTime.push({isGapSlot: 1, time: strTime, date: __calender_date});
                        isTimeStart = true;
                        continue;
                    }

                    if (!isTimeStart && timeArr[time] == "available")
                        prevTime = time;
                    else if (timeArr[time] == "unavailable")
                        prevTime = null;

                } //ends for()

                var teacherid =<?php echo $_SESSION['TeacherID']; ?>;

                slotDetail.push({slotGID: (teacherid + milliseconds), slotDetail: slotTime});

                $.ajax({
                    type: "POST",
                    url: "teacher-ajaxcalls.php",
                    data: {subpage: "cancelfreeSlot", teacherid: teacherid, slotData: slotDetail},
                    dataType: "json",
                    success: function (response) {
                        if (response.trim() == "success") {
                            showNotification("alert-success", "Beschikbaar is verwijderd", "bottom", "center", "", "");
                            calendar = $('#calendar').calendar(options);
                        } else {
                            showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                        }
                        $(document.body).css({'cursor': 'default'});
                        $("#submitBtn").prop("disabled", false);
                    },
                    error: function (xhr, ajaxOptions, thrownError) {
                        if ((xhr.responseText).trim() == "success") {
                            showNotification("alert-success", "Beschikbaar is verwijderd", "bottom", "center", "", "");
                            calendar = $('#calendar').calendar(options);
                        } else {
                            showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                        }
                        $(document.body).css({'cursor': 'default'});
                        $("#submitBtn").prop("disabled", false);
                    }// func ends
                });// ends ajax
            }// func ends

            function __onclick() {
                var x = 1;
            }



        </script>

        <style>
            .modal__content {
                top: 25% !important;
                width: 60% !important;
            }

            .modal__body {
                padding: 5% !important;
            }
        </style>


    </body>
    </html>

    <div class="modal modal-dialog-centered fade"
         id="modal-delete-type"
         tabindex="-1"
         role="dialog"
         aria-labelledby="myModalLabel"
         aria-hidden="true" style="display:none;">
        <div class="modal__content">
            <div class="modal__header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <h6 class="modal__title" id="myModalLabel">Weet je zeker dat je de bijles wilt annuleren?</h6>
            </div>
            <div class="modal__body">
                <div id="deresulttype"></div>
                <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                    <input type="hidden" id="delete-id-type" name="delete-id-type"/>
                    <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                    <button type="button" class="btn btn-success" id="delete-yes-type">Ja</button>
                    <button type="button" class="btn btn-danger" id="delete-no-type">Nee</button>
                </form>
            </div>
        </div>
    </div>

    <div class="modal modal-dialog-centered fade" id="appointment-delete-type" style="display: none;" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal__content">
            <div class="modal__header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <h6 class="modal__title" id="myModalLabel2">Weet je zeker dat je het verzoek wilt weigeren?</h6>
            </div>
            <div class="modal__body">
                <div id="deresulttype"></div>
                <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                    <input type="hidden" id="delete-id-type" name="delete-id-type"/>
                    <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                    <button type="button" class="btn btn-success" id="delete-yes-type2">Ja</button>
                    <button type="button" class="btn btn-danger" id="delete-no-type2">Nee</button>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-dialog-centered fade" id="availability-delete-type" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
        <div class="modal__content">
            <div class="modal__header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <h6 class="modal__title" id="myModalLabel3">WEET JE ZEKER DAT JE HET VERZOEK WILT WEIGEREN?</h6>
            </div>
            <div class="modal__body">
                <div id="deresulttype"></div>
                <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                    <input type="hidden" id="delete-id-type" name="delete-id-type"/>
                    <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                    <button type="button" class="btn btn-success" id="set-no-availability">Ja</button>
                    <button type="button" class="btn btn-danger" id="availability-no-type">Nee</button>
                </form>
            </div>
        </div>
    </div>
    <div class="modal modal-dialog-centered fade" id="modal-information-type" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true" style="display:none;">
        <div class="modal__content">
            <div class="modal__header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <h6 class="modal__title modal-title" id="myModalLabel"></h6>
            </div>
            <div class="modal__body">
                <div id="deresulttype"></div>
                <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                    <input type="hidden" id="delete-id-type" name="delete-id-type"/>
                    <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                    <h6 class="modal-text modal__text--appointment-cancel"></h6>
                </form>
            </div>
        </div>
    </div>
    <div id="fullCalModal" class="modal modal-dialog-centered fade" style="display: none;">
        <div class="modal__content">
            <div class="modal__header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span> <span
                            class="sr-only">close</span></button>
                <h4 id="modalTitle" class="modal__title"></h4>
            </div>
            <div id="modalBody" class="modal__body"></div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>

    <?php
}

?>

<script type="application/javascript">
    $('#modal-information-type').on('hidden.bs.modal', function () {
        location.reload();
    })
</script>
