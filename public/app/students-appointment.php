<?php


use API\Endpoint\Calendar;

require_once('includes/connection.php');
require_once('../../api/endpoint/Calendar.php');
$thisPage = "Appointment";
session_start();
if (!isset($_SESSION['Student'])) {
    header('Location: index.php');
} else {
    $query4 = "DELETE FROM `calendarbooking` WHERE `datee` <= CURDATE() AND `studentID` = " . $_SESSION['StudentID'] . " AND `accepted` = 0";
    mysqli_query($con, $query4);

    $str = "";
    $cal = new Calendar($db, ['studentID' => $_SESSION['StudentID']]);
    $result = $cal->GetAppointments(['history' => 1]);
    $outputArray = $result->getResult();

    $i = 1;
    foreach ($outputArray as $key => $value) {
        $pending = isset($value['pending']) ? $value['pending'] : null;
        $accepted = isset($value['accepted']) ? $value['accepted'] : null;
        $deletable = isset($value['deletable']) ? $value['deletable'] : null;
        $history = isset($value['history']) ? $value['history'] : null;
        if ($pending) {
            foreach ($pending as $key2 => $value2) {
                $userID = isset($value2['userID']) ? $value2['userID'] : null;
                $forename = isset($value2['forename']) ? $value2['forename'] : null;
                $begin = isset($value2['begin']) ? $value2['begin'] : null;
                $end = isset($value2['end']) ? $value2['end'] : null;
                $telephone = isset($value2['telephone']) ? $value2['telephone'] : null;
                $email = isset($value2['email']) ? $value2['email'] : null;
                $address = isset($value2['address']) ? $value2['address'] : null;
                $value_id = isset($value2['id']) ? $value2['id'] : null;
                $deletable = isset($value2['deletable']) ? $value2['deletable'] : null;
                $str .= "
                        <tr>
                                <td class='hide-on-mobile'>$i</td>
                                <td><a href='teacher-detailed-view?tid=" . $userID . "'>" . $forename . "</a></td>
                                <td>" . $key . " " . $begin . " - " . $end . "</td>
                                <td>" . $address . "</td>
                                <td>
                                <button type='button' class='action-button inline-elem-button btn btn-raised btn-warning btn-round waves-effect' value='cancel' ";
                if ($deletable) $str .= "onclick='cancelSlot($(this), " . $value_id . ",1)'";
                else $str .= "disabled";
                $str .= ">VERZOEK ANNULEREN</button>
                                </td>
                        </tr>
                        ";
                $i++;
            }
        }
    }

    $i = 1;
    $str2 = "";
    foreach ($outputArray as $key => $value) {
        $pending = isset($value['pending']) ? $value['pending'] : null;
        $accepted = isset($value['accepted']) ? $value['accepted'] : null;
        $deletable = isset($value['deletable']) ? $value['deletable'] : null;
        $history = isset($value['history']) ? $value['history'] : null;
        if ($accepted) {
            foreach ($accepted as $key2 => $value2) {
                $userID = isset($value2['userID']) ? $value2['userID'] : null;
                $forename = isset($value2['forename']) ? $value2['forename'] : null;
                $begin = isset($value2['begin']) ? $value2['begin'] : null;
                $end = isset($value2['end']) ? $value2['end'] : null;
                $telephone = isset($value2['telephone']) ? $value2['telephone'] : null;
                $email = isset($value2['email']) ? $value2['email'] : null;
                $address = isset($value2['address']) ? $value2['address'] : null;
                $value_id = isset($value2['id']) ? $value2['id'] : null;
                $deletable = isset($value2['deletable']) ? $value2['deletable'] : null;
                $str2 .= "
                        <tr>
                                <td class='hide-on-mobile'>$i</td>
                                <td><a href='teacher-detailed-view?tid=" . $userID . "'>" . $forename . "</a></td>
                                <td>" . $key . " " . $begin . " - " . $end . "</td>
                                <td>" . $address . "</td> 
                                <td>
                                <button type='button' class='action-button inline-elem-button btn btn-raised btn-warning btn-round waves-effect' value='cancel' ";
                if ($deletable) $str2 .= "onclick='cancelSlot($(this), " . $value_id . ",2)'";
                else $str2 .= "disabled";
                $str2 .= ">AFSPRAAK ANNULEREN</button></td>
                        </tr>
                        ";
                $i++;
            }
        }
    }

    $query3 = "SELECT cb.teacherID, MIN(cb.starttime)stime, MAX(cb.endtime)etime, cb.datee AS daatee, cb.calendarbookingID AS cbid, cb.accepted, cb.place_name, cb.slot_group_id, (SELECT c.firstname FROM contact c WHERE cb.teacherID=c.userID ORDER BY `datee` DESC LIMIT 1)teacher FROM calendarbooking cb where cb.accepted=1 AND cb.isClassTaken=1 AND cb.isgapslot=0 AND cb.studentID={$_SESSION['StudentID']} GROUP BY slot_group_id ORDER BY cb.datee DESC, cb.starttime ASC";

    $result3 = mysqli_query($con, $query3);

    $str3 = "";
    $i = 0;
    while ($row = mysqli_fetch_assoc($result3)) {
        $i++;
        $str3 .= "<tr>
                        <td class='hide-on-mobile'>$i</td>
                        <td><a href='teacher-detailed-view.php?tid={$row['teacherID']}'>{$row['teacher']}</a></td>
                        <td>{$row['daatee']} {$row['stime']} - {$row['etime']}</td>
                        <td>{$row['place_name']}</td>
                        </tr>
                        ";
    }
    ?>

    <!doctype html>
    <html class="no-js " lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description"
              content="Een overzicht van alle geboekte bijlessen per maand | Bijles Aan Huis; kwaliteit, innovatie en betrouwbaarheid.">
        <title>Afspraken | Bijles Aan Huis</title>
        <?php
        require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
        ?>
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css"
              rel="stylesheet"/>
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/>
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet'/>
        <link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print'/>
        <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">

        <!-- New  Style -->
        <script src="assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
        <script src="assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
        <script src="assets/js/pages/tables/jquery-datatable.js"></script>
        <script src="https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>

        <link rel="stylesheet" href="assets/css/main.css">
        <link rel="stylesheet" href="assets/css/color_skins.css">
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

        <!-- New  Style -->

        <style type="text/css">

            .card.calendar-card {
                background-color: #FFFFFF;
                border: 2px solid #f1f1f1;
                border-radius: 6px;
            }

            .card.calendar-card .header {
                background-color: #F1f1f1;
                padding: 20px 40px;

            }

            .card.calendar-card .header .ma-subtitle {
                font-family: 'Poppins', sans-serif !important;
                font-weight: 600;
                font-size: 21px !important;
            }

            .card.calendar-card .header .ma-subtitle::before {
                content: none;
            }

            .card.calendar-card .body {
                padding: 0;
            }

            .card.calendar-card .tab-content {
                padding: 20px 40px;
            }

            @media (max-width: 576px) {
                .card.calendar-card .tab-content {
                    padding: 20px 11px;
                }
            }

            .card.calendar-card .tab-content .card .body .col-sm-12 {
                overflow-x: auto;
            }

            .card.calendar-card .tab-content .active {
                background: transparent !important;
                /* padding:20px 40px; */
            }

            .card.calendar-card .nav-tabs {
                padding: 20px 40px;
            }


            .card.calendar-card .card {
                background-color: #FFFFFF;
            }

            .card.calendar-card .card table {
                background-color: #F1F1F1;
            }

            .theme-green .action-button {
                width: 180px !important;
                margin-bottom: 10px !important;
            }

            .theme-green .action-button.btn-primary {
                color: #FFFFFF !important;
                background-color: #5CC75F !important;
                font-weight: normal !important;
            }

            .theme-green .action-button.btn-primary:hover {
                box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.17);
            }

            .theme-green .action-button.btn-warning {
                color: #FFFFFF !important;
                background-color: #FFBD6B !important;
                font-weight: normal !important;
            }

            .theme-green .action-button.btn-warning:hover {
                box-shadow: 0 3px 8px 0 rgba(0, 0, 0, 0.17);
            }


            .modal-dialog {
                margin-top: 70px;
            }

            /*Placeholder Color */
            input {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            select {
                border: 1px solid #bdbdbd !important;

                color: white !important;
            }

            input:focus {
                background: transparent !Important;
            }

            select:focus {
                background: transparent !Important;
            }

            .wizard .content {
                /*overflow-y: hidden !important;*/
            }

            .wizard .content label {

                color: white !important;

            }

            .wizard > .steps .current a {
                background-color: #029898 !Important;
            }

            .wizard > .steps .done a {
                background-color: #828f9380 !Important;
            }

            .wizard > .actions a {
                background-color: #029898 !Important;
            }

            .wizard > .actions .disabled a {
                background-color: #eee !important;
            }

            button.btn.btn-simple {
                border-color: #486066 !important;
            }

            .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
                color: white;
            }

            table {
                color: white;
            }

            div.show .dropdown-menu {
                display: block;
            }


            .multiselect.dropdown-toggle.btn.btn-default {
                display: none !important;
            }


            .nav-tabs {
                border: 0;
                padding: 15px .7rem;
                border-bottom: 1px solid #486066 !important;
            }

            .theme-green .nav-tabs .nav-link.active {
                color: white !important;
                background-color: #5cc75f !important;
                border: 2px solid #5cc75f !important;
            }

            .fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
                color: black !important;
            }

            .theme-green .page-loader-wrapper {
                background: #FAFBFC;
            }

            select.form-control.form-control-sm {
                color: black !important;
                padding-bottom: 0;
            }

            .card .body {
                background-color: #FAFBFC !important;
            }

            select.form-control.form-control-sm {
                color: #486066 !important;
                padding: 0;
            }

            .btn, .theme-green .btn-primary, .pagination .page-item .page-link {
                border-radius: 6px !important;
            }

            .btn-warning {
                margin: 0 !important;
                color: #FFBD6B !important;
                border: 2px solid #FFBD6B !important;
            }

            .btn-warning:hover {
                background-color: #FFBD6B !important;
            }

            /* extra popup modal styling */
            .modal h6 {
                text-transform: lowercase;
            }

            .modal .modal-title {
                font-size: 18px !important;
                font-weight: bold !important;
            }

            .modal .modal-text {
                padding: 0 20px 27px 20px;
                padding: 18%;
            }

            .modal-content .modal-header button {
                right: 12px !important;
                top: 6px !important;
            }

            .custom-padding {
                padding: 0 0 1rem 0 !important;
                margin-top: 1rem;
            }

            section.content {
                margin: 60px 0px 15px 60px;
            }

            .row:before, .row:after {
                display: none !important;
            }

            .tab-content .dataTables_wrapper div.row .dataTables_filter input {
                height: 29px;
            }

            @media screen and (max-width: 870px) {
                .tab-content .dataTables_wrapper div.row .dataTables_filter input {
                    max-width: 80%;
                }

                .tab-content .dataTables_wrapper div.row .col-6, .tab-content .dataTables_wrapper div.row .col-sm-12.col-md-6 {
                    max-width: 50%;
                }
            }

            @media screen and (max-width: 768px) {
                .hide-on-mobile {
                    display: none !important;
                }

                .tab-content .dataTables_wrapper div.row .dataTables_filter input {
                    max-width: 60%;
                }

                .card .body .nav-tabs {
                    padding-left: 10px;
                    padding-right: 10px;
                }

                .card .body .nav-tabs .nav-item {
                    width: 33.333%;
                }

                .nav-tabs > .nav-item > .nav-link {
                    padding: 11px 7px;
                }
            }

            @media screen and (max-width: 576px) {
                .card .body .nav-tabs {
                    padding-left: 3px;
                    padding-right: 0px;
                }
            }

            @media screen and (max-width: 480px) {
                .card .body .nav-tabs {
                    padding-left: 15px;
                    padding-right: 15px;
                }

                .card .body .nav-tabs .nav-item {
                    width: 90%;
                    margin-left: 5%;
                }
            }

            .modal__content {
                width: 60% !important;
            }

            .modal__body {
                padding: 5% !important;
            }

            /* Why not deleted? : Somehow the datatables_length dropdown is misformed without it, esspecially firefox*/
            tfoot {
                display: none;
            }

            .modal {
                padding-top: 200px !important;
            }

            .modal-dialog-centered {
                display: -webkit-box;
                display: -ms-flexbox;
                display: flex;
                -webkit-box-align: center;
                -ms-flex-align: center;
                align-items: center;
                min-height: calc(100% - (.5rem * 2));
            }

            @media (min-width: 576px) {
                .modal-dialog-centered {
                    min-height: calc(100% - (1.75rem * 2));
                }
            }

        </style>
        <?php
        $activePage = basename($_SERVER['PHP_SELF']);
        ?>
    </head>
    <body class="theme-green">
    <!-- Page Loader -->
    <div class="page-loader-wrapper" style="background: #FAFBFC !important;">
        <div class="loader">
            <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48"
                                     alt="Oreo"></div>
            <p></p>
        </div>
    </div>

    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>


    <?php
    require_once('includes/header.php');
    ?>

    <style>
        @media screen and (min-width:576px) {
            section.content {
                padding-right:4%;
                padding-left:4%;
            }
        }
    </style>
    <!-- Main Content -->
    <section class="content page-calendar">
        <div class="block-header" style="display:none">
            <?php require_once('includes/studentTopBar.php'); ?>
        </div>
        <div class="container ma-sidebar-container" style="margin-top:0 !important;">

            <div class="row clearfix">
                <div class="col-xl-12 col-lg-12 col-md-12">
                    <div class="card calendar-card">
                        <div class="header" style="background-color:#F1F1F1;">
                            <h2 class="ma-subtitle">Afspraken</h2>

                        </div>
                        <div class="body">
                            <!-- Nav tabs -->
                            <ul class="nav nav-tabs">
                                <li class="nav-item"><a class="nav-link btn-primary active" data-toggle="tab"
                                                        href="#pending">IN&nbsp;AFWACHTING</a></li>
                                <li class="nav-item"><a class="nav-link btn-primary" data-toggle="tab" href="#confirm">GEACCEPTEERD</a>
                                </li>
                                <li class="nav-item"><a class="nav-link btn-primary" data-toggle="tab" href="#complete">GEWEEST</a>
                                </li>
                            </ul>
                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div role="tabpanel" class="tab-pane in active" id="pending">
                                    <!-- Exportable Table -->
                                    <div class="row clearfix">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="body">
                                                    <table class="table table-bordered table-striped js-basic-example table-hover dataTable">
                                                        <thead>
                                                        <tr>
                                                            <th class='hide-on-mobile'>#</th>
                                                            <th>Naam docent</th>
                                                            <th>Datum en tijd</th>
                                                            <th>Adres</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tfoot>
                                                        <tr>
                                                            <th class='hide-on-mobile'>#</th>
                                                            <th>Naam docent</th>
                                                            <th>Datum en tijd</th>
                                                            <th>Adres</th>
                                                            <th></th>
                                                        </tr>
                                                        </tfoot>
                                                        <tbody>
                                                        <?php echo $str; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- #END# Exportable Table -->
                                </div>

                                <div role="tabpanel" class="tab-pane" id="confirm">
                                    <!-- Exportable Table -->
                                    <div class="row clearfix">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="body">
                                                    <table class="table table-bordered table-striped js-basic-example table-hover dataTable">
                                                        <thead>
                                                        <tr>
                                                            <th class='hide-on-mobile'>#</th>
                                                            <th>Naam docent</th>
                                                            <th>Datum en tijd</th>
                                                            <th>Adres</th>
                                                            <th></th>
                                                        </tr>
                                                        </thead>
                                                        <tfoot>
                                                        <tr>
                                                            <th class='hide-on-mobile'>#</th>
                                                            <th>Naam docent</th>
                                                            <th>Datum en tijd</th>
                                                            <th>Adres</th>
                                                            <th></th>
                                                        </tr>
                                                        </tfoot>
                                                        <tbody>
                                                        <?php echo $str2; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- #END# Exportable Table -->
                                </div>

                                <div role="tabpanel" class="tab-pane" id="complete">
                                    <!-- Exportable Table -->
                                    <div class="row clearfix">
                                        <div class="col-lg-12">
                                            <div class="card">
                                                <div class="body">
                                                    <table class="table table-bordered table-striped js-basic-example table-hover dataTable">
                                                        <thead>
                                                        <tr>
                                                            <th class='hide-on-mobile'>#</th>
                                                            <th>Naam docent</th>
                                                            <th>Datum en tijd</th>
                                                            <th>Adres</th>
                                                        </tr>
                                                        </thead>
                                                        <tfoot>
                                                        <tr>
                                                            <th class='hide-on-mobile'>#</th>
                                                            <th>Naam docent</th>
                                                            <th>Datum en tijd</th>
                                                            <th>Adres</th>
                                                        </tr>
                                                        </tfoot>
                                                        <tbody>
                                                        <?php echo $str3; ?>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- #END# Exportable Table -->
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

            </div>
        </div>
    </section>
    <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
    <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


    <?php
    require_once('includes/footerScriptsAddForms.php');
    ?>
    <!-- <script src='assets/plugins/fullcalendar/lib/moment.min.js'></script> -->
    <!-- <script src='assets/plugins/fullcalendar/fullcalendar.min.js'></script> -->
    <!-- <script src='assets/plugins/fullcalendar/lib/moment.min.js'></script> -->
    <!--<script src="assets/js/pages/calendar/calendar.js"></script>-->
    <!-- <script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script> -->

    <script>


    </script>
    <!-- new script --><!-- Jquery DataTable Plugin Js -->
    <script src="assets/bundles/datatablescripts.bundle.js"></script>
    <script src="assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
    <script src="assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
    <script src="assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
    <script src="assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
    <script src="assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>

    <script src="assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->
    <script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
    <script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
    <!-- new script -->
    <script type="text/javascript">


        var _obj = null;
        var _id = null;
        var _name = null;
        var _label = "";
        var _modalLabel = "";
        $(document).ready(function () {
            // $(".inline-elem-button").click(function(){
            //     var btn = $(this).val();
            //     alert("Ajax call for "+ btn);
            // });

            $(".page-loader-wrapper").css("display", "none");


            $("#delete-yes-type").click(function () {
                cancelPending(_obj, _id);
                $("#modal-delete-type").modal('hide');
            });

            $("#delete-no-type").click(function () {
                $("#modal-delete-type").modal('hide');
                _obj = null;
                _id = null;
            });
            setTimeout(function () {
                var temporaryHTML = $('.tab-content .dataTables_wrapper div.row .dataTables_length label').html().replace(' weergeven', '<span class="hide-on-mobile"> weergeven</span>');
                $('.tab-content .dataTables_wrapper div.row .dataTables_length label').html(temporaryHTML);
                $('.tab-content .dataTables_wrapper .dataTables_filter, .tab-content .dataTables_wrapper .dataTables_length').parent().addClass('col-6');
                $('.tab-content .dataTables_wrapper .dataTables_filter, .tab-content .dataTables_wrapper .dataTables_length').parent().removeClass('col-sm-12 col-md-6');
            }, 1000);

        });

        function cancelSlot(obj, id, name) {
            _obj = obj;
            _id = id;

            if (name == 2) {
                _label = "De bijles is geannuleerd";
                _modalLabel = "Weet je zeker dat je de bijles wilt annuleren?";
            } else {
                _label = "Het verzoek is geannuleerd";
                _modalLabel = "Weet je zeker dat je het verzoek wilt annuleren?";
            }


            $('#modal-delete-type').appendTo("body").modal('show');
            $("#myModalLabel").text(_modalLabel);
        }

        function cancelPending(obj, id) {
            var url = "/api/Calendar/CancelTutor";
            $.ajax({
                type: "POST",
                url: url,
                data: {id: id},
                dataType: "json",
                success: function (response) {

                    $('#modal-information-type .modal-title').text('Je hebt het bijlesverzoek afgewezen!');
                    $('#modal-information-type .modal-text').html("De leerling wordt hierover door ons standaard per e-mail geïnformeerd, of persoonlijk per telefoon indien het een nieuwe leerling betreft. Hierdoor hoef jij de leerling dus niet meer te informeren over deze afwijzing. Indien je helemaal niet meer beschikbaar bent voor leerlingen, geef dat dan aan op de <a href='/app/edit-profile' style='color:#5CC75F !important;'>Profiel aanpassen</a> pagina.");
                    $('#modal-information-type').appendTo("body").modal('show');
	                obj.closest('tr').remove();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
                }
            });// ends ajax
        }


    </script>

    </body>
    </html>
    <div class="modal modal-dialog-centered fade" id="modal-delete-type" tabindex="-1" role="dialog"
         aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal__content">
                <div class="modal__header">
                    <button type="button" class="close" data-dismiss="modal"><span
                                aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h6 class="modal__title" id="myModalLabel">Weet je zeker dat je de afspraak wilt verwijderen?</h6>
                </div>
                <div class="modal__body">
                    <div id="deresulttype"></div>
                    <form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
                        <input type="hidden" id="delete-id-type" name="delete-id-type"/>
                        <input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType"/>
                        <button type="button" class="btn btn-success" id="delete-yes-type">Ja</button>
                        <button type="button" class="btn btn-danger" id="delete-no-type">Nee</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <div id="fullCalModal" class="modal modal-dialog-centered fade">
        <div class="modal-dialog">
            <div class="modal__content">
                <div class="modal__header">
                    <button type="button" class="inline-elem-button close" data-dismiss="modal"><span
                                aria-hidden="true">×</span> <span class="sr-only">close</span></button>
                    <h4 id="modalTitle" class="modal-title modal__title"></h4>
                </div>
                <div id="modalBody" class="modal__body"></div>
                <div class="modal-footer">
                    <button type="button" class="inline-elem-button btn btn-default" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>