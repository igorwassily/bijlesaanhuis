<?php

use API\Endpoint\Geo;

require_once('includes/connection.php');
require_once('../../api/endpoint/Geo.php');
$thisPage = "Teachers Profile";
session_start();

if (!isset($_SESSION['Student']) && false) {
    header('Location: index.php');
} else {
    $postCode = null;
    $lat = "";
    $long = "";
    if (!empty($_SESSION['StudentID'])) {
        $query = "SELECT * FROM contact c  WHERE userID=" . $_SESSION['StudentID'];
        $result = mysqli_query($con, $query);

        $d = mysqli_fetch_assoc($result);

        $geo = new Geo($db);

        $d['lat'] = $d['latitude'];
        $d['long'] = $d['longitude'];

        unset($d['latitude']);
        unset($d['longitude']);

        $lat = $d['lat'];
        $long = $d['long'];

        $gr = $geo->Coordinate($d);

        if ($gr->getStatus() == 200) {
            $postCode = $gr->getResult()['postalCode'];
        }
        if(isset($d['postalcode'])) {
            $postCode = $d['postalcode'];
        }

        //	    if (isset($_POST["submission"])) {
        //		    $d['latitude'] = $_POST['latitude'];
        //		    $d['longitude'] = $_POST['longitude'];
        //		    $d['place_name'] = $_POST['place_name'];
        //	    }
    }

?>

    <!doctype html>
    <html class="no-js" lang="en">

    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=Edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <meta name="description" content="Een handig overzicht van alle bijlesdocenten uit jouw regio | Alles voor het beste resultaat! | Niet goed? Geld terug!">
        <title>Docenten | Bijles Aan Huis</title>
        <?php require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php'); ?>
        <link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
        <link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
        <link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
        <link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
        <link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
        <link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">

        <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
        <link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />
        <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
        <link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>
        <link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css" />

        <script>
            var isComingFromHomePage = '';
        </script>
        <style type="text/css">
            .top__banner {
                background: #5cc75f;
                text-align: center;
                margin: 0 -1.875rem -5rem -1.875rem !important;
                padding: 6rem 10rem 10rem 10rem;
            }

            button.btn.dropdown-toggle.bs-placeholder.btn-round.btn-simple {
                border-radius: 6px !important;
                background-color: #FAFBFC !important;
                color: #486066 !important;
                border: 2px solid #F1F1F1;
            }

            button.btn.dropdown-toggle.btn-round.btn-simple {
                border-radius: 6px !important;
                background-color: #FAFBFC !important;
                color: #486066 !important;
                border: 2px solid #F1F1F1;
            }

            /* div#course-row1 {
				border: 2px solid #F1F1F1;
				border-radius: 6px;
			}*/

            .fa,
            .far,
            .fas {
                font-family: "Font Awesome 5 Free" !important;
                font-size: 16px !important;
                color: white !important;
            }

            .material-icons {
                font-size: 12px !important;
                color: white !important;
            }

            @import url('https://fonts.googleapis.com/css?family=Rokkitt:300,700');

            .banner {
                display: flex;
                align-items: center;
                padding: 1.25em 2em;
                border-radius: 5px;

                color: #fff;
                font-family: "Rokkitt";
            }

            .banner--gradient-bg {
                /*background: linear-gradient(-16deg, #809079e5, #87ca83e5 45%, #a2d0b7 45%);*/
            }

            .banner__text {
                line-height: 1.4;
                text-align: center;
                font-weight: 600;
                font-family: Poppins !important;
            }

            .custom_info_box {
                background: #5cc75f;
                border-radius: 6px;
                padding: 3px;
                color: white !important;
                font-family: Poppins !important;
                display: inline-block;
                margin-bottom: 4px;
                font-size: 12px;
            }

            .custom_info_box>small,
            strong {
                color: white !important;
                font-family: Poppins !important;
            }

            .inline-elem-button {
                margin-top: 20px;
            }

            .form-control,
            .btn-round.btn-simple {
                color: #486066 !important;
                z-index: 1070;
            }

            .input-group-addon {
                color: #486066;
            }

            .text {
                color: black;
            }

            .margin-top {
                margin-top: 10px;
            }

            .checkbox {
                float: left;
                margin-right: 20px;
            }

            .margintop {
                margin-top: 20px;
                color: #50d38a;
            }

            /*Placeholder Color */
            input {
                border: 1px solid #bdbdbd !important;
            }

            select {
                border: 1px solid #bdbdbd !important;
                /*color: white !important; */
            }

            /*	input:focus{
				background:transparent !Important;
					}

					select:focus{
				background:transparent !Important;
				}
			*/

            .wizard .content {
                /*overflow-y: hidden !important;*/
            }

            .wizard .content label {

                color: white !important;

            }

            .wizard>.steps .current a {
                background-color: #029898 !Important;
            }

            .wizard>.steps .done a {
                background-color: #828f9380 !Important;
            }

            .wizard>.actions a {
                background-color: #029898 !Important;
            }

            .wizard>.actions .disabled a {
                background-color: #eee !important;
            }

            .bootstrap-select>.dropdown-toggle.bs-placeholder,
            .bootstrap-select>.dropdown-toggle.bs-placeholder:hover,
            .bootstrap-select>.dropdown-toggle.bs-placeholder:focus,
            .bootstrap-select>.dropdown-toggle.bs-placeholder:hover {
                color: white;
            }

            table {
                color: white;
            }

            .multiselect.dropdown-toggle.btn.btn-default {
                display: none !important;
            }


            .fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
                color: black !important;
            }

            #result {
                position: absolute;
                z-index: 999;
                top: 90%;
                left: 22px;
                background-color: white;
            }

            .search-box input[type="text"],
            #result {
                width: 100%;
                box-sizing: border-box;
            }

            /* Formatting result items */
            #result a:hover {
                color: cyan !Important;
            }

            .mapboxgl-ctrl-attrib,
            .mapboxgl-ctrl-logo {
                display: none !important;
            }

            .form-control[disabled],
            .form-control[readonly],
            fieldset[disabled] .form-control {
                background-color: transparent !important;
            }

            .bootstrap-select[disabled] button {
                color: gray !important;
                border: 1px solid gray !important;
            }

            .bootstrap-select>.dropdown-toggle.bs-placeholder,
            .bootstrap-select>.dropdown-toggle.bs-placeholder:hover,
            .bootstrap-select>.dropdown-toggle.bs-placeholder:focus,
            .bootstrap-select>.dropdown-toggle.bs-placeholder:hover {
                color: white;
                outline: none !important;
            }

            .dropdown-menu>ul>li>a {
                color: #555 !important;
            }

            /* select field double print issue */

            .btn-group {
                background: transparent;
            }

            .form-control {
                background-color: transparent !important;
            }

            span.bs-caret {
                display: none !important;
            }

            div#course-row1>div>p {
                margin: 25px 0 10px;
            }

            .card .body {
                background-color: #FAFBFC !important;
            }

            .mapboxgl-ctrl-top-right {
                top: -2%;
                right: 36.5%;
                /* text-align: left !important; */
            }

            .mapboxgl-ctrl-geocoder {
                font: 15px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
                position: relative;
                background-color: #fafbfc !important;
                width: 33.3333%;
                min-width: 280px !important;
                max-width: 360px;
                z-index: 1;
                border-radius: 3px;
                border-color: #f1f1f1 !important;
            }

            @media screen and (min-width: 992px) and (max-width: 1080px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 250px !important;
                }
            }

            @media screen and (min-width: 988px) and (max-width: 991px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 409px !important;
                }
            }

            @media screen and (max-width: 987px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 408px !important;
                }
            }

            @media screen and (min-width: 970px) and (max-width: 985px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 400px !important;
                }
            }

            @media screen and (min-width: 930px) and (max-width: 969px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 380px !important;
                }
            }

            @media screen and (min-width: px) and (max-width: 929px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 360px !important;
                }
            }

            @media screen and (max-width: 768px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 290px !important;
                }
            }

            @media screen and (max-width: 425px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 325px !important;
                }
            }

            @media screen and (max-width: 375px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 275px !important;
                }
            }

            @media screen and (max-width: 320px) {
                .mapboxgl-ctrl-geocoder {
                    min-width: 220px !important;
                }
            }


            .mapboxgl-ctrl-geocoder input[type='text']::placeholder {
                color: #486066 !important;
            }

            div.school-course>div>ul>li.selected>a>span.glyphicon.glyphicon-ok.check-mark:before {
                content: "\e082";
                color: #ffbd6b;
            }

            div.school-course>div>ul>li:not(.selected)>a>span.glyphicon.glyphicon-ok.check-mark:before {
                content: "\e081";

                color: rgb(92, 199, 95);
            }

            .bootstrap-select.btn-group.school-course .dropdown-menu li a span.check-mark {
                position: absolute;
                display: inline-block !important;
                right: 15px;
                margin-top: 5px;
            }

            /* .d-flex:hover {
                box-shadow: 0 0 8px #969696 !important;
                transition: all .2s ease-in-out;
            } */

            .text-primary,
            .text-primary * {
                font-size: 21px !important;
                color: #4A6269 !important;
                font-family: Poppins !important;
            }

            .ml-101 {
                margin-left: -101px;
            }

            .ml-98 {
                margin-left: 98px;
            }

            /*@media (min-width: 1200px){.ml-101{margin-left:0px !important;} .ml-98{margin-left:0px !important;}}



	@media (min-width: 768px){.ml-101{margin-left:0px !important;} .ml-98{margin-left:0px !important;}}*/
            #onlineTutoring_line_msg {

                text-align: center;
                width: 100%;
                margin-top: 20px;
                font-weight: bold;
            }

            div.school-course>div.dropdown-menu {
                width: 130% !important;
            }

            div.school-type>div.dropdown-menu {
                width: 100% !important;
            }

            .teacherNames a {
                color: #5cc75f !important;
                font-family: Poppins !important;
                font-size: 12px;
                padding: 3px;
                display: inline-block;
                height: 30px !important;
                line-height: 45px !important;
            }

            .teacherNames a:hover {
                text-decoration: underline;
            }


            ._level {
                font-weight: 600;
                font-size: 19px;
                margin-top: 1rem;
                padding: 0 0.625rem;
                text-align: center;
            }

            .margin-top {
                margin-top: 20px !important;
            }

            .top__banner {
                background: #5cc75f;
                text-align: center;
                margin: 0 -1.875rem -5rem -1.875rem !important;
                padding: 6rem 10rem 10rem 10rem;
            }

            .top__banner h1 {
                font-size: 3.25rem !important;
                font-weight: 700;
                padding-bottom: 10px;
            }

            .top__banner h2 {
                font-size: 2.125rem !important;
                color: #FFFFFF !important;
                margin-top: 2rem;
            }

            .teachers {
                margin-top: -0.5rem;
                margin-left: 0.03125rem;
            }

            .tutor__profile {
                background: #FFFFFF !important;
                box-shadow: 0 0 12px rgba(0, 0, 0, 0.175), 0 0 0 1px rgba(63, 63, 68, 0.05);
                transition-property: box-shadow, border !important;
                transition-duration: .3s !important;
                transition-timing-function: ease-in-out !important;
                margin-bottom: .875rem !important;
                border-radius: .125rem;
                /* padding-top: 20px; */
                padding-bottom: 20px;
            }

            .tutor__profile:hover {
                box-shadow: 0 0 8px #969696 !important;
            }

            .tutor__profile .col-md-6.col-lg-7 {
                padding-top: 1.5rem;
            }

            .tutor__profile__course-price {
                display: block;
            }

            .tutor__profile__travel-cost {
                display: none;
                padding-bottom: 1rem;
            }

            .f-p {
                font-size: 13px !important;
                padding: 4px 8px !important;
            }

            .align__bottom {
                position: absolute;
                bottom: 0;
                left: 15px;
                right: 15px;
            }

            @media screen and (min-width: 768px) {
                .tutor__profile {
                    padding-bottom: 0;
                }

                .align__bottom {
                    bottom: 20px;
                }

                .container.tutor__profile .row>div {
                    padding-bottom: 10px;
                }
            }

            .tutor__profile-traveller .align__bottom {
                padding-top: 1rem;
            }

            .alignment {
                padding: 0.75rem 0 0 0.25rem;
                color: #BBB !important;
                position: absolute;
            }

            .card-title {
                text-decoration: underline;
                padding-bottom: 0 !important;
                margin-bottom: .75rem !important;
                word-wrap: break-word;
                -webkit-hyphens: auto;
                -ms-hyphens: auto;
                hyphens: auto;
            }

            .tutor__profile .text-primary {
                word-wrap: break-word;
            }

            .card-des {
                color: #7898A0 !important;
            }

            .custom_info_box {
                color: #96AFB5 !important;
            }

            .custom_info_box,
            .online__tutoring,
            .travelling__tutor {
                background: none !important;
            }

            .custom_info_box,
            .online__tutoring,
            .travelling__tutor,
            .home__icon {
                padding-right: 0.875rem;
            }

            .online__icon,
            .travel__icon,
            .home__icon {
                font-size: 3.15rem !important;
            }

            .online__icon__large,
            .travel__icon__large,
            .cross__icon__large,
            .home__icon__large {
                font-size: 8.25rem !important;
            }

            .partition {
                border-left: 1px solid #e5eaef !important;
            }

            .partition__separator__line {
                display: none;
            }

            .separator__line {
                border: 1px solid #96AFB5 !important;
                width: 100%;
            }

            .online__icon,
            .online__icon__large {
                color: #5CC75F !important;
            }

            .travel__icon,
            .travel__icon__large {
                color: #FFBD6B !important;
            }

            .home__icon {
                color: #96afb5 !important;
            }

            .no__more__tutors {
                /* display: block; */
                padding: 1rem 0;
                margin-right: 0;
                margin-left: 0;
            }

            .cross__icon__large {
                color: #F44336 !important;
            }

            .search__icon {
                color: #486066 !important;
                padding-top: 0.3125rem;
            }

            .custom__btn {
                color: #FFFFFF !important;
                border: 0.125rem solid #5CC75F;
                border-radius: 0.4375rem;
                font-family: 'Poppins', Helvetica, Arial, Lucida, sans-serif !important;
                font-weight: 600;
                text-transform: uppercase;
                background-color: #5CC75F;
                width: 100%;
                transition: 0.3s;
            }

            .custom__btn:hover {
                color: #5CC75F !important;
                background-image: initial;
                background-color: #FFFFFF;
            }

            .margin__top__five {
                margin-top: 0.3125rem;
            }

            .flex {
                display: flex;
                flex-direction: column;
                justify-content: space-between;
            }

            .teachers {
                width: 100%;
            }

            .add-on .input-group-btn>.btn {
                border-left-width: 0;
                left: -2px;
                -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
                box-shadow: inset 0 1px 1px rgba(0, 0, 0, 0.075);
            }

            /* stop the glowing blue shadow */
            .add-on .form-control:focus {
                box-shadow: none;
                -webkit-box-shadow: none;
                border-color: #cccccc;
            }

            .input[type=text] {
                color: #486066 !important;
            }

            .navbar-form {
                padding: 0;
            }

            .google__review {
                margin: 0.25rem 0 0.8125rem 0;
            }

            .quote__icon__large {
                font-size: 2rem !important;
                color: #486066 !important;
                padding-bottom: 1rem;
            }

            .outline {
                border: 4px solid #F1F1F1;
                border-radius: 6px;
                padding: 1rem;
            }

            .text-primary-bold {
                text-align: left !important;
                font-size: 1.9125rem !important;
                font-weight: bolder;
                padding-top: 1rem;
            }

            .margin__bottom__0 {
                margin-bottom: 0 !important;
            }

            .border {
                border: 2px solid #F1F1F1;
                border-radius: 6px;
                background-color: #FAFBFC !important;
                font-size: 14px;
                padding: 20px;
                font-weight: 400;
            }

            @media screen and (min-width: 1024px) {
                .docent_level_bottom {
                    padding-top: .5rem !important;
                }
            }

            @media screen and (max-width: 1024px) {
                .top__banner {
                    padding: 2rem .75rem 8rem .75rem;
                }

                .docent_level_bottom {
                    margin-right: -15px;
                    margin-left: -15px;
                }

                .docent_level_bottom.supreme {
                    font-size: 17px;
                }

                .docent_level_bottom.supreme .fa.alignment.tippy {
                    top: 13px;
                }
            }

            @media screen and (min-width: 768px) and (max-width: 992px) {
                .travel__separator .text-primary-bold {
                    padding-top: 2rem !important;
                    padding-left: 2rem !important;
                }

                .tutor__profile .col-md-6.col-lg-7 {
                    margin-left: -15px;
                    flex: 0 0 calc(50% + 15px);
                    max-width: calc(50% + 15px);
                }

                .tutor__profile .center__alignment {
                    display: flex;
                }

                .tutor__profile__travel-cost {
                    padding: 0 0 6.5rem 0;
                }
            }

            @media screen and (max-width: 768px) {
                .top__banner {
                    margin: 0 -1.5rem -6rem -1.5rem !important;
                    padding: 4rem 6rem 8rem 6rem;
                }

                .teachers {
                    margin-top: -0.625rem;
                    margin-left: 0.03125rem;
                    padding: 0 0.625rem 0.625rem 0;
                }

                .padding {
                    padding-left: 1.5rem;
                    padding-right: 1.5rem;
                }

                .margin__top__sixteen {
                    margin-top: 1rem;
                }

                .neg-margin {
                    margin-right: 1rem;
                    margin-left: -1rem;
                    margin-top: .5rem;
                }
            }

            @media screen and (min-width: 768px) {
                ._level-md {
                    display: none !important;
                }
            }

            .docent_level_bottom {
                position: relative;
                padding: 0;
            }

            ._level-md span {
                position: relative;
            }

            .docent_level_bottom span,
            ._level-md span {
                padding-right: 20px;
            }

            .fa.alignment.tippy {
                position: absolute;
                right: 0;
                top: 15px;
                padding: 0 !important;
            }

            .docent_level_top .fa.alignment.tippy {
                right: -3px;
                top: 25px;
            }

            @media screen and (max-width: 767px) {
                .docent_level_bottom {
                    display: none;
                }

                ._level_container {
                    padding-bottom: 5rem;
                }

                .text-primary {
                    margin-top: .5rem;
                }

                .tutor__profile .col-md-7.col-sm-6 {
                    padding-top: 0;
                    text-align: center;
                }

                .tutor__profile .center__alignment {
                    width: 100%;
                    text-align: center !important;
                }
            }

            @media screen and (max-width: 767px) {
                .top__banner {
                    margin: 0 -1rem -6rem -1rem !important;
                    padding: 2rem .75rem 8rem .75rem;
                }

                .partition {
                    border-left: none !important;
                }

                .partition__separator__line {
                    display: block;
                }

                .book__text {
                    display: none;
                }

                .center__alignment {
                    text-align: center !important;
                }

                .tooltip.bottom {
                    left: 0 !important;
                }

                .tooltip.left {
                    top: -240px !important;
                    left: -213px !important;
                }

                .neg-margin {
                    margin-right: 0;
                    margin-left: 0;
                }

                .text-primary-bold {
                    text-align: center !important;
                }
            }

            .review__comment {
                padding-top: 2rem;
                padding-bottom: 2rem;
            }

            h1 {
                /* padding-top: 2rem; */
                text-align: center;
            }

            .comment {
                padding: 1.5rem;
                margin: 1rem;
                text-align: center;
                box-shadow: 0 0 12px rgba(0, 0, 0, 0.175);
                border-radius: .25rem;
            }

            .bold {
                font-weight: bold;
            }

            .mapboxgl-ctrl-geocoder input[type='text'] {
                color: #486066;
                font-size: 0.875rem;
                height: 2.625rem;
                border-color: #486066 !important;
                padding: 0.625rem 0.7rem 0.625rem 1.375rem;
                border-radius: 6px 0 0 6px !important;
            }

            .mapboxgl-ctrl-geocoder,
            .mapboxgl-ctrl-geocoder ul {
                box-shadow: 0 0 0 0 rgba(0, 0, 0, 0) !important;
            }

            .tooltip-arrow {
                display: none;
            }

            .tooltip.bottom {
                left: 10% !important;
            }

            .tooltip-inner {
                padding: 0.5rem 0.7rem;
                min-width: 290px;
                text-align: start;
                background: #FFFFFF;
                box-shadow: 0 0 6px 2px rgba(0, 0, 0, .4);
            }

            .tooltip.in {
                opacity: 1;
                z-index: 9999;
            }

            .fa {
                cursor: default;
            }

            div.school-course div.dropdown-menu {
                width: 250px !important;
            }

            div.school-year div.dropdown-menu {
                width: 90px !important;
            }

            .btn-group.bootstrap-select.form-control.show-tick.school-year.update-onchange {
                /* width: 70px; */
            }

            .card.action_bar .btn {
                width: 100% !important;
            }

            .tippy-popper .tippy-tooltip {
                background-color: #FFFFFF !important;
                box-shadow: 0 0 4px 2px rgba(0, 0, 0, 0.4) !important;
                line-height: 1.6 !important;
            }

            .tippy-popper .tippy-tooltip * {
                text-align: left;
            }

            .tippy-popper .tippy-tooltip a {
                color: #5cc75f !important;
            }

            .tippy-popper .tippy-backdrop {
                display: none;
            }

            @media screen and (min-width: 992px) {
                .tippy-popper {
                    margin-right: 10% !important;
                }
            }

            @media screen and (min-width: 576px) and (max-width: 991px) {
                .tippy-popper {
                    margin-right: 6% !important;
                }
            }

            @media screen and (max-width: 575px) {
                .tippy-popper {
                    margin-right: 10% !important;
                }
            }

            /* Fix for Safari .row issue of wrong column alignment */
            .row:before,
            .row:after {
                display: none !important;
            }


            ._level-md {
                font-weight: 600;
                font-size: 19px;
                margin-top: 1rem;
                padding: 0 0;
                text-align: unset;
                display: inline;
                float: right;
                margin-top: 0;
                /* margin-right: 20px; */
            }

            .schoolyeardiv {
                display: block;
            }

            .bootstrap-select .dropdown-toggle:focus {
                outline: none !important;
            }

            <?php if (!isset($_SESSION['StudentID'])) {
                echo "section.content {margin: 60px 0px 15px 0px !important;} div#pbe-footer-wa-wrap{padding-left: 0px !important;}";
            } ?>

            /* NEW CSS */
            @media screen and (min-width: 992px) {
                .tippy-popper {
                    margin-right: 10% !important;
                }
            }

            @media screen and (min-width: 768px) {
                .no__more__tutors__icon {
                    display: flex;
                    align-items: center;
                }
            }

            @media screen and (min-width: 576px) and (max-width: 991px) {
                .tippy-popper {
                    margin-right: 6% !important;
                }
            }

            @media screen and (max-width: 575px) {
                .tippy-popper {
                    margin-right: 10% !important;
                }
            }

            /* END NEW CSS */
            section .container {
                width: 100%;
                position: relative;
                text-align: left;
                /* width: 80%; */
                max-width: 1080px;
                margin: auto;
                margin-left: auto;
                margin-right: auto;
                padding-right: 15px;
                padding-left: 15px;
            }

            .ma-subtitle {
                font-size: 21px !important;
                color: #5cc75f !important;
                font-family: 'Poppins', sans-serif !important;
                font-weight: 600;
                font-style: normal;
            }

            .card-title {
                text-decoration: underline;
                padding-bottom: 0 !important;
                margin-bottom: .75rem !important;
                word-wrap: break-word;
            }

            .card-des {
                color: #7898A0 !important;
            }

            .custom_info_box * {
                font-size: 12px;
            }

            section.content>.container {
                width: 90%;
            }

            @media screen and (min-width:768px) and (max-width:820px) {
                section.content>.container {
                    width: 98%;
                }
            }

            section.content {
                padding-top: 0 !important;
            }

            #newCourse div.dropdown-menu {
                margin-top: 41px !important;
            }

            .docent_level_bottom .docent_level_bottom__title {
                font-weight: 700;
                padding: 0;
                display: inline-block;
                margin-bottom: 20px;
            }

            @media screen and (min-width:768px) and (max-width: 992px) {
                .docent_level_bottom .docent_level_bottom__title {
                    margin-bottom: 0;
                }
            }

            .docent_level_bottom__title {
                font-size: 19px;
            }

            .comment {
                padding: 1.5rem;
                margin: 1rem;
                text-align: center;
                box-shadow: 0 0 12px rgba(0, 0, 0, 0.175);
                border-radius: .25rem;
            }

            .comment {
                display: flex !important;
                align-items: start;
                flex-wrap: wrap;
            }

            .comment>* {
                width: 100%;
                display: block;
            }

            .top__banner>* {
                margin-left: 1rem;
                margin-right: 1rem;
            }
        </style>
        <?php $activePage = basename($_SERVER['PHP_SELF']); ?>
    </head>

    <body class="theme-green">
        <!-- Page Loader -->
        <div class="page-loader-wrapper">
            <div class="loader">
                <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
                <p>Please wait...</p>
            </div>
        </div>
        <!-- Overlay For Sidebars -->
        <div class="overlay"></div>

        <?php require_once('includes/header.php'); ?>

        <style>
            section.content>.container-fluid {
                width: 100%;
                max-width: unset;
                padding-right: 0;
                padding-left: 0;
            }

            div#newCourse div p {
                padding-left: 0;
            }
        </style>
        <!-- Main Content -->
        <section class="content page-calendar" style="overflow:hidden !important; background:#FFFFFF !important; margin: 0 0 0 0 !important; padding-top: 0;padding-right:0;padding-left:0;">
            <!--<div class="block-header">
			<#?php
				if (isset($_SESSION['StudentID']))
					require_once('includes/studentTopBar.php');
				?>
			<style>
				.mapboxgl-ctrl-geocoder li>a {
					color: black !important;
				}
			</style>
		</div> -->
            <div class="container-fluid">
                <div class="row clearfix">
                    <div class="col-lg-12 teacher__header">
                        <div class="top__banner">
                            <h1>Vind de bijlesdocent die het best bij jou past</h1>
                            <h2 class="ma-subtitle book__text">Boek direct je bijlesdocent of vraag om een gratis
                                persoonlijke kennismaking. Hulp nodig? Wij kennen onze docenten goed en helpen je
                                graag!</h2>
                            <img class="google__review" src="./assets/images/Google-Stars-4.8.svg" alt="Google Star Review">
                            <h2 class="ma-subtitle" style="margin-top: 0.3125rem !important"> 4.8/5 beoordeling op
                                Google </h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container padding">
                <div class="row">
                    <div class="col-lg-12" style="padding-left:0; padding-right:0">
                        <div class="card action_bar" style="margin-bottom: 1.5rem !important">
                            <div class="border">
                                <div class="row clearfix">
                                    <div class="col-lg-12 col-md-12 col-12" style="z-index: 5">
                                        <div id="newCourse" style="margin-bottom: 0;">
                                            <div class="row clearfix">
                                                <!-- id="course-row1" -->
                                                <input type="hidden" name="counter" class="counter" value="1" />
                                                <div class="col-sm-5 col-lg-3 school-typo" id="schooltypediv">
                                                    <p class="margin__top__sixteen margin__bottom__0" style="color: white !important;">
                                                        <b>Schooltype</b>
                                                    </p>
                                                    <select class="form-control show-tick school-type dynamic-course" data-size="3" data-dropup-auto="false" id="school-type" name="select-school-type" required>
                                                    </select>
                                                </div>
                                                <div class="col-sm-5 col-lg-2 schoolcoursediv" style="z-index: 4" id="schoolcoursediv">
                                                    <p class="margin__top__sixteen margin__bottom__0" style="color: white !important;"><b>Vak(ken)</b>
                                                        <i class="fa tippy" data-toggle="tooltip" data-placement="bottom" data-tippy-content="Het is mogelijk om meerdere vakken te selecteren. Dan worden, indien beschikbaar, eerst de docenten getoond die bijles geven in al die vakken en zonder reiskosten bij jou aan huis komen." style="color: #BBB !important;">&#xf05a;</i>
                                                        <i style="color: #5CC75F !important; float: right; display: none;" id="icon__school__course" class="fas fa-plus-circle"></i>
                                                    </p>
                                                    <select class="form-control show-tick school-course update-onchange update-onchange-subject" data-dropup-auto="false" multiple="multiple" required name="school-course[]" id="school-course">
                                                    </select>
                                                </div>

                                                <div class="col-6 col-sm-4 col-lg-2 schoolleveldiv" style="z-index: 3" id="schoolleveldiv">
                                                    <p class="margin__top__sixteen margin__bottom__0" style="color: white !important;"><b>Niveau</b></p>
                                                    <select class="form-control show-tick school-level update-onchange" data-dropup-auto="false" required id="school-level" name="school-level[]">
                                                    </select>
                                                </div>
                                                <div id="schoolyeardiv" class="col-6 col-sm-4 col-lg-2 schoolyeardiv" style="z-index:2">
                                                    <p class="margin__top__sixteen margin__bottom__0" style="color: white !important;"><b>Jaar</b></p>
                                                    <select class="form-control show-tick school-year update-onchange" data-dropup-auto="false" required id="school-year" name="school-year[]">
                                                    </select>
                                                </div>
                                                <div class="col-sm-4 col-lg-3 teachers-profile2__postcode" id="postcodediv">
                                                    <p class="margin__top__sixteen margin__bottom__0" style="color: white !important;">
                                                        <b>Locatie</b>
                                                    </p>
                                                    <label for="error" id="errorMessage2" style="padding-bottom: 11px; color:red !important; display: none;">
                                                        Wij hebben geen adres gevonden
                                                    </label>

                                                    <form class="navbar-form" role="search" style="margin: 0; border-top-width: 0; border-bottom-width: 0;">

                                                        <div class="input-group add-on">
                                                            <!-- <div id="geocoder"style="float:left;"></div> -->
                                                            <!--<input class="form-control" placeholder="Utrecht..." name="srch-term" id="srch-term" type="text" style="color: #486066; font-size: 0.875rem; height: 2.625rem; border-color: #486066 !important; padding: 0.625rem 0.7rem 0.625rem 1.375rem;">-->
                                                            <input class="teachers-profile2__postcode-input" type="text" placeholder="Postcode" id="postcode3" maxlength="7" <?php
                                                                                                                                                                                if (!empty($postCode)) {
                                                                                                                                                                                    echo "disabled value='$postCode' title='Ga naar ‘profiel aanpassen’ om de postcode te wijzigen'";
                                                                                                                                                                                }
                                                                                                                                                                                ?> style="<? if (!empty($postCode)) echo 'cursor: not-allowed'; ?>">
                                                            <!--<input type="text" placeholder="Huisnummer" id="huisnummer3" required style="margin-left:30px;border:1px solid black !important; border-radius:6px !important; background-color:#FAFBFC !important; color:#486066 !important; height:4.5vh;width: 35%; display: inline-block; position: absolute;"> -->
                                                            <input type="hidden" class="form-control" name="_lat" id="lat3" value="<?php echo $lat; ?>">
                                                            <input type="hidden" class="form-control" name="_lng" id="lng3" value="<?php echo $long; ?>">
                                                            <input type="hidden" class="form-control" name="_place_name" id="place_name3">
                                                            <!--<div class="input-group-btn" style="width: 2.33625rem;">
                                                            <button class="btn btn-default" id="filter_search"
                                                                    type="button"
                                                                    style="margin-top: 0; margin-left: 0; border-color: #486066 !important; border-left: 0.0625rem; left: 0; padding: 0.5625rem 0.7rem;">
                                                                <i class="glyphicon glyphicon-search"></i></button> 
                                                        </div>-->
                                                        </div>
                                                    </form>

                                                    <div class="col-lg-12" style="display:none;">
                                                        <div id='map' style="height: 5rem; width: 75% !important; margin-bottom: 1rem" data-lat="<?php echo (@$d['latitude']) ? $d['latitude'] : 0; ?>" data-lng="<?php echo (@$d['longitude']) ? $d['longitude'] : 0; ?>" data-place="<?php echo (@$d['place_name']) ? $d['place_name'] : "''"; ?>"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <?php if (isset($_SESSION['StudentID'])) {
                                                $stmt = $con->prepare("SELECT teacherID, (SELECT firstname from contact c where c.userID=cb.teacherID)as name FROM `calendarbooking` cb where cb.studentID=$_SESSION[StudentID] GROUP BY teacherID");
                                                $stmt->execute();
                                                $stmt->bind_result($teacherID, $name);
                                                $stmt->store_result();

                                                $display = "none";
                                                if ($stmt->num_rows > 0) {
                                                    $display = "block";
                                                }
                                            ?>
                                                <div class="row teachers">
                                                    <div class="col-lg-12 col-md-12">
                                                        <b>
                                                            <asd style="display: <?php echo $display ?>;">Jouw docenten:</asd>
                                                            <span class="teacherNames"><?php
                                                                                        $strr = "";
                                                                                        while ($stmt->fetch()) {
                                                                                            $strr .= "<a href='teacher-detailed-view.php?tid=$teacherID' onclick='saveToStorage();'>$name</a>, ";
                                                                                        }
                                                                                        $strr = rtrim($strr, ", ");
                                                                                        echo $strr;
                                                                                        ?></span>
                                                        </b>
                                                    </div>
                                                </div>
                                            <?php } ?>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-2 col-md-6">
                                                <!-- <button type="button" class="btn btn-raised btn-primary m-b-10 bg-green btn-block waves-effect btn-block" id="searchTeacher" name="searchTeacher">Search</button>-->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container" id="rankedResult">
                <!--<div class="alert alert-info" style='border-radius:10px; width:100%; text-align:center'> Please use above filter to search the teacher within your 50 km radius...! </div>-->
            </div>

            <div class="container" id="no-result" style="display: none">
                <div class='row no__more__tutors outline center__alignment'>
                    <div class="col-md-2 no__more__tutors__icon">
                        <i class='fa cross__icon__large'>&#xf00d;</i>
                    </div>
                    <div class="col-md-10">
                        <h4 class='text-primary'> Geen docenten gevonden! </h4>
                        <p class='card-text'> Sorry, wij hebben geen docent die voldoet aan je zoekcriteria.
                            Laat je gegevens gerust achter in het contactformulier,
                            waarna we contact opnemen zodra we een geschikte docent hebben.
                            Natuurlijk is dit geheel vrijblijvend.</p>
                    </div>
                </div>
            </div>

            <div class="row clearfix" id="loading" style="display: none">
                <div class='container'>
                    <div class='row'>
                        <div class="loader" style="margin-left: auto;margin-right: auto;">
                            <div class="m-t-30">
                                <img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo">
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3 col-sm-6 col-sm-offset-3">
                        <input type="hidden" name="set_offset" id="set_offset" value="0" />
                        <button id="loadmore" name="loadmore" class="custom__btn f-p calendar-transition loadmore" style="margin-top: 1rem;"> Meer docenten tonen
                        </button>
                    </div>
                </div>
            </div>

            <div class="container review__comment">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="autoplay__carousel">
                            <div class="comment">
                                <i class="fas fa-quote-right quote__icon__large"></i>
                                <p>Bijles Aan Huis is een goed georganiseerd bijlesinstituut. Door de fijne manier van
                                    lesgeven werd bijles veel efficiënter dan zelf leren.</p>
                                <h3 class="bold" style="padding-top: 4.5rem">Sidney</h3>
                                <p>Vwo 6, natuurkunde</p>
                            </div>
                            <div class="comment">
                                <i class="fas fa-quote-right quote__icon__large"></i>
                                <p>Fijn dat wij ook in het weekend bijles kunnen krijgen. Elke zondag heeft onze zoon
                                    bijles, zodat hij elke week goed voorbereid is voor natuurkunde.</p>
                                <h3 class="bold" style="padding-top: 4.5rem">Yvonne</h3>
                                <p>Vwo 5, natuurkunde</p>
                            </div>
                            <div class="comment">
                                <i class="fas fa-quote-right quote__icon__large"></i>
                                <p>Wij hoorden via via van deze manier van bijles en dat bevalt ons heel goed. Ook is de
                                    helpdesk van Bijles Aan Huis erg behulpzaam waar nodig.</p>
                                <h3 class="bold" style="padding-top: 4.5rem">Kristine</h3>
                                <p>Havo 5, natuurkunde</p>
                            </div>
                            <div class="comment">
                                <i class="fas fa-quote-right quote__icon__large"></i>
                                <p>Met enthousiasme en passie voor het vak, werd mijn zoon tot een hoger resultaat
                                    gebracht. We planden een vast moment in de week in en hadden daardoor nooit meer
                                    omkijkeen naar
                                    scheikunde.</p>
                                <h3 class="bold" style="padding-top: 1.5rem">Suzanne Doornewaard</h3>
                                <p>Vwo 5, scheikunde</p>
                            </div>
                            <div class="comment">
                                <i class="fas fa-quote-right quote__icon__large"></i>
                                <p>Heel leuk en vernieuwend concept. Vooral het inboeken van bijlese is heel
                                    laagdrempelig. De docent was altijd vrolijk en wist wat hij deed. Door het succes hebben
                                    we het aantal bijlessen kunnen minderen.</p>
                                <h3 class="bold" style="padding-top: 1.5rem">Derek</h3>
                                <p>Vmbo 3, scheikunde</p>
                            </div>
                            <div class="comment">
                                <i class="fas fa-quote-right quote__icon__large"></i>
                                <p>De docent doorloopt de stof met veel plezier en overtuiging waardoor onze dochter
                                    scheikunde snel oppakt en vol vertrouwen naar de toets gaat. Daarnaast is het
                                    plannen flexibel en makkkelijk via de site van Bijles Aan Huis.</p>
                                <h3 class="bold">Tea</h3>
                                <p>Vwo 4, scheikunde</p>
                            </div>
                            <div class="comment">
                                <i class="fas fa-quote-right quote__icon__large"></i>
                                <p>Alles werd helder, waardoor mijn dochter het weer ziet zitten. Ze scoort nu tot haar
                                    eigen verbazing structureel een 7 en lijkt al klaar voor het economie examen!</p>
                                <h3 class="bold" style="padding-top: 3rem">Antoinette van den Brink</h3>
                                <p>Vwo 6, economie</p>
                            </div>
                            <div class="comment">
                                <i class="fas fa-quote-right quote__icon__large"></i>
                                <p>De bijles economie en wiskunde hebben gewerkt. Één docent voor beide vakken maakt het
                                    makkelijk. De docent weet meer over economie te vertellen dan het boek.</p>
                                <h3 class="bold" style="padding-top: 3rem">Cas</h3>
                                <p>Vwo 6, economie</p>
                            </div>
                            <div class="comment">
                                <i class="fas fa-quote-right quote__icon__large"></i>
                                <p>Het gaat veel sneller dan zelfstandig leren. Vaak had ik al mijn huiswerk voor
                                    economie
                                    tijdens de bijles al af, waardoor ik meer tijd had voor andere dingen.</p>
                                <h3 class="bold" style="padding-top: 3rem">Josefien</h3>
                                <p>Vwo 6, economie</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <script src="https://unpkg.com/popper.js@1"></script>
        <script src="https://unpkg.com/tippy.js@4"></script>
        <script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
        <script>
            $('.autoplay__carousel').slick({
                slidesToShow: 3,
                slidesToScroll: 1,
                autoplay: true,
                autoplaySpeed: 8000,
                arrows: false,
                responsive: [{
                        breakpoint: 1050,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2,
                            infinite: true
                        }
                    }, {
                        breakpoint: 600,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                        }
                    }, {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                        }
                    }
                    // You can unslick at a given breakpoint now by adding:
                    // settings: "unslick"
                    // instead of a settings object
                ]
            });
            $('.autoplay__carousel').on('setPosition', function() {
                resizeSlickSlider();
            });

            $(window).on('resize', function(e) {
                resizeSlickSlider();
            });

            function resizeSlickSlider() {
                $slickSlider = $('.autoplay__carousel');
                $slickSlider.find('.slick-slide').height('auto');

                var slickTrack = $slickSlider.find('.slick-track');
                var slickTrackHeight = $(slickTrack).height();

                $slickSlider.find('.slick-slide').css('height', slickTrackHeight + 'px');
            }
        </script>
        <script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
        <script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


        <div class="modal modal-fullscreen fade" id="modal-map" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h6 class="modal-title" id="myModalLabel">MAP</h6>
                    </div>
                    <div class="modal-body">
                        <div id='map' style="overflow: visible;"></div>
                        <pre id='coordinates' class='coordinates'></pre>
                    </div>
                </div>
            </div>
        </div>
        <!-- Jquery steps Wizard Code -->
        <?php require_once('includes/footerScriptsAddForms.php'); ?>

        <!-- Latest compiled and minified JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

        <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
        <script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>
        <script type="text/javascript" src="/app/assets/js/teacher-search.js"></script>

        <script type="application/javascript">
            $(document).ready(function() {
                <?php
                //$_SESSION['popup_msg'] = true;

                if (isset($_SESSION['popup_msg'])) {
                ?>
                    showNotification("alert-success", "Je account is geactiveerd", "bottom", "center", "", "");
                <?php
                    unset($_SESSION['popup_msg']);
                }
                ?>
                tippy('.tippy', {
                    placement: 'bottom-start',
                    animation: 'fade',
                    theme: 'light',
                    interactive: true,
                    interactiveBorder: 0
                });
                $('[data-toggle="tooltip"]').tooltip({});
            });
        </script>

    </body>

    </html>
<?php } ?>