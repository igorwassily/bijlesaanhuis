<?php
require_once('includes/connection.php');
$thisPage="Teachers Profile";
session_start();
if(!isset($_SESSION['Student']) && false)
{
	header('Location: index.php');
}
else 
{
		$query = "SELECT * FROM contact c  WHERE userID=".$_SESSION['StudentID'];
		$result = mysqli_query($con, $query);

		$d = mysqli_fetch_assoc($result);
	
		
	?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Docenten | Bijles Aan Huis</title>
<?php
		require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">
<link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">	
<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet" />
<link rel='stylesheet' href='https://use.fontawesome.com/releases/v5.7.0/css/all.css' integrity='sha384-lZN37f5QGtY3VHgisS14W3ExzMWZxybE1SJSEsQp9S+oqd12jhcu+A56Ebc1zFSJ' crossorigin='anonymous'>	

<style type="text/css">
button.btn.dropdown-toggle.bs-placeholder.btn-round.btn-simple {
    border-radius: 6px !important;
	background-color:#FAFBFC !important;
	color:#486066  !important;
	border:2px solid #F1F1F1;
}
button.btn.dropdown-toggle.btn-round.btn-simple {
	    border-radius: 6px !important;
	background-color:#FAFBFC !important;
	color:#486066  !important;
	border:2px solid #F1F1F1;
}	
	div#course-row1 {
    border: 2px solid #F1F1F1;
		border-radius:6px;
}
	._level
	{
		font-weight: 600;
		font-size: 19px;
		margin-top: 30px;
		width: 190px;
	}
.fa, .far, .fas {
    font-family: "Font Awesome 5 Free" !important;
	font-size:12px !important;
	color: white !important;
}	
	.material-icons
	{
	  font-size:12px !important;
	  color: white !important;
	}
	@import url('https://fonts.googleapis.com/css?family=Rokkitt:300,700');

.banner{
  display: flex;
  align-items: center; 
  padding: 1.25em 2em;
  border-radius: 5px;
 
  color: #fff;
  font-family: "Rokkitt";
}
.banner--gradient-bg {
  /*background: linear-gradient(-16deg, #809079e5, #87ca83e5 45%, #a2d0b7 45%);*/
}
.banner__text{
    line-height: 1.4;
    text-align: center;
	font-weight: 600;
    font-family: poppins !important;
}


	.custom_info_box
	{
	background: #5cc75f;
    border-radius: 6px;
    padding: 3px;
    color: white !important;
    font-family: poppins !important;
    display: inline-block;
    margin-bottom: 4px;
    font-size: 12px;
	}	
	
	.custom_info_box > small,strong{
	    color: white !important;
    font-family: poppins !important;	
	}	
    .inline-elem-button{margin-top: 20px;} 
    .form-control, .btn-round.btn-simple{color: #486066 !important;}
    .input-group-addon{color:#486066;}
    .text{color:black;}
    .margin-top{margin-top:10px;}
    .checkbox{float: left;margin-right: 20px;}
    .margintop{margin-top: 20px; color:#50d38a;}
	
    
	/*Placeholder Color */
input{	
border: 1px solid #bdbdbd !important;
	}
	
	select{	
border: 1px solid #bdbdbd !important;

/*color: white !important; */
	}	
	
/*	input:focus{	
background:transparent !Important;
	}
	
	select:focus{	
background:transparent !Important;
	}
	*/
	.wizard .content
	{
		/*overflow-y: hidden !important;*/
	}
	
	.wizard .content label {

    color: white !important;

}
	.wizard>.steps .current a 
	{
		background-color: #029898 !Important;
	}
	.wizard>.steps .done a
	{
		background-color: #828f9380 !Important;
	}
	.wizard>.actions a
	{
		background-color: #029898 !Important;
	}
	.wizard>.actions .disabled a
	{
		background-color: #eee !important;
	}
	
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
	

	.fc-time-grid-event.fc-v-event.fc-event.fc-start.fc-end.fc-draggable.fc-resizable {
    color: black !important;
}
	
	#result{
        position: absolute;
z-index: 999;
top: 90%;
left: 22px;
background-color: white;
    }
    .search-box input[type="text"], #result{
        width: 100%;
        box-sizing: border-box;
    }
    /* Formatting result items */
   #result a:hover{
        
	   color: cyan !Important;
    }
	.modal-content{height: 400px !important;}
	.mapboxgl-ctrl-attrib, .mapboxgl-ctrl-logo{display:none !important;}
	
	.form-control[disabled], .form-control[readonly], fieldset[disabled] .form-control {
    background-color: transparent !important;
	}
	
	.bootstrap-select[disabled] button
	{
		color: gray !important;
		border: 1px solid gray !important;
	}
	
	.bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}
	
	.dropdown-menu > ul > li > a
	{
		color: #555 !important;
	}
	
<!-- select field double print issue -->
.btn-group {
    background: transparent;
}
.form-control{
	background-color: transparent !important;
}
span.bs-caret {
    display: none !important;
}
div#course-row1 > div > p {
    margin: 25px 0 10px;
}
	.card .body {
    background-color: #FAFBFC !important;
}
	
	.mapboxgl-ctrl-top-right {
	top: -2%;
    right: 36.5%;
    /* text-align: left !important; */
}
	
	.mapboxgl-ctrl-geocoder {
    font: 15px/20px 'Helvetica Neue', Arial, Helvetica, sans-serif;
    position: relative;
    background-color: #fafbfc !important;
    width: 33.3333%;
    min-width: 240px;
    max-width: 360px;
    z-index: 1;
    border-radius: 3px;
    border-color: #f1f1f1 !important;
}
	
	.mapboxgl-ctrl-geocoder input[type='text']::placeholder {
    color: #486066 !important;
}
	
	div.school-course>div>ul>li.selected>a>span.glyphicon.glyphicon-ok.check-mark:before {
    content: "\e082";
		color:#ffbd6b;
}
	div.school-course>div>ul>li:not(.selected)>a>span.glyphicon.glyphicon-ok.check-mark:before {
    content: "\e081";
		
		color:rgb(92, 199, 95);
}
	.bootstrap-select.btn-group.school-course .dropdown-menu li a span.check-mark 
     {
        position: absolute;
		display: inline-block !important;
		right: 15px;
		margin-top: 5px;
}
	.d-flex:hover{
		box-shadow:4px 4px 6px #969696 !important;
	}
	.text-primary
	{
		font-size: 21px !important;
		color: #486066 !important;	
		font-family: poppins !important;
	
	}
	.ml-101{margin-left:-101px ;}
	.ml-98{margin-left:98px ;}	

/*@media (min-width: 1200px){.ml-101{margin-left:0px !important;} .ml-98{margin-left:0px !important;}}



	@media (min-width: 768px){.ml-101{margin-left:0px !important;} .ml-98{margin-left:0px !important;}}*/
	#onlineTutoring_line_msg{

		text-align: center;
		width: 100%;
		margin-top: 20px;
		font-weight: bold;
	}
	div.school-course > div.dropdown-menu {
    width: 130% !important;
}
	
	div.school-type > div.dropdown-menu {
    width: 115% !important;
}
	.teacherNames a{color:#5cc75f !important; font-family: poppins !important;font-size: 12px;}
	.teacherNames a:hover{text-decoration: underline;}
	.card-des {font-size:14px;}
	.mapboxgl-ctrl-geocoder{display:none;}
	
	<?php if(!isset($_SESSION['StudentID'])){
			echo "section.content {margin: 60px 0px 15px 0px !important;} div#pbe-footer-wa-wrap{padding-left: 0px !important;}";	
		}
	?>
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
	?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div>
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="background:#FFFFFF !important;">
    <div class="block-header">
       <?php  
			if(isset($_SESSION['StudentID']))
				require_once('includes/studentTopBar.php');  
		?>
		<style> .mapboxgl-ctrl-geocoder li > a {color: black !important;}</style>
    </div>
    <div class="container-fluid ma-sidebar-container">
		<div class="row clearfix">
            <div class="col-lg-12">
                <div class="card action_bar">
					<div class="header" style="background-color:#F1F1F1;">
                    	<h2 class="ma-subtitle">Boek Een Docent</h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-lg-12 col-md-12 col-12">
								
                                <div id="newCourse">
                        <div class="row clearfix" id="course-row1">
							<input type="hidden" name="counter" class="counter" value="1" />
                            <div class="col-lg-2 col-md-6">
                                <p style="color: white !important;"> <b>Schooltype</b> </p>
                                 
                                    <select class="form-control show-tick school-type dynamic-course" id="school-type" style="" name="select-school-type" required>
										<option value=''selected>Selecteer</option>
										
                                    <?php
									
										$stmt = $con->prepare("SELECT DISTINCT typeID, typeName from schooltype group by typeName");
										$stmt->execute();
										$stmt->bind_result($typeID,$typeName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $typeID; ?>"><?php echo $typeName; ?></option>
									<?php
										}
									?>

                                </select>	
								
                            </div>
							<div class="col-lg-2 col-md-6">
								<p styl="color: white !important;"> <b>Vak(ken)</b>
									<i style="background:transparent;color:#486066  !important; font-size: 16px !important;" class="fa"></i>
									<i class="fa fa-plus-circle" id="mini-subject-list" style="display:none;font-size:18px !important;color:#5cc75f !important; margin-left: 38px;"></i></p>
                                <select class="form-control show-tick school-course update-onchange update-onchange-subject" multiple="multiple" required name="school-course[]" id="school-course1">
									<!--<option value="">Select</option>-->
                                </select>
								
                            </div>
							<div class="col-lg-2 col-md-6">
                                <p styl="color: white !important;"> <b>Niveau</b> </p>
                                <select class="form-control show-tick school-level update-onchange"  required id="school-level" name="school-level[]">
									<option value=''selected>Selecteer</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoollevel");
										$stmt->execute();
										$stmt->bind_result($levelID,$levelName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $levelID; ?>"><?php echo $levelName; ?></option>
									<?php
										} 
									?>
                                </select>
                            </div>
							
							<div class="col-lg-2 col-md-6">
                                <p styl="color: white !important;"> <b>Jaar</b> </p>
                                <select class="form-control show-tick school-year update-onchange" required id="school-year" name="school-year[]">
									<option value=''selected>Selecteer</option>
                                    <?php
									
										$stmt = $con->prepare("SELECT * from schoolyear");
										$stmt->execute();
										$stmt->bind_result($yearID,$yearName);
										$stmt->store_result();
										while($stmt->fetch())
										{
									?>
									<option value="<?php echo $yearID; ?>"><?php echo $yearName; ?></option>
									<?php
										} 
									?>
                                </select>
                            </div>
							<div class="col-lg-4 col-md-12">
								<p style="color: white !important;"> <b>Bijleslocatie</b> </p>
								<div id='map' style="height: 105px; overflow:visible;" data-lat="<?php echo (@$d['latitude'])?$d['latitude'] :0; ?>" data-lng="<?php echo (@$d['longitude'])?$d['longitude']:0; ?>" data-place="<?php echo (@$d['place_name'])?$d['place_name']: "''"; ?>"></div>
							</div>

								<?php if(isset($_SESSION['StudentID'])){?>
								<div class="row" style="margin-top: -49px;margin-left: 4px;">
										<div class="col-lg-12 col-md-12">
											<b>Jouw docenten:
												<span class="teacherNames"><?php 
												
										$stmt = $con->prepare("SELECT teacherID, (SELECT firstname from contact c where c.userID=cb.teacherID)as name FROM `calendarbooking` cb where cb.studentID=$_SESSION[StudentID] GROUP BY teacherID");
										$stmt->execute();
										$stmt->bind_result($teacherID, $name);
										$stmt->store_result();		
										$strr = "";
											while($stmt->fetch())
											{  
												$strr .= "<a href='teacher-detailed-view.php?tid=$teacherID'>$name</a>, "; 
											}
										$strr = rtrim($strr, ", ");
										echo $strr;
									?></span></b> 
											</div>
									  </div>	
								<?php } ?>
                        </div>
						<div class="row">
							
														<div class="col-lg-2 col-md-6">
							<!--	<button  type="button" class="btn btn-raised btn-primary m-b-10 bg-green btn-block waves-effect btn-block" id="searchTeacher" name="searchTeacher">Search</button>-->
							</div>
						</div>
									
						</div>
								
                            </div> 
							
                        </div>
                    </div>
                </div>
            </div>           
        </div>
		<div class="row clearfix"  id="rankedResult">
			
			<!--<div class="alert alert-info" style='border-radius:10px; width:100%; text-align:center'> Please use above filter to search the teacher within your 50 km radius...! </div>-->
        </div>
		<input type="hidden" name="set_offset" id="set_offset" value="0" />
<center><button id="loadmore" name="loadmore" class="btn btn-primary calendar-transition loadmore" style="margin-top: 10px; border-radius: 7px !important;background: #5cc75f !important; color:white !important"> Meer docenten tonen </button></center>
    </div> 
</section>
	
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
	
	<div class="modal modal-fullscreen fade" id="modal-map" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
		  <h6 class="modal-title" id="myModalLabel">MAP</h6>
      </div>
      <div class="modal-body">
		 	<div id='map' style="overflow: visible;"></div>
			<pre id='coordinates' class='coordinates'></pre>
      </div>
      
    </div>
  </div>
</div>
	<!-- Jquery steps Wizard Code --> 
<?php
    require_once('includes/footerScriptsAddForms.php');
?>
	
	
<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>
	
	<script>
	
		function searchTeacher(){
		//$("#searchTeacher").click(function(e){
			
		  var school_type = $("#school-type").val() || [];
		  school_type = (typeof(school_type) == "object")? school_type.join(",") : school_type;
		
		  var school_level = $("#school-level").val() || [];
		  school_level =  (typeof(school_level) == "object")? school_level.join(",") : school_level;
	
		  var school_year = $("#school-year").val() || [];
		  school_year =  (typeof(school_year) == "object")? school_year.join(",") : school_year;
			
		  var school_course = $("#school-course1").val() || [];
		  school_course =  (typeof(school_course) == "object")? school_course.join(",") : school_course;
			 
			var laat, lnng, placeName;
			if(isAddressChanged){
				laat = lat;
				lnng = lng;
				placeName = place_name;
			}else{
				laat = $("#map").attr("data-lat");
				lnng = $("#map").attr("data-lng");
				placeName = $("#map").attr("data-place");
			}
			_offset = $("#set_offset").val();
			$.ajax({
				url: 'actions/mapbox.php',
				data: {laat:laat, lnng:lnng, placeName:placeName, stype:school_type, slevel:school_level, syear:school_year, scourse:school_course,offset:_offset,limit:_limit},
				type: "POST",
				//dataType: "json",
				success: function (result) {

					var afterText = $("<p>").html(result).text() ;
	
					if(afterText.trim() == "No Tutor found ...!"){
						/*$("#rankedResult").html("<div class='alert alert-danger' style='border-radius:10px; width:100%; text-align:center'>No Tutor found ...! </div>");
						$("#loadmore").css("display", "none");*/
						if(isLoadMoreClicked)
						{
							$("#loadmore").css("display", "none");								
						}else 
						{
						  	$("#rankedResult").html(result);
							$("#loadmore").css("display", "none");					
						}

						return;
					}else{
						$("#rankedResult").append(result);
						var offset = $("#set_offset").val();
						var newoffset = parseInt(offset)+parseInt(_limit);
						$("#set_offset").val(newoffset);							
						$("#loadmore").css("display", "block");	
					}
				}
			});
			
		//}); //$("#searchTeacher").click() ends
	}
	
	</script>	
	
	<script type="text/javascript">
		
		
		var lat=<?php echo (@$d['latitude'])? $d['latitude'] : 51.933452;?>;
		var lng =<?php echo (@$d['longitude'])? $d['longitude'] : 4.79276;?>;
		var place_name =<?php echo (@$d['place_name'])? "'".$d['place_name']."'" : "''" ;?>;
		/*console.log("place Name"+place_name);*/
		var isAddressChanged = false;
		var mapconditon = true;
		var _offset = 0;
		var _limit = 10;
		var newOffset = 0;
		var loadMore_flag = true;
		var isLoadMoreClicked = false;
		
function firstSearch(laat, lnng, placeName){
			var _offset = $("#set_offset").val();
			$.ajax({
				url: 'actions/mapbox.php',
				data: {laat:laat, lnng:lnng, placeName:placeName, stype:"", slevel:"", syear:"", scourse:"",offset:_offset,limit:_limit},
				type: "POST",
				//dataType: "json",
				success: function (result) {
					$("#rankedResult").append(result);
					var offset = $("#set_offset").val();
					var newoffset = parseInt(offset)+parseInt(_limit);
					$("#set_offset").val(newoffset);
					loadMore_flag = false;
				}
			});
}// firstSearch() ends
		
		
		/* Fetch Pre Information for courses */
	  
		function fetch_preinfo(cat, className, rowID) { 

			$.ajax({
				url: 'actions/fetch_preinfo.php',
				data: {'cat': cat},
				type: "POST",
				//dataType: "json",
				success: function (result) {
					//$(this).parent().parent().siblings().find(".school-course").html("");
					var rowselector = "#";
					var selector = ".";
					//var separator ">";
					var classNam = rowselector.concat(rowID);
					var classNam1 = selector.concat(className);
					$(classNam).children().find(classNam1).not(".btn-group",".bootstrap-select").html("");
					var json_obj = $.parseJSON(result);
					var output = '';
					for (var i in json_obj) 
					{
						$(classNam).children().find(classNam1).not(".btn-group",".bootstrap-select").append('<option value="'+String(json_obj[i].ID)+'">'+String(json_obj[i].Name)+'</option>');
					}

					//$(classNam).children().find(classNam1).not(".btn-group",".bootstrap-select").selectpicker('refresh');
				}
			});

		}
		
		
$(document).ready(function(){
<?php if(isset($_SESSION['Student'])){ ?>
		$(".mapboxgl-ctrl-geocoder input").css({"background":"#e0e0e0 !important","cursor":"not-allowed" });
        $(".mapboxgl-ctrl-geocoder input").attr("disabled",true);

	
<?php } ?>
	
	$("#mini-subject-list").click(function(){$("#school-course1").click();});
	
	var	 schoolType_h = "";
	//$("#"+schoolyear_h).attr('checked', 'checked');
	<?php if(isset($_POST["submission"])){ ?>
		 schoolType_h = <?php echo ($_POST["schoolType_h"])?$_POST["schoolType_h"] : "''";?>;
	var schoolsubject_h = <?php echo ($_POST["schoolSubject_h"])?$_POST["schoolSubject_h"] : "''";?>; // checked inside in ajax response...39%
		var schoolLevel_h = <?php echo ($_POST["schoolLevel_h"])?$_POST["schoolLevel_h"] : "''";?>;
		var schoolyear_h = <?php echo ($_POST["schoolYear_h"])?$_POST["schoolYear_h"] : "''";?>;
		console.log("#school-type option[value='"+schoolType_h+"']");
	     var flag= false;
		if(schoolType_h){
			$("#school-type option[value='"+schoolType_h+"']").attr('selected', 'selected'); 
			 $("#school-type").selectpicker('refresh'); 

		  var stype = $("#school-type").val();

		  stype = (typeof(stype) == "object")? stype.join(",") : stype;
		  var slevel = ""; //$("#school-level").children("option:selected").val()
		 // slevel =  (typeof(slevel) == "object")? slevel.join(",") : "";
	
		  var syear = ""; // $("#school-year").children("option:selected").val()
		 // syear =  (typeof(syear) == "object")? syear.join(",") : "";	  
		   $.ajax({
                            url: 'actions/fetch_courses2.php',
                            data: {'typeID': stype, 'levelID':slevel, 'yearID':syear},
				  			type: "POST",
							//dataType: "json",
                            success: function (result) {
                					$("#school-course1").html(result);
								$("#school-course1").selectpicker('refresh');
								if(schoolsubject_h){
									 $("#school-course1 option[value='"+schoolsubject_h+"']").attr('selected', 'selected');
									$("#school-course1").selectpicker('refresh'); 
									//$("#school-course1").trigger("change");
									searchTeacher();
										var cid = schoolsubject_h; 
										 $.ajax({
											 url: 'actions/course_filter.php',
											 data: {subpage:"courselevel", cid:cid},
											 type: "POST",
											 //dataType: "json",
											 success: function (result) {
												 $("#school-level").html(result);
												 $("#school-level").selectpicker('refresh');
												 $("#school-level option[value='"+schoolLevel_h+"']").attr('selected', 'selected');
												 $("#school-level").selectpicker('refresh'); 
												 
												var clid = schoolLevel_h; 
												var cid = $("#school-course1").val();
												 $.ajax({
													 url: 'actions/course_filter.php',
													 data: {subpage:"courseyear", cid:cid, clid:clid},
													 type: "POST",
													 //dataType: "json",
													 success: function (result) {
														 $("#school-year").html(result);
														 $("#school-year").selectpicker('refresh');
														 if(schoolyear_h){
															 $("#school-year option[value='"+schoolyear_h+"']").attr('selected', 'selected');
															 $("#school-year").selectpicker('refresh');
															 console.log("syh");
															 searchTeacher();
														 } 														 
													 }
													 }); //ajax ends												 
											 }
											 
										 }); //ajax ends
																	
								}
							}
			   
                      }); 
			searchTeacher();
						}

		
		 	
		
		 lat=<?php echo ($_POST['latitude'])? $_POST['latitude'] : 43.6568;?>;
		 lng =<?php echo ($_POST['longitude'])? $_POST['longitude'] : -79.4512;?>;
		 place_name =<?php echo ($_POST['place_name'])? "'".$_POST['place_name']."'" : "''" ;?>;	
	console.log(schoolType_h+" : "+schoolsubject_h+" : "+schoolLevel_h+" : "+schoolyear_h+" : lat : "+lat+" : placeName: "+place_name);
	
	<?php }else { ?>
		firstSearch(lat, lng, place_name);
	<?php } ?>
	
	
	 $("#mapBtn").click(function(){
          $('#modal-map').appendTo("body").modal('show'); 
		 
  map.resize();
	  });
	
	$("#loadmore").on("click", function(){
		isLoadMoreClicked = true;
		searchTeacher();
		
	});
	
	
	 fetch_preinfo("SchoolType","school-type","course-row1");
	  fetch_preinfo("SchoolLevel","school-level","course-row1");
	  fetch_preinfo("SchoolYear","school-year","course-row1");
	fetch_preinfo("SchoolCourse","school-course","course-row1");
	
	$(".update-onchange-subject").change(function(e){
		$("#mini-subject-list").css("display","inline-block");
	});
	/*$("#mini-subject-list").click(function(e){
		console.log("clicked");
	});	*/
	
	
	$(".update-onchange").change(function(e){
		$("#set_offset").val(0);
		$("#rankedResult").html(" ");
		e.stopPropagation();
		console.log("update-onchange");
		searchTeacher();
	});
	
	
	$(".dynamic-course" ).change(function(e) {
		$("#set_offset").val(0);
		$("#rankedResult").html(" ");		
		  e.stopPropagation();
		console.log("dynamic course");
		/*console.log("i am triggered");*/
		  var stype = $("#school-type").val();

		  stype = (typeof(stype) == "object")? stype.join(",") : stype;
		  var slevel = ""; //$("#school-level").children("option:selected").val()
		 // slevel =  (typeof(slevel) == "object")? slevel.join(",") : "";
	
		  var syear = ""; // $("#school-year").children("option:selected").val()
		 // syear =  (typeof(syear) == "object")? syear.join(",") : "";
		
		 if(stype == <?php echo ELEMENTRY_TYPE_ID; ?>){
		  	 /*$("#school-level").parent().parent().css("display","none");*/
			 $("#school-level").attr("required", false);
		     $("#school-level").attr('disabled',true);
		
		  	 /*$("#school-year").parent().parent().css("display","none");*/
				$("#school-year").attr("required", false);
		    $("#school-year").attr('disabled',true);

			  slevel = "";
			  syear = "";
		  }else{
		  	 /*$("#school-level").parent().parent().css("display","block");*/
			  $("#school-level").attr("required", true);					 
			  $("#school-level").attr("disbaled", false);
		  	 /*$("#school-year").parent().parent().css("display","block");*/
	        $("#school-year").attr("required", true);
			  $("#school-year").attr("disbaled", false);
		  }
		  
		   $.ajax({
                            url: 'actions/fetch_courses2.php',
                            data: {'typeID': stype, 'levelID':slevel, 'yearID':syear},
				  			type: "POST",
							//dataType: "json",
                            success: function (result) {
                					$("#school-course1").html(result);
								$("#school-course1").selectpicker('refresh');
								 searchTeacher();
							}
                      }); 
		 
	  }); //show-tick ends
	
	$("#school-course1").change(function(){
		return 0;
		var cid = $(this).val(); 
		/*cid =  (typeof(cid) == "object")? cid.join(",") : "";*/
		 $.ajax({
			 url: 'actions/course_filter.php',
			 data: {subpage:"courselevel", cid:cid},
			 type: "POST",
			 //dataType: "json",
			 success: function (result) {
				 $("#school-level").html(result);
				 $("#school-level").selectpicker('refresh');
			 }
		 }); //ajax ends
	});// change ends
		
		
	$("#school-level").change(function(){
		return;
		var clid = $(this).val(); 
		var cid = $("#school-course1").val();
		/*cid =  (typeof(cid) == "object")? cid.join(",") : "";		*/
		 $.ajax({
			 url: 'actions/course_filter.php',
			 data: {subpage:"courseyear", cid:cid, clid:clid},
			 type: "POST",
			 //dataType: "json",
			 success: function (result) {
				 $("#school-year").html(result);
				 $("#school-year").selectpicker('refresh');
			 }
		 }); //ajax ends
	});// change ends
	
});
</script>
	<script>
mapboxgl.accessToken = "<?php echo ACCESS_TOKEN; ?>";
//	mapboxgl.accessToken = 'sk.eyJ1Ijoic21jb2RlIiwiYSI6ImNqdW10c3BiZjFlY2Q0NG5xMmU2bGZ4dXkifQ.g02iyxlcdvXAMV3a3lmcYg';
var map = new mapboxgl.Map({
container: 'map',
style: 'mapbox://styles/mapbox/streets-v11',
//	style: 'https://api.mapbox.com/geocoding/v5/mapbox.places',
center: [lng, lat],
zoom: 13
});
		
var geocoder = new MapboxGeocoder({
accessToken: mapboxgl.accessToken, limit:10, mode:'mapbox.places', types:'address' ,countries: 'nl', placeholder: "Zoeken"
	//, countries: 'nl'
});

// search bar
map.addControl(geocoder);

		
/*map.setLayoutProperty('country-label', 'text-field', ['get', 'name_fr']);*/
 
// After the map style has loaded on the page, add a source layer and default
// styling for a single point.
map.on('load', function() {
	$(".mapboxgl-ctrl-geocoder input").val(place_name);
	//$(".mapboxgl-canary").css("visibility","visible");
    //$(".mapboxgl-canary").addClass('properties'); 
// Listen for the `result` event from the MapboxGeocoder that is triggered when a user
// makes a selection and add a symbol that matches the result.
geocoder.on('result', function(ev) {
	console.log("Here: ",ev);
	lat = ev.result.center[1];
	lng = ev.result.center[0];
	place_name = ev.result.place_name;
	isAddressChanged = true;
	$("#rankedResult").html(" ");
	$("#set_offset").val(0);
	searchTeacher();
map.getSource('point').setData(ev.result.geometry);
});
});









var canvas = map.getCanvasContainer();
 
var geojson = {
"type": "FeatureCollection",
"features": [{
"type": "Feature",
"geometry": {
"type": "Point",
"coordinates": [lng, lat]
}
}]
};
 
function onMove(e) {
var coords = e.lngLat;
 
// Set a UI indicator for dragging.
canvas.style.cursor = 'grabbing';
 
// Update the Point feature in `geojson` coordinates
// and call setData to the source layer `point` on it.
geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
map.getSource('point').setData(geojson);
}
 
function onUp(e) {
var coords = e.lngLat;
 console.log(e.lngLat);
	lat = e.lngLat.lat;
	lng = e.lngLat.lng;	
	isAddressChanged = true;
// Print the coordinates of where the point had
// finished being dragged to on the map.
coordinates.style.display = 'block';
coordinates.innerHTML = 'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat;
canvas.style.cursor = '';
			
	geocoder['eventManager']['limit'] = 1;	
	geocoder['options']['limit'] = 1;	
   geocoder.query(lng+","+lat);
  
	
// Unbind mouse/touch events
map.off('mousemove', onMove);
map.off('touchmove', onMove);
	geocoder['eventManager']['limit'] = 10;	
	geocoder['options']['limit'] = 10;	
}
 
map.on('load', function() {

// Add a single point to the map
map.addSource('point', {
"type": "geojson",
"data": geojson
});
 
map.addLayer({
"id": "point",
"type": "circle",
"source": "point",
"paint": {
"circle-radius": 10,
"circle-color": "#3887be"
}
});
 
// When the cursor enters a feature in the point layer, prepare for dragging.
map.on('mouseenter', 'point', function() {
map.setPaintProperty('point', 'circle-color', '#3bb2d0');
canvas.style.cursor = 'move';
});
 
map.on('mouseleave', 'point', function() {
map.setPaintProperty('point', 'circle-color', '#3887be');
canvas.style.cursor = '';
});
 
map.on('mousedown', 'point', function(e) {
// Prevent the default map drag behavior.
e.preventDefault();
 
canvas.style.cursor = 'grab';
 
map.on('mousemove', onMove);
map.once('mouseup', onUp);
});
 
map.on('touchstart', 'point', function(e) {
if (e.points.length !== 1) return;
 
// Prevent the default map drag behavior.
e.preventDefault();
 
map.on('touchmove', onMove);
map.once('touchend', onUp);
});

});

</script>
 
</body>
</html>
<?php
}
?>