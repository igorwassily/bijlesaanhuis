$(function () {
	$('.js-basic-example').DataTable({
		language: {
			url:'/app/assets/js/pages/tables/Dutch.json'
		}
	});

    //Exportable table
    $('.js-exportable').DataTable({
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
    });
});