let OFFSET = 0;
let LIMIT = 10;
let suspend = false;
let lastCategory = 0;
let resultList;
let filter = null;
let LAT = $('#lat3').val() || 0;
let LONG = $('#lng3').val() || 0;
let PSTCODE = $('#postcode3').val() || '';
let schoolTypeChanged = false;
let searchCount = 0;

$(document).ready(function () {
	init();

	// sticky separator
	let windowSelect = $(window);

	windowSelect.scroll(function() {
		let sticky = $(".sticky").closest(".teachers-profile__separator").eq(0);
		let resetPos = $(".container.tutor__profile").eq(0).offset().top;

		if(sticky) {
			let stickyPos = sticky.offset().top;
			let windowPos = windowSelect.scrollTop();

			if(windowPos >= stickyPos && !sticky.hasClass("ignore")) {
				sticky.addClass('fixed');
				sticky.find(".sticky-close").on("click", function() {
					sticky.removeClass('fixed');
					sticky.addClass('ignore');
				});
			}
			if(windowPos <= resetPos) {
				sticky.removeClass('fixed');
			}
		}
	});
});



function init() {
	resultList = $('#rankedResult');

	$("#mini-subject-list").click(function () {
		$("#school-course").click();
	});

	$(".update-onchange-subject").change(function (e) {
		$("#mini-subject-list").css("display", "inline-block");
		$(document).click();

		// ACTIVATE PLUS BUTTON HERE
		$(".fa-plus-circle").css("display", "inline");

	});

	var pressed = false;

	$('#icon__school__course').click(function (e) {
		if (pressed === false) {
			openNextStep('school-course');
			pressed = true;
		} else {
			pressed = false;
			closePreviousStep('school-course');
			resetInputs();
		}
	});

	$('#loadmore').on('click', nextPage);

	$("#postcode3").on("input paste", function () {
		PSTCODE = $('#postcode3').val();
		OFFSET = 0;
		filter.select = true;

		postalCode();
		//searchTeacher(filter);
	});

	/* SCHOOL TYPE */
	$('select[id=school-type]').change(function () {
		schoolTypeChanged = true;
		loadAllSelects();

		if ($(this).val() === "9") {
			showElementarySchool();
		} else {
			showHighSchool();
		}


	});

	$('select[id=school-course]').on('change', function () {
		loadAllSelects()
	});

	$('select[id=school-level]').on('change', function () {
		loadAllSelects()
	});

	$('select[id=school-year]').on('change', function () {
		loadAllSelects()
	});

	filter = {};

	if (location.search.length > 0) {
		filter = QueryParamsToJSON();

		if (filter.school && filter.school === "9") {
			// BASISSCHOOL
			showElementarySchool();
		}
		if (filter.lat) {
			LAT = filter.lat;
		}
		if (filter.long) {
			LONG = filter.long;
		}
		if (filter.postCode) {
			filter.postCode = filter.postCode.replace('+', '');
			filter.postCode = filter.postCode.replace(' ', '');
			PSTCODE = filter.postCode;
			$('#postcode3').val(PSTCODE);
		}
	} else {
		filter = {
			school: 8,
			course: [31],
			level: 4,
			year: 12,
			select: false
		};
	}

	loadAllSelects(filter);
}

function showElementarySchool() {
	$('#schoolcoursediv, #postcodediv, #schooltypediv').addClass('col-lg-4');
	$('#schoolcoursediv, #postcodediv, #schooltypediv').removeClass('col-lg-2');
	$('#postcodediv, #schooltypediv').addClass('col-lg-4');
	$('#postcodediv, #schooltypediv').removeClass('col-lg-3');
	$('#postcodediv').addClass('col-sm-12');
	$('#postcodediv').removeClass('col-sm-4');
	$('#schoolleveldiv').hide();
	$('#schoolyeardiv').hide();
}

function showHighSchool() {
	$('#schoolcoursediv').addClass('col-lg-2');
	$('#schoolcoursediv').removeClass('col-lg-4');
	$('#postcodediv, #schooltypediv').addClass('col-lg-3');
	$('#postcodediv, #schooltypediv').removeClass('col-lg-4');
	$('#postcodediv').addClass('col-sm-4');
	$('#postcodediv').removeClass('col-sm-12');
	$('#schoolleveldiv').show();
	$('#schoolyeardiv').show();
}

function postalCode() {
	let postCodeInput = $("#postcode3");
	let postalCode = postCodeInput.val();
	postalCode = postalCode.replace(' ', '');
	if (postalCode.match(new RegExp('[0-9A-Za-z]{6}')) || postalCode.length === 0) {
		PSTCODE = postalCode;
		lastCategory = postalCode.length === 0 ? 2 : 0;
		loading(true);
		searchTeacher();
	}

	// if ($('#postcode3').val().length === 0) {
	// 	$('#postcode3').css('text-transform', 'capitalize');
	// } else {
	// 	$('#postcode3').css('text-transform', 'uppercase');
	// }
}

function QueryParamsToJSON() {
	let list = location.search.slice(1).split('&'),
		result = {};

	list.forEach(function (keyval) {
		keyval = keyval.split('=');
		var key = keyval[0];
		if (/\[[0-9]*\]/.test(key) === true) {
			var pkey = key.split(/\[[0-9]*\]/)[0];
			if (typeof result[pkey] === 'undefined') {
				result[pkey] = [];
			}
			result[pkey].push(decodeURIComponent(keyval[1] || ''));
		} else {
			result[key] = decodeURIComponent(keyval[1] || '');
		}
	});

	return JSON.parse(JSON.stringify(result));
}

function getSelectedValues() {
	let data = { school: null, course: [], level: null, year: null, postCode: null };

	let schoolType = $('select[id=school-type]').find(':selected');

	if (schoolType.length > 0 && schoolType.val() !== "") {
		data.school = schoolType.val();
	}

	$('select[id=school-course]').find(':selected').each(function (k, e) {
		data.course.push(e.value);
	});

	if ($('#schoolleveldiv').is(':not(:hidden)')) {
		let schoolLevel = $('select[id=school-level]').find(':selected');
		if (schoolLevel.length > 0 && schoolLevel.val() !== "") {
			data.level = schoolLevel.val();
		}
	}

	if ($('#schoolyeardiv').is(':not(:hidden)')) {
		let schoolYear = $('select[id=school-year]').find(':selected');
		if (schoolYear.length > 0 && schoolYear.val() !== "") {
			data.year = schoolYear.val();
		}
	}

	return data;
}

function fillSelect(select, data) {
	select.html("<option value=\"\" hidden>Selecteer</option>");

	data.forEach(function (value) {
		if (value.selected) {
			select.append($(`<option value="${value.id}" selected>${value.text}</option>`));
		} else {
			select.append($(`<option value="${value.id}">${value.text}</option>`));
		}
	});

	select.selectpicker('refresh');
}

function loadAllSelects(data) {
	if (suspend) {
		return 0;
	}

	OFFSET = 0;
	lastCategory = PSTCODE.length === 0 ? 2 : 0;

	if (!data) {
		data = getSelectedValues();
	}

	if (schoolTypeChanged) {
		data.course = [];
		schoolTypeChanged = false;
	}

	filter = data;
	loading(true);

	$.ajax({
		url: '/api/Teacher/Criteria',
		data: data,
		type: "POST",
		//dataType: "json",
		success: function (result) {
			if (result.school) {
				let select = $('select[id=school-type]');
				fillSelect(select, result.school);
			}

			if (result.course) {
				let select = $('select[id=school-course]');
				fillSelect(select, result.course);
			}

			if (result.level) {
				let select = $('select[id=school-level]');
				fillSelect(select, result.level);
			}

			if (result.year) {
				let select = $('select[id=school-year]');
				fillSelect(select, result.year);
			}

			searchTeacher(data);
		},
		error: function (err) {
			console.log(err);
			loading(false);
		}
	});
}

function openNextStep(id) {
	// To Open
	$('.' + id + "> .dropdown-menu").css('display', 'block');
}

function closePreviousStep(id) {
	//console.log("close");
	// To close
	$('.' + id + "> .dropdown-menu").css('display', 'none');
}

$("form").keypress(function (e) {
	//Enter key
	if (e.which == 13) {
		//console.log("ENTER PRESSED");
		return false;
	}
});

function createSeparator(type) {
	/* type = 1 -> Travel, type = 2 -> Online */
	let title = ``;
	let body = ``;
	let icon = ``;

	if (type === 0) {
		//title = `<h1 class='text-primary-bold' style='padding-top: 2.5rem'>Without travel costs. Please translate this</h1>`;
		//icon = `<i class='fas home__icon home__icon__large fa-home'></i>`;

	} else if (type === 1) {
		title = `<h1 class='text-primary-bold' style='padding-top: 2.5rem'>Onderstaande docenten wonen iets verder weg, waardoor reiskosten van toepassing zijn.</h1>`;
		icon = `<i class='fas travel__icon__large'>&#xf206;</i>`;

	} else if (type === 2) {
		title = `<h1 class='text-primary-bold sticky'>Onderstaande docenten zijn voor jou alleen beschikbaar voor online bijles!</h1>`;
		icon = `<i class='fas online__icon__large'>&#xf109;</i>`;
		body = `<p> Wil je een berichtje zodra een nieuwe docent beschikbaar is die bij jou aan huis komt? Laat dan <a href='/bijlesverzoek' target='_blank' style='color:#5CC75F !important'>hier</a> je verzoek achter.</p>`;
	}


	return `
        <div class='container margin-top' style="margin-bottom: 20px;">
            <div class='row outline center__alignment teachers-profile__separator'>
                <div class='col-md-2 neg-margin'>
                    ${icon}
                </div>
                <div class='col-md-10'>
                    ${title}
                    ${body}
                    <div class="sticky-close">X</div>
                </div>
            </div>
        </div>`;
}

function createCard(forename, description, title, image, level, rate, teacher, showTravelIcon, travelCost, showOnlineIcon, showHouseIcon, course, courseTagged, showSubjects) {
	let courseOutput = '';
	let counter = 0;


	course.forEach(function (value, key) {
		if (counter < 2) {
			if (courseOutput.length > 0) {
				courseOutput += ', ';
			}

			courseOutput += value;
		} else if (counter === 2) {
			courseOutput += ' en meer..';
		} else {
			return 0;
		}

		counter++;
	});

	let courseTaggedOutput = '';
	courseTagged.forEach(function (value, key) {
		// if (courseTaggedOutput.length > 0) {
		// 	courseTaggedOutput += ', ';
		// }

		courseTaggedOutput += value + ', ';
	});

	return `
<div class="container tutor__profile">
        <div class="row">
            <div class="col-md-3 col-lg-2">
                 <a class="link-disabled" href="./teacher-detailed-view.php?tid=${teacher}"><img class="img-responsive center-block round__off"
                src="${image}"
                class="img-thumbnail border-0"/></a>
            </div>
            <div class="col-md-6 col-lg-7">
                <div class="text-left">
                    <div class="pt-4 pb-2">
						 <h4 class="text-primary">
						 	<a class="link-disabled" href="./teacher-detailed-view.php?tid=${teacher}">${forename}</a>
							<div class="docent_level_top _level-md">
								<span>${level} docent</span>
								<sup><i class="fa alignment tippy" data-toggle="tooltip" data-placement="bottom"
								style='font-family: "Font Awesome 5 Free" !important;'
                        		data-tippy-content="${getLevelDescription(level,rate,forename)}">&#xf05a;</i></sup>
							</div>
						</h4>
                        <div class="card-text">
                            <span class="card-text">
                                <a class="link-disabled" href="./teacher-detailed-view.php?tid=${teacher}"><p class="card-title link-disabled">${title}</p></a></span>
                                <span class="card-text card-des">${description}</span>
                                <hr id='split-line' class="separator__line"/>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-5 col-sm-6 center__alignment">
                                          <span class="online__tutoring" style="display: ${showOnlineIcon}">
                                             <a href="/online-bijles-uitleg" target="_blank">
                                                <i class="fas online__icon tippy" data-toggle="tooltip"
                                                data-placement="bottom"
                                                data-tippy-content='Deze docent is beschikbaar voor online bijles. Meer weten, klik <a href="/online-bijles-uitleg" target="_blank">hier</a>!'>&#xf109;</i>
                                            </a>
                               
                                        </span>
                                         <span class="travelling__tutor" style="display: ${showTravelIcon}">
                                            <a href="/reiskosten" target="_blank">
                                                <i class="fas travel__icon tippy" data-toggle="tooltip"
                                                       data-placement="bottom"
                                                       data-tippy-content='Gezien je bijleslocatie, betaal je voor deze docent € ${travelCost} reiskosten. Meer weten? Kik <a href="/reiskosten" style="color:#FFBD6B !important;" target="_blank">hier</a>!'>&#xf206;</i>
                                            </a>
                                         </span>
                                         <span class="travelling__free" style="display: ${showHouseIcon}">
                                            <i class="fas fa-home home__icon tippy"
                                                data-toggle="tooltip" data-placement="bottom"
                                                data-tippy-content="Deze bijlesdocent komt bij jou aan huis."></i>
                                         </span>      
                                    </div>
                                    <div class="col-md-7 col-sm-6">
                                      <span class="custom_info_box" style="display: ${showSubjects}">
                                           <b>${courseTaggedOutput}</b> ${courseOutput}
                                      </span>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
          <div class="col-md-3 partition">
            <div class="pt-4 pb-2 _level_container">
                <hr id="split-line" class="partition__separator__line"/>
                <div class="_level docent_level_bottom ${level}">
                    <span class="docent_level_bottom__title">${level} docent</span>
						<i class="fa alignment tippy docent_level_bottom__icon" data-toggle="tooltip"
                        data-placement="bottom" style='font-family: "Font Awesome 5 Free" !important;'
                        data-tippy-content="${getLevelDescription(level,rate,forename)}">&#xf05a;</i>
                    <span  class="docent_level_bottom__cost" style="display: ${showTravelIcon}">Reiskosten: € ${travelCost}</span>
                    </div>` +

		/*<div class="_level tutor__profile__course-price">
			<small>${rate} € per
			lesuur*</small><br/>
		</div>*/
		`<div class="align__bottom pb-2 teachers-profile2-buttons">
				<a href="./teacher-detailed-view.php?tid=${teacher}" class="teachers-profile2__button--green link-disabled">
					BEKIJK PROFIEL
				</a>
				<a href="./teacher-detailed-view.php?tid=${teacher}#teacher-detail-view-hash" class="teachers-profile2__button--green link-disabled"
                    name="reserve-slot">
                    BEKIJK BESCHIKBAARHEID
                </a>
                <div class="teachers-profile2-buttons__overlay">
                	<div class="teachers-profile2-buttons__overlay-text">
                		Vul eerst de zoekbalk in
					</div>
				</div>
            </div>
        </div>
    </div>
</div>
</div>
`;
}

function getLevelDescription(level,rate,name) {
	let levelDescription = "";
	switch (level) {
		case 'Junior':
			levelDescription = `Voor al onze docenten garanderen wij onze hoge kwaliteit. Een Junior docent heeft de stof recent zeer succesvol gevolgd, waardoor het erg vers in het geheugen zit. ${name} is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. Het tarief van een Junior docent is <b>€ ${rate}</b>,- per lesuur van 45 minuten.`;
			break;

		case 'Senior':
			levelDescription = `Een Senior docent heeft, vaak door middel van een gerelateerde studie, een uitmuntende kennis in zijn/haar vakgebied. ${name} is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. Het tarief van een Senior docent is <b>€ ${rate}</b>,- per lesuur van 45 minuten.`;
			break;

		case 'Supreme':
			levelDescription = `Een Supreme docent is een professional is in zijn/haar vakgebied. ${name} is zorgvuldig geselecteerd op basis van kennis van het vak en didactische vaardigheden. Het tarief van een Supreme docent is <b>€ ${rate}</b>,- per lesuur van 45 minuten.`;
			break;
	}
	return levelDescription;
}

function searchTeacher(data, scroll = false) {
	if (!data) {
		data = getSelectedValues();
	}

	searchCount++;

	data.limit = LIMIT;
	data.offset = OFFSET;
	data.postCode = PSTCODE;
	data.lat = LAT;
	data.long = LONG;

	$.ajax({
		url: '/api/Teacher/Search',
		data: data,
		type: "POST",
		dataType: "json",
		success: function (result) {
			if (result.data.length === 0) {
				$('#no-result').show();
			}

			let showSubjects = '';

			result.data.forEach(function (value) {
				/* CAT 0 => AT HOME, CAT 1 => TRAVEL COSTS , CAT 2 => ONLINE*/
				if (lastCategory !== value.category && value.category !== 0) {
					resultList.append($(createSeparator(value.category)));
					lastCategory = value.category;

					tippy('.tippy', {
						placement: 'bottom-start',
						animation: 'fade',
						theme: 'light',
						interactive: true,
						interactiveBorder: 0
					});
					// $('[data-toggle="tooltip"]').tooltip({});
				}

				let travelCosts = 0;
				let showOnlineIcon = 'none';
				let showBikeIcon = 'none';
				let showHouseIcon = 'none';

				if (value.travelCost) {
					travelCosts = value.travelCost.toFixed(2)
				}

				if (value.online) {
					showOnlineIcon = 'inline-block';
				}

				if (value.house) {
					showHouseIcon = 'inline-block';
				}

				if (value.travel) {
					showBikeIcon = 'inline-block';
				}

				resultList.append($(createCard(value.forename, value.description, value.title, value.image, value.docentType, value.rate, value.teacher, showBikeIcon, travelCosts, showOnlineIcon, showHouseIcon, value.courses, value.tagged, showSubjects)));
				tippy('.tippy', {
					placement: 'bottom-start',
					animation: 'fade',
					theme: 'light',
					interactive: true,
					interactiveBorder: 0
				});

				$('.link-disabled').on('click', function(e){
					// if(searchCount == 1) {
					// 	e.preventDefault();
					// }
					if($("#postcode3").val().length < 6) {
						e.preventDefault();
					}
				});
			});
			$('[data-toggle="tooltip"]').tooltip({});

			if (result.moreResults) {
				$('#loadmore').show();
			} else {
				$('#loadmore').hide();
			}

			if (result.search) {
				history.pushState(null, "Docenten | Bijles Aan Huis", "teachers-profile2" + result.search);
			}

			loading(false);
			if($("#postcode3").val().length > 5) {
				$(".teachers-profile2-buttons__overlay").hide();
			}
			// if(searchCount > 1) {
			// 	if($("#postcode3").val().length > 5) {
			// 		$(".teachers-profile2-buttons__overlay").hide();
			// 	}
			// }
		},
		error: function (err) {
			console.log("Something went wrong.", err);
			loading(false);
		}
	});
}

function nextPage() {
	OFFSET += LIMIT;
	searchTeacher(filter);
}

function resetInputs() {
	$('.school-level > .dropdown-menu').css('display', '');
	$('.school-year > .dropdown-menu').css('display', '');
	$('.school-type > .dropdown-menu').css('display', '');
	$('.school-course > .dropdown-menu').css('display', '');
}

function loading(loading) {
	if (loading) {
		suspend = true;
		$('#loadmore').hide();
		$('#no-result').hide();
		$('#loading').show();
		resultList.hide();
		resultList.html(" ");
	} else {
		$('#loading').hide();
		resultList.show();
		suspend = false;
	}
}
