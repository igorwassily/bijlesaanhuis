$(document).ready(function(){

    flatpickr(".datepicker-here", {
        dateFormat: "d/m/Y",
        allowInput: true, 
        "locale": "nl",
        defaultDate: "01/01/2000",
        disableMobile: "true"   
 

});

    $(".datepicker-here").val('');
});            