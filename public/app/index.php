<?php
session_start();
if(isset($_SESSION['TeacherID'])){
	header('Location: teachers-dashboard.php');
	exit;
}
	
if(isset($_SESSION['StudentID'])){
	header('Location: students-dashboard.php');
	exit;
}
?>
<!doctype html>
<html class="no-js " lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=Edge">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
    <meta name="description" content="Door in te loggen, ontdek je onze vele mogelijkheden | Bijles Aan Huis; kwaliteit, innovatie en betrouwbaarheid.">
    <title>Inloggen | Bijles Aan Huis</title>
    <?php
    require_once('includes/connection.php');
    require_once('includes/mainCSSFiles.php');
    ?>
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-select/css/bootstrap-select.css">
    <link rel="stylesheet" type="text/css" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
    <link rel="stylesheet" type="text/css" href="assets/css/bootstrap-multiselect.css" type="text/css">
    <link rel="stylesheet" type="text/css" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="https://www.jqueryscript.net/css/jquerysctipttop.css">
</head>

<body class="authentication login-body">
	<?php	
       require_once('includes/header.php');
    ?>
    <style>
        .navbar {
            margin-bottom: 0;
            border-bottom: none;
        }
    </style>
    <div class="page-header">
        <div class="container content-container">
            <div class="col-md-12 login-container login-container--sm-padding">
                <div class="card-plain">
                    <?php
                        require_once("includes/connection.php");
                        if(isset($_GET['msg']))
                        {
                    ?>
                    <div class="bs-example">
                        <div class="alert alert-danger">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?php echo test_input($_GET['msg']); ?>
                        </div>
                    </div>
                    <?php
                        }
                        if(isset($_GET['successMsg']))
                        {
                    ?>
                    <div class="bs-example">
                        <div class="alert alert-success">
                            <a href="#" class="close" data-dismiss="alert">&times;</a>
                            <?php echo test_input($_GET['successMsg']); ?>
                        </div>
                    </div>
                    <?php
                        }
                    ?>
                    <form id="login_form" class="form login-form" method="post" action="actions/login">
                        <div class="header">
                            <h5 class="ma-heading login-form-header">Inloggen</h5>
                        </div>
                        <div class="login-form-content">
                            <div class="login-form-content__group">
                                <input type="text" class="login-form-input" placeholder="E-mailadres" name="username" required maxlength="60" minlength="5" />
                                <span class="input-group-addon inloggen-input-icon">
                                    <i class="zmdi zmdi-account-circle"></i>
                                </span>
                            </div>
                            <div class="login-form-content__group">
                                <input type="password" placeholder="Wachtwoord" class="login-form-input" name="password" required maxlength="20" minlength="8" title="Wachtwoord moet minimaal 8 tekens bevatten" />
                                <span class="input-group-addon inloggen-input-icon">
                                    <i class="zmdi zmdi-lock"></i>
                                </span>
                            </div>
                            <div class="login-form-content__group">
                                <div id="recaptcha" class="g-recaptcha" data-sitekey="6LdLlcYUAAAAAAwwGkpo4z_TF1Zt41s52VaagFpo"></div>
                            </div>
                        </div>
                        <div class="login-form__button-container">
                            <button type="submit" name="login" id="login" class="btn login-form-button">INLOGGEN</button>
                        </div>
                        <div class="login-form-footer">
                            <p class="login-form-footer__text">
                                <a class="login-form-footer__link" href="forgotPassword">Wachtwoord vergeten?</a>
                            </p>
                            <p class="login-form-footer__text">
                                <a class="login-form-footer__link PopupBtnRegister">Registreer als leerling </a>/ <a href="docenten-registratie" class="login-form-footer__link">aanmeldprocedure docent</a>
                            </p>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <?php
            require_once('includes/footerScriptsAddForms.php');
        ?>
    </div>

<script src="assets/bundles/libscripts.bundle.js"></script>
<script src="assets/bundles/vendorscripts.bundle.js"></script>
<script>
    $(".navbar-toggler").on('click',function() {
        $("html").toggleClass("nav-open");
    });
    $('.form-control').on("focus", function() {
        $(this).parent('.input-group').addClass("input-group-focus");
    }).on("blur", function() {
        $(this).parent(".input-group").removeClass("input-group-focus");
    });
</script>
<script src="https://www.google.com/recaptcha/api.js?explicit&hl=nl"  async defer></script>
<script>
    $( document ).ready(function() {
        // Make recaptcha required
        window.onload = function() {
            var $recaptcha = document.querySelector('#g-recaptcha-response-1');
            if($recaptcha) {
                $recaptcha.setAttribute("required", "required");
            }
        };
    });

    function isIE() {
        ua = navigator.userAgent;
        /* MSIE used to detect old browsers and Trident used to newer ones*/
        var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;
        return is_ie;
    }

    function isEdge(){
        ua = navigator.userAgent;
        var is_ed = ua.indexOf("Edge") > -1;
        return is_ed;
    }

    /* for IE and Edge */
    if (isIE()){
        alert('Applicatie is niet compatibel met deze browser.');
        window.location.replace("https://bijlesaanhuis.nl");
    } else if (isEdge()){
        alert('Applicatie is niet compatibel met deze browser.');
        window.location.replace("https://bijlesaanhuis.nl");
    }
</script>

</body>
</html>
