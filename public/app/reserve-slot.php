<?php

  require_once('includes/connection.php');
$thisPage="Teachers Profile";
session_start();
if(!isset($_SESSION['Student']) || !isset($_POST['teacherid']))
{
  header('Location: index.php');
}
else
{
    $query = "SELECT c.*, t.customhourly FROM `contact` c, teacher t WHERE t.userID=c.userID AND c.userID=".@$_POST['teacherid'];

    $result = mysqli_query($con, $query);
    $data = mysqli_fetch_assoc($result);

  ?>

<!doctype html>
<html class="no-js " lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<meta name="description" content="Responsive Bootstrap 4 and web Application ui kit.">
<title>Teacher Schedule</title>
<?php
    require_once('includes/connection.php');
        require_once('includes/mainCSSFiles.php');
?>
<link href="assets/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />
<link href="assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />
<link rel="stylesheet" href="assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css">
<link href='assets/plugins/fullcalendar/fullcalendar.min.css' rel='stylesheet' />
<link href='assets/plugins/fullcalendar/fullcalendar.print.min.css' rel='stylesheet' media='print' />
<link rel="stylesheet" href="assets/css/bootstrap-multiselect.css" type="text/css">

<!-- New Calendar Style -->
<link href="https://www.jqueryscript.net/css/jquerysctipttop.css" rel="stylesheet" type="text/css">

    <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="calendar/css/calendar.css">

    <style type="text/css">
    #calendar { background-color:#fafafa !important; font-family:'Roboto'; margin:auto; margin-bottom: 65px; }
    .calendar-page-header { margin-bottom: 65px; margin-top: 10px; margin-right: 20px; } 
    .cal-row-head {background-color: #fafafa !important;}
    .empty {  background-color: white !important; border: 2px solid black;}    
    .inline_time { color: #50d38a !important; }
    .greenHeading{ color: #50d38a !important; margin-left: 20px; }
    .page-header{height: 0 !important;padding-bottom: 0px;margin: -25px 0 50px;border-bottom: 0px;}
    </style>
<!-- New Calendar Style -->

<style type="text/css">
    
  /*Placeholder Color */
input{  
border: 1px solid #bdbdbd !important;

color: white !important;
  }
  
  select{ 
border: 1px solid #bdbdbd !important;

color: white !important;
  }
  
  input:focus{  
background:transparent !Important;
  }
  
  select:focus{ 
background:transparent !Important;
  }
  
  .wizard .content
  {
    /*overflow-y: hidden !important;*/
  }
  
  .wizard .content label {

    color: white !important;

}
  .wizard>.steps .current a 
  {
    background-color: #029898 !Important;
  }
  .wizard>.steps .done a
  {
    background-color: #828f9380 !Important;
  }
  .wizard>.actions a
  {
    background-color: #029898 !Important;
  }
  .wizard>.actions .disabled a
  {
    background-color: #eee !important;
  }
  
  .btn.btn-simple{
    border-color: white !important;
}
  .bootstrap-select > .dropdown-toggle.bs-placeholder, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover, .bootstrap-select > .dropdown-toggle.bs-placeholder:focus, .bootstrap-select > .dropdown-toggle.bs-placeholder:hover {
    color: white;
}

table
{
    color: white;
}
.multiselect.dropdown-toggle.btn.btn-default
{
    display: none !important;
}
  
  .navbar.p-l-5.p-r-5
  {
    display: none !important;
  }
  
</style>
<?php
$activePage = basename($_SERVER['PHP_SELF']);
  ?>
</head>
<body class="theme-green">
<!-- Page Loader -->
<div class="page-loader-wrapper">
    <div class="loader">
        <div class="m-t-30"><img class="zmdi-hc-spin" src="assets/images/logo.svg" width="48" height="48" alt="Oreo"></div>
        <p>Please wait...</p>        
    </div>
</div> 
<!-- Overlay For Sidebars -->
<div class="overlay"></div>


<?php
        require_once('includes/header.php');
?>

<!-- Main Content -->
<section class="content page-calendar" style="margin-top: 0px !important;">
    <div class="block-header">
        <?php require_once('includes/studentTopBar.php'); ?>      
    </div>
    <div class="container-fluid">
         <div class="row">
            <div class="col-md-12 col-lg-8 col-xl-8">
                <div class="card">
                    <div class="body">
                        <div class="calendar-page-header page-header">
                            <div class="pull-right form-inline">
                                <div class="btn-group">
                                    <button class="btn btn-primary" data-calendar-nav="prev"><< Prev</button>
                                    <button class="btn btn-default" data-calendar-nav="today">Today</button>
                                    <button class="btn btn-primary" data-calendar-nav="next">Next >></button>
                                </div>
                            </div>
							<h3></h3>
                        </div>
                        <div id="calendar"></div>                        
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-4 col-xl-4">
                <div class="card profile-header">
                    <div class="body">
                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-12">
                                <div class="profile-image float-md-right"> <img src="/assets/images/sm/avatar5.jpeg" alt=""> </div>
                            </div>
                            <div class="col-lg-8 col-md-8 col-12">
                                <h4 class="m-t-0 m-b-0"><strong><?php echo $data['firstname']; ?></strong> <?php echo $data['lastname']; ?></h4>
                                <span class="job_post">Teacher</span>
                                <p><?php echo $data['address']; ?></p>
                                <div>
                                    <button class="btn btn-primary btn-round ">Send Message</button>
                                </div>

                            </div> 
                            <div class="col-lg-12 col-md-12 col-12" id="about">
                            <hr>
                            <small class="text-muted">Cost Per Hour: </small>
                            <p>$<?php echo $data['customhourly']; ?></p>
                            <hr>
                            <small class="text-muted">Email address: </small>
                            <p><?php echo $data['email']; ?></p>
                            <hr>
                            <small class="text-muted">Phone: </small>
                            <p><?php echo $data['telephone']; ?></p>
                            <hr>
                            <small class="text-muted">Postal: </small>
                            <p><?php echo $data['postalcode']; ?></p>
                            <hr>
                            <small class="text-muted">Birth Date: </small>
                            <p class="m-b-0"><?php echo $data['dateofbirth']; ?></p>
                        </div>               
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<script src="assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->


<?php
    require_once('includes/footerScriptsAddForms.php');
?>
<!-- <script src='assets/plugins/fullcalendar/lib/moment.min.js'></script> -->
<!-- <script src='assets/plugins/fullcalendar/fullcalendar.min.js'></script> -->
<!-- <script src='assets/plugins/fullcalendar/lib/moment.min.js'></script> -->
<!--<script src="assets/js/pages/calendar/calendar.js"></script>-->
<!-- <script type="text/javascript" src="assets/js/bootstrap-multiselect.js"></script> -->

<!-- New Calendar JS -->
<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.8.3/underscore-min.js"></script>
    <script src="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jstimezonedetect/1.0.6/jstz.min.js"></script>
    <script type="text/javascript" src="calendar/js/calendar.js"></script>
<script src="assets/plugins/bootstrap-notify/bootstrap-notify.js"></script> <!-- Bootstrap Notify Plugin Js -->
<script src="assets/js/pages/ui/notifications.js"></script> <!-- Custom Js -->
    <!-- <script type="text/javascript" src="calendar/js/app.js"></script> -->
<!-- New Calendar JS -->

<script>

    var teacherid = <?php echo @$_POST['teacherid']; ?>;
    var hourlyCost = <?php echo (@$data['customhourly'])? @$data['customhourly'] : 0; ?>;
    
    var calendar;
    var options;

  $(document).ready(function() {

  	$(".page-loader-wrapper").css("display", "none");
	  
    "use strict";

    options = {
        events_source: "student-ajaxcall.php?teacherid=<?php echo @$_POST[teacherid]; ?>",
        view: 'month',
        tmpl_path: 'calendar/tmpls/',
        tmpl_cache: false,
         // day: '2013-03-21',
        // day: 'now',

        merge_holidays: false,
        display_week_numbers: false,
        weekbox: false,
        //shows events which fits between time_start and time_end
        show_events_which_fits_time: true,

        onAfterEventsLoad: function(events) {
            if(!events) {
                return;
            }
            var list = $('#eventlist');
            list.html('');

            $.each(events, function(key, obj) {
                //converting the timeZone to UTC
                obj.start = new Date(parseInt(obj.start)).setTimezoneOffset(0);
                obj.end = new Date(parseInt(obj.end)).setTimezoneOffset(0);

                $(document.createElement('li'))
                    .html('<a href="' + obj.url + '">' + obj.title + '</a>')
                    .appendTo(list);
            });
        },
        onAfterViewLoad: function(view) {
            $('.page-header h3').text(this.getTitle());
            $('.btn-group button').removeClass('active');
            $('button[data-calendar-view="' + view + '"]').addClass('active');
        },
        classes: {
            months: {
                general: 'label'
            }
        }
    };

     calendar = $('#calendar').calendar(options);

    $('.btn-group button[data-calendar-nav]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.navigate($this.data('calendar-nav'));
        });
    });

    $('.btn-group button[data-calendar-view]').each(function() {
        var $this = $(this);
        $this.click(function() {
            calendar.view($this.data('calendar-view'));
        });
    });

    $('#first_day').change(function(){
        var value = $(this).val();
        value = value.length ? parseInt(value) : null;
        calendar.setOptions({first_day: value});
        calendar.view();
    });

    $('#language').change(function(){
        calendar.setLanguage($(this).val());
        calendar.view();
    });

    $('#events-in-modal').change(function(){
        var val = $(this).is(':checked') ? $(this).val() : null;
        calendar.setOptions({modal: val});
    });
    $('#format-12-hours').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({format12: val});
        calendar.view();
    });
    $('#show_wbn').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({display_week_numbers: val});
        calendar.view();
    });
    $('#show_wb').change(function(){
        var val = $(this).is(':checked') ? true : false;
        calendar.setOptions({weekbox: val});
        calendar.view();
    });
    $('#events-modal .modal-header, #events-modal .modal-footer').click(function(e){
        //e.preventDefault();
        //e.stopPropagation();
    });

    $(".cal-month-day").on("click",function(){
        console.log($(this).find(".cal-slide-content").html());
  
    });

    $(".list-item").on("mouseover",function(e){
        e.preventDefault(); 
        alert("heree");
        console.log($(this).html());
    });

  }); //main $()


  function removeButton(obj){
    $(obj).find(".inline-elem-buttons-box").remove();
  }


  function addButton(obj, id){
    console.log("addButton : ", obj);
    var cases = $(obj).attr("data-event");
    // if(cases == "empty"){
    //     $(obj).append('');
    // }
     $(obj).append('<span class="inline-elem-buttons-box">\
   <button type="button" class="inline-elem-button btn btn-raised btn-warning btn-round waves-effect" value="cancel" onclick="cancelSlot(this, '+id+')">Cancel Slot</button>\
</span>');

  }// addButton function


  function cancelSlot(obj, id){
      $.ajax({
        type: "POST", 
        url: "student-ajaxcall.php",
        data: { subpage:"slotCancellation", cbID: id},
        dataType: "json",
        success: function(response) { 
                      if(response == "success"){
                          console.log(response); 
                          showNotification("alert-success", "Slot Booking is Cancelled", "bottom", "center", "", "");
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                       calendar = $('#calendar').calendar(options);
                  },
        error: function(xhr, ajaxOptions, thrownError) { 
                      if(xhr.responseText == "success"){
                          console.log(xhr.responseText); 
                          showNotification("alert-success", "Slot Booking is Cancelled", "bottom", "center", "", "");
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                       calendar = $('#calendar').calendar(options);

                      }
      });// ends ajax
  }

  function booking(){

     $(document).css({'cursor' : 'wait'});
     $("#submitBtn").prop("disabled", true);    

    var slotData = "";
    var time = "";
    var date = "";
    
    $("#cal-slide-content").find(".inline-elem-check").each(function(index, obj){
        if($(this).prop("checked")){
          var time = $(this).attr("data-time");
          var date = $(this).attr("data-date");
          slotData += date+","+time+"&";
        }
    });

      slotData = slotData.substring(0, slotData.length-1);

      $.ajax({
        type: "POST", 
        url: "student-ajaxcall.php",
        data: { subpage:"slotBooking", teacherid: teacherid, slotData:slotData, hourlyCost:hourlyCost},
        dataType: "json",
        success: function(response) { 
                      if(response == "success"){
                          console.log(response); 
                          showNotification("alert-success", "Request has been sent", "bottom", "center", "", "");
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                       calendar = $('#calendar').calendar(options);
                  },
        error: function(xhr, ajaxOptions, thrownError) { 
                      if(xhr.responseText == "success"){
                          console.log(xhr.responseText); 
                          showNotification("alert-success", "Request has been sent", "bottom", "center", "", "");
                      }else{
                         showNotification("alert-danger", "Something Went Wrong, try Later !", "bottom", "center", "", "");
                      }
                      $(document.body).css({'cursor' : 'default'});
                      $("#submitBtn").prop("disabled", false);
                       calendar = $('#calendar').calendar(options);

                      }
      });// ends ajax

  }// func ends 

</script>

</body>
</html>
<?php
}
?>