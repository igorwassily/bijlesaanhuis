<?php
	$q = "SELECT COUNT(DISTINCT cb.slot_group_id)counti FROM calendarbooking cb where cb.accepted=0 AND cb.isClassTaken=0 AND cb.isSlotCancelled=0 AND cb.teacherID=$_SESSION[TeacherID] AND datee>=CURRENT_DATE() GROUP BY  cb.teacherID";
	$r = mysqli_query($con, $q);
	
	$__d = mysqli_fetch_assoc($r);

	$counti = ($__d['counti'] > 0)? '<span class="leftSlot">'.$__d['counti'].'</span>' : "";

?>
<style>
.ma-list{height: unset !important;}
.slimScrollDiv{
	height: auto !important;
    overflow: unset !important;
    position: unset !important;}
</style>
<!-- Left Sidebar -->
<aside id="leftsidebar" class="sidebar">
    <div class="tab-content">
        <div class="tab-pane stretchRight active" id="dashboard">
            <div class="menu">
                <ul class="list ma-list">
                    <li>
                        <div class="user-info m-b-20 p-b-15">
							<?php
								$teacherid = $_SESSION['TeacherID'];
									$stmt = $con->prepare("select profile.image from profile inner join teacher ON (teacher.profileID = profile.profileID) inner join user on (user.userID = teacher.userID) inner join usergroup on (user.usergroupID = usergroup.usergroupID) where user.userID = ?");
									$contacttype = 'Teacher';
								$stmt->bind_param("i",$teacherid);
								$stmt->execute();
								$stmt->bind_result($profile);
								$stmt->store_result();
								$stmt->fetch();
							?>
                            <div class="image"><a href="teachers-dashboard"><img src="<?php echo ($profile)? $profile : constant("DEFAULT_PIC"); ?>" alt="User" style="width:112px !important; height: 100px !important;">
								</a></div>
                            <div class="detail">
                                <h4>Hey, <?php echo ucwords(@$_SESSION['TeacherFName']); ?>
								</h4>
								<small><span><a href="/teacher-profile/?tid=<?php echo  $_SESSION['TeacherID'];?>"></a><a href="logout">Uitloggen</a></span></small>
                            </div>
                        </div>	
                    </li>
                    <li<?php if ($thisPage=="Teachers Dashboard") echo " class=\"active open\""; ?>><a href="teachers-dashboard" class="sidemenu_a"><i class="zmdi zmdi-home"></i><span>Kalender</span></a></li>

					
					<li<?php if ($thisPage=="Teachers SlotCreation") echo " class=\"active open\""; ?>><a  class="sidemenu_a" href="teachers-slotcreation"><i class="zmdi zmdi-plus-circle-o"></i><span>Beschikbaarheid aangeven</span></a></li>

					<li<?php if ($thisPage=="Appointment") echo " class=\"active open\""; ?>><a  class="sidemenu_a" href="teachers-appointment"><i class="zmdi zmdi-calendar-note"></i><span>Afspraken</span><?php echo $counti; ?></a></li>
                    <li<?php if ($thisPage=="email" || $thisPage=="chat") echo " class=\"active open\""; ?>><a  class="sidemenu_a" href="message"><i class="zmdi zmdi-email"></i><span>Berichten</span><div class="notify <?php echo ($__newMessages < 1)? "hide" : ""; ?>" id="emailNotify"><span class="heartbit"></span><span class="point"></span></div></a></li>
					<!--<li<?php// if ($thisPage=="Student List") echo " class=\"active open\""; ?>><a  class="sidemenu_a" href="student-list"><i class="zmdi zmdi-open-in-new"></i><span>Bijles inboeken</span></a></li>-->
					<li<?php if ($thisPage=="Student List") echo " class=\"active open\""; ?>><a  class="sidemenu_a" href="/app/student-profile"><i class="zmdi zmdi-open-in-new"></i><span>Bijles inboeken</span></a></li>
           					<li<?php if ($thisPage=="My Profile") echo " class=\"active open\""; ?>><a  class="sidemenu_a" href="/app/teacher-detailed-view.php?tid=<?php echo $_SESSION['TeacherID']; ?>"><i class="zmdi zmdi-account-box"></i><span>Mijn profiel</span></a></li>
                  <!-- <li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Appointments</span> </a>
                        <ul class="ml-menu">
                           <li<?php //if ($thisPage=="Create Appointment") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="#"><span>Create Appointment</span></a></li>
                        </ul>
                    </li> -->

				<!--	<li> <a href="javascript:void(0);" class="menu-toggle"><i class="zmdi zmdi-view-headline"></i><span>Students</span> </a>
                        <ul class="ml-menu">
                           <li<?php //if ($thisPage=="Overview") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="#"><span>Overview</span></a></li>
							<li<?php //if ($thisPage=="Create Student") echo " class=\"active open\""; ?> keep-menu-open="true"><a href="#"><span>Create Student</span></a></li>
                        </ul>
                    </li> -->
					<li<?php if ($thisPage=="Edit Profile") echo " class=\"active open\""; ?>><a  class="sidemenu_a" href="edit-profile"><i class="zmdi zmdi-edit"></i><span>Profiel aanpassen</span></a></li>
                    
                </ul>
            </div>
        </div>
    </div>    
</aside>