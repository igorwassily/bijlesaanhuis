<!-- Main Content -->

<link href="/app/assets/css/edit-student-profile.css" rel="stylesheet"/>
<link href="/app/assets/css/edit-tutor-profile.css" rel="stylesheet"/>
<section class="content edit-profile-content edit-profile-content--student">
	<?php
	if (strpos($_SERVER['REQUEST_URI'], 'app/Admin') !== false) {
		echo '<div class="block-header">';
		require_once('includes/adminTopBar.php');
		echo '</div>';
	}
	?>
    <form class="edit-profile-form" action="/app/actions/edit_scripts.php" method="post">
        <div class="edit-profile-content__section">
            <div class="edit-profile-header">
                <h2 class="ma-subtitle edit-profile-header__heading">Accountgegevens</h2>
            </div>
			<?php
			$studentid = ((isset($_SESSION['StudentID'])) ? $_SESSION['StudentID'] : $_GET['sid']);

			$stmt = $con->prepare("SELECT firstname, lastname, dateofbirth, telephone, email, address, postalcode, city, country, latitude, longitude, place_name, s.admin_comments FROM contact 
                                                    inner join contacttype 
                                                    on (contacttype.contacttypeID = contact.contacttypeID) 
                                                    left join student s on s.userID = contact.userID 
                                                    where contact.userID = ?
                                                    AND contact.prmary = ?
                                                    AND contacttype.contacttype = ?");
			$contacttype = 'Student';
			$primary = 1;
			$stmt->bind_param("iis", $studentid, $primary, $contacttype);
			$stmt->execute();
			$stmt->bind_result($fname, $lname, $dob, $telephone, $email, $address, $postalcode, $city, $country, $lat, $lng, $place_name, $adminComment);
			$stmt->store_result();
			$stmt->fetch();

			//$studentid = $_SESSION['StudentID'];
			$stmt1 = $con->prepare("select username, user.email, contact.telephone, contact.latitude as lat, contact.longitude as `long`

                                                from user 
                                                left join contact on contact.userID = user.userID
                                                inner join usergroup on (user.usergroupID = usergroup.usergroupID) 
                                                where user.userID = ? 
                                                and usergroup.usergroupname = ?");
			$contacttype1 = 'Student';
			$stmt1->bind_param("is", $studentid, $contacttype1);
			$stmt1->execute();
			$stmt1->bind_result($username, $email, $telephone, $lat, $lng);
			$stmt1->store_result();
			$stmt1->fetch();
			?>
            <div class="edit-profile-body">
                <div class="edit-profile-content__group">
                    <label class="input-label" for="booking-code">E-mailadres
                        <i class="fa input-label__i" data-toggle="tooltip" data-placement="right"
                           title="Dit e-mailadres wordt gebruikt voor alle vormen van contact, behalve facturering. Voor facturering wordt onderstaand ouderlijk e-mailadres gebruikt">&#xf05a;</i>
                    </label>
                    <input type="email" class="input-text" placeholder="E-mailadres" name="email" id="email"
                           value="<?php echo $email; ?>">
                </div>
                <div class="edit-profile-content__group">
                    <label class="input-label" for="enfants">Telefoonnummer</label>
                    <input type="text" class="input-text" placeholder="Telefoonnummer" name="stelephone" id="telephone"
                           value="<?php echo $telephone; ?>"
                           pattern=" *\+? *\(? *([0-9] *){3} *\)? *[-\.]? *([0-9] *){4,12}">
                    <span class="terrorLabel hide"></span>
                </div>
                <div class="edit-profile-content__group">
                    <label class="input-label" for="booking-code">Wachtwoord</label>
                    <input type="password" class="input-text" placeholder="Wachtwoord" name="password" id="password"
                           minlength="8">
                    <input name="toggle" type="checkbox" id="toggle" value='0' onchange='togglePassword(this);'>
                    <label style="font-weight: 100; margin-top:5px;" for="toggle" id='toggleText'>&nbsp; Laat wachtwoord
                        zien</label>
				</div>
				<div class="edit-profile-content__group button-wrapper">
                    <button type="submit" class="btn edit-profile-button" id="" name="update-teacher">OPSLAAN</button>
                </div>
            </div>
            <div class="edit-profile-content__row">
                <input type="hidden" class="form-control" placeholder="First Name" name="student-id" id="student-id"
                       value="<?php echo $studentid; ?>">
                <input type="hidden" class="form-control" placeholder="Username" name="username" id="username"
                       value="<?php echo $username; ?>" readonly>
            </div>
        </div>

		<?php
		if (isset($_SESSION['AdminUser'])) {
			?>
            <div class="edit-profile-content__section">
                <div class="edit-profile-header">
                    <h2 class="ma-subtitle edit-profile-header__heading">Admin Comments</h2>
                </div>
				<?php
				$notes = json_decode($adminComment, true);
				if (empty($notes)) {
					if (empty($adminComment)) {
						$notes = [];
					} else {
						$notes = [[
							"text" => $adminComment,
							"time" => "",
							"author" => "",
						]];
					}
				}
				?>
                <div class="row clearfix">
                    <div class="col-lg-12 col-xl-12">
                        <div class="card chat-app" style="background-color: #f1f1f1;margin-bottom: 0;">
                            <div class="chat">
                                <div class="chat-history">
                                    <ul id="adminNoteList">
										<?php
										foreach ($notes as $note) {
											?>
                                            <li class="clearfix">
                                                <div class="message-data text-right">
                                                    <span class="message-data-time"><?php echo $note['time']; ?></span>
                                                    &nbsp;
                                                    <span class="message-data-name"><?php echo $note['author']; ?></span>
                                                    <i class=""></i></div>
                                                <div class="message other-message float-right"><?php echo $note['text']; ?></div>
                                            </li>
											<?php
										}
										?>
                                    </ul>
                                </div>
                                <div class="chat-message clearfix">
                                    <div class="input-group p-t-15">
                                        <input type="text" class="form-control" id="adminNote" autocomplete="off"
                                               placeholder="typ hier..."
                                               style="background-color: white;color:black!important;height: 4rem;"/>
                                        <span id="btnAdminNote" class="input-group-addon"
                                              style="padding:0 18px;background-color: white!important;">
                                        <i class="glyphicon glyphicon-send" id="chatsenderBtn"></i>
                                    </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
			<?php
		}
		?>

        <div class="edit-profile-content__section">
            <div class="edit-profile-header">
                <h2 class="ma-subtitle edit-profile-header__heading">Persoonsgegevens</h2>
            </div>
            <div class="edit-profile-body">
				<?php
				//$studentid = $_SESSION['StudentID'];
				$stmt11 = $con->prepare("SELECT firstname, 
                                                       lastname, 
                                                       dateofbirth, 
                                                       telephone, 
                                                       email, 
                                                       address, 
                                                       postalcode, 
                                                       city, 
                                                       country,
                                                       place_name
                                                from contact 
                                                where contact.userID = ? 
                                                AND contact.prmary = ? 
                                                AND contact.contacttypeID = ?");
				$contacttype = '2'; //Student
				$primary = 1;
				$stmt11->bind_param("iii", $studentid, $primary, $contacttype);
				$stmt11->execute();
				$stmt11->bind_result($sfname, $slname, $sdob, $stelephone, $semail, $saddress, $spostalcode, $scity, $scountry, $place_name);
				$stmt11->store_result();
				$stmt11->fetch();

				$street = "";
				$houseNumber = "";
				$pos = strrpos($address, " ");
				if ($pos !== false) {
					$street = substr($address, 0, $pos);
					$houseNumber = substr($address, $pos + 1);
				}

				$stmt = $con->prepare("SELECT firstname, 
                                                     lastname, 
                                                     dateofbirth, 
                                                     telephone, 
                                                     email, 
                                                     address, 
                                                     postalcode, 
                                                     city, 
                                                     country 
                                                from contact 
                                                where contact.userID = ? 
                                                AND contact.prmary = ? 
                                                AND contact.contacttypeID = ?");
				$contacttype = '3'; //Parent
				$primary = 0;
				$stmt->bind_param("iii", $studentid, $primary, $contacttype);
				$stmt->execute();
				$stmt->bind_result($pfname, $plname, $pdob, $ptelephone, $pemail, $paddress, $ppostalcode, $pcity, $pcountry);
				$stmt->store_result();
				$stmt->fetch();

				?>
                <div class="edit-profile-content__group">
                    <label class="input-label" for="booking-code">Voornaam leerling</label>
                    <input type="hidden" class="form-control" placeholder="First Name" name="choice" id="choice"
                           value="StudentPersonalInfo">
                    <input type="hidden" class="form-control" placeholder="First Name" name="student-id" id="student-id"
                           value="<?php echo $studentid; ?>">
                    <input type="hidden" class="form-control" placeholder="First Name" name="sdob" id="dob"
                           value="<?php echo $sdob; ?>">
                    <input type="hidden" class="form-control" placeholder="First Name" name="saddress" id="address"
                           value="<?php echo $saddress; ?>">
                    <input type="hidden" class="form-control" placeholder="First Name" name="spostal-code"
                           id="postal-code" value="<?php echo $spostalcode; ?>">
                    <input type="hidden" class="form-control" placeholder="First Name" name="scity" id="city"
                           value="<?php echo $scity; ?>">
                    <input type="text" class="input-text" placeholder="Voornaam leerling" name="sfirst-name"
                           id="first-name" value="<?php echo $sfname; ?>">
                </div>
                <div class="edit-profile-content__group">
                    <label class="input-label" for="booking-code">Achternaam leerling</label>
                    <input type="text" class="input-text" placeholder="Achternaam leerling" name="slast-name"
                           id="last-name" value="<?php echo $slname; ?>">
                </div>
                <div class="edit-profile-content__group">
                    <label class="input-label" for="booking-code">Voornaam ouder</label>
                    <input type="hidden" class="form-control" placeholder="First Name" name="student-id" id="student-id"
                           value="<?php echo $studentid; ?>">
                    <input type="text" class="input-text" placeholder="Voornaam ouder" name="pfirst-name"
                           id="first-name" value="<?php echo $pfname; ?>">
                </div>
                <div class="edit-profile-content__group">
                    <label class="input-label" for="booking-code">Achternaam ouder</label>
                    <input type="text" class="input-text" placeholder="Achternaam ouder" name="plast-name"
                           id="last-name" value="<?php echo $plname; ?>">
                </div>
                <div class="edit-profile-content__group">
                    <label class="input-label" for="enfants">Telefoonnummer ouder</label>
                    <input type="text" class="input-text" placeholder="Telefoonnummer ouder" name="ptelephone"
                           id="telephone1" value="<?php echo $ptelephone; ?>"
                           pattern=" *\+? *\(? *([0-9] *){3} *\)? *[-\.]? *([0-9] *){4,12}">
                    <span class="terrorLabel2 hide"> </span>
                </div>
                <div class="edit-profile-content__group">
                    <label class="input-label" for="enfants">E-mailadres ouder
                        <i class="fa input-label__i" data-toggle="tooltip" data-placement="right"
                           title="Dit e-mailadres wordt uitsluitend gebruikt voor ">&#xf05a;</i>
                    </label>
                    <input type="text" class="input-text" placeholder="E-mailadres ouder" name="pemail" id="pemail"
                           value="<?php echo $pemail; ?>" required>
				</div>
				<div class="edit-profile-content__group button-wrapper">
                    <button type="submit" class="btn edit-profile-button" id="" name="update-teacher">OPSLAAN</button>
                </div>
            </div>
        </div>

        <div class="edit-profile-content__section--2col">
            <div class="edit-profile-content__section">
                <div class="edit-profile-header">
                    <h2 class="ma-subtitle edit-profile-header__heading">Pas bijleslocatie aan</h2>
                </div>
                <input type="hidden" class="form-control" name="lat" id="lat3"
                       value="<?php echo ($lat) ? $lat : 33.719361; ?>">
                <input type="hidden" class="form-control" name="lng" id="lng3"
                       value="<?php echo ($lng) ? $lng : 73.074144; ?>">
                <input type="hidden" class="form-control" name="place_name" id="place_name3"
                       value="<?php echo $place_name; ?>">
                <input type="hidden" class="form-control" name="country" id="country" value="<?php echo $country; ?>">
            	<div class="edit-profile-body">
                    <div class="edit-profile-content__group">
                        <label for="error" id="errorMessage3"
                               style="padding-bottom: 11px; color:red !important; display: none;">Deze combinatie
                            bestaat niet.</label>
                        <input class="input-text" type="text" placeholder="Postcode" id="postcode3" name="postcode"
                               required value="<?php echo $postalcode; ?>">
                    </div>
                    <div class="edit-profile-content__group">
                        <input class="input-text" type="text" placeholder="Huisnummer" id="huisnummer3"
                               name="housenumber" required value="<?php echo $houseNumber; ?>">
                    </div>
                    <div class="edit-profile-content__group">
                        <input class="input-text" type="text" placeholder="Straatnaam" id="straatnaam3" name="street"
                               required value="<?php echo $street; ?>">
					</div>
                    <div class="edit-profile-content__group">
                        <input class="input-text" type="text" placeholder="Woonplaats" id="woonplaats3" name="city"
                               required value="<?php echo $city; ?>">
                        <!--                        <div id='map' style='position:absolute; top: 0;bottom: 0;width: 100%; height: 100%;  overflow:visible;'></div>-->
                    </div>
                    <div class="edit-profile-content__group button-wrapper">
                        <button type="submit" class="btn edit-profile-button" id="" name="update-locatie">OPSLAAN
                        </button>
                        <input type="hidden" class="form-control" placeholder="First Name" name="student-id"
                               id="student-id" value="<?php echo $studentid; ?>">
                    </div>
                </div>
            </div>
        </div>
    </form>
</section>
<script src="/app/assets/bundles/libscripts.bundle.js"></script>
<script src="/app/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->

<?php
if (!isset($_SESSION['AdminUser'])) {
	require_once('footerScriptsAddForms.php');
}
?>

<?php

if (isset($_GET['successMsg'])) {
	echo '<script type="text/javascript>"';
	echo 'swal("Good job!", "You clicked the button!", "success");';
	echo '</script>';
}

?>
<script>
	$(document).ready(function () {

		/**
		 * Quick hack or fix by Oscar to have the page loading properly
		 */
		$(".theme-green .page-loader-wrapper").css('display', 'none');

		var passwordCount = 0;
		$(document).on('change', '#password', function () {
			if (passwordCount > 0) {
				console.log('password change');
				$(this).siblings('.changed').remove();
				if ($(this).val().length < 8) {
					$(this).after('<label class="changed">Het wachtwoord moet minimaal 8 tekens lang zijn</label>');
				} else {
					$(this).after('<label class="changed">Klik op Opslaan om je veranderingen op te slaan</label>');
				}
			}
			passwordCount++;
		});

		$(":input").keyup(function () {

			// avoid redundant info message beneath the input
			if ($(this).parents().eq(0).find('.changed').length == 0) {
				var imageChange = "Selecteer het juiste gebied en klik op Opslaan";
				var passwordChange = "Het wachtwoord moet minimaal 8 tekens lang zijn";
				var personalChange = "Klik op Opslaan om je veranderingen op te slaan";
				var invalidPhone = "Voer een geldig telefoonnummer in";
				var normalErrorMessage = "Klik op Opslaan om je veranderingen op te slaan";

				if ($(this).attr('name') == 'stelephone') {
					var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}/;

					if (re.test($(this).val()) !== true) {
						$(this).after('<label class="changed">' + invalidPhone + '</label>');
					} else {
						$(this).after('<label class="changed">' + normalErrorMessage + '</label>');
					}
				}

				if ($(this).attr('name') !== 'password' && $(this).attr('name') !== 'stelephone' && this.id !== 'login-password') {
					$(this).after('<label class="changed">' + normalErrorMessage + '</label>');
				}

				if ($(this).attr('name') !== 'password' && this.id !== 'login-password') {
					console.log('not password');
					console.log(this);
				}
			}
		});

		var place_name = $('#place_name3').val();
		var lat = $('#lat3').val();
		var long = $('#lng3').val();

		$("#huisnummer3, #postcode3").on("change paste", function () {
			var houseNumber = $('#huisnummer3').val();

			if (houseNumber.length > 0 && houseNumber.length <= 4) {
				var postalCode = $('#postcode3').val();

				if (postalCode.length >= 4 && postalCode.length <= 6) {
					$.ajax({
						type: "GET",
						url: "/api/Geo/Address?postalCode=" + postalCode + "&streetNumber=" + houseNumber,
						success: function (response) {
							$('#errorMessage3').css("display", "none");
							let street = response.streetName;
							let place = response.locality;

							$('#straatnaam3').val(street);
							$('#woonplaats3').val(place);

							let lat = response.latitude;
							let long = response.longitude;
							isAddressChanged = true;

							$('#lat3').val(lat);
							$('#lng3').val(long);

							let placeName = street + " " + response.streetNumber + ", " + response.postalCode + " " + place + " Nederland";
							$('#place_name3').val(placeName);
							$('#country').val("Nederland");

							$(function () {
								$("button[name='update-locatie']").attr("disabled", false);
							});
						},
						error: function (error) {
							console.log("error!s");
							console.log(error);
							$('#errorMessage3').css("display", "block");
							//$('#update-more-courses').attr("disabled",true);
							$(function() {
								$("button[name='update-locatie']").attr("disabled", true);
							});

							$('#straatnaam3').val("");
							$('#woonplaats3').val("");

							$('#lat3').val("");
							$('#lng3').val("");
							$('#place_name3').val("");
						}
					});
				}
			}
		});

		$("#add-level-btn2").click(function (e) {
			e.preventDefault();

			var phone = $("#telephone").val();

			if (!(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(phone))) {
				$(".terrorLabel").text("Invalid telephone number").removeClass("hide");
				return;
			}

			$("#submit-btn2").click();

		});

		$("#add-level-btn3").click(function (e) {
			e.preventDefault();

			var phone = $("#telephone1").val();
			alert(230);
			if (!(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,12}$/im.test(phone))) {
				$(".terrorLabel2").text("Invalid telephone number").removeClass("hide");
				return;
			}

			$("#submit-btn3").click();

		});


		$("#add-level-btn1").click(function (e) {
			e.preventDefault();

			var pass = $("#password").val();
			var cpass = $("#cpassword").val();

			if (pass.length < 8) {
				$(".errorLabel").text("Password must be minimum 8 characters").removeClass("hide");
				return;
			}

			if (pass != cpass) {
				$(".errorLabel").text("Password does not matched").removeClass("hide");
				return;
			}

			$("#submit-btn").click();

		});

	});

	var lat =<?php echo ($lat) ? $lat : 33.719361; ?>;
	var lng =<?php echo ($lng) ? $lng : 73.074144; ?>;
	var place_name = <?php echo ($place_name) ? "'" . $place_name . "'" : "''"; ?>;
	var map = null;
	var canvas = null;


	function togglePassword(obj) {
		var checkBox = document.getElementById("toggle");

		// If the checkbox is checked, display the output text
		if (checkBox.checked == true) {
			$("#password").attr("type", "text");
		} else {
			$("#password").attr("type", "password");
		}
	}

	function addAdminNote(msg) {
		if (msg !== "") {
			$.ajax({
				type: "POST",
				url: "/app/actions/edit_scripts.php",
				data: {
					"sid": "<?php echo $studentid; ?>",
					"action": "add_note",
					"value": msg,
				},
				success: function (response) {
					console.log(response);
					if (response.success) {
						let n = response.data[response.data.length - 1];
						let e = '<li class="clearfix"><div class="message-data text-right"><span class="message-data-time">' + n.time + '</span>&nbsp;<span class="message-data-name">' + n.author + '</span> <i class=""></i></div><div class="message other-message float-right">' + n.text + '</div></li>';
						$("#adminNoteList").append(e);
						$("#adminNote").val("");
					}
				},
				error: function (error) {
					console.log("error!s");
					console.log(error);
				}
			});
		}
	}

	$(document).ready(function () {
		$("#btnAdminNote").on('click', function () {
			addAdminNote($('#adminNote').val());
		});
		$("#adminNote").on("keypress", function (event) {
			if (event.key === 'Enter') {
				event.preventDefault();
				addAdminNote($('#adminNote').val());
			}
		});

		$("#showHide").on('change', function () {
			if ($(this).is(":checked")) {
				$("#password").attr("type", "text");
				//$("#labelforShowHide").html("Hide Password");
			} else {
				$("#password").attr("type", "password");
				//$("#labelforShowHide").html("Show Password");

			}
		});

		$('[data-toggle="tooltip"]').tooltip();
	});

</script>