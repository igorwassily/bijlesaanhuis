<?php

require_once('connection.php');
require_once("../../../inc/Email.php");

session_start();

if (isset($_POST['filename'])) {
	if ($_POST['filename'] == "chat_text_msg_by_sender") {
		$message = isset($_POST['msg']) ? $_POST['msg'] : "";
		if (empty($message)) {
			echo '{"status":"failed"}';
			exit();
		}
		$message = test_input($message);

		$senderID = (isset($_POST['senderId']) && $_POST['senderId']) ? $_POST['senderId'] : 0;
		$receiverID = (isset($_POST['recvId']) && $_POST['recvId']) ? $_POST['recvId'] : 0;
		$subject = (isset($_POST['subj']) && $_POST['subj']) ? $_POST['subj'] : 0;
		$time = (isset($_POST['datetime']) && $_POST['datetime']) ? $_POST['datetime'] : 0;
		$flag = true;

		if ($time)
			$query = "INSERT INTO `email` (`emailID`, `senderID`, `receiverID`, `subject`, `message`, `date_time`, `firstMessage`) VALUES (NULL, " . $senderID . ", " . $receiverID . ", '" . $subject . "', '" . $message . "',  '" . $time . "', ".$flag.");";
		else {
			date_default_timezone_set('Europe/Amsterdam');
			$d1 = new DateTime();
			$query = "INSERT INTO `email` (`emailID`, `senderID`, `receiverID`, `subject`, `message`, `date_time`, `firstMessage`) VALUES (NULL, " . $senderID . ", " . $receiverID . ", '" . $subject . "', '" . $message . "',  '" . $d1->format('Y-m-d H:i:s') . "', ".$flag.");";
		}

		$result = mysqli_query($con, $query);

		if ($result) {
			if($message != "Bijlesverzoek ingediend" || $message != "Bijles geannuleerd" || strpos($message, "Bijles geannuleerd / geweigerd") !== true || $message != "Bijles geaccepteerd" || $message != "Afspraak ingeboekt" || strpos($row['message'], "Bijles geaccepteerd") !== false){
				$id = mysqli_insert_id($con);
				$tpl = isset($_SESSION['StudentID']) ? 'message_for_tutor_via_public_profile_for_tutor' : 'message_from_tutor_sent_to_student';
				$email = new Email($db, $tpl);
				$email->Prepare($id);
				$email->Send('receiver');
			}

			echo '{"status":"success"}';
		} else
			echo '{"status":"failed"}';
	}//if ends here

	elseif ($_POST['filename'] == "loadAllMessages") {

		$senderID = (isset($_POST['senderId']) && $_POST['senderId']) ? $_POST['senderId'] : 0;
		$receiverID = (isset($_POST['recvId']) && $_POST['recvId']) ? $_POST['recvId'] : 0;
		$senderName = (isset($_POST['senderName']) && $_POST['senderName']) ? $_POST['senderName'] : 'unknown';

		$query = "SELECT senderID, receiverID, message, is_read, DATE_FORMAT(date_time, '%H:%i, %d %M %Y ')daatetime FROM `email`  WHERE senderID IN ($senderID, $receiverID) AND receiverID IN ($senderID, $receiverID) ORDER BY emailID ASC";

		$result = mysqli_query($con, $query);

		while ($row = mysqli_fetch_assoc($result)) { //zmdi zmdi-circle me
			if ($receiverID == $row['senderID']) {
				$styleClass2 = "";
				$messageClass2 = "other-message";


				if($row['message'] == "Bijlesverzoek ingediend" || $row['message'] == "Bijles geannuleerd" || strpos($row['message'], "Bijles geannuleerd / geweigerd") !== false || $row['message'] == "Bijles geaccepteerd" || $row['message'] == "Afspraak ingeboekt" || strpos($row['message'], "Bijles geaccepteerd") !== false){
					$styleClass2 = "class='auto-message-list-item'";
					$messageClass2 = "other-auto-message";
				}

				if (!empty($row['message']) && $row['message'] != "0") {
					echo '<li '.$styleClass2.' class="clearfix"><div class="message-data text-right"> 
					<span class="message-data-time">' . getDutchMonth($row['daatetime']) . '</span> &nbsp; 
					<span class="message-data-name">Ik</span> <i class=""></i> </div> 
					<div class="message '.$messageClass2.'">' . $row['message'] . '</div>                                
				 </li>';
				}

			} else {
				$styleClass = "";
				$messageClass = "my-message";

				if($row['message'] == "Bijlesverzoek ingediend" || $row['message'] == "Bijles geannuleerd" || strpos($row['message'], "Bijles geannuleerd / geweigerd") !== false || $row['message'] == "Bijles geaccepteerd" || $row['message'] == "Afspraak ingeboekt" || strpos($row['message'], "Bijles geaccepteerd") !== false){
					$styleClass = "class='auto-message-list-item'";
					$messageClass = "my-auto-message";
				}



				if (!empty($row['message']) && $row['message'] != "0") {
					echo '<li '.$styleClass.'><div class="message-data">
                    <span class="message-data-time">' . getDutchMonth($row['daatetime']) . '</span> &nbsp;
					<span class="message-data-name"><i class=""></i>' . $senderName . '</span>
                </div>
                <div class="message '. $messageClass.'">
                    ' . $row['message'] . '
                </div>
            </li>';
				}
			}
		}//while ends

		//mark all message as read
		$query2 = "UPDATE `email` SET `is_read`=1 WHERE senderID=$senderID and receiverID=$receiverID;";

		$result = mysqli_query($con, $query2);
	}//elseif ends here

	elseif ($_POST['filename'] == "notifi" && (isset($_SESSION['StudentID']) || isset($_SESSION['TeacherID']))) {

		// $receiverID = (isset($_POST['recvId']) && $_POST['recvId'])? $_POST['recvId']:0;

		$receiverID = (isset($_SESSION['StudentID'])) ? $_SESSION['StudentID'] : $_SESSION['TeacherID'];

		$query = "SELECT COUNT(emailID)unreademails  FROM `email`  WHERE receiverID=$receiverID AND is_read=0";

		$result = mysqli_query($con, $query);
		$data = mysqli_fetch_assoc($result);

		echo $data['unreademails'];

		/*if($data['email'] > 0){
			echo "Unread Messsages";
		}else{
			echo "No New Messages";
		}
		*/

	}//elseif ends here

	elseif ($_POST['filename'] == "loadsender_msg") {

		$senderID = (isset($_POST['senderId']) && $_POST['senderId']) ? $_POST['senderId'] : 0;
		$receiverID = (isset($_POST['recvId']) && $_POST['recvId']) ? $_POST['recvId'] : 0;
		$senderName = (isset($_POST['senderName']) && $_POST['senderName']) ? $_POST['senderName'] : 'unknown';

		$query = "SELECT senderID, receiverID, message, is_read, DATE_FORMAT(date_time, '%H:%i, %d %M %Y ')daatetime, `system` FROM `email`  WHERE senderID=$senderID AND receiverID=$receiverID AND is_read=0 ORDER BY emailID ASC";

		$result = mysqli_query($con, $query);

		while ($row = mysqli_fetch_assoc($result)) { //zmdi zmdi-circle online
			if ($senderID == $row['senderID']) {
				$styleClass = "";
				$messageClass = "my-message";

				if($row['message'] == "Bijlesverzoek ingediend" || $row['message'] == "Bijles geannuleerd" || strpos($row['message'], "Bijles geannuleerd / geweigerd") !== false || $row['message'] == "Bijles geaccepteerd" || $row['message'] == "Afspraak ingeboekt" || strpos($row['message'], "Bijles geacceptered") !== false || !empty($row['system'])){
					$styleClass = "class='auto-message-list-item'";
					$messageClass = "my-auto-message";
				}

				echo '<li '.$styleClass.'><div class="message-data">
                    <span class="message-data-time">' . getDutchMonth($row['daatetime']) . '</span>  &nbsp;
					<span class="message-data-name"><i class=""></i>' . $senderName . '</span>
                </div>
                <div class="message '.$messageClass.'">
                    ' . $row['message'] . '
                </div>
            </li>';
			} else {
				echo "No new msg";
			}
		}
		$query = "UPDATE `email` SET `is_read`=1 WHERE senderID=$senderID and receiverID=$receiverID;";
		$result = mysqli_query($con, $query);
		// if ($result) {
		// 	echo "Updated";
		// }else{
		// echo "Not Updated";
		// }
	}//elseif ends here
	elseif ($_POST['filename'] == 'deleteMessage') {
		$id = mysqli_real_escape_string($con, $_POST['id']);
		$query = "DELETE FROM email WHERE emailID=" . $id;

		$result = mysqli_query($con, $query);

		echo "succ";
	}
}

function getDutchMonth($date)
{
	$x = explode(",", $date);
	$m = explode(" ", trim($x[1]));

	$months = array(
		"January" => "Januari",
		"February" => "Februari",
		"March" => "Maart",
		"April" => "April",
		"May" => "Mei",
		"June" => "Juni",
		"July" => "Juli",
		"August" => "Augustus",
		"September" => "September",
		"October" => "Oktober",
		"November" => "November",
		"December" => "December"
	);

	return $x[0] . ", " . $m[0] . " " . $months[$m[1]] . " " . $m[2];
}
