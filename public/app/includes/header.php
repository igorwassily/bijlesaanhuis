<?php
    require_once('connection.php');
    require_once('tracking.php');
?>

<?php



$userID   =   "";

if(isset($_SESSION['TeacherID'])){
    $userID = $_SESSION['TeacherID'];
} else if(isset($_SESSION['StudentID'])){
    $userID = $_SESSION['StudentID'];
}

	$m_query= 'SELECT e.senderID, e.receiverID , (SELECT message FROM email ee WHERE (ee.senderID IN (e.receiverID, c.userID) AND ee.receiverID IN (e.receiverID, c.userID))  ORDER BY ee.emailID DESC LIMIT 1)message, (SELECT DATE_FORMAT(date_time, "%H:%i, %d %M %Y") FROM email ee WHERE (ee.senderID IN (e.receiverID, c.userID) AND ee.receiverID IN (e.receiverID, c.userID))  ORDER BY ee.emailID DESC LIMIT 1)dtime, (SELECT COUNT(emailID)email FROM `email` WHERE email.senderID=e.senderID AND email.receiverID=e.receiverID AND is_read=0)unreadMail, (SELECT image FROM `profile` p , teacher t WHERE p.profileID=t.profileID AND t.userID=c.userID)image, (SELECT contacttype FROM `contacttype` WHERE `contacttype`.contacttypeID=c.contacttypeID)contacttype, c.firstname, c.lastname, c.contacttypeID FROM email e, contact c WHERE ((receiverID='.$userID.' AND c.userID=e.senderID) ) AND c.userID<>'.$userID.' AND contacttypeID IN (1,2) GROUP BY c.userID ORDER BY e.emailID ASC';


        $m_result = mysqli_query($con, $m_query);
        $newMessages = 0;

        while($m_result && $m_data = mysqli_fetch_assoc($m_result)){

            if($m_data['unreadMail'] > 0){
                $newMessages++;
            }
        }

if(isset($_SESSION['TeacherID'])){
    $q = "SELECT COUNT(DISTINCT cb.slot_group_id)counti FROM calendarbooking cb where cb.accepted=0 AND cb.isClassTaken=0 AND cb.isSlotCancelled=0 AND cb.teacherID=$_SESSION[TeacherID] AND datee>=CURRENT_DATE() GROUP BY  cb.teacherID";
    $r = mysqli_query($con, $q);

    $__d = mysqli_fetch_assoc($r);

    if($__d !== NULL){
	    $counti = (isset($__d['counti']) && $__d['counti'] > 0)? '<span style="margin: 0 0 0 12px;" class="leftSlot">'.$__d['counti'].'</span>' : "";
    }
    else {
        $counti = "";
    }

}

if(isset($_SESSION['StudentID'])){
    $q = "SELECT COUNT(DISTINCT cb.slot_group_id)counti FROM calendarbooking cb where cb.accepted=1 AND cb.isClassTaken=0 AND cb.isSlotCancelled=0 AND cb.studentID=$_SESSION[StudentID] AND datee>=CURRENT_DATE() GROUP BY  cb.studentID";
	$r = mysqli_query($con, $q);

	$__d = mysqli_fetch_assoc($r);

	$counti = (isset($__d['counti']) && $__d['counti'] > 0)? '<span style="margin: 0 0 0 12px;" class="leftSlot">'.$__d['counti'].'</span>' : "";

}



?>
<link rel='stylesheet'  href='/modal.css' type='text/css' />

<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">

<!--<link href="/wp-content/themes/Divi/style.css?ver=3.19.18" rel="stylesheet">-->

<script src="https://code.jquery.com/jquery-1.12.4.min.js"></script>
<script src="/app/assets/plugins/jquery-validation/jquery.validate.js" defer></script> <!-- Jquery Validation Plugin Css -->

<?php
if(!isset($_SESSION['TeacherID']) && !isset($_SESSION['StudentID'])){
?>
<script src="/app/assets/js/modal.js" defer></script>
<?php }  ?>

<link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' />

<!-- datepicker test -->
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">

<style>
.zmdi{
	font: normal normal normal 14px/1 'Material-Design-Iconic-Font' !important;
}
.glyphicon{
	font-family: 'Glyphicons Halflings' !important;
}
.form-control{
	background-color: transparent !important;
}
.ma-main-title{
	font-size: 36px !important;
	color: #5cc75f !important;
	font-family: 'Poppins', sans-serif;
	font-weight: 400;
}
.ma-subtitle{
	font-size:21px !important;
	color: #5cc75f !important;
	font-family: 'Poppins', sans-serif !important;
	font-weight: 600;
    font-style: normal;

}
.block-header{
    padding: 5px !important;
}
#leftsidebar{
	margin-top: 119px;
}
#leftsidebar .user-info{padding-top: 20px;}
/* section.content{margin-top: 119px !important;} */


/*Tob Bar*/
#top-header{
    z-index: 9999;
    width: 100%;
	position: absolute;
    top: 0;
}
#top-header .container{
    padding-top: 9px;
    font-weight: 600;
    line-height: 11px;
    font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif;
    padding-bottom: 10px;
    padding-left: 0px;
    padding-right: 0px;
}
#et-info{
	float: left;
    display: block;
}
#et-info, #et-secondary-menu>ul>li a{
	padding-bottom: 0 !important;
}
#et-info-phone,#et-info-phone span{font-size:12px; color: #fff !important;}
#et-info-phone i{margin-right: 2px; color: #fff !important;}
#et-secondary-menu{float: right;}
#et-secondary-nav{list-style: none;}
#et-secondary-nav, #et-secondary-nav li{
	display: inline-block;
	margin-bottom: 0;
}
#et-secondary-nav li{
	text-align: right;
	margin-right: 15px;
}
#et-secondary-nav li a{font-size: 12px; color: #fff !important;}


/* Navigation */

.navbar-default{
	margin-top:32px;
}
.navbar{
	width:100%;
	background-color: #fff !important;
	border-radius: 0px;
	position:relative;
	z-index:99;
	display:block;
}
.navbar-nav{
	display:inline-block;
	float: right;
	margin-right: 0px;
    margin-top: 20px;
}
.navbar-brand{
	width: 224px;
    height: 85px;
}
.navbar-brand img{width:100%;}

.navbar a:not(.btn):not(.dropdown-item){
	    color: #486066 !important;
}
.navbar a:not(.btn):not(.dropdown-item):hover{
	    color: #5CC75F !important;
}
.nav.navbar-nav li a.active-nav-item {
    color: #5cc75f!important;
}
.active-nav-item::after {
    background: #5cc75f!important;
    display: inline-block;
    height: 4px;
    width: 100%;
    content: "";
    position: absolute;
    bottom: 0;
    left: 0;
}

.navbar-default .navbar-nav>li>a{
	padding: 12px 15px;
    font-size: 16px;
    font-family: 'Poppins', sans-serif !important;
    font-weight: 600;
}
.navbar-collapse .navbar-nav .nav-item>a{
	padding: 10px 0px !important;
}
.navbar .navbar-toggle{
	width: 42px;
    height: 38px;
    margin-top: -4px;
    margin-right: 17px;
}
.navbar-default .navbar-toggle::before{
	display: none;
}

.initials{
    height: 55px !important;
    width: 55px;
    margin-top: -5px;
    margin-left: 10px;
    min-height: 110% !important;
}
.initials .circle{
    background-color: #eaeaea;
    border-radius: 100%;
    padding: 30px 30px !important;
    font-size: 25px !important;
    text-align: center;
    float: left;
}
.initials .circle:hover{
    color: #486066 !important;
    background-color: #eaeaea !important;
}
.initials .dropdown-menu{
    padding: 0 !important;
}
.initials .dropdown-menu li a{
    line-height: 30px;
}
.initials i{
    padding-right: 10px;
}
.profilepic i{
    opacity: 0;
}
.profile-picture {
    position: absolute;
    top: 21px;
    left: 0;
    right: 0;
}
.nav li li{
    padding: 0 0 !important;
    border-color: rgba(0, 0, 0, 0.05);
    border-style: solid;
    border-width: 0 0 1px 0;
}
.leftSlot{
    background-color: #FE5D5D  !important;
}
.leftSlot:hover{
    color: white !important;
}
.inloggen:after{
    content: '';
    display: inline-block;
    height: 15px;
    width: 15px;
    background: url(/s3/content/user-1.png);
    background-size: 100% 100%;
    margin-left: 5px;
}
.ma-dropdown li:last-of-type{
    border-bottom: none;
}

/*responsive*/
@media (max-width: 1168px){
    .ma-sidebar-container{
        margin-top: 50px !important;
    }
}


@media (max-width: 991px){
    #logo{
        width: 100% !important;
    }
	.navbar-nav{
		margin-right: 10px;
	}
	.navbar-default .navbar-nav>li>a{
		padding: 15px 10px;
	}
	#et-info{
		padding-top: 0 !important;
	}
	#et-secondary-menu, #et-secondary-nav{
		display: block !important;
	}
}

@media (min-width: 1070px){
    #top-header .container{
        width: 90% !important;
        max-width: 100% !important;
    }
}

.mob-caret{
      display: none;
      position: absolute;
  }

.meer-mob{
    display: none;
}
@media (max-width: 1070px){
    .navbar.navbar-default > .container {
        padding-right: 0 !important;
    }
    .navbar-collapse {
        margin-top: 1px;
    }
    .navbar-collapse.collapse {
        display: none !important;
    }
    .navbar-collapse.collapse.in {
        display: block !important;
    }
    .navbar-header .collapse, .navbar-toggle {
        display:block !important;
    }
    .navbar-header {
        float:none;
    }
    ul.nav.navbar-nav.navbar-right {
       height: 100% !important;
    }
    .container>.navbar-collapse, .container>.navbar-header {
        margin-right: -15px;
        margin-left: -15px;
    }
    .navbar-toggle {
        display: block !important;
    }
  .meer-mob{
      display: inline-block;
  }

  .mob-caret{
      display: inline-block ;
      position: relative;
  }

  .initials .circle{
    background: none !important;
    text-align: left !important;
    float: none !important;
    border-radius: 0px !important;
    font-size: 16px !important;
  }

  .initials .circle:hover{
      background: none !important;
      color: #5cc75f;
  }


  .profile-picture{
      display: none;
  }
    .navbar-collapse{
        text-align: left !important;
        width: 100%;
        position: absolute;
        background: white;
        padding-left: 65px !important;
        padding-top: 5%;
        padding-bottom: 5%;
        padding-right: 35px !important;

    }
	.navbar-header{
		width:100%;
	}
	.navbar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand {
		display: block;
		width: 240px;
		height: 75px;
		margin: 5px;
	}
	.navbar-default .navbar-collapse, .navbar-default .navbar-form{
		text-align: center;
	}
	.navbar-nav{
		display: inline-block;
		float: none !important;
		margin-right: 0px;
		margin-top: 0;
		margin-bottom: 0;
        width: 100%;
	}
	.navbar .navbar-toggle {
		position: relative;
		margin-right: 25px;
		margin-top: 28px;
	}
    .initials{
        margin-top: 5px;
    }
    nav .dropdown:hover ul{
        display: contents !important;
    }
    nav .dropdown{
        width: 100%
    }
    nav .dropdown-menu{
        width: 100% !important;
    }
    nav .dropdown-menu a{
        font-size: 16px !important;
    }
    nav .profilepic i{
        opacity: 1;
        float: right;
    }
    nav .dropdown .dropdown-menu li a{
        padding: 10px 0px !important;
        border-bottom: solid 1px rgba(0, 0, 0, 0.05) !important;
    }

    .navbar-toggle{
        border: none !important;
    }
    .navbar-toggle{
        border: none !important;
    }
    .ma-dropdown-menu .container{
        display: contents;
    }
    .ma-dropdown-menu .row{
        display: contents;
    }
    .ma-dropdown-menu .ma-left, .ma-right{
        width: 100% !important;
        padding-left: 0 !important;
    }
    .navbar-right li{
        display: contents !important;
    }
    .ma-dropdown h3{
        margin-top: 20px;
    }
    nav .dropdown .ma-dropdown li a{
        padding-left: 0px !important;
        border-bottom: solid 1px rgba(0, 0, 0, 0.05) !important;
    }

}
@media (max-width: 556px){
    #logo{
        width: 70% !important;
        margin-left: -40px;
    }
	.navbar>.container .navbar-brand, .navbar>.container-fluid .navbar-brand{
		width: 205px;
		height: 65px;
	}
	.navbar .navbar-toggle{
		margin-right: 0px;
		margin-top: 23px;
	}
	#et-secondary-menu{display: none !important;}
	#et-info{width: 100%; text-align: center !important;}
}

.ma-group-addon{
	padding: 6px 8px !important;
    text-align: center !important;
}
.logo_container{
    width: 250px;
    display: inline-block;
    vertical-align: middle;
}
#et-top-navigation{
    position: absolute;
    right: 0;
    top: 43%;
}
span.logo_helper{
    display: block !important;
}
#top-header, #et-secondary-nav li ul{
    background-color: #5cc75f !important;
}




/** PBE CSS **/
.page-template-page-template-blank.pbe-above-header #pbe-above-header-wa-wrap,
.page-template-page-template-blank.pbe-below-header #pbe-below-header-wa-wrap,
.page-template-page-template-blank.pbe-footer #pbe-footer-wa-wrap {
    display:none !important;
}
#pbe-above-content-wa-wrap .et_pb_widget {
    display: block;
    width: 100%;
    position: relative;
    /*margin-top: -15px;*/
    margin-bottom: 50px;
}

#pbe-above-content-wa-wrap .et_pb_section {
    z-index: 99;
}

#pbe-below-content-wa-wrap .et_pb_widget {
    display: block;
    width: 100%;
    position: relative;
    /*margin-top: -15px;*/
}

#pbe-below-content-wa-wrap .et_pb_section {
    z-index: 99;
}

#main-header .et_pb_widget, #content-area .et_pb_widget {
    width: 100%;
}

#main-header .et_pb_widget p {
    padding-bottom: 0;
}

#pbe-above-header-wa .widget-conditional-inner {
    background: #fff;
    padding: 0;
    border: none;
}

#pbe-above-header-wa select {
    background: #f1f1f1;
    box-shadow: none;
    border-radius: 3px;
    height: 40px;
    padding-left: 10px;
    padding-right: 10px;
    border: none;
}

#pbe-above-header-wa .et_pb_widget
{
    float:none;
}

#pbe-footer-wa-wrap .et_pb_widget {
    width: 100%;
    display: block;
}

.page-container form input[type=text] {
    display: block;
    margin-bottom: 20px;
    width: 100%;
    background: #f1f1f1;
    padding: 10px 20px;
    box-shadow: none;
    border: none;
    font-weight: 700;
}

.page-container form p {
    font-size: 14px;
}

.page-container form {
    padding: 10px 20px;
}

#pbe-footer-wa-wrap {
    position: relative;
    /*top: -15px;*/
}

#pbe-above-header-wa-wrap,
#pbe-below-header-wa-wrap,
#pbe-above-content-wa-wrap,
#pbe-below-content-wa-wrap,
#pbe-footer-wa-wrap {
    position: relative;
    z-index: 9;
}
/* Fixes issues with overlapping widget areas */
.pbe-above-header #main-header .container,
.pbe-below-content #main-content article,
.pbe-footer #main-footer {
    clear: both;
 }
 .pbe-below-content #main-content {
    float: left;
    display: block;
    width: 100%;
}
.pbe-below-content #main-footer {
    float: left;
    width: 100%;
}


/*footer*/
.et_pb_column_1_3 h4, .et_pb_column_1_4 h4, .et_pb_column_1_5 h4, .et_pb_column_1_6 h4, .et_pb_column_2_5 h4{
	font-size: 18px;
}
et_pb_text_inner p{padding-bottom: 1em;}
.et_pb_widget a{
	color: #666;
    text-decoration: none;
}
ul.et_pb_social_media_follow{
	margin: 0 0 22px;
    padding: 0;
    list-style-type: none!important;
	    margin-top: 25px !important;
}
.et_pb_social_media_follow li{
	display: inline-block;
    position: relative;
}
.et_pb_blurb_position_left .et_pb_blurb_content{
	position: relative;
    max-width: 550px;
    margin: 0 auto;
    text-align: left;
}
.et_pb_main_blurb_image{
	    display: inline-block;
    max-width: 100%;
    margin-bottom: 30px;
    line-height: 0;
}
.et_pb_blurb_position_left .et_pb_main_blurb_image, .et_pb_blurb_position_right .et_pb_main_blurb_image{
display: table-cell;
    width: 32px;
    line-height: 0;
}
.et_pb_blurb .et_pb_image_wrap{
	display: block;
    margin: auto;
}
.et_pb_blurb_position_left .et_pb_main_blurb_image img, .et_pb_blurb_position_right .et_pb_main_blurb_image img{
	    width: inherit;
}
.et_pb_animation_off{
	opacity: 1;
}
.et_pb_blurb_position_left .et_pb_blurb_container, .et_pb_blurb_position_right .et_pb_blurb_container{
	    display: table-cell;
    vertical-align: top;
}
.et_pb_blurb_position_left .et_pb_blurb_container{
	    padding-left: 15px;
}
.et_pb_blurb.et_pb_text_align_left .et_pb_blurb_content .et_pb_blurb_container {
    text-align: left;
}
.et_pb_blurb_content p:last-of-type {
    padding-bottom: 0;
}
.et_pb_row_4col .et_pb_column{
    width: 25%;
}
/*#pbe-footer-wa-wrap{
    width: 84% !important;
    margin-left: 16% !important;
}*/
ul.et_pb_social_media_follow{
    margin-top: 25px !important;
}
.et-social-linkedin a.icon:before{
	content: "\e09d";
}
.et_pb_column{
	float: left;
    position: relative;
    z-index: 9;
    background-position: center;
    background-size: cover;
}
.et_pb_module{
	-webkit-animation-duration: .2s;
    -moz-animation-duration: .2s;
    -o-animation-duration: .2s;
    animation-duration: .2s;
	webkit-animation-timing-function: linear;
    -moz-animation-timing-function: linear;
    -o-animation-timing-function: linear;
    animation-timing-function: linear;
}
.et_pb_text_align_left{
	text-align: left;
}
.et_pb_section{
	background-color: #eaeaea!important;
}

	/*.et_pb_section{
	padding: 3% 1% 1% 1%;
}*/
.et_pb_section {
    padding: 0 0 2% 0 !important;
}
.et_pb_row {
    padding:0 !important;
}
.et_pb_module_header{
	font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important;
    font-size: 14px !important;
    color: #5cc75f!important;
	font-weight: 600;
}
.main-footersection .et_pb_main_blurb_image img{
	border-radius: 50%;
}
.et_pb_blurb_11.et_pb_blurb {
    font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif;
    color: #666666!important;
    margin-right: -100px!important;
}
.et_pb_blurb_10.et_pb_blurb {
    font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif;
    color: #666666!important;
    margin-right: -100px!important;
    margin-bottom: 30px!important;
}
.et_pb_blurb_9.et_pb_blurb {
    font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif;
    color: #666666!important;
    margin-right: -100px!important;
    margin-bottom: 30px!important;
}
.et_pb_text_inner  a>span{
	color: #5cc75f !important;
	font-weight: 600 !important;
}
.et_pb_text_inner>h4{
	font-weight: 600 !important;
}
.et_pb_blurb_description>p{
	font-weight: 600;
}
.et_pb_row_16.et_pb_row {
    padding-top: 40px!important;
    padding-bottom: 40px!important;
    margin-bottom: 0px!important;
    padding-top: 40px;
    padding-bottom: 40px;
}
.et_pb_row_17.et_pb_row {
    padding-top: 27px!important;
    padding-right: 0px!important;
    padding-bottom: 0px!important;
    padding-left: 0px!important;
    padding-top: 27px;
    padding-right: 0px;
    padding-bottom: 0;
    padding-left: 0px;
}
@media (max-width: 1135px) and (min-width: 981px){
    #pbe-footer-wa-wrap .main-footersection .et_pb_row {
        width: 95%!important;
        max-width: 95%!important;
    }
}
.sidebar{
	/*position: absolute !important;*/
	 position: fixed !important;
}
section.content{
    position: inherit !important;
    margin-bottom: 0 !important;
}
	ul.nav.navbar-nav.navbar-right
	{
		height:65px;
	}
.navbar-nav.navbar-right>li{
	display: block ;
	height:100% !important;
}
.et_pb_social_media_follow_network_0 a.icon{
	background-color: #3b5998!important;
}
.et_pb_social_media_follow_network_1 a.icon{
	background-color: #007bb6!important;
}
.navbar-nav>li>.dropdown-menu{
	margin-top: 25px !important;
	/*margin-top: 5px !important;*/
	border-bottom: 0;
    border-left: 0;
    border-right: 0;
}
.dropdown-menu:before{
	left: auto !important;
    right: 0 !important;
    color: #5cc75f !important;
	top: -9px !important;
	display: none !important;
}
.dropdown-menu-small{
    padding: 0 !important;
}
.et-search-form, .et_mobile_menu, .footer-widget li:before, .nav li ul, blockquote{
	border-color: #5cc75f !important;
}
.dropdown-menu .container{	max-width: inherit !important; width: 100%; margin: 0 !important;}
.dropdown{position: static !important;}
/*.dropdown-menu{position: absolute !important;}*/
.ma-dropdown-menu {text-align: center; right: 5% !important; width: 90% !important; }
.ma-dropdown li a{font-family: 'Poppins', sans-serif !important; display: block; padding: 10px !important; font-weight: 400 !important;}
.ma-dropdown li a:hover{background: #f7f7f7;}
.ma-dropdown li{    border-color: rgba(0, 0, 0, 0.05);
    border-style: solid;
    border-width: 0 0 1px 0; padding: 0 !important;}
.ma-dropdown{padding: 0 10px !important;}
.ma-dropdown .ma-left, .ma-dropdown .ma-right{display: inline-block; width: 45%;}
.ma-dropdown h3{font-size: 16px; font-weight: 600; border-width: 0 0 3px 0; border-style: solid; border-color: #5cc75f;}






.navbar-default .container{
	width: 92% !important;
	max-width: 100% !important;
}
.et_mobile_menu li a, .nav li li a {
    font-size: 14px;
    -webkit-transition: all .2s ease-in-out;
    -moz-transition: all .2s ease-in-out;
    transition: all .2s ease-in-out;
}
/*Sidebar Scroll*/

.sidebar{
	height: calc(100% - 119px);
    overflow-y: auto;
}

.sidebar::-webkit-scrollbar {width: 6px;}

.sidebar::-webkit-scrollbar-track {
    -webkit-box-shadow: inset 0 0 6px rgba(0,0,0,0.3);
}
.sidebar::-webkit-scrollbar-thumb
{
	outline: 1px solid slategrey;
	background-color: #5cc75f;
	border-radius: 10px;
}
.ma-close, .ma-caret{margin-left: 10px;}
.ma-close{display: none;}
.ma-close:before{font-size: 12px !important;}
.ma-caret{font-size: 17px !important;}
.navbar a.dropdown-toggle:hover .ma-close{display: inline-block; color: #5cc75f !important;}
.navbar a.dropdown-toggle:hover .ma-caret{display: none;}



.inloggen {
    cursor: pointer;
}
#top-header {
    position: relative;
}
.navbar-default {
    margin-top: 0 !important;
}
section.content {
    padding-right: 0;
    padding-left: 0;
}
span.profile-picture {
    top: 20.5px;
    font-size: 27px;
}
section.content > .container-fluid {
    width: 90%;
    max-width: 1200px;
}
.fa, .far, .fas {
    margin-right: 0 !important;
    line-height: unset !important;
}
</style>
<?php
    require_once('modal.php');
?>

<style>
#fc_frame, #fc_frame.fc-widget-normal {
  bottom: 15px !important;
}
.card .header h2 small {
    font-size: 14px;
}
.navbar {
    margin-bottom: 0;
    border-bottom: none;
}

body .page-header .container.content-container {
    width: 100% !important;
    height: calc(100vh - 200px)  !important;
    display: flex !important;
    align-items: center  !important;
}
body .page-header .container.content-container .login-container {
    padding: 0  !important;
    height: auto  !important;
    margin: auto !important;
}
@media screen and (min-width:1070px) {
    .navbar-right .werken-bij .dropdown-menu {
        right: calc(100px + 1.5%) !important;
    }
}
/* @media screen and (max-width:1070px) {
    .dropdown-menu.dropdown-menu-small > .row {
        margin-right: -10%;
        margin-left: -13%;
        width: 120%;
    }
    .dropdown-menu-item-text {
        margin-left:11%;
    }
    .navbar-collapse {
        width: 112%;
        margin-left: -6% !important;
    }
}
@media screen and (max-width:576px) {
    .dropdown-menu.dropdown-menu-small > .row {
        margin-right: -15%;
        margin-left: -15%;
        width: 130%;
    }
    .dropdown-menu-item-text {
        margin-left:15%;
    }
} */
</style>
<!-- Top Bar -->
<nav class="navbar p-l-5 p-r-5" style="display:none;">
    <ul class="nav navbar-nav navbar-left" style="text-align: right !important;">
        <li>
            <div class="navbar-header">
            </div>
        </li>
		<li class="hidden-md-down"><a href="BlankPage" title="Events"><b>Calendar</b></a></li>
        <li class="hidden-md-down"><a href="BlankPage" title="Message"><b>Message</b></a></li>
        <li><a href="BlankPage" title="Contact List"><b>Contacts</b></a></li>
        <li><a href="BlankPage" ><b>Notifications</b></a></li>
		 <li><a href="BlankPage" ><b>Projects</b></a></li>
        <li class="hidden-sm-down">
            <div class="input-group">
                <input type="text" class="form-control" placeholder="Search...">
                <span class="input-group-addon ma-group-addon">
                    <i class="zmdi zmdi-search"></i>
                </span>
            </div>
        </li>
        <li style="float: right;">

            <a href="logout" class="mega-menu" data-close="true"><b>logout</b></a>
        </li>
    </ul>
</nav>



<div id="top-header">
    <div class="container clearfix">
        <div id="et-info">
            <a href="tel:+31851303558" id="et-info-phone">
                <span data-balloon="size: 2x" data-balloon-pos="up" class="db color-inherit link hover-lime" style="max-height: 6px;display: inline-block;width: 9px;">
                    <svg aria-hidden="true" focusable="false" data-prefix="fas" data-icon="phone-alt" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" class="svg-inline--fa fa-phone-alt fa-w-16 fa-2x">
                        <path fill="currentColor" d="M497.39 361.8l-112-48a24 24 0 0 0-28 6.9l-49.6 60.6A370.66 370.66 0 0 1 130.6 204.11l60.6-49.6a23.94 23.94 0 0 0 6.9-28l-48-112A24.16 24.16 0 0 0 122.6.61l-104 24A24 24 0 0 0 0 48c0 256.5 207.9 464 464 464a24 24 0 0 0 23.4-18.6l24-104a24.29 24.29 0 0 0-14.01-27.6z" class="" style="color: white !important;"></path>
                    </svg>
                </span>
                &nbsp;085-13<span>03558</span>
            </a>
            </div>
        <div id="et-secondary-menu">
            <ul id="et-secondary-nav" class="menu">
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-119">
                    <a href="/over-ons/">Over ons</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-2304">
                    <a href="/veelgestelde-vragen/">Veelgestelde vragen</a>
                </li>
                <li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-117">
                    <a href="/contact/">Contact</a>
                </li>
            </ul>
        </div>
    </div>
</div>
<!-- Navigation -->
<nav class="navbar navbar-default">
  <div class="container">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
      <button id="navbar-toggle-temporary" type="button" class="navbar-toggle"><!-- <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false"> -->
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/">
		<img alt="Brand" src="/s3/content/Bijles-e1551529964908.png">
	  </a>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">

      <ul class="nav navbar-nav navbar-right">
        <!-- <li><a href="/">Home</a></li> -->
        <?php if(!isset($_SESSION['TeacherID'])){ ?>
            <li><a href="/app/teachers-profile2">Vind docent</a></li>
        <!-- <?php if(!isset($_SESSION['TeacherID']) && !isset($_SESSION['StudentID'])){ ?>
            <li class="dropdown">
                <a href="#" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Vakken 
               <i class="ma-caret zmdi zmdi-caret-down"></i><i class="zmdi zmdi-close ma-close"></i></a>
            <?php $q = "select crs.coursename, st.typeID, st.typeName from courses crs, schooltype st where  st.typeID = crs.coursetype AND st.typeName != 'Basisschool' GROUP BY crs.coursename ORDER BY crs.coursename" ;
                $r = mysqli_query($con, $q);

                $q1 = "select crs.coursename, st.typeID, st.typeName from courses crs, schooltype st where  st.typeID = crs.coursetype AND st.typeName = 'Basisschool' GROUP BY crs.coursename ORDER BY crs.coursename" ;
                $r1 = mysqli_query($con, $q1);
                ?>
            <ul class="dropdown-menu ma-dropdown-menu">
                <div class="container" style="width: 100% !important">
                    <div class="row">
                        <div style="float:left !important;" class="col-lg-8 col-12 ma-dropdown">
                            <h3 style="margin-bottom: 20px;">MIDDELBARE SCHOOL</h3> 
                            <div class="row">
                                <?php $c=1; while($row = mysqli_fetch_assoc($r)){ ?>
                                
                                <?php if($c==1){ ?>
                                <div class="ma-left col-md-6 col-12">
                                <?php } ?>
                                    <?php if($c < 9){ ?> <li><a href="http://bijlesaanhuis.nl/bijles/<?=$row["coursename"] ?>"><?=$row["coursename"] ?></a></li><?php $c++; continue; } ?>
                                    
                                <?php if($c == 9){ ?>
                                </div>
                                <div class="ma-right col-md-6 col-12">
                                <?php } ?>
            
                                    <?php if($c <= 16){   ?> <li><a href="http://bijlesaanhuis.nl/bijles/<?=$row["coursename"] ?>"><?=$row["coursename"] ?></a></li><?php } ?>
                                <?php if($c==16){ ?>
                                </div>
                                    
                                <?php  } ?>
                                <?php $c++; } ?>
                            </div>
                        </div>

                            <div style="float:left !important;"  class="col-md-4 col-12 ma-dropdown">
                                <h3 style="margin-bottom: 20px;">BASISSCHOOL</h3>
                                <?php while($row1 = mysqli_fetch_assoc($r1)){ ?>
                                    <li><a href="http://bijlesaanhuis.nl/bijles/<?=$row1["coursename"] ?>"><?=$row1["coursename"] ?></a></li>

                                <?php } ?>
                            </div>
                    </div> 
                </div>

            </ul> 
        
            </li>
        <?php } ?> -->
        <?php if(!isset($_SESSION['TeacherID']) && !isset($_SESSION['StudentID'])){ ?>
            <li><a href="/prijzen/">Tarieven</a></li>
            <li class="dropdown werken-bij">
                <a href="#" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Werken bij
                <i class="ma-caret zmdi zmdi-caret-down"></i><i class="zmdi zmdi-close ma-close"></i></a>
                <ul class="dropdown-menu dropdown-menu-small">
                    <div class="row">
                        <div style="float:left !important; padding: 0 14px !important;" class="col-lg-12 col-12 ma-dropdown">
                            <li><a href="/bijles-geven/"><span class="dropdown-menu-item-text">Bijles geven</span></a></li>
                            <li><a href="/vacatures/"><span class="dropdown-menu-item-text">Vacatures</span></a></li>
                        </div>
                    </div>
                </ul>
            </li>
        <?php } ?>
        <?php } ?>
        <?php if(isset($_SESSION['TeacherID']) || isset($_SESSION['StudentID'])){ ?>
            <li><a href="<?php if(isset($_SESSION['TeacherID'])){ ?>/app/teachers-dashboard<?php }else{ ?>/app/students-dashboard<?php } ?>">Kalender</a></li>
        <li><a href="<?php if(isset($_SESSION['TeacherID'])){ ?>/app/teachers-appointment<?php }else{ ?>/app/students-appointment<?php } ?>">Afspraken<?php echo $counti; ?></a></li>
        <li><a href="/app/message">Berichten<?php if($newMessages > 0){ ?><span style="margin: 0 0 0 12px;" class="leftSlot bg-amber"><?php echo $newMessages; ?></span><?php } ?></a></li>
        <?php } ?>
        <?php if(isset($_SESSION['TeacherID'])){ ?>
            <li><a href="/app/student-list">Bijles inboeken</a></li>
        <?php } ?>
		  <?php if(!isset($_SESSION['TeacherID']) && !isset($_SESSION['StudentID'])){ ?>

          <li><a <?php if(!isset($_SESSION['TeacherID']) && !isset($_SESSION['StudentID'])){ ?> class="PopupBtn inloggen" <?php } ?>>Account</a></li>
          <!-- <li><a <?php if(!isset($_SESSION['TeacherID']) && !isset($_SESSION['StudentID'])){ ?> class="PopupBtn inloggen" <?php } ?> <?php if(isset($_SESSION['TeacherID']) || isset($_SESSION['StudentID'])){ ?> href="/app/" class="inloggen" <?php } ?>>Account</a></li> -->

		  <?php }else {?>

        <li class="dropdown initials">
          <a href="#" class="dropdown-toggle disabled profilepic <?php if(isset($_SESSION['StudentLName'])){ ?> circle <?php } ?>" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                <?php
                    if(isset($_SESSION['TeacherID'])){

                        $teacherid = $_SESSION['TeacherID'];
                        $stmt = $con->prepare("select profile.title, profile.shortdescription, profile.description, profile.image, profile.profileID from profile inner join teacher ON (teacher.profileID = profile.profileID) inner join user on (user.userID = teacher.userID) inner join usergroup on (user.usergroupID = usergroup.usergroupID) where user.userID = ? and usergroup.usergroupname = ?");
                        $contacttype = 'Teacher';
                        $stmt->bind_param("is",$teacherid,$contacttype);
                        $stmt->execute();
                        $stmt->bind_result($title_profile, $short,$desc,$profile,$profileID);
                        $stmt->store_result();
                        $stmt->fetch();

                        if(empty($profile)) $profile = "assets/images/profile_av.jpg";

                        ?>
                        <span class="meer-mob">Meer</span>
                        <img class="profile-picture" src="<?php echo str_replace('../','',$profile)?>" alt="User" style="height:56px; border-radius: 100%;top:0;"><i class="ma-caret zmdi zmdi-caret-down"></i><i class="zmdi zmdi-close ma-close"></i>
                        <?php
                    }elseif($_SESSION['StudentID']){
                        echo "<span class='meer-mob'>Meer</span>";
                        echo "<i class='ma-caret zmdi zmdi-caret-down mob-caret'></i><i class='zmdi zmdi-close ma-close mob-caret'></i>";
                        echo "<span class='profile-picture'>" . substr(ucwords($_SESSION['StudentFName']), 0, 1) . substr(ucwords($_SESSION['StudentLName']), 0, 1) . "</span>";
                    }
                ?>
            </a>
                <ul class="dropdown-menu">
				<?php if(isset($_SESSION['TeacherID'])){
                    echo "<li><a href='/app/teacher-detailed-view.php?tid=" . $_SESSION['TeacherID'] . "' >";
                    echo "<i class='zmdi zmdi-account-box'></i>Openbaar profiel";
                    echo "</a></li>";
                    echo "<li><a href='/app/teachers-slotcreation' >";
					echo "<i class='zmdi zmdi-plus-circle-o'></i>Algemene beschikbaarheid";
                    echo "</a></li>";
                    #echo "<li><a href='/docenten-resouces' >";
					#echo "<i class='zmdi zmdi-calendar-note'></i>Bestanden en links";
                    #echo "</a></li>";
                    echo "<li><a href='/app/edit-profile' >";
					echo "<i class='zmdi zmdi-edit'></i>Profiel aanpassen";
                    echo "</a></li>";
                    echo "<li><a href='/app/logout'>";
					echo "<i class='zmdi zmdi-sign-in'></i>Uitloggen";
					echo "</a></li>";
                }elseif($_SESSION['StudentID']){
                    echo "<li><a href='/app/student-edit-profile' >";
					echo "<i class='zmdi zmdi-account-box'></i> Profiel aanpassen";
                    echo "</a></li>";
                    echo "<li><a href='/app/logout'>";
					echo "<i class='zmdi zmdi-sign-in'></i>Uitloggen";
					echo "</a></li>";
                }
                else {
                    // echo "<a href='/app/'>";
                    echo "<a class='PopupBtn'>";
					echo "Login";
					echo "</a>";
				}
					 ?>
				</li>
			</ul>
        </li>
		<!-- <li class="dropdown">
			<a href="#" class="dropdown-toggle disabled" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
				Account<span class="caret ma-caret"></span> <i class="zmdi zmdi-close ma-close"></i> </a>
			<ul class="dropdown-menu">
				<li>
				<?php if($_SESSION['TeacherID'] != null || $_SESSION['StudentFName'] != null ){
					echo "<a href='/app/logout' >";
					echo "Logout";
					echo "</a>";
				}else {
					// echo "<a href='/app/'>";
                    echo "<a class='PopupBtn'>";
					echo "Login";
					echo "</a>";
				}
					 ?>
				</li>
			</ul>
		</li> -->
		<?php } ?>

      </ul>
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
</nav>
<noscript>
    <img height="1" width="1" style="display:none"
        src="https://www.facebook.com/tr?id=478656312853879&ev=PageView&noscript=1"/>
</noscript>
<script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
        n.callMethod.apply(n,arguments):n.queue.push(arguments)};
        if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
        n.queue=[];t=b.createElement(e);t.async=!0;
        t.src=v;s=b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t,s)}(window, document,'script',
        'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '478656312853879');
    fbq('track', 'PageView');


    jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
});
jQuery('#navbar-toggle-temporary').on('click',function() {
	if(jQuery('#bs-example-navbar-collapse-1').hasClass('collapse')) {
		jQuery('#bs-example-navbar-collapse-1').show();
		jQuery('#bs-example-navbar-collapse-1').removeClass('collapse');
		jQuery('#bs-example-navbar-collapse-1').addClass('show');
    } else {
		jQuery('#bs-example-navbar-collapse-1').hide();
		jQuery('#bs-example-navbar-collapse-1').removeClass('show');
		jQuery('#bs-example-navbar-collapse-1').addClass('collapse');
    }
});

/* Setting active navigation item style
    Search words: active current page tab nav nav-item nav item*/
// jQuery(document).ready(function() {
//     if(jQuery(window).width() > 1040) {
//         var addressFilter = 'bijlesaanhuis.nl';
//         /*  cut off base-url and possible file-types*/
//         var currentAddressScope = window.location.href.substring(window.location.href.indexOf(addressFilter) + addressFilter.length).split(/\.[a-z]/)[0];
//         /*  Link of Vind docent page has /app/.. while other links have no /app/ prefix
//             Thus, the other links are found in the if clause*/
//         if(jQuery('.nav>li>a[href$="'+currentAddressScope+'"]').length == 0) {
//             addressFilter += '/app/';
//             currentAddressScope = window.location.href.substring(window.location.href.indexOf(addressFilter) + addressFilter.length).split(/\.[a-z]/)[0];
//         }
//         jQuery('.nav>li>a[href$="'+currentAddressScope+'"]').parents().eq(0).addClass('active-nav-item');
//     }
// });
</script> 
