<!-- Main Content -->

<link href="/app/assets/css/edit-tutor-profile.css" rel="stylesheet" />
<section class="content edit-profile-content">
	<?php
	if (strpos($_SERVER['REQUEST_URI'], 'app/Admin') !== false) {
		echo '<div class="block-header">';
		require_once('includes/adminTopBar.php');
		echo '</div>';
	}
	?>
	<form action="/app/actions/edit_scripts.php" method="post" id="info" class="edit-profile-form" enctype='multipart/form-data'>
		<div class="edit-profile-content__section">
			<div class="edit-profile-header">
				<h2 class="ma-subtitle edit-profile-header__heading">Persoonlijke informatie</h2>
			</div>
			<?php
			$teacherid = !empty($_GET['tid']) ? $_GET['tid'] : (!empty($_SESSION['TeacherID']) ? $_SESSION['TeacherID'] : "");
			$stmt = $con->prepare("SELECT firstname, lastname, dateofbirth, telephone, email, address, postalcode, city, country, latitude, longitude, place_name, address, postalcode, city, country, admin_comments from teacher, contact where contact.userID = ? AND contact.prmary = 1 AND teacher.userID=contact.userID");
			$contacttype = 'Teacher';

			$stmt->bind_param("i", $teacherid);
			$stmt->execute();
			$stmt->bind_result($fname, $lname, $dob, $telephone, $email, $address, $postalcode, $city, $country, $lat, $lng, $place_name, $address, $postalCode, $city, $country, $adminComment);
			$stmt->store_result();
			$stmt->fetch();

			$dob = date("Y-m-d", strtotime($dob));
			?>
			<div class="edit-profile-content__row edit-profile-content__row--2col">
				<div class="edit-profile-content__group">
					<label class="input-label" for="booking-code">Voornaam</label>
					<input type="hidden" class="input-text" placeholder="First Name" name="choice" id="choice" value="TeacherPersonalInfo" autofocus>
					<input type="hidden" class="input-text" placeholder="First Name" name="teacher-id" id="teacher-id" value="<?php echo $teacherid; ?>">
					<input type="text" class="input-text" placeholder="Voornaam" name="first-name" id="first-name" value="<?php echo $fname; ?>">
				</div>
				<div class="edit-profile-content__group">
					<label class="input-label" for="booking-code">Achternaam</label>
					<input type="text" class="input-text" placeholder="Achternaam" name="last-name" id="last-name" value="<?php echo $lname; ?>">
				</div>
			</div>
			<div class="edit-profile-content__row edit-profile-content__row--2col">
				<div class="edit-profile-content__group">
					<label class="input-label" for="booking-code">Geboortedatum</label>
					<input type="date" class="input-text" placeholder="Geboortedatum " name="dob" id="dob" min="1900-01-01" max="2010-01-01" value="<?php echo $dob; ?>">
				</div>
				<div class="edit-profile-content__group">
					<label class="input-label" for="enfants">Telefoonnummer</label>
					<input type="text" class="input-text" placeholder="Telefoonnummer" name="telephone" id="telephone" value="<?php echo $telephone; ?>" pattern=" *\+? *\(? *([0-9] *){3} *\)? *[-\.]? *([0-9] *){4,12}">
					<span class="terrorLabel hide"> </span>
				</div>
			</div>
			<?php
			//			$teacherid = $_SESSION['TeacherID'];
			$stmt = $con->prepare("select username, email from user inner join usergroup on (user.usergroupID = usergroup.usergroupID) where user.userID = ? and usergroup.usergroupname = ?");
			$contacttype = 'Teacher';
			$stmt->bind_param("is", $teacherid, $contacttype);
			$stmt->execute();
			$stmt->bind_result($username, $email);
			$stmt->store_result();
			$stmt->fetch();
			?>
			<div class="edit-profile-content__row edit-profile-content__row--2col">
				<div class="edit-profile-content__group">
					<label class="input-label" for="booking-code">E-mailadres</label>
					<input type="email" class="input-text" placeholder="E-mailadres" name="email" id="email" value="<?php echo $email; ?>">
				</div>
				<div class="edit-profile-content__group">
					<label class="input-label" for="booking-code">Wachtwoord</label>
					<input type="password" class="password input-text" placeholder="Wachtwoord" name="password" id="password" minlength="8">
					<label id='toggleText'>
						<input name="toggle" type="checkbox" id="toggle" value='0' onchange='togglePassword(this);'>
						Laat wachtwoord zien
					</label>
				</div>
			</div>
			<div class="edit-profile-content__row edit-profile-content__row--2col">
				<?php
				$stmt = $con->prepare("SELECT onlineteaching, allownewstudents, travelRange FROM teacher where userID = ?");
				$stmt->bind_param("i", $teacherid);
				$stmt->execute();
				$stmt->bind_result($online_courses, $acc_new_std, $travelRange);
				$stmt->store_result();
				$stmt->fetch();
				//				echo $onl;
				?>
				<div class="edit-profile-content__group">
					<div class="edit-profile-content__row--2col">
						<div class="edit-profile-content__group">
							<label class="input-label input-label__block" for="Online Courses">Beschikbaar voor online
								bijles?
								<i class="fa input-label__i" data-toggle="tooltip" data-placement="right" title="Online bijles kan je op je eigen manier invullen, bijvoorbeeld via Skype.">&#xf05a;</i>
							</label>
							<div id="radioBtn" class="btn-group radio-button-faux">
								<a class="btn btn-success btn-sm radio-button-faux__option <?php if ($online_courses == 1) {
																								echo 'active';
																							} else {
																								echo 'notActive';
																							} ?>" data-toggle="online_courses" data-title="o1">Ja</a>
								<a class="btn btn-success btn-sm radio-button-faux__option <?php if ($online_courses == 0) {
																								echo 'active';
																							} else {
																								echo 'notActive';
																							} ?>" data-toggle="online_courses" data-title="o0">Nee</a>
							</div>
							<input type="hidden" name="online_courses" id="online_courses" value="<?php echo $online_courses; ?>">
						</div>

						<div class="edit-profile-content__group">
							<label for="Accepting New Students" class="input-label input-label__block">Beschikbaar voor
								nieuwe leerlingen?
								<i class="fa input-label__i" data-toggle="tooltip" data-placement="right" title="Door je beschikbaarheid voor nieuwe leerlingen uit te zetten, vinden nieuwe leerlingen jou niet meer op de Vind Docent pagina. We verzoeken je om met je huidige leerlingen altijd persoonlijk afspraken te maken over je beschikbaarheid.">&#xf05a;</i>
							</label>
							<div id="radioBtn" class="btn-group radio-button-faux">
								<a class="btn btn-success btn-sm radio-button-faux__option <?php if ($acc_new_std == 1) {
																								echo 'active';
																							} else {
																								echo 'notActive';
																							} ?>" data-toggle="acc_new_std" data-title="n1">Ja</a>
								<a class="btn btn-success btn-sm radio-button-faux__option <?php if ($acc_new_std == 0) {
																								echo 'active';
																							} else {
																								echo 'notActive';
																							} ?>" data-toggle="acc_new_std" data-title="n0">Nee</a>
							</div>
							<input type="hidden" name="acc_new_std" id="acc_new_std" value="<?php echo $acc_new_std; ?>">
						</div>
					</div>
				</div>
				<div class="edit-profile-content__group">
					<label for="travel_range" class="input-label">Maximale reisafstand</label>
					<input type="number" class="input-text" name="travel_range" id="travel_range" min="3.5" max="20" step="0.1" required title="Vul minimaal 3,5 km in." value="<?php echo $travelRange; ?>" />
				</div>
			</div>
			<div class="edit-profile-content__row">
				<div class="edit-profile-content__group">
					<input type="hidden" class="form-control" placeholder="Username" name="username" id="username" value="<?php echo $username; ?>" readonly>
					<button type="submit" class="btn edit-profile-button" id="update-more-courses" name="update-teacher">OPSLAAN
					</button>
				</div>
			</div>
		</div>

		<?php
		if (isset($_SESSION['AdminUser']) && isset($showAdmin) && $showAdmin) {
		?>
			<div class="edit-profile-content__section">
				<div class="edit-profile-header">
					<h2 class="ma-subtitle edit-profile-header__heading">Admin Comments</h2>
				</div>
				<?php
				$notes = json_decode($adminComment, true);
				if (!empty($adminComment) && empty($notes)) {
					$notes = [[
						"text" => $adminComment,
						"time" => "",
						"author" => "",
					]];
				}
				?>
				<div class="row clearfix">
					<div class="col-lg-12 col-xl-12">
						<div class="card chat-app">
							<div class="chat">
								<div class="chat-history">
									<ul id="adminNoteList">
										<?php
										foreach ($notes as $note) {
											// Skip empty note
											if ($note['time'] == '') continue;
										?>
											<li class="clearfix">
												<div class="message-data text-right">
													<span class="message-data-time"><?php echo $note['time']; ?></span>
													&nbsp;
													<span class="message-data-name"><?php echo $note['author']; ?></span>
													<i class=""></i></div>
												<div class="message other-message float-right"><?php echo $note['text']; ?></div>
											</li>
										<?php
										}
										?>
									</ul>
								</div>
								<div class="chat-message clearfix">
									<div class="input-group p-t-15">
										<input type="text" class="form-control" id="adminNote" autocomplete="off" placeholder="type hier...">
										<span id="btnAdminNote" class="input-group-addon">
											<i class="glyphicon glyphicon-send" id="chatsenderBtn"></i>
										</span>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php
		}
		?>

		<div class="edit-profile-content__section">
			<div class="edit-profile-header">
				<h2 class="ma-subtitle edit-profile-header__heading">Profiel informatie</h2>
			</div>
			<?php
			//			$teacherid = $_SESSION['TeacherID'];
			$stmt = $con->prepare("select p.title, p.shortdescription, p.description, p.image, p.profileID, t.travelRange, t.current_education, a.Motivation from teacher t left join profile p on t.profileID = p.profileID left join applications a on t.userID = a.userID WHERE t.userID=?");
			$stmt->bind_param("i", $teacherid);
			$stmt->execute();
			$stmt->bind_result($title_profile, $short, $desc, $profile, $profileID, $travelRange, $education, $motivation);
			$stmt->store_result();
			$stmt->fetch();
			$imgPreview = empty($profile) ? 'profile_av.jpg' : basename($profile);
			?>
			<div class="edit-profile-content__row">

				<div class="demo-wrap upload-demo">
					<div class="edit-profile-content__image-container">
						<input type="hidden" name="current-image" value="<?php echo basename($profile); ?>" />
						<div class="image">
							<div id="jcrop" class="demo-wrap__jcrop">
								<img id="jcrop-image" class="demo-wrap__img" src="/s3/profileImages/<?= $imgPreview ?>" alt="User">
							</div>
							<input id="png" type="hidden" name="imgb" />
							<div class="upload-demo-wrap">
								<div id="upload-demo"></div>
							</div>
						</div>
						<div id="image-edit-submit-box" class="edit-profile-information-box">
							<div>
								<button type="submit" class="btn edit-profile-button" id="add-level-btn" name="update-teacher">OPSLAAN</button>
							</div>
						</div>
						<div class="image-upload">
							<input class="image-upload__input" type="file" id="file" name="profile-image" value="Profielfoto kiezen" accept="image/*" />
							<label class="btn image-upload__label" for="file" />Profielfoto kiezen</label>
						</div>
						<div class="edit-profile-information-box__additional">
							<p class="edit-profile-additional__p">Voor je leerlingen ben je het gezicht van Bijles
								Aan Huis. Het is belangrijk dat:</p>
							<ul class="edit-profile-additional__list">
								<li class="edit-profile-additional__list-item">Je kijkt in de camera.</li>
								<li class="edit-profile-additional__list-item">Je kijkt met minimaal een beetje
									vrolijkheid.
								</li>
								<li class="edit-profile-additional__list-item">De foto goede belichting heeft.</li>
							</ul>
						</div>
					</div>
					<canvas id="canvas" class="profile-image-preview"></canvas>
					<div class="edit-profile-content__information-container">
						<div class="edit-profile-information-box">
							<h5 class="edit-profile-information-box__title">Profieltitel <span class="edit-profile-information-box__chars">(<span id="title_profile"><?php echo (75 - strlen($title_profile)); ?></span> van 75 tekens):</span>
							</h5>
							<div class="edit-profile-information-box__additional">
								<p class="edit-profile-additional__p">Dit is zichtbaar voor alle leerlingen. Benoem
									je huidige scholing of functie Bijvoorbeeeld: Gediplomeerd psycholoog; Vrije
									Universiteit - Psychologie; Vwo 5 - Economie en Maatschappij.</p>
							</div>
							<textarea class="form-control input-textarea edit-profile-information-box__textarea" placeholder="Profieltitel" maxlength="75" name="title_profile" id="title_profile" onkeyup="countChar(this, 75, 'title_profile')"><?php echo $title_profile; ?></textarea>
						</div>
						<div class="edit-profile-information-box">
							<h5 class="edit-profile-information-box__title">Korte profielbeschrijving <span class="edit-profile-information-box__chars">(<span id="short_profile"><?php echo (180 - strlen($short)); ?></span> van 180 tekens):</span>
							</h5>
							<div class="edit-profile-information-box__additional">
								<p class="edit-profile-additional__p">Dit is zichtbaar voor alle leerlingen voordat
									zij op je profiel klikken. Benoem je educatieve of professionele achtergrond en
									motivatie om bijles te geven. Verkoop jezelf gerust.</p>
							</div>
							<input type="hidden" class="form-control" placeholder="First Name" name="profile-id" id="profile-id" value="<?php echo $profileID; ?>">
							<textarea class="form-control input-textarea edit-profile-information-box__textarea" placeholder="Korte profielbeschrijving" maxlength="180" name="short-description" id="short-description" onkeyup="countChar(this, 180, 'short_profile')"><?php echo $short; ?></textarea>
						</div>
						<div class="edit-profile-information-box">
							<h5 class="edit-profile-information-box__title">Lange profielbeschrijving <span class="edit-profile-information-box__chars">(<span id="long_profile"><?php echo (2000 - strlen($desc)); ?></span> van 2,000 tekens):</span>
							</h5>
							<div class="edit-profile-information-box__additional">
								<p class="edit-profile-additional__p">Deze tekst wordt weergeven in je uitgebreide
									profiel. Zorg dat de leerling alle informatie krijgt die het nodig heeft,
									zoals:</p>
								<ul class="edit-profile-additional__list">
									<li class="edit-profile-additional__list-item">Vertel uitgebreider over je
										studie of opleidingsachtergrond.
									</li>
									<li class="edit-profile-additional__list-item">Hoe geef je bijles?</li>
									<li class="edit-profile-additional__list-item">Waarom kan jij leerlingen goed
										helpen? Heb je een bepaalde intrinsieke motivatie?
									</li>
									<li class="edit-profile-additional__list-item">Benoem het niet als je geen
										ervaring hebt, maar benadruk het als je dat wel hebt.
									</li>
									<li class="edit-profile-additional__list-item">Vertel eventueel aanvullende
										informatie die zinvol kan zijn voor de leerling.
									</li>
									<li class="edit-profile-additional__list-item">Je kan hier ook meer over je
										algemene beschikbaarheid vertellen, omdat je kalender wellicht niet altijd
										representatief is.
									</li>
								</ul>
							</div>
							<textarea class="form-control input-textarea edit-profile-information-box__textarea" placeholder="Lange profielbeschrijving " maxlength="2000" name="full-description" id="full-description" onkeyup="countChar(this, 2000, 'long_profile')"><?php echo $desc; ?></textarea>
						</div>
						<?php
						if (isset($_SESSION['AdminUser'])) {
						?>
							<div class="edit-profile-information-box">
								<h5 class="edit-profile-information-box__title">Education</h5>
								<input type="text" class="form-control" disabled name="education" id="education" value="<?php echo $education; ?>" />
							</div>
							<div class="edit-profile-information-box">
								<h5 class="edit-profile-information-box__title">Motivation</h5>
								<textarea class="form-control input-textarea edit-profile-information-box__textarea" name="motivation" id="motivation" disabled onkeyup="countChar(this, 2000, 'long_profile')"><?php echo $motivation; ?></textarea>
							</div>
						<?php
						}
						?>
						<div class="edit-profile-information-box">
							<button type="submit" class="btn edit-profile-button" id="add-level-btn" name="update-teacher">OPSLAAN
							</button>
						</div>
					</div>
				</div>

			</div>
		</div>

		<div class="edit-profile-content__section--2col">
			<div class="edit-profile-content__section">
				<div class="edit-profile-header">
					<h2 class="ma-subtitle edit-profile-header__heading">Pas je locatie aan</h2>
				</div>
				<input type="hidden" class="form-control input-text" name="lat" id="lat3" value="<?php echo ($lat) ? $lat : 33.719361; ?>">
				<input type="hidden" class="form-control input-text" name="lng" id="lng3" value="<?php echo ($lng) ? $lng : 73.074144; ?>">
				<input type="hidden" class="form-control input-text" name="place_name" id="place_name3" value="<?php echo ($place_name) ? $place_name : ""; ?>">
				<input type="hidden" name="teacher-id" id="teacher-id" value="<?php echo $teacherid; ?>">
				<div class="edit-profile-content__row edit-profile-content__row--2col">
					<div class="edit-profile-content__group">
						<label for="error" id="errorMessage3">Deze combinatie bestaat niet.</label>
						<input class="input-text" type="text" placeholder="Postcode" name="postalCode" id="postcode3" value="<?php echo ($postalcode) ? $postalcode : ""; ?>">
					</div>
					<div class="edit-profile-content__group">
						<?php
						$streetName = substr($address, 0, strrpos($address, " "));
						$huisnumber = substr($address, strrpos($address, " ") + 1, strlen($address));
						?>
						<input class="input-text" type="text" placeholder="Huisnummer" name="huisnummer" id="huisnummer3" value="<?php echo ($huisnumber) ? $huisnumber : ""; ?>">
					</div>
				</div>
				<div class="edit-profile-content__row">
					<div class="edit-profile-content__group">
						<input class="input-text" type="text" placeholder="Straatnaam" name="straatnaam" id="straatnaam3" required class="form-control registraion_form_input" value="<?php echo ($streetName) ? $streetName : ""; ?>">
					</div>
				</div>
				<div class="edit-profile-content__row">
					<div class="edit-profile-content__group">
						<input class="input-text" type="text" placeholder="Woonplaats" name="woonplats" id="woonplaats3" value="<?php echo ($city) ? $city : ""; ?>" required>
					</div>
				</div>
				<div class="edit-profile-content__row">
					<div class="edit-profile-content__group">
						<button type="submit" class="btn edit-profile-button" id="update-more-courses" name="update-locatie">OPSLAAN
						</button>
					</div>
				</div>
			</div>
				<?php
				//$q = "SELECT requested_teacherlevel_rate_ID rtID, teacherlevel_rate_ID tID, tlr.level FROM teacher, teacherlevel_rate tlr WHERE userID=$_SESSION[TeacherID] AND (tlr.ID=teacherlevel_rate_ID || tlr.ID=teacher.requested_teacherlevel_rate_ID) LIMIT 1";
				$q = "SELECT teacherlevel_rate_ID, requested_teacherlevel_rate_ID, (SELECT internal_level FROM teacherlevel_rate stlr WHERE stlr.ID=teacher.requested_teacherlevel_rate_ID)requested_tlevel, (SELECT internal_level FROM teacherlevel_rate sstlr WHERE sstlr.ID=tlr.next_teacherlevelID)ntlevel , tlr.next_teacherlevelID ntlevelID, tlr.internal_level AS `level` FROM teacher, teacherlevel_rate tlr WHERE userID=$teacherid AND tlr.ID=teacherlevel_rate_ID";
				$r = mysqli_query($con, $q);
				$f = mysqli_fetch_array($r);
				if (!isset($f)) {
					$f = [];
				}
				$tLevel = isset($f['level']) ? $f['level'] : null;
				$ntlevelID = isset($f['ntlevelID']) ? $f['ntlevelID'] : null;
				$ntlevel = isset($f['ntlevel']) ? $f['ntlevel'] : null;
				$requested_tlevel = isset($f['requested_tlevel']) ? $f['requested_tlevel'] : null;
				$teacherlevel_rate_ID = isset($f['teacherlevel_rate_ID']) ? $f['teacherlevel_rate_ID'] : null;
				$requested_teacherlevel_rate_ID = isset($f['requested_teacherlevel_rate_ID']) ? $f['requested_teacherlevel_rate_ID'] : null;
				//fetching level name
				$q1 = "SELECT * FROM teacherlevel_rate tlr";
				$r1 = mysqli_query($con, $q1);

				if (isset($f['level'])) {
				?>
				<div class="edit-profile-content__section">
					<div class="edit-profile-header">
						<h2 class="ma-subtitle edit-profile-header__heading">Aanvraag indienen voor hoger
							docenttype</h2>
					</div>
					<div class="edit-profile-content__row">
						<div class="edit-profile-information-box__additional">
							<p class="edit-profile-additional__p">Aan het indienen van een nieuw docenttype zijn enkele
								voorwaarden verbonden. Daarom vragen we je om hier eerst het hoofdstukje ‘type docent’
								te lezen. </p>
						</div>
					</div>
					<div class="edit-profile-content__row edit-profile-content__row--2col">
						<div class="edit-profile-content__group edit-profile-content__group--layered">
							<label class="input-label">Huidig docenttype: <span class="clevel"><?php echo $tLevel; ?></span></label>
						</div>
						<div class="edit-profile-content__group edit-profile-content__group--layered">
							<div id="dlevel_place">
								<?php
								if ($teacherlevel_rate_ID == 4) {
									echo "<p class='edit-profile-button--faux'>Verzoek voor type " . $tLevel . " is ingediend</p>";
								} elseif (!$requested_teacherlevel_rate_ID) { ?>
									<input type="hidden" id="requestedlevel" name="requestedlevel" value="<?php echo $ntlevelID; ?>" />
									<button type="button" class="edit-profile-button--faux" id="sendRequest">Verzoek
										indienen om <?php echo $ntlevel; ?> te worden
									</button>
								<?php
								} else {
									echo "<p class='edit-profile-button--faux'>Verzoek voor type " . $requested_tlevel . " is ingediend</p>";
								}
								?>
							</div>
						</div>
					</div>
				</div>
			<?php } ?>
		</div>

		<div class="edit-profile-content__section">
			<div class="edit-profile-header">
				<h2 class="ma-subtitle edit-profile-header__heading">Vakken waarin je bijles geeft</h2>
			</div>
			<?php
			//			$teacherid = $_SESSION['TeacherID'];
			$stmt = $con->prepare("select applicationID, IDdocument, Educationdocument from applications where userID = ?");
			$contacttype = 'Teacher';
			$stmt->bind_param("i", $teacherid);
			$stmt->execute();
			$stmt->bind_result($appid, $iddoc, $educationdoc);
			$stmt->store_result();
			$stmt->fetch();
			$stmt->close();
			?>
			<div class="edit-profile-content__row">
				<div class="table-responsive">
					<table class="table table-bordered table-striped table-hover dataTable js-basic-example1 dt-responsive" id="view-schoolcourses" cellspacing="0" width="100%">
						<thead>
							<tr class="text-center">
								<th>Schoolvak</th>
								<th>Schooltype</th>
								<th>Niveau</th>
								<th>Leerjaar</th>
								<th>Geaccepteerd?</th>
								<th></th>
							</tr>
						</thead>
						<tbody>
							<?php
							$stmt = $con->prepare("SELECT tc.teacherID, tc.teacher_courseID, courses.courseID, courses.coursename, tc.permission, (SELECT GROUP_CONCAT(typeName) FROM schooltype WHERE FIND_IN_SET(typeID, courses.coursetype))typeName, (SELECT GROUP_CONCAT(schoollevel.levelName) FROM schoollevel WHERE FIND_IN_SET(levelID, tc.schoollevelID))levelName, (SELECT GROUP_CONCAT(schoolyear.yearName) FROM schoolyear WHERE FIND_IN_SET(yearID, tc.schoolyearID))yearName FROM courses, teacher_courses tc WHERE tc.courseID=courses.schoolcourseID AND tc.teacherID=?  AND courses.courselevel=tc.schoollevelID  ORDER BY coursename");
							$stmt->bind_param("i", $teacherid);
							$stmt->execute();
							$stmt->bind_result($assignteacher, $assigncourse, $id, $name, $permission, $type, $level, $year);
							$stmt->store_result();
							$ii = 1;
							while ($stmt->fetch()) {
							?>
								<tr>
									<td><?php echo $name; ?></td>
									<td><?php echo $type; ?></td>
									<td><?php echo $level; ?></td>
									<td><?php echo $year; ?></td>
									<td><?php echo translateToDutch($permission); ?></td>
									<td>
										<a href="javascript:;" class="delete-record-course" data-id="<?php echo $assignteacher . "," . $assigncourse; ?>">Verwijderen</a>
									</td>
								</tr>
							<?php
							}
							?>
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<div class="edit-profile-content__section edit-profile-content__section--dropdowns">
			<div class="edit-profile-header">
				<h2 class="ma-subtitle edit-profile-header__heading">Nieuw vak / leerjaar / niveau aanvragen</h2>
			</div>
			<div class="edit-profile-content__row edit-profile-content__row--dropdowns">
				<div id="newCourse">
					<div id="course-row1">
						<input type="hidden" name="counter" class="counter" value="1" />
						<div class="edit-profile-content__group--aligned">
							<label class="input-label">Schooltype *</label>
							<select class="form-control show-tick dynamic-course school-type" data-size="3" data-dropup-auto="false" name="select-school-type[]" id="school-type">
								<option selected>Schooltype *</option>
								<?php
								$stmt = $con->prepare("SELECT DISTINCT typeID, typeName from schooltype group by typeName");
								$stmt->execute();
								$stmt->bind_result($typeID, $typeName);
								$stmt->store_result();
								while ($stmt->fetch()) {
								?>
									<option value="<?php echo $typeID; ?>"><?php echo $typeName; ?></option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="edit-profile-content__group--aligned">
							<label class="input-label">Schoolvak *</label>
							<select class="form-control show-tick school-course" name="school-course[]" data-dropup-auto="false" id="school-course1">
							</select>
						</div>
						<div class="edit-profile-content__group--aligned">
							<label class="input-label">Niveau *</label>
							<select class="form-control show-tick school-level" id="school-level" data-actions-box="true" data-dropup-auto="false" multiple="multiple" name="school-level[]">
								<option>Select</option>
								<?php
								$stmt = $con->prepare("SELECT * from schoollevel");
								$stmt->execute();
								$stmt->bind_result($levelID, $levelName);
								$stmt->store_result();
								while ($stmt->fetch()) {
								?>
									<option value="<?php echo $levelID; ?>"><?php echo $levelName; ?></option>
								<?php
								}
								?>
							</select>
						</div>
						<div class="edit-profile-content__group--aligned">
							<button type="button" class="btn edit-profile-button" id="add-courses" name="update-teacher">Verzoek indienen
							</button>
						</div>
					</div>
				</div>
			</div>
		</div>
	</form>
</section>

<script src="/app/assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="/app/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

<?php
require_once('footerScriptsAddForms.php');

?>
<script type="text/javascript">
	$(document).ready(function() {

		$('.image-upload').click(function() {
			var imageChange = "Selecteer het juiste gebied en klik op Opslaan";
			$('.image-upload__error-label').remove();
			$(this).after('<label class="changed image-upload__error-label">' + imageChange + '</label>');

		});

		$(document).on('change', '.password', function() {
			if ($(this).val().length < 8) {
				$(this).after('<label class="changed">Het wachtwoord moet minimaal 8 tekens lang zijn</label>');
			} else {
				$(this).after('<label class="changed">Klik op Opslaan om je veranderingen op te slaan</label>');
			}
		});

		// show info message beneath the input element if its value has changed
		$(":input").keyup(function() {
			// avoid redundant info message beneath the input
			$(this).parents().eq(0).find('.changed').remove();
			var imageChange = "Selecteer het juiste gebied en klik op Opslaan";
			var passwordChange = "Het wachtwoord moet minimaal 8 tekens lang zijn";
			var personalChange = "Klik op Opslaan om je veranderingen op te slaan";
			var invalidPhone = "Voer een geldig telefoonnummer in";
			var normalErrorMessage = "Klik op Opslaan om je veranderingen op te slaan";

			if ($(this).attr('name') == 'telephone') {
				var re = /^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}/;

				if (re.test($(this).val()) !== true) {
					$(this).after('<label class="changed">' + invalidPhone + '</label>');
				} else {
					$(this).after('<label class="changed">' + normalErrorMessage + '</label>');
				}
			}

			if ($(this).attr('name') == 'title_profile' && $(this).attr('name') == 'short-description' && $(this).attr('name') == 'full-description' && $(this).attr('name') == 'first-name' && $(this).attr('name') == 'last-name' && $(this).attr('name') == 'telephone' && $(this).attr('name') == 'dob' && $(this).attr('name') == 'email') {
				$(this).after('<label class="changed">' + personalChange + '</label>');
			}

			if ($(this).attr('name') !== 'password' && $(this).attr('name') !== 'telephone') {
				$(this).after('<label class="changed">' + normalErrorMessage + '</label>');
			}
		});

		$('.js-basic-example1').DataTable({
			'iDisplayLength': 100,
			language: {
				url: 'https://cdn.datatables.net/plug-ins/1.10.19/i18n/Dutch.json'
			}
		});

		/* Fetch Pre Information for courses */

		function fetch_preinfo(cat, className, rowID) {

			$.ajax({
				url: '/app/actions/fetch_preinfo.php',
				data: {
					'cat': cat
				},
				type: "POST",
				//dataType: "json",
				success: function(result) {
					//$(this).parent().parent().siblings().find(".school-course").html("");
					var rowselector = "#";
					var selector = ".";
					//var separator ">";
					var classNam = rowselector.concat(rowID);
					var classNam1 = selector.concat(className);
					$(classNam).children().find(classNam1).not(".btn-group", ".bootstrap-select").html("");
					var json_obj = $.parseJSON(result);
					var output = '';
					if (className === "school-type") {
						$(classNam).children().find(classNam1).not(".btn-group", ".bootstrap-select").append('<option>Selecteer</option>');
					}
					for (var i in json_obj) {
						$(classNam).children().find(classNam1).not(".btn-group", ".bootstrap-select").append('<option value="' + String(json_obj[i].ID) + '">' + String(json_obj[i].Name) + '</option>');
					}

					$(classNam).children().find(classNam1).not(".btn-group", ".bootstrap-select").selectpicker('refresh');
				}
			});

		}


		fetch_preinfo("SchoolType", "school-type", "course-row1");

		$("#school-course1").change(function() {
			var cid = $(this).val();
			$.ajax({
				url: '/app/actions/course_filter.php',
				data: {
					'subpage': "teacherCourselevelyear",
					cid: cid
				},
				type: "POST",
				success: function(result) {
					$("#school-level").html(result);
					$("#school-level").selectpicker('refresh');
				}
			});
		});

		$("#school-type").change(function() {
			var cid = $(this).val();
			$.ajax({
				url: '/app/actions/course_filter.php',
				data: {
					'subpage': "teacherCourselevelyear",
					cid: cid
				},
				type: "POST",
				success: function(result) {
					$("#school-level").html(result);
					$("#school-level").selectpicker('refresh');
				}
			});
		});

		$("#sendRequest").click(function() {

			var rlevel = $("#requestedlevel").val();

			$.ajax({
				url: '/app/teacher-ajaxcalls.php',
				data: {
					'subpage': "teacherlevel_requested",
					tid: _tid,
					'levelID': rlevel
				},
				type: "POST",
				success: function(result) {
					console.log('result', result);
					if (result.includes("success")) {
						showNotification("alert-success", "We hebben je verzoek ontvangen", "bottom", "center", "", "");
						$("#dlevel_place").html("Verzoek is ingediend");
					} else {
						showNotification("alert-danger", "Failed to send Request, Try later.", "bottom", "center", "", "");
					}
				}
			});

		});

		var _type = "";
		var _course = "";
		var _level = "";
		var _year = "";
		var _courseID = "";
		var _tid = <?php echo $teacherid; ?>;

		$("#add-courses").click(function(e) {
			e.preventDefault();

			var type = $("#school-type").val();
			var course = $("#school-course1").val();
			var level = $("#school-level").val();

			if (type == <?php echo ELEMENTRY_TYPE_ID; ?>) {
				if (!(type && course)) {
					showNotification("alert-danger", "Vul alle velden in", "bottom", "center", "", "");
					return 1;
				}
				_type = $('#school-type option:selected').text();
				_course = $('#school-course1 option:selected').text();

			} else {
				if (!(type && course && level)) {
					showNotification("alert-danger", "Vul alle velden in", "bottom", "center", "", "");
					return 1;
				}
				_type = $('#school-type option:selected').text();
				_course = $('#school-course1 option:selected').text();
				_level = $("#school-level option:selected").map(function() {
					return $(this).text();
				}).get().join(',');
			}

			$.ajax({
				url: '/app/teacher-ajaxcalls.php',
				data: {
					'subpage': "teachercourse_request",
					tid: _tid,
					type: type,
					course: course,
					level: level
				},
				type: "POST",
				success: function(result) {
					result = (result.trim()).split("*");
					if (result[0] == "success") {
						showNotification("alert-success", "We hebben je verzoek ontvangen", "bottom", "center", "", "");
						_courseID = result[1];


						window.location.reload();
					} else {
						showNotification("alert-danger", "Er is iets fout gegaan, probeer later!", "bottom", "center", "", "");
					}
					_type = "";
					_course = "";
					_level = "";
					_year = "";
					_courseID = "";
				}
			});
		});

		$("#add-level-btn2").click(function(e) {
			e.preventDefault();

			var phone = $("#telephone").val();

			if (!(/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(phone))) {
				$(".terrorLabel").text("Invalid telephone number").removeClass("hide");
				return;
			}

			$("#submit-btn2").click();

		});


		$("#add-level-btn1").click(function(e) {
			e.preventDefault();

			var pass = $("#password").val();
			var cpass = $("#cpassword").val();

			if (pass.length < 8) {
				$(".errorLabel").text("Password must be minimum 8 characters").removeClass("hide");
				return;
			}

			if (pass != cpass) {
				$(".errorLabel").text("Password does not matched").removeClass("hide");
				return;
			}

			$("#submit-btn").click();

		});

		$(document).on("click", "a.delete-record-course", function(e) {
			var id = $(this).attr("data-id");

			$('#view-schoolcourses').waitMe('hide');
			$("#delete-id-course").val(id);
			$("#modal-delete").modal('show');
			$('#modal-delete-course').appendTo("body").modal('show');
		});

		$("#delete-yes-course").click(function() {
			var id = $("#delete-id-course").val();
			$.ajax({
				url: '/app/actions/delete_scripts.php',
				type: 'POST',
				data: {
					id: id,
					choice: "TeacherAssignedCourse"
				},
				success: function(result) {
					switch (result) {
						case "Error":
							$('#deresultcourse').appendTo("body");
							$("#deresultcourse").html(
								'<div id="error-delete" class="alert alert-danger alert-dismissable">' +
								'<button type="button" class="close" ' +
								'data-dismiss="alert" aria-hidden="true">' +
								'&times;' +
								'</button>' +
								'Er was een error met je aanvraag.' +
								'</div>');
							setTimeout(function() {
								$("#deresultcourse").html("");
								$("#modal-delete-course").modal('hide');
							}, 1000);
							break;
						case "Success":
							$('#deresultcourse').appendTo("body");
							setTimeout(function() {
								$("#modal-delete-course").modal('hide');
								window.location.reload();
							}, 1000);
							break;
					}
				}
			});
		});

		$("#delete-no-course").click(function() {
			$("#modal-delete-course").modal('hide');

		});

		$(document).on("change", ".dynamic-course", function(e) {
			e.stopImmediatePropagation();

			var stype = $("#school-type").val();
			var slevel = $("#school-level").val();
			slevel = (typeof(slevel) == "object") ? slevel.join(",") : slevel;

			var syear = $("#school-year").val();
			syear = (typeof(syear) == "object") ? syear.join(",") : syear;

			if (stype == <?php echo ELEMENTRY_TYPE_ID; ?>) {
				$("#school-level").parent().parent().css("display", "none");
				$("#school-level").prop("disabled", true);
				$("#school-year").parent().parent().css("display", "none");
				$("#school-year").prop("disabled", true);
				slevel = "";
				syear = "";
			} else {
				$("#school-level").parent().parent().css("display", "inline-block");
				$("#school-level").prop("disabled", false);
				$("#school-year").parent().parent().css("display", "inline-block");
				$("#school-year").prop("disabled", false);
			}

			$.ajax({
				url: '/app/actions/fetch_courses2.php',
				data: {
					'typeID': stype,
					'levelID': slevel,
					'yearID': syear
				},
				type: "POST",
				//dataType: "json",
				success: function(result) {
					$("#school-course1").html(result);
					$("#school-course1").selectpicker('refresh');
				}
			});

		}); //show-tick ends


		/* Courses AJAX Calls */
		$("#add-more-courses").click(function() {
			var count = $(".body").children().find(".row").length - 1;
			var te = count;
			$("#newCourse").append('<div class="row clearfix" id="course-row' + te + '" ><input type="hidden" name="counter" class="counter" value="' + te + '" /><div class="col-lg-3 col-md-6"><p> <b>Select School Type *</b> </p><select class="form-control show-tick school-type" name="select-school-type[]" required></select></div><div class="col-lg-3 col-md-6"><p> <b>Select School Level *</b> </p><select class="form-control show-tick school-level" required name="school-level[]"></select></div><div class="col-lg-3 col-md-6"><p> <b>Select School Year *</b> </p><select class="form-control show-tick school-year" required name="school-year[]"></select></div><div class="col-lg-3 col-md-6"><p> <b>Select School Course *</b> </p><select class="form-control show-tick school-course" required name="school-course[]" id="school-course' + te + '"></select></div></div>');
			fetch_preinfo("SchoolType", "school-type", "course-row" + te);
			fetch_preinfo("SchoolLevel", "school-level", "course-row" + te);
			fetch_preinfo("SchoolYear", "school-year", "course-row" + te);
			$(".school-course").selectpicker('refresh');
		});

	});


	function countChar(val, totalCharater, elem) {
		var len = val.value.length;
		if (len >= totalCharater) {
			val.value = val.value.substring(0, totalCharater);
		} else {
			$('#' + elem).text(totalCharater - len);
		}
	}
</script>
<div class="modal modal-dialog-centered fade" id="modal-delete-type" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal__content">
		<div class="modal__header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h6 class="modal__title" id="myModalLabel">Weet je zeker dat je het verzoek wilt annuleren?</h6>
		</div>
		<div class="modal__body">
			<div id="deresulttype"></div>
			<form id="delete-form-type" class="form-horizontal form-label-left" method="post" action="">
				<input type="hidden" id="delete-id-type" name="delete-id-type" />
				<input type="hidden" id="delete-choice" name="delete-choice" value="SchoolType" />
				<button type="button" class="btn btn-success" id="delete-yes-type">Ja</button>
				<button type="button" class="btn btn-danger" id="delete-no-type">Nee</button>
			</form>
		</div>
	</div>
</div>
<div class="modal modal-dialog-centered fade" id="modal-delete-course" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal__content">
		<div class="modal__header">
			<button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
			<h6 class="modal__title" id="myModalLabel">Weet je zeker dat je dit vak wilt verwijderen?</h6>
		</div>
		<div class="modal__body">
			<div id="deresultcourse"></div>
			<form id="delete-form-course" class="form-horizontal form-label-left" method="post" action="">
				<input type="hidden" id="delete-id-course" name="delete-id-course" />
				<input type="hidden" id="delete-choice" name="delete-choice" value="TeacherAssignedCourse" />
				<button type="button" class="btn btn-success" id="delete-yes-course">Ja</button>
				<button type="button" class="btn btn-danger" id="delete-no-course">Nee</button>
			</form>
		</div>
	</div>
</div>

<script>
	function addAdminNote(msg) {
		if (msg !== "") {
			$.ajax({
				type: "POST",
				url: "/app/actions/edit_scripts.php",
				data: {
					"tid": "<?php echo $teacherid; ?>",
					"action": "add_note",
					"value": msg,
				},
				success: function(response) {
					if (response.success) {
						let n = response.data[response.data.length - 1];
						let e = '<li class="clearfix"><div class="message-data text-right"><span class="message-data-time">' + n.time + '</span>&nbsp;<span class="message-data-name">' + n.author + '</span> <i class=""></i></div><div class="message other-message float-right">' + n.text + '</div></li>';
						$("#adminNoteList").append(e);
						$("#adminNote").val("");
					}
				},
				error: function(error) {
					console.log("error!s");
					console.log(error);
				}
			});
		}
	}

	$(document).ready(function() {
		$("#btnAdminNote").on('click', function() {
			addAdminNote($('#adminNote').val());
		});
		$("#adminNote").on("keypress", function(event) {
			if (event.key === 'Enter') {
				event.preventDefault();
				addAdminNote($('#adminNote').val());
			}
		});

		var place_name = $('#place_name3').val();
		var lat = $('#lat3').val();
		var long = $('#lng3').val();

		$("#huisnummer3, #postcode3").on("change paste", function() {
			var houseNumber = $('#huisnummer3').val();

			var foo = $('#postcode3').val().split(" ").join("");
			if (foo.length > 0) {
				foo = foo.match(new RegExp('.{1,4}', 'g')).join(" ");
			}
			$('#postcode3').val(foo);

			if ($('#postcode3').val().length === 0) {
				$('#postcode3').css('text-transform', 'capitalize');
			} else {
				$('#postcode3').css('text-transform', 'uppercase');
			}

			if (houseNumber.length > 0 && houseNumber.length <= 4) {
				var postalCode = $('#postcode3').val().replace(" ", "");

				if (postalCode.length >= 4 && postalCode.length <= 6) {
					$.ajax({
						type: "GET",
						url: "/api/Geo/Address?postalCode=" + postalCode + "&streetNumber=" + houseNumber,
						success: function(response) {
							$('#errorMessage3').css("display", "none");

							let street = response.streetName;
							let place = response.locality;

							$('#straatnaam3').val(street);
							$('#woonplaats3').val(place);

							let lat = response.latitude;
							let long = response.longitude;
							isAddressChanged = true;

							$('#lat3').val(lat);
							$('#lng3').val(long);

							let placeName = street + " " + response.streetNumber + ", " + response.postalCode + " " + place + " Nederland";
							$('#place_name3').val(placeName);

							$(function() {
								$("button[name='update-locatie']").attr("disabled", false);
							});
						},
						error: function(error) {
							console.log("error");
							console.log(error);
							$('#errorMessage3').css("display", "block");
							$(function() {
								$("button[name='update-locatie']").attr("disabled", true);
							});

							$('#straatnaam3').val("");
							$('#woonplaats3').val("");

							$('#lat3').val("");
							$('#lng3').val("");
							$('#place_name3').val("");
						}
					});
				}
			}
		});


		$("#showHide").on('change', function() {
			if ($(this).is(":checked")) {
				$(".password").attr("type", "text");
			} else {
				$(".password").attr("type", "password");
			}
		});
		var sel = "";
		var tog = "";
		$('#radioBtn a').on('click', function() {
			sel = $(this).data('title');
			tog = $(this).data('toggle');

			if (sel == 'o0') {
				$('#' + tog).prop('value', sel);
				$('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
				$('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
			} else if (sel == 'o1') {
				$('#online_courses').val(1);
				$('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
				$('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
			}

			if (sel == 'n0') {
				$('#' + tog).prop('value', sel);
				$('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
				$('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
			} else if (sel == 'n1') {
				$('#acc_new_std').val(1);
				$('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
				$('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');
			}
		});

		$("#delete-yes-type").click(function() {
			$('#' + tog).prop('value', sel);

			$('a[data-toggle="' + tog + '"]').not('[data-title="' + sel + '"]').removeClass('active').addClass('notActive');
			$('a[data-toggle="' + tog + '"][data-title="' + sel + '"]').removeClass('notActive').addClass('active');

			$("#modal-delete-type").modal('hide');
		});

		$("#delete-no-type").click(function() {
			$("#modal-delete-type").modal('hide');
		});
	});
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-jcrop/2.0.4/js/Jcrop.js"></script>
<script>
	/***************************image crop****************************/
	var picture_width;
	var picture_height;
	var crop_max_width = 400;
	var crop_max_height = 280;


	$(document).ready(function() {

		$("#file").change(function() {
			picture(this);
		});

		$(".submitBtn").click(function(e) {
			e.preventDefault();
			var base64 = $("#png").val();
			$("#info").submit();
		});


		function picture(input) {
			if (input.files && input.files[0]) {
				var reader = new FileReader();
				reader.onload = function(e) {
					$("#jcrop").html("").append("<img  src=\"" + e.target.result + "\" alt=\"\" />");
					picture_width = $("#jcrop img").width();
					picture_height = $("#jcrop img").height();

					// Set JCrop size smaller than borders
					if (crop_max_width > $('.demo-wrap.upload-demo').width()) {
						crop_max_width = $('.demo-wrap.upload-demo').width();
					}
					if (crop_max_height > $(window).height()) {
						crop_max_height = $(window).height() * 0.9;
					}

					/**************center selection****************/
					var x = Math.abs(((picture_width / 2) - (700 / 2)));
					var y = Math.abs(((picture_height / 2) - (700 / 2)));
					var x1 = x + 700;
					var y1 = y + 700;
					/**************center selection****************/
					$("#jcrop  img").Jcrop({
						aspectRatio: 1,
						onChange: canvas,
						onSelect: canvas,
						setSelect: [0, 0, 400, 400],
						boxWidth: crop_max_width,
						boxHeight: crop_max_height
					});

					showImageSubmitButton();
				}
				reader.readAsDataURL(input.files[0]);
			}
		}

		function addMarginTopToProfileInformation() {
			if ($(window).width() > 720) {
				// Profile information gets margin-top equal to JCrop's height + 20px
				$('.edit-profile-information-box .edit-profile-information-box__title')
					.first()
					.css('margin-top', ($('.jcrop-active').height() + 20) + 'px');
			} else {
				/* If screen format changes and new upload is done,
				   the profile information does not need a margin-top */
				$('.edit-profile-information-box .edit-profile-information-box__title')
					.first()
					.css('margin-top', '0');
			}
			// JCrop is centered in the screen
			$('.jcrop-active').css('margin-left', Math.floor(($('.demo-wrap.upload-demo').width() / 2 - ($('.jcrop-active').width() / 2))) + 'px');
			$('.image').css('padding', '0px');
		}

		function showImageSubmitButton() {
			// Only if the button display is none, show the button
			if ($('#image-edit-submit-box:visible').length == 0) {
				$('#image-edit-submit-box').show();
			}
		}

		function canvas(_coords) {
			var imageObj = $("#jcrop img")[0];
			var canvas = $("#canvas")[0];
			canvas.width = _coords.w;
			canvas.height = _coords.h;
			var context = canvas.getContext("2d");
			context.drawImage(imageObj, _coords.x, _coords.y, _coords.w, _coords.h, 0, 0, canvas.width, canvas.height);
			png();
			addMarginTopToProfileInformation();
		}

		function png() {
			var png = $("#canvas")[0].toDataURL('image/jpeg', 0.50);
			$("#png").val(png);
		}

		function dataURLtoBlob(dataURL) {
			var BASE64_MARKER = ';base64,';
			if (dataURL.indexOf(BASE64_MARKER) == -1) {
				var parts = dataURL.split(',');
				var contentType = parts[0].split(':')[1];
				var raw = decodeURIComponent(parts[1]);

				return new Blob([raw], {
					type: contentType
				});
			}
			var parts = dataURL.split(BASE64_MARKER);
			var contentType = parts[0].split(':')[1];
			var raw = window.atob(parts[1]);
			var rawLength = raw.length;
			var uInt8Array = new Uint8Array(rawLength);
			for (var i = 0; i < rawLength; ++i) {
				uInt8Array[i] = raw.charCodeAt(i);
			}

			return new Blob([uInt8Array], {
				type: contentType
			});
		}

	});
</script>

<?php
function getDutchMonth($date)
{
	if (empty($date)) return "";

	$x = explode(",", $date);
	$m = explode(" ", trim($x[1]));

	$months = array(
		"January" => "Januari",
		"February" => "Februari",
		"March" => "Maart",
		"April" => "April",
		"May" => "Mei",
		"June" => "Juni",
		"July" => "Juli",
		"August" => "Augustus",
		"September" => "September",
		"October" => "Oktober",
		"November" => "November",
		"December" => "December"
	);

	return $x[0] . ", " . $m[0] . " " . $months[$m[1]] . " " . $m[2];
}

?>