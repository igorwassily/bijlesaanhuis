<style>
/* footer responsive css */
	.et_pb_text_inner a{
		font-size: 16px !important;
	}
	.et_pb_section{
		padding: 0 0 2% 0 !important;
	}
	.et_pb_row{
		width: 80% !important;
		margin: 0 auto; 
		max-width: 3000px !important;
	}
	.copy-right span{
		font-size: 16px !important;
	}
	.social-mob{
		display: none !important;
	}
	.et_pb_row_17{
		padding-top: 65px !important;
	}
	@media (max-width: 767px){
		.et_pb_section{
			padding-top: 30px !important;
		}
		.et_pb_text_inner a span{
			font-size: 12px !important;
		}
		.copyright_column{
			width: 100% !important;
		}
		.copy-right span{
			font-size: 12px !important;
		}
		.et_pb_blurb_9.et_pb_blurb{
			margin-right: 0px !important;
		}
		.et_pb_blurb_10.et_pb_blurb{
			margin-right: 0px !important;
		}
		.et_pb_blurb_11.et_pb_blurb{
			margin-right: 0px !important;
		}
		.et_pb_module_header{
			font-size: 13px !important;
		}
		.et_pb_blurb_description{
			font-size: 12px !important;
		}
		.social-mob{
			display: block !important;
			text-align: center !important;			
		}
		.social-desk{
			display: none !important;
		}
		.et_pb_row_17{
			padding-top: 30px !important;
			width: 100% !important;
		}
	}

	@media (max-width: 485px){
		.et_pb_column{
			width: 50% !important;
		}
	}

	@media (max-width: 980px){
		.et_pb_column .et_pb_module { 
			margin-bottom: 5px !important;
		}
		.et_pb_column .footer-person{
			margin-bottom: 30px !important;
		}
	}
</style>

<div id="footer">
    <div class="footer__container">
        <ul class="footer__list">
            <li>
                <h4 class="footer__list--title">Info</h4>
            </li>
            <li><a href="/docenten">Vind docent</a></li>
            <li><a href="/over-ons">Wie zijn wij?</a></li>
            <li><a href="/voorwaarden">Voorwaarden</a></li>
            <li><a href="/bijles-geven">Bijles geven</a></li>
            <li><a href="/contact">Contact</a></li>
            <li><a href="/blog">Blog</a></li>
        </ul>
        <ul class="footer__list">
            <li>
                <h4 class="footer__list--title">Locaties</h4>
            </li>
            <li><a href="/bijles/utrecht">Bijles Utrecht</a></li>
            <li><a href="/bijles/rotterdam">Bijles Rotterdam</a></li>
            <li><a href="/bijles/amsterdam">Bijles Amsterdam</a></li>
            <li><a href="/bijles/den-haag">Bijles Den Haag&nbsp;</a></li>
            <li><a href="/bijles/leiden">Bijles Leiden</a></li>
            <li><a href="/alle-locaties">Alle locaties</a></li>

        </ul>
        <ul class="footer__list">
            <li>
                <h4 class="footer__list--title">Vakken</h4>
            </li>
            <li><a href="/bijles/wiskunde">Bijles wiskunde</a></li>
            <li><a href="/bijles/economie">Bijles economie</a></li>
            <li><a href="/bijles/scheikunde">Bijles scheikunde</a></li>
            <li><a href="/bijles/natuurkunde">Bijles natuurkunde</a></li>
            <li><a href="/bijles/biologie">Bijles biologie</a></li>
            <li><a href="/alle-vakken">Alle vakken</a></li>
        </ul>
        <ul class="footer__list d-sm-none d-md-inline-block footer__list__team--top">
            <li>
                <h4 class="footer__list--title">Ons team</h4>
            </li>
            <li class="footer__list__people-item">
                <div class="footer__list__people-img">
                    <img style="width: 50px !important; height: 50px !important;"
                        src="/s3/content/Florian-Zandbergen.gif" alt="">
                </div>
                <div class="footer__list__people-details">
                    <h5 class="footer__list__people-name">Florian</h5>
                    <div class="footer__list__people-description">
                        Oprichter
                    </div>
                </div>
            </li>
            <li class="footer__list__people-item">
                <div class="footer__list__people-img">
                    <img style="width: 50px !important; height: 50px !important;"
                        src="/s3/content/Jamie_Schuller_Bijles_Aan_Huis.png"
                        alt="">
                </div>
                <div class="footer__list__people-details">
                    <h5 class="footer__list__people-name">Jamie</h5>
                    <div class="footer__list__people-description">
                        Oprichter
                    </div>
                </div>
            </li>
            <li class="footer__list__people-item">
                <div class="footer__list__people-img">
                    <img style="width: 50px !important;min-width: 50px !important; height: 50px !important;"
                        src="/s3/content/Silvana.png" alt="">
                </div>
                <div class="footer__list__people-details">
                    <h5 class="footer__list__people-name">Silvana</h5>
                    <div class="footer__list__people-description">
                        Marketing Specialist
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="footer__container d-none d-sm-block d-md-none footer__list__team--bottom">
        <h4 class="footer__list--title">Ons team</h4>
    </div>
    <div class="footer__container d-none d-sm-flex d-md-none footer__list__team--bottom">
        <li class="footer__list__people-item">
            <div class="footer__list__people-img">
                <img style="width: 50px !important; height: 50px !important;"
                    src="/s3/content/Florian-Zandbergen.gif" alt="">
            </div>
            <div class="footer__list__people-details">
                <h5 class="footer__list__people-name">Florian</h5>
                <div class="footer__list__people-description">
                    Oprichter
                </div>
            </div>
        </li>
        <li class="footer__list__people-item">
            <div class="footer__list__people-img">
                <img style="width: 50px !important; height: 50px !important;"
                    src="/s3/content/Jamie_Schuller_Bijles_Aan_Huis.png"
                    alt="">
            </div>
            <div class="footer__list__people-details">
                <h5 class="footer__list__people-name">Jamie</h5>
                <div class="footer__list__people-description">
                    Oprichter
                </div>
            </div>
        </li>
        <li class="footer__list__people-item">
            <div class="footer__list__people-img">
                <img style="width: 50px !important; min-width: 50px !important; height: 50px !important;"
                    src="/s3/content/Silvana.png" alt="">
            </div>
            <div class="footer__list__people-details">
                <h5 class="footer__list__people-name">Silvana</h5>
                <div class="footer__list__people-description">
                    Marketing Specialist
                </div>
            </div>
        </li>
    </div>
    
    <div class="footer__container footer__social-media">
        <div>
            <a href="https://www.facebook.com/bijlesaanhuisnl/" title="Follow on Facebook" target="_blank"><svg class="svg-inline--fa fa-facebook-square fa-w-14" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="facebook-square" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M400 32H48A48 48 0 0 0 0 80v352a48 48 0 0 0 48 48h137.25V327.69h-63V256h63v-54.64c0-62.15 37-96.48 93.67-96.48 27.14 0 55.52 4.84 55.52 4.84v61h-31.27c-30.81 0-40.42 19.12-40.42 38.73V256h68.78l-11 71.69h-57.78V480H400a48 48 0 0 0 48-48V80a48 48 0 0 0-48-48z"></path></svg></a>
            <a href="https://www.linkedin.com/company/bijlesaanhuis/" title="Follow on LinkedIn" target="_blank"><svg class="svg-inline--fa fa-linkedin fa-w-14" aria-hidden="true" focusable="false" data-prefix="fab" data-icon="linkedin" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 448 512" data-fa-i2svg=""><path fill="currentColor" d="M416 32H31.9C14.3 32 0 46.5 0 64.3v383.4C0 465.5 14.3 480 31.9 480H416c17.6 0 32-14.5 32-32.3V64.3c0-17.8-14.4-32.3-32-32.3zM135.4 416H69V202.2h66.5V416zm-33.2-243c-21.3 0-38.5-17.3-38.5-38.5S80.9 96 102.2 96c21.2 0 38.5 17.3 38.5 38.5 0 21.3-17.2 38.5-38.5 38.5zm282.1 243h-66.4V312c0-24.8-.5-56.7-34.5-56.7-34.6 0-39.9 27-39.9 54.9V416h-66.4V202.2h63.7v29.2h.9c8.9-16.8 30.6-34.5 62.9-34.5 67.2 0 79.7 44.3 79.7 101.9V416z"></path></svg></a>
        </div>
    </div>
    <div class="footer__copyright">
        <p>&nbsp; Copyright © Bijles Aan Huis BV. Alle rechten voorbehouden.</p>
    </div>
</div>
<link href="/app/assets/css/footer.css" rel="stylesheet" />

<!-- Jquery Core Js --> 
<script src="/app/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/app/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>
<script src="/app/assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js -->

<?php
if(!isset($_SESSION['TeacherID']) && !isset($_SESSION['StudentID'])){
?>
<!--<script src="/app/assets/js/modal.js"></script>-->
<?php }  ?>
<script src="/app/assets/plugins/jquery-steps/jquery.steps.js"></script> <!-- JQuery Steps Plugin Js -->

	
<script src="/app/assets/plugins/momentjs/moment.js"></script>
<script src="/app/assets/plugins/bootstrap-select/js/bootstrap-select.js"></script>
<script src="/app/assets/js/pages/forms/basic-form-elements.js"></script> 
<script src="/app/assets/js/pages/forms/advanced-form-elements.js"></script> 
<script src="/app/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- Jquery DataTable Plugin Js --> 
<script src="/app/assets/bundles/datatablescripts.bundle.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="/app/assets/js/pages/tables/jquery-datatable.js"></script>
<script src = "https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="/app/assets/plugins/waitme/waitMe.js"></script>
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="/app/assets/plugins/hoverIntent/jquery-hoverIntent.js"></script>


<!-- datepicker test -->
<script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/nl.js"></script>
<script src="/app/assets/vendortest/datepickertest.js"></script>

<script src="https://wchat.freshchat.com/js/widget.js"></script>

<!-- Start of  Zendesk Widget script -->
<script id="ze-snippet" src="https://static.zdassets.com/ekr/snippet.js?key=ef230f4b-cb81-44e0-a8cf-52e39b9f054e"> </script>
<!-- End of  Zendesk Widget script -->

<script>
  function initFreshChat() {
    window.fcWidget.init({
      token: "7fcc8618-4a87-47e7-a3e6-eb3d092e4cea",
      host: "https://wchat.freshchat.com"
    });
  }
  function initialize(i,t){var e;i.getElementById(t)?initFreshChat():((e=i.createElement("script")).id=t,e.async=!0,e.src="https://wchat.freshchat.com/js/widget.js",e.onload=initFreshChat,i.head.appendChild(e))}function initiateCall(){initialize(document,"freshchat-js-sdk")}window.addEventListener?window.addEventListener("load",initiateCall,!1):window.attachEvent("load",initiateCall,!1);
</script>

<script>
	var Global_Variable_timeRange = null;
	
	function __onClick(obj, date){
		console.log(Global_Variable_timeRange[date]);
	}

	function togglePassword(obj){
		 var checkBox = document.getElementById("toggle");
  
		  // If the checkbox is checked, display the output text
		  if (checkBox.checked == true){
			$("#password").attr("type", "text");
		  } else {
			$("#password").attr("type", "password");
		  }
	}
	function run_waitMe(el, num, effect){}

	function notifi(){
	    /*
        $.ajax({
            type: "POST", 
            url: "/app/includes/chat-ajax.php",
            data: { filename: "notifi"},
            dataType: "text",
            success: function(response) { 
                if(response == "Unread Messsages"){
                        $("#emailNotify").removeClass("hide");
                }else{
                        $("#emailNotify").addClass("hide");
                    }
            },
            error: function(xhr, ajaxOptions, thrownError) { 
                if(xhr.responseText == "Unread Messsages"){
                        $("#emailNotify").removeClass("hide");
                }else{
                        $("#emailNotify").addClass("hide");
                    }
            }
        });// ajax ends

	     */
    }//func ends

	  $(document).ready(function(){

      setInterval(notifi, 3000);
      $($("li.active.open").closest("ul.ml-menu").get()).css("display","block");
 
    });//document ends 
    
</script>
<script>
function isIE() {
  ua = navigator.userAgent;
  /* MSIE used to detect old browsers and Trident used to newer ones*/
  var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1; 
  
  return is_ie; 
}
function isEdge(){
	ua = navigator.userAgent;
	 var is_ed = ua.indexOf("Edge") > -1;
	 return is_ed;
}
/* for IE and Edge */
if (isIE()){
    alert('Application is not compatible with this browser.'); 
	window.location.replace("https://bijlesaanhuis.nl");
}else if(isEdge()){
	alert('Application is not compatible with this browser.');
	window.location.replace("https://bijlesaanhuis.nl");
}
</script> 
<script>
jQuery('ul.nav li.dropdown').hover(function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
}, function() {
  jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
});
</script>

<script src="/app/assets/js/pages/ui/notifications.js"></script>

<!-- <script src="/app/assets/plugins/fontawesome/js/all.min.js"></script> -->
