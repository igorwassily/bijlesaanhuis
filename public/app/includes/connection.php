<?php

$cwd = getcwd();
chdir(__DIR__);
require_once '../../../vendor/autoload.php';
require_once '../../../inc/Common.php';
chdir($cwd);

$servername = getenv('RDS_HOSTNAME');
$username = getenv('RDS_USERNAME');
$password = getenv('RDS_PASSWORD');
$dbname = getenv('RDS_DB_NAME');

// Create connection
$con = new mysqli($servername, $username, $password, $dbname);
$con->set_charset("utf8mb4");
// Check connection
if ($con->connect_error) {
	die("Connection failed: " . $con->connect_error);
}
$db = Common::GetDb();

//$con->set_charset("utf8");

//for ajax server processing
$sql_details = array(
	'user' => $username,
	'pass' => $password,
	'db' => $dbname,
	'host' => $servername
);


define("ADMIN_ID", 1);

define("ELEMENTRY_TYPE_ID", 9);


define("DEFAULT_PIC", "assets/images/profile_av.jpg");
//define("TUTOR_KILOMETER_LIMIT",15); // in kilometers not used for now


define("TUTOR_NO_TRAVEL_COST_KILOMETER_LIMIT", 4); // in kilometers
define("TUTOR_MAX_TRAVEL_KILOMETER_LIMIT", 12); // in kilometers


define("UNIT_TIME_FOR_COST_CALCULATION", 45); //minutes
define("SLOT_TIME_IN_MINIUTES", 15);
define("SLOT_TIME_GAP_IN_MINIUTES", 0);
define("MINIMUM_TIME_FOR_APPOINTMENT_SLOT", 45);
define("DEFAULT_TIME_FOR_APPOINTMENT_SLOT", 90);


define("MAXIMUM_TIME_SLOT_ALLOWED_TO_STUDENT", 100 * 60); // 100hours into mins
define("SITE_URL", "https://bijlesaanhuis.nl/app/");
define("NO_REPLY_EMAIL", "noreply@bijlesaanhuis.nl");
define("ADMIN_EMAIL", "faizan_218@hotmail.com");


define("EMAIL_ICON1_IMAGE", SITE_URL . "img/color-facebook-48.png");
define("EMAIL_ICON2_IMAGE", SITE_URL . "img/color-link-48.png");
define("EMAIL_ICON3_IMAGE", SITE_URL . "color-linkedin-48.png");
define("EMAIL_ICON1_URL", "https://www.facebook.com/bijlesaanhuisnl/");
define("EMAIL_ICON2_URL", "https://bijlesaanhuis.nl/");
define("EMAIL_ICON3_URL", "https://www.linkedin.com/company/bijlesaanhuis/");

define("ACCESS_TOKEN", "pk.eyJ1IjoiYmlqbGVzYWFuaHVpcyIsImEiOiJjanVyNzhwbjIxb2pkM3ltdW51OHMwdXI5In0.u0dCwVetw_3tn1M_lfQfPw");

function test_input($data)
{
	global $con;

	$data = trim($data);
	$data = stripslashes($data);
	$data = htmlspecialchars($data);

	return $con->real_escape_string($data);
}


