<link rel="stylesheet" href="/app/assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/app/assets/plugins/dropzone/dropzone.css">
<link rel="stylesheet" href="/app/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
<link rel="stylesheet" href="/app/assets/plugins/morrisjs/morris.min.css" />
<!-- JQuery DataTable Css -->
<link rel="stylesheet" href="/app/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
<link rel="stylesheet" href="https://wotcnow.com/pm/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css">

<!-- Bootstrap Select Css -->
<link rel="stylesheet" href="/app/assets/plugins/bootstrap-select/css/bootstrap-select.css" />

<!-- Custom Css -->
<link rel="stylesheet" href="/app/assets/css/main.css">
<link rel="stylesheet" href="/app/assets/css/color_skins.css">
<link rel="stylesheet" href="/app/assets/plugins/waitme/waitMe.css">
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">

<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">



<style>
	#ui-datepicker-div{
z-index:350 !important;
}
	body{
		background-color: #FAFBFC !important;
	}
	.hide{display:none;}
	
	
	input[type=text], input[type=password], input[type=search], input[type=email], input[type=number], input[type=date],select, textarea{
		font-family: poppins;
		font-size: 14px;
		font-weight: normal;
		background-color: #FAFBFC !important;
		border-radius:6px;
		color: #486066 !important;
	}
	
	#profileImage {
	      width: 100px;
		height: 100px;
		border-radius: 50%;
		background: #F1F1F1;
		font-size: 35px;
		color: #486066;
		text-align: center;
		line-height: 106px;
		margin: Auto;
    	margin-bottom: 13px;
	}
	
	#profileImageMini {
	     width: 40px;
		height: 40px;
		border-radius: 50%;
		background: #F1F1F1;
		font-size: 15px;
		color: #486066 !important;
		text-align: center;
		line-height: 41px;
		margin: Auto;
		margin-right: 10px;
	}
	
	li.list-group-item {
		background-color: #e8e8e8 !important;
	}
	.chat .chat-history .other-message {
		background: #d6d6d6 !important;
	}
	
	.leftSlot{
		font-size: 12px;
		line-height: 11px;
		color: white !important;
		background-color: #61a1de;
		border-radius: 30%;
		display: inline-block;
		height: 20px;
		padding: 5px 10px;
		position: relative;
		top: -5px;
	}
	div.btn-group > button.btn.btn-default {
		color: #5CC75F;
	}
	a.sidemenu_a span:hover{
		color: #5CC75F !important;
	}
	a, img{box-shadow: 0px 0px 0px 0px !important;}
	
	*{
		color:#486066!important;
		font-size: 16px;
		font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif !important; 
	}
	
	.bg-green {
    background: transparent !important;
    color: #5CC75F !important;
    border: 2px solid #5CC75F !important;
    border-radius: 22px !important;
    letter-spacing: 2px;
    font-size: 14px;
    font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif!important;
    font-weight: 600!important;
    text-transform: uppercase!important;
}
	
table.dataTable {
    border-collapse: collapse !important;
}
	
	a.page-link {
    background-color: grey !important;
}
.event-inverse {
    background-color: #696767 !important;
}
span.event-item {
    color: #b3afaf !important;
}
span.pull-left.event {
    margin-left: 13px;
}
.alert.alert-danger {
    background-color: rgba(255,54,54,0.8) !important;
}
	
hr{border-top: 1px solid #486066 !important;}
.page-calendar .event-name:hover {
    background: #ededed !important;  /*#d0d0d0*/
}

	
/*<!-- Calendar month-day color --->*/
.cal-month-day.red.cal-day-inmonth{background-color: #e0080873 !important;}
.cal-month-day.green.cal-day-inmonth{background-color: #08e00873 !important;}
/*<!-- Calendar month-day color --->*/

	
	/*<!-- Calendar DAY month-day color --->*/
	.cal-cell1 {font-size: 18px;}
	/*<!-- Calendar DAY month-day color --->*/

	
/*<!-- Calendar pre/next button --->*/
div.calendar-page-header > div.pull-right > div.btn-group > button:nth-child(1) {
    border-radius: 100px 0px 0px 100px !important;
}
div.calendar-page-header > div.pull-right > div.btn-group > button:first-child {
    border-radius: 100px 0px 0px 100px !important;
}
div.calendar-page-header > div.pull-right > div.btn-group > button:nth-child(2) {
    display: none !important;
}
div.calendar-page-header > div.pull-right > div.btn-group > button:nth-child(3) {
    border-radius: 0px 100px 100px 0px  !important;
}	
/*<!-- End Calendar pre/next button -->*/
div.colorLegend span.color {
    display: inline-block !important;
    width: 20px !important;
    height: 20px !important;
    border-radius: 6px !important;
}
div.colorLegend span.available{
	display: inline-block !important;
    width: 20px !important;
    height: 20px !important;
    border-radius: 6px !important;
	background-color: #a7ef9f !important;
	margin-left:10px;
	}
div.colorLegend span.request{
	display: inline-block !important;
    width: 20px !important;
    height: 20px !important;
    border-radius: 6px !important;
	background-color: #ffbd6b !important;
	margin-left:10px;
	}
div.colorLegend span.appointment{
	display: inline-block !important;
    width: 20px !important;
    height: 20px !important;
    border-radius: 6px !important;
	background-color: #5cc75f !important;
	margin-left:10px;
	}
.cal-event-list .event.pull-left {
    margin-top: 5px;
}
ul.pagination > li >a.page-link{
    color: #5CC75F !important;
    background-color: transparent !important;
    border: 2px solid #5CC75F !important;
}
ul.pagination li a.page-link:hover{
    color: white !important;
    background-color: #5cc75f !important;
    border: 2px solid #50d38a !important;
}
ul.pagination > li >a.page-link.active{
    color: white !important;
    background-color: #5cc75f !important;
    border: 2px solid #50d38a !important;
}
.page-calendar .b-primary {border-color: #ff9d00 !important;}
.b-warning {border-color: #ff0 !important;}
.b-special {border-color: #18ce0f !important;}
.theme-green .page-loader-wrapper {background: #FAFBFC;}
.theme-green .page-loader-wrapper .loader p{color:#FAFBFC !important;}
.theme-green .page-loader-wrapper .loader p:after{color:#486066 !important; margin-left: -100px; content: "Even geduld aub...";}

	
.dropdown-menu {
    margin-top: 11px !important;
}
.btn-primary {
    background: transparent !important;
    color: #5CC75F !important;
    border: 2px solid #5CC75F !important;
    border-radius: 22px !important;
    letter-spacing: 0;
    font-size: 14px;
    font-family: 'Poppins',Helvetica,Arial,Lucida,sans-serif!important;
    font-weight: 600!important;
    text-transform: uppercase!important;
}
.btn-primary:hover, .theme-green .btn-primary:focus {
    color: #F1F1F1 !important;
    background-color: #5CC75F !important;
}
	
	
	/* Dailog Box style Ja/Nae */
	
.btn-success {
    background-color: #5CC75F !important;
    border-color: #5CC75F !important;	
    color: #FAFBFC !important;
    font-family: Poppins !important;
    font-size: 16px !important;
}
.btn-danger {
    background-color: #FFBD6B !important;
    border-color: #FFBD6B !important;
    color: #FAFBFC !important;
    font-family: Poppins !important;
    font-size: 16px !important;
}
	
	/* showNotification */
.alert.alert-success > span {
    color: #FAFBFC !important;
}
.alert.alert-success{
    background-color: #5cc75ffa;
}
.alert.alert-success>button.close {
    color: #FAFBFC !important;
}

.alert.alert-danger > span {
    color: #FAFBFC !important;
}
.alert.alert-danger{
    background-color: #FFBD6B;
}
.alert.alert-danger>button.close {
    color: #FAFBFC !important;
}
.calender_dropdown{
	margin-left: 20px;
	display: inline;
}
.calender_dropdownText{
	display: block;
	text-decoration: underline;
}

/* .theme-green section.content:before {
    background-color: transparent !important;
} */
.navbar .dropdown .dropdown-menu {
	visibility: unset;
	opacity: 1;
}
.tippy-content {
	font-size: 14px;
}
</style>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139743478-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-139743478-1');
</script>
