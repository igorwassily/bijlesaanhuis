<?php
require_once("connection.php");
// to implement ProjectManager Data Table
// DB table to use
$table = 'user';

// Table's primary key
$primaryKey = 'userID';

// Array of database columns which should be read and sent back to DataTables.
// The `db` parameter represents the column name in the database, while the `dt`
// parameter represents the DataTables column identifier. In this case simple
// indexes
$columns = array(
	array( 'db' => '`u`.`userID`',     'dt' => 0, 'field' => 'userID'),
	array( 'db' => '`u`.`branchID`',     'dt' => 1, 'field' => 'branchID'),
	array( 'db' => '`u`.`username`',     'dt' => 2, 'field' => 'username'),
	array( 'db' => '`u`.`loginattempts`',     'dt' => 3, 'field' => 'loginattempts'),
	array( 'db' => '`u`.`created_at`',     'dt' => 4, 'field' => 'created_at'),
	array( 'db' => '`u`.`updated_by`',     'dt' => 5, 'field' => 'updated_by'),
	array( 'db' => '`u`.`updated_at`',     'dt' => 6, 'field' => 'updated_at')
);

// SQL server connection information



/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * If you just want to use the basic configuration for DataTables with PHP
 * server-side, there is no need to edit below this line.
 */

/*require( 'ssp.class.php' );
$where = "`ApplicationStatus` != '0'";

echo json_encode(
	//SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns )
    SSPCustom::simpleCustom( $_GET, $sql_details, $table, $primaryKey, $columns, $where )*/
    require( 'ssp.customized.class.php' );
//$where = "";

$joinQuery = "FROM `user` AS `u`";
$extraWhere = "";  

echo json_encode(
	SSP::simple( $_GET, $sql_details, $table, $primaryKey, $columns, $joinQuery, $extraWhere)
);
?>