<!--<script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>-->
<!--<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>-->
<!--<script src="/app/assets/bundles/libscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<!--<script src="/app/assets/bundles/vendorscripts.bundle.js"></script> <!-- Lib Scripts Plugin Js -->
<!----------------------------------recaptcha-------------- -->
<script src="https://www.google.com/recaptcha/api.js?explicit&hl=nl" async defer></script>
<!--<script src="https://unpkg.com/popper.js@1"></script>-->
<script src="https://unpkg.com/tippy.js@4"></script>
<style>
    .tippy-popper {
        z-index: 2000000001 !important;
        max-width: 80%;
    }

    .tippy-popper .tippy-tooltip {
        background-color: #FFFFFF !important;
        box-shadow: 0 0 4px 2px rgba(0, 0, 0, 0.4) !important;
        line-height: 1.6 !important;
    }

    .tippy-popper .tippy-tooltip * {
        text-align: left;
    }

    .tippy-popper .tippy-tooltip a {
        color: #5cc75f !important;
    }

    .tippy-popper .tippy-backdrop {
        display: none;
    }

    .cursor {
        cursor: pointer;
    }

    .width {
        width: 90% !important;
        margin: auto !important;
    }

    .login_form_footer {
        border: none;
    }

    /*.modal-content {*/
    /*    -webkit-animation-name: none !important;*/
    /*    -webkit-animation-duartion: none !important;*/
    /*    animation-name: none !important;*/
    /*    animation-duration: 0ms !important;*/
    /*    border-radius: 6px;*/
    /*    border: none !important;*/
    /*    width: 65%;*/
    /*    height: auto !important;*/
    /*    max-height: 95%;*/
    /*    overflow-y: auto;*/
    /*    overflow-x: hidden;*/
    /*    !* margin: 2rem 0; *!*/
    /*}*/

    .border__top {
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
    }

    .border__bottom {
        border-bottom-left-radius: 6px;
        border-bottom-right-radius: 6px;
    }

    .btn {
        display: block !important;
        margin: auto !important;
        width: 90% !important;
    }

    .modal-header {
        border-bottom: solid #bbb 1px !important;
        border-top-left-radius: 6px;
        border-top-right-radius: 6px;
        display: block;
    }

    .border__line {
        width: 100%;
        margin-bottom: 2.375rem;
        border: 1px solid #bbb;
    }

    .modal-btn {
        background-color: #FFBD6B !important;
        color: #FFFFFF !important;
    }

    .m-16 {
        padding-top: 1rem;
        margin-bottom: -1rem;
        text-align: center;
        font-weight: bold;
        font-size:
    }

    .login_form_btn {
        padding-left: 0 !important;
        padding-right: 0 !important;
    }

    .modal-header h2 {
        padding: 1rem !important;
        text-align: center;
    }

    .modal-body {
        padding: 0 1.25rem !important;
    }

    .modal-content h2 {
        padding: 1.5rem 0 1.5rem 0 !important;
    }

    .h {
        height: 21px;
    }

    #msform input {
        border: 1px solid #bdbdbd !important;
    }

    @media screen and (max-width: 480px) {
        .modal-content {
            width: 80% !important;
        }

        .modal-header {
            padding: 2px 0 !important;
        }
    }

    @media screen and (max-width: 375px) {
        .modal-header {
            padding: 5px 0 !important;
        }
    }

    @media screen and (min-width: 480px) and (max-width: 576px) {
        .modal-content {
            width: 60% !important;
        }
    }

    @media screen and (min-width: 576px) and (max-width: 680px) {
        .modal-content {
            width: 50% !important;
        }
    }

    @media screen and (min-width: 768px) and (max-width: 991px) {
        .modal-content {
            width: 45% !important;
        }
    }

    @media screen and (min-width: 970px) and (max-width: 2000px) {
        .modal-content {
            width: 40% !important;
        }
    }

    @media screen and (min-width: 2000px) and (max-width: 3000px) {
        .modal-content {
            width: 25% !important;
        }
    }

    @media screen and (max-width: 1024px) {
        .modal-content h2 {
            text-align: center;
        }
    }

    .border-top {
        border-top: 1px solid #BBB;
    }

    .m-t-38 {
        margin-top: 38px;
    }

    .no-t-m {
        margin-top: 0 !important;
    }

    .m-b-28 {
        margin-bottom: 1.75rem;
    }

    #progressbar li {
        background-color: #a7ef9f54;
    }

    .mar {
        margin-top: 1rem;
    }

    .pad {
        padding: 2rem 0;
    }

    .modal-body {
        border-bottom-left-radius: 6px;
        border-bottom-right-radius: 6px;
    }

    .modal-title {
        font-size: 24px;
        padding: 0 1rem !important;
        text-align: center;
        color: #5cc75f !important;
    }

    .form-group {
        margin: 2rem 0 !important;
    }

    @media only screen and (max-width: 767px) {
        .modal-title {
            padding: 0 1.75rem !important;
        }

        .margin-top {
            margin-top: 2rem !important;
        }

    }

    @media only screen and (max-width: 375px) {
        .pad {
            padding: 2.5rem 0;
        }
    }

    .tooltip.fade, .ui-tooltip {
        z-index: 2000000001 !important;
        opacity: 1;
        display: none;
    }

    /* .tooltip.fade, .ui-tooltip.ui-widget {
        z-index: 100000 !important;
    } */
    .tooltip-inner {
        background-color: #FFFFFF !important;
        box-shadow: 0 0 4px 2px rgba(0, 0, 0, 0.4) !important;
    }

    .registeren p.form-error, .login_form p.form-error {
        color: #e45e5e !important;
    }

    .inlog-btn {
        padding-bottom: 0;
    }

    .inlog-btn-text {
        color: #5cc75f !important;
    }

    .container {
        width: 100%;
    }

    #login_form .recaptcha-input {
        padding: 0 14px !important;
    }



    @media screen and (max-width: 400px) {

        #login_form .content {
            padding: 0px 0 20px 0px;
        }
    }

   

    .modal--h5_text {
        margin-left: 15px;
        margin-right: 15px;
        padding-bottom: 10px !important;
        font-size: 16px !important;
    }

    .modal .btn {
        margin: 0 auto 1rem auto !important;
    }

    .register-Succes p {
        font-weight: normal;
    }

    .modal-content .form-control:focus {
        -webkit-box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(102, 175, 233, .6);
        box-shadow: inset 0 1px 1px rgba(0, 0, 0, .075), 0 0 8px rgba(102, 175, 233, .6);
        border: 1px solid #bdbdbd !important;
    }

    .modal-content .name-input-wrapper {
        display: flex;
    }

    .modal-content .name-input-wrapper #first-name, .modal-content .name-input-wrapper #parent-first-name {
        margin: 0 2.5% 0 0 !important;
    }

    .modal-content .name-input-wrapper input {
        max-width: 47.5% !important;
    }

    .modal-content .msg-error {
        margin: 0;
    }

    .modal-info-icon {
        margin-left: 0 !important;
        margin-right: 0 !important;
    }

    /* Override main.css for modal layout*/
    .modal-content .input-group, .modal-content .form-group {
        margin-bottom: 0px;
    }

    .modal-content input::placeholder {
        color: #2c3e50 !important;
        opacity: .8;
    }

    @media screen and (max-width: 320px) {
        .modal-content .name-input-wrapper {
            display: block;
        }

        .modal-content .name-input-wrapper #last-name, .modal-content .name-input-wrapper #parent-last-name {
            margin-top: 10px !important;
        }

        .modal-content .name-input-wrapper input {
            max-width: 100% !important;
            width: 100% !important;
        }

        #msform label {
            margin-left: 0;
        }

        .warningmsg {
            color:red !important;
            font-color:red !important;
            display:none !important;
        }

        #login_form.login-form #recaptcha, #login_form.login_form #recaptcha {
        transform:scale(0.6) !important;
        -webkit-transform:scale(0.6) !important;
        transform-origin:0 0 !important;
        -webkit-transform-origin:0 0 !important;
        } 
        #login_form .recaptcha-input{
            padding: 0 !important;

        }
    }
    @media screen and (max-width: 768px){ 
        #login_form.login-form #recaptcha, #login_form.login_form #recaptcha {
        transform:scale(0.77) !important;
        -webkit-transform:scale(0.77) !important;
        transform-origin:0 0 !important;
        -webkit-transform-origin:0 0 !important;
        padding: 0 !important;

        } 
        #login_form .recaptcha-input{
            padding: 0 !important;

        }

        .input-group.input-lg.login-input-group{
            padding: 0 !important;
        }

        #login_form .content{
            padding: 0px 0px 20px 0px;
        }
    } 
}
</style>

<script>
    function checkEmailAddress() {
        // test if e-mail already exists in db
        var ajax_url = '/check-email-exists.php';
        var email = $("#email").val();
        var data = {email: email, usergroup: "Student"};

        $.ajax({
            url: ajax_url,
            type: "POST",
            data: data
        }).done(function (responseTxt) {
            console.log('response', responseTxt);
            if (responseTxt.indexOf("Duplicate") === -1) {
                // if duplicate warning has been shown, hide it
                //$(".register-duplicate").fadeOut();
                $("#email").removeClass("error");
                $(".duplicate-error").remove();
                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.hide();

                scrollToModalTop();

                initMap();
            } else {
                // Show e-mail duplicate warning
                $("#email").addClass("error");
                $(".duplicate-error").remove();
                $("#email").after("<p class='form-error duplicate-error'>Het ingevoerde e-mailadres is al in gebruik</p>");
            }
        });
    }

</script>

<div id="login-register-modal" class="modal">
    <!-- Modal content -->
    <div class="modal-content modal--no-scroll" id="modal-content">
        <div class="modal-header border__top" name="modal-header">
            <div class="keuze1">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 class="registreren-modal-title">Registeren of inloggen</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-inloggen inloggen">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2>Inloggen</h2>
                        </div>
                    </div>
                </div>
            </div>
            <div class="login-Succes">
                <h2>U bent nu ingelogd</h2>
            </div>
            <div class="register-Succes">
                <h2>Verificatiemail ontvangen</h2>
            </div>
            <div class="registeren">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <h2 style="text-align: center">Registeren</h2>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <p>Registreer en boek direct de bijlesdocent die bij jou past!</p>
                        </div>
                    </div>
                </div>
                <!-- progressbar -->
                <ul id="progressbar">
                    <li class="active"></li>
                    <li></li>
                </ul>
            </div>
            <span class="close">&times;</span>
        </div>
        <div class="modal-body border__bottom">
            <div class="login-Succes">
                <a class="account-btn" href="teachers-dashboard.php">Account</a>
            </div>
            <div class="keuze1">
                <p>Om een afspraak te kunnen maken of een bericht te versturen, moet je je eerst registeren. Registeren kost slechts 2 minuten!</p>
                <a class="cursor modal-btn registreren-modal-btn">REGISTREREN</a>
            </div>
            <div class="register-Succes">
                <p class="m-b-28">Je bent een stap dichter bij betere schoolcijfers! We hebben een e-mail verstuurd,
                    zodat je je account kan activeren. Heb je een vraag? We zijn er graag voor je!</p>
            </div>
            <div class="modal-inloggen inloggen">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <form id="login_form" class="form login_form" onsubmit="return submit_form()">
                                <div class="content centeer">
                                    <div class="form-float">
                                        <label for="username">E-mailadres</label>
                                        <input id="login-email" type="text" class="form-control login_form_input"
                                               placeholder="E-mailadres" name="username" required maxlength="60"
                                               minlength="5"/>
                                        <!-- <span class="input-group-addon inloggen-input-icon">
                                            <i class="zmdi zmdi-account-circle"></i>
                                        </span> -->
                                    </div>
                                    <div class="form-float" style="margin-bottom: 15px;">
                                        <label for="password">Wachtwoord</label>
                                        <input id="login-password" type="password" placeholder="Wachtwoord"
                                               class="form-control login_form_input" name="password" required
                                               maxlength="20" minlength="8"
                                               title="Wachtwoord moet minimaal 8 tekens bevatten"/>
                                        <!-- <span class="input-group-addon inloggen-input-icon">
                                            <i class="zmdi zmdi-lock"></i>
                                        </span> -->
                                    </div>
                                </div>
                                <div class="input-group input-lg centeer recaptcha-input">
                                    <span class="msg-error error"></span>
                                    <div id="recaptcha" class="g-recaptcha"
                                         data-sitekey="6LebmZ8UAAAAAJU6v6g1ntuuNZycJMbr8k6Pv5F3"
                                         ></div>
                                </div>
                                <div class="centeer login_form_btn">
                                    <button type="button" name="login" id="login-modal">INLOGGEN</button>
                                </div>
                                <span class="msg-error"></span>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-register registeren">
                <!-- multistep form -->
                <form id="msform" id="studentRegistration">
                    <!-- fieldsets -->
                    <fieldset>
                        <div class="form-float">
                            <!--<label for="username">Username</label>-->
                            <input type="hidden"
                                   class="form-control"
                                   placeholder="User Group"
                                   name="choice"
                                   id="choice"
                                   value="Student-Registration"/>
                            <input type="hidden"
                                   class="form-control"
                                   placeholder="User Group"
                                   name="usergroup"
                                   id="usergroup"
                                   value="Student"/>
                            <input type="hidden"
                                   class="form-control"
                                   placeholder="Username"
                                   name="username"
                                   id="username"/>
                        </div>
                        <div class="form-float">
                            <!--<span class="modal-tooltip" tooltip="Dit e-mailadres wordt gebruikt voor alle vormen van contact, behalve facturering. Voor facturering wordt het ouderlijk e-mailadres van de volgende pagina gebruikt." flow="down"><img style="margin-top: 5px; height: 20px;" src="/app/assets/images/Minimalist_info_Icon.png"></span>-->
                            <label class="p-l-5">E-mailadres *
                                <i style="color: #BBB !important;"
                                   class="fa modal-info-icon tippy"
                                   data-toggle="tooltip"
                                   data-placement="bottom"
                                   data-tippy-content="Dit e-mailadres wordt gebruikt voor alle vormen van contact, behalve facturering. Voor facturering wordt het ouderlijk e-mailadres van de volgende pagina gebruikt."><img
                                            style="margin: 7px 7px 0 7px; height: 15px;"
                                            src="/app/assets/images/info-icon.png"></i>
                            </label>
                            <!-- <i style="color: #BBB !important;" class="fa modal-info-icon" data-toggle="tooltip" data-placement="bottom" 
                                title="Dit e-mailadres wordt gebruikt voor alle vormen van contact, behalve facturering. Voor facturering wordt het ouderlijk e-mailadres van de volgende pagina gebruikt."><img style="margin: 7px 7px 0 7px; height: 15px;" src="/app/assets/images/info-icon.png"></i> -->
                            <input type="email"
                                   class="form-control width"
                                   placeholder=" E-mailadres" name="email"
                                   id="email"
                                   required/>
                            <input type="hidden"
                                   name="checkEmail"
                                   id="checkEmail"
                                   value=""/>
                        </div>
                        <div class="form-float">
                            <label class="p-l-5 p-t-10">Telefoonnummer *</label>
                            <input type="tel"
                                   class="form-control width"
                                   placeholder="Telefoonnummer"
                                   name="telephone"
                                   id="telephone"
                                   onfocus="checkEmailAddress();">
                        </div>
                        <div class="form-float">
                            <label class="p-l-5 p-t-10">Wachtwoord *</label>
                            <input type="password"
                                   class="form-control width"
                                   placeholder="Wachtwoord"
                                   name="password"
                                   id="passwordreg"
                                   minlength="8"
                                   required/>
                        </div>

                        <div class="form-float" style="padding-bottom: 1rem">
                            <label class="p-l-5 p-t-10">Wachtwoord bevestigen *</label>
                            <input type="password"
                                   class="form-control width"
                                   placeholder="Wachtwoord bevestigen"
                                   name="confirm_password"
                                   id="confirm_password"
                                   minlength="8"
                                   required/>
                        </div>
                        <input type="button"
                               id="next-part1"
                               name="next"
                               class="btn next action-button"
                               value="Volgende"/>
                    </fieldset>

                    <!-- Secondary form //-->
                    <fieldset>
                        <div class="form-float"
                             style="margin: 0 5%;">

                            <div class="form-groups">

                                <label class="p-l-5 p-t-10"
                                       style="margin-left:0; margin-right:15.5px;">
                                    Leerling*
                                    <i style="color: #BBB !important;"
                                       class="fa modal-info-icon tippy"
                                       data-toggle="tooltip"
                                       data-placement="bottom"
                                       data-tippy-content="Indien de naam van de leerling uit privacy overwegingen niet gegeven wenst te worden, kan gebruik gemaakt worden van een fictieve naam of de naam van een ouder / verzorger.">
                                            <img style="margin-top: 5px; height: 15px;"
                                                 alt="info"
                                                 src="/app/assets/images/info-icon.png" />
                                    </i>
                                </label>

                                <div class="input-group"
                                     style="padding-bottom: 14px;">
                                    <input type="text"
                                           class="form-control h no-t-m"
                                           placeholder="Voornaam"
                                           name="first-name"
                                           id="first-name"
                                           required/>
                                    <p id="warningmsg_fn"
                                          style="color:red !important; display:none;">
                                        Voornaam is verplicht
                                    </p>
                                </div>

                                <div class="input-group"
                                     style="padding-bottom: 14px;">
                                    <input type="text"
                                           class="form-control h no-t-m"
                                           placeholder="Achternaam"
                                           name="last-name"
                                           id="last-name"
                                           required/>
                                    <p id=last-name-desc"
                                          class="warningmsg_ln"
                                          style="color:red !important; display:none;">
                                        Achternaam is verplicht
                                    </p>
                                </div>
                            </div>


                        <div class="form-groups">

                            <label class="p-l-5 p-t-10"
                                   style="margin-left:0; margin-right:15.5px;">
                                Ouder*
                            </label>

                            <div class="input-group" style="padding-bottom: 14px;">
                                <input type="text"
                                       class="form-control h no-t-m"
                                       placeholder="Ouder voornaam"
                                       name="parent-first-name"
                                       id="parent-first-name"
                                       required
                                       />
                                <p id="parent-first-name-descs"
                                      class="warningmsg_pf"
                                      style="color:red !important; display:none;">
                                    Ouder voornaam is verplicht
                                </p>
                            </div>
                            <div class="input-group" style="padding-bottom: 14px;">
                                <input type="text"
                                       class="form-control h no-t-m"
                                       placeholder="Ouder achternaam"
                                       name="parent-last-name"
                                       id="parent-last-name"
                                       required/>
                                <p id="parent-last-name-desc"
                                      class="warningmsg_pl"
                                      style="color:red !important; display:none;">
                                    Ouder achternaam is verplicht
                                </p>

                            </div>
                        </div>

                        <!-- Ouderlijk Email Adres //-->
                        <div class="form-groups">

                                <label class="p-l-5 p-t-10"
                                       style="margin-left:0;margin-right:4px;">
                                    Ouderlijk e-mailadres*
                                    <i style="color: #BBB !important;"
                                       class="fa modal-info-icon tippy"
                                       data-toggle="tooltip"
                                       data-placement="bottom"
                                       data-tippy-content="Dit e-mailadres wordt gebruikt voor maandelijkse facturering, uiterlijk elke 10e van de maand volgend op de maand waarop de bijles plaatsvond.">
                                            <img style="margin-top: 5px; height: 15px;"
                                                 src="/app/assets/images/info-icon.png">
                                    </i>
                                </label>

                                <div class="input-group">
                                    <input type="email"
                                           class="form-control h no-t-m"
                                           placeholder="E-mailadres"
                                           name="parent-email"
                                           id="parent-email"
                                           required
                                           aria-describedby="parent-email-desc"/>
                                    <p id="parent-email-desc"
                                          class="warningmsg_pe"
                                          style="color:red !important; display:none;">
                                        Vul een geldig e-mailadres in
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- End ouderlijk email adres //-->

                        <div class="form-float width">
                            <label for="postcode"
                                   class="p-l-5 p-t-10"
                                   style="margin-left: 0rem; display: block">
                                Bijlesadres*
                            </label>

                            <input type="text"
                                   placeholder="Postcode"
                                   id="postcode"
                                   name="postcode"
                                   required
                                   class="form-control h no-t-m"
                                   style="width: 50%; display: inline-block;">

                            <input type="text"
                                   placeholder="Huisnummer"
                                   id="huisnummer"
                                   name="huisnummer"
                                   required
                                   class="form-control h no-t-m"
                                   style="width: 40%; display: inline-block; right: 5%; position: absolute;">
                        </div>

                        <div class="form-float width">
                            <input type="text" placeholder="Straatnaam" name="streetname" id="straatnaam" class="form-control h no-t-m"
                                   required>
                            <input type="hidden" class="form-control" name="_lat" id="lat">
                            <input type="hidden" class="form-control" name="_lng" id="lng">
                            <input type="hidden" class="form-control" name="_place_name" id="place_name">
                        </div>

                        <div class="form-float width"
                             style="padding-bottom: 14px;">
                            <input type="text" class="form-control h no-t-m"
                                   placeholder="Woonplaats"
                                   id="woonplaats"
                                   name="city"
                                   required>

                            <!--  <div class="map-wrapper" style="height: 312px; padding: 0 !mportant; display: flex !important;">
								  <input type="hidden" class="form-control" name="_lat" id="lat">
								  <input type="hidden" class="form-control"  name="_lng" id="lng">
								  <input type="hidden" class="form-control"  name="_place_name" id="place_name">
								  <div id='_map' style="position:relative; top: 0;bottom: 0;width: 100%; height: 100%; overflow:visible;"> -->
                            <!-- THIS IS MY CUSTOM ONE
                            <input type="text" placeholder="adres" id="adres_input" required>
-->
                            <!-- STORE LAT LNG PLACE NAME IN HIDDEN FIELDS JUST LIKE BEFORE -->

                            <!-- </div>
                        </div>-->
                        </div>
                        <pre id='coordinates' class='coordinates' style="display:none;"></pre>

                        <!--
                        <h5 class="register-duplicate">Het e-mail adres wat u heeft ingevuld is al in gebruik</h5>
                        //-->

                        <h5 class="register-error">Er is iets fout gegaan met het registeren</h5>
                        <label for="error"
                               id="errorMessage"
                               style="margin-left: 0rem; color:red !important; display: none;">
                            We hebben geen adres gevonden, neem contact met ons op
                        </label>
                        <input type="button" name="previous" class="btn previous action-button" value="Vorige"/>
                        <input type="submit" name="next" class="btn next action-button" value="Afronden"
                               id="submit-register" style="margin-top: 1rem !important"/>
                    </fieldset>
                </form>
            </div>
        </div>
        <div class="modal-bottom">
            <div class="keuze1">
                <h4 class="modal-bottom__link">Inloggen</h4>
            </div>
            <div class="register-Succes border-top">
                <a class="cursor modal-btn inlog-btn-after-register" style="background-color: #5cc75f !important;">INLOGGEN</a>
            </div>
            <div class="modal-register registeren" style="margin-bottom:2rem"></div>
            <div class="modal-inloggen inloggen border-top">
                <div class="footer text-center login_form_footer">
                    <h5 class="inlog-error js-inlog-error">Het e-mailadres en/of wachtwoord is niet correct</h5>
                    <h5 class="inlog-error js-mail-correct">Het e-mailadres en/of wachtwoord is niet correct</h5>

                    <p class="login-form-footer__text">
                        <a href="/app/forgotPassword" class="login-form-footer__link">Wachtwoord vergeten?</a>
                    </p>
                    <p class="login-form-footer__text">
                        <a id="registeer-from-login" class="login-form-footer__link PopupBtnRegister registreren-modal-btn">Registreer als leerling</a> / <a href="/app/docenten-registratie" class="login-form-footer__link">Aanmeldprocedure docent</a>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        $('[data-toggle="tooltip"]').tooltip({});
    });
</script>
 