<?php

ini_set('display_errors', 1);
error_reporting(E_ALL);
require_once("app/includes/connection.php");
require_once("../inc/Email.php");

$userID = "";

/* Student Registration */

if (!empty($_POST['usergroup'])) {
	$usergroup = test_input($_POST['usergroup']);
} else {
	$usergroup = "";
}

$stmt = $con->prepare("SELECT usergroupID 
                                    FROM usergroup 
                                    WHERE usergroupname = ?");
$stmt->bind_param("s", $usergroup);
$stmt->execute();
$stmt->bind_result($usergroupID);
$stmt->fetch();
$stmt->close();

$stmt1 = $con->prepare("INSERT INTO user( usergroupID, 
                                                 username, 
                                                 password, 
                                                 email, 
                                                 verificationcode,
                                                 active,
                                                 registered_from) values ( ?, ?, ?, ?, ?, ?, ? )");

if (!empty($_POST['username'])) {
	$username = test_input($_POST['username']);
} else {
	$username = "";
}

if (!empty($_POST['password'])) {

	$password = md5(test_input($_POST['password']));
} else {
	$password = "";
}

if (!empty($_POST['email'])) {

	$email = test_input($_POST['email']);
} else {
	$email = "";
}

$verificationcode = md5(uniqid(rand(), true));
$active = 0;

/**
 * check if referer otherwise use the request URI
 */
$registered_from = 'nowhere';
if (isset($_SERVER['HTTP_REFERER'])) {
	$registered_from = $_SERVER['HTTP_REFERER'];
} elseif (isset($_SERVER['REQUEST_URI'])) {
	$registered_from = $_SERVER['REQUEST_URI'];
}

$stmt1->bind_param("issssis", $usergroupID,
	$username,
	$password,
	$email,
	$verificationcode,
	$active,
	$registered_from);
if ($stmt1->execute()) {
	$userID = $stmt1->insert_id;
	$stmt1->close();
} else {
	echo $con->error;
}

$stmt2 = $con->prepare("SELECT contacttypeID 
                                    FROM contacttype 
                                    WHERE contacttype = ?");
$stmt2->bind_param("s", $usergroup);
$stmt2->execute();
$stmt2->bind_result($contacttypeID);
$stmt2->fetch();
$stmt2->close();

$stmt3 = $con->prepare("INSERT INTO contact ( contacttypeID, userID, prmary, firstname, lastname, telephone, email, address, postalcode, city, country, emailverified, latitude, longitude, place_name) 
                                            values (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)");

if (!empty($_POST['first_name'])) {

	$firstname = test_input($_POST['first_name']);
} else {
	$firstname = "";
}

if (!empty($_POST['last_name'])) {

	$lastname = test_input($_POST['last_name']);
} else {
	$lastname = "";
}
$name = $firstname;

if (!empty($_POST['telephone'])) {

	$telephone = test_input($_POST['telephone']);
} else {
	$telephone = "";
}

if (!empty($_POST['lat'])) {

	$lat = test_input($_POST['lat']);
} else {
	$lat = "";
}
if (!empty($_POST['lng'])) {

	$lng = test_input($_POST['lng']);
} else {
	$lng = "";
}
if (!empty($_POST['place_name'])) {

	$place_name = test_input($_POST['place_name']);
} else {
	$place_name = "";
}

if (!empty($_POST['postcode'])) {
	$postalcode = test_input($_POST['postcode']);
} else {
	$postalcode = "";
}
if (!empty($_POST['huisnummer'])) {
	$housenumber = test_input($_POST['huisnummer']);
} else {
	$housenumber = "";
}
if (!empty($_POST['streetname'])) {
	$street = test_input($_POST['streetname']);
} else {
	$street = "";
}
$address = $street . " " . $housenumber;
if (!empty($_POST['city'])) {
	$city = test_input($_POST['city']);
} else {
	$city = "";
}
$country = "Nederland";

$emailverified = 0;
$primary = 1;

$stmt3->bind_param("iiissssssssisss", $contacttypeID, $userID, $primary, $firstname, $lastname, $telephone, $email, $address, $postalcode, $city, $country, $emailverified, $lat, $lng, $place_name);
if ($stmt3->execute()) {
	$contactID = $stmt3->insert_id;
	$stmt3->close();
}

$stmt4 = $con->prepare("INSERT INTO student (userID,contactID) values (?,?)");
$stmt4->bind_param("ii", $userID, $contactID);
$stmt4->execute();
$stmt4->close();

$contacttype = "Parent";

$stmt2 = $con->prepare("SELECT contacttypeID from contacttype where contacttype = ?");
$stmt2->bind_param("s", $contacttype);
$stmt2->execute();
$stmt2->bind_result($contacttypeID);
$stmt2->fetch();
$stmt2->close();

$stmt3 = $con->prepare("INSERT INTO contact (contacttypeID,userID,prmary,firstname,lastname,telephone,email,emailverified) values (?,?,?,?,?,?,?,?)");

if (!empty($_POST['parent_first_name'])) {
	$firstname = test_input($_POST['parent_first_name']);
} else {
	$firstname = "";
}
$name .= " / $firstname";

if (!empty($_POST['parent_last_name'])) {
	$lastname = test_input($_POST['parent_last_name']);
} else {
	$lastname = "";
}

if (!empty($_POST['parent_email'])) {
	$parent_email = test_input($_POST['parent_email']);
} else {
	$parent_email = "";
}

$emailverified = 0;
$primary = 0;

$stmt3->bind_param("iiissssi", $contacttypeID, $userID, $primary, $firstname, $lastname, $telephone, $parent_email, $emailverified);
if ($stmt3->execute()) {
	$contactID = $stmt3->insert_id;
	$stmt3->close();
}

$stmt4 = $con->prepare("INSERT INTO student (userID,contactID) values (?,?)");
$stmt4->bind_param("ii", $userID, $contactID);
$stmt4->execute();
$stmt4->close();

$alink = 'https://' . $_SERVER['HTTP_HOST'] . '/app/verify?email=' . $email . '&hash=' . $verificationcode;

$email = new Email($db, 'registration_confirmation_student');
$email->Prepare($userID, ['link' => $alink]);

$sentmail = 0;
if ($email->Send('student')) {
	$sentmail = 1;

	$email = new Email($db, 'student_registration_confirmation_for_parent');
	$email->Prepare($userID);
	$email->Send('parent');
}
if ($sentmail == 1) {
	echo "Success";

} else {
	echo "Error";
}
