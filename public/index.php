<?php
error_reporting(E_ALL);
ini_set('display_errors',1);

use Template\Template;

session_start();

chdir(__DIR__);

require_once '../vendor/autoload.php';
require_once '../inc/Common.php';
require_once '../inc/template/Template.php';

$tpl = new Template();
$tpl->Render();
$tpl->Display();
