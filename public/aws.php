<?php

session_start();

chdir(__DIR__);

require_once '../vendor/autoload.php';
require_once '../inc/Common.php';
require_once '../inc/AWS.php';

$service = '';

$url = $_SERVER['REQUEST_URI'];
$pos = strpos($url, '?');
if ($pos > 0) $url = substr($url, 0, $pos);
$url = trim($url, '/');
$pos = strpos($url, '/');
if ($pos > 0) {
	$service = substr($url, 0, $pos);
	$url = substr($url, $pos);
}
$url = trim($url, '/');

$aws = new AWS();

switch ($service) {
	case 's3':
		$file = $aws->getFile($url);
		if (is_null($file)) {
			http_response_code(404);
		} else {
			header("Content-Type: {$file['ContentType']}");
			echo $file['Body'];
		}
		break;
	default:
		http_response_code(400);
}
