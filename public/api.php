<?php

session_start();

use API\Api;

chdir(__DIR__);

require_once '../vendor/autoload.php';
require_once '../inc/Common.php';
require_once '../api/Api.php';

header("Content-Type: application/json");

$api = new Api();
$api->Handle();
