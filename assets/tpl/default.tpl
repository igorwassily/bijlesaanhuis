{extends file="layout.tpl"}

{block name="content"}
	{foreach from=$components item=component}
		{if $component@first}
			{continue}
    	{/if}

		{include "sections/{$component.tpl}.tpl" data=$component.data}
	{/foreach}
{/block}