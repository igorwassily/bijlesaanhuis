{literal}
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
            j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
            'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
            })(window,document,'script','dataLayer','GTM-52JWB73');</script>
<!-- End Google Tag Manager -->
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-52JWB73"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<noscript>
	<img height="1" width="1" style="display:none"
	     src="https://www.facebook.com/tr?id=478656312853879&ev=PageView&noscript=1"/>
</noscript>

<script>
	function trackEvent(category, event, label, value, duration) {
		let data = {
			'event': 'GAEvent',
			'eventCategory': category,
			'eventAction': event,
			'eventLabel': label
		};
		if (value) {
			data.eventValue = value;
		}
		if (duration) {
			data.eventDuration = duration;
		}
{/literal}
		{if $environment == 'production'}
			dataLayer.push(data);
		{else}
			data.tracking = false;
		{/if}

		console.log(data);
	}
</script>
