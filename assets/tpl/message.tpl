<div class="msg-wrapper">
	<div id="msg" class="{if isset($msg)}msg--{$msg.type}{else}msg--hidden{/if}">
		<div id="msg-body" class="msg-body msg-body--grey">
        	{if isset($msg)}{$msg.text}{/if}
		</div>
		<div class="msg-dismiss">
			<i id="msg-dismiss" class="msg-icon msg-icon--blue-grey fas fa-times"></i>
		</div>
	</div>
</div>