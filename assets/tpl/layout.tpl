<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<meta name="description" content="{$page.description}">

	<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
	<meta property="og:locale" content="en_US">
	<meta property="og:type" content="article">
	<meta property="og:title" content="{$page.socialMediaTitle}">
	<meta property="og:description" content="{$page.socialMediaDescription}">
	<meta property="og:url" content="https://"/>
	<meta property="og:site_name" content="Bijles Aan Huis">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:description" content="{$page.socialMediaDescription}">
	<meta name="twitter:title" content="{$page.socialMediaTitle}">

	<title>{$page.title}</title>

    {include "Tracking.tpl"}

    {foreach $jsHeader as $name}
		<script src="/js/{$name}"></script>
    {/foreach}

	<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
    {foreach $css as $name}
		<link rel="stylesheet" href="/css/{$name}">
    {/foreach}

    {if !$signedIn}
		<script src="https://www.google.com/recaptcha/api.js?explicit&hl=nl" async defer></script>
    {/if}
</head>
<body>
{if !$signedIn}
    {include 'Modal.tpl'}
{/if}

{include "message.tpl"}
{include "sections/{$components[0].tpl}.tpl" data=$components[0].data}

<div class="template-content">
	<div class="template-content__container">
        {block name="content"}{/block}
	</div>
</div>

{foreach $js as $name}
	<script src="/js/{$name}"></script>
{/foreach}

{include "Footer.tpl"}

{if isset($msg)}
	<script type="application/javascript">
		$(function () {
			autoDismissMsg();
		})
	</script>
{/if}
</body>
</html>