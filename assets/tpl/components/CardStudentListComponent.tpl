{extends file="components/WrapperCardComponent.tpl"}

{block name="wrapper-card-header"}
  <h5>Leerling: {$data.name}</h5>
{/block}

{block name="wrapper-card-body"}
  <p><span>Adres:</span> <span>{$data.address}</span></p>
  <p><span>Tel.nummer contactpersoon:</span> <span>{$data.phoneStudent}</span></p>

  <p><span>E-mailadres contactpersoon:</span> <span>{$data.emailStudent}</span></p>
  <p><span>Naam ouder:</span> <span>{$data.forenameParent}</span></p>
  <p><span>E-mailadres ouder:</span> <span>{$data.emailParent}</span></p>
  
  <a class="student-list__link" href="/student/profile?sid={$data.id}">Bijles inboeken</a>
{/block}