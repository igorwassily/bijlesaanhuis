<div class="tutoring-info__tutor-container">
    <div class="tutoring-info__tutor-card">
        <a href="https://bijlesaanhuis.nl/app/teacher-detailed-view.php?tid=855&c=0&d=0&t" alt="Een vriendelijke en bekwame docent">
            <img class="tutoring-info__tutor-card__img"
                data-src="/s3/profileImages/15738279115dceb5476cf5a.png">
            <div class="tutoring-info__tutor-card__tutor-details">
                <h5>Niek</h5>
                <span>VWO 6, N&T/N&G</span>
            </div>
        </a>
    </div>
    <div class="tutoring-info__tutor-card">
        <a href="https://bijlesaanhuis.nl/app/teacher-detailed-view.php?tid=907&c=0&d=0&t" alt="Een vriendelijke en bekwame docent">
            <img class="tutoring-info__tutor-card__img"
                data-src="/s3/profileImages/15740767075dd28123443c7.png">
            <div class="tutoring-info__tutor-card__tutor-details">
                <h5>Lynn</h5>
                <span>Bedrijfskunde aan Erasmus</span>
            </div>
        </a>
    </div>
    <div class="tutoring-info__tutor-card">
        <a href="https://bijlesaanhuis.nl/app/teacher-detailed-view.php?tid=916&c=0&d=0&t" alt="Een vriendelijke en bekwame docent">
            <img class="tutoring-info__tutor-card__img"
                data-src="/s3/profileImages/15741629335dd3d1f54b12a.png">
            <div class="tutoring-info__tutor-card__tutor-details">
                <h5>Sofi</h5>
                <span>Ontwerpen aan de TU Delft</span>
            </div>
        </a>
    </div>
    <div class="tutoring-info__tutor-card">
        <a href="https://bijlesaanhuis.nl/app/teacher-detailed-view.php?tid=901&c=0&d=0&t" alt="Een vriendelijke en bekwame docent">
            <img class="tutoring-info__tutor-card__img"
                data-src="/s3/profileImages/15735601135dca9f30dac92.png">
            <div class="tutoring-info__tutor-card__tutor-details">
                <h5>Stan</h5>
                <span>Journalistiek aan Erasmus</span>
            </div>
        </a>
    </div>
</div>