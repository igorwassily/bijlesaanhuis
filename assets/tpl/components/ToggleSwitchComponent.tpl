<div class="toggle-switch">
    {if isset($data.label) && !empty($data.label)}
        <label class="input-label {if isset($data.labelClass) && !empty($data.labelClass)}{$data.labelClass}{/if}" for="{$data.input.id}">
            {$data.label}
            {if isset($data.iconLabel) && !empty($data.iconLabel)}
                <span class="input-label__icon tippy" data-toggle="tooltip" data-placement="bottom" data-tippy-content="{$data.iconLabel}">
                    <i class="fas fa-{$data.iconType}"></i>
                </span>
            {/if}
        </label>
    {/if}
    <div class="toggle-switch__block">
        <a class="toggle-switch__button {if $data.input.default == 1}active{/if}" data-title="{$data.input.value}1">Ja</a>
        <a class="toggle-switch__button {if $data.input.default == 0}active{/if}" data-title="{$data.input.value}0">Nee</a>
    </div>
    <input class="toggle-switch__hidden" type="hidden" name="{$data.input.id}" id="{$data.input.id}" value="{$data.input.default}">
</div>