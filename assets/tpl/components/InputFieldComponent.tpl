<div class="input-field">
    {if isset($data.label) && isset($data.input.id)}
        <label class="input-label {if isset($data.labelClass) && !empty($data.labelClass)}{$data.labelClass}{/if}" for="{$data.input.id}">
            {$data.label}
            {if isset($data.iconLabel) && !empty($data.iconLabel)}
                <span class="input-label__icon tippy" data-toggle="tooltip" data-placement="bottom" data-tippy-content="{$data.iconLabel}">
                    <i class="fas fa-{$data.iconType}"></i>
                </span>
            {/if}
        </label>
    {/if}
    {if ({$data.input.type} == "textarea")}
        <textarea id="{$data.input.id}"
                  name="{$data.input.name}"
                  value="{$data.input.value}"
                  placeholder="{$data.input.placeholder}"
                  class="input-textarea" {if isset($data.input.maxlength) && !empty($data.input.maxlength)}
                  maxlength="{$data.input.maxlength}"{/if}
                  {if isset($data.input.messageRequired)}required{/if}></textarea>
    {elseif ({$data.type} == "checkbox")}
        <div class="input-field__checkbox-group">
            <ul id="{$data.input.id}" class="input-checkbox-list" {if isset($data.minSelected)}data-selected="{$data.minSelected}"{/if}>
                {foreach $data.boxes item=field name=field}
                    <li class="input-checkbox-list__item">
                        <label class="input-checkbox-list__label" for="{$field.name}">
                            <input class="input-checkbox-list__input"
                                   type="checkbox" id="{$field.id}"
                                   name="{$field.name}"
                                   {if isset($field.checked)}checked{/if}
                            >
                            {$field.label}
                        </label>
                    </li>
            {/foreach}
            </ul>
        </div>
    {elseif ({$data.input.type} == "datepicker")}
        <input id="{$data.input.id}"
               name="{$data.input.name}"
               type="text",
               value="{$data.input.value}",
               placeholder="{$data.input.placeholder}"
               class="input-{$data.input.type} {$data.input.class} {if isset($data.inputIcon) && !empty($data.inputIcon)}input--icon{/if}{if isset($data.inputIcon.iconStart) && !empty($data.inputIcon.iconStart)}-front{/if}"
                {if isset($data.input.min) && !empty($data.input.min)} min="{$data.input.min}" {/if}{if isset($data.input.max) && !empty($data.input.max)} max="{$data.input.max}" {/if}
                {if isset($data.input.messageRequired) && !empty($data.input.messageRequired)}required{/if}
        >
    {else}
        <input id="{$data.input.id}"
               name="{$data.input.name}"
               type="{$data.input.type}"
               value="{$data.input.value}"
               placeholder="{$data.input.placeholder}"
               class="input-{$data.input.type} {$data.input.class} {if isset($data.inputIcon) && !empty($data.inputIcon)}input--icon{/if}{if isset($data.inputIcon.iconStart) && !empty($data.inputIcon.iconStart)}-front{/if}"
               {if isset($data.input.min) && !empty($data.input.min)} min="{$data.input.min}" {/if}{if isset($data.input.max) && !empty($data.input.max)} max="{$data.input.max}" {/if}
               {if isset($data.input.messageRequired) && !empty($data.input.messageRequired)}required{/if}
        >
        {if isset($data.inputIcon) && !empty($data.inputIcon)}
            <span class="input__icon {if isset($data.inputIcon.iconStart) && !empty($data.inputIcon.iconStart)}input__icon--front{/if}">
                <i class="fas fa-{$data.inputIcon.icon}"></i>
            </span>
        {/if}
    {/if}
    {if isset($data.messageInfo) && !empty($data.messageInfo)}
        <label class="input-label--info">{$data.messageInfo}</label>
    {/if}
    {if isset($data.input.messageRequired) && !empty($data.input.messageRequired)}
        <label class="input-label--required">{$data.input.messageRequired}</label>
    {/if}
    {if isset($data.messageError) && !empty($data.messageError)}
        <label class="input-label--error">{$data.messageError}</label>
    {/if}
    {if isset($data.showPassword) && !empty($data.showPassword)}
        <div class="input-checkbox--password">
            <label class="input-label--checklabel">
                <input class="input-checkpassword" type="checkbox" name="checkpassword">
                Laat wachtwoord zien
            </label>
        </div>
    {/if}
</div>