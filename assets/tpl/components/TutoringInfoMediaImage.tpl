{if !$safari }
    <picture class="tutoring-info__image__media">
        {foreach $data.imageSrcSet as $srcSet}
        {if isset($srcSet.maxScreenSize)}
            {assign "sourceMedia" "(max-width: {$srcSet.maxScreenSize}px)"}
        {else}
            {assign "sourceMedia" "(min-width: {$srcSet.minScreenSize}px)"}
        {/if}
        <source media='{$sourceMedia}' srcset='{$srcSet.src}' sizes='(max-width: 256px) 100vw, 256px'>
        {/foreach}
        <img data-src="{$data.imageSrc}" alt="{$data.imageAltText}" style="width:auto;">
    </picture>
{else}
    <img data-src="{$data.imageSrc}" alt="{$data.imageAltText}" style="width:auto;">
{/if}