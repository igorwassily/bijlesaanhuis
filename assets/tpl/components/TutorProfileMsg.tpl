<div class="tutor-profile-msg tutor-profile-msg--full">
    <div class="tutor-profile-msg__title">Stuur Florian een berichtje</div>
    <div class="tutor-profile-msg__info">Hey ik ben Floriandadssadssas Ik sta graag voor je klaar om vragen te beantwoorden. Ik reageer zo snel mogelijk!</div>
    <textarea class="tutor-profile-msg__textarea" placeholder="Hey Floriandadssadssas, ik ben op zoek naar een bijlesdocent. Kunnen we kennismaken?"></textarea>
    <button id="tutor-profile-send" class="tutor-profile-msg__button">Bericht versturen</button>
</div>
<div id="send-message-modal" class="modal-wrapper">
    <div class="tutor-profile-msg tutor-profile-msg--modal">
        <div id="send-message-modal__close" class="modal-wrapper__close">X</div>
        <div class="tutor-profile-msg__title">Stuur Florian een berichtje</div>
        <div class="tutor-profile-msg__info">Hey ik ben Floriandadssadssas Ik sta graag voor je klaar om vragen te beantwoorden. Ik reageer zo snel mogelijk!</div>
        <textarea class="tutor-profile-msg__textarea" placeholder="Hey Floriandadssadssas, ik ben op zoek naar een bijlesdocent. Kunnen we kennismaken?"></textarea>
        <button id="tutor-profile-send--modal" class="tutor-profile-msg__button">Bericht versturen</button>
    </div>
</div>