{extends file="components/WrapperCardComponent.tpl"}

{block name="wrapper-card-header"}
  <h3>Berichten</h3>
{/block}

{block name="wrapper-card-body"}
  <div class="message__list">
    {foreach $data.messagesList item=message name="messages"}    
      {include "components/MessageComponent.tpl" data=$message}
    {foreachelse}
      <div class="message__empty">
        <p class="message__empty-title">Geen breichten</p>
        
        {if $user.student}
          <p>
            Bijles is de eerste stap naar betere cijfers!
          </p>
          <div class="message-button-wrapper">
            <a class="message-button" href="todo-add-check-for-students" role="link" title="Vind docent">Vind docent</a>
          </div>
        {/if}
        
      </div>
    {/foreach}
  </div>
{/block}