<div class="dashboard-appointment">
  <div class="dashboard-appointment__item">
    <div class="dashboard-appointment__date">
      <p class="date-day">{$data.dateDay}</p>
      <p>{$data.dateMonth}</p>
      <p>{$data.dateYear}</p>
    </div>

    <div class="dashboard-appointment__info">
      <ul role="list">
        <li role="listitem"><i class="icon-semicircle">U</i></li>
        <li role="listitem"><i class="fa fa-clock"></i>{$data.timeStart}-{$data.timeEnd}</li>
        <li role="listitem"><i class="fa fa-phone-alt"></i>{$data.phoneNumber}</li>
        <li role="listitem"><i class="fa fa-envelope"></i><a href="mailto:{$data.email}" role="link">{$data.email}</a></li>
        
        {if isset($data.location)}
          <li role="listitem"><i class="fa fa-location-arrow"></i> {$data.location}</li>
        {/if}
      </ul>
    </div>
  </div> 
</div> 