<div class="question-collection__container">
    <div class="question-collection__text">
        <div class="question-collection__text__seperator-title">
            {if (isset($data.seperatorTitle))}
                {$data.seperatorTitle}
            {else}
                &nbsp;
            {/if}
        </div>
        {if (isset($data.title))}
            <div class="question-collection__text__title">
                {$data.title}
            </div>
        {/if}
    </div>
    {foreach from=$data.contents item=questionCard name=card }
        
	    {include "../components/QuestionCollectionCard.tpl" data=$questionCard}

    {/foreach}
</div>