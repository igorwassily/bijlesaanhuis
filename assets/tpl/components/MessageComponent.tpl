<a class="message-item" role='link' href={$data.chatURL}>
  {if !empty($data.contactImage)}
    <img src="{$data.contactImage}" class="message-profile-image" />
  {else}
    <div class="message-profile-default-image">
      <i class="fa fa-user"></i>
    </div>
  {/if}

  <div class="message-body">
    <div class="message-row">
      <p class="message-name">
        <span>
          {$data.contactForename}
        </span>
        
        <span class="message-new-label">
          Nieuw
        </span>
      </p>

      <time class="message-date">
        {$data.messageDate}
      </time>
    </div>

    <div class="message-row">
      <p class="message-contact-type">
        {$data.contactType}
      </p>

      <p class="message-text">
        {$data.message}
      </p>
    </div>
  </div>
</a>
