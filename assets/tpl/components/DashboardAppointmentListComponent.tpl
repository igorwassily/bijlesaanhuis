{extends file="components/WrapperCardComponent.tpl"}

{block name="wrapper-card-header"}
  <h3>Toekomstige afspraken</h3>
{/block}

{block name="wrapper-card-body"}
  <div class="dashboard-appointment__list">
    {foreach $data.dashboardAppointmentList item=appointment name="appointments"}    
        {include "components/DashboardAppointmentComponent.tpl" data=$appointment}
    {foreachelse}
      <div class="dashboard-appointment__empty">
        <p>Geen toekomstige afspraken</p>
        
          <div class="dashboard-appointment-button-wrapper">
            <a class="dashboard-appointment-button" href="todo-add-check-for-students" role="link" title="Vind docent">Vind docent</a>
          </div>
        {if $user.student}
        {/if}
      </div>
    {/foreach}
  </div>
{/block}