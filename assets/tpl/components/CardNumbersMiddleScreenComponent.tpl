{if $data.numbers}
	<div class="cards__card-number-tablet">
		<div class="cards__card-divider"></div>
		<div class="cards__card-number">
    		<span>{$cardIteration}</span>
		</div>
	</div>
{/if}