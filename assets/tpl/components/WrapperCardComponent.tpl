<div class="wrapper-card">
    <div class="wrapper-card__header">
        {block name="wrapper-card-header"}{/block}
    </div>
    <div class="wrapper-card__body">
        {block name="wrapper-card-body"}{/block}
    </div>
</div>
