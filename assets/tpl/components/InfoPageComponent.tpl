<form>
  {if (isset($data.title))}
  <div class="info-page__header">
    <p>
      {$data.title}
    </p>
  </div>
  {/if}

  <div class="info-page__body">
    <p>
      {$data.description}
    </p>

    {if (isset($data.inputSubmitButton))}
    {include "components/InputSubmitButtonComponent.tpl" data=$data.inputSubmitButton}
    {/if}
  </div>

  {if (isset($data.footerContent))}
  <div class="info-page__footer">
    {if isset($data.footerContent.description)}
    <p>{$data.footerContent.description}</p>
    {/if}

    {if isset($data.footerContent.links)}
    <div>
      {foreach $data.footerContent.links item=link name=link}

      {if !$link@first}
      <span class="link-divider">
        &nbsp;/&nbsp;
      </span>
      {/if}

      <a href="{$link.link}">{$link.text}</a>
      {/foreach}
    </div>
    {/if}
  </div>
  {/if}
</form>