<div class="tutor-profile-info">
    <div class="tutor-profile-info__subsection tutor-profile-info__subsection--top">
        <img class="tutor-profile-image" src="https://bijlesaanhuis.nl/app/assets/images/profile_av.jpg" alt="Tutor Profile Image">
        <div class="tutor-profile-details">
            <div class="tutor-profile-title">
                <div class="tutor-profile-title__name">{$data.name}</div>
                <div class="tutor-profile-title__level">{$data.level} <i class="fa tutor-profile-icon--gray tippy" data-tippy-content="Floriandadssadssas Is een Supreme docent. Voor Supreme docenten garanderen wij onze hoogst mogelijke kwaliteit. Het voordeel van een Supreme docent is dat de docent een jarenlange ervaring heeft in het geven van onderwijs. Floriandadssadssas is zorgvuldig geselecteerd op basis van de best mogelijke didactische vaardigheden en vakspecifieke kennis." tabindex="0">&#xf05a;</i></div>
            </div>
            <div class="tutor-profile-subjects">
                <i class="tutor-profile-icon--green fas fa-book"></i>Frans, Engels, Bedrijfseconomie, Biologie, Duits, Economie, Aardrijkskunde, Geschiedenis, Rekenen, Latijn, M&O, Natuurkunde, Wiskunde onderbouw, Wiskunde A/C, Wiskunde D en Nask
            </div>
            <div class="tutor-profile-price">
                <i class="tutor-profile-icon--green fas fa-dollar-sign"></i>€ 24.00 per lesuur
            </div>
        </div>
    </div>
    <div class="tutor-profile-info__subsection">
        <div class="tutor-profile-subtitle">Over Florian</div>
        <div class="tutor-profile-furtherinfo">
            <p class="tutor-profile-furtherinfo__text visible">{$data.shortDesc}... <span class="tutor-profile-furtherinfo__more">meer tonen</span></p>
            <p class="tutor-profile-furtherinfo__text">{$data.longDesc} <span class="tutor-profile-furtherinfo__more">minder tonen</span></p>
        </div>
    </div>
</div>