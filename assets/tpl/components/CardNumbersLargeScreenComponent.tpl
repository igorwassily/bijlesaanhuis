{if $data.numbers}
	<div class="cards__section-container cards__card-number__container">    
		{foreach $data.content item=card name=card}
			<div class="cards__card-number-desktop">
				<div class="cards__card-divider"></div>
            	<div class="cards__card-number">
					<span>{$smarty.foreach.card.iteration}</span>
				</div>
			</div>
		{/foreach}
	</div>
{/if}