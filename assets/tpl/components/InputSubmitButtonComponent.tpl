<div class="{if isset($data.maxWidth) && $data.maxWidth} input-submit-button--max-width {else} {/if}">
    <input id="{$data.input.id}" name="{$data.input.name}" type="submit" value="{$data.input.value}" class="input-submit-button {$data.input.class}" />
</div>