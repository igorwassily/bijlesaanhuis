<video controls="" class="video-content lazy">
    {foreach $data.videoSrcSet item=srcSet}
        {$fileType = explode('.', $srcSet.src)}
        {$fileType = end($fileType)}
        <source type="video/{$fileType}" data-src="{$srcSet.src}"> 
    {/foreach}
</video>