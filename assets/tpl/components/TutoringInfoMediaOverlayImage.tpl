<div class="tutoring-info__video__overlay">
    <i class="tutoring-info__video__overlay-icon far fa-play-circle"></i>
    <img class="tutoring-info__video__overlay-img" data-src="{$data.overlayImageSrc}">
</div>
{* <div class="tutoring-info__video__overlay" data-src="{$data.overlayImageSrc}" style="background-image:url({$data.overlayImageSrc})"></div> *}