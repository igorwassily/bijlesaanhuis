{extends file="components/WrapperCardComponent.tpl"}

{block name="wrapper-card-header"}
  <h3>Bijles Onboeken</h3>
{/block}


{block name="wrapper-card-body"}
  {foreach $data.studentList item=student key=i name="students"}    
    <div class="student-list__card">
      {include "components/CardStudentListComponent.tpl" data=$student}
    </div>
  {foreachelse}
    <p class="student-list__empty">Geen leerlingen</p>
  {/foreach}
{/block}