<div class="question-collection__question-wrapper">
    <h4 class="question-collection__question--title">
        {if (isset($data.title))}
            <span class="question-collection__question--title-text">
                {$data.title}
            </span>
        {/if}
        <span class="question-collection__question--title-icon"><i class="fas fa-plus-circle"></i></span>
    </h4>
    <div class="question-collection__question--description" style="display:none;">
        {if (isset($data.description))}
            {$data.description}
        {/if}
    </div>
</div>