    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=3.5">
	{* <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport"> *}
	<meta name="description" content="{$page.description}">

	<meta name="robots" content="max-snippet:-1, max-image-preview:large, max-video-preview:-1">
	<meta property="og:locale" content="en_US">
	<meta property="og:type" content="article">
	<meta property="og:title" content="{$page.socialMediaTitle}">
	<meta property="og:description" content="{$page.socialMediaDescription}">
	<meta property="og:url" content="https://"/>
	<meta property="og:site_name" content="Bijles Aan Huis">
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:description" content="{$page.socialMediaDescription}">
	<meta name="twitter:title" content="{$page.socialMediaTitle}">

	<title>{$page.title}</title>

	{* <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script> *}
	<link rel="icon" href="/s3/content/cropped-Bijles-Wiskunde-32x32.png" sizes="32x32" />
	{* <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-select@1.13.9/dist/css/bootstrap-select.min.css"> *}
	{* <link rel="stylesheet" href="/app/assets/plugins/waitme/waitMe.css"> *}
	{* <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css"> *}
	{* <link href="/app/template/plugins/font-awesome/css/all.min.css" rel="stylesheet"> *}
	<link href="https://fonts.googleapis.com/css?family=Poppins&display=swap" rel="stylesheet">
	{* <script src="/app/assets/plugins/jquery-validation/jquery.validate.js" defer></script> *}

	{* {if !$signedIn}
		<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/flatpickr/dist/flatpickr.min.css">
	{/if} *}

{* <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script> *}

	{* <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
	      integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous"> *}

	{* <link rel="stylesheet" href="/app/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.css"> *}
	{* <link rel="stylesheet" href="https://netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> *}
	{* <link rel="stylesheet" href="{$pathTpl}/css/main.css"> *}

	{* content of old mainCSSFiles.php *}


	{* <link rel="stylesheet" href="/app/assets/plugins/bootstrap/css/bootstrap.min.css"> *}
	{* <link rel="stylesheet" href="/app/assets/plugins/dropzone/dropzone.css">
	<link rel="stylesheet" href="/app/assets/plugins/jvectormap/jquery-jvectormap-2.0.3.min.css"/>
	<link rel="stylesheet" href="/app/assets/plugins/morrisjs/morris.min.css" /> *}
	<!-- JQuery DataTable Css -->
	{* <link rel="stylesheet" href="/app/assets/plugins/jquery-datatable/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" href="https://wotcnow.com/pm/vendors/datatables.net-responsive-bs/css/responsive.bootstrap.min.css"> *}

<!-- Latest compiled and minified CSS -->

	<!-- Bootstrap Select Css -->
	{* <link href="/app/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet"/> *}
	{* <link rel="stylesheet" href="/app/assets/css/bootstrap-multiselect.css" type="text/css"> *}

	<!-- Custom Css -->
	{* <link rel="stylesheet" href="/app/assets/css/main.css"> *}
	{* <link rel="stylesheet" href="/app/assets/css/color_skins.css"> *}

	<!-- Start header.php -->
	{* <link href="/wp-content/themes/Divi/style.css?ver=3.19.18" rel="stylesheet"> *}

	{if !$signedIn}
		{* <link rel='stylesheet'  href='/wp-content/themes/Divi/css/modal.css' type='text/css' /> *}
		{* <script src="/app/assets/js/modal.js" defer></script> *}
		<!-- datepicker test -->
	{/if}

	{* <link href='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.css' rel='stylesheet' />
	<link rel='stylesheet' href='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.css' type='text/css' /> *}
	<!-- TODO: Put into header component -->
	{* <link rel="stylesheet" href='/app/template/css/page-header.css'/> *}
	<!-- End header.php -->