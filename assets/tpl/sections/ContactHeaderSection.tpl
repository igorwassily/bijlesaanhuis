<section class="contact-header__section">
    <div class="contact-header__container">
        <div class="contact-header__text">
            {if (isset($data.title))}
                <div class="contact-header__text__title">
                    {$data.title}
                </div>
            {/if}
            <div class="contact-header__details">
                <div class="contact-header__detail">
                    <label class="contact-header__detail--label">E-mail:</label>
                    <div class="contact-header__detail--text"><a style="color: #486066;" href="mailto:{$data.email}">{$data.email}</a></div>
                </div>
                <div class="contact-header__detail">
                    <label class="contact-header__detail--label">Telefoonnummer:</label>
                    <div class="contact-header__detail--text"><a style="color: #486066;" href="tel:{$data.phone}">{$data.phone}</a></div>
                </div>
                <div class="contact-header__detail">
                    <label class="contact-header__detail--label">WhatsApp:</label>
                    <div class="contact-header__detail--text">{$data.whatsapp} <a href="{$data.whatsappLink}" style="font-weight:300;"> of <span style="color: #5cc75f;">klik op deze link</span></a></div>
                </div>
                <div class="contact-header__detail">
                    <label class="contact-header__detail--label">Chat:</label>
                    <div class="contact-header__detail--text">Het groene icoontje rechtsonder</div>
                </div>
            </div>
        </div>
    </div>
</section>