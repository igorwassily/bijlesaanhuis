{extends file="sections/CardParentSection.tpl"}

{block name="cardContent"}
	{if isset($card.imgSrc) && !empty($card.imgSrc)}
		<div class="card-basic-section__card__img">
            {if !$safari}
				<picture>
                    {if isset($card.imgSrcSet)}
                        {foreach $card.imgSrcSet as $srcSet}
	                        {if isset($srcSet.maxScreenSize)}
	                            {assign "sourceMedia" "(max-width: {$srcSet.maxScreenSize}px)"}
	                        {else}
								{assign "sourceMedia" "(min-width: {$srcSet.minScreenSize}px)"}
	                        {/if}
							<source media='{$sourceMedia}' srcset='{$srcSet.src}'
							        sizes='(max-width: 256px) 100vw, 256px'>
                        {/foreach}
                    {/if}

					<img data-src="{$card.imgSrc}" alt="{$card.imgAltText}" style="width:auto;">
				</picture>
            {else}
				<img data-src="{$card.imgSrc}" alt="{$card.imgAltText}" style="width:auto;">
            {/if}
		</div>
    {/if}
    {if isset($card.title)}
		<div class="card-basic-section__card__title">
            {$card.title}
		</div>
    {/if}
    {if isset($card.description)}
		<div class="card-basic-section__card__description">
            {$card.description}
		</div>
    {/if}
{/block}