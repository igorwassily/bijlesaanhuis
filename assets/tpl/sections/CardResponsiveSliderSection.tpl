{extends file="sections/CardParentSection.tpl"}

{block name="topContent"}
	{include "../components/CardNumbersLargeScreenComponent.tpl" data=$data}
{/block}

{block name="aboveCardContent"}
	{include "../components/CardNumbersMiddleScreenComponent.tpl" data=$data}
{/block}

{block name="cardContent"}
    {if isset($card.imgSrc) && !empty($card.imgSrc)}
		<div class="card-responsive-slider__card__img">
            {if !$safari}
				<picture>
                    {if isset($card.imgSrcSet)}
                        {foreach $card.imgSrcSet as $srcSet}
	                        {if isset($srcSet.maxScreenSize)}
	                            {assign "sourceMedia" "(max-width: {$srcSet.maxScreenSize}px)"}
	                        {else}
	                            {assign "sourceMedia" "(min-width: {$srcSet.minScreenSize}px)"}
	                        {/if}
							<source media='{$sourceMedia}' srcset='{$srcSet.src}'
									sizes='(max-width: 256px) 100vw, 256px'>
                        {/foreach}
                    {/if}

					<img data-src="{$card.imgSrc}" alt="{$card.title}" style="width:auto;">
				</picture>
            {else}
				<img data-src="{$card.imgSrc}" alt="{$card.title}" style="width:auto;">
            {/if}
		</div>
    {/if}
    {if isset($card.title)}
		<div class="card-responsive-slider__card__title">
            {$card.title}
		</div>
    {/if}
    {if isset($card.description)}
		<div class="card-responsive-slider__card__description">
            {$card.description}
		</div>
    {/if}
{/block}
{block name="bottomContent"}
	<div class="cards__section-container card-responsive-slider__slider">
        <div class="review__comment">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card-responsive-slider-autoplay__carousel">
                        {foreach $data.content item=card name=card}
							{assign "cardIteration" "{$smarty.foreach.card.iteration}"}
							<div class="card-responsive-slider__card-wrapper">
								<div class="card-responsive-slider__card">
									<div class="card-responsive-slider__card__content">
            	            			{if isset($card.imgSrc) && !empty($card.imgSrc)}
											<div class="card-responsive-slider__card__img">
                    	    	    	    	{if !$safari}
													<picture>
	        		                                	{if isset($card.imgSrcSet)}
    		        	                                	{foreach $card.imgSrcSet as $srcSet}
	                    			                        	{if isset($srcSet.maxScreenSize)}
	                                			                	{assign "sourceMedia" "(max-width: {$srcSet.maxScreenSize}px)"}
					                                            {else}
	            				                                    {assign "sourceMedia" "(min-width: {$srcSet.minScreenSize}px)"}
	                            				                {/if}
																<source media='{$sourceMedia}' srcset='{$srcSet.src}'
															        sizes='(max-width: 256px) 100vw, 256px'>
            				                                {/foreach}
                            				            {/if}

														<img data-src="{$card.imgSrc}" alt="{$card.title}" style="width:auto;">
													</picture>
                                				{else}
													<img data-src="{$card.imgSrc}" alt="{$card.title}" style="width:auto;">
                				                {/if}
											</div>
                				        {/if}
				                        {if isset($card.title)}
											<div class="card-responsive-slider__card__title">
                                				{$card.title}
											</div>
                        				{/if}
				                        {if isset($card.description)}
											<div class="card-responsive-slider__card__description">
				                                {$card.description}
											</div>
				                        {/if}
										{include "../components/CardNumbersSmallScreenComponent.tpl" data=$data}
									</div>
								</div>
							</div>
						{/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
{/block}
