<section class="card-topic-section__section">
    {if $data.title}
		<div class="card-topic-section__section-container">
			<h1 class="card-topic-section__title">
				{$data.title}
			</h1>
		</div>
    {/if}
	<div class="card-topic-section__section-container card-topic-section__container">
        {foreach $data.content item=card name=card}
			<div class="card-topic-section__card-wrapper">
				{if isset($card.link) && !empty($card.link)}
					<a class="card-topic-section__card-link" href="{$card.link}">
				{/if}
					<div class="card-topic-section__card">
						<div class="card-topic-section__card__content">
                        	{if isset($card.imgSrc) && !empty($card.imgSrc)}
								<div class="card-topic-section__card__img">
									<img data-src="{$card.imgSrc}" alt="{$card.title}" style="width:auto;">
								</div>
    	                    {/if}
        	                {if isset($card.title)}
								<div class="card-topic-section__card__title">
                	                {$card.title}
								</div>
                        	{/if}
                        	{if isset($card.description)}
								<div class="card-topic-section__card__description">
                	                {$card.description}
								</div>
        	                {/if}
						</div>
					</div>
				{if isset($card.link) && !empty($card.link)}
					</a>
				{/if}
			</div>
		{/foreach}
	</div>
</section>
