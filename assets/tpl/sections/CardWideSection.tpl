<section class="card-wide-section__section">
    {if $data.title}
		<div class="card-wide-section__section-container">
			<h1 class="card-wide-section__title">
				{$data.title}
			</h1>
		</div>
    {/if}
	<div class="card-wide-section__section-container card-wide-section__container">
        {foreach $data.content item=card name=card}
			<div class="card-wide-section__card-wrapper">
                <div class="card-wide-section__card">
					<div class="card-wide-section__card__content">
                        {if isset($card.imgSrc) && !empty($card.imgSrc)}
							<div class="card-wide-section__card__img">
                                <img data-src="{$card.imgSrc}" alt="{$card.title}" style="width:auto;">
							</div>
                        {/if}
                        {if isset($card.description)}
							<div class="card-wide-section__card__description">
                                {$card.description}
							</div>
                        {/if}
						<div class="card-wide-section__card__titles-wrapper">
    	                    {if isset($card.title)}
								<div class="card-wide-section__card__title">
                            	    {$card.title}
								</div>
                    	    {/if}
                	        {if isset($card.subtitle)}
								<div class="card-wide-section__card__subtitle">
        	                        {$card.subtitle}
								</div>
	                        {/if}
						</div>
					</div>
				</div>
			</div>
        {/foreach}
	</div>
</section>