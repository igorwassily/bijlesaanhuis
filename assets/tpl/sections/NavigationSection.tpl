<nav id="top-nav" class="navbar navbar-expand navbar-primary">
    <div class="w-100 order-1 order-md-0 d-none d-sm-inline-block">
        <a class="nav-link top-nav__phone-number" href="tel:+31851303558"><i class="fas fa-phone-alt"></i> 085-13<span>03558</span></a>
    </div>
    <div class="mx-auto order-0 d-sm-none">
        <a class="nav-link top-nav__phone-number" href="tel:+31851303558"><i class="fas fa-phone-alt"></i> 085-13<span>03558</span></a>
    </div>
    <div class="order-3 d-none d-sm-inline-block">
        <ul class="navbar-nav ml-auto">
            <li class="nav-item">
                <a class="nav-link" href="/over-ons/">Over ons</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/veelgestelde-vragen/">Veelgestelde vragen</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="/contact/">Contact</a>
            </li>
        </ul>
    </div>
</nav>
<nav id="page-nav" class="navbar navbar-expand-lg navbar-light bg-white">
    <a class="navbar-brand" href="/">
        <img alt="Bijlesaanhuis Startpage" src="/s3/content/Bijles-e1551529964908.png">
    </a>
    <div class="navbar-collapse collapse w-100 order-3 dual-collapse2">
        
        <ul class="navbar-nav ml-auto">
            {* <li class="nav-item"><a class="nav-link" href="/">Home</a></li> *}
        {if !$user.teacher}
            <li class="nav-item"><a class="nav-link" href="/app/teachers-profile2">Vind docent</a></li>
       
            {if !$signedIn}
                <li class="nav-item"><a class="nav-link" href="/prijzen/">Tarieven</a></li>
            {/if}
        {/if}
        {if $signedIn} 
            <li class="nav-item"><a class="nav-link" href="{if $user.teacher}/app/teachers-dashboard{else}/app/students-dashboard{/if}">Kalender</a></li>
            <li class="nav-item"><a class="nav-link" href="{if $user.teacher}/app/teachers-appointment{else}/app/students-appointment{/if}">Afspraken
                {if $data.count > 0}
                    <div class="nav-item__message">
                        <span class="nav-item__message--badge badge badge-danger">&nbsp;{$data.count}&nbsp;</span>
                    </div>
                {/if}
                </a>
            </li>
            <li class="nav-item"><a class="nav-link" href="/app/message">Berichten
                {if $data.messages > 0}
                    <div class="nav-item__message">
                        <span class="nav-item__message--badge badge badge-danger">&nbsp;{$data.messages}&nbsp;</span>
                    </div>
                {/if}
                </a>
            </li>
        {/if}  
        {if $user.teacher}
            <li class="nav-item"><a class="nav-link" href="/app/student-list">Bijles inboeken</a></li>
        {/if}
		{if !$signedIn}
              <li class="dropdown initials dropdown-default">
                <div class="dropdown__hover-effect"></div>
              <a href="#" class="nav-link dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    <span class='d-sm-inline'>Werken bij</span>
                    <i class='ma-caret fas fa-angle-down mob-caret'></i><i class='fas fa-times ma-close mob-caret'></i>
                </a> 
                <ul class="dropdown-menu"> 
                    <li class="nav-item"><a class="nav-link" href="/bijles-geven/">
                        Bijles geven
                    </a></li>
                     <li class="nav-item"><a class="nav-link" href="/vacatures/">
                        Vacatures
                    </a></li>
			</ul>
        </li>
            <li class="nav-item"><a class="nav-link PopupBtn inloggen">Account</a></li>  
		{else}
        {* <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle profilepic {if $user.student} circle {/if}" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Dropdown
                    {if $user.teacher}
                        <span class="meer-mob">Meer</span>
                        <img class="profile-picture" src="{$user.image}" alt="User" style="height:56px; border-radius: 100%;top:0;"><i class="ma-caret zmdi zmdi-caret-down"></i><i class="zmdi zmdi-close ma-close"></i>
                    {elseif $user.student}
                        <span class='meer-mob'>Meer</span>
                        <i class='ma-caret zmdi zmdi-caret-down mob-caret'></i><i class='zmdi zmdi-close ma-close mob-caret'></i>
                        <span class='profile-picture d-none d-sm-inline'>{$user.initials}</span>
                    {/if}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                <a class="dropdown-item" href="#">Action</a>
                <a class="dropdown-item" href="#">Another action</a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#">Something else here</a>
            </div>
        </li> *}
            <li class="dropdown initials {if $user.student}dropdown-student{/if}">
                <div class="dropdown__hover-effect"></div>
              <a href="#" class="nav-link dropdown-toggle disabled profilepic {if $user.student} circle {/if}" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">
                    {if $user.teacher}
                        <span class="meer-mob d-lg-none">Meer</span>
                        <img class="profile-picture d-none d-lg-inline-block" src="{$data.image}" alt="User" style="height:56px; border-radius: 100%;top:3px;"><i class="ma-caret zmdi zmdi-caret-down"></i><i class="zmdi zmdi-close ma-close"></i>
                    {elseif $user.student}
                        <span class='meer-mob d-lg-none'>Meer</span>
                        <i class='ma-caret fas fa-angle-down mob-caret d-lg-none'></i><i class='zmdi zmdi-close ma-close mob-caret'></i>
                        <span class='profile-picture d-none d-lg-inline-block'>{$user.initials}</span>
                    {/if}
                </a> 
                <ul class="dropdown-menu"> 
				{if $user.teacher}
                    <li class="nav-item"><a class="nav-link" href="/app/teacher-detailed-view.php?tid={$user.id}">
                    <i class='fas fa-user-circle'></i>Openbaar profiel
                    </a></li>
                    <li class="nav-item"><a class="nav-link" href='/app/teachers-slotcreation' >
					<i class='fas fa-plus-circle'></i>Algemene beschikbaarheid
                    </a></li>
                    <li class="nav-item"><a class="nav-link" href='/app/edit-profile' >
					<i class='fas fa-pen'></i>Profiel aanpassen
                    </a></li>
                    <li class="nav-item"><a class="nav-link" href='/app/logout'>
					<i class='fas fa-sign-in-alt'></i>Uitloggen
					</a></li>
                {elseif $user.student}
                    <li class="nav-item"><a class="nav-link" href='/app/student-edit-profile' >
					<i class='fas fa-user-circle'></i> Profiel aanpassen
                    </a></li>
                    <li class="nav-item"><a class="nav-link" href='/app/logout'>
					<i class='fas fa-sign-in-alt'></i>Uitloggen
					</a></li>
                {else}
                    <a class="nav-link" class='PopupBtn'>
					    Login
					</a>
				{/if}
				</li> 
			</ul>
        </li>
		{/if}
		
        </ul>
    </div>
    <button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target=".dual-collapse2" aria-label="Mobile Menu Button">
        <span class="navbar-toggler-icon"></span>
    </button>
</nav>

