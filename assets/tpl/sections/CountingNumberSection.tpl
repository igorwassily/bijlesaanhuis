<section class="counting-number__section">
    <div class="counting-number__container">
        <div class="counting-number__text">
            {if (isset($data.title))}
                <div class="counting-number__text__title">
                    <span class="counting-number__animate" data-number="{$data.title}">{$data.title}</span>{if (isset($data.unit))}{$data.unit}{/if}
                </div>
            {/if}
            {if (isset($data.subtitle))}
                <div class="counting-number__text__subtitle">
                    {$data.subtitle}
                </div>
            {/if}
        </div>
    </div>
</section>