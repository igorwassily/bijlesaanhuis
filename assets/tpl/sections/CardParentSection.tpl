<section class="cards__section">
    <div class="cards__section-container">
		{if $data.title}
			<h1 class="cards__title">
				{$data.title}
			</h1>
    	{/if}
        {if (isset($data.titleImgSrc))}
            <img class="cards__title-image" src="{$data.titleImgSrc}" alt="{$data.titleImgAltText}"
                 sizes="(max-width: 136px) 100vw, 136px" />
        {/if}
        {if (isset($data.subtitle))}
            <p class="cards__subtitle">{$data.subtitle}</p>
        {/if}
	</div>

	{block name="topContent"}{/block}

	<div class="cards__section-container cards__container">
        {foreach $data.content item=card name=card}
			<div class="cards__card-wrapper">

				{assign var="card" value=$card} 
				{assign "cardIteration" "{$smarty.foreach.card.iteration}"}
				{block name="aboveCardContent"}{/block}

				<div class="cards__card">
					<div class="cards__card__content">

                        {block name="cardContent"}{/block}

					</div>
				</div>
			</div>
        {/foreach}
	</div>

	{block name="bottomContent"}{/block}

</section>
