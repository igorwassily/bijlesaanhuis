<section class="text-info__section" style="background-color:{if isset($data.sectionBackgroundColor) && !empty($data.sectionBackgroundColor)} {$data.sectionBackgroundColor} {else} white {/if}">
    <div class="text-info__container">
        <div class="text-info__text">
            {if isset($data.title)}
                <div class="text-info__text__title">
                    {$data.title}
                </div>
            {/if}
            {if isset($data.subtitle)}
                <div class="text-info__text__subtitle">
                    {$data.subtitle}
                </div>
            {/if}
            {if isset($data.description)}
                <div class="text-info__text__description">
                    {$data.description}
                </div>
            {/if}
        </div>
    </div>
</section>