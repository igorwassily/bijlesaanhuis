<section class="searchbar-header__section">
    {* <link href="/app/assets/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" /> *}
    <div class="searchbar-header__section__background" data-src="/s3/content/shutterstock_1044241258-5-min-min-1.jpg"></div>
    <div class="searchbar-header__container">
        <div class="searchbar-header__text">
            {if (isset($data.title))}
                <div class="searchbar-header__text__title">
                    {$data.title}
                </div>
            {/if}
            {if (isset($data.description))}
                <div class="searchbar-header__text__description">
                    {$data.description}
                </div>
            {/if}
        </div>
        <div class="searchbar-header__searchbar">
            <div class="searchbar-header__searchbar-content" role="search" method="POST" id="searchform" style="width: 100%;">
                <!-- INLINE STYLE JUST FOR DEV PURPOSES -->
                <div id="schooltypediv" class="searchbar-header__searchbar__input-wrapper school-type col-12 col-md-6 col-lg-4">
                    <i class="fas fa-university"></i>
                    <select class="selectpicker form-control show-tick school-type searchbar-header__searchbar__input placeholder" data-size="2" data-dropup-auto="false" id="school-type" name="schoolType_h" required data-title="Selecteer schooltype">
                        <option hidden>Selecteer schooltype</option>
                        {foreach $data.schoolTypes as $s}
                            <option value="{$s.typeID}">{$s.typeName}</option>
                        {/foreach}
                    </select>
                </div>
                <div id="schoolcoursediv" class="searchbar-header__searchbar__input-wrapper school-subjects col-12 col-md-6 col-lg-4">
                    <i class="fas fa-book"></i>
                    <select class="selectpicker form-control show-tick school-course update-onchange update-onchange-subject searchbar-header__searchbar__input" data-dropup-auto="false" multiple required name="schoolSubject_h" id="school-course" data-size="6" data-none-selected-text="Selecteer schoolvak">
                        <option hidden>Selecteer schoolvak</option>
                    </select>
                </div>
                <div id="schoolleveldiv" class="searchbar-header__searchbar__input-wrapper school-level col-12 col-md-6 col-lg-2">
                    <i class="fas fa-calendar-alt"></i>
                    <select class="selectpicker form-control show-tick school-level update-onchange searchbar-header__searchbar__input" data-dropup-auto="false" required id="school-level" name="schoolLevel_h" data-title="Selecteer niveau">
                        <option hidden>Selecteer schoollevel</option>
                        {foreach $data.schoollevel as $s}
                            <option value="{$s.levelID}">{$s.levelName}</option>
                        {/foreach}
                    </select>
                </div>
                <div id="schoolyeardiv" class="searchbar-header__searchbar__input-wrapper school-year col-12 col-md-6 col-lg-2">
                    <i class="fas fa-bars"></i>
                    <select class="selectpicker form-control show-tick school-year update-onchange searchbar-header__searchbar__input" data-dropup-auto="false" required id="school-year" name="schoolYear_h" data-title="Selecteer schooljaar">
                        <option hidden>Selecteer schooljaar</option>
                        {foreach $data.schoolTypes as $s}
                            <option value="{$s.yearID}">{$s.yearName}</option>
                        {/foreach}
                    </select>
                </div>
                <div id="postcodediv" class="searchbar-header__searchbar__input-wrapper postcode col-12 col-lg-2">
                    <input type="text" class="searchbar-header__searchbar__input" name="postcode" placeholder="Postcode" value="" id="postcode3" />
                    <input type="hidden" id="latitude" name="latitude" value="" style="display:none;" />
                    <input type="hidden" id="longitude" name="longitude" value="" style="display:none;" />
                    <input type="hidden" id="place_name3" name="place_name" value="" style="display:none;" />
                </div>
                <div id="submitdiv" class="searchbar-header__searchbar__input-wrapper js-search-button col-12 col-lg-2">
                    <button type="submit" id="submission" name="submission" class="searchbar-header__searchbar__input searchbar-header__searchbar__input-button">Zoeken</button>
                </div>
            </div>
        </div>
    </div>
</section>