<section class="contact-formular-small-small__section">
    <div class="contact-formular-small__container">
        <div class="contact-formular-small__text">
            <div class="contact-formular-small__text__title">
                Direct solliciteren?
            </div>
            <div class="contact-formular-small__text__description">
                Vul het onderstaande contactformulier in. Neem voor vragen gerust eerst <a href="/contact">hier</a> of via de chat contact met ons op.
            </div>
        </div>
        <form method="post">
            <div class="contact-formular-small__header__email">
                <input type="text" class="contact-formular-small__header__email-input" name="name" placeholder="Naam" required {if !empty($data.form.name)}value="{$data.form.name}"{/if} />
                <input type="text" class="contact-formular-small__header__email-input" name="email" placeholder="E-mailadres" required {if !empty($data.form.email)}value="{$data.form.email}"{/if} />
                <input type="text" class="contact-formular-small__header__email-input" name="phone" placeholder="Telefoonnummer" required {if !empty($data.form.phone)}value="{$data.form.phone}"{/if} />
                <input type="text" class="contact-formular-small__header__email-input" name="message" placeholder="Link Linkedin profiel" required {if !empty($data.form.message)}value="{$data.form.message}"{/if} />
            </div>
            <div class="contact-formular-small__button-wrapper">
                <input class="contact-formular-small__text__button" type="submit" name="subContactSmall" value="VERSTUUR" />
            </div>
        </form>
    </div>
</section>