<section class="card-slider__section">
    <div class="card-slider__section-container card-slider__title-container">
		{if $data.title}
			<h1 class="card-slider__title">
				{$data.title}
			</h1>
    	{/if}
        {if (isset($data.titleImgSrc))}
            <img class="card-slider__title-image" src="{$data.titleImgSrc}" alt="{$data.titleImgAltText}"
                 sizes="(max-width: 136px) 100vw, 136px" />
        {/if}
        {if (isset($data.subtitle))}
            <p class="card-slider__subtitle">{$data.subtitle}</p>
        {/if}
	</div>
    <div class="card-slider__section-container card-slider__container">
        <div class="container review__comment">
            <div class="row">
                <div class="col-lg-12">
                    <div class="autoplay__carousel">
                        {foreach from=$data.contents item=sliderCard name=card}
                        <div class="comment">
                            <i class="fas fa-quote-right quote__icon__large"></i>
                            <p>
                                {if $sliderCard.description}
                                    {$sliderCard.description}
                                {/if}
                            </p>
                            <h3 class="bold card-slider__author" style="padding-top: 4.5rem">
                                {if (isset($sliderCard.title))}
                                    {$sliderCard.title}
                                {/if}
                            </h3>
                            <p>
                                {if (isset($sliderCard.subtitle))}
                                    {$sliderCard.subtitle}
                                {/if}
                            </p>
                        </div>
                        {/foreach}
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>