<section class="site-map__section">
    <div class="site-map__container">
        <div class="site-map__text">
            <div class="site-map__text__title">
                Sitemap
            </div>
            {foreach from=$data.sites item=site}
                <div class="site-map__text__description">
                    <a href="/{$site.url}">{$site.url}</a>
                </div>
            {/foreach}
        </div>
    </div>
</section>