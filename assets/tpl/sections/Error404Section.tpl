<section class="error-404__section">
    <div class="error-404__container">
        <div class="error-404__text">
            <div class="error-404__text__title">
                Pagina niet gevonden!
            </div>
            <div class="error-404__text__description">
                De pagina die je zoekt is wellicht verwijderd, de naam is veranderd of is tijdelijk buiten gebruik.
            </div>
        </div>
        <div class="error-404__button-wrapper">
            <a class="error-404__button" href="/">
                Ga naar de home pagina
            </a>
        </div>
    </div>
</section>