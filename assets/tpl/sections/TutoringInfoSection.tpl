<section class="tutoring-info__section">
    <div class="tutoring-info__container">
        {if $data.showVideo || ($data.showImage && isset($data.imageSrc)) || $data.showTutors}
            <div class="tutoring-info__video tutoring-info--position-{$data.mediaLeftPositionClass}">
                <div class="tutoring-info__video__media">
                    {if $data.showVideo}
                        {include 'components/TutoringInfoMediaVideo.tpl'}
                    {elseif $data.showImage && isset($data.imageSrc) }
                        {include 'components/TutoringInfoMediaImage.tpl'}
                    {elseif $data.showTutors}
                        {include 'components/TutoringInfoMediaTutorCards.tpl'}
                    {/if}
                </div>
                {if isset($data.overlayImageSrc)}
                    {include 'components/TutoringInfoMediaOverlayImage.tpl'}
                {/if}
            </div>
        {/if}
        <div class="tutoring-info__text {$data.textLeftPositionClass}">
            {if isset($data.title)}
                <div class="tutoring-info__text__title">
                    {$data.title}
                </div>
            {/if}
            {if isset($data.subtitle)}
                <div class="tutoring-info__text__subtitle">
                    {$data.subtitle}
                </div>
            {/if}
            {if isset($data.description)}
                <div class="tutoring-info__text__description">
                    {$data.description}
                </div>
            {/if}
            {if !$data.hideButton}
                <div class="tutoring-info__text__button">
                    <a href="{$data.buttonLink}">{$data.buttonText}</a>
                </div>
            {/if}
        </div>
        {if $data.showVideo || ($data.showImage && isset($data.imageSrc)) || $data.showTutors}
            <div class="tutoring-info__video tutoring-info__video-right tutoring-info--position-{$data.mediaRightPositionClass}">
                <div class="tutoring-info__video__media">
                    {if $data.showVideo}
                        {include 'components/TutoringInfoMediaVideo.tpl'}
                    {elseif $data.showImage && isset($data.imageSrc)}
                        {include 'components/TutoringInfoMediaImage.tpl'}
                    {elseif $data.showTutors}
                        {include 'components/TutoringInfoMediaTutorCards.tpl'}
                    {/if}
                </div>
                {if isset($data.overlayImageSrc)}
                    {include 'components/TutoringInfoMediaOverlayImage.tpl'}
                {/if}
            </div>
        {/if}
    </div>
</section>