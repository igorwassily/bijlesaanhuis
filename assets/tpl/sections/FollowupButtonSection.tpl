<section class="followup-button__section">
    <div class="followup-button__container">
        <div class="followup-button__text">
            {if (isset($data.title))}
                <div class="followup-button__text__title">
                    {$data.title}
                </div>
            {/if}
            {if (isset($data.description))}
                <div class="followup-button__text__description">
                    {$data.description}
                </div>
            {/if}
        </div>
        {if (isset($data.buttonText) && isset($data.buttonLink))}
            <div class="followup-button__button-wrapper">
                <a class="followup-button__text__button" href="{$data.buttonLink}">{$data.buttonText}</a>
            </div>
        {/if}
    </div>
</section>