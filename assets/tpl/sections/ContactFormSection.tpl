<section class="contact-formular__section">
    <div class="contact-formular__container">
        <div class="contact-formular__text">
            <div class="contact-formular__text__title">
                Contactformulier
            </div>
            <div class="contact-formular__text__description">
                Ook via onderstaand contactformulier reageren wij op alle vragen en opmerkingen. Als je bijvoorbeeld de juiste bijlesdocent niet hebt kunnen vinden, kan je via dit formulier een beroep doen op onze hulp. We helpen graag!
            </div>
        </div>
        <form method="post">
            <div class="contact-header__email">
                <input type="text" class="contact-header__email-input" name="name" placeholder="Naam" required {if !empty($data.form.name)}value="{$data.form.name}"{/if} />
                <input type="text" class="contact-header__email-input" name="email" placeholder="E-mailadres" required {if !empty($data.form.email)}value="{$data.form.email}"{/if} />
                <input type="text" class="contact-header__email-input" name="phone" placeholder="Telefoonnummer" required {if !empty($data.form.phone)}value="{$data.form.phone}"{/if} />
                <input type="text" class="contact-header__email-input" name="city" placeholder="Woonplaats" required {if !empty($data.form.city)}value="{$data.form.city}"{/if} />
                <input type="text" class="contact-header__email-input" name="address" placeholder="Adres" required {if !empty($data.form.address)}value="{$data.form.address}"{/if} />
                <input type="text" class="contact-header__email-input" name="subject" placeholder="Vak" required {if !empty($data.form.subject)}value="{$data.form.subject}"{/if} />
                <input type="text" class="contact-header__email-input" name="level" placeholder="Leerjaar en niveau" required {if !empty($data.form.level)}value="{$data.form.level}"{/if} />
                <textarea type="text" class="contact-header__email-input" name="message" placeholder="Bericht" required >{if !empty($data.form.message)}{$data.form.email}{/if}</textarea>
            </div>
            <div class="contact-formular__button-wrapper">
                <input class="contact-formular__text__button" type="submit" name="subContact" value="VERSTUUR" />
            </div>
        </form>
    </div>
</section>