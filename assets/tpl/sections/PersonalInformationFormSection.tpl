{extends file="components/WrapperCardComponent.tpl"}

{block name="wrapper-card-header"}
    <h3>Persoonlijke informatie</h3>
{/block}

{block name="wrapper-card-body"}
    <div class="personal-information-form">
        {foreach $data.inputFields item=field name=field}
            {include "components/InputFieldComponent.tpl" data=$field}
        {/foreach}
        <div class="personal-information-form__block">
            {foreach $data.toggleSwitch item=field name=field}
                {include "components/ToggleSwitchComponent.tpl" data=$field}
            {/foreach}
        </div>
    </div>
    {include "components/InputSubmitButtonComponent.tpl" data=$data.inputSubmitButton}
{/block}
