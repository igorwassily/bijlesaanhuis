{extends file="sections/CardParentSection.tpl"}

{block name="cardContent"}
	{if isset($card.title)}
		<div class="card-price-section__card__title">
			{$card.title}
		</div>
	{/if}
	{if isset($card.price)}
		<div class="card-price-section__card__price">
			<span class="card-price-section__card__price--euro-sign">€</span>
			<span class="card-price-section__card__price--amount">{$card.price}</span>
			<span class="card-price-section__card__price--hour">/lesuur*</span>
		</div>
	{/if}
	{if isset($card.description)}
		<div class="card-price-section__card__description">
			{$card.description}
		</div>
	{/if}
	{if isset($card.buttonText) && isset($card.buttonLink)}
		<div class="card-price-section__button-wrapper">
			<a class="card-price-section__text__button" href="{$card.buttonLink}">{$card.buttonText}</a>
		</div>
	{/if}
{/block}