{*
<link href="/app/template/css/modal.css" rel="stylesheet" /> *}
{* {literal} *}
{*
<script>
    function checkEmailAddress() {
        // test if e-mail already exists in db
        var ajax_url = '/check-email-exists.php';
        var email = $("#email").val();
        var data = { email: email, usergroup: "Student" };

        $.ajax({
            url: ajax_url,
            type: "POST",
            data: data
        }).done(function (responseTxt) {
            console.log('response', responseTxt);
            if (responseTxt.indexOf("Duplicate") === -1) {
                // if duplicate warning has been shown, hide it
                //$(".register-duplicate").fadeOut();
                $("#email").removeClass("error");
                $(".duplicate-error").remove();
                //show the next fieldset
                next_fs.show();
                //hide the current fieldset with style
                current_fs.hide();

                scrollToModalTop();

                initMap();
            } else {
                // Show e-mail duplicate warning
                $("#email").addClass("error");
                $(".duplicate-error").remove();
                $("#email").after("<p class='form-error duplicate-error'>Het ingevoerde e-mailadres is al in gebruik</p>");
            }
        });
    }

</script> *}
{* <script src="/app/assets/bundles/libscripts.bundle.js"></script>
<script src="/app/assets/bundles/vendorscripts.bundle.js"></script> *}

{* <link href="/app/template/css/modal.css" rel="stylesheet" /> *}
{* {literal} *}
{* <script src='https://api.tiles.mapbox.com/mapbox-gl-js/v0.53.1/mapbox-gl.js'></script>
<script src='https://api.mapbox.com/mapbox-gl-js/plugins/mapbox-gl-geocoder/v3.1.6/mapbox-gl-geocoder.min.js'></script>
<!----------------------------------recaptcha-------------- -->
<script src="https://www.google.com/recaptcha/api.js?explicit&hl=nl" async defer></script>
<script src="https://unpkg.com/popper.js@1"></script>
<script src="https://unpkg.com/tippy.js@4"></script> *}

<div id="login-register-modal" class="bijles-modal">
    <div class="bijles-modal__wrapper">
        <!-- Modal content -->
        <div class="bijles-modal__content" id="bijles-modal__content__keuzel">
            <span class="close">&times;</span>
            <div class="bijles-modal__header" name="bijles-modal__header">
                <div class="bijles-modal__keuze1">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2 style="text-align: center">Registeren</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bijles-modal__body">
                <div class="bijles-modal__keuze1">
                    <h4 class="m-16">Registreren kost slechts 2 minuten!</h4>
                    <a class="cursor modal-btn registreren-modal-btn">REGISTREREN</a>
                </div>
            </div>
            <div class="bijles-modal__bottom">
                <div class="bijles-modal__keuze1">
                    <h4 class="m-16" style="font-size:16px; padding-bottom: 3rem; padding-top:2rem;">Heb je al een
                        account?
                        Log <span class="inlog-btn-text cursor">hier</span> in.</h4>
                </div>
            </div>
        </div>




        <div class="bijles-modal__content" id="bijles-modal__content__inloggen">
            <span class="close">&times;</span>
            <div class="bijles-modal__header" name="bijles-modal__header">
                <div class="bijles-modal__inloggen inloggen">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <h2>Inloggen</h2>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bijles-modal__body">
                <div class="bijles-modal__inloggen inloggen">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="login_form" class="form login_form" onsubmit="return submit_form()">
                                    <div class="content centeer">
                                        <div class="form-float">
                                            <label for="username">E-mailadres</label>
                                            <input id="login-email" type="text" class="form-control login_form_input"
                                                placeholder="E-mailadres" name="username" required maxlength="60"
                                                minlength="5" />
                                            <span class="input-group-addon inloggen-input-icon">
                                                <i class="zmdi zmdi-account-circle"></i>
                                            </span>
											</div>
											<div class="form-float">
												<label for="password">Wachtwoord</label>
												<input id="login-password" type="password" placeholder="Wachtwoord"
												       class="form-control login_form_input" name="password" required
												       maxlength="20" minlength="8"
												       title="Wachtwoord moet minimaal 8 tekens bevatten"/>
												<span class="input-group-addon inloggen-input-icon">
                                                <i class="zmdi zmdi-lock"></i>
                                            </span>
											</div>
										</div>
										<div class="input-group input-lg centeer recaptcha-input">
											<span class="msg-error error"></span>
											<div id="recaptcha" class="g-recaptcha"
											     data-sitekey="6LebmZ8UAAAAAJU6v6g1ntuuNZycJMbr8k6Pv5F3"></div>
										</div>
										<div class="centeer login_form_btn">
											<button type="button" name="login" id="login-modal">INLOGGEN</button>
										</div>
										<span class="msg-error"></span>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="bijles-modal__bottom">
					<div class="bijles-modal__inloggen inloggen">
						<div class="login_form_footer">
							<h5 class="inlog-error js-inlog-error">Het e-mailadres en/of wachtwoord is niet correct</h5>
							<h5 class="inlog-error js-mail-correct">Het e-mailadres en/of wachtwoord is niet
								correct</h5>
							<h5 class="h5_text modal--h5_text">
								<a href="/app/forgotPassword" class="link">Wachtwoord vergeten?</a>
							</h5>
							<h5 class="h5_text modal--h5_text">
								<a id="registeer-from-login" class="link">Registreer als leerling / </a>
								<a href="/app/docenten-registratie" class="link">Aanmeldprocedure docent</a>
							</h5>
						</div>
					</div>
				</div>
			</div>


			<div class="bijles-modal__content" id="bijles-modal__content__login-success">
				<span class="close">&times;</span>
				<div class="bijles-modal__header" name="bijles-modal__header">
					<div class="bijles-modal__login-success">
						<h2>U bent nu ingelogd</h2>
					</div>
				</div>
				<div class="bijles-modal__body">
					<div class="bijles-modal__login-success">
						<a class="account-btn" href="teachers-dashboard.php">Account</a>
					</div>
				</div>
				<div class="bijles-modal__bottom"></div>
			</div>


			<div class="bijles-modal__content" id="bijles-modal__content__register-success">
				<span class="close">&times;</span>
				<div class="bijles-modal__header" name="bijles-modal__header">
					<div class="bijles-modal__register-success">
						<h2>Verificatiemail ontvangen</h2>
					</div>
				</div>
				<div class="bijles-modal__body">
					<div class="bijles-modal__register-success">
						<p class="m-b-28">Je bent een stap dichter bij betere schoolcijfers! We hebben een e-mail
							verstuurd,
							zodat je je account kan activeren. Heb je een vraag? We zijn er graag voor je!</p>
					</div>
				</div>
				<div class="bijles-modal__bottom">
					<div class="bijles-modal__register-success">
						<a class="cursor modal-btn inlog-btn-after-register"
						   style="background-color: #5cc75f !important;">INLOGGEN</a>
					</div>
				</div>
			</div>


			<div class="bijles-modal__content" id="bijles-modal__content__register">
				<span class="close">&times;</span>
				<div class="bijles-modal__header" name="bijles-modal__header">
					<div class="bijles-modal__register">
						<div class="container">
							<div class="row">
								<div class="col-md-12">
									<h2 style="text-align: center">Registeren</h2>
								</div>
							</div>
							<div class="row">
								<div class="col-md-12">
									<p>Registreer en boek direct de bijlesdocent die bij jou past!</p>
								</div>
							</div>
						</div>
						<!-- progressbar -->
						<ul id="progressbar">
							<li class="active"></li>
							<li></li>
						</ul>
					</div>
				</div>
				<div class="bijles-modal__body">
					<div class="bijles-modal__register registeren">
						<!-- multistep form -->
						<form id="msform" id="studentRegistration">
							<!-- fieldsets -->
							<fieldset>
								<div class="form-float">
									<input type="hidden" class="form-control" placeholder="User Group" name="choice"
									       id="choice" value="Student-Registration"/>
									<input type="hidden" class="form-control" placeholder="User Group" name="usergroup"
									       id="usergroup" value="Student"/>
									<input type="hidden" class="form-control" placeholder="Username" name="username"
									       id="username"/>
								</div>
								<div class="form-float">
									<label class="p-l-10">E-mailadres *
										<span style="margin: 7px 7px 0 7px; height: 15px;color: #BBB !important;"
										      class="tippy"
										      data-toggle="tooltip" data-placement="bottom"
										      data-tippy-content="Dit e-mailadres wordt gebruikt voor alle vormen van contact, behalve facturering. Voor facturering wordt het ouderlijk e-mailadres van de volgende pagina gebruikt."><i
													class="fas fa-info-circle"></i></span>
									</label>
									<input type="email" class="form-control width" placeholder=" E-mailadres"
									       name="email"
									       id="email" required/>
									<input type="hidden" name="checkEmail" id="checkEmail" value=""/>
								</div>
								<div class="form-float">
									<label class="p-l-10 p-t-10">Telefoonnummer *</label>
									<input type="tel" class="form-control width" placeholder="Telefoonnummer"
									       name="telephone" id="telephone" onfocus="checkEmailAddress();">
								</div>
								<div class="form-float">
									<label class="p-l-10 p-t-10">Wachtwoord *</label>
									<input type="password" class="form-control width" placeholder="Wachtwoord"
									       name="password" id="passwordreg" minlength="8" required/>
								</div>

								<div class="form-float" style="padding-bottom: 1rem">
									<label class="p-l-10 p-t-10">Wachtwoord bevestigen *</label>
									<input type="password" class="form-control width"
									       placeholder="Wachtwoord bevestigen"
									       name="confirm_password" id="confirm_password" minlength="8" required/>
								</div>
								<input type="button" id="next-part1" name="next" class="btn next action-button"
								       value="Volgende"/>
							</fieldset>

							<!-- Secondary form //-->
							<fieldset>
								<div class="form-float" style="margin: 0 5%;">

									<div class="form-groups">

										<label class="p-l-10 p-t-10" style="margin-left:0; margin-right:15.5px;">
											Leerling *
											<span style="margin-top: 5px; height: 15px;color: #BBB !important;"
											      class="tippy"
											      data-toggle="tooltip" data-placement="bottom"
											      data-tippy-content="Indien de naam van de leerling uit privacy overwegingen niet gegeven wenst te worden, kan gebruik gemaakt worden van een fictieve naam of de naam van een ouder / verzorger."><i
														class="fas fa-info-circle"></i>
                                        </span>
										</label>

										<div class="input-group" style="padding-bottom: 14px;">
											<input type="text" class="form-control h no-t-m" placeholder="Voornaam"
											       name="first-name" id="first-name" required/>
											<p id="warningmsg_fn" style="color:red !important; display:none;">
												Voornaam is verplicht
											</p>
										</div>

										<div class="input-group" style="padding-bottom: 14px;">
											<input type="text" class="form-control h no-t-m" placeholder="Achternaam"
											       name="last-name" id="last-name" required/>
											<p id=last-name-desc" class="warningmsg_ln"
											   style="color:red !important; display:none;">
												Achternaam is verplicht
											</p>
										</div>
									</div>


									<div class="form-groups">

										<label class="p-l-10 p-t-10" style="margin-left:0; margin-right:15.5px;">
											Ouder *
										</label>

										<div class="input-group" style="padding-bottom: 14px;">
											<input type="text" class="form-control h no-t-m"
											       placeholder="Ouder voornaam"
											       name="parent-first-name" id="parent-first-name" required/>
											<p id="parent-first-name-descs" class="warningmsg_pf"
											   style="color:red !important; display:none;">
												Ouder voornaam is verplicht
											</p>
										</div>
										<div class="input-group" style="padding-bottom: 14px;">
											<input type="text" class="form-control h no-t-m"
											       placeholder="Ouder achternaam"
											       name="parent-last-name" id="parent-last-name" required/>
											<p id="parent-last-name-desc" class="warningmsg_pl"
											   style="color:red !important; display:none;">
												Ouder achternaam is verplicht
											</p>

										</div>
									</div>

									<!-- Ouderlijk Email Adres //-->
									<div class="form-groups">

										<label class="p-l-10 p-t-10" style="margin-left:0;margin-right:15.5px;">
											Ouderlijk e-mailadres *
											<span style="margin-top: 5px; height: 15px;color: #BBB !important;"
											      class="tippy"
											      data-toggle="tooltip" data-placement="bottom"
											      data-tippy-content="Dit e-mailadres wordt gebruikt voor maandelijkse facturering, uiterlijk elke 10e van de maand volgend op de maand waarop de bijles plaatsvond."><i
														class="fas fa-info-circle"></i>
                                        </span>
                                    </label>

                                    <div class="input-group">
                                        <input type="email" class="form-control h no-t-m" placeholder="E-mailadres"
                                            name="parent-email" id="parent-email" required
                                            aria-describedby="parent-email-desc" />
                                        <p id="parent-email-desc" class="warningmsg_pe"
                                            style="color:red !important; display:none;">
                                            Vul een geldig e-mailadres in
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- End ouderlijk email adres //-->

                            <div class="form-float width">
                                <label for="postcode" class="p-l-5 p-t-10" style="margin-left: 0rem; display: block">
                                    Bijlesadres *
                                </label>

                                <input type="text" placeholder="Postcode" id="postcode" required
                                    class="form-control h no-t-m" style="width: 50%; display: inline-block;">

                                <input type="text" placeholder="Huisnummer" id="huisnummer" required
                                    class="form-control h no-t-m"
                                    style="width: 40%; display: inline-block; right: 5%; position: absolute;">
                            </div>

                            <div class="form-float width">
                                <input type="text" placeholder="Straatnaam" id="straatnaam"
                                    class="form-control h no-t-m" required>
                                <input type="hidden" class="form-control" name="_lat" id="lat">
                                <input type="hidden" class="form-control" name="_lng" id="lng">
                                <input type="hidden" class="form-control" name="_place_name" id="place_name">
                            </div>

                            <div class="form-float width" style="padding-bottom: 14px;">
                                <input type="text" class="form-control h no-t-m" placeholder="Woonplaats"
                                    id="woonplaats" required>
                            </div>
                            <pre id='coordinates' class='coordinates' style="display:none;"></pre>

                            <h5 class="register-error">Er is iets fout gegaan met het registeren</h5>
                            <label for="error" id="errorMessage"
                                style="margin-left: 0rem; color:red !important; display: none;">
                                We hebben geen adres gevonden, neem contact met ons op
                            </label>
                            <input type="button" name="previous" class="btn previous action-button" value="Vorige" />
                            <input type="submit" name="next" class="btn next action-button" value="Afronden"
                                id="submit-register" style="margin-top: 1rem !important" />
                        </fieldset>
                    </form>
                </div>
            </div>
            <div class="bijles-modal__bottom">
                <div class="bijles-modal__register registeren" style="margin-bottom:2rem"></div>
            </div>
        </div>




    </div>
</div>
{* {/literal} *}
{* <script src="/app/template/js/modal.js"></script> *}