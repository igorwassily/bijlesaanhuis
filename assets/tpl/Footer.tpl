<div id="footer">
    <div class="footer__container">
        <ul class="footer__list">
            <li>
                <h4 class="footer__list--title">Info</h4>
            </li>
            <li><a href="/docenten">Vind docent</a></li>
            <li><a href="/over-ons">Wie zijn wij?</a></li>
            <li><a href="/voorwaarden">Voorwaarden</a></li>
            <li><a href="/bijles-geven">Bijles geven</a></li>
            <li><a href="/contact">Contact</a></li>
            <li><a href="/blog">Blog</a></li>
        </ul>
        <ul class="footer__list">
            <li>
                <h4 class="footer__list--title">Locaties</h4>
            </li>
            <li><a href="/bijles/utrecht">Bijles Utrecht</a></li>
            <li><a href="/bijles/rotterdam">Bijles Rotterdam</a></li>
            <li><a href="/bijles/amsterdam">Bijles Amsterdam</a></li>
            <li><a href="/bijles/den-haag">Bijles Den Haag&nbsp;</a></li>
            <li><a href="/bijles/leiden">Bijles Leiden</a></li>
            <li><a href="/alle-locaties">Alle locaties</a></li>

        </ul>
        <ul class="footer__list">
            <li>
                <h4 class="footer__list--title">Vakken</h4>
            </li>
            <li><a href="/bijles/wiskunde">Bijles wiskunde</a></li>
            <li><a href="/bijles/economie">Bijles economie</a></li>
            <li><a href="/bijles/scheikunde">Bijles scheikunde</a></li>
            <li><a href="/bijles/natuurkunde">Bijles natuurkunde</a></li>
            <li><a href="/bijles/biologie">Bijles biologie</a></li>
            <li><a href="/alle-vakken">Alle vakken</a></li>
        </ul>
        <ul class="footer__list footer__list__team--top">
            <li>
                <h4 class="footer__list--title">Ons team</h4>
            </li>
            <li class="footer__list__people-item">
                <div class="footer__list__people-img">
                    <img style="width: 50px !important; height: 50px !important;"
                        src="/s3/content/Florian-Zandbergen.gif" alt="">
                </div>
                <div class="footer__list__people-details">
                    <h5 class="footer__list__people-name">Florian</h5>
                    <div class="footer__list__people-description">
                        Oprichter
                    </div>
                </div>
            </li>
            <li class="footer__list__people-item">
                <div class="footer__list__people-img">
                    <img style="width: 50px !important; height: 50px !important;"
                        src="/s3/content/Jamie_Schuller_Bijles_Aan_Huis.png"
                        alt="">
                </div>
                <div class="footer__list__people-details">
                    <h5 class="footer__list__people-name">Jamie</h5>
                    <div class="footer__list__people-description">
                        Oprichter
                    </div>
                </div>
            </li>
            <li class="footer__list__people-item">
                <div class="footer__list__people-img">
                    <img style="width: 50px !important; height: 50px !important;"
                        src="/s3/content/Silvana.png" alt="">
                </div>
                <div class="footer__list__people-details">
                    <h5 class="footer__list__people-name">Silvana</h5>
                    <div class="footer__list__people-description">
                        Marketing Specialist
                    </div>
                </div>
            </li>
        </ul>
    </div>
    <div class="footer__container footer__list__team--bottom">
        <h4 class="footer__list--title">Ons team</h4>
    </div>
    <div class="footer__container footer__list__team--bottom">
        <li class="footer__list__people-item">
            <div class="footer__list__people-img">
                <img style="width: 50px !important; height: 50px !important;"
                    src="/s3/content/Florian-Zandbergen.gif" alt="">
            </div>
            <div class="footer__list__people-details">
                <h5 class="footer__list__people-name">Florian</h5>
                <div class="footer__list__people-description">
                    Oprichter
                </div>
            </div>
        </li>
        <li class="footer__list__people-item">
            <div class="footer__list__people-img">
                <img style="width: 50px !important; height: 50px !important;"
                    src="/s3/content/Jamie_Schuller_Bijles_Aan_Huis.png"
                    alt="">
            </div>
            <div class="footer__list__people-details">
                <h5 class="footer__list__people-name">Jamie</h5>
                <div class="footer__list__people-description">
                    Oprichter
                </div>
            </div>
        </li>
        <li class="footer__list__people-item">
            <div class="footer__list__people-img">
                <img style="width: 50px !important; height: 50px !important;"
                    src="/s3/content/Silvana.png" alt="">
            </div>
            <div class="footer__list__people-details">
                <h5 class="footer__list__people-name">Silvana</h5>
                <div class="footer__list__people-description">
                    Marketing Specialist
                </div>
            </div>
        </li>
    </div>
    
    <div class="footer__container footer__social-media">
        <div>
            <a href="https://www.facebook.com/bijlesaanhuisnl/" title="Follow on Facebook" target="_blank" rel="noopener"><i
                    class="fab fa-facebook-square"></i></a>
            <a href="https://www.linkedin.com/company/bijlesaanhuis/" title="Follow on LinkedIn" target="_blank" rel="noopener"><i
                    class="fab fa-linkedin"></i></a>
        </div>
    </div>
    <div class="footer__copyright">
        <p>&nbsp; Copyright © Bijles Aan Huis BV. Alle rechten voorbehouden.</p>
        <p class="version-number">Version: {if isset($versionLink)}<a href="{$versionLink}" target="_blank">{$version}</a>{else}{$version}{/if}</p>
    </div>
</div>
{* START These are necessary *}
{* <link href="/app/template/css/footer.css" rel="stylesheet" /> *}
{* <script src="/app/assets/bundles/mainscripts.bundle.js"></script><!-- Custom Js --> *}
{* <script src="/app/assets/plugins/momentjs/moment.js"></script> *}
<!-- datepicker test -->
{* <script src="https://cdn.jsdelivr.net/npm/flatpickr"></script>
<script src="https://npmcdn.com/flatpickr/dist/l10n/nl.js"></script>
<script src="https://wchat.freshchat.com/js/widget.js"></script> *}
{* END These are necessary *}









<!-- Jquery Core Js -->
<!-- <script src="/app/assets/plugins/sweetalert/sweetalert.min.js"></script>
<script src="/app/assets/plugins/bootstrap-notify/bootstrap-notify.js"></script>-->

{*
<!-- Jquery Validation Plugin Css -->
<script src="/app/assets/plugins/jquery-validation/jquery.validate.js"></script>
*}


{* <script src="/app/assets/js/pages/forms/basic-form-elements.js"></script> 
<script src="/app/assets/js/pages/forms/advanced-form-elements.js"></script> 
<script src="/app/assets/plugins/bootstrap-tagsinput/bootstrap-tagsinput.js"></script>

<!-- Jquery DataTable Plugin Js -->
<script src="/app/assets/bundles/datatablescripts.bundle.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/dataTables.buttons.min.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/buttons.bootstrap4.min.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/buttons.colVis.min.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/buttons.html5.min.js"></script>
<script src="/app/assets/plugins/jquery-datatable/buttons/buttons.print.min.js"></script>
<script src="/app/assets/js/pages/tables/jquery-datatable.js"></script>
<script src = "https://cdn.datatables.net/responsive/2.2.3/js/dataTables.responsive.min.js"></script>
<script src="/app/assets/plugins/waitme/waitMe.js"></script>

<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

<script src="/app/assets/plugins/hoverIntent/jquery-hoverIntent.js"></script> *}


<script>
    {literal}
    window.fcWidget.init({
        token: "7fcc8618-4a87-47e7-a3e6-eb3d092e4cea",
        host: "https://wchat.freshchat.com",
        locale: "nl"
    });
    
    var Global_Variable_timeRange = null;

    function __onClick(obj, date) {
        console.log(Global_Variable_timeRange[date]);
    }


    function togglePassword(obj) {
        var checkBox = document.getElementById("toggle");

        // If the checkbox is checked, display the output text
        if (checkBox.checked == true) {
            $("#password").attr("type", "text");
        } else {
            $("#password").attr("type", "password");
        }
    }

    $('[data-toggle="tooltip"]').tooltip({});
    tippy('.tippy', {
        placement: 'bottom-start',
        animation: 'fade',
        theme: 'light',
        interactive: true,
        interactiveBorder: 20
    });

    function run_waitMe(el, num, effect) { }

    function notifi() {
        $.ajax({
            type: "POST",
            url: "/app/includes/chat-ajax.php",
            data: {
                filename: "notifi"
            },
            dataType: "text",
            success: function (response) {
                if (response == "Unread Messsages") {
                    $("#emailNotify").removeClass("hide");
                } else {
                    $("#emailNotify").addClass("hide");
                }
            },
            error: function (xhr, ajaxOptions, thrownError) {
                if (xhr.responseText == "Unread Messsages") {
                    $("#emailNotify").removeClass("hide");
                } else {
                    $("#emailNotify").addClass("hide");
                }
            }
        }); // ajax ends
    } //func ends

    $(document).ready(function () {

        // setInterval(notifi, 3000);
        $($("li.active.open").closest("ul.ml-menu").get()).css("display", "block");

    }); //document ends 

    function isIE() {
        ua = navigator.userAgent;
        /* MSIE used to detect old browsers and Trident used to newer ones*/
        var is_ie = ua.indexOf("MSIE ") > -1 || ua.indexOf("Trident/") > -1;

        return is_ie;
    }

    function isEdge() {
        ua = navigator.userAgent;
        var is_ed = ua.indexOf("Edge") > -1;
        return is_ed;
    }
    /* for IE and Edge */
    if (isIE()) {
        alert('Application is not compatible with this browser.');
        window.location.replace("https://bijlesaanhuis.nl");
    } else if (isEdge()) {
        alert('Application is not compatible with this browser.');
        window.location.replace("https://bijlesaanhuis.nl");
    }

    jQuery('ul.nav li.dropdown').hover(function () {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(500);
    }, function () {
        jQuery(this).find('.dropdown-menu').stop(true, true).delay(100).fadeOut(500);
    });
    {/literal}
</script>

<script src="/app/assets/js/pages/ui/notifications.js"></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=UA-139743478-1"></script>
<script>
        {literal}
        (function (h, o, t, j, a, r) {
            h.hj = h.hj || function () {
                (h.hj.q = h.hj.q || []).push(arguments)
            };
            h._hjSettings = {
                hjid: 1411079,
                hjsv: 6
            };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script');
            r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');

        /* Global site tag (gtag.js) - Google Analytics */
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }

        gtag('js', new Date());
        gtag('config', 'UA-139743478-1');

        {/literal}
        $(document).ready(function () {
            {if isset($showPopup) && $showPopup}
                showNotification("alert-success", "Je account is geactiveerd", "bottom", "center", "", "");
            {/if}
        });

</script>