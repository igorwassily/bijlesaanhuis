
let OFFSET = 0;
let LIMIT = 10;
let suspend = false;
let lastCategory = 0;
let resultList;
let filter = null;
let LAT = 0;
let LONG = 0;
let PSTCODE = '';
let firstSearch = false;
let schoolTypeChanged = false;
let resultUrl = '';
let schoolLevelChanged = false;

$(document).ready(function () {
    init();
});

function init() {

    $("#postcode3").on("change paste", function () {
        loading(true);
        PSTCODE = $('#postcode3').val();
        OFFSET = 0;
        lastCategory = 0;
        filter.select = true;
        postalCode();
    });

    /* SCHOOL TYPE */

    $('select[id="school-type"]').on('change', function () {
        schoolTypeChanged = true;
        loadAllSelects();

        if ($(this).val() === "9") {
            showElementarySchool();
        } else {
            showHighSchool();
        }
        $('#school-course').selectpicker('toggle');
    });

    $('select[id="school-course"]').on('change', function () {
        loadAllSelects();
    });

    $('select[id="school-level"]').on('change', function () {
        schoolLevelChanged = true;
        loadAllSelects();
        $('#school-year').selectpicker('toggle');
    });

    $('select[id="school-year"]').on('change', function () {
        loadAllSelects();
        $('#postcode3').focus();
    });

    $('#submission').on('click', function () {
        location.href = '/app/teachers-profile2' + resultUrl + '&postCode=' + $('#postcode3').val();
    });

    filter = {};

    if (location.search.length > 0) {
        filter = QueryParamsToJSON();

        if (filter.school && filter.school === "9") {
            // BASISSCHOOL
            showElementarySchool();
        }
        if (filter.lat) {
            LAT = filter.lat;
        }
        if (filter.long) {
            LONG = filter.long;
        }
        if (filter.postCode) {
            PSTCODE = filter.postCode;
            $('#postcode3').val(PSTCODE);
        }
    } else {
        filter = {
            school: 8,
            course: [31],
            level: 4,
            year: 12,
            select: false
        };

    }

    loadAllSelects(filter, true);

    //$('.btn[data-id="school-course"]').parent().find('span.filter-option').first().text('Selecteer Vak');
    $('.selectpicker').selectpicker();
    $('.selectpicker').selectpicker('refresh');
}


function showElementarySchool() {
    $('#schooltypediv, #schoolcoursediv').addClass('col-lg-4');
    $('#schooltypediv, #schoolcoursediv').removeClass('col-lg-2 col-lg-3');
    $('#schoolleveldiv').hide();
    $('#schoolyeardiv').hide();
    //$('#schooltypediv .school-type.show button,#schoolcoursediv .school-course.show button,#schoolleveldiv .school-level.show button,#schoolyeardiv .school-year.show button').attr('aria-expanded', false);
    //$('#schooltypediv .school-type.show,#schoolcoursediv .school-course.show,#schoolleveldiv .school-level.show,#schoolyeardiv .school-year.show').removeClass('show');
}

function showHighSchool() {
    $('#schooltypediv, #schoolcoursediv').addClass('col-lg-2');
    $('#schooltypediv, #schoolcoursediv').removeClass('col-lg-3 col-lg-4');
    $('#schoolleveldiv').show();
    $('#schoolyeardiv').show();
    //$('#schooltypediv .school-type.show button,#schoolcoursediv .school-course.show button,#schoolleveldiv .school-level.show button,#schoolyeardiv .school-year.show button').attr('aria-expanded', false);
    //$('#schooltypediv .school-type.show,#schoolcoursediv .school-course.show,#schoolleveldiv .school-level.show,#schoolyeardiv .school-year.show').removeClass('show');
}

function postalCode() {

    var foo = $('#postcode3').val().split(" ").join("");
    if (foo.length > 0) {
        foo = foo.match(new RegExp('.{1,4}', 'g')).join(" ");
    }


    $('#postcode3').val(foo);

    if ($('#postcode3').val().length === 0) {
        $('#postcode3').css('text-transform', 'capitalize');
    } else {
        $('#postcode3').css('text-transform', 'uppercase');
    }

    var postalCode = $('#postcode3').val().replace(' ', '');

    // POSTALCODE 2012ES
    // HOUSE NUMBER 30
    //console.log(postalCode);

    if (postalCode.length >= 6 && postalCode.length <= 7) {
        //console.log("in here");
        $.ajax
            ({
                type: "GET",
                url: "/api/Geo/Address?postalCode=" + postalCode,
                success: function (response) {
                    $('#errorMessage2').css("display", "none");

                    LAT = response.latitude;
                    LONG = response.longitude;
                    PSTCODE = postalCode;

                    resultList.html(" ");
                    //searchTeacher();
                },
                error: function (error) {
                    //console.log("error");
                    console.log(error);

                    //console.log("Combination doesn't exist!");
                    $('#errorMessage2').css("display", "block");

                    $('#lat3').val("");
                    $('#lng3').val("");
                    $('#place_name3').val("");

                    LAT = 0;
                    LONG = 0;
                    PSTCODE = '';
                }
            });
    } else {
        $('#errorMessage2').css("display", "none");
    }
}

function QueryParamsToJSON() {
    let list = location.search.slice(1).split('&'),
        result = {};

    list.forEach(function (keyval) {
        keyval = keyval.split('=');
        var key = keyval[0];
        if (/\[[0-9]*\]/.test(key) === true) {
            var pkey = key.split(/\[[0-9]*\]/)[0];
            if (typeof result[pkey] === 'undefined') {
                result[pkey] = [];
            }
            result[pkey].push(decodeURIComponent(keyval[1] || ''));
        } else {
            result[key] = decodeURIComponent(keyval[1] || '');
        }
    });

    return JSON.parse(JSON.stringify(result));
}

function getSelectedValues() {
    let data = { school: null, course: [], level: null, year: null, postCode: null };

    let schoolType = $('select[id="school-type"]').find(':selected');

    if (schoolType.length > 0 && schoolType.val() !== "") {
        data.school = schoolType.val();
    }

    $('select[id="school-course"]').find(':selected').each(function (k, e) {
        data.course.push(e.value);
    });

    if ($('#schoolleveldiv').is(':not(:hidden)')) {
        let schoolLevel = $('select[id="school-level"]').find(':selected');
        if (schoolLevel.length > 0 && schoolLevel.val() !== "") {
            data.level = schoolLevel.val();
        }
    }

    if ($('#schoolyeardiv').is(':not(:hidden)')) {
        let schoolYear = $('select[id="school-year"]').find(':selected');
        if (schoolYear.length > 0 && schoolYear.val() !== "") {
            data.year = schoolYear.val();
        }
    }

    return data;
}

function fillSelect(select, data) {
    select.html("");

    data.forEach(function (value) {
        if (value.selected) {
            select.append($(`<option value="${value.id}" selected>${value.text}</option>`));
        } else {
            select.append($(`<option value="${value.id}">${value.text}</option>`));
        }
    });

    select.selectpicker('refresh');
}

function loadAllSelects(data) {
    if (suspend) {
        return 0;
    }

    OFFSET = 0;
    lastCategory = 0;

    if (!data) {
        data = getSelectedValues();
    }

    if (schoolTypeChanged) {
        data.course = [];
        //schoolTypeChanged = false;
    }

    filter = data;
    loading(true);

    $.ajax({
        url: '/api/Teacher/Criteria',
        data: data,
        type: "POST",
        //dataType: "json",
        success: function (result) {
            if (result.school) {
                let select = $('select[id="school-type"]');
                fillSelect(select, result.school);

                /* Removing the <li> from bootstrap select generated by <option>Selecteer Schooltype</option> */
                //$('.btn[data-id="school-type"]').parent().find('li').first().remove();
            }

            if (result.course) {
                let select = $('select[id="school-course"]');
                fillSelect(select, result.course);

                /* Removing the <li> from bootstrap select generated by <option>Selecteer Schooltype</option> */
                //$('.btn[data-id="school-course"]').parent().find('li').first().remove();
            }

            if (result.level) {
                let select = $('select[id="school-level"]');
                fillSelect(select, result.level);
            }

            if (result.year) {
                let select = $('select[id="school-year"]');
                fillSelect(select, result.year);
            }
            if (schoolTypeChanged) {
                $('#school-course').selectpicker('toggle');
                schoolTypeChanged = false;
            }
            if (schoolLevelChanged) {
                $('#school-year').selectpicker('toggle');
                schoolLevelChanged = false;
            }
            resultUrl = result.search;

            loading(false);
        },
        error: function (err) {
            console.log(err);
            loading(false);
        }
    });
    setTimeout(function() {
        if ($('.card-slider__section').css('background-color') == "rgb(241, 241, 241)") {
            $('.comment.slick-slide').css('background-color', '#fff');
        }
    },1000)
}

function openNextStep(id) {
    // To Open
    var array = ['type', 'course', 'level', 'type'];
    for (let i = 0; i < array.length; i++) {
        if (id == array[i]) {
            $('#school-' + id).addClass('open');
            continue;
        }
        if ($('#school-' + id).hasClass('open')) {
            $('#school-' + id).removeClass('open');
        }
    }
    //$('.' + id + "> .dropdown-menu").css('display', 'block');
    //$('#' + id).selectpicker('toggle');
}

function closePreviousStep(id) {
    //console.log("close");
    // To close
    $('.' + id + "> .dropdown-menu").css('display', 'none');
    //$('#' + id).selectpicker('toggle');
}

$("form").keypress(function (e) {
    //Enter key
    if (e.which == 13) {
        //console.log("ENTER PRESSED");
        return false;
    }
});

function resetInputs() {
    $('.school-level > .dropdown-menu').css('display', '');
    $('.school-year > .dropdown-menu').css('display', '');
    $('.school-type > .dropdown-menu').css('display', '');
    $('.school-course > .dropdown-menu').css('display', '');
}

function loading(loading) {
    if (loading) {
        suspend = true;
        // $('#loadmore').hide();
        // $('#no-result').hide();
        // $('#loading').show();
        //resultList.hide();
        //resultList.html(" ");
    } else {
        // $('#loading').hide();
        //resultList.show();
        suspend = false;
    }
}