$(document).ready(function () {
    // onscroll appearance of section
    //$('.counting-number__text__title').fadeOut();

    /* Every time the window is scrolled ... */
    $(window).scroll(function () {

        /* Check the location of each desired element */
        $('.counting-number__text__title').each(function (i) {

            var bottom_of_object = $(this).parent().offset().top + $(this).parent().outerHeight();
            var bottom_of_window = $(window).scrollTop() + $(window).height();

            var $counter = $(this).find('.counting-number__animate');
            console.log("$counter.text()",$counter.text());
            $val = $(this).find('.counting-number__animate').attr('data-number');
            console.log("$val",$val);

            /* If the object is completely visible in the window, fade it in */
            if (bottom_of_window > bottom_of_object && $val && $val > 0) {
                $({ Counter: 0 }).animate({ Counter: $val }, {
                    duration: 1500,
                    easing: 'swing',
                    step: function () {
                        $($counter).text(Math.ceil(this.Counter));
                    }
                });
                //$(this).animate({ 'opacity': '1' }, 500);
                $($counter).removeAttr('data-number');
            }

        });

    });
});