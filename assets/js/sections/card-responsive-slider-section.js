$(document).ready(function () {
    $('.card-responsive-slider-autoplay__carousel').slick({
        dots: true,
        infinite: true,
        speed: 500,
        cssEase: 'linear'
    });
});