/** Handling click on the image overlay of the video:
*   1. Play the video.
*   2. Hide the overlay image.
*/
$(document).ready(function() {
    console.log('tutoring-info');
    $('.tutoring-info__video__overlay').on('click',function() {
        console.log($(this));
        $(this).parent().find('video').get(0).play();
        $(this).hide("fast");
    })
}); 