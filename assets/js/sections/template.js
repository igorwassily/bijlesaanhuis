/* Lazy loading images and let them fade in when they are loaded. */
$(document).ready(function () {
    /* Images with data-src attribute */
    [].forEach.call(document.querySelectorAll('img[data-src]'), function (img) {
        img.setAttribute('src', img.getAttribute('data-src'));
        img.onload = function () {
            img.removeAttribute('data-src');
        };
    });
    /* background image for div element with data-src attribute */
    [].forEach.call(document.querySelectorAll('div[data-src]'), function (div) {
        var bg = div.getAttribute('data-src');
        if (bg) {
            var src = bg;
            $img = $('<img>').attr('src', src).on('load', function () {
                div.removeAttribute('data-src');
                $(div).parent().parent().find("video").css('opacity', 1);
            });
        }
    });

    var lazyVideos = [].slice.call(document.querySelectorAll("video.lazy"));

    if ("IntersectionObserver" in window) {
        var lazyVideoObserver = new IntersectionObserver(function (entries, observer) {
            entries.forEach(function (video) {
                if (video.isIntersecting) {
                    for (var source in video.target.children) {
                        var videoSource = video.target.children[source];
                        if (typeof videoSource.tagName === "string" && videoSource.tagName === "SOURCE") {
                            videoSource.src = videoSource.dataset.src;
                        }
                    }

                    video.target.load();
                    video.target.classList.remove("lazy");
                    lazyVideoObserver.unobserve(video.target);
                }
            });
        });

        lazyVideos.forEach(function (lazyVideo) {
            lazyVideoObserver.observe(lazyVideo);
        });
    } else {
        //alert('NO IntersectionObserver');
        $("video source").each(function () {
            var sourceFile = $(this).attr("data-src");
            $(this).attr("src", sourceFile);
            var video = this.parentElement;
            video.load();
            //video.play();
        });
    }
// });

/* The FadeIn effect for all sections on scrolling down */
// $(document).ready(function () {
    $(window).scroll(function () {
        if ($(window).width() < 992) {
            var windowBottom = $(this).scrollTop() + $(this).innerHeight();
            var selector = ".tutoring-info__section:not(.faded), .cards__section:not(.faded), .slider-cards__section:not(.faded)";
            $(selector).each(function () {
                /* Check the location of each desired element */
                var objectBottom = $(this).offset().top + Math.min($(window).height() / 3, $(this).outerHeight() / 3);

                /* If the element is completely within bounds of the window, fade it in */
                if (objectBottom < windowBottom) { //object comes into view (scrolling down)
                    if ($(this).css("opacity") == 0) {
                        $(this).fadeTo(500, 1);
                        $(this).addClass('faded');
                    }
                } else { //object goes out of view (scrolling up)
                    if ($(this).css("opacity") == 1) { $(this).fadeTo(500, 0); }
                }
            });
        }
    }).scroll(); //invoke scroll-handler on page-load
});


document.addEventListener("DOMContentLoaded", function () {
    // var lazyVideos = [].slice.call(document.querySelectorAll("video.lazy"));

    // if ("IntersectionObserver" in window) {
    //     var lazyVideoObserver = new IntersectionObserver(function (entries, observer) {
    //         entries.forEach(function (video) {
    //             if (video.isIntersecting) {
    //                 for (var source in video.target.children) {
    //                     var videoSource = video.target.children[source];
    //                     if (typeof videoSource.tagName === "string" && videoSource.tagName === "SOURCE") {
    //                         videoSource.src = videoSource.dataset.src;
    //                     }
    //                 }

    //                 video.target.load();
    //                 video.target.classList.remove("lazy");
    //                 lazyVideoObserver.unobserve(video.target);
    //             }
    //         });
    //     });

    //     lazyVideos.forEach(function (lazyVideo) {
    //         lazyVideoObserver.observe(lazyVideo);
    //     });
    // } else {
    //     //alert('NO IntersectionObserver');
    //     $("video source").each(function () {
    //         var sourceFile = $(this).attr("data-src");
    //         $(this).attr("src", sourceFile);
    //         var video = this.parentElement;
    //         video.load();
    //         //video.play();
    //     });
    // }
});