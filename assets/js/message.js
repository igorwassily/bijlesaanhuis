$(function () {
	$('#msg-dismiss').on('click', dismissMsg);
});

function showMsg(text, type) {
	$('#msg-body').html(text);
	let msg = $('#msg');
	msg.removeClass();
	msg.addClass('msg--' + type);
	msg.slideDown();

	autoDismissMsg();
}

function autoDismissMsg() {
	setTimeout(dismissMsg, 5000);
}

function dismissMsg() {
	$('.msg-wrapper').slideUp();
}