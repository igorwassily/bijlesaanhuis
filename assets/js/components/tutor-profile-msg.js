$(document).ready(function() {
    $("#tutor-profile-send").on("click", function() {
        if($(document).width() <= 768) {
            $("#send-message-modal").addClass("visible");
        } else {
            alert("send message button clicked");
        }
    });
    $("#tutor-profile-send--modal").on("click", function() {
        alert("modal send message button clicked");
    });
    $("#send-message-modal__close, #send-message-modal").on("click", function() {
        $("#send-message-modal").removeClass("visible");
    });
    $(".tutor-profile-msg--modal").on("click", function(e) {
        e.stopPropagation();
    });
});
