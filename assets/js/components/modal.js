var _lat = 52.1326, _lng = 5.2913;
var _place_name = "";
var _isMapInitialise = true;
var _map = null;
var _canvas = null;
var ingelogd = 0;

function checkEmailAddress() {
    // test if e-mail already exists in db
    var ajax_url = '/app/actions/check-email-exists.php';
    var email = $("#email").val();
    var data = { email: email, usergroup: "Student" };

    $.ajax({
        url: ajax_url,
        type: "POST",
        data: data
    }).done(function (responseTxt) {
        console.log('response', responseTxt);
        if (responseTxt.indexOf("Duplicate") === -1) {
            // if duplicate warning has been shown, hide it
            //$(".register-duplicate").fadeOut();
            $("#email").removeClass("error");
            $(".duplicate-error").remove();
            //show the next fieldset
            next_fs.show();
            //hide the current fieldset with style
            current_fs.hide();

            scrollToModalTop();

            initMap();
        } else {
            // Show e-mail duplicate warning
            $("#email").addClass("error");
            $(".duplicate-error").remove();
            $("#email").after("<p class='form-error duplicate-error'>Het ingevoerde e-mailadres is al in gebruik</p>");
        }
    });
}

function initMap() {

    // SWITCH TO POSTCODE API HERE


    mapboxgl.accessToken = "pk.eyJ1IjoiYmlqbGVzYWFuaHVpcyIsImEiOiJjanVyNzhwbjIxb2pkM3ltdW51OHMwdXI5In0.u0dCwVetw_3tn1M_lfQfPw";
    _map = new mapboxgl.Map({
        container: '_map',
        style: 'mapbox://styles/mapbox/streets-v11',
        center: [5.55, 52.31667],
        zoom: 7
    });


    /*
    var _geocoder = new MapboxGeocoder({
    accessToken: mapboxgl.accessToken, limit:10, mode:'mapbox.places',countries: 'nl', placeholder:'Straatnaam en huisnummer'
    });

    _map.addControl(_geocoder);
    */

// After the map style has loaded on the page, add a source layer and default
// styling for a single point.
    var biggerSmaller;
    _map.on('load', function () {

        _map.resize();


// Listen for the `result` event from the MapboxGeocoder that is triggered when a user
// makes a selection and add a symbol that matches the result.
        _geocoder.on('result', function (ev) {
            console.log("Here: ", ev);
            _lat = ev.result.center[1];
            _lng = ev.result.center[0];
            _place_name = ev.result.place_name;
            _isAddressChanged = true;
            $("#_lat").val(_lat);
            $("#_lng").val(_lng);
            $("#_place_name").val(_place_name);
            _map.getSource('point').setData(ev.result.geometry);
        });
    });


    _canvas = _map.getCanvasContainer();

    var _geojson = {
        "type": "FeatureCollection",
        "features": [{
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [5.2913, 52.1326]
            }
        }]
    };

    function _onMove(e) {
        var coords = e.lngLat;

// Set a UI indicator for dragging.
        _canvas.style.cursor = 'grabbing';

// Update the Point feature in `geojson` coordinates
// and call setData to the source layer `point` on it.
        _geojson.features[0].geometry.coordinates = [coords.lng, coords.lat];
        _map.getSource('point').setData(_geojson);

    }

    function _onUp(e) {

        var coords = e.lngLat;
        console.log(e.lngLat);
        _lat = e.lngLat.lat;
        _lng = e.lngLat.lng;
        _isAddressChanged = true;

// Print the coordinates of where the point had
// finished being dragged to on the map.
        coordinates.style.display = 'block';
//coordinates.innerHTML = 'Longitude: ' + coords.lng + '<br />Latitude: ' + coords.lat;
        _canvas.style.cursor = '';
        _geocoder['eventManager']['limit'] = 1;
        _geocoder['options']['limit'] = 1;
        _geocoder.query(_lng + "," + _lat);


// Unbind mouse/touch events
        _map.off('mousemove', _onMove);
        _map.off('touchmove', _onMove);
        _geocoder['eventManager']['limit'] = 10;
        _geocoder['options']['limit'] = 10;
    }

    _map.on('load', function () {

// Add a single point to the map
        _map.addSource('point', {
            "type": "geojson",
            "data": _geojson
        });

        _map.addLayer({
            "id": "point",
            "type": "circle",
            "source": "point",
            "paint": {
                "circle-radius": 10,
                "circle-color": "#3887be"
            }
        });

// When the cursor enters a feature in the point layer, prepare for dragging.
        _map.on('mouseenter', 'point', function () {
            _map.setPaintProperty('point', 'circle-color', '#3bb2d0');
            _canvas.style.cursor = 'move';
        });

        _map.on('mouseleave', 'point', function () {
            _map.setPaintProperty('point', 'circle-color', '#3887be');
            _canvas.style.cursor = '';
        });

        _map.on('mousedown', 'point', function (e) {
// Prevent the default map drag behavior.
            e.preventDefault();

            _canvas.style.cursor = 'grab';

            _map.on('mousemove', _onMove);
            _map.once('mouseup', _onUp);
        });

        _map.on('touchstart', 'point', function (e) {
            if (e.points.length !== 1) return;

// Prevent the default map drag behavior.
            e.preventDefault();

            _map.on('touchmove', _onMove);
            _map.once('touchend', _onUp);
        });

    });

    $(".mapboxgl-ctrl-geocoder input").prop("required", true);
}


$(document).ready(function () {


    $("#huisnummer, #postcode").on("change keyup paste", function () {

        var houseNumber = $('#huisnummer').val();

        // POSTALCODE 2012ES
        // HOUSE NUMBER 30

        if (houseNumber.length > 0 && houseNumber.length <= 4) {
            console.log(houseNumber);
            var postalCode = $('#postcode').val();

            if (postalCode.length >= 4 && postalCode.length <= 6) {
                console.log(postalCode);

                $.ajax
                ({
                    type: "GET",
                    url: "/api/Geo/Address?postalCode=" + postalCode + "&streetNumber=" + houseNumber,
                    success: function (response) {
                            $('#errorMessage').css("display", "none");
                            let street = response.streetName;
                            let place = response.locality;

                            $('#straatnaam').val(street);
                            $('#woonplaats').val(place);

                            let lat = response.latitude;
                            let long = response.longitude;

                            $('#lat').val(lat);
                            $('#lng').val(long);

                            // BUILD WHOLE ADDRESS IN THIS FIELD
                            let placeName = street + " " + response.streetNumber + ", " + response.postalCode + " " + place + " Netherlands";

                            $('#place_name').val(placeName);
                    },
                    error: function (error) {
                        console.log("error");
                        console.log(error);
                        $('#errorMessage').css("display", "block");
                        $('#straatnaam').val("");
                        $('#woonplaats').val("");
                    }
                });
            }
        }
    });

    // triggers for the popup 
    if ($('.inloggen').hasClass('PopupBtn')) {
        $("#sendMsg").addClass('PopupBtn');
        $(".leerling-register").addClass('PopupBtnRegister');
        $(".open-message-modal").addClass('PopupBtn');
        $('.open-message-modal').removeAttr('id');

        // remove href to prevent loading a new page
        $(".leerling-register").removeAttr("href");
        $('#sendMsg').click(function (e) {
            e.preventDefault();
        });
    }


    // Get the modal 
    var modal = document.getElementById("login-register-modal");

    // Get the button that opens the modal
    var btn = document.getElementsByClassName("PopupBtn");

    // Get the <span> element that closes the modal
    var span = document.getElementsByClassName("close")[0];

    $(".inlog-btn").click(function () {
        showLoginDialogue();
    });

    $(".inlog-btn-text").click(function () {
        showLoginDialogue();
    });

    $(".inlog-btn-after-register").click(function () {
        $(".register-Succes").hide();
        showLoginDialogue();
    });

    $(".registreren-modal-btn").click(function () {
        showRegisterDialogue();
    });

    $("#registeer-from-login").click(function () {
        showRegisterDialogue();
    });
    // When the user clicks the button, open the modal 
    $(".PopupBtn").click(function () {
        /* If it is happening from Bericht verstuuren Button 
            on teachers-detailed-view then open keuzel modal
            otherwise open login modal*/
        console.log('$(".PopupBtn").click(function () {');
        if (($(this).attr('name') === "tutorprofileContactForm_SUBMIT" || $(this).hasClass('open-message-modal')) && location.href.indexOf('teacher-detailed-view') > 0) {
            showLoginAndRegisterDialogue();
        } else {
            showLoginDialogue();
        }
    });

    $(".PopupBtnRegister").click(function () {
        showRegisterDialogue();
    });

    function showRegisterDialogue() {
        $("#bijles-modal__content__keuzel").hide();
        $("#bijles-modal__content__login-success").hide();
        $("#bijles-modal__content__register-success").hide();
        $("#bijles-modal__content__inloggen").hide();
        $("#bijles-modal__content__register").show();
        showModal();
        trackEvent('Registration Student', 'Click', 'Start');
    }

    function showLoginDialogue() {
        $("#bijles-modal__content__keuzel").hide();
        $("#bijles-modal__content__login-success").hide();
        $("#bijles-modal__content__register-success").hide();
        $("#bijles-modal__content__register").hide();
        $("#bijles-modal__content__inloggen").show();
        showModal();
        // Resize recaptcha
        $('.g-recaptcha').css('transform-origin', '0 0');
        if ($('#login-email').outerWidth() <= 304) {
            $('.g-recaptcha').css({
                transform: 'scale(' + ($('#login-email').outerWidth() / $('.g-recaptcha').width()) + ')',
                paddingLeft: '8px'
            });
        } else {
            $('.g-recaptcha').css({
                transform: 'scale(1.0)',
                paddingLeft: '0px'
            });
        }
    }


    function showLoginAndRegisterDialogue() {
        $("#bijles-modal__content__keuzel").show();
        $("#bijles-modal__content__login-success").hide();
        $("#bijles-modal__content__register-success").hide();
        $("#bijles-modal__content__inloggen").hide();
        $("#bijles-modal__content__register").hide();
        showModal();
    }

    function showRegisterSuccess() {
        $("#bijles-modal__content__keuzel").hide();
        $("#bijles-modal__content__login-success").hide();
        $("#bijles-modal__content__register-success").show();
        $("#bijles-modal__content__inloggen").hide();
        $("#bijles-modal__content__register").hide();
        showModal();
        trackEvent('Registration Student', 'Click', 'Success');
    }

    function showLoginSuccess() {
        $("#bijles-modal__content__keuzel").hide();
        $("#bijles-modal__content__login-success").show();
        $("#bijles-modal__content__register-success").hide();
        $("#bijles-modal__content__inloggen").hide();
        $("#bijles-modal__content__register").hide();
        showModal();
    }

    function showModal() {
        console.log('function showModal() {');
        $('#login-register-modal').css('display','flex');
        // modal.style.display = "block";
        var width = $(".bijles-modal__content").width();
        $(".modal-tooltip").after().width(width);
    }

    function scrollToModalTop(divId = 'bijles-modal__header') {
        var aTag = $("div[name=\"" + divId + "\"]");
        $(".bijles-modal__content").animate({scrollTop: (aTag.offset().top)}, "slow");
    }

    // When the user clicks on <span> (x), close the modal
    $('.close').on('click', function () {
        $('#login-register-modal').hide();
    });

    // When the user clicks anywhere outside of the modal, close it
    window.onclick = function (event) {
        if (event.target == modal) {
            $('#login-register-modal').hide();
        }
    }
    //jQuery time
    var current_fs, next_fs, previous_fs; //fieldsets
    var left, opacity, scale; //fieldset properties which we will animate
    var animating; //flag to prevent quick multi-click glitches


    $("#next-part1").click(function () {
        var email = $("#email").val();
        var telephone = $("#telephone").val();
        var password = $("#passwordreg").val();
        var confirm_password = $("#confirm_password").val();
        var checkEmail = $("#checkEmail").val();

        var mailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var telregex = /^[\+ ]?[( ]?[0-9 ]{3}[) ]?[-\s\.]?[0-9 ]{3}[-\s\.]?[0-9 ]{4,6}$/;
        var passregex = /^.{8,}$/;

        if (mailregex.test(email)) {
            $("#email").removeClass("error");
            $(".email-error").remove();
            if (telregex.test(telephone)) {
                $("#telephone").removeClass("error");
                $(".telephone-error").remove();
                if (passregex.test(password)) {
                    $("#passwordreg").removeClass("error");
                    $(".password-error").remove();
                    if (password == confirm_password) {
                        $("#confirm-password").removeClass("error");
                        $(".confirm-error").remove();
 
                        current_fs = $(this).parent();
                        next_fs = $(this).parent().next();

                        //activate next step on progressbar using the index of next_fs
                        $("#progressbar li").eq($("fieldset").index(next_fs)).addClass("active");

                        // test if e-mail already exists in db
                        var ajax_url = '/app/actions/check-email-exists.php';

                        var data = {email: email, usergroup: "Student"};

                        $.ajax({
                            url: ajax_url,
                            type: "POST",
                            data: data
                        }).done(function (responseTxt) {
                            console.log('response', responseTxt);
                            if (responseTxt.indexOf("Duplicate") == -1) {
                                // if duplicate warning has been shown, hide it
                                //$(".register-duplicate").fadeOut(); 
                                $("#email").removeClass("error");
                                $(".duplicate-error").remove();
                                //show the next fieldset
                                next_fs.show();
                                //hide the current fieldset with style
                                current_fs.hide();

                                scrollToModalTop();

                                initMap();
                            } else {
                                // Show e-mail duplicate warning
                                $("#email").addClass("error");
                                $(".duplicate-error").remove();
                                $("#email").after("<p class='form-error duplicate-error'>Het ingevoerde e-mailadres is al in gebruik</p>");
                            }
                        });
                    } else {
                        $("#confirm_password").addClass("error");
                        $("#passwordreg").addClass("error");
                        $(".confirm-error").remove();
                        $("#confirm_password").after("<p class='form-error confirm-error'>De wachtwoorden komen niet overeen</p>");
                    }
                } else {
                    $("#passwordreg").addClass("error");
                    $(".password-error").remove();
                    $("#passwordreg").after("<p class='form-error password-error'>Gebruik minimaaal 8 tekens</p>");

                }
            } else {
                $("#telephone").addClass("error");
                $(".telephone-error").remove();
                $("#telephone").after("<p class='form-error telephone-error'>Vul een correct telefoonnummer in</p>");
            }
        } else {
            $("#email").addClass("error");
            $(".email-error").remove();
            $("#email").after("<p class='form-error email-error'>Vul een geldig e-mailadres in</p>");
        }
    });

    try {
        $('#msform').validate({
            errorPlacement: function (error, element) {
                return true;
            }
        });
    } catch (e) {
        console.log(e);
    }

    $("#submit-register").click(function () {

        $("#warningmsg_fn").attr('style', 'display: none !important; color:red !important;');
        $(".warningmsg_ln").attr('style', 'display: none !important; color:red !important;');
        $(".warningmsg_pf").attr('style', 'display: none !important; color:red !important;');
        $(".warningmsg_pl").attr('style', 'display: none !important; color:red !important;');
        $(".warningmsg_pe").attr('style', 'display: none !important; color:red !important;');

        var email = $("#email").val();
        var checkEmail = $("#checkEmail").val();
        var telephone = $("#telephone").val();
        var password = $("#passwordreg").val();
        var confirm_password = $("#confirm_password").val();
        var first_name = $("#first-name").val();
        var last_name = $("#last-name").val();
        var parent_first_name = $("#parent-first-name").val();
        var parent_last_name = $("#parent-last-name").val();
        var parent_email = $("#parent-email").val();
        var choice = $("#choice").val();
        var usergroup = $("#usergroup").val();
        var username = $("#username").val();

        var lat = $("#lat").val();
        var lng = $("#lng").val();
        var place_name = $("#place_name").val();

        var mailregex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        var letterregex = /[a-zA-Z]/

        if (first_name && letterregex.test(first_name)) {
            $("#first-name").removeClass("error");
            $(".first-error").remove();
            if (last_name && letterregex.test(last_name)) {
                $("#last-name").removeClass("error");
                $(".last-error").remove();
                if (parent_first_name && letterregex.test(parent_first_name)) {
                    $("#parent-first-name").removeClass("error");
                    $(".parent-first-error").remove();
                    if (parent_last_name && letterregex.test(parent_last_name)) {
                        $("#parent-last-name").removeClass("error");
                        $(".partent-last-error").remove();
                        if (mailregex.test(parent_email)) {
                            $("#parent-email").removeClass("error");
                            $(".parent-email-error").remove();

                            // $(".bijles-modal__register-success").fadeIn();
                            // $(".bijles-modal__register").hide();
                            showRegisterSuccess();
                            $("#reset").trigger("click"); // clicking reset button to reset form after completion

                            // Action will be function name which we will create in functions.php
                            var action = "submit_request_a_quote";

                            // this is required url for ajax calls in the wordpress
                            var ajax_url = '/app/actions/register.php';

                            var data = {
                                email: email,
                                checkEmail: checkEmail,
                                telephone: telephone,
                                password: password,
                                confirm_password: confirm_password,
                                first_name: first_name,
                                last_name: last_name,
                                parent_first_name: parent_first_name,
                                parent_last_name: parent_last_name,
                                parent_email: parent_email,
                                choice: choice,
                                usergroup: usergroup,
                                username: username,
                                lat: lat,
                                lng: lng,
                                place_name: place_name
                            };


                            $.ajax({
                                url: ajax_url,
                                type: "POST",
                                data: data
                            }).done(function (responseTxt) {
                                // if (responseTxt.indexOf("Duplicate") == -1) {
                                //     $(".register-Succes").fadeIn();
                                //     $(".registeren").hide();
                                //     $("#reset").trigger("click"); // clicking reset button to reset form after completion
                                // } else {
                                //     $(".register-duplicate").fadeIn();
                                // }
                            });
                            return false;

                        } else {
                            $("#parent-email").addClass("error");
                            $(".parent-email-error").remove();
                            $(".warningmsg_pe").attr('style', 'display: block !important; color:red !important;');
                        }
                    } else {
                        $("#parent-last-name").addClass("error");
                        $(".parent-last-error").remove();
                        $(".warningmsg_pl").attr('style', 'display: block !important; color:red !important;');
                    }
                } else {
                    $("#parent-first-name").addClass("error");
                    $(".parent-first-error").remove();
                    $(".warningmsg_pf").attr('style', 'display: block !important; color:red !important;');
                }
            } else {
                $("#last-name").addClass("error");
                $(".last-error").remove();
                $(".warningmsg_ln").attr('style', 'display: block !important; color:red !important;');
            }
        } else {
            $("#first-name").addClass("error");
            $(".first-error").remove();
            $("#warningmsg_fn").attr('style', 'display: block !important; color:red !important;');
        }
    });


    $(".previous").click(function () {

        current_fs = $(this).parent();
        previous_fs = $(this).parent().prev();

        //de-activate current step on progressbar
        $("#progressbar li").eq($("fieldset").index(current_fs)).removeClass("active");

        //hide the current fieldset with style
        current_fs.hide();

        //show the previous fieldset
        previous_fs.fadeIn();

        scrollToModalTop();
    });

    $(".submit").click(function () {
        return false;
    });

    $(window).resize(function () {
        var width = $(".bijles-modal__content").width();
        $(".bijles-modal__tooltip").after().width(width);
    });
    
    $('#login-modal').on('click', function () {
        console.log("$('#login-modal').on('click', function () {");
        var $captcha = $('#recaptcha'),
            response = grecaptcha.getResponse();
    
        if (response.length === 0) {
            $('.msg-error').text("");
            if (!$captcha.hasClass("error")) {
                $captcha.addClass("error");
            }
        } else {
            $('.msg-error').text('');
            $captcha.removeClass("error");
        }
    
        // test if e-mail is verified
        var ajax_url = '/app/actions/check-email-exists.php';
    
        var data = {email: $('#login-email').val()};
    
        $(".js-inlog-error").hide();
        $(".js-mail-correct").hide();
    
        $.ajax({
            url: ajax_url,
            type: "POST",
            data: data
        }).done(function (responseTxt) {
            console.log('response', responseTxt);
            if (responseTxt.indexOf("Unverified") == -1 && responseTxt.indexOf("Duplicate") > -1) {
                $("#login-email").removeClass("error");
                $(".verify-email-error").remove();
    
                if (grecaptcha.getResponse().length > 0) {
                    // Action will be function name which we will create in functions.php
                    var action = "submit_request_a_quote";
                    // this is required url for ajax calls in the wordpress
                    var ajax_url = '/app/actions/inlog.php';
    
                    var data = {email: $('#login-email').val(), password: $('#login-password').val()};
    
                    $.ajax({
                        url: ajax_url,
                        type: "POST",
                        data: data
                    }).done(function (responseTxt) {
                        if (responseTxt == "succes") {
                            location.reload();
                        } else {
                            $(".js-inlog-error").fadeIn();
                        }
                    });
                }
            } else if (responseTxt.indexOf("Duplicate") > -1) {
                // Show e-mail duplicate warning
                $("#login-email").addClass("error");
                $(".verify-email-error").remove();
                $("#login-email").after("<p class='form-error verify-email-error'>Bevestig uw e-mail adres met de bevestigingslink</p>");
            } else {
                $(".js-mail-correct").fadeIn();
            }
        });
    
        return false;
    });
});



//$('#login_form').submit(function(){
// var submitstate = true;


// var $captcha = $( '#recaptcha' ),
//     response = grecaptcha.getResponse();

// if (response.length === 0) {
//   $( '.msg-error').text( "" );
//   if( !$captcha.hasClass( "error" ) ){
//     $captcha.addClass( "error" );
//     submitstate = false;
//     return false;
//   }
// } else {
//   $( '.msg-error' ).text('');
//   $captcha.removeClass( "error" );
//     //return true;
// }

// // test if e-mail is verified
// var ajax_url 		= '/check-email-exists.php';

// var data 			= {email:$('#login-email').val()};

// $.ajax({
//     url:ajax_url,
//     type:"POST",
//     data:data
// }).done(function(responseTxt){
//     console.log('response',responseTxt);
//     if(responseTxt.indexOf("Unverified") == -1){
//         // if duplicate warning has been shown, hide it
//         //$(".register-duplicate").fadeOut();
//         $("#login-email").removeClass("error");
//         $(".verify-email-error").remove();
//     }else{
//         // Show e-mail duplicate warning
//         $("#login-email").addClass("error");
//         $(".verify-email-error").remove();
//         $("#login-email").after("<p class='form-error verify-email-error'>Bevestig uw e-mail adres met de bevestigingslink</p>");
//         submitstate = false;
//     }

//     // if(submitstate) {
//     //     $('#login_form').submit();
//     // }
// });
//});

// Make recaptcha required
window.onload = function () {
    var $recaptcha = document.querySelector('#g-recaptcha-response');

    if ($recaptcha) {
        $recaptcha.setAttribute("required", "required");
    }
};