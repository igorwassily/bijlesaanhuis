$( document ).ready(function() {
    // toggle switch control
    $(".toggle-switch .toggle-switch__button").on("click", function() {
        let clickedButton = this;
        if(!$(clickedButton).hasClass("active")) {
            // change active appearance
            $(clickedButton).siblings(".toggle-switch__button").removeClass("active");
            $(clickedButton).addClass("active");

            // change hidden input value
            let dataTitle = $(clickedButton).attr("data-title");
            let regex = new RegExp("1");

            if(regex.test(dataTitle)) {
                $(clickedButton).closest('.toggle-switch__block').siblings('.toggle-switch__hidden').val(1);
            } else {
                $(clickedButton).closest('.toggle-switch__block').siblings('.toggle-switch__hidden').val(0);
            }

        }
    });
});