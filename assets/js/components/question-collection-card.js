$(document).ready(function () {
    $('.question-collection__question--title').on('click', function () {
        $(this).parent().find('.question-collection__question--description').toggle('1200');
        const icon = $(this).parent().find('.question-collection__question--title-icon i');
        $(icon).fadeOut(function () {
            if ($(icon).hasClass('fa-plus-circle')) {
                $(icon).addClass('fa-minus-circle');
                $(icon).removeClass('fa-plus-circle');
            } else {
                $(icon).addClass('fa-plus-circle');
                $(icon).removeClass('fa-minus-circle');
            }
            $(icon).fadeIn();
        });
    });
});