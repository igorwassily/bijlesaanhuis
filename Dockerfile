# download dependencies with composer
FROM composer:1 as vendor

COPY composer.json composer.json

COPY laravel laravel

RUN composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --no-dev \
    --prefer-dist
    
RUN cd laravel && composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --no-dev \
    --prefer-dist

# run apache with newest php 7
FROM php:7-apache

# copy files (don't forget required shared volumes)
COPY --chown=www-data:www-data api /var/www/api
COPY --chown=www-data:www-data assets /var/www/assets
COPY --chown=www-data:www-data cron /var/www/cron
COPY --chown=www-data:www-data inc /var/www/inc
COPY --chown=www-data:www-data public /var/www/html
COPY --chown=www-data:www-data --from=vendor /app/vendor /var/www/vendor
COPY --chown=www-data:www-data --from=vendor /app/laravel/vendor /var/www/laravel/vendor
COPY --chown=www-data:www-data laravel /var/www/laravel

# install project dependencies
RUN apt update && apt install -y locales libicu-dev unzip && \
	sed -i 's/^# *\(nl_NL.UTF-8\)/\1/' /etc/locale.gen && \
	locale-gen && \
	docker-php-ext-configure intl && \
	docker-php-ext-install intl && \
	docker-php-ext-install mysqli && \
	docker-php-ext-install pdo_mysql && \
	a2enmod rewrite && \
	mv "$PHP_INI_DIR/php.ini-production" "$PHP_INI_DIR/php.ini" && \
# clean up
	apt purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false -o APT::AutoRemove::SuggestsImportant=false $BUILD_LIBS && \
	apt -y autoremove && \
    apt clean && \
	rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /var/cache/*

# push version into image
ARG v
ARG vl
ENV VERSION=$v
ENV VERSION_LINK=$vl

# initialize container at startup
COPY init.sh /usr/local/bin/init.sh
RUN chmod +x /usr/local/bin/init.sh
ENTRYPOINT [ "init.sh" ]