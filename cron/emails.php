<?php

chdir(__DIR__);

include "../public/app/includes/connection.php";
include "../inc/Email.php";

try {
	$now = new DateTime('now', new DateTimeZone("Europe/Amsterdam"));
} catch (Exception $e) {
	die("failed to create time object: " . $e->getMessage());
}

$time = $now->format("H:i:s");
if ($time >= "23:00:00" || $time <= "06:30:00") {
	die("sleeping time ($time)");
}

// SECTION Reminder Consultation Teacher
$query = $con->query("SELECT cb.admincalendarbookingID FROM admincalendarbooking cb JOIN teacher t on cb.teacherID = t.userID LEFT JOIN admin_msg_read m ON m.type = 5 AND m.userID = t.teacherID WHERE m.userID IS NULL AND STR_TO_DATE(CONCAT(cb.datee, ' ', cb.starttime), '%Y-%m-%d %k:%i') BETWEEN DATE_ADD(NOW(), INTERVAL 1 HOUR) AND DATE_ADD(NOW(), INTERVAL 5 HOUR)");

while ($row = $query->fetch_assoc()) {
	$email = new Email($db, 'consultation_reminder_for_tutor');
	$email->Prepare($row['admincalendarbookingID']);

	if ($email->Send('teacher')) {
		$con->query("INSERT INTO admin_msg_read (`type`, userID) VALUES (5, {$row['teacherID']})");
	} else {
		echo "failed to send mail to {$row['email']}\n";
	}
}
