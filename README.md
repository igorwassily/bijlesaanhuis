[![Build Status](https://drone.bijlesaanhuis.nl/api/badges/Bijles-aan-Huis-B-V/bijlesaanhuis/status.svg)](https://drone.bijlesaanhuis.nl/Bijles-aan-Huis-B-V/bijlesaanhuis)

# Bijles aan Huis

## Directory structure
* Application root
  * api  
    The new API, currently for calendar, teacher search, geo information and templates.  
    Every endpoint has to be a child of the class Endpoint. Public methods can be called with the url `/api/{endpoint}/{method}`
  * assets  
    Contains smarty templates, javascript and css files. They will be merged and minified on the first request
  * cron  
    Contains files which are executed as cron jobs
  * inc  
    Contains the business logic of the project which is not included in the api
  * public  
    This is the web root. Files inside can be directly accessed with the url 
    * app  
      The old app directory (deprecated, will be removed)
    * font  
      Contains fonts
    * img  
      Contains small static images

## Setup environment

Docker is used to deploy the project. The docker-compose file describes the environment and contains this project itself, a database, a database management tool and mailcatcher (non-production only). Copy the file *docker-compose.yml.sample* to *docker-compose.yml* (don't commit this file). Volume shares have to be updated, the local paths start with `/path/to/` in the sample configuration.

### Volume shares
* Service database
  * `/path/to/db:/var/lib/mysql`
* Service web (this project, only required for development)
  * `/path/to/bijlesaanhuis\api:/var/www/api`  
    Synchronize files with host (development only)
  * `/path/to/bijlesaanhuis/assets:/var/www/assets`  
    Synchronize files with host (development only)
  * `/path/to/bijlesaanhuis/inc:/var/www/inc`  
    Synchronize file with host (development only)
  * `/path/to/bijlesaanhuis/public/app:/var/www/html/app`  
    Synchronize files with host (development only)

### Install docker
[Install docker](https://runnable.com/docker/getting-started/) and on windows [add a shared drive](https://blogs.msdn.microsoft.com/stevelasker/2016/06/14/configuring-docker-for-windows-volumes/)

### Build and run
Build the docker image of this project locally (`docker-compose build`) and start/update the container (`docker-compose up -d`)

### Cron jobs
Cron jobs have to run inside the container, but they are triggered by the host. Add the following entry (it's an example) to `/etc/crontab`:
```
*/10 *  * * * root docker exec -u www-data web php -f /var/www/cron/emails.php
```
The script `cron/email.php` of the container `web` will be executed every 10 minutes.

### Database
The databases is included in docker-compose.yml. It is mapped to the port 3307 (by default) of the host. The service adminer is a simple web-based management tool and can be accessed over the port 8007 (by default) of the host.

### Mailcatcher
To receive emails locally it is required to setup a smtp host. Mailcatcher is included in docker-compose.yml, the inbox can be accessed by `localhost:1080`.
  
Notice: the *from* field is always overridden

## Workflow with git
Before coding or pushing always pull the latest changes. Working changes should be committed and if the feature/fix is ready, push these commits to the desired branch (they are explained below).

Every sprint has its own branch (named `sprint/{year}-{number-of-sprint}`, the number of sprint contains always two digits), which is the primary one. Every push into this branch is automatically deployed to https://sprint.bijlesaanhuis.nl, please test your work there.

The sprint branch can be directly used for small bug fixes or other small changes, but the default behavior is creating a new branch for every bug/feature. There is no naming convention yet, but it should be meaningful (e. g. issue id). Don't use general expressions. These changes have to be tested local and in case everything works, a pull request to the sprint branch has to be created. The lead of the department will review the changes and perform the merge. After this, the feature has to be tested along the other changes on https://sprint.bijlesaanhuis.nl.

At the end of a sprint, the sprint branch is merged into master and verified on https://staging.bijlesaanhuis.nl. Product owner and dev lead will decide to deploy it to production. By creating a tag, it is automatically deployed to production.

In case an emergency bug occurs ('showstopper'), a branch from the current tag has to be created. The bug will be fixed within this branch and the fix will be deployed again by creating a new tag. 

## Staging/sprint environment

Before publishing changes, the project should be tested on a nearly live environment.  
The sprint environment is accessible by https://sprint.bijlesaanhuis.nl.
Changes on a sprint branch are automatically deployed.  
The staging environment is accessible by https://staging.bijlesaanhuis.nl.
Changes on master are automatically deployed.

Mails are caught and available on https://mail.staging.bijlesaanhuis.nl. The *from* field will be overridden.
