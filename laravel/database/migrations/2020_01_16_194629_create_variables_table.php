<?php

use App\Models\Variable;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateVariablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('variables', function (Blueprint $table) {
            $table->string('name')->primary();
            $table->string('value');
        });

        Variable::create([
            'name' => 'lessonDurationStep',
            'value' => '15'
        ]);
        Variable::create([
            'name' => 'lessonDurationMin',
            'value' => '45'
        ]);
        Variable::create([
            'name' => 'lessonDurationMax',
            'value' => '240'
        ]);
        Variable::create([
            'name' => 'consultationDurationStep',
            'value' => '15'
        ]);
        Variable::create([
            'name' => 'consultationDurationMin',
            'value' => '45'
        ]);
        Variable::create([
            'name' => 'consultationDurationMax',
            'value' => '240'
        ]);
        Variable::create([
            'name' => 'freeTravelRange',
            'value' => '3.5'
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('variables');
    }
}
