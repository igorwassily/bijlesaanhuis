<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeacherSlotsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacherslots', function (Blueprint $table) {
            $table->bigIncrements('slotID');
            $table->integer('teacherID');
            $table->string('title')->nullable();
            $table->date('datee')->nullable();
            $table->date('datee_end')->nullable();
            $table->string('mon_time', 20)->default('');
            $table->string('tue_time', 20)->default('');
            $table->string('wed_time', 20)->default('');
            $table->string('thur_time', 20)->default('');
            $table->string('fri_time', 20)->default('');
            $table->string('sat_time', 20)->default('');
            $table->string('sun_time', 20)->default('');
            $table->integer('status')->default(1);
            $table->text('mon_additional')->default('');
            $table->text('tue_additional')->default('');
            $table->text('wed_additional')->default('');
            $table->text('thur_additional')->default('');
            $table->text('fri_additional')->default('');
            $table->text('sat_additional')->default('');
            $table->text('sun_additional')->default('');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacherslots');
    }
}
