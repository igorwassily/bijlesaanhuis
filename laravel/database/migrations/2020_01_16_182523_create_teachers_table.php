<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTeachersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('teacher', function (Blueprint $table) {
            $table->bigIncrements('teacherID');
            $table->integer('userID');
            $table->integer('teacherclassID')->nullable();
            $table->integer('contactID');
            $table->integer('profileID')->nullable();
            $table->integer('courseID')->default(0);
            $table->integer('educationID')->nullable();
            $table->string('current_education', 170)->nullable();
            $table->integer('customhourly')->nullable();
            $table->string('profileurl')->nullable();
            $table->boolean('onlineteaching')->default(0);
            $table->boolean('allownewstudents')->default(0);
            $table->string('bankaccount', 170)->nullable();
            $table->string('contract', 170)->nullable();
            $table->double('travelRange');
            $table->integer('requested_teacherlevel_rate_ID')->nullable();
            $table->enum('permission', ['Requested', 'Accepted', 'Rejected', ''])->nullable();
            $table->integer('teacherlevel_rate_ID')->nullable();
            $table->text('admin_comments')->nullable();
            $table->boolean('isConfirmed')->default(0);
            $table->integer('consulationType')->default(0);
            $table->integer('reminder')->default(0);
            $table->integer('label_docenten_1')->default(0);
            $table->integer('label_docenten_2')->default(0);
            $table->timestamp('date_label_set_1')->nullable();
            $table->timestamp('date_label_set_2')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('teacher');
    }
}
