<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('appointments', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('student');
            $table->integer('teacher');
            $table->integer('type'); // 0 => tutoring; 1 => consultation
            $table->timestamp('begin');
            $table->timestamp('end');
            $table->integer('duration');
            $table->boolean('online');
            $table->string('level_int')->nullable();
            $table->string('level_ext')->nullable();
            $table->double('amount_int')->nullable();
            $table->double('amount_ext')->nullable();
            $table->double('travel_cost')->nullable();
            $table->double('travel_distance')->nullable();
            $table->double('latitude_teacher')->nullable();
            $table->double('longitude_teacher')->nullable();
            $table->double('latitude_student')->nullable();
            $table->double('longitude_student')->nullable();
            $table->integer('status'); // 0 => requested; 1 => accepted; 2 => taken; 3 => canceled
            $table->integer('status_by');
            $table->timestamp('status_time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
