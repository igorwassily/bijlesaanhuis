<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarBookingTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendarbooking', function (Blueprint $table) {
            $table->bigIncrements('calendarbookingID');
            $table->integer('studentID');
            $table->integer('teacherID');
            $table->integer('slotID')->nullable();
            $table->string('slot_group_id', 50)->nullable();
            $table->boolean('isgapslot')->default(0);
            $table->integer('calendarstatusID')->nullable();
            $table->date('datee');
            $table->string('starttime', 10);
            $table->string('endtime', 10);
            $table->integer('duration');
            $table->string('internal_bookingamount');
            $table->string('external_bookingamount');
            $table->string('travel_cost');
            $table->integer('travel_distance_category_id')->nullable();
            $table->string('travel_distance');
            $table->string('internal_teacher_level')->nullable();
            $table->string('external_teacher_level')->nullable();
            $table->boolean('paidclient')->nullable();
            $table->string('latitude')->nullable();
            $table->string('longitude')->nullable();
            $table->string('placename')->nullable();
            $table->boolean('accepted')->default(0);
            $table->boolean('isClassOnline')->default(0);
            $table->boolean('isClassTaken')->default(0);
            $table->boolean('isSlotCancelled')->default(0);
            $table->timestamp('timestamp')->useCurrent();
            $table->integer('status')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendarbooking');
    }
}
