<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\CalendarBooking;
use App\Models\Teacher;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(CalendarBooking::class, function (Faker $faker) {
    return [
        'teacherID' => $faker->randomNumber(),
        'studentID' => $faker->randomNumber(),
        'internal_bookingamount' => '',
        'external_bookingamount' => '',
        'travel_cost' => '',
        'travel_distance' => ''
    ];
});
