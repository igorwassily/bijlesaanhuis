<?php

use App\User;
use Faker\Factory;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    public function run()
    {
        User::truncate();

        $faker = Factory::create();

        $password = Hash::make('12345678');

        User::create([
            'email' => 'admin@tutor.dev.we-think.nl',
            'password' => Hash::make('Admin@2024'),
            'usergroupID' => 3,
            'active' => 1,
            'disabled' => 0
        ]);

        User::create([
            'email' => 'pihog@vxmail2.net',
            'password' => $password,
            'usergroupID' => 2,
            'active' => 1,
            'disabled' => 0
        ]);

        User::create([
            'email' => 'koweyagur@alltopmail.com',
            'password' => $password,
            'usergroupID' => 1,
            'active' => 1,
            'disabled' => 0
        ]);

        for ($i = 0; $i < 50; $i++) {
            User::create([
                'email' => $faker->email,
                'password' => $password,
                'usergroupID' => $faker->boolean ? 1 : 2,
                'active' => 1,
                'disabled' => 0
            ]);
        }
    }
}
