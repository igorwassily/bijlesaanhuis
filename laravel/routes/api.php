<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (Request $request) {
    return DB::table('admin')->get();
});

/*
 * User section
 */
Route::post('register/student', 'Auth\RegisterController@student');
Route::post('register/teacher', 'Auth\RegisterController@teacher');
Route::post('login', 'Auth\LoginController@login');
Route::middleware('auth:api')->post('logout', 'Auth\LoginController@logout');

/*
 * Teacher section
 */
Route::get('teacher/{id}/available', 'TeacherController@available');
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('teacher', 'TeacherController@list');
    Route::get('teacher/{id}', 'TeacherController@get');
    Route::get('teacher/{id}/appointment', 'TeacherController@appointments');
    Route::get('teacher/{id}/appointment/{CalendarBooking}', 'TeacherController@appointment');
    Route::get('teacher/{id}/appointment/pending', 'TeacherController@pending');
    Route::get('teacher/{id}/appointment/accepted', 'TeacherController@accepted');
    Route::get('teacher/{id}/appointment/taken', 'TeacherController@taken');
    Route::get('teacher/{id}/appointment/canceled', 'TeacherController@canceled');
    Route::post('teacher/{id}/appointment', 'TeacherController@book');
    Route::delete('teacher/{id}/appointment/{CalendarBooking}', 'TeacherController@cancel');

    Route::group(['prefix' => 'geo'], function() {
        Route::get('address', 'GeoController@address')->name('geo.address');
        Route::get('coordinate', 'GeoController@coordinate')->name('geo.coordinate');
    });
});
