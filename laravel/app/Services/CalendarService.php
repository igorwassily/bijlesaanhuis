<?php

namespace App\Services;

use App\Contracts\CalendarServiceInterface;
use App\Contracts\PriceServiceInterface;
use App\Models\CalendarBooking;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use App\Models\Variable;
use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use Illuminate\Support\Facades\Auth;

class CalendarService implements CalendarServiceInterface
{
    protected ?User $user;
    protected PriceServiceInterface $priceService;

    public function __construct(PriceServiceInterface $priceService)
    {
        $this->user = Auth::guard('api')->user();
        $this->priceService = $priceService;
    }

    public function calcAvailabilityConsultation(DateTime $from, DateTime $to)
    {
        // TODO: Implement calcAvailabilityConsultation() method.
    }

    public function calcAvailabilityTeacher(Teacher $teacher, DateTime $from, DateTime $to)
    {
        $future = new DateTime();
        $future->add(new DateInterval('P1D'));
        if ($from < $future) $from = $future;

        $tz = $this->user ? $this->user->timeZone() : new DateTimeZone('Europe/Amsterdam');

        $step = Variable::where('name', 'lessonDurationStep')->first()->value;
        $minimum = Variable::where('name', 'lessonDurationMin')->first()->value;
        $maximum = Variable::where('name', 'lessonDurationMax')->first()->value;

        $absent = $teacher->absentDays();
        $slots = $this->slots($teacher);
        $booked = $this->retrieveAppointments($teacher, $from, $to, 10);

        $available = [];

        $day = $from;

        $today = date('Y-m-d');
        try {
            $dateTime = new DateTime('now', $tz);
        } catch (Exception $e) {
            $dateTime = new DateTime();
        }
        $dateTime->add(new DateInterval('P1D'));

        // MONTHLY TIME RANGE
        while ($day <= $to) {
            // Weekday
            $wd = strtolower($day->format('D'));

            $year = $day->format('Y');
            $month = $day->format('m');
            $d = $day->format('d');
            $date = "$year-$month-$d";

            if (!isset($absent[$date]) && $date > $today && isset($slots[$wd])) {
                foreach ($slots[$wd] as $slot) {
                    $slot['begin']->setDate($year, $month, $d);
                    /** @var DateTime $begin */
                    $begin = clone $slot['begin'];
                    $slot['end']->setDate($year, $month, $d);
                    /** @var DateTime $end */
                    $end = clone $slot['begin'];
                    $end = $end->add(new DateInterval("PT{$step}M"));
                    while ($end <= $slot['end']) {
                        $is = $begin > $dateTime;

                        if (!empty($booked[$date]) && $is) {
                            foreach ($booked[$date] as $item) {
                                if ($begin >= $item['begin'] && $begin <= $item['end'] || $end >= $item['begin'] && $end <= $item['end']) {
                                    $is = false;
                                    break;
                                }
                            }
                        }

                        if ($is) {
                            $available[$date][] = [
                                "begin" => $begin->format('H:i'),
                                "end" => $end->format('H:i'),
                            ];
                        }
                        $begin = $begin->add(new DateInterval("PT{$step}M"));
                        $end = $end->add(new DateInterval("PT{$step}M"));
                    }
                }


                if (!empty($available[$date])) {
                    $available[$date] = $this->consolidate($available[$date], $minimum, $maximum, $step);
                }
            }

            $day = $day->add(new DateInterval('P1D'));
        }

        return $available;
    }

    public function calcAvailabilityStudent(Student $student, DateTime $from, DateTime $to)
    {
        // TODO: Implement calcAvailabilityStudent() method.
    }

    public function convertToFullCalendar(array $data, $grouped)
    {
        // TODO: Implement convertToFullCalendar() method.
    }

    private function consolidate(array $day, int $pMin, int $pMax, int $pStep)
    {
        $data = [];

        $globalMax = 0;
        $max = 0;
        $lastBegin = null;
        for ($i = sizeof($day) - 1; $i >= 0; $i--) {
            if ($day[$i]['end'] == $lastBegin) {
                $max += $pStep;
            } else {
                $max = $pStep;
            }

            $lastBegin = $day[$i]['begin'];
            if ($max >= $pMin) {
                $data[] = [
                    'begin' => $day[$i]['begin'],
                    'min' => $pMin,
                    'max' => $max
                ];
                if ($max > $globalMax) {
                    $globalMax = $max;
                }
            }
        }

        $data = array_reverse($data);
        $globalMax = $globalMax >= 90 ? 90 : $pMin;
        for ($i = sizeof($data) - 1; $i >= 0; $i--) {
            if ($data[$i]['max'] >= $globalMax) {
                $data[$i]['suggested'] = true;
                break;
            }
        }

        return $data;
    }

    private function slots(Teacher $teacher)
    {
        $tblSlot = $teacher->slots()->firstOrFail();
        if ($this->user) {
            $tz = $this->user->timeZone();
        } else {
            $tz = new DateTimeZone('Europe/Amsterdam');
        }

        $wdNl = [
            'mon' => 'mon',
            'tue' => 'din',
            'wed' => 'woe',
            'thu' => 'don',
            'fri' => 'vri',
            'sat' => 'zat',
            'sun' => 'zon',
        ];

        $slots = [
            'mon' => [],
            'tue' => [],
            'wed' => [],
            'thu' => [],
            'fri' => [],
            'sat' => [],
            'sun' => [],
        ];

        $dow = 0;
        foreach ($slots as $day => &$data) {
            $dow++;
            $field = $day == 'thu' ? 'thur' : $day;
            $time = $tblSlot["{$field}_time"];
            if (!empty($time)) {
                $time = explode('-', $time);

                $d = [
                    'begin' => DateTime::createFromFormat('G:i', $time[0], $tz),
                    'end' => DateTime::createFromFormat('G:i', $time[1], $tz)
                ];

                if ($d['begin'] == false || $d['end'] == false) {
                    continue;
                }

                $data[] = $d;
            }

            $times = json_decode($tblSlot["{$field}_additional"], true);
            $day = $wdNl[$day];

            if (!empty($times)) {
                $i = 1;
                foreach ($times as $item) {
                    if (!isset($item["{$day}$i"])) {
                        continue;
                    }
                    $time = explode(':', $item["{$day}$i"]);

                    if (sizeof($time) <= 3) {
                        continue;
                    }

                    $d = [
                        'begin' => DateTime::createFromFormat('G:i', "{$time[0]}:{$time[1]}", $tz),
                        'end' => DateTime::createFromFormat('G:i', "{$time[2]}:{$time[3]}", $tz)
                    ];

                    if ($d['begin'] == false || $d['end'] == false) {
                        continue;
                    }

                    $data[] = $d;
                    $i++;
                }
            }

            usort($data, function ($i1, $i2) {
                if ($i1['begin'] == $i2['begin']) {
                    return 0;
                } elseif ($i2['begin'] > $i1['begin']) {
                    return -1;
                } else {
                    return 1;
                }
            });
        }

        return $slots;
    }

    public function retrieveAppointments(Teacher $teacher, ?DateTime $from, ?DateTime $to, $status = -1, $grouped = false)
    {
        $groupKey = [
            0 => 'pending',
            1 => 'accepted',
            2 => 'taken',
            3 => 'canceled'
        ];

        $data = $teacher->appointments($from, $to, $status)->get();

        $appointments = [];

        foreach ($data as $entry) {
            $d = $this->parseAppointment($entry);

            if ($status == 10) {
                $appointments[$entry->datee][] = $d;
            } elseif ($grouped) {
                $appointments[$groupKey[$d['status']]][] = $d;
            } else {
                $appointments[] = $d;
            }
        }

        return $appointments;
    }

    public function parseAppointment(CalendarBooking $booking)
    {
        $appointment = [
            'begin' => new DateTime("{$booking->datee} {$booking->starttime}:00"),
            'end' => new DateTime("{$booking->datee} {$booking->endtime}:00"),
            'duration' => $booking->duration,
        ];
        if ($booking->isSlotCanceled) {
            $appointment['status'] = 3;
        } elseif ($booking->isClassTaken) {
            $appointment['status'] = 2;
        } elseif ($booking->accepted) {
            $appointment['status'] = 1;
        } else {
            $appointment['status'] = 0;
        }

        return $appointment;
    }
}
