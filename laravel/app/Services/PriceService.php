<?php

namespace App\Services;

use App\Contracts\PriceServiceInterface;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\Variable;

class PriceService implements PriceServiceInterface
{
    /**
     * Calculates the great-circle distance between two points, with
     * the Haversine formula.
     * @param float $latitude1 Latitude of start point in [deg decimal]
     * @param float $longitude1 Longitude of start point in [deg decimal]
     * @param float $latitude2 Latitude of target point in [deg decimal]
     * @param float $longitude2 Longitude of target point in [deg decimal]
     * @return float Distance between points in [km] (same as earthRadius)
     */
    public function distanceByCoordinates($latitude1, $longitude1, $latitude2, $longitude2): float
    {
        $earthRadius = 6371.000; // result in [km]

        $latFrom = deg2rad($latitude1);
        $lonFrom = deg2rad($longitude1);
        $latTo = deg2rad($latitude2);
        $lonTo = deg2rad($longitude2);

        $latDelta = $latTo - $latFrom;
        $lonDelta = $lonTo - $lonFrom;

        $angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
                cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

        return round($angle * $earthRadius, 3);
    }

    public function distance(Teacher $teacher, Student $student): float
    {
        return $this->distanceByCoordinates($teacher->contact()->latitude, $teacher->contact()->longitude, $student->contact()->latitude, $student->contact()->longitude);
    }

    public function price(Teacher $teacher, Student $student, int $duration): array
    {
        $distance = $this->distance($teacher, $student);
        $level = $teacher->level();

        $freeRange = Variable::where('name', 'freeTravelRange')->first()->value;

        return [
            'level' => $level->internal_level,
            'internal' => round(($duration * $level->internal_rate) / 45, 2),
            'external' => round(($duration * $level->rate) / 45, 2),
            'distance' => $distance,
            'travel' => $this->travelCost($distance, $freeRange, $level->cost_per_km),
        ];
    }

    public function travelCost(float $distance, float $freeRange, float $price): float
    {
        $price = round($price * ($distance - $freeRange), 2);
        if ($price < 0) $price = 0;
        return $price;
    }
}
