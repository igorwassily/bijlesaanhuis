<?php

namespace App\Services;

use Geocoder\Model\Coordinates;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use PostcodeNl_Api_RestClient;
use Geocoder\StatefulGeocoder;

class Geo
{
    /**
     * @var PostcodeNl_Api_RestClient
     */
    private $geoCoderNl;

    /**
     * @var StatefulGeocoder
     */
    private $geoCoder;

    public function __construct()
    {
        $this->geoCoder = app('geoCoder');
        $this->geoCoderNl = app('geoCoderNl');
    }

    public function getAddressInfo(string $address, string $postalCode, string $streetNumber = null): ?array
    {
        try {
            if (!empty($postalCode) && !empty($streetNumber)) {
                $data = $this->geoCoderNl->lookupAddress($postalCode, $streetNumber);
                return $this->_formatNl($data);
            } else {
                $query = GeocodeQuery::create(' ');
                if (!empty($address)) $query = $query->withText($address);
                elseif (!empty($postalCode)) {
                    $query = $query->withText('');
                    $query = $query->withData("components", "country:NL|postal_code:$postalCode");
                }

                $result = $this->geoCoder->geocodeQuery($query);
                if ($result->count() == 0) {
                    return null;
                }

                return $result->first()->toArray();
            }

        } catch (\Exception $e) {
            return null;
        }

    }

    public function getCoordinatesInfo(float $lat, float $long): ?array
    {
        $query = ReverseQuery::create(new Coordinates($lat, $long));
        $result = $this->geoCoder->reverseQuery($query);
        if ($result->count() == 0) {
            return null;
        }

        return $result->first()->toArray();
    }

    private function _formatNl(array $data): array
    {
        return [
            'providedBy' => 'PostcodeNl',
            'latitude' => $data['latitude'],
            'longitude' => $data['longitude'],
            'streetName' => $data['street'],
            'streetNumber' => $data['houseNumber'],
            'postalCode' => $data['postcode'],
            'locality' => $data['city'],
            'subLocality' => null,
            'bounds' => null,
            'adminLevels' => null,
            'country' => 'Nederland',
            'countryCode' => 'NL'
        ];
    }
}
