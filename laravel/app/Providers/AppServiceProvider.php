<?php

namespace App\Providers;

use Http\Client\Curl\Client;
use Illuminate\Support\ServiceProvider;
use PostcodeNl_Api_RestClient;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\StatefulGeocoder;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('geoProvider', function ($app) {
            $httpClient = new Client();
            return new GoogleMaps($httpClient, null, config('services.googleMaps.apiKey'));
        });
        $this->app->singleton('geoCoder', function ($app) {
            return new StatefulGeocoder(app('geoProvider'), 'nl');
        });
        $this->app->singleton('geoCoderNl', function ($app) {
            return new PostcodeNl_Api_RestClient(
                config('services.postCodeNl.appKey'),
                config('services.postCodeNl.appSecret')
            );
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
