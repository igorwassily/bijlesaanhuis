<?php

namespace App\Http\Controllers;

use App\Contracts\CalendarServiceInterface;
use App\Contracts\PriceServiceInterface;
use App\Models\CalendarBooking;
use App\Models\Student;
use App\Models\Teacher;
use App\Models\User;
use App\Models\Variable;
use DateInterval;
use DateTime;
use DateTimeZone;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TeacherController extends Controller
{
    protected ?User $user;
    protected CalendarServiceInterface $calendarService;

    public function __construct(CalendarServiceInterface $calendarService)
    {
        $this->user = Auth::guard('api')->user();
        $this->calendarService = $calendarService;
    }

    public function appointments(Request $request, $id, $status = -1)
    {
        if ($status != 10) {
            if (empty($this->user)) {
                abort(401);
            } elseif ($this->user->isStudent() || $this->user->isTeacher() && $this->user->teacherID != $id) {
                abort(403);
            }
        }

        $validatedData = $request->validate([
            'from' => 'string',
            'to' => 'string',
            'grouped' => 'bool'
        ]);
        if (empty($validatedData['from'])) {
            $from = null;
        } else {
            $from = DateTime::createFromFormat('Y-m-d\TH:i:sO', $validatedData['from']);
            if ($from === false) {
                abort(422);
            }
        }
        if (empty($validatedData['to'])) {
            $to = null;
        } else {
            $to = DateTime::createFromFormat('Y-m-d\TH:i:sO', $validatedData['to']);
            if ($to === false) {
                abort(422);
            }
        }
        $grouped = $validatedData['grouped'];

        /** @var Teacher $teacher */
        $teacher = Teacher::query()->where('userID', '=', $id)->firstOrFail();

        return $this->calendarService->retrieveAppointments($teacher, $from, $to, $status, $grouped);
    }

    public function pending(Request $request, $id)
    {
        return $this->appointments($request, $id, 0);
    }

    public function accepted(Request $request, $id)
    {
        return $this->appointments($request, $id, 1);
    }

    public function taken(Request $request, $id)
    {
        return $this->appointments($request, $id, 2);
    }

    public function canceled(Request $request, $id)
    {
        return $this->appointments($request, $id, 3);
    }

    public function appointment(Request $request, int $id, CalendarBooking $booking)
    {
        if (empty($this->user)) {
            abort(401);
        } elseif ($this->user->userID != $booking->teacherID && $this->user->userID != $booking->studentID || $booking->teacherID != $id) {
            abort(403);
        }
        return $this->calendarService->parseAppointment($booking);
    }

    private function retrieveAppointments(Teacher $teacher, ?DateTime $from, ?DateTime $to, $status = -1, $grouped = false)
    {
        $groupKey = [
            0 => 'pending',
            1 => 'accepted',
            2 => 'taken',
            3 => 'canceled'
        ];

        $data = $teacher->appointments($from, $to, $status)->get();

        $appointments = [];

        foreach ($data as $entry) {
            $d = $this->calendarService->parseAppointment($entry);

            if ($status == 10) {
                $appointments[$entry->datee][] = $d;
            } elseif ($grouped) {
                $appointments[$groupKey[$d['status']]][] = $d;
            } else {
                $appointments[] = $d;
            }
        }

        return $appointments;
    }

    public function available(Request $request, $id)
    {
        $validatedData = $request->validate([
            'from' => 'required',
            'to' => 'required',
        ]);

        $from = DateTime::createFromFormat('Y-m-d\TH:i:sO', $validatedData['from']);
        if ($from === false) abort(422);
        $to = DateTime::createFromFormat('Y-m-d\TH:i:sO', $validatedData['to']);
        if ($to === false) abort(422);

        /** @var Teacher $teacher */
        $teacher = Teacher::query()->where('userID', '=', $id)->firstOrFail();

        return $this->calendarService->calcAvailabilityTeacher($teacher, $from, $to);
    }

    public function book(Request $request, $id)
    {
        if (empty($this->user)) {
            abort(401);
        } elseif (!$this->user->isStudent()) {
            abort(403);
        }

        $step = Variable::where('name', 'lessonDurationStep')->first()->value;
        $minimum = Variable::where('name', 'lessonDurationMin')->first()->value;
        $maximum = Variable::where('name', 'lessonDurationMax')->first()->value;

        $validatedData = $request->validate([
            'begin' => 'required|date_format:Y-m-d H:i|after:+1day|before:+1year',
            'duration' => "required|integer|between:$minimum,$maximum",
            'online' => 'required|bool',
        ]);
        if ($validatedData['duration'] % $step != 0) {
            abort(422);
        }

        $begin = DateTime::createFromFormat('Y-m-d\TH:i', $validatedData['begin']);

        /** @var Student $student */
        $student = Student::query()->where('userID', '=', $this->user->userID)->firstOrFail();
        /** @var Teacher $teacher */
        $teacher = Teacher::query()->where('userID', '=', $id)->firstOrFail();
        if (!$teacher->isConfirmed) {
            abort(403);
        }

        if (!$teacher->allownewstudents) {
            $p = CalendarBooking::where(['teacherID', 'studentID', 'accepted'], '=', [$id, $this->user->userID, 1])->first();
            if (empty($p)) {
                abort(403);
            }
        }
        if ($validatedData['online'] && !$teacher->onlineteaching) {
            abort(403);
        }

        $available = $this->calendarService->calcAvailabilityTeacher($teacher, $begin, $begin);
        $date = $begin->format('Y-m-d');
        if (!isset($available[$date])) {
            abort(404);
        }
        $found = false;
        foreach ($available[$date] as $slot) {
            if ($slot['begin'] == $begin) {
                if ($available['duration'] <= $slot['max'] && $available['duration'] >= $slot['min']) {
                    $found = true;
                    break;
                } else {
                    abort(404);
                }
            }
        }
        if (!$found) {
            abort(404);
        }

//        $pricing = $this->priceService->price($teacher, $student, $validatedData['duration']);
//        CalendarBooking::create([
//            'studentID' => $student->userID,
//            'teacherID' => $teacher->userID,
//            'datee' => $date,
//            'starttime' => $begin->format('H:i'),
//            'endtime' => $begin->add(new DateInterval("PT{$validatedData['duration']}M"))->format('H:i'),
//            'duration' => $validatedData['duration'],
//            'internal_bookingamount' => $pricing['internal'],
//            'external_bookingamount' => $pricing['external'],
//            'travel_cost' => $pricing['travel'],
//            'travel_distance' => $pricing['distance'],
//            'internal_teacher_level' => $pricing['level'],
//            'accepted' => 0,
//            'isClassOnline' => $validatedData['online'],
//            'isClassTaken' => 0,
//            'isSlotCancelled' => 0,
//        ]);
    }

    public function cancel(Request $request, int $id, CalendarBooking $booking)
    {
        if (empty($this->user)) {
            abort(401);
        } elseif ($this->user->userID != $booking->teacherID && $this->user->userID != $booking->studentID || $booking->teacherID != $id) {
            abort(403);
        }

        $appointment = $this->calendarService->parseAppointment($booking);
        $now = new DateTime('now', new DateTimeZone('Europe/Amsterdam'));
        if ($this->user->isStudent() && $appointment['begin'] <= $now || $appointment['begin']->format('Y-m') < $now->format('Y-m')) {
            abort(403);
        }

        $booking->isSlotCancelled = 1;
        $booking->save();

        return $this->calendarService->parseAppointment($booking);
    }
}
