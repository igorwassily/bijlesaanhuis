<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Contact;
use App\Models\Profile;
use App\Models\Student;
use App\Models\Teacher;
use App\Providers\RouteServiceProvider;
use App\User;
use DateTime;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'email' => ['required', 'string', 'email', 'max:170', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'string', 'max:170'],
            'forename' => ['required', 'string', 'max:170'],
            'surname' => ['required', 'string', 'max:170'],
            'postcode' => ['required', 'string', 'max:170'],
            'housenumber' => ['required', 'string', 'max:170'],
        ]);
    }

    /**
     * Get a validator for an incoming student registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorStudent(array $data)
    {
        return Validator::make($data, [
            'parent_email' => ['required', 'string', 'email', 'max:170'],
            'parent_forename' => ['required', 'string', 'max:170'],
            'parent_surname' => ['required', 'string', 'max:170'],
        ]);
    }

    /**
     * Get a validator for an incoming teacher registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validatorTeacher(array $data)
    {
        return Validator::make($data, [
            'birthday' => ['required', 'string', 'date_format:Y-m-d'],
            'education' => ['required', 'string', 'max:170'],
            'distance' => ['required', 'string', 'regex:/^\d{1,2}([\.,]\d{1,2})?$/'],
            'motivation' => ['string'],
            'school-type' => ['required', 'array'],
            'school-course' => ['required', 'array'],
            'school-level' => ['array'],
            'consultation' => ['required', 'date_format:Y-m-d H:i'],
            'skype' => ['string', 'max:170'],
            'online' => ['boolean'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param Request $request
     * @return \App\User
     */
    protected function student(Request $request)
    {
        $this->validator($request->all())->validate();
        $this->validatorStudent($request->all())->validate();

        $user = User::create([
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'usergroupID' => 1,
            'verificationcode' => md5(Str::random(30).now()),
            'active' => 0,
            'creation_datetime' => new DateTime()
        ]);

        $c = Contact::create([
            'contacttypeID' => 2,
            'userID' => $user->userID,
            'prmary' => 1,
            'firstname' => $request->get('forename'),
            'lastname' => $request->get('surname'),
            'dateofbirth' => $request->get('birthday'),
            'telephone' => $request->get('phone'),
            'email' => $request->get('email'),
            'postalcode' => $request->get('postcode'),
            'country' => 'Nederland',
        ]);

        Contact::create([
            'contacttypeID' => 3,
            'userID' => $user->userID,
            'prmary' => 0,
            'firstname' => $request->get('parent_forename'),
            'lastname' => $request->get('parent_surname'),
        ]);

        Student::create([
            'userID' => $user->userID,
            'contactID' => $c->contactID,
        ]);

        return $user;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function teacher(Request $request)
    {
        $this->validator($request->all())->validate();
        $this->validatorTeacher($request->all())->validate();

        $user = User::create([
            'email' => $request->get('email'),
            'password' => Hash::make($request->get('password')),
            'usergroupID' => 2,
            'verificationcode' => md5(Str::random(30).now()),
            'active' => 0,
            'creation_datetime' => new DateTime()
        ]);

        $c = Contact::create([
            'contacttypeID' => 1,
            'userID' => $user->userID,
            'prmary' => 1,
            'firstname' => $request->get('forename'),
            'lastname' => $request->get('surname'),
            'dateofbirth' => $request->get('birthday'),
            'telephone' => $request->get('phone'),
            'email' => $request->get('email'),
            'postalcode' => $request->get('postcode'),
            'country' => 'Nederland',
        ]);

        Teacher::create([
            'userID' => $user->userID,
            'contactID' => $c->contactID,
            'current_education' => $request->get('education'),
            'onlineteaching' => $request->get('online'),
            'travelRange' => $request->get('distance'),
            'isConfirmed' => 0,
        ]);

        return $user;
    }

    protected function registered(Request $request, $user)
    {
        $user->generateToken();
        return response()->json(['data' => $user->toArray()], 201);
    }
}
