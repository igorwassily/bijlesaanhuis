<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function login(Request $request)
    {
        try {
            $this->validateLogin($request);

            $user = null;
            if ($this->attemptLogin($request)) {
                $user = $this->guard()->user();
            } elseif ($this->attemptOldLogin($request)) {
                $user = User::where('email', $request->input('email'))->first();
            }

            if ($user) {
                if (!$user->active || $user->disabled) {
                    return 422;
                }
                $user->generateToken();

                return response()->json([
                    'data' => $user->toArray(),
                ]);
            }

            return $this->sendFailedLoginResponse($request);
        } catch (\Exception $e) {
            return response()->json([
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'error' => $e->getMessage()
            ]);
        }
    }

    public function logout(Request $request)
    {
        $user = Auth::guard('api')->user();

        if ($user) {
            $user->api_token = null;
            $user->save();
        }

        return response()->json(['data' => 'User logged out.'], 200);
    }

    private function attemptOldLogin(Request $request): bool
    {
        return User::where('email', $request->input('email'))
            ->where('password', md5($request->input('password')))
            ->exists();
    }
}
