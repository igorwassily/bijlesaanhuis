<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AppointmentController extends Controller
{
    protected User $user;

    public function __construct()
    {
        $this->user = Auth::guard('api')->user();
    }

    public function list()
    {
        return Appointment::all();
    }

    public function available($id)
    {
        $u = User::findOrFail($id);
        if (!$this->isAuthorized($u)) {
            abort(403);
        }

        return Appointment::all();
    }

    public function get(Appointment $appointment)
    {
        return $appointment;
    }

    public function book(Request $request)
    {
        return Appointment::create($request->all());
    }

    public function update(Request $request, Appointment $appointment)
    {
        $appointment->update($request->all());
        return $appointment;
    }

    public function cancel(Request $request, Appointment $appointment)
    {
        $appointment->delete();
        return 204;
    }

    protected function isAuthorized(User $u, bool $availability = false): bool
    {
        if (empty($this->user)) {
            return $availability && $u->isTeacher();
        }

        return $this->user->userID == $u->userID || $availability && ($u->isTeacher() || $this->user->isTeacher() && $u->isStudent());
    }
}
