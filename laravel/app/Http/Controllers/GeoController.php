<?php

namespace App\Http\Controllers;

use App\Services\Geo;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class GeoController extends Controller
{

    public function address(Request $request)
    {
        $address = $request->input('address', '');
        $postalCode = $request->input('postalCode', '');
        $streetNumber = $request->input('streetNumber', '');

        if (empty($address) && empty($postalCode)) {
            return response('', Response::HTTP_BAD_REQUEST);
        }

        $postalCode = strtoupper(str_replace(' ', '', $postalCode));
        $cacheKey = 'addr_info_' . md5(implode('', [$address, $postalCode, $streetNumber]));

        $addressInformation = cache()->rememberForever($cacheKey, function () use ($address, $postalCode, $streetNumber) {
            $info = resolve(Geo::class)
                ->getAddressInfo($address, $postalCode, $streetNumber);

            if(is_null($info)) {
                $info = ['notFound' => true];
            }

            return $info;
        });

        return response()->json($addressInformation);
    }

    public function coordinate(Request $request, Geo $geoService)
    {
        $lat = $request->input('lat', 0);
        $long = $request->input('long', 0);

        if (empty($lat) && empty($long)) {
            return response('', Response::HTTP_BAD_REQUEST);
        }

        try {
            $info = $geoService
                ->getCoordinatesInfo($lat, $long);

            if(is_null($info)) {
                return response('', Response::HTTP_NOT_FOUND);
            }

            return response()->json($info);
        } catch (\Exception $e) {
            return response($e->getMessage(), Response::HTTP_NOT_FOUND);
        }
    }
}
