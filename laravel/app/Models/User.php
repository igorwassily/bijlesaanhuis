<?php

namespace App\Models;

use DateTimeZone;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;
use IntlDateFormatter;

class User extends Authenticatable
{
    use Notifiable;

    protected $table = 'user';

    protected $primaryKey = 'userID';

    public $timestamps = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function dateFormatter(): IntlDateFormatter
    {
        return new IntlDateFormatter('nl_NL', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
    }

    public function timeZone(): DateTimeZone
    {
        return new DateTimeZone('Europe/Amsterdam');
    }

    public function userGroup()
    {
        return $this->hasOne('UserGroup', 'usergroupID', 'usergroupID');
    }

    public function isAdmin()
    {
        return $this->usergroupID == 3;
    }

    public function isCustomer()
    {
        return $this->usergroupID != 3;
    }

    public function isTeacher()
    {
        return $this->usergroupID == 2;
    }

    public function isStudent()
    {
        return $this->usergroupID == 1;
    }

    public function generateToken()
    {
        $this->api_token = Str::random(60) . time();
        $this->save();
    }


}
