<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherSlot extends Model
{
    protected $table = 'teacherslots';

    protected $primaryKey = 'slotID';

    public $timestamps = false;
}
