<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolLevel extends Model
{
    protected $table = 'schoollevel';

    protected $primaryKey = 'levelID';

    public $timestamps = false;
}
