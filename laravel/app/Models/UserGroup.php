<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserGroup extends Model
{
    protected $table = 'usergroup';

    protected $primaryKey = 'usergroupID';

    public $timestamps = false;

    public function users()
    {
        return $this->hasMany('User', 'usergroupID', 'usergroupID');
    }
}
