<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolCourse extends Model
{
    protected $table = 'schoolcourse';

    protected $primaryKey = 'schoolcourseID';

    public $timestamps = false;
}
