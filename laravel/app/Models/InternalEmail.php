<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class InternalEmail extends Model
{
    protected $table = 'internalmail';

    protected $primaryKey = 'internalmailID';

    public $timestamps = false;
}
