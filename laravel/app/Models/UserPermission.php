<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    protected $table = 'userpermission';

    protected $primaryKey = 'permissionID';

    public $timestamps = false;
}
