<?php


namespace App\Models;

use DateTime;
use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'teacher';

    protected $primaryKey = 'teacherID';

    public $timestamps = false;

    public function level()
    {
        return $this->hasOne('\App\Models\TeacherLevel', 'ID', 'teacherlevel_rate_ID');
    }

    public function contact()
    {
        return $this->hasOne('App\Models\Contact', 'userID', 'userID');
    }

    public function profile()
    {
        return $this->hasOne('App\Models\Profile', 'profileID', 'profileID');
    }

    public function appointments(?DateTime $from = null, ?DateTime $to = null, int $status = -1)
    {
        $appointments = $this->hasMany('App\Models\CalendarBooking', 'teacherID', 'userID');
        if (!is_null($from)) {
            $appointments = $appointments->where('datee', '>=', $from->format('Y-m-d'));
        }
        if (!is_null($to)) {
            $appointments = $appointments->where('datee', '<=', $to->format('Y-m-d'));
        }
        switch ($status) {
            case 0:
                $appointments = $appointments->where('accepted', '=', 0)
                    ->where('isSlotCancelled', '=', 0);
                break;
            case 1:
                $appointments = $appointments->where('accepted', '=', 1)
                    ->where('isSlotCancelled', '=', 0)
                    ->where('isClassTaken', '=', 0);
                break;
            case 2:
                $appointments = $appointments->where('accepted', '=', 1)
                    ->where('isSlotCancelled', '=', 0)
                    ->where('isClassTaken', '=', 1);
                break;
            case 3:
                $appointments = $appointments->where('isSlotCancelled', '=', 1);
                break;
            case 10:
                $appointments = $appointments->where('isSlotCancelled', '=', 0);
                break;
        }
        return $appointments;
    }

    public function slots()
    {
        return $this->hasOne('App\Models\TeacherSlot', 'teacherID', 'userID');
    }

    public function absentDays()
    {
        $days = [];
        $data = $this->hasMany('App\Models\TeacherAbsent', 'teacherID', 'userID')->get();
        foreach ($data as $d) {
            $days[$d->datee] = $d->datee;
        }
        return $days;
    }
}
