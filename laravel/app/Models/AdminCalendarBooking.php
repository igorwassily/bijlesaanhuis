<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AdminCalendarBooking extends Model
{
    protected $table = 'admincalendarbooking';

    protected $primaryKey = 'admincalendarbookingID';

    public $timestamps = false;
}
