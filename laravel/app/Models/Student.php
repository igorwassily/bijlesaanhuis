<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    protected $table = 'student';

    protected $primaryKey = 'studentID';

    public $timestamps = false;

    public function contact()
    {
        return $this->contacts()->where('contacttypeID','=', '2');
    }

    public function parent()
    {
        return $this->contacts()->where('contacttypeID','=', '3');
    }

    protected function contacts()
    {
        return $this->hasMany('Contact', 'userID', 'userID');
    }

    public function appointments()
    {
        return $this->hasMany('CalendarBooking', 'studentID', 'userID');
    }
}
