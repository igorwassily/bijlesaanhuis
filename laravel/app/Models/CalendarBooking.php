<?php

namespace App\Models;

use Faker\Provider\DateTime;
use Illuminate\Database\Eloquent\Model;

class CalendarBooking extends Model
{
    protected $table = 'calendarbooking';

    protected $primaryKey = 'calendarbookingID';

    public $timestamps = false;
}
