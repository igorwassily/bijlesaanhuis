<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Variable extends Model
{
    protected $table = 'variables';

    protected $primaryKey = 'name';

    public $timestamps = false;

    public $fillable = ['name', 'value'];
}
