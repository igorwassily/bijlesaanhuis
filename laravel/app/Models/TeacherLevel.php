<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherLevel extends Model
{
    protected $table = 'teacherlevel_rate';

    protected $primaryKey = 'ID';

    public $timestamps = false;

    public function teachers()
    {
        return $this->hasMany('Teacher', 'teacherlevel_rate_ID', 'ID');
    }
}
