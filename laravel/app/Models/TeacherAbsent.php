<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherAbsent extends Model
{
    protected $table = 'teacher_absent';

    protected $primaryKey = 'absentID';

    public $timestamps = false;

    protected array $fillable = ['datee', 'teacherID'];
}
