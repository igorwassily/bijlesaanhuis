<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CalendarStatus extends Model
{
    protected $table = 'calendarstatus';

    protected $primaryKey = 'calendarstatusID';

    public $timestamps = false;
}
