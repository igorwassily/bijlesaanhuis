<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherCourse extends Model
{
    protected $table = 'teacher_courses';

    protected $primaryKey = 'teacher_courseID';

    public $timestamps = false;
}
