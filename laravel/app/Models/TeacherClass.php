<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class TeacherClass extends Model
{
    protected $table = 'teacherclass';

    protected $primaryKey = 'teacherclassID';

    public $timestamps = false;
}
