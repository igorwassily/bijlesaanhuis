<?php


namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SchoolType extends Model
{
    protected $table = 'schooltype';

    protected $primaryKey = 'typeID';

    public $timestamps = false;
}
