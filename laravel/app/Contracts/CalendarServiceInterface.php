<?php

namespace App\Contracts;

use App\Models\CalendarBooking;
use App\Models\Student;
use App\Models\Teacher;
use DateTime;

interface CalendarServiceInterface
{
    public function calcAvailabilityConsultation(DateTime $from, DateTime $to);
    public function calcAvailabilityTeacher(Teacher $teacher, DateTime $from, DateTime $to);
    public function calcAvailabilityStudent(Student $student, DateTime $from, DateTime $to);
    public function convertToFullCalendar(array $data, $grouped);
    public function parseAppointment(CalendarBooking $booking);
}
