<?php

namespace App\Contracts;

use App\Models\Student;
use App\Models\Teacher;

interface PriceServiceInterface
{
    public function distanceByCoordinates($latitude1, $longitude1, $latitude2, $longitude2): float;

    public function distance(Teacher $teacher, Student $student): float;

    public function travelCost(float $distance, float $freeRange, float $price): float;

    public function price(Teacher $teacher, Student $student, int $duration): array;
}
