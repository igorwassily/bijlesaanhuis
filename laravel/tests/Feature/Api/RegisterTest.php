<?php

namespace Tests\Feature\Api;

use App\User;
use Tests\TestCase;

class RegisterTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRequiresStudent()
    {
        $this->json('POST', self::API_PREFIX . '/register/student')
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'email',
                    'password',
                ]
            ]);
    }

    public function testStudentMin()
    {
        $payload = [
            'email' => faker
        ];
        $this->json('POST', self::API_PREFIX . '/register/student')
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'email',
                    'password',
                ]
            ]);
    }
}
