<?php


namespace Tests\Feature\Api;


use App\Models\User;
use Illuminate\Support\Str;
use Tests\TestCase;

class GeoTest extends TestCase
{
    public function testAddressNoAddressInRequest()
    {
        $user = $this->createUser();

        $params = ['api_token' => $user->api_token];
        $this->json('GET', self::API_PREFIX . '/geo/address', $params)
            ->assertStatus(400);
    }

    public function testAddress()
    {
        $user = $this->createUser();

        $params = ['api_token' => $user->api_token, 'address' => 'New York', 'postalCode' => '123', 'streetNumber' => 12];
        $this->json('GET', self::API_PREFIX . '/geo/address', $params)
            ->assertStatus(200);
    }

    public function testCoordinateNoParamsInRequest()
    {
        $user = $this->createUser();

        $params = ['api_token' => $user->api_token];
        $this->json('GET', self::API_PREFIX . '/geo/coordinate', $params)
            ->assertStatus(400);
    }

    public function testCoordinateInvalidLatLong()
    {
        $user = $this->createUser();

        $params = ['api_token' => $user->api_token, 'lat' => 100, 'long' => 100];
        $this->json('GET', self::API_PREFIX . '/geo/coordinate', $params)
            ->assertStatus(404);
    }

    public function testCoordinateValidLatLong()
    {
        $user = $this->createUser();

        $params = ['api_token' => $user->api_token, 'lat' => 22, 'long' => 33];
        $this->json('GET', self::API_PREFIX . '/geo/coordinate', $params)
            ->assertStatus(200);
    }

    private function createUser()
    {
        return factory(User::class)->create([
            'email' => 'testlogin@user.com',
            'password' => bcrypt('toptal123'),
            'usergroupID' => 1,
            'active' => 1,
            'disabled' => 0,
            'api_token' => Str::random(64)
        ]);
    }
}
