<?php

namespace Tests\Feature\Api;

use App\Models\CalendarBooking;
use App\Models\Teacher;
use App\Models\TeacherAbsent;
use App\Models\TeacherSlot;
use DateInterval;
use DateTime;
use DateTimeZone;
use Tests\TestCase;

class TeacherTest extends TestCase
{
    public function testAvailability()
    {
        factory(Teacher::class)->create([
            'userID' => 999,
            'contactID' => 0,
            'travelRange' => 5,
            'isConfirmed' => 1,
        ]);

        factory(TeacherSlot::class)->create([
            'teacherID' => 999,
            'mon_time' => '01:00-02:00',
            'wed_time' => '09:15-10:30',
            'thur_time' => '01:00-19:00',
            'mon_additional' => '[{"mon1":"19:00:22:00"}]',
            'tue_additional' => '[{"din1":"19:00:20:00"},{"din2":"10:15:11:00"}]',
            'wed_additional' => '[]',
        ]);

        $tz = new DateTimeZone('Europe/Amsterdam');
        $from = new DateTime('now', $tz);
        $from->add(new DateInterval('P2D'));
        $from->modify('next monday');
        $to = clone $from;
        factory(CalendarBooking::class)->create([
            'teacherID' => 999,
            'datee' => $to->format('Y-m-d'),
            'starttime' => '20:00',
            'endtime' => '20:45',
            'duration' => 45,
        ]);
        factory(CalendarBooking::class)->create([
            'teacherID' => 999,
            'datee' => $to->format('Y-m-d'),
            'starttime' => '01:00',
            'endtime' => '01:45',
            'duration' => 45,
            'isSlotCancelled' => 1
        ]);
        $to->add(new DateInterval('P3D'));
        TeacherAbsent::create([
            'datee' => $to->format('Y-m-d'),
            'teacherID' => 999
        ]);
        $to->add(new DateInterval('P3D'));
        $params = [
            'from' => $from->format('Y-m-d\TH:i:sO'),
            'to' => $to->format('Y-m-d\TH:i:sO'),
        ];

        $this->json('GET', self::API_PREFIX . '/teacher/999/available', $params)
            ->assertStatus(200)
            ->assertJson([
                $from->format('Y-m-d') => [
                    [
                        'begin' => '01:00',
                        'min' => 45,
                        'max' => 60
                    ],
                    [
                        'begin' => '01:15',
                        'min' => 45,
                        'max' => 45
                    ],
                    [
                        'begin' => '19:00',
                        'min' => 45,
                        'max' => 45
                    ],
                    [
                        'begin' => '21:00',
                        'min' => 45,
                        'max' => 60
                    ],
                    [
                        'begin' => '21:15',
                        'min' => 45,
                        'max' => 45,
                        'suggested' => true
                    ],
                ],
                ($from->add(new DateInterval('P1D')))->format('Y-m-d') => [
                    [
                        'begin' => '10:15',
                        'min' => 45,
                        'max' => 45
                    ],
                    [
                        'begin' => '19:00',
                        'min' => 45,
                        'max' => 60
                    ],
                    [
                        'begin' => '19:15',
                        'min' => 45,
                        'max' => 45,
                        'suggested' => true
                    ]
                ],
                ($from->add(new DateInterval('P1D')))->format('Y-m-d') => [
                    [
                        'begin' => '09:15',
                        'min' => 45,
                        'max' => 75
                    ],
                    [
                        'begin' => '09:30',
                        'min' => 45,
                        'max' => 60
                    ],
                    [
                        'begin' => '09:45',
                        'min' => 45,
                        'max' => 45,
                        'suggested' => true
                    ]
                ],
            ]);
    }

    public function testBookRequiresSignIn()
    {
        factory(Teacher::class)->create([
            'userID' => 999,
            'contactID' => 0,
            'travelRange' => 5,
            'isConfirmed' => 1,
        ]);

        factory(TeacherSlot::class)->create([
            'teacherID' => 999,
            'mon_time' => '01:00-02:00',
            'wed_time' => '09:15-10:30',
            'thur_time' => '01:00-19:00',
            'mon_additional' => '[{"mon1":"19:00:22:00"}]',
            'tue_additional' => '[{"din1":"19:00:20:00"},{"din2":"10:15:11:00"}]',
            'wed_additional' => '[]',
        ]);

        $tz = new DateTimeZone('Europe/Amsterdam');
        $from = new DateTime('now', $tz);
        $from->add(new DateInterval('P2D'));
        $from->modify('next monday');

        $params = [
            'begin' => $from->format('Y-m-d') . ' 01:15',
            'duration' => 45,
            'online' => false,
        ];
        $this->json('POST', self::API_PREFIX . '/teacher/999/appointment', $params)
            ->assertStatus(401);
    }
}
