<?php

namespace Tests\Feature\Feature;

use App\Models\User;
use Tests\TestCase;

class LoginTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testRequiresUserAndPw()
    {
        $this->json('POST', self::API_PREFIX . '/login')
            ->assertStatus(422)
            ->assertJsonStructure([
                'message',
                'errors' => [
                    'email',
                    'password',
                ]
            ]);
    }

    public function testTeacherLoginSuccessfully()
    {
        $this->login(2, 1, 0, false)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'userID',
                    'email',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);
    }

    public function testTeacherLoginOldSuccessfully()
    {
        $this->login(2, 1, 0, true)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'userID',
                    'email',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);
    }

    public function testStudentLoginSuccessfully()
    {
        $this->login(1, 1, 0, false)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'userID',
                    'email',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);
    }

    public function testStudentLoginOldSuccessfully()
    {
        $this->login(1, 1, 0, true)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'userID',
                    'email',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);
    }

    public function testAdminLoginSuccessfully()
    {
        $this->login(3, 1, 0, false)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'userID',
                    'email',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);
    }

    public function testAdminLoginOldSuccessfully()
    {
        $this->login(3, 1, 0, true)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'userID',
                    'email',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);
    }

    public function testTeacherLogoutSuccessfully()
    {
        $this->login(2, 1, 0, false);
        $this->json('GET', self::API_PREFIX . '/logout')
            ->assertStatus(200);
    }

    public function testStudentLogoutSuccessfully()
    {
        $this->login(1, 1, 0, false);
        $this->json('GET', self::API_PREFIX . '/logout')
            ->assertStatus(200);
    }

    public function testAdminLogoutSuccessfully()
    {
        $this->login(3, 1, 0, false);
        $this->json('GET', self::API_PREFIX . '/logout')
            ->assertStatus(200);
    }

    public function testPreventInactiveUserLogin()
    {
        $this->login(1, 0, 0, false)
            ->assertStatus(401);
    }

    public function testPreventDisabledUserLogin()
    {
        $this->login(1, 1, 1, false)
            ->assertStatus(401);
    }

    private function login($userGroupID, $active = 1, $disabled = 0, $old = false)
    {
        $user = new User();
        $user->forceFill([
            'email' => 'testlogin@user.com',
            'password' => $old ? md5('toptal123') : bcrypt('toptal123'),
            'usergroupID' => $userGroupID,
            'active' => $active,
            'disabled' => $disabled
        ]);
        $user->save();

        $payload = ['email' => 'testlogin@user.com', 'password' => 'toptal123'];

        return $this->json('POST', self::API_PREFIX . '/login', $payload);
    }
}
