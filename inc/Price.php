<?php

use Doctrine\DBAL\Connection;

class Price
{
	/**
	 * @param $rateExt
	 * @param $rateInt
	 * @param $priceKm
	 * @param $duration int     minutes
	 * @param $distance float   km
	 * @param $freeRange int    km
	 * @return array
	 */
	public static function Calculate($rateExt, $rateInt, $priceKm, $duration, $distance, $freeRange)
	{
		return [
			'internal' => round(($duration / 45) * $rateInt, 2),
			'external' => round(($duration / 45) * $rateExt, 2),
			'distance' => $distance,
			'travelCosts' => self::TravelCosts($priceKm, $distance, $freeRange),
		];
	}

	public static function CalcById(Connection $db, $appointment)
	{
		$sql = "SELECT starttime, endtime, teacherID, studentID FROM calendarbooking WHERE calendarbookingID=$appointment";
		$query = $db->executeQuery($sql);
		$data = $query->fetch(PDO::FETCH_ASSOC);

		$begin = DateTime::createFromFormat("H:i", $data['starttime']);
		$end = DateTime::createFromFormat("H:i", $data['endtime']);
		$duration = ($end->getTimestamp() - $begin->getTimestamp()) / 60;

		return self::CalcByDuration($db, $data['teacherID'], $data['studentID'], $duration);
	}

	public static function CalcByDuration(Connection $db, $teacher, $student, $duration)
	{
		$sql = "SELECT r.internal_rate, r.rate, r.cost_per_km, r.level FROM teacher t LEFT JOIN teacherlevel_rate r ON t.teacherlevel_rate_ID = r.id WHERE t.userID=$teacher";
		$query = $db->executeQuery($sql);
		$rate = $query->fetch(PDO::FETCH_ASSOC);

		$locTeacher = self::_getCoordinates($db, $teacher);
		$locStudent = self::_getCoordinates($db, $student);

		$distance = self::Distance($locTeacher['latitude'], $locTeacher['longitude'], $locStudent['latitude'], $locStudent['longitude']);

		$sql = "SELECT value from variables WHERE name='freeTravelRange'";
		$query = $db->executeQuery($sql);
		$freeTravelRange = $query->fetch(PDO::FETCH_NUM)[0];

		$result = self::Calculate($rate['rate'], $rate['internal_rate'], $rate['cost_per_km'], $duration, $distance, $freeTravelRange);
		$result['level'] = $rate['level'];
		$result['rate'] = $rate['rate'];

		return $result;
	}

	/**
	 * Calculates the great-circle distance between two points, with
	 * the Haversine formula.
	 * @param float $latitudeFrom Latitude of start point in [deg decimal]
	 * @param float $longitudeFrom Longitude of start point in [deg decimal]
	 * @param float $latitudeTo Latitude of target point in [deg decimal]
	 * @param float $longitudeTo Longsitude of target point in [deg decimal]
	 * @param float $earthRadius Mean earth radius in [m]
	 * @return float Distance between points in [km] (same as earthRadius)
	 */
	public static function Distance($latitudeFrom, $longitudeFrom, $latitudeTo, $longitudeTo, $earthRadius = 6371.000)
	{
		$latFrom = deg2rad($latitudeFrom);
		$lonFrom = deg2rad($longitudeFrom);
		$latTo = deg2rad($latitudeTo);
		$lonTo = deg2rad($longitudeTo);

		$latDelta = $latTo - $latFrom;
		$lonDelta = $lonTo - $lonFrom;

		$angle = 2 * asin(sqrt(pow(sin($latDelta / 2), 2) +
				cos($latFrom) * cos($latTo) * pow(sin($lonDelta / 2), 2)));

		return round($angle * $earthRadius, 3);
	}

	/**
	 * @param $priceKm
	 * @param $distance  float  km
	 * @param $freeRange int    km
	 * @return float
	 */
	public static function TravelCosts($priceKm, $distance, $freeRange)
	{
		$price = round($priceKm * ($distance - $freeRange), 2);
		if ($price <= 0) $price = 0;
		return $price;
	}

	private static function _getCoordinates(Connection $db, $userID)
	{
		$sql = "SELECT longitude, latitude FROM contact c WHERE c.userID = $userID";
		$query = $db->executeQuery($sql);
		return $query->fetch(PDO::FETCH_ASSOC);
	}
}
