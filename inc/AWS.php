<?php

use Aws\S3\Exception\S3Exception;
use Aws\S3\S3Client;

class AWS
{
	private ?S3Client $s3;
	private string $s3BucketContent;
	private string $s3BucketPrivate;

	public function __construct()
	{
		$region = getenv('aws_region');
		$key = getenv('aws_access_key_id');
		$secret = getenv('aws_secret_access_key');
		$this->s3BucketContent = getenv('aws_bucket_content');
		$this->s3BucketPrivate = getenv('aws_bucket_private');

		if (empty($key)) {
			$this->s3 = null;
		} else {
			$this->s3 = new S3Client([
				'region' => $region,
				'version' => 'latest',
				'credentials' => [
					'key' => $key,
					'secret' => $secret,
				]
			]);
		}
	}

	public function getFile($pFile)
	{
		if (is_null($this->s3)) return null;

		$bucket = $this->s3BucketPrivate;
		$pos = strpos($pFile, '/');
		if ($pos > 0) {
			if (substr($pFile, 0, $pos) == 'content') {
				$bucket = $this->s3BucketContent;
				$pFile = substr($pFile, $pos);
			}
		}
		$pFile = trim($pFile, '/');
		try {
			return $this->s3->getObject([
				'Bucket' => $bucket,
				'Key' => $pFile,
			]);
		} catch (S3Exception $e) {
			// TODO log
		}

		return null;
	}

	public function upload($pName, $pContent)
	{
		if (is_null($this->s3)) return false;

		try {
			$this->s3->putObject([
				'Bucket' => $this->s3BucketPrivate,
				'Key' => "profileImages/$pName",
				'ContentType' => 'image',
				'Body' => $pContent
			]);

			return true;
		} catch (S3Exception $e) {
			var_dump($e);
			// TODO log
			return false;
		}
	}
}