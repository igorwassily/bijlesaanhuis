<?php

use Doctrine\DBAL\Configuration;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\DriverManager;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

class Common
{
	public static function GetDb(): ?Connection
	{
		$dbParams = [
			'dbname' => getenv('RDS_DB_NAME'),
			'user' => getenv('RDS_USERNAME'),
			'password' => getenv('RDS_PASSWORD'),
			'host' => getenv('RDS_HOSTNAME'),
			'port' => getenv('RDS_PORT'),
			'charset' => 'utf8mb4',
			'driver' => 'mysqli'
		];

		try {
			$db = DriverManager::getConnection($dbParams, new Configuration());
			if (!$db->connect()) {
				return null;
			}
		} catch (DBALException $e) {
			return null;
		}

		return $db;
	}

	public static function GetParam(array $pParams, $pName, string $pType = 'int', $pDefault = 0)
	{
		if (!isset($pParams[$pName])) {
			return $pDefault;
		}
		$value = $pParams[$pName];
		$valid = true;
		switch ($pType) {
			case 'int':
				$valid = is_numeric($value);
				if ($valid) $value = intval($value);
				break;
			case 'array':
				$valid = is_array($value);
				break;
			case 'string':
				$valid = !is_array($value) && !empty($value);
				break;
			case 'float':
				$float = floatval($value);
				if ($float == $value) {
					$value = $float;
				} else {
					$valid = false;
				}
				break;
			case 'bool':
				if ($value == 1 || strcasecmp($value, 'true') == 0) {
					$value = true;
				} elseif ($value == 0 || strcasecmp($value, 'false') == 0) {
					$value = false;
				} else {
					$valid = false;
				}
				break;
		}

		return $valid ? $value : $pDefault;
	}

	public static function GetUserInfo(): array
	{
		$user = [
			'teacher' => false,
			'student' => false,
			'admin' => false,
			'signedIn' => false
		];

		if (isset($_SESSION['StudentID'])) {
			$user['id'] = $_SESSION['StudentID'];
			$user['forename'] = $_SESSION['StudentFName'];
			$user['surname'] = $_SESSION['StudentLName'];;
			$user['initials'] = substr(ucwords($_SESSION['StudentFName']), 0, 1) . substr(ucwords($_SESSION['StudentLName']), 0, 1);
			$user['student'] = true;
			$user['signedIn'] = true;
		} elseif (isset($_SESSION['TeacherID'])) {
			$user['id'] = $_SESSION['TeacherID'];
			$user['forename'] = $_SESSION['TeacherFName'];
			$user['surname'] = $_SESSION['TeacherLName'];
			$user['initials'] = substr(ucwords($_SESSION['TeacherFName']), 0, 1) . substr(ucwords($_SESSION['TeacherLName']), 0, 1);
			$user['teacher'] = true;
			$user['signedIn'] = true;
		} elseif (isset($_SESSION['AdminID'])) {
			$user['admin'] = true;
		}

		return $user;
	}

	public static function IsLocal(): bool
	{
		return getenv('ENVIRONMENT') == 'local';
	}

	public static function IsStaging(): bool
	{
		return getenv('ENVIRONMENT') == 'staging';
	}

	public static function IsProduction(): bool
	{
		return getenv('ENVIRONMENT') == 'production';
	}

	public static function getVersion(): string
	{
		return self::IsLocal() ? 'local' : getenv('VERSION');
	}

	public static function getVersionLink(): string
	{
		return self::IsProduction() ? '' : getenv('VERSION_LINK');
	}

	public static function GetHtmlParam(array $pParams, $pName, string $pType = 'int', $pDefault = 0)
	{
		$param = self::GetParam($pParams, $pName, $pType, $pDefault);
		if (is_array($param)) {
			array_walk_recursive($param, function (&$a, $b) {
				$a = htmlspecialchars_decode($a, ENT_QUOTES | ENT_HTML5);
			});
		} else {
			$param = htmlspecialchars_decode($param, ENT_QUOTES | ENT_HTML5);
		}
		return $param;
	}

	public static function Logger(string $channel)
	{
		$logger = new Logger($channel);
		$streamHandler = new StreamHandler('php://stderr', Logger::DEBUG);
		$streamHandler->setFormatter(new JsonFormatter());
		$logger->pushHandler($streamHandler);
		return $logger;
	}
}
