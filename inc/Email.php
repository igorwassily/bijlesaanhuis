<?php

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

class Email
{
	private $_db;
	private $_name;
	private $_body;
	private $_bodyPlain;
	private $_subject;
	private $_data;
	private $_type;
	private $_mailer;
	private $_fromEmail = "info@bijlesaanhuis.nl";
	private $_fromName = "Bijles Aan Huis";
	private $_bccEmail = "";
	private $_bccName = "";

	public function __construct(Connection $pDB, string $pName)
	{
		$this->_name = $pName;
		$this->_db = $pDB;
		$this->_mailer = new PHPMailer(true);
		$this->_mailer->isSMTP();
		$this->_mailer->Host = getenv('SMTP_HOST');
		$this->_mailer->Port = getenv('SMTP_PORT');
		$user = getenv('SMTP_USER');
		if (!empty($user)) {
			$this->_mailer->SMTPAuth = true;
			$this->_mailer->Username = $user;
			$this->_mailer->Password = getenv('SMTP_PASSWORD');
		}
		if (!empty(getenv('SMTP_TLS'))) {
			$this->_mailer->SMTPSecure = PHPMailer::ENCRYPTION_STARTTLS;
		}
	}

	public function Prepare($pID, array $pData = []): bool
	{
		if (!$this->_build()) return false;

		$data = $this->_retrieveAll($pID);
		if (is_null($data)) return false;
		if (!empty($pData)) {
			$data = array_merge($pData, $data);
		}

		$domain = 'bijlesaanhuis.nl';
		$environment = getenv('ENVIRONMENT');
		if (Common::IsProduction()) {
			try {
				$this->_mailer->addBCC('bahmailcatcher@gmail.com', 'BAH mailcatcher');
			} catch (Exception $e) {
			}
		} else {
			$domain = "$environment.$domain";
		}
		$data['domain'] = "https://$domain";

		$this->_data = $data;
		foreach ($data as $key => $value) {
			$this->_body = str_replace("#$key#", $value, $this->_body);
			$this->_subject = str_replace("#$key#", $value, $this->_subject);
		}
		$this->_body = str_replace('#subject#', $this->_subject, $this->_body);

		return true;
	}

	public function SetBCC($pEmail, $pName='') {
		$this->_bccEmail = $pEmail;
		$this->_bccName = $pName;
	}

	public function SetFrom($pEmail, $pName='') {
		$this->_fromEmail = $pEmail;
		$this->_fromName = $pName;
	}

	public function Send($pTo, $pToName=''): bool
	{
		if (!empty($this->_data["{$pTo}_email"])) $to = $this->_data["{$pTo}_email"];
		else $to = $pTo;

		if (!empty($this->_data["{$pTo}_name"])) $name = $this->_data["{$pTo}_name"];
		else $name = $pToName;

		try {
			$this->_mailer->setFrom($this->_fromEmail, $this->_fromName);
			$this->_mailer->addReplyTo($this->_fromEmail, $this->_fromName);
			$this->_mailer->addAddress($to, $name);

			if (!empty($this->_bccEmail)) {
				$this->_mailer->addBCC($this->_bccEmail, $this->_bccName);
			}

			$this->_mailer->isHTML();
			$this->_mailer->Subject = $this->_subject;
			$this->_mailer->Body = $this->_body;
			if (!empty($this->_bodyPlain)) {
				$this->_mailer->AltBody = $this->_bodyPlain;
			}

			return $this->_mailer->send();
		} catch (Exception $e) {
			// TODO log
			return false;
		}
	}

	private function _build()
	{
		$name = 'index';
		try {
			$stmt = $this->_db->prepare("SELECT html, header, text, subject, type FROM email_tpl WHERE name=?");
		} catch (DBALException $e) {
			return false;
		}
		$stmt->bindParam(1, $name);
		if (!$stmt->execute()) {
			return false;
		}
		$row = $stmt->fetch(PDO::FETCH_ASSOC);
		if ($row === false) {
			return false;
		}
		if (is_null($row['text'])) $row['text'] = '';
		$this->_body = $this->_replaceContent($row['html']);
		$this->_bodyPlain = $this->_replaceContent($row['text'], false);

		$name = $this->_name;
		if (!$stmt->execute() || !($row = $stmt->fetch(PDO::FETCH_ASSOC))) return false;
		$this->_type = $row['type'];
		$this->_subject = $row['subject'];
		$this->_body = str_replace('{body}', $row['html'], $this->_body);
		$this->_body = str_replace('{customHeader}', $row['header'], $this->_body);
		$this->_bodyPlain = str_replace('{body}', $row['text'], $this->_bodyPlain);

		$this->_body = preg_replace('/{[a-zA-Z0-9-_]+}/', '', $this->_body);

		return true;
	}

	private function _replaceContent(string $pText, bool $pHtml = true)
	{
		$result = "";

		$name = '';
		try {
			$stmt = $this->_db->prepare("SELECT html, text FROM email_tpl WHERE name=?");
		} catch (DBALException $e) {
			return false;
		}
		$stmt->bindParam(1, $name);

		preg_match_all('/{([a-zA-Z-_0-9]+)}/', $pText, $matches, PREG_OFFSET_CAPTURE);

		$pos = 0;
		foreach ($matches[0] as $i => $match) {
			$result .= substr($pText, $pos, $match[1] - $pos);
			$pos = $match[1] + strlen($match[0]);
			$name = $matches[1][$i][0];
			if (!$stmt->execute() || !($row = $stmt->fetch())) {
				$result .= $match[0];
				continue;
			}
			$result .= $pHtml ? $row['html'] : $row['text'];
		}

		return $result;
	}

	private function _retrieveAll($pID): ?array
	{
		$data = [];
		switch ($this->_type) {
			case 'appointment':
				$e = $this->_retrieve('appointment', $pID);
				if (is_null($e)) return null;
				$data = $e;
				$e = $this->_retrieve('teacher', $data['teacher']);
				if (is_null($e)) return null;
				$data = array_merge($data, $e);
				$e = $this->_retrieve('student', $data['student']);
				if (is_null($e)) return null;
				$data = array_merge($data, $e);
				$e = $this->_retrieve('parent', $data['student']);
				if (is_null($e)) return null;
				$data = array_merge($data, $e);
				break;
			case 'consultation':
				$e = $this->_retrieve('consultation', $pID);
				if (is_null($e)) return null;
				$data = $e;
				$e = $this->_retrieve('teacher', $data['teacher']);
				if (is_null($e)) return null;
				$data = array_merge($data, $e);
				break;
			case 'user':
			case 'teacher':
				$data = $this->_retrieve($this->_type, $pID);
				if (is_null($data)) return null;
				break;
			case 'student':
				$e = $this->_retrieve('student', $pID);
				if (is_null($e)) return null;
				$data = $e;
				$e = $this->_retrieve('parent', $pID);
				if (is_null($e)) return null;
				$data = array_merge($data, $e);
				break;
			case 'message':
				$e = $this->_retrieve('message', $pID);
				if (is_null($e)) return null;
				$data = $e;
				$e = $this->_retrieve($data['receiverType'] == 1 ? 'teacher' : 'student', $data['receiver']);
				if (is_null($e)) return null;
				$data = array_merge($data, $e);
				$e = $this->_retrieve($data['receiverType'] == 1 ? 'student' : 'teacher', $data['sender']);
				if (is_null($e)) return null;
				$data = array_merge($data, $e);
				$e = $this->_retrieve('parent', $data[$data['receiverType'] == 1 ? 'sender' : 'receiver']);
				if (is_null($e)) return null;
				$data = array_merge($data, $e);
				$data['receiver_email'] = $data[$data['receiverType'] == 1 ? 'teacher_email' : 'student_email'];
				break;
		}
		return $data;
	}

	private function _retrieve($pType, $pID): ?array
	{
		switch ($pType) {
			case 'teacher':
				$query = $this->_db->executeQuery("SELECT c.*, p.image, r.level FROM contact c JOIN teacher t ON t.userID=c.userID LEFT JOIN profile p on t.profileID=p.profileID LEFT JOIN teacherlevel_rate r ON r.ID=t.teacherlevel_rate_ID WHERE c.userID=$pID AND c.prmary=1");
				break;

			case 'student':
			case 'user':
				$query = $this->_db->executeQuery("SELECT * FROM contact WHERE userID=$pID AND prmary=1");
				break;

			case 'parent':
				$query = $this->_db->executeQuery("SELECT * FROM contact WHERE userID=$pID AND prmary=0");
				break;

			case 'appointment':
				$query = $this->_db->executeQuery("SELECT * FROM calendarbooking WHERE calendarbookingID=$pID");
				break;

			case 'consultation':
				$query = $this->_db->executeQuery("SELECT * FROM admincalendarbooking WHERE admincalendarbookingID=$pID");
				break;

			case 'message':
				$query = $this->_db->executeQuery("SELECT c.contacttypeID, e.message, e.senderID, e.receiverID FROM email e LEFT JOIN contact c ON c.userID=e.receiverID WHERE e.emailID=$pID");
				break;

			default:
				return null;
		}

		if (empty($query)) return null;
		$row = $query->fetch(PDO::FETCH_ASSOC);
		if ($row === false) return null;

		switch ($pType) {
			case 'user':
			case 'teacher':
			case 'student':
			case 'parent':
				$data = [
					"{$pType}_id" => $row['userID'],
					"{$pType}_forename" => $row['firstname'],
					"{$pType}_surname" => $row['lastname'],
					"{$pType}_name" => "{$row['firstname']} {$row['lastname']}",
					"{$pType}_email" => "{$row['email']}",
					"{$pType}_phone" => $row['telephone'],
					"{$pType}_address" => $row['address'],
					"{$pType}_postal_code" => $row['postalcode'],
					"{$pType}_city" => $row['city'],
					"{$pType}_country" => $row['country'],
					"{$pType}_place" => $row['place_name'],
				];
				if ($pType == 'teacher') {
					$data["{$pType}_skype"] = $row['skypeID'];
					$img = $row['image'];
					if (empty($img)) $img = 'profile_av.jpg';
					else $img = basename($img);
					$data["{$pType}_img"] = "s3/profileImages/$img";
					$data["{$pType}_level"] = $row['level'];
				}
				break;

			case 'appointment':
			case 'consultation':
				$data = [
					'date' => $row['datee'],
					'begin' => $row['starttime'],
					'end' => $row['endtime'],
					'teacher' => $row['teacherID'],
				];
				if ($pType == 'appointment') {
					$data['student'] = $row['studentID'];
				}
				break;

			case 'message':
				$data = [
					'message' => $row['message'],
					'sender' => $row['senderID'],
					'receiver' => $row['receiverID'],
					'receiverType' => $row['contacttypeID']
				];
				break;

			default:
				return null;
		}

		return $data;
	}

}
