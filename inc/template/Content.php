<?php

namespace Template;

class Content
{
	private const DIR_BASE = '/var/www/';

	private array $css = [];
	private array $cssExt = [];
	private array $cssOrder = [];

	private array $js = [];
	private array $jsHeader = [];
	private array $jsExt = [];
	private array $jsExtHeader = [];
	private array $jsOrder = [];

	private array $data;

	function __construct($data = [])
	{
		$this->data = $data;
	}

	/**
	 * @return array
	 */
	public function getCss(): array
	{
		$data = [];
		foreach ($this->cssOrder as $item) {
			if (isset($this->css[$item])) {
				$data[] = [
					'file' => $this->css[$item],
					'head' => false,
					'ext' => false
				];
			} elseif (isset($this->cssExt[$item])) {
				$data[] = [
					'file' => $this->cssExt[$item],
					'head' => false,
					'ext' => true
				];
			}
		}
		return $data;
	}

	/**
	 * @param string $css
	 */
	public function addCss(string $css): void
	{
		if (!isset($this->css[$css])) {
			$this->cssOrder[] = $css;
			$this->css[$css] = self::DIR_BASE . "assets/css/{$css}.css";
		}
	}

	/**
	 * @param string $css
	 */
	public function addCssExt(string $css): void
	{
		if (!isset($this->cssExt[$css])) {
			$this->cssOrder[] = $css;
			$this->cssExt[$css] = $css;
		}
	}

	/**
	 * @param string $css
	 */
	public function addCssDep(string $css): void
	{
		if (!isset($this->css[$css])) {
			$this->cssOrder[] = $css;
			$this->css[$css] = self::DIR_BASE . "vendor/{$css}.css";
		}
	}

	/**
	 * @return array
	 */
	public function getJs(): array
	{
		$data = [];
		foreach ($this->jsOrder as $item) {
			if (isset($this->js[$item])) {
				$data[] = [
					'file' => $this->js[$item],
					'head' => false,
					'ext' => false
				];
			} elseif (isset($this->jsExt[$item])) {
				$data[] = [
					'file' => $this->jsExt[$item],
					'head' => false,
					'ext' => true
				];
			} else if (isset($this->jsHeader[$item])) {
				$data[] = [
					'file' => $this->jsHeader[$item],
					'head' => true,
					'ext' => false
				];
			} elseif (isset($this->jsExtHeader[$item])) {
				$data[] = [
					'file' => $this->jsExtHeader[$item],
					'head' => true,
					'ext' => true
				];
			}
		}
		return $data;
	}

	/**
	 * @param string $js
	 * @param bool $header
	 */
	public function addJs(string $js, bool $header = false): void
	{
		if ($header) {
			if (!isset($this->jsHeader[$js])) {
				$this->jsOrder[] = $js;
				$this->jsHeader[$js] = self::DIR_BASE . "assets/js/{$js}.js";
			}
		} else if (!isset($this->js[$js])) {
			$this->jsOrder[] = $js;
			$this->js[$js] = self::DIR_BASE . "assets/js/{$js}.js";
		}
	}

	/**
	 * @param string $js
	 * @param bool $header
	 */
	public function addJsExt(string $js, bool $header = false): void
	{
		if ($header) {
			if (!isset($this->jsHeader[$js])) {
				$this->jsOrder[] = $js;
				$this->jsExtHeader[$js] = $js;
			}
		} else if (!isset($this->js[$js])) {
			$this->jsOrder[] = $js;
			$this->jsExt[$js] = $js;
		}
	}

	/**
	 * @param string $js
	 * @param bool $header
	 */
	public function addJsDep(string $js, bool $header = false): void
	{
		if ($header) {
			if (!isset($this->jsHeader[$js])) {
				$this->jsOrder[] = $js;
				$this->jsHeader[$js] = self::DIR_BASE . "vendor/{$js}.js";
			}
		} else if (!isset($this->js[$js])) {
			$this->jsOrder[] = $js;
			$this->js[$js] = self::DIR_BASE . "vendor/{$js}.js";
		}
	}

	/**
	 * @return array
	 */
	public function getJsHeader(): array
	{
		return $this->jsHeader;
	}

	/**
	 * @return array
	 */
	public function getData(): array
	{
		return $this->data;
	}

	/**
	 * @param array $data
	 */
	public function setData(array $data): void
	{
		$this->data = $data;
	}

	/**
	 * @param string $name
	 * @param bool $header
	 */
	public function addPlugin(string $name, bool $header = false): void
	{
		$files = glob(self::DIR_BASE . "assets/plugins/{$name}/*", GLOB_NOSORT);
		foreach ($files as $file) {
			if (preg_match("@/([a-zA-Z-_.0-9]+)\.(css|js)$@", $file, $matches) === 1) {
				if ($matches[2] == 'css') {
					if (!isset($this->css[$matches[1]])) {
						$this->cssOrder[] = $matches[1];
						$this->css[$matches[1]] = self::DIR_BASE . "assets/plugins/{$name}/{$matches[1]}.css";
					}
				} elseif ($matches[2] == 'js') {
					if ($header) {
						if (!isset($this->jsHeader[$matches[1]])) {
							$this->jsOrder[] = $matches[1];
							$this->jsHeader[$matches[1]] = self::DIR_BASE . "assets/plugins/{$name}/{$matches[1]}.js";
						}
					} else if (!isset($this->js[$matches[1]])) {
						$this->jsOrder[] = $matches[1];
						$this->js[$matches[1]] = self::DIR_BASE . "assets/plugins/{$name}/{$matches[1]}.js";
					}
				}
			}
		}
	}
}
