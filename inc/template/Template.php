<?php

namespace Template;

use Common;
use Doctrine\DBAL\Connection;
use Exception;
use Monolog\Logger;
use Smarty;
use Template\Component\CardNumberSection;
use Template\Component\CardPriceSection;
use Template\Component\CardResponsiveSliderSection;
use Template\Component\CardBasicSection;
use Template\Component\CardSliderSection;
use Template\Component\CardTopicSection;
use Template\Component\CardWideSection;
use Template\Component\ChatMessageSection;
use Template\Component\ContactHeaderSection;
use Template\Component\ContactFormSection;
use Template\Component\ContactFormSmallSection;
use Template\Component\CountingNumberSection;
use Template\Component\DashboardSection;
use Template\Component\Error404Section;
use Template\Component\FollowupButtonSection;
use Template\Component\InfoPageSection;
use Template\Component\MessageSection;
use Template\Component\NavigationSection;
use Template\Component\PersonalInformationFormSection;
use Template\Component\QuestionCollectionSection;
use Template\Component\SearchbarHeaderSection;
use Template\Component\SiteMapSection;
use Template\Component\StudentListSection;
use Template\Component\TextInfoSection;
use Template\Component\TutorProfileSection;
use Template\Component\TutoringInfoSection;

require_once '../api/endpoint/Template.php';
require_once '../inc/Email.php';
require_once '../inc/template/Component.php';
require_once '../inc/template/ComponentAction.php';
require_once '../inc/template/Content.php';
require_once '../inc/template/Cache.php';
require_once '../inc/template/ActionResult.php';
require_once '../inc/template/sections/CardParentSection.php';
require_once '../inc/template/sections/CardBasicSection.php';
require_once '../inc/template/sections/CardNumberSection.php';
require_once '../inc/template/sections/CardPriceSection.php';
require_once '../inc/template/sections/CardResponsiveSliderSection.php';
require_once '../inc/template/sections/CardSliderSection.php';
require_once '../inc/template/sections/CardTopicSection.php';
require_once '../inc/template/sections/CardWideSection.php';
require_once '../inc/template/sections/ChatMessageSection.php';
require_once '../inc/template/sections/ContactHeaderSection.php';
require_once '../inc/template/sections/ContactFormSection.php';
require_once '../inc/template/sections/ContactFormSmallSection.php';
require_once '../inc/template/sections/CountingNumberSection.php';
require_once '../inc/template/sections/DashboardSection.php';
require_once '../inc/template/sections/Error404Section.php';
require_once '../inc/template/sections/FollowupButtonSection.php';
require_once '../inc/template/sections/InfoPageSection.php';
require_once '../inc/template/sections/MessageSection.php';
require_once '../inc/template/sections/NavigationSection.php';
require_once '../inc/template/sections/PersonalInformationFormSection.php';
require_once '../inc/template/sections/QuestionCollectionSection.php';
require_once '../inc/template/sections/SearchbarHeaderSection.php';
require_once '../inc/template/sections/SiteMapSection.php';
require_once '../inc/template/sections/StudentListSection.php';
require_once '../inc/template/sections/TutorProfileSection.php';
require_once '../inc/template/sections/TextInfoSection.php';
require_once '../inc/template/sections/TutoringInfoSection.php';

class Template
{
	private Smarty $_smarty;
	private ?Connection $_db;
	private array $_url;
	private string $_tpl;
	private bool $safari;
	private bool $chrome;
	private Logger $logger;
	private string $identifier;

	public function __construct()
	{
		$this->identifier = microtime(false) . rand(10000, 1000000);
		$this->logger = Common::Logger('template');
		$url = strtolower($_SERVER['REQUEST_URI']);
		$pos = strpos($url, "?");
		if ($pos > 0) {
			$url = substr($url, 0, $pos);
		}

		$url = trim($url, '/');
		if (empty($url)) {
			$this->_url = ['homepage'];
		} else {
			$this->_url = explode("/", $url);
		}
		$this->logger->debug('called constructor', ['url' => $this->_url, 'identifier' => $this->identifier]);

		$this->_smarty = new Smarty();
		$this->_smarty->setTemplateDir('../assets/tpl');
		$this->_smarty->loadFilter('output', 'trimwhitespace');
		$this->_smarty->assign('environment', getenv('ENVIRONMENT'));
		$this->_smarty->assign('version', Common::getVersion());
		$vl = Common::getVersionLink();
		if (!empty($vl)) {
			$this->_smarty->assign('versionLink', $vl);
		}

		if (isset($_GET['msg'])) {
			$msg = [
				'text' => $_GET['msg'],
				'type' => isset($_GET['type']) ? $_GET['type'] : 'info'
			];
			$this->_smarty->assign('msg', $msg);
		}

		$this->_db = Common::GetDb();
	}

	public function __destruct()
	{
		$this->logger->debug('called destructor', ['url' => $this->_url, 'identifier' => $this->identifier]);
		if (!is_null($this->_db)) {
			$this->_db->close();
		}
	}

	public function Render()
	{
		$this->logger->debug('called render', ['url' => $this->_url, 'identifier' => $this->identifier]);
		if (is_null($this->_db)) {
			$this->_error(500);
			return;
		}

		$template = new \API\Endpoint\Template($this->_db);
		$buildResult = $template->Build($this->_url);
		$result = $buildResult->getResult();
		$this->logger->debug('got page', ['response' => $result, 'identifier' => $this->identifier]);

		if ($buildResult->getStatus() != 200) {
			if (empty($result['sections'])) {
				$this->_error($buildResult->getStatus());
				return;
			} else {
				http_response_code($buildResult->getStatus());
			}
		}

		$this->_smarty->assign('page', $result['page']);

		$user = Common::GetUserInfo();
		$signedIn = $user['teacher'] || $user['student'];
		$this->_smarty->assign('user', $user);
		$this->_smarty->assign('signedIn', $signedIn);

		$this->_checkUserAgent();

		$components = $this->_buildComponents($result['sections']);
		$this->logger->debug('built components', ['url' => $this->_url, 'identifier' => $this->identifier]);

		$execAction = !empty($_POST);
		$componentsData = [];
		/** @var Component $component */
		$this->logger->debug("call " . __LINE__, ['url' => $this->_url, 'identifier' => $this->identifier]);
		foreach ($components as $component) {
			$data = $component->Build();
			$this->logger->debug("call " . __LINE__, ['tpl' => $component->getTpl(), 'url' => $this->_url, 'identifier' => $this->identifier]);
			$componentsData[] = [
				'tpl' => $component->getTpl(),
				'data' => $data->getData(),
				'c' => $data
			];
			if ($execAction && $component->HasAction()) {
				$this->logger->debug("call " . __LINE__, ['url' => $this->_url, 'identifier' => $this->identifier]);
				/** @var ActionResult $result */
				$result = $component->ExecuteAction($_POST);
				if ($result->isApplied()) {
					if ($result->isReload()) {
						$uri = $_SERVER['REQUEST_URI'];
						if ($result->hasMessage()) {
							$uri .= strpos($uri, '?') === false ? '?' : '&';
							$uri .= 'msg=' . urlencode($result->getMessage());
							$uri .= '&type=' . $result->getLevelStr();
						}
						$this->logger->debug('component forces reload', ['url' => $this->_url, 'identifier' => $this->identifier]);
						http_response_code(303);
						header("Location: $uri");
						die();
					}
					if ($result->hasMessage()) {
						$this->_smarty->assign('msg', $result->toArray());
					}
					break;
				}
			}
		}
		$this->logger->debug("call " . __LINE__, ['url' => $this->_url, 'identifier' => $this->identifier]);

		$this->_smarty->assign('components', $componentsData);
		$this->_buildDependencies(str_replace('/', '_', $result['page']['url']), $componentsData);
		$this->logger->debug('built dependencies', ['url' => $this->_url, 'identifier' => $this->identifier]);

		if (isset($_SESSION['popup_msg'])) {
			$this->_smarty->assign('showPopup', true);
			unset($_SESSION['popup_msg']);
		}

		//		$template = Common::GetParam($page, 'template', 'string', '');
		$template = 'default';
		if (empty($template)) $template = 'default';
		$this->_tpl = $template;
	}

	private function _buildDependencies(string $page, array $componentsData)
	{
		$this->logger->debug("call " . __LINE__, ['url' => $this->_url, 'identifier' => $this->identifier]);
		$cacheEnabled = getenv('CACHE');
		if ($cacheEnabled === false) $cacheEnabled = true;
		else $cacheEnabled = !empty($cacheEnabled);
		$cache = new Cache($cacheEnabled);
		$this->logger->debug("call " . __LINE__, ['url' => $this->_url, 'identifier' => $this->identifier]);

		$content = new Content();
		$content->addJsDep('components/jquery/jquery.min', true);
		$content->addPlugin('popper', true);
		$content->addJsDep('twbs/bootstrap/dist/js/bootstrap.min', true);
		$content->addCssDep('components/font-awesome/css/all.min');
		$content->addCssDep('twbs/bootstrap/dist/css/bootstrap.min');
		$content->addCssDep('snapappointments/bootstrap-select/dist/css/bootstrap-select.min');
		$content->addJsDep('snapappointments/bootstrap-select/dist/js/bootstrap-select.min');
		$content->addPlugin('jquery');
		$content->addCss('message');
		$content->addJs('message');
		$content->addCss('components/footer');
		$content->addJsExt('https://bijlesaanhuis.nl/app/assets/bundles/mainscripts.bundle.js');
		$content->addPlugin('flatpickr');
		$content->addJsExt('https://wchat.freshchat.com/js/widget.js');
		$content->addPlugin('waitme');
		$content->addPlugin('momentjs');
		$content->addPlugin('tippy');
		$content->addPlugin('facebook', true);

		$this->logger->debug("call " . __LINE__, ['url' => $this->_url, 'identifier' => $this->identifier]);
		$cache->addContent($content);
		$this->logger->debug("call " . __LINE__, ['url' => $this->_url, 'identifier' => $this->identifier]);

		/** @var Content $data */
		foreach ($componentsData as $c) {
			$data = $c['c'];
			$cache->addContent($data);
		}

		$this->logger->debug("call " . __LINE__, ['url' => $this->_url, 'identifier' => $this->identifier]);
		$cache->build($page, $this->identifier);
		$this->logger->debug("call " . __LINE__, ['url' => $this->_url, 'identifier' => $this->identifier]);

		$this->_smarty->assign("css", $cache->getCss());
		$this->_smarty->assign("js", $cache->getJs());
		$this->_smarty->assign("jsHeader", $cache->getJs(true));
	}

	private function _error($code)
	{
		http_response_code($code);
		 switch ($code) {
		 	case 400:
		 		$this->_tpl = 'errors/400';
		 		break;
		 	case 404:
		 		$this->_tpl = 'errors/404';
		 		break;
		 	case 500:
		 		$this->_tpl = 'errors/500';
		 		break;
		 	default:
		 		$this->_tpl = 'errors/500';
		 }
	}

	/**
	 * Initialization of the components
	 * @param $sections : array of db table "section" entries for the page to build
	 * Returns an array of initialized components
	 * @return array
	 */
	private function _buildComponents(array $sections): array
	{
		$components[] = new NavigationSection($this->_db, 'NavigationSection');
		foreach ($sections as $section) {
			$component = json_decode($section['data'], true);
			switch ($section['component']) {
				case 'card':
					$components[] = new CardBasicSection($this->_db, 'CardBasicSection', $component);
					break;

				case 'card-numbers':
					$components[] = new CardNumberSection($this->_db, 'CardNumbersSection', $component);
					break;

				case 'card-price':
					$components[] = new CardPriceSection($this->_db, 'CardPriceSection', $component);
					break;

				case 'card-responsive-slider':
					$components[] = new CardResponsiveSliderSection($this->_db, 'CardResponsiveSliderSection', $component);
					break;

				case 'card-slider':
					$components[] = new CardSliderSection($this->_db, 'CardSliderSection', $component);
					break;

				case 'card-topic':
					$components[] = new CardTopicSection($this->_db, 'CardTopicSection', $component);
					break;

				case 'card-wide':
					$components[] = new CardWideSection($this->_db, 'CardWideSection', $component);
					break;

				case 'chat-message-section':
					$components[] = new ChatMessageSection($this->_db, 'ChatMessageSection', $component);
					break;	

				case 'contact-form':
					$components[] = new ContactFormSection($this->_db, 'ContactFormSection', $component);
					break;

				case 'contact-form-small':
					$components[] = new ContactFormSmallSection($this->_db, 'ContactFormSmallSection', $component);
					break;

				case 'contact-header':
					$components[] = new ContactHeaderSection($this->_db, 'ContactHeaderSection', $component);
					break;

				case 'counting-number':
					$components[] = new CountingNumberSection($this->_db, 'CountingNumberSection', $component);
					break;

				case 'dashboard-section':
					$components[] = new DashboardSection($this->_db, 'DashboardSection', $component);
					break;		

				case 'error-404':
					$components[] = new Error404Section($this->_db, 'Error404Section', $component);
					break;

				case 'followup-button':
					$components[] = new FollowupButtonSection($this->_db, 'FollowupButtonSection', $component);
					break;

				case 'info-page-section':
						$components[] = new InfoPageSection($this->_db, 'InfoPageSection', $component);
						break;
				
				case 'message-section':
						$components[] = new MessageSection($this->_db, 'MessageSection', $component);
						break;

				case 'personal-information-form-section':
					$components[] = new PersonalInformationFormSection($this->_db, 'PersonalInformationFormSection', $component);
					break;

				case 'question-collection':
					$components[] = new QuestionCollectionSection($this->_db, 'QuestionCollectionSection', $component);
					break;

				case 'searchbar-header':
					$components[] = new SearchbarHeaderSection($this->_db, 'SearchbarHeaderSection', $component);
					break;

				case 'site-map':
					$components[] = new SiteMapSection($this->_db, 'SiteMapSection', $component);
					break;
					
				case 'student-list-section':
					$components[] = new StudentListSection($this->_db, 'StudentListSection', $component);
					break;

				case 'text-info-section':
					$components[] = new TextInfoSection($this->_db, 'TextInfoSection', $component, $this->safari);
					break;

				case 'tutor-profile':
					$components[] = new TutorProfileSection($this->_db, 'TutorProfileSection', $component);
					break;

				case 'tutoring-info':
					$components[] = new TutoringInfoSection($this->_db, 'TutoringInfoSection', $component, $this->safari);
					break;
			}
		}
		return $components;
	}

	private function _checkUserAgent()
	{
		// Get browser parameters
		$ua = $_SERVER["HTTP_USER_AGENT"];
		$safariOrChrome = strpos($ua, 'Safari') !== false; // Browser is either Safari or Chrome (since Chrome User-Agent includes the word 'Safari')
		$this->chrome = strpos($ua, 'Chrome') !== false; // Browser is Chrome
		$this->safari = $safariOrChrome && !$this->chrome;
		$this->_smarty->assign('chrome', $this->chrome);
		$this->_smarty->assign('safari', $this->safari);
	}

	public function Display()
	{
		$this->logger->debug('display page', ['url' => $this->_url, 'identifier' => $this->identifier]);
		try {
			$this->_smarty->display("{$this->_tpl}.tpl");
		} catch (Exception $e) {
			$this->logger->debug('failed display page', ['url' => $this->_url, 'exception' => $e, 'identifier' => $this->identifier]);
			if (Common::IsLocal()) {
				var_dump($e);
			}
			if (strpos($this->_tpl, 'errors') === false) {
				$this->_error(500);
				$this->Display();
			}
		}
	}
}
