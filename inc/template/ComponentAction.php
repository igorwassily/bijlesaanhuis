<?php

namespace Template;

abstract class ComponentAction extends Component
{
	public function HasAction(): bool
	{
		return true;
	}

	abstract public function ExecuteAction(array $pParams): ActionResult;
	abstract protected function getData(array $pParams): array;
}