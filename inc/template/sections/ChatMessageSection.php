<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class ChatMessageSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        
        $content = new Content($data);

        $content->addCss('components/chat-message-component');        
        $content->addCss('sections/chat-message-section');

        return $content;
    }
}
