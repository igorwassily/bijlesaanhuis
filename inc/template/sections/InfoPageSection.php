<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class InfoPageSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data['infoPage'] = [
            "title" => "Title",
            "description" => "Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat natus odit excepturi ullam rem, harum amet quisquam deserunt fuga? Dignissimos animi mollitia dicta veniam praesentium distinctio vero obcaecati, dolores error?",
            "inputField" => [
                "label" => "Wachtwoord",
                "iconLabel" => "We need the password!",
                "input" => [
                    "id" => "thisisanid",
                    "name" => "thisisaname",
                    "type" => "password",
                    "value" => "thisisavalue",
                    "class" => "thisisaclass"
                ],
                "messageInfo" => "Info message",
                "messageError" => "Error message",
                "showPassword" => true
            ],
            "footerContent" => [
                "description" => "Footer description Lorem ipsum dolor sit amet consectetur adipisicing elit. Placeat natus odit excepturi ullam rem, harum amet quisquam deserunt fuga? Dignissimos animi mollitia dicta veniam praesentium distinctio vero obcaecati, dolores error?",
                "links" => [
                    [
                        "text" => "This is a link text",
                        "link" => "/test-url"
                    ],
                    [
                        "text" => "This is a second link text",
                        "link" => "/test-url2"
                    ]
                ]
            ],
            "inputSubmitButton" => [
                "input" => [
                    "id" => "thisisanid",
                    "name" => "thisisaname",
                    "value" => "vind docent",
                    "class" => "thisisaclass"
                ],
                "maxWidth" => false
            ],
        ];
        $data['inputField'] = [
            "label" => "Wachtwoord",
            "iconLabel" => "We need the password!",
            "input" => [
                "id" => "thisisanid",
                "name" => "thisisaname",
                "type" => "password",
                "value" => "thisisavalue",
                "class" => "thisisaclass"
            ],
            "messageInfo" => "Info message",
            "messageError" => "Error message",
            "showPassword" => true
        ];
        $data['inputSubmitButton'] = [
            "input" => [
                "id" => "thisisanid",
                "name" => "thisisaname",
                "value" => "vind docent",
                "class" => "thisisaclass"
            ],
            "maxWidth" => true
        ];

        $content = new Content($data);

        $content->addCss('components/wrapper-card-component');
        $content->addCss('components/input-field-component');
        $content->addCss('components/input-submit-button-component');

        $content->addCss('components/info-page-component');
        
        $content->addCss('sections/info-page-section');
        

        return $content;
    }
}
