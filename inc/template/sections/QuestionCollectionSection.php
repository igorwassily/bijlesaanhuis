<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class QuestionCollectionSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data['seperatorTitle'] = Common::GetParam($this->params, 'seperatorTitle', 'string', null);
        $data['title'] = Common::GetParam($this->params, 'collectionTitle', 'string', null);
        $data['contents'] = Common::GetHtmlParam($this->params, 'contents', 'array', null);

        $content = new Content($data);
        $content->addCss('components/question-collection-card');
        $content->addJs('components/question-collection-card');

        $content->addCss('sections/question-collection-section');
        $content->addJs('sections/question-collection-section');

        return $content;
    }
}
