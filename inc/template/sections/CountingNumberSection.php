<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class CountingNumberSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data['title'] = Common::GetParam($this->params, 'title', 'string', null);
        $data['unit'] = Common::GetParam($this->params, 'unit', 'string', null);
        $data['subtitle'] = Common::GetParam($this->params, 'subtitle', 'string', null);

        $content = new Content($data);
        $content->addCss('sections/counting-number-section');
        $content->addJs('sections/counting-number-section');

        return $content;
    }
}
