<?php

namespace Template\Component;

use Common;
use Template\Component\CardParentSection;
use Template\Content;

class CardPriceSection extends CardParentSection
{

	public function Build(): Content
	{
        $content = parent::Build();
		$data = $content->getData();
		
		$data['content'] = Common::GetParam($this->params, 'contents', 'array', [
			array(
				"title" => "Junior",
				"description" => '<ul class="card-price-section__card__description__list"><li><span>Beschikt over excellente kennis</span></li><li><span>Heeft bewezen didactische vaardigheden</span></li><li><span>Staat door leeftijd dicht bij de leerling</span></li></ul>',
				"price" => "14,50",
				"buttonText" => "BOEK DOCENT",
				"buttonLink" => "/app/teachers-profile2"
			),
			array(
				"title" => "Senior",
				"description" => '<ul class="card-price-section__card__description__list"><li><span>Beschikt over excellente kennis</span></li><li><span>Heeft bewezen didactische vaardigheden</span></li><li><span>Staat dicht bij de leerling</span></li><li><span>Volgt een gerelateerde studie of heeft het VWO uitmuntend afgerond.</span></li></ul>',
				"price" => "17.50",
				"buttonText" => "BOEK DOCENT",
				"buttonLink" => "/app/teachers-profile2"
			),
			array(
				"title" => "Supreme",
				"description" => '<ul class="card-price-section__card__description__list"><li><span>Beschikt over excellente kennis</span></li><li><span>Heeft bewezen didactische vaardigheden</span></li><li><span>Staat dicht bij de leerling</span></li><li><span>Heeft een gerelateerde studie afgerond of werkervaring als docent.</span></li></ul>',
				"price" => "24,-",
				"buttonText" => "BOEK DOCENT",
				"buttonLink" => "/app/teachers-profile2"
			)
		]);

        $content->setData($data);
		$content->addCss('sections/card-price-section');
		$content->addJs('sections/card-price-section');

		return $content;
	}
}
