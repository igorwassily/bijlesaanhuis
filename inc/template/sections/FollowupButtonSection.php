<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class FollowupButtonSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data['title'] = Common::GetParam($this->params, 'title', 'string', 'Of laat je adviseren');
        $data['description'] = Common::GetParam($this->params, 'description', 'string', 'Wij staan altijd voor je klaar om vragen te beantwoorden of om je te helpen met het vinden van de meest geschikte bijlesdocent!');
        $data['buttonText'] = Common::GetParam($this->params, 'buttonText', 'string', 'CONTACT OPNEMEN');
        $data['buttonLink'] = Common::GetParam($this->params, 'buttonLink', 'string', '/contact');

        $content = new Content($data);
        $content->addCss('sections/followup-button-section');
        $content->addJs('sections/followup-button-section');

        return $content;
    }
}
