<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class MessageSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data['messagesList'] = [
          [
            "chatURL" => "www.anything.here",
            "contactImage" => "",
            "contactForename" => "John Doe1",
            "contactType" => "Leerling",
            "message" => "some short message some short message some short message some short message some short message some short message",
            "messageDate" => "17:42, 09 Januari 2020",
          ],
          [
            "chatURL" => "www.anything.here",
            "contactImage" => "https://i1.wp.com/rollercoasteryears.com/wp-content/uploads/Thrive-During-Finals-.jpg",
            "contactForename" => "John Doe2",
            "contactType" => "Docent",
            "message" => "some short message some short message some short message some short message some short message some short message",
            "messageDate" => "17:42, 09 Januari 2020",
          ],
          [
            "chatURL" => "www.anything.here",
            "contactImage" => "",
            "contactForename" => "John Doe3",
            "contactType" => "Leerling",
            "message" => "some short message some short message some short message some short message some short message some short message",
            "messageDate" => "17:42, 09 Januari 2020",
          ],
        ];

        $content = new Content($data);

        $content->addCss('components/wrapper-card-component');
        $content->addCss('components/message-component');
        $content->addCss('sections/message-section');

        return $content;
    }
}
