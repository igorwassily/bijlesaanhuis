<?php

namespace Template\Component;

use Common;
use Email;
use Template\ActionResult;
use Template\Component;
use Template\ComponentAction;
use Template\Content;

class ContactFormSmallSection extends ComponentAction
{
	public function Build(): Content
	{
		$data = [
			'form' => $this->getData($_POST)
		];

		$content = new Content($data);
		$content->addCss('sections/contact-formular-small-section');
		$content->addJs('sections/contact-formular-small-section');

		return $content;
	}

	public function ExecuteAction(array $pParams): ActionResult
	{
		if (!isset($pParams['subContactSmall'])) return new ActionResult();

		$data = $this->getData($pParams);

		foreach ($data as $d) {
			if (empty($d)) {
				return new ActionResult(true, false, ActionResult::ERROR, "Invalid parameters");
			}
		}

		$email = new Email($this->db, 'contact_small');
		$email->Prepare(null, $data);
		$email->SetFrom($data['email'], $data['name']);
		if (!$email->Send('info@bijlesaanhuis.nl', 'Bijles Aan Huis')) {
			return new ActionResult(true, false, ActionResult::ERROR, "Failed to send email");
		}

		return new ActionResult(true, true, ActionResult::SUCCESS, "Sent email");
	}

	protected function getData(array $pParams): array
	{
		return [
			'email' => Common::GetParam($pParams, 'email', 'string', null),
			'name' => Common::GetParam($pParams, 'name', 'string', null),
			'phone' => Common::GetParam($pParams, 'phone', 'string', null),
			'message' => Common::GetParam($pParams, 'message', 'string', null)
		];
	}
}
