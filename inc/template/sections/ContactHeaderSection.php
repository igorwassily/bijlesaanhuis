<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class ContactHeaderSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data['title'] = 'Contact';
        $data['email'] = 'info@bijlesaanhuis.nl';
        $data['phone'] = '085-1303558';
        $data['whatsapp'] = '085-130355';
        $data['whatsappLink'] = 'https://wa.me/31851303558';

        $content = new Content($data);
        $content->addCss('sections/contact-header-section');
        $content->addJs('sections/contact-header-section');

        return $content;
    }
}
