<?php

namespace Template\Component;

use Common;
use Template\Component\CardParentSection;
use Template\Content;

class CardNumberSection extends CardParentSection
{

    public function Build(): Content
	{
        $content = parent::Build();
        $data = $content->getData();
        
        $data['title'] = Common::GetParam($this->params, 'title', 'string', 'Hoe het werkt');
        $data['content'] = Common::GetParam($this->params, 'contents', 'array', [
        array(
            "imgAltText" => "Desktop",
            "imgSrc" => "/s3/content/Bijles_Online_New-1-1.png",
            "imgSrcSet" => array(
                array(
                    "src" => "/s3/content/Bijles_Online_New-1-1.png",
                    "maxScreenSize" => "991",
                    "minScreenSize" => null
                ),
                array(
                    "src" => "/s3/content/Bijles_Online_New-1-1.png",
                    "maxScreenSize" => null,
                    "minScreenSize" => "992"
                )
            ),
            "title" => "Selecteer je docent",
            "description" => "Ben je op zoek naar aanvullend onderwijs, op de leerling afgestemd? Selecteer de docent die het best bij jou past aan de hand van het docentenprofiel. Onze helpdesk adviseert graag bij het maken van de keuze!"
        ),
        array(
            "imgAltText" => "Calendar",
            "imgSrc" => "/s3/content/Bijles_Amsterdam_New-1.png",
            "imgSrcSet" => array(
                array(
                    "src" => "/s3/content/Bijles_Amsterdam_New-1.png",
                    "maxScreenSize" => "991",
                    "minScreenSize" => null
                ),
                array(
                    "src" => "/s3/content/Bijles_Amsterdam_New-1.png",
                    "maxScreenSize" => null,
                    "minScreenSize" => "992"
                )
            ),
            "title" => "Maak direct een afspraak",
            "description" => "Je kan via onze site direct met de docent een afspraak maken voor bijles, huiswerkbegeleiding of examentraining. Of boek een kosteloos en vrijblijvend kennismakingsgesprek!"
        ),
        array(
            "imgAltText" => "Uprising bar chart",
            "imgSrc" => "/s3/content/Untitled-1.png",
            "imgSrcSet" => array(
                array(
                    "src" => "/s3/content/Untitled-1.png",
                    "maxScreenSize" => "991",
                    "minScreenSize" => null
                ),
                array(
                    "src" => "/s3/content/Untitled-1.png",
                    "maxScreenSize" => null,
                    "minScreenSize" => "992"
                )
            ),
            "title" => "Boek vooruitgang!",
            "description" => "Als de juiste bijlesdocent is gevonden, kan aan de hand van extra ondersteuning de vooruitgang beginnen. De docent begint graag direct met een gedegen plan van aanpak voor betere schoolresultaten!"
        )]);
        $data['numbers'] = true;

        $content->setData($data);
        $content->addCss('sections/card-numbers-section');
        
        return $content;
    }
}