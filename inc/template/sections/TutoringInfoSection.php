<?php

namespace Template\Component;

use Common;
use Doctrine\DBAL\Connection;
use Template\Component;
use Template\Content;

class TutoringInfoSection extends Component
{

    private $_defaultImageAltText = [
        "default-1" => "Docent en leerling",
        "default-2" => "Docent en leerling",
        "default-3" => "Docent en leerling",
        "default-4" => "Docent en leerling"
    ];
    private $_defaultImageSrc = [
        "default-1" => "/s3/content/shutterstock_1155179839.jpg",
        "default-2" => "/s3/content/shutterstock_261210911-2-min.jpg",
        "default-3" => "/s3/content/image-5.png",
        "default-4" => "/s3/content/download-3.png"
    ];
    private $_defaultVideoSrcSet = [
        "default-1" => [
            array(
                "src" => "/s3/content/BAH_interview_shorter.mp4"
            )
        ],
        "default-2" => [
            array(
                "src" => "/s3/content/BAH_interview_longer.mp4"
            )
        ],
        "default-3" => [
            array(
                "src" => "/s3/content/BAH_for_tutors_edited.mp4"
            )
        ],
        "default-4" => [
            array(
                "src" => "/s3/content/BAH_video-for-tut_3.mp4"
            )
        ]
    ];
    private $_defaultImageOverlaySrc = [
        "default-1" => "/s3/content/Optimized-video-overlay-people.jpg",
        "default-2" => "/s3/content/Optimized-video-overlay-people.jpg",
        "default-3" => "/s3/content/Screen-Shot-2019-03-11-at-4.36.00-PM-1.png",
        "default-4" => "/s3/content/Screen-Shot-2019-03-11-at-4.36.00-PM-1.png"
    ];

	private bool $safari;

	public function __construct(Connection $pDB, string $pTpl, array $pParams = [], bool $safari = false)
	{
		parent::__construct($pDB, $pTpl, $pParams);
		$this->safari = $safari;
	}

	public function Build(): Content
    {
        $data = [];
        
        $data['title'] = Common::GetParam($this->params, 'title', 'string', '');
        $data['subtitle'] = Common::GetParam($this->params, 'subtitle', 'string', null);
        $data['description'] = Common::GetHtmlParam($this->params, 'description', 'string', '');

        $data['hideButton'] = Common::GetParam($this->params, 'hideButton', 'bool', false);
        $data['buttonText'] = Common::GetParam($this->params, 'buttonText', 'string', 'VIND DOCENT');
        $data['buttonLink'] = Common::GetParam($this->params, 'buttonLink', 'string', '/app/teachers-profile2');
        
        $data['showTutors'] = Common::GetParam($this->params, 'showTutors', 'bool', false);

        $data['showVideo'] = Common::GetParam($this->params, 'showVideo', 'bool', false);
        if ($data['showVideo']) {
            $data['videoSrcSet'] = Common::GetParam($this->params, 'videoSrcSet', 'array', null);
            $data['overlayImageSrc'] = (isset($data['videoSrcSet']) && isset($data['videoSrcSet'][0]['src']) && isset($this->_defaultImageOverlaySrc[$data['videoSrcSet'][0]['src']])) ? $this->_defaultImageOverlaySrc[$data['videoSrcSet'][0]['src']] : $this->_defaultImageOverlaySrc['default-1'];
            $data['videoSrcSet'] = (isset($data['videoSrcSet']) && isset($data['videoSrcSet'][0]['src']) && isset($this->_defaultVideoSrcSet[$data['videoSrcSet'][0]['src']])) ? $this->_defaultVideoSrcSet[$data['videoSrcSet'][0]['src']] : $this->_defaultVideoSrcSet['default-1'];
        }

        $data['showImage'] = Common::GetParam($this->params, 'showImage', 'bool', false);
        $data['imageSrc'] = Common::GetParam($this->params, 'imageSrc', 'string', null);
        if ($data['showImage'] && isset($data['imageSrc'])) {
            $data['imageAltText'] = Common::GetParam($this->params, 'imageAltText', 'string', null);
            $data['imageAltText'] = (isset($data['imageSrc']) && isset($this->_defaultImageAltText[$data['imageSrc']])) ? $this->_defaultImageAltText[$data['imageSrc']] : "";
            $data['imageSrc'] = (isset($data['imageSrc']) && isset($this->_defaultImageSrc[$data['imageSrc']])) ? $this->_defaultImageSrc[$data['imageSrc']] : $data['imageSrc'];
            $data['imageSrcSet'] = Common::GetParam($this->params, 'imageSrcSet', 'array', null);
        }

        $data['mediaLeft'] = Common::GetParam($this->params, 'mediaLeft', 'bool', false);
        $data['mediaLeftPositionClass'] = "hidden";
        $data['mediaRightPositionClass'] = "right";
        $data['textLeftPositionClass'] = "tutoring-info__text-left";
        if ($data['mediaLeft']) {
            $data['mediaLeftPositionClass'] = "left";
            $data['mediaRightPositionClass'] = "hidden";
            $data['textLeftPositionClass'] = "";
        }
        if (!$data['showVideo'] && !$data['showImage'] && !$data['showTutors']) {
            $data['mediaLeftPositionClass'] = "hidden";
            $data['mediaRightPositionClass'] = "hidden";
            $data['textLeftPositionClass'] = "tutoring-info__text-full";
        }

        $content = new Content($data);

        $content->addCss('components/tutoring-info-media-tutor-cards');
        $content->addCss('components/tutoring-info-media-video');
        $content->addCss('components/tutoring-info-media-overlay-image');

        $content->addCss('sections/tutoring-info-section');
        $content->addJs('sections/tutoring-info-section');
        if($this->safari) {
            $content->addCss('sections/tutoring-info-section-safari');
        }

		$content->addCss('sections/template');
		$content->addJs('sections/template');

        return $content;
    }
}
