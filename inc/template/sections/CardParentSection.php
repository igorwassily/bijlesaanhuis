<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class CardParentSection extends Component
{
	public function Build(): Content
	{
		$data = [];
		$data['title'] = Common::GetParam($this->params, 'title', 'string', null);
		$data['content'] = Common::GetParam($this->params, 'contents', 'array', null);
        $data['numbers'] = false;

		$content = new Content($data);
		$content->addCss('sections/card-parent-section');
		
		return $content;
	}
}
