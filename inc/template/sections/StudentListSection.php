<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class StudentListSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data['studentList'] = [
          [
            "id" => 0,
            "name" => "John Doe",
            "address" => "some address",
            "phoneStudent" => "01231456789",
            "emailStudent" => "test@test.com",
            "forenameParent" => "Forename of the student's parent",
            "emailParent" => "test@test.com",
          ],
          [
            "id" => 1,
            "name" => "John Doe",
            "address" => "some address",
            "phoneStudent" => "01231456789",
            "emailStudent" => "test@test.com",
            "forenameParent" => "Forename of the student's parent",
            "emailParent" => "test@test.com",
          ],
          [
            "id" => 2,
            "name" => "John Doe",
            "address" => "some address",
            "phoneStudent" => "01231456789",
            "emailStudent" => "test@test.com",
            "forenameParent" => "Forename of the student's parent",
            "emailParent" => "test@test.com",
          ],
        ];

        $content = new Content($data);

        $content->addCss('components/wrapper-card-component');
        $content->addCss('components/card-student-list-component');
        $content->addCss('sections/student-list-section');

        return $content;
    }
}
