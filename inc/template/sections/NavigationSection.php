<?php

namespace Template\Component;

use Template\Component;
use Template\Content;
use Common;
use PDO;

class NavigationSection extends Component
{
	public function Build(): Content
	{
		$data = [
			'count' => '',
			'messages' => 0
		];

		$logger = Common::Logger('NavSec');
		$logger->debug('NavSec start');
		$user = Common::GetUserInfo();
		if ($user['signedIn']) {
			$logger->debug('NavSec signed in');
			$data['messages'] = $this->db->createQueryBuilder()
				->select('senderID')
				->distinct()
				->from('email')
				->where('receiverID=:user')
				->andWhere('is_read=0')
				->setParameter('user', $user['id'])
				->execute()
				->rowCount();
			$logger->debug('NavSec calculated number of messages');

			if ($user['teacher']) {
				$logger->debug('NavSec teacher');
				$data['count'] = $this->db->createQueryBuilder()
					->select('calendarbookingID')
					->from('calendarbooking')
					->where('teacherID=:user')
					->andWhere('isClassTaken=0')
					->andWhere('isSlotCancelled=0')
					->andWhere('accepted=0')
					->andWhere('datee>=CURRENT_DATE()')
					->setParameter('user', $user['id'])
					->execute()
					->rowCount();
				$logger->debug('NavSec calculated number of requests');

				$profile = $this->db->createQueryBuilder()
					->select('profileID')
					->from('teacher')
					->where('userID=:user')
					->setParameter('user', $user['id'])
					->execute()
					->fetch(PDO::FETCH_NUM)[0];
				$logger->debug('NavSec got profile id');

				$img = $qb = $this->db->createQueryBuilder()
					->select('image')
					->from('profile', 'p')
					->where("p.profileID=:profile")
					->setParameter('profile', $profile)
					->execute()
					->fetch(PDO::FETCH_NUM)[0];
				$logger->debug('NavSec got image');
				$img = empty($img) ? 'profile_av.jpg' : basename($img);

				$data['image'] = "/s3/profileImages/$img";
			}

			if ($user['student']) {
				$logger->debug('NavSec student');
				$data['count'] = $this->db->createQueryBuilder()
					->select('calendarbookingID')
					->from('calendarbooking')
					->where('studentID=:user')
					->andWhere('isClassTaken=0')
					->andWhere('isSlotCancelled=0')
					->andWhere('accepted=0')
					->andWhere('datee>=CURRENT_DATE()')
					->setParameter(':user', $user['id'])
					->execute()
					->rowCount();
				$logger->debug('NavSec calculated number of requests');
			}
		}
		$logger->debug('NavSec finished', ['data' => $data]);

		$content = new Content($data);
		$content->addCss('sections/navigation');
		if (!Common::GetUserInfo()['signedIn']) {
			$content->addCss('components/modal');
			$content->addJs('components/modal');
		}

		return $content;
	}
}
