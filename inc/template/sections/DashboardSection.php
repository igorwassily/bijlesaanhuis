<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class DashboardSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data['dashboardAppointmentList'] = [
          [
            "dateDay" => 10,
            "dateMonth" => "Jan",
            "dateYear" => 2020,
            "timeStart" => "19:15",
            "timeEnd" => "20:00",
            "phoneNumber" => "02131233234",
            "email" => "test@test.com",
            "location" => "Paris, France",
          ],
          [
            "dateDay" => 20,
            "dateMonth" => "Feb",
            "dateYear" => 2020,
            "timeStart" => "09:15",
            "timeEnd" => "10:00",
            "phoneNumber" => "02131233234",
            "email" => "test@test.com",
            "location" => "Paris, France",
          ],
          [
            "dateDay" => 25,
            "dateMonth" => "Jun",
            "dateYear" => 2020,
            "timeStart" => "09:15",
            "timeEnd" => "10:00",
            "phoneNumber" => "02131233234",
            "email" => "test@test.com",
            "location" => "Paris, France",
          ],
        ];

        $content = new Content($data);

        $content->addCss('sections/dashboard-section');

        $content->addCss('components/wrapper-card-component');
        $content->addCss('components/dashboard-appointment-component');
        $content->addCss('sections/dashboard-appointment-section');

        return $content;
    }
}
