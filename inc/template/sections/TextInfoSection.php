<?php

namespace Template\Component;

use Common;
use Doctrine\DBAL\Connection;
use Template\Component;
use Template\Content;

class TextInfoSection extends Component
{
	public function Build(): Content
    {
        $data = [];
        
        $data['title'] = Common::GetParam($this->params, 'title', 'string', '');
        $data['subtitle'] = Common::GetParam($this->params, 'subtitle', 'string', null);
        $data['description'] = Common::GetHtmlParam($this->params, 'description', 'string', '');
        $data['sectionBackgroundColor'] = Common::GetHtmlParam($this->params, 'sectionBackgroundColor', 'string', '');

        $content = new Content($data);

        $content->addCss('sections/text-info-section');

        return $content;
    }
}
