<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;
use PDO;

class SiteMapSection extends Component
{
	public function Build(): Content
    {
        $data = [];
        
        $qb = $this->db->createQueryBuilder();

        $qb = $qb->select('*')->from('page')->groupBy('url');
        $data['sites'] = $qb->execute()->fetchAll(PDO::FETCH_ASSOC);

        $content = new Content($data);

        $content->addCss('sections/site-map-section');
        $content->addJs('sections/site-map-section');

        return $content;
    }
}
