<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class Error404Section extends Component
{
    public function Build(): Content
    {
        $data = [];
        $content = new Content($data);
        $content->addCss('sections/error-404-section');

        return $content;
    }
}
