<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;

class PersonalInformationFormSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data["inputFields"] = [
            "voornamTextField" => [
                "label" => "Voornaam",
                "iconLabel" => "abc",
                "iconType" => "info-circle",
                "input" => [
                    "id" => "first-name",
                    "name" => "first-name",
                    "type" => "text",
                    "value" => "a",
                    "placeholder" => "Voornaam",
                    "class" => "personal-information-form__input",
                    "messageRequired" => "Klik op Opslaan om je veranderingen op te slaan"
                ]
            ],
            "achternamTextField" => [
                "label" => "Achternaam",
                "input" => [
                    "id" => "last-name",
                    "name" => "last-name",
                    "type" => "text",
                    "value" => "a",
                    "placeholder" => "Achternaam",
                    "class" => "personal-information-form__input",
                    "messageRequired" => "Klik op Opslaan om je veranderingen op te slaan"
                ]
            ],
            "dobDateField" => [
                "label" => "Geboortedatum",
                "input" => [
                    "id" => "dob",
                    "name" => "dob",
                    "value" => "2020-01-01",
                    "type" => "datepicker",
                    "placeholder" => "Geboortedatum",
                    "class" => "personal-information-form__input",
                    "messageRequired" => "Klik op Opslaan om je veranderingen op te slaan"
                ]
            ],
            "telephoneField" => [
                "label" => "Telefoonnummer",
                "input" => [
                    "id" => "telephone",
                    "name" => "telephone",
                    "type" => "tel",
                    "value" => "01234567890",
                    "placeholder" => "Telefoonnummer",
                    "class" => "personal-information-form__input",
                    "messageRequired" => "Klik op Opslaan om je veranderingen op te slaan"
                ]
            ],
            "emailField" => [
                "label" => "E-mailadres",
                "input" => [
                    "id" => "email",
                    "name" => "email",
                    "type" => "email",
                    "value" => "",
                    "placeholder" => "E-mailadres",
                    "class" => "",
                    "messageRequired" => "Klik op Opslaan om je veranderingen op te slaan"
                ],
                "messageError" => "Vul een correct e-mailadres in"
            ],
            "passwordField" => [
                "label" => "Wachtwoord",
                "input" => [
                    "id" => "password",
                    "name" => "password",
                    "type" => "password",
                    "value" => "12345678",
                    "placeholder" => "Please enter a valid password.",
                    "class" => "",
                    "messageRequired" => "Klik op Opslaan om je veranderingen op te slaan"
                ],
                "messageError" => "Vul minstens 8 tekens in",
                "showPassword" => true
            ],
            "travelNumField" => [
                "label" => "Maximale reisafstand",
                "input" => [
                    "id" => "travel_range",
                    "name" => "travel_range",
                    "type" => "number",
                    "value" => "4",
                    "min" => "3.5",
                    "placeholder" => "4",
                    "class" => "",
                    "messageRequired" => "Klik op Opslaan om je veranderingen op te slaan"
                ],
                "messageError" => "Vul minimaal 3,5 km in."
            ]
        ];

        $data["toggleSwitch"] = [
            "aToggleSwitch" => [
                "label" => "Beschikbaar voor online bijles?",
                "iconLabel" => "Online bijles kan je op je eigen manier invullen, bijvoorbeeld via Skype.",
                "iconType" => "info-circle",
                "input" => [
                    "id" => "online_courses",
                    "name" => "online_courses",
                    "value" => "a",
                    "default" => "1",
                    "class" => ""
                ]
            ],
            "bToggleSwitch" => [
                "label" => "Beschikbaar voor nieuwe leerlingen?",
                "iconLabel" => "Door je beschikbaarheid voor nieuwe leerlingen uit te zetten, vinden nieuwe leerlingen jou niet meer op de Vind Docent pagina. We verzoeken je om met je huidige leerlingen altijd persoonlijk afspraken te maken over je beschikbaarheid.",
                "iconType" => "info-circle",
                "input" => [
                    "id" => "online_courses",
                    "name" => "online_courses",
                    "value" => "b",
                    "default" => "0",
                    "class" => ""
                ]
            ]
        ];

        $data["inputSubmitButton"] = [
            "input" => [
                "id" => "thisisanid",
                "name" => "thisisaname",
                "value" => "vind docent",
                "class" => "thisisaclass"
            ],
            "maxWidth" => true
        ];

        $content = new Content($data);

        $content->addCss('components/wrapper-card-component');
        $content->addCss('components/input-field-component');
        $content->addCss('components/input-submit-button-component');
        $content->addCss('components/toggle-switch-component');
        $content->addJs('components/input-field-component');
        $content->addJs('components/toggle-switch-component');

        $content->addCss('sections/personal-information-form-section');
        $content->addJs('sections/personal-information-form-section');

        return $content;
    }
}
