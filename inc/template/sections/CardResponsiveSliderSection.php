<?php

namespace Template\Component;

use Common;
use Template\Component\CardParentSection;
use Template\Content;

class CardResponsiveSliderSection extends CardParentSection
{
    public function Build(): Content
    {
		$content = parent::Build();
        $data = $content->getData();
        $data['title'] = Common::GetParam($this->params, 'title', 'string', 'Hoe het werkt');
        
		$data['content'] = Common::GetParam($this->params, 'contents', 'array', [
			array(
				"imgAltText" => "Registreren",
				"imgSrc" => "https://bijlesaanhuis.nl/wp-content/uploads/2019/07/notes.png",
				"title" => "Registreren",
				"description" => null
			),
			array(
				"imgAltText" => "Profiel maken",
				"imgSrc" => "https://bijlesaanhuis.nl/wp-content/uploads/2019/07/resume.png",
				"title" => "Profiel maken",
				"description" => null
			),
			array(
				"imgAltText" => "Interview",
				"imgSrc" => "https://bijlesaanhuis.nl/wp-content/uploads/2019/07/interview-2.png",
				"title" => "Interview",
				"description" => null
			),
			array(
				"imgAltText" => "Contract",
				"imgSrc" => "https://bijlesaanhuis.nl/wp-content/uploads/2019/07/diploma.png",
				"title" => "Contract",
				"description" => null
			)
		]);
        $data['numbers'] = true;

		$content->setData($data);
		$content->addCss('sections/card-responsive-slider-section');
        $content->addJs('sections/card-responsive-slider-section');

        $content->addCssExt('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
        $content->addCssExt('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css');
        $content->addJsExt('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js');

		$content->addCss('sections/template');
		$content->addJs('sections/template');

        return $content;
    }
}
