<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;
use PDO;

class TutorProfileSection extends Component
{
    public function Build(): Content
    {
        $data = [];

        $data['name'] = Common::GetParam($this->params, 'name', 'string', '');
        $data['level'] = Common::GetParam($this->params, 'level', 'string', null);
        $data['shortDesc'] = Common::GetParam($this->params, 'shortDesc', 'string', '');
        $data['longDesc'] = Common::GetParam($this->params, 'longDesc', 'string', '');

        $content = new Content($data);

        $content->addCss('sections/tutor-profile-section');
        $content->addCss('components/tutor-profile-info');
        $content->addCss('components/tutor-profile-msg');

        $content->addJs('components/tutor-profile-msg');
        $content->addJs('components/tutor-profile-info');

        return $content;
    }
}
