<?php

namespace Template\Component;

use Common;
use Template\Component\CardParentSection;
use Template\Content;

class CardTopicSection extends CardParentSection
{
    public function Build(): Content
    {
        
        $content = parent::Build();

        $content->addCss('sections/card-topic-section');
        $content->addJs('sections/card-topic-section');

		$content->addCss('sections/template');
		$content->addJs('sections/template');

        return $content;
    }
}
