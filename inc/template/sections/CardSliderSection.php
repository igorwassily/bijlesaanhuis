<?php

namespace Template\Component;

use Common;
use Template\Component\CardParentSection;
use Template\Content;

class CardSliderSection extends CardParentSection
{
    public function Build(): Content
    {
        $content = parent::Build();
        $data = $content->getData();
        
        $data['title'] = Common::GetParam($this->params, 'title', 'string', 'Wat ouders en leerlingen van ons vinden');
        $data['subtitle'] = Common::GetParam($this->params, 'subtitle', 'string', '4,8/5 beoordeling op Google');
        $data['titleImgAltText'] = Common::GetParam($this->params, 'titleImgAltText', 'string', '4,8 of 5 beoordeling op Google');
        $data['titleImgSrc'] = Common::GetParam($this->params, 'titleImgSrc', 'string', '/s3/content/Google-Stars-4.8.png');
        $data['contents'] = Common::GetParam($this->params, 'contents', 'array', [
            array(
                "title" => "Josefien",
                "subtitle" => "havo 2",
                "description" => "‘Het gaat veel sneller dan zelfstandig leren. Vaak had ik al mijn huiswerk tijdens de bijles al af, waardoor ik meer tijd had voor andere dingen.’"
            ),
            array(
                "title" => "Else Verkleij",
                "subtitle" => "havo 4",
                "description" => "‘Mijn dochter en ik zijn super blij met de docent die haar bijles komt geven. Ik merk dat haar prestaties zienderogen verbeteren en ze heeft ook meer plezier in school.’"
            ),
            array(
                "title" => "Marianne Ketels",
                "subtitle" => "vmbo 3",
                "description" => "‘Mijn zoon had wat moeite om mee te komen op school. Wat ik fijn vind, is dat de bijlesdocent de lessen perfect afstemt op het niveau van mijn zoon.’"
            ),
            array(
                "title" => "Karin Schippers",
                "subtitle" => "vwo 6",
                "description" => "‘Betrouwbaarheid van de docent vind ik heel erg belangrijk Ze is altijd op tijd en ze werkt alles op een heel rustige en prettige manier met mij door. Ik ben vooruitgegaan.’"
            ),
            array(
                "title" => "Keisha",
                "subtitle" => "vmbo 4",
                "description" => "‘Ik had geen idee wat ik allemaal moet leren voor mijn examens. Jelle van Bijles Aan Huis laat me aan de hand van oude examens oefenen en dat geeft me vertrouwen.’"
            ),
            array(
                "title" => "Kailey Bruggeman",
                "subtitle" => "vwo 1",
                "description" => "‘De bijles met Bijles Aan Huis is leuk en ik haal er hogere cijfers door. Ik vind het niet erg om soms wat langer door te moeten leren.’"
            )
        ]);

		$content->setData($data);
        $content->addCss('sections/card-slider-section');
        $content->addJs('sections/card-slider-section');
        $content->addCssExt('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css');
        $content->addCssExt('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick-theme.css');
        $content->addJsExt('https://cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js');
		$content->addCss('sections/template');
		$content->addJs('sections/template');

        return $content;
    }
}
