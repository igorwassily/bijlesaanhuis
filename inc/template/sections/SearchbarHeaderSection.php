<?php

namespace Template\Component;

use Common;
use Template\Component;
use Template\Content;
use PDO;

class SearchbarHeaderSection extends Component
{
    public function Build(): Content
    {
        $data = [];
        $data['title'] = Common::GetParam($this->params, 'title', 'string', null);
        $data['description'] = Common::GetParam($this->params, 'description', 'string', 'Vind direct de bijlesdocent die bij jou past!');

        $qb = $this->db->createQueryBuilder();

        $qb = $qb->select('typeID', 'typeName')->distinct()->from('schooltype')->groupBy('typeName');
        $data['schoolTypes'] = $qb->execute()->fetchAll(PDO::FETCH_ASSOC);

        $qb = $qb->select('*')->from('schoollevel');
        $data['schoolLevel'] = $qb->execute()->fetchAll(PDO::FETCH_ASSOC);

        $qb = $qb->select('yearID','yearName')->from('schoolyear');
        $data['schoolYears'] = $qb->execute()->fetchAll(PDO::FETCH_ASSOC);
        
        $content = new Content($data);
        $content->addCss('sections/searchbar-header-section');
        $content->addJs('sections/searchbar-header-section');
		$content->addCss('sections/template');
		$content->addJs('sections/template');

        return $content;
    }
}
