<?php

namespace Template\Component;

use Common;
use Template\Component\CardParentSection;
use Template\Content;

class CardBasicSection extends CardParentSection
{

	public function Build(): Content
	{
        $content = parent::Build();
        $data = $content->getData();
        
        $data['title'] = Common::GetParam($this->params, 'title', 'string', 'Hoe het werkt');
		$data['content'] = Common::GetParam($this->params, 'contents', 'array', [
			array(
				"imgAltText" => "Tutor on a canvas",
				"imgSrc" => "/s3/content/Bijles_Thuis_New-1.svg",
				"title" => "Niet goed? Geld terug!",
				"description" => "Wij hebben door nauwkeurige selectie een groot vertrouwen in onze docenten. Indien de docent niet voldoet aan onze kwaliteitseisen, dan betalen wij deze les!"
			),
			array(
				"imgAltText" => "Multiple Tutors",
				"imgSrc" => "/s3/content/Bijles_Utrecht_New-1.svg",
				"title" => "Docent op de leerling afgestemd",
				"description" => "Je selecteert zelf de docent die bij <span style='font-weight:bold;'>jouw wensen</span> past. De docent komt vervolgens vaak zonder reiskosten naar jou toe, zodat je een ideale voorbereiding op toetsen hebt."
			),
			array(
				"imgAltText" => "Speeding student",
				"imgSrc" => "/s3/content/Bijles_New.svg",
				"title" => "Wij helpen graag!",
				"description" => "We zijn zowel telefonisch als via de mail, chat of WhatsApp bereikbaar om ervoor te zorgen dat jouw bijles optimaal is. Ons team is getraind om de leerling te koppelen aan de voor jou beste docent."
			)
		]);

        $content->setData($data);
		$content->addCss('sections/card-basic-section');
		$content->addCss('sections/template');
		$content->addJs('sections/template');

		return $content;
	}
}
