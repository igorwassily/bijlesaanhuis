<?php

namespace Template\Component;

use Common;
use Template\Component\CardParentSection;
use Template\Content;

class CardWideSection extends CardParentSection
{

	public function Build(): Content
	{
		$content = parent::Build();

		$content->addCss('sections/card-wide-section');
		$content->addJs('sections/card-wide-section');

		return $content;
	}
}
