<?php

namespace Template;

use Common;
use MatthiasMullie\Minify\CSS;
use MatthiasMullie\Minify\JS;
use MatthiasMullie\Minify\Minify;
use Monolog\Logger;

class Cache
{
	private const DIR_WEB = '/var/www/html/';

	private bool $minify;
	private Minify $mJs;
	private Minify $mJsHead;
	private Minify $mCss;
	private Minify $mCssHead;
	private array $added = [];
	private array $result = [];
	private array $resultHead = [];
	private Logger $logger;

	public function __construct($pMinify = true)
	{
		$this->logger = Common::Logger('cache');
		$this->minify = $pMinify;
		$this->mJs = new JS();
		$this->mJsHead = new JS();
		$this->mCss = new CSS();
		$this->mCssHead = new CSS();

		if (Common::IsLocal()) {
			self::clear();
		}
	}

	public function addContent(Content $content)
	{
		$this->add($this->mCss, $this->mCssHead, 'css', $content->getCss());
		$this->add($this->mJs, $this->mJsHead, 'js', $content->getJs());
	}

	private function add(Minify &$pMin, Minify &$pMinHead, string $type, array $pFiles)
	{
		foreach ($pFiles as &$item) {
			$this->logger->debug("cache " . __LINE__, ['file' => $item['file']]);
			if ($item['ext']) {
				$name = 'ext.' . basename($item['file']);
				$this->downloadFile($item['file'], "$type/$name");
				$item['file'] = self::DIR_WEB . "$type/$name";
			}
			if (!isset($this->added[$type][$item['file']])) {
				$this->added[$type][$item['file']] = true;
				if (!$this->minify) {
					$name = basename($item['file']);
					$this->copyFile($item['file'], "$type/$name");
					$item['file'] = $name;
				}
				if ($item['head']) {
					$pMinHead->add($item['file']);
					$this->resultHead[$type][] = $item['file'];
				} else {
					$pMin->add($item['file']);
					$this->result[$type][] = $item['file'];
				}
			}
		}
		$this->logger->debug("cache " . __LINE__);
	}

	public function build($page, $identifier='')
	{
		$this->logger->debug("cache " . __LINE__, ['page' => $page, 'identifier' => $identifier]);
		if ($this->minify) {
			$filesCss = glob(self::DIR_WEB . "css/{$page}.*.*", GLOB_NOSORT);
			$filesJs = glob(self::DIR_WEB . "js/{$page}.*.*", GLOB_NOSORT);
			$this->logger->debug("cache " . __LINE__, ['page' => $page, 'identifier' => $identifier]);
			$this->result = [];
			$this->resultHead = [];
			if (empty($filesCss)) {
				$local = Common::IsLocal();
				if ($local) {
					error_reporting(E_ALL & ~E_DEPRECATED);
				}
				$this->logger->debug("cache " . __LINE__, ['page' => $page, 'identifier' => $identifier]);

				$time = time();
				$css = $this->mCss->minify();
				$css = str_replace("../webfonts", "/font", $css);
				$css = str_replace("./fonts", "/font", $css);
				$css = str_replace("./ajax-loader.gif", "/img/ajax-loader.gif", $css);
				file_put_contents(self::DIR_WEB . "css/{$page}.$time.css", $css);
				$this->logger->debug("cache " . __LINE__, ['page' => $page, 'identifier' => $identifier]);

				$this->mJs->minify(self::DIR_WEB . "js/{$page}.$time.js");
				$this->mJsHead->minify(self::DIR_WEB . "js/{$page}.top.$time.js");

				$this->result['css'][] = "{$page}.$time.css";
				$this->result['js'][] = "{$page}.$time.js";
				$this->resultHead['js'][] = "{$page}.top.$time.js";
				$this->logger->debug("cache " . __LINE__, ['page' => $page, 'identifier' => $identifier]);

				if ($local) {
					error_reporting(E_ALL);
				}
				$this->logger->debug("cache " . __LINE__, ['page' => $page, 'identifier' => $identifier]);
			} else {
				$this->logger->debug("cache " . __LINE__, ['page' => $page, 'identifier' => $identifier]);
				foreach ($filesCss as $file) {
					$this->result['css'][] = basename($file);
				}
				$this->logger->debug("cache " . __LINE__, ['page' => $page, 'identifier' => $identifier]);
				foreach ($filesJs as $file) {
					if (strpos($file, '.top.') !== false) {
						$this->resultHead['js'][] = basename($file);
					} else {
						$this->result['js'][] = basename($file);
					}
				}
				$this->logger->debug("cache " . __LINE__, ['page' => $page, 'identifier' => $identifier]);
			}
		}
	}

	private function downloadFile($pUrl, $pDestination): bool
	{
		if (file_exists(self::DIR_WEB . $pDestination)) return true;
		$content = file_get_contents($pUrl);
		if ($content === false) return false;
		file_put_contents(self::DIR_WEB . $pDestination, $content);
		return true;
	}

	private function copyFile($pSource, $pDestination): bool
	{
		if (file_exists($pDestination)) return true;
		return copy($pSource, self::DIR_WEB . $pDestination);
	}

	public static function clear(string $pPage = '')
	{
		$page = empty($pPage) ? '*' : "{$pPage}.*";
		$filesCss = glob(self::DIR_WEB . "css/{$page}.*", GLOB_NOSORT);
		$filesJs = glob(self::DIR_WEB . "js/{$page}.*", GLOB_NOSORT);
		foreach ($filesCss as $file) {
			if (substr($file, -1) == '.') continue;
			if (!empty($pPage) || strpos($file, '/ext.') === false) {
				unlink($file);
			}
		}
		foreach ($filesJs as $file) {
			if (substr($file, -1) == '.') continue;
			if (!empty($pPage) || strpos($file, '/ext.') === false) {
				unlink($file);
			}
		}
	}

	public function getCss(bool $head = false): array
	{
		if ($head) {
			return isset($this->resultHead['css']) ? $this->resultHead['css'] : [];
		} else {
			return isset($this->result['css']) ? $this->result['css'] : [];
		}
	}

	public function getJs(bool $head = false): array
	{
		if ($head) {
			return isset($this->resultHead['js']) ? $this->resultHead['js'] : [];
		} else {
			return isset($this->result['js']) ? $this->result['js'] : [];
		}
	}
}