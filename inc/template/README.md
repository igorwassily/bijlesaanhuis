<a href="https://bijlesaanhuis.nl/"><img src="https://bijlesaanhuis.nl/s3/content/Bijles.png" title="Bijles Aan Huis Template" alt="Bijles Aan Huis Template"></a>

# Bijles Aan Huis Template

> Light weight, clean, component-based and reusable substitute of the previous wordpress page

## Table of Contents

- [Template](#template)
- [Sections](#sections)
    * [Searchbar Header](#searchbar-header)
    * [Tutoring Info](#tutoring-info)
    * [Cards Numbers](#cards-numbers)
    * [Cards](#cards)
    * [Slider Cards](#slider-cards)
    * [Followup Button](#followup-button)
- [General](#general)
- [Effects](#effects)
- [Components](#components)

---

## Template
*You can find a <a href="https://staging.bijlesaanhuis.nl/app/template/templateExample">full example copied page here</a>.*
- Template to copy: `/app/template/template-to-copy.php`
    1. Copy the file where you like to. (Don't override the original template-to-copy.php)
    2. Set an appropriate name for the copied file.
    3. Init the parameters as you like
    4. You can set every variable to null
    5. `// string , hiding component when missing` indicates that if the specific paremter is set to **null**, the comonent will be hidden
        - All other parameters are optional!
    6. The sections are ordered chronologically and will always stay in place. You still have all options to manipulate each's content


**Caution**
If you change the directory, the import paths within the template need to be adjusted
```php
// string
$pathToTemplateDirectory = "";
// string
$pathToIncludesDirectory = "../includes/";
// string
$pathToAppDirectory = "../../app/";
// string
$pathToAppAssetsDirectory = "../assets/";
```

Also set the page meta description and titles. You can either use the same Title for page and social media or override it.
```php
// string , hiding component when missing
$templateTitle = "🎓Bijles Aan Huis | Alles voor het beste resultaat!";
 // string
$templateTitleSocialMedia = $templateTitle;
// string , hiding component when missing
$templateDescription = "Bekijk en boek direct onze zorgvuldig geselecteerde docenten | Bijles vanaf 14,50 per lesuur! | Niet goed? Geld terug!";
 // string
$templateDescriptionSocialMedia = $templateDescription;
```

## Sections
You will find these parameters. Here you see what results you get :)
### Searchbar Header
```php
// searchbar-header-section
// string , hiding component when missing
$searchbarHeaderTitle1 = "Bijles Wiskunde";
// string , hiding component when missing
$searchbarHeaderDescription1 = null;
```
<img src="https://bijlesaanhuis.nl/s3/content/template-searchbar-header.png" title="Searchbar Header" alt="Searchbar Header">


### Tutoring Info
```php
// tutoring-info-section
// string , hiding component when missing
$tutoringInfoTitle1 = "Onze wiskunde bijles";
// string , hiding component when missing
$tutoringInfoDescription1 = "Voor de bovenbouw van de havo en het vwo is wiskunde opgedeeld in 4 soorten: wiskunde A, wiskunde B, wiskunde C, wiskunde D. Wiskunde A en wiskunde B zijn de meeste voorkomende soorten, ...";
// optional properties
// boolean
$tutorInfoShowTutors1 = null;
// boolean
$tutorInfoHideButton1 = null;
// boolean
$tutorInfoMediaLeft1 = null;
// optional image
// string
$tutorInfoImageAltText4 = null;
// string
$tutorInfoImageSrc4 = null;
// array of arrays
$tutorInfoImageSrcSet4 = null;
```
**Video**
This goes for `$tutorInfoShowTutors1 = null;`
<img src="https://bijlesaanhuis.nl/s3/content/template-tutoring-info-section-video.png" title="Tutoring Info" alt="Tutoring Info">

**Tutors**
This goes for `$tutorInfoShowTutors1 = true;` and `$tutorInfoHideButton1 = true;` and `$tutorInfoMediaLeft1 = true;`
<img src="https://bijlesaanhuis.nl/s3/content/template-tutoring-info-section-tutors.png" title="Tutoring Info" alt="Tutoring Info">

**Image**
This goes for `$tutorInfoImageSrc4 = "https://bijlesaanhuis.nl/s3/content/tutor_and_pupil.jpg";`
<img src="https://bijlesaanhuis.nl/s3/content/template-tutoring-info-section-image.png" title="Tutoring Info" alt="Tutoring Info">

### Cards Numbers
```php
// cards-number-section
// boolean , hiding component when true
$cardsNumberHide1 = null;
// string
$cardNumberHeader1 = null;
// array
$cardNumberContents1 = null;
```
<img src="https://bijlesaanhuis.nl/s3/content/template-cards-numbers.png" title="Cards Numbers" alt="Cards Numbers">


### Cards
```php
// cards-section
// boolean , hiding component when true
$cardsHide1 = null;
// string
$cardHeader1 = null;
// array
$cardContents1 = null;
```
<img src="https://bijlesaanhuis.nl/s3/content/template-cards.png" title="Cards" alt="Cards">

### Slider Cards
```php

// slider-cards-section
// boolean , hiding component when true
$sliderCardsHide1 = null;
// string
$sliderCardHeader1 = null;
// string
$sliderCardSubHeader1 = null;
// string
$sliderCardHeaderImgAltText1 = null;
// string
$sliderCardHeaderImgSrc1 = null;
// array
$sliderCardContents1 = null;
```
<img src="https://bijlesaanhuis.nl/s3/content/template-slider-cards.png" title="Cards" alt="Cards">

### Followup Button
```php
// followup-button-section
// boolean , hiding component when true
$followupButtonHide1 = null;
// string
$followupButtonTitle1 = null;
// string
$followupButtonDescription1 = null;
// string
$followupButtonButtonText1 = null;
// string
$followupButtonButtonLink1 = null;
```
<img src="https://bijlesaanhuis.nl/s3/content/template-followup-button.png" title="Followup Button" alt="Followup Button">

---

## General
- All components consist of optional parameters. Not inserted parameters will simply not be shown.
- If you want to set a line break, insert `<br>` into the text string parameter
- All components need to be initialized within the **template file** as follows:
    1. `// init XXX section variables`
    2. Asigning parameter values of the component
    3. Importing component ```php require('components/component.php');```
- **Reusable Components:**
    - Cards Section
    - Follow Up Button Section
    - Tutoring Info Section
- **Not reusable Components**
    - Slider Cards Section
    - Searchbar Header Section
    - Template Header `;)`
- **Import paths for require and include**
    - Please change the values to correct directory path in the **copied** template.php
    - `$pathToTemplateDirectory = "";`
    - `$pathToIncludesDirectory = "../includes/";`
    - `$pathToAppAssetsDirectory = "../../assets/";`
    - `$pathToAppDirectory = "../../../app/";`

---

## Effects
- **Fade in of Sections on Scroll:**
    > `/css/template.css`
    > `/js/template.js`
    - Currently faded sections are `Tutoring Info Section`, `Cards Section` and `Slider Cards Section`
- **Lazy loading and fade in of images**
    > `/css/template.css`
    > `/js/template.js`
    - If you develop a new component and want to make an image lazy loading add data-src instead of src
    ```html
    <img data-src="src/goes/here.png" />
    ```
    - If you develop a new component and want to make an background-image lazy loading use a div-element add data-src and background-image:url()
    ```html
    <div data-src="src/goes/here.png" style="background-image:url(src/goes/here.png)"></div>
    ```
    > Videos are recently hidden behind the overlay image. That image should have and data-src attribute as described.

---

## Components

### Template Header
> The component is designed for setting a page title, a page social media title, a page meta description and a page social media desctiption.
#### Customizing
- (Recommended) Page title
- (Recommended) Page meta description text
- (Recommended) Page social media desctiption text

#### Parameters
- `$templateTitle : string` - The Title to be displayed in the browser tab
- `$templateDescription : string` - The description for meta tags in search engines
> The Social Media links only need to be initialized if they have different content than `$templateTitle` or `$templateDescription`
- `$templateTitleSocialMedia : string` - The Title to be displayed in social media links
- `$templateDescriptionSocialMedia : string` - The description to be displayed in social media links

#### Example
- **Example with page title, description and social media page title and desctiption**
```php
        
    //init template-header
    $templateTitle = "Bijles Wiskunde | Bijles Aan Huis";
    $templateTitleSocialMedia = "Bijles Wiskunde";
    $templateDescription = "Vind direct de bijlesdocent die bij jou past!";
    $templateDescriptionSocialMedia = "Vind direct de bijlesdocent die bij jou past!";

    require_once($pathToTemplateDirectory.'components/template-header.php');
```

---

### FollowUp Button Section
> The component is designed for showing a title, a description and an action button with a link.
#### Customizing
- (Optional) Component title
- (Recommended) Component description text
- (Recommended) Component Action Button

#### Parameters
- `$followupButtonHide: boolean` -  Hide the component if true
- `$followupButtonTitle : string` - The components title
- `$followupButtonDescription : string` - The components description / text
- `$followupButtonButtonText : string` - Text of the action button
- `$followupButtonButtonLink : string` - Link where the click on the action button leads to

#### Example
- **Example with title, description and action button**
```php
        // init followup button section variables
        $followupButtonHide = null;
        $followupButtonTitle = "Of laat je adviseren";
        $followupButtonDescription = "Wij staan altijd voor je klaar om vragen te beantwoorden of om je te helpen met het vinden van de meest geschikte bijlesdocent!";
        $followupButtonButtonText = "CONTACT OPNEMEN";
        $followupButtonButtonLink = "/contact";

        require($pathToTemplateDirectory.'components/followup-button-section.php');
```

---

### Cards Section
> The component is designed for three cards.
#### Customizing
- (Optional) Component title
- (Optional) Card image
- (Optional) Card title
- (Optional) Card description
- (Optional) Card numbers

*In the template-to-copy.php this component has been split into $cardsNumber and $cards for usability reasons. It is still the same underlying component*
#### Parameters
- `$cardsHide: boolean` -  Hide the component if true
- `$cardHeader: string` - The components title above the cards

- `$cardContents: array of arrays` - The cards parameters
    > The child arrays consists of following card parameters:
    - `numberImgSrc: string : image src link` - Enables an image above the card
        - **Caution!** Needs to be set on the first image at least, preferred for all 3 cards
    - `numberImgAltText: string`
    - `imgSrc: string : image src link` - Enables card image

    - `imgSrcSet: array of arrays` - the srcsets parameters
        > The child arrays consists of the following parameters:
        - `src: string` - image src link
        > Note: Choose either maxScreensize of minScreenSize, never both in the same child array of imgSrcSet
        - `maxScreenSize: string` - Number of pixels (without px!) as string
        - `minScreenSize: string` - Number of pixels (without px!) as string

    - `imgAltText: string`
    - `title: string` - The cards Title
    - `description: string` - The cards description / text

#### Example
- **Simple Example of cards with only title and description**
```php
        // init cards section variables
        $cardsHide = null;
        $cardHeader = "Test Cards Section";
        $cardContents = [
            array(
                "title" => "Niet goed? Geld terug!",
                "description" => "Wij hebben door nauwkeurige selectie een groot vertrouwen in onze docenten. Indien de docent niet voldoet aan onze kwaliteitseisen, dan betalen wij deze les!"
            ),
            array(
                "title" => "Docent op de leerling afgestemd",
                "description" => "Je selecteert zelf de docent die bij <span style='font-weight:bold;'>jouw wensen</span> past. De docent komt vervolgens vaak zonder reiskosten naar jou toe, zodat je een ideale voorbereiding op toetsen hebt."
            ),
            array(
                "title" => "Wij helpen graag!",
                "description" => "We zijn zowel telefonisch als via de mail, chat of WhatsApp bereikbaar om ervoor te zorgen dat jouw bijles optimaal is. Ons team is getraind om de leerling te koppelen aan de voor jou beste docent."
            )
        ];

        require($pathToTemplateDirectory.'components/cards-section.php');
```
- **Advanced example with card image source sets, card number pictures and all possible parameters set:**
```php
        // init cards section variables with numbers
        $cardsNumberHide = null;
        $cardNumberHeader = "Hoe het werkt";
        $cardNumberContents = [
            array(
                "numberImgAltText" => "1",
                "numberImgSrc" => "https://bijlesaanhuis.nl/s3/content/one-3.png",
                "imgAltText" => "Desktop",
                "imgSrc" => "https://bijlesaanhuis.nl/s3/content/Bijles_Online_New-1-1.png",
                "imgSrcSet" => array(
                    array(
                        "src" => "https://bijlesaanhuis.nl/s3/content/Bijles_Online_New-1-1-150x150.png",
                        "maxScreenSize" => "991",
                        "minScreenSize" => null
                    ),
                    array(
                        "src" => "https://bijlesaanhuis.nl/s3/content/Bijles_Online_New-1-1.png",
                        "maxScreenSize" => null,
                        "minScreenSize" => "992"
                    )
                ),
                "title" => "Selecteer je docent",
                "description" => "Ben je op zoek naar aanvullend onderwijs, op de leerling afgestemd? Selecteer de docent die het best bij jou past aan de hand van het docentenprofiel. Onze helpdesk adviseert graag bij het maken van de keuze!"
            ),
            array(...),
            array(...)
        ];

        require($pathToTemplateDirectory.'components/cards-section.php');
```

---
### Slider Cards Section
> The component is designed for **at least 4 cards**.
#### Customizing
- (Optional) Component title
- (Optional) Component title image
- (Optional) Component subtitle
- (Recommended) Card image
- (Recommended) Card title
- (Recommended) Card subtitle
- (Recommended) Card description

#### Parameters
- `$sliderCardsHide: boolean` -  Hide the component if true
- `$sliderCardHeader: string` - The components title above the cards
- `$sliderCardHeaderImgSrc: string : image src link` - Small title image between component title and subtitle
- `$sliderCardHeaderImgAltText: string`
- `$sliderCardSubHeader: string` - The components subtitle above the cards

- `$sliderCardContents: array of arrays` - The cards parameters
    > The child arrays consists of following card parameters:
    - `imgSrc: string : image src link` - Enables card image

    - `imgSrcSet: array of arrays` - the srcsets parameters
        > The child arrays consists of the following parameters:
        - `src: string` - image src link
        > Note: Choose either maxScreensize of minScreenSize, never both in the same child array of imgSrcSet
        - `maxScreenSize: string` - Number of pixels (without px!) as string
        - `minScreenSize: string` - Number of pixels (without px!) as string

    - `imgAltText: string`
    - `title: string` - The cards author
    - `description: string` - Short author information, e.g. `Havo 5, Wiskunde`

#### Example
- **Example with all necessary parameters set:**
```php
        // init slider-cards section variables
        $sliderCardsHide = null;
        $sliderCardHeader = "Wat ouders en leerlingen van ons vinden";
        $sliderCardSubHeader = "4,8/5 beoordeling op Google";
        $sliderCardHeaderImgAltText = "4,8 of 5 beoordeling op Google";
        $sliderCardHeaderImgSrc = "https://bijlesaanhuis.nl/s3/content/Google-Stars-4.8.png";
        $sliderCardContents = [
            array(
                "imgAltText" => "Desktop",
                "imgSrc" => "https://bijlesaanhuis.nl/app/img/quote_literal.png",
                "title" => "Mike Evers",
                "subtitle" => "Havo 5, wiskunde",
                "description" => "‘Ik stond gemiddeld een 5.8 voor wiskunde voordat ik met Bijles Aan Huis begon en ik ben nu geëindigd met een 7.5 voor mijn centraal examen. Dus ik vind dat Bijles Aan Huis echt helpt, vooral omdat ze de tijd nemen om uit te leggen en heel duidelijk met handige ezelsbruggetjes uitleggen en dus ook resultaat boeken.’"
            ),
            array(...),
            array(...),
            array(...)
        ];

        require($pathToTemplateDirectory.'components/slider-cards-section.php');
```

---

### Tutoring Info Section
> The component is designed for showing a title, a description and **either** a video with image overlay **or** an image.
#### Customizing
- (Optional) Component title
- (Recommended) Component title media (Image, Tutor Cards, or Video with Overlay Image)
- (Recommended) Component description text
- (Optional) Component action button
- (Optional) Opt In Show the media left instead of right to the text

#### Parameters
- `$tutoringInfoTitle : string` - The components title
- `$tutoringInfoDescription : string` - The components description / text
- `$tutorInfoButtonText : string` - Text of the action button
- `$tutorInfoButtonLink : string` - Link where the click on the action button leads to

- **Caution!** Either set `$tutorInfoVideoSrcSet` **and** $`tutorInfoOverlayImageSrc` **or** `$tutorInfoImageSrc` *(and `$tutorInfoImageSrcSet, $tutorInfoImageAltText` (optional)*) **or** `$tutorInfoShowTutors = true`
- `$tutorInfoVideoSrcSet : array of arrays`
    > The child arrays consists of following card parameters:
    - `src : string : video src link` - Link for each source. Consider different filetypes, e.g. .mp4, .webm
- `$tutorInfoOverlayImageSrc : string : image src link` - The Overlay Image for Video should be quite the same format es the video

- `$tutorInfoShowTutors: boolean` - if `true` clickable tutor cards will be shown
- `$tutorInfoImageSrc : string` - image src link
- `$tutorInfoImageSrcSet : array of arrays`
            array(
              "src" => string,
               // only choose one, set the other to null
              "maxScreenSize" => string,
              "minScreenSize" => string
            ), array( ... )
         )
- `$tutorInfoImageAltText : string`

- `$tutorInfoMediaLeft : boolean` - If set true, the video with overlay or the image will be displayed on the left side of the screen
    
#### Example
- **Example with video and overlay image and action button**
```php
        // init tutoring info section variables
        $tutoringInfoTitle = "Onze wiskunde bijles";
        $tutoringInfoDescription = "Voor de bovenbouw van de havo en het vwo is wiskunde opgedeeld in 4 soorten: wiskunde A, wiskunde B, wiskunde C, wiskunde D. Wiskunde A en wiskunde B zijn de meeste voorkomende soorten, TEST waarbij wiskunde A gericht is op kansrekening en statistiek en wiskunde B gericht is op algebra. Het positieve van wiskunde is dat het niveau van een leerling met de juiste begeleiding binnen enkele lessen drastisch omhoog gebracht kan worden. Docenten wiskunde worden niet alleen geselecteerd op basis van cognitieve kwaliteiten, maar ook op basis van empathisch vermogen.";
        $tutorInfoShowTutors = null;
        $tutorInfoButtonText = "VIND DOCENT";
        $tutorInfoButtonLink = "/app/teachers-profile2";
        $tutorInfoVideoSrcSet = array(
            array(
                "src" => "https://bijlesaanhuis.nl/s3/content/BAH-video-for-parents_3.mp4"
            ),
            array(
                "src" => "https://localhost/bijle/s3/content/BAH-video-for-parents_3.mp4"
            )
        );
        $tutorInfoOverlayImageSrc = "https://bijlesaanhuis.nl/s3/content/Screen-Shot-2019-03-11-at-4.36.00-PM-1.png";

        require($pathToTemplateDirectory.'components/tutoring-info-section.php');
```


- **Example with image as media and media put to the left**
```php
        // init tutoring info section variables with media on the left side
        $tutoringInfoTitle = "Onze docenten";
        $tutoringInfoDescription = "Door de groei van Bijles Aan Huis hebben we leerlingen van de basisschool en middelbare school uit steeds meer steden mogen helpen met betere schoolcijfers. Voor de meeste vakken hebben we in de volgende steden, en woonplaatsen daaromheen, direct een docent beschikbaar: Amstelveen, Helmond, Arnhem, Alphen aan den Rijn, Breda, Nijmegen, Den Haag, Maastricht, Eindhoven, Delft, Rotterdam, Leiden, Groningen, Utrecht, Amsterdam, Dordrecht, Hengelo, Enschede en Tilburg. Onze wiskunde selectie staat op dit moment klaar om hulp te bieden bij wiskunde onderwerpen zoals vergelijkingen oplossen, differentiëren, meetkunde en het uitrekenen van de onbekende ‘X’.";
        $tutorInfoShowTutors = null;
        $tutorInfoImageAltText = "Tutors";
        $tutorInfoImageSrc = "https://bijlesaanhuis.nl/s3/content/Bijles-docenten-wit-e1558561656245.jpg";
        $tutorInfoImageSrcSet = array(
            array(
                "src" => "https://bijlesaanhuis.nl/s3/content/Mobile-docenten-dummy-wit-e1565550934924.jpg",
                "maxScreenSize" => "991",
                "minScreenSize" => null
            ),
            array(
                "src" => "https://bijlesaanhuis.nl/s3/content/Bijles-docenten-wit-e1558561656245.jpg",
                "maxScreenSize" => null,
                "minScreenSize" => "992"
            )
        );
        $tutorInfoMediaLeft = true;

        require($pathToTemplateDirectory.'components/tutoring-info-section.php');
```


- **Example with clickable tutor cards**
```php
        // init tutoring info section variables with media on the left side
        $tutoringInfoTitle = "Onze docenten";
        $tutoringInfoDescription = "Door de groei van Bijles Aan Huis hebben we leerlingen van de basisschool en middelbare school uit steeds meer steden mogen helpen met betere schoolcijfers. Voor de meeste vakken hebben we in de volgende steden, en woonplaatsen daaromheen, direct een docent beschikbaar: Amstelveen, Helmond, Arnhem, Alphen aan den Rijn, Breda, Nijmegen, Den Haag, Maastricht, Eindhoven, Delft, Rotterdam, Leiden, Groningen, Utrecht, Amsterdam, Dordrecht, Hengelo, Enschede en Tilburg. Onze wiskunde selectie staat op dit moment klaar om hulp te bieden bij wiskunde onderwerpen zoals vergelijkingen oplossen, differentiëren, meetkunde en het uitrekenen van de onbekende ‘X’.";
        $tutorInfoShowTutors = true;

        require($pathToTemplateDirectory.'components/tutoring-info-section.php');
```

---
