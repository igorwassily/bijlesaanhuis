<?php

namespace Template;

use Doctrine\DBAL\Connection;

abstract class Component
{
	protected $db;
	protected $params;
	protected $tpl;

	function __construct(Connection $pDB, string $pTpl, array $pParams = [])
	{
		$this->db = $pDB;
		$this->tpl = $pTpl;
		$this->params = $pParams;
	}

	public abstract function Build(): Content;

	public function getTpl(): string
	{
		return $this->tpl;
	}

	public function HasAction(): bool
	{
		return false;
	}

}
