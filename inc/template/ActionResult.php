<?php

namespace Template;

class ActionResult
{
	public const SUCCESS = 1;
	public const INFO = 2;
	public const WARNING = 3;
	public const ERROR = 4;

	private const LEVEL = [
		self::SUCCESS => 'success',
		self::INFO => 'info',
		self::WARNING => 'warning',
		self::ERROR => 'error'
	];

	private bool $_applied;
	private bool $_reload;
	private int $_level;
	private string $_message;

	public function __construct($pApplied = false, $pReload = false, int $pLevel = self::SUCCESS, $pMsg = '')
	{
		$this->_applied = $pApplied;
		$this->_reload = $pReload;
		$this->_level = $pLevel;
		$this->_message = $pMsg;
	}

	/**
	 * @return bool
	 */
	public function isApplied(): bool
	{
		return $this->_applied;
	}

	/**
	 * @param bool $applied
	 */
	public function setApplied(bool $applied): void
	{
		$this->_applied = $applied;
	}

	/**
	 * @return bool
	 */
	public function isReload(): bool
	{
		return $this->_reload;
	}

	/**
	 * @param bool $reload
	 */
	public function setReload(bool $reload): void
	{
		$this->_reload = $reload;
	}

	/**
	 * @return int
	 */
	public function getLevel(): int
	{
		return $this->_level;
	}

	/**
	 * @return string
	 */
	public function getLevelStr(): string
	{
		return self::LEVEL[$this->_level];
	}

	/**
	 * @param int $level
	 */
	public function setLevel(int $level): void
	{
		$this->_level = $level;
	}

	/**
	 * @return bool
	 */
	public function hasMessage(): bool
	{
		return !empty($this->_message);
	}

	/**
	 * @return string
	 */
	public function getMessage(): string
	{
		return $this->_message;
	}

	/**
	 * @param string $message
	 */
	public function setMessage(string $message): void
	{
		$this->_message = $message;
	}

	public function toArray()
	{
		return [
			'type' => self::LEVEL[$this->_level],
			'text' => $this->_message
		];
	}
}