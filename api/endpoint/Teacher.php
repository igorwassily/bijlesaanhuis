<?php

namespace API\Endpoint;

require_once '../api/Endpoint.php';
require_once '../inc/Price.php';
require_once 'Geo.php';

use API\Endpoint;
use API\Result;
use Common;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Driver\ResultStatement;
use PDO;
use Price;

class Teacher extends Endpoint
{
	const FALLBACK_LIMIT = 10;
	const FALLBACK_OFFSET = 0;

	/*
	* Parameter:
	* lat (can be empty)
	* long (can be empty)
	* placeName (can be empty)
	* school
	* level
	* year
	* course (Array)
	* offset
	* Limit
	* postCodeEmpty (true/false)
	*/
	public function Search(array $pParams): Result
	{
		$offset = Common::GetParam($pParams, 'offset', 'int', self::FALLBACK_OFFSET);
		$limit = Common::GetParam($pParams, 'limit', 'int', self::FALLBACK_LIMIT);
		$s = Common::GetParam($pParams, 'select', 'bool', true);

		$school = Common::GetParam($pParams, 'school');
		$course = Common::GetParam($pParams, 'course', 'array', []);
		$level = Common::GetParam($pParams, 'level');
		$year = Common::GetParam($pParams, 'year');

		$sql = "SELECT value FROM variables WHERE name='freeTravelRange'";
		$query = $this->db->executeQuery($sql);
		$freeTravelRange = $query->fetch(PDO::FETCH_NUM)[0];

		$selectGroup = '';
		$select = '';
		$where = '';
		$having = '';
		$whereCountCourses = '';

		$selectGroup .= 't.userID, t.onlineteaching, t.travelRange, t.teacherlevel_rate_ID';
		$selectGroup .= ', c.firstname, c.lastname, c.latitude, c.longitude';
		$selectGroup .= ', tlr.cost_per_km, tlr.level, tlr.rate';
		$selectGroup .= ', p.image, p.title, p.shortdescription';

		$where .= 't.allownewstudents = 1 AND t.isConfirmed = 1 AND t.teacherlevel_rate_ID IS NOT NULL';
		$where .= ' AND u.active = 1';
		$where .= " AND (c.latitude IS NOT NULL AND c.latitude <> '' AND c.longitude IS NOT NULL AND c.longitude <> '' OR t.onlineteaching = 1)";

		$order = 'ORDER BY cat ASC'; // ASC

		// Search for given school criteria
		if (!empty($school)) {
			$whereCountCourses .= "tc.schooltypeID={$school}";
		}

		if (!empty($level)) {
			if (!empty($whereCountCourses)) $whereCountCourses .= ' AND ';
			$whereCountCourses .= "tc.schoollevelID={$level}";
		}

		if (!empty($year)) {
			if (!empty($whereCountCourses)) $whereCountCourses .= ' AND ';
			$whereCountCourses .= "CONCAT(',', tc.schoolyearID, ',') LIKE '%,{$year},%'";
		}

		if (!empty($course)) {
			$placeholder = "";

			foreach ($pParams['course'] as $sub) {
				if (!empty($placeholder)) {
					$placeholder .= " OR ";
				}
				$placeholder .= "tc.courseID = {$sub}";
			}

			if (!empty($placeholder)) {
				if (!empty($whereCountCourses)) $whereCountCourses .= ' AND ';
				$whereCountCourses .= "(" . $placeholder . ")";
			}
		}
		if (!empty($whereCountCourses)) $where .= " AND $whereCountCourses";

		$sqlCntCourses = '';
		if (empty($course)) {
			$select .= ', 0 as cnt';
		} else {
			if (!empty($whereCountCourses)) $whereCountCourses = "WHERE $whereCountCourses";
			$sqlCntCourses = "LEFT JOIN (SELECT tc.teacherID, COUNT(*) as cnt FROM teacher_courses tc $whereCountCourses GROUP BY tc.teacherID) tcj ON tcj.teacherID=t.userID";
		}

		// calculate the distance
		$coordinates = $this->_getLatLong($pParams);

		if (is_null($coordinates)) {
			$select .= ', 2 AS cat';
			$where .= " AND t.onlineteaching = 1";
		} else {
			$pParams['postCode'] = $coordinates['postCode'];
			$select .= ", @d:= 111.045 * DEGREES(ACOS(LEAST(
                       COS(RADIANS({$coordinates['latitude']})) * COS(RADIANS(c.latitude)) * COS(RADIANS({$coordinates['longitude']} - c.longitude)) +
                       SIN(RADIANS({$coordinates['latitude']})) * SIN(RADIANS(c.latitude)), 1.0))) AS distance,
            IF(c.latitude IS NULL OR c.latitude = '' OR c.longitude IS NULL OR c.longitude = '', 2, IF(CEIL(@d) > t.travelRange, 2, IF(CEIL(@d) <= 4, 0, 1))) AS cat";

			$having = "HAVING t.onlineteaching IN (1, if(distance < t.travelRange, 0, -1))";
			$order .= ", distance ASC";
		}

		$sqlTpl = "FROM teacher t
         LEFT JOIN user u ON t.userID = u.userID
         LEFT JOIN contact c ON t.userID = c.userID AND c.prmary = 1
         LEFT JOIN profile p ON t.profileID = p.profileID
         LEFT JOIN teacherlevel_rate tlr ON t.teacherlevel_rate_ID = tlr.ID AND tlr.isdeleted <> 1
         LEFT JOIN teacher_courses tc ON t.userID = tc.teacherID AND tc.permission='Accepted'
		 $sqlCntCourses
WHERE $where GROUP BY $selectGroup $having";

		$query = $this->db->executeQuery("SELECT $selectGroup $select $sqlTpl $order LIMIT $limit OFFSET $offset");
		$result = $this->_processing($query, $course, $school, $level, $year, $freeTravelRange, $s);

		$sql = "SELECT COUNT(*) FROM (SELECT t.userID $select $sqlTpl) t";
		$query = $this->db->executeQuery($sql);
		$rows = $query->fetch(PDO::FETCH_NUM)[0];
		$moreResults = $offset + $limit < $rows;

		$searchParams = null;

		if ($s) {
			$searchParams = $this->_urlEncode($pParams);
		}

		return new Result(['data' => $result, 'moreResults' => $moreResults, 'search' => $searchParams]);
	}

	private function _urlEncode(array $pParams): string
	{
		$output = "";

		foreach ($pParams as $key => $value) {
			if (!empty($value) && $key != "select" && $key != 'ajax') {
				$output .= (empty($output) ? '?' : '&');

				if (is_array($value)) {
					$first = true;
					foreach ($value as $val) {
						if (!$first) $output .= "&";
						$first = false;
						$output .= $key . '[]=' . urlencode($val);
					}
				} else {
					$output .= $key . '=' . urlencode($value);
				}
			}
		}

		return $output;
	}

	private function _reSort(array $results): array
	{
		$category = array();
		foreach ($results as $key => $row) {
			$category[$key] = $row['category'];
		}
		array_multisort($category, SORT_ASC, $results);
		return $results;
	}

	private function _processing(ResultStatement $query, array $subjects, $school, $level, $year, $freeTravelRange, bool $courses): array
	{
		$result = [];

		$teacher = 0;

		$stmt = $this->db->prepare("SELECT tc.courseID, c.coursename, c.courselevel, c.coursetype, tc.schoolyearID FROM teacher_courses tc JOIN courses c ON tc.courseID = c.schoolcourseID WHERE tc.teacherID = ? AND c.coursetype = ?");
		$stmt->bindParam(1, $teacher);
		$stmt->bindValue(2, $school);

		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			$entry = [
				'forename' => $row['firstname'],
				'surname' => $row['lastname'],
				'title' => empty($row['title']) ? '' : $row['title'],
				'description' => empty($row['shortdescription']) ? 'Stuur mij een bericht voor meer informatie.' : $row['shortdescription'],
				'teacher' => intval($row['userID']),
				'image' => '/s3/profileImages/' . (empty($row['image']) ? 'profile_av.jpg' : basename($row['image'])),
				'docentType' => $row['level'],
				'rate' => floatval($row['rate']),
				'online' => !empty($row['onlineteaching']),
				'travel' => $row['cat'] == 1 && !empty($row['distance']) && $row['distance'] <= $row['travelRange'] && $row['distance'] > $freeTravelRange,
				'house' => isset($row['distance']) && !empty($row['distance']) && $row['distance'] <= $row['travelRange'],
				'category' => intval($row['cat']),
				'travelCost' => 0,
				'courses' => [],
				'tagged' => [],
			];

			if ($row['cat'] == 1) {
				$entry['travelCost'] = Price::TravelCosts($row['cost_per_km'], $row['distance'], $freeTravelRange);

				if (empty($entry['travelCost'])) {
					$entry['category'] = 0;
					$entry['travel'] = false;
				}
			}

			if ($courses) {
				$teacher = $row['userID'];
				$stmt->execute();

				while ($row2 = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$courseID = $row2['courseID'];
					$courseName = $row2['coursename'];
					if ($row2['coursetype'] == $school && (empty($level) || $row2['courselevel'] == $level) && (empty($year) || strpos(",{$row2['schoolyearID']},", ",$year,") !== false) && in_array($row2['courseID'], $subjects)) {
						if (!isset($entry['tagged'][$courseID])) {
							$entry['tagged'][$courseID] = $row2['coursename'];

							if (isset($entry['courses'][$courseID])) {
								unset($entry['courses'][$courseID]);
							}
						}
					} elseif (!isset($entry['tagged'][$courseID])) {
						$entry['courses'][$courseID] = $courseName;
					}
				}

				$entry['courses'] = array_values($entry['courses']);
				$entry['tagged'] = array_values($entry['tagged']);
			}

			$result[] = $entry;
		}

		$result = $this->_reSort($result);

		return $result;
	}

	public function Criteria($pParams): Result
	{
		$result = [];

		$school = Common::GetParam($pParams, 'school');
		$course = Common::GetParam($pParams, 'course', 'array', []);
		$level = Common::GetParam($pParams, 'level');
		$year = Common::GetParam($pParams, 'year');
		$select = Common::GetParam($pParams, 'select', 'bool', true);

		try {
			$query = $this->db->executeQuery("SELECT typeID, typeName FROM schooltype ORDER BY typeName");
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
				$result['school'][] = [
					'id' => $row['typeID'],
					'text' => $row['typeName'],
					'selected' => $row['typeID'] == $school && $select,
				];
			}
		} catch (DBALException $e) {
		}

		$years = '';
		$addedCourses = [];
		$addedLevels = [];

		try {
			$query = $this->db->executeQuery("SELECT c.schoolcourseID, c.coursename, c.courselevel, l.levelName, GROUP_CONCAT(c.courseyear) AS 'years' FROM courses c LEFT JOIN schoollevel l ON l.levelID=c.courselevel WHERE c.coursetype=$school GROUP BY c.schoolcourseID, c.coursename, c.courselevel ORDER BY c.coursename");
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
				$courseSelected = in_array($row['schoolcourseID'], $course);
				$yearSelected = strpos(",{$row['years']},", ",$year,") !== false;
				$levelSelected = $row['courselevel'] == $level;

				if (empty($course) || $courseSelected) {
					if ((!isset($addedLevels[$row['courselevel']]) && (empty($year) || $yearSelected) && !empty($row['levelName']))) {
						$addedLevels[$row['courselevel']] = true;
						$result['level'][] = [
							'id' => $row['courselevel'],
							'text' => $row['levelName'],
							'selected' => $levelSelected && $select
						];
					}
					if ((empty($level) || $levelSelected) && !empty($row['years'])) {
						$years .= $row['years'];
					}
				}
				if ((empty($year) || $yearSelected)
					&& (empty($level) || $levelSelected)
					&& !isset($addedCourses[$row['schoolcourseID']])) {
					$addedCourses[$row['schoolcourseID']] = true;
					$result['course'][] = [
						'id' => $row['schoolcourseID'],
						'text' => $row['coursename'],
						'selected' => $courseSelected && $select,
					];
				}
			}
		} catch (DBALException $e) {
		}

		if (!empty($years)) {
			try {
				$query = $this->db->executeQuery("SELECT yearID, yearName FROM schoolyear WHERE yearID IN ($years)");
				while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
					$result['year'][] = [
						'id' => $row['yearID'],
						'text' => $row['yearName'],
						'selected' => $year == $row['yearID'] && $select,
					];
				}
			} catch (DBALException $e) {
				while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
					$result['year'][] = [
						'id' => $row['yearID'],
						'text' => $row['yearName'],
						'selected' => $year == $row['yearID'] && $select,
					];
				}
			}
		}
		//$result['years'] = $years;
		$result['search'] = $this->_urlEncode($pParams);

		return new Result($result);
	}

	private function _getLatLong($pParams): ?array
	{
		if ($this->_student) {
			try {
				$query = $this->db->executeQuery("SELECT postalcode AS postCode, latitude, longitude FROM contact WHERE userID={$this->_userID} AND prmary=1 AND latitude IS NOT NULL AND latitude <> '' AND longitude IS NOT NULL AND longitude <> ''");
				$row = $query->fetch(PDO::FETCH_ASSOC);
				if (empty($row['postCode'])) $row['postCode'] = '';
				return $row;
			} catch (DBALException $e) {
			}
		}

		$postCode = Common::GetParam($pParams, 'postCode', 'string', '');
		$lat = Common::GetParam($pParams, 'lat', 'float');
		$long = Common::GetParam($pParams, 'long', 'float');


		if ($lat != 0 && $long != 0) {
			return [
				'latitude' => $lat,
				'longitude' => $long,
				'postCode' => $postCode,
			];
		}

		if (!empty($postCode)) {
			$geo = new Geo($this->db, $pParams);
			$result = $geo->Address(['postalCode' => $postCode]);
			if ($result->getStatus() == 200) {
				$r = $result->getResult();
				$r['postCode'] = $r['postalCode'];
				unset($r['postalCode']);
				return $r;
			}
		}

		return null;
	}

}
