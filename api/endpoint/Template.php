<?php

namespace API\Endpoint;

require_once '../api/Endpoint.php';

use API\Endpoint;
use API\Result;
use Common;
use Exception;
use PDO;

class Template extends Endpoint
{
	private $_name;
	private $_tpl;
	private $_subject;
	private $_data;
	private $_type;

	public function Create()
	{
	}

	public function DeleteSection($pParams): Result {
		try {
			$sectionId = Common::GetParam($pParams,'id','int',null);
			if (isset($sectionId)) {
				$sectionId = (int) $sectionId;
				// $query = $this->db->query("DELETE FROM section WHERE `id`=$sectionId");
				$query = $this->db->delete('section', ['id' => $sectionId]);

				$result = new Result();
				if($query > 0) {
					$result = new Result(["success" => true], 200);
				} else {
					$result = new Result(["error" => "error when running SQL statement".print_r($query)]);
				}
				return $result;
				//return new Result($query);
			} else {
				return new Result(["error" => "parameter name needs to be set"], 400);
			}
		} catch (Exception $e) {
			return new Result(["error" => $e->getTraceAsString()], 500);
		}
	}

	public function GetSections($pParams): Result
	{
		try {
			if (isset($pParams['page'])) {
				$page = $pParams['page'];

				$qb = $this->db->createQueryBuilder();
		
				$qb = $qb
				->select('*')
				->from('section','s')
				->where('s.page= ? ')
				->setParameter(0, $page)
				->orderBy('s.position');
				$query = $qb->execute()->fetchAll(PDO::FETCH_ASSOC);
		
				$result = $query;
				return new Result($result);
			} else {
				return new Result(["error" => "parameter name needs to be set"], 400);
			}
		} catch (Exception $e) {
			return new Result(["error" => $e->getTraceAsString()], 500);
		}
	}

	public function GetPageList(): Result
	{
		$qb = $this->db->createQueryBuilder();

		$qb = $qb
		->select('*')
		->from('page')
		->orderBy('url');
		$query = $qb->execute()->fetchAll(PDO::FETCH_ASSOC);

		$result = $query;
		return new Result($result);
	}

	public function Build($url): Result
	{
		$url = implode('/', $url);

		$query = $this->db->createQueryBuilder()
			->select("p.id, p.template, p.url, p.title, p.description, p.socialMediaTitle, p.socialMediaDescription")
			->from("page p")
			->where("p.url=:url")
			->setParameter('url', $url)
			->execute();

		if ($query->rowCount() == 0) {
			$result = $this->Build(['errors', '404']);
			$result->setStatus(404);
			return $result;
		}
		$page = $query->fetch(\PDO::FETCH_ASSOC);
		$result = [
			'page' => $page,
			'sections' => []
		];

		$query = $this->db->createQueryBuilder()
			->select("s.component, s.position, s.data")
			->from("section s")
			->where("s.page=?")
			->setParameter(0, $page['id'])
			->orderBy("s.position")
			->execute();

		while ($query->rowCount() > 0 && $row = $query->fetch(\PDO::FETCH_ASSOC)) {
			$result['sections'][] = $row;
		}

		return new Result($result);
	}

	// public function Test($pParams): Result {
	//     $p1 = Common::GetParam($pParams, 'template_name', 'string', '');

	//     return new Result([]);
	// }

	public function Prepare($pID, array $pData = []): bool
	{
		if (!$this->_build()) return false;

		$data = $this->_retrieveAll($pID);
		if (is_null($data)) return false;
		if (!empty($pData)) {
			$data = array_merge($pData, $data);
		}

		$domain = 'bijlesaanhuis.nl';
		$environment = getenv('ENVIRONMENT');
		if (!empty($environment) && $environment != 'production') {
			$domain = "$environment.$domain";
		}
		$data['domain'] = "https://$domain";

		$this->_data = $data;
		foreach ($data as $key => $value) {
			$this->_tpl = str_replace("#$key#", $value, $this->_tpl);
			$this->_subject = str_replace("#$key#", $value, $this->_subject);
		}
		$this->_tpl = str_replace('#subject#', $this->_subject, $this->_tpl);

		return true;
	}
}
