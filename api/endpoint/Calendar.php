<?php

namespace API\Endpoint;

$cwd = getcwd();
chdir(__DIR__);
require_once '../Endpoint.php';
require_once '../../inc/Email.php';
require_once '../../inc/Price.php';
chdir($cwd);

use API\Endpoint;
use API\Result;
use Common;
use DateInterval;
use DateTime;
use DateTimeZone;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Email;
use Exception;
use PDO;
use Price;

/**
 * Class Calendar - a very simple PHP class for rendering PHP templates
 */
class Calendar extends Endpoint
{
	private int $_lessonDurationMin;
	private int $_lessonDurationMax;
	private int $_lessonDurationStep;

	private int $_consultationDurationMin;
	private int $_consultationDurationMax;
	private int $_consultationDurationStep;

	public function __construct(Connection $pDB, array $pParams = [])
	{
		parent::__construct($pDB, $pParams);

		$query = $pDB->executeQuery("SELECT value FROM variables WHERE name='lessonDurationMin'");
		$this->_lessonDurationMin = (int)$query->fetch(PDO::FETCH_NUM)[0];
		$query = $pDB->executeQuery("SELECT value FROM variables WHERE name='lessonDurationMax'");
		$this->_lessonDurationMax = (int)$query->fetch(PDO::FETCH_NUM)[0];
		$query = $pDB->executeQuery("SELECT value FROM variables WHERE name='lessonDurationStep'");
		$this->_lessonDurationStep = (int)$query->fetch(PDO::FETCH_NUM)[0];
		$query = $pDB->executeQuery("SELECT value FROM variables WHERE name='consultationDurationMin'");
		$this->_consultationDurationMin = (int)$query->fetch(PDO::FETCH_NUM)[0];
		$query = $pDB->executeQuery("SELECT value FROM variables WHERE name='consultationDurationMax'");
		$this->_consultationDurationMax = (int)$query->fetch(PDO::FETCH_NUM)[0];
		$query = $pDB->executeQuery("SELECT value FROM variables WHERE name='consultationDurationStep'");
		$this->_consultationDurationStep = (int)$query->fetch(PDO::FETCH_NUM)[0];
	}

	/**
	 * Simple method to retrieve all bookings that are possible
	 *
	 * @param $pParams array
	 * @return Result Result
	 */
	public function GetFreeSlotsTutor($pParams): Result
	{
		if (empty($this->_teacherID) && empty($this->_studentID)) {
			return new Result([], 400);
		}

		$range = $this->_parseTimeRange($pParams);

		$booked = $this->_getBookedTimeSlots($range['from']->format("Y-m-d"), $range['to']->format('Y-m-d'), false, $this->_studentID);
		$general = $this->_availableSlots(false, $this->_teacher && !empty($this->_studentID));

		$availableTimeSlots = $this->_calcBookableSlots($general, $booked, $range['from'], $range['to'], $this->_lessonDurationStep, $this->_lessonDurationMin, $this->_lessonDurationMax, true, null, $this->_teacher && !empty($this->_studentID));

		if (isset($pParams['from'])) {
			$availableTimeSlots = $this->_convertToBootstrapCalendar($availableTimeSlots, false, $this->_teacherID);
		}

		return new Result($availableTimeSlots);
	}

	/**
	 * Simple method to retrieve all bookings that are possible
	 *
	 * @param $pParams array
	 * @return Result Result
	 */
	public function GetFreeSlotsConsultation($pParams): Result
	{
		$range = $this->_parseTimeRange($pParams);

		$booked = $this->_getBookedTimeSlots($range['from']->format('Y-m-d'), $range['to']->format('Y-m-d'), true);
		$general = $this->_availableSlots(true);

		$availableTimeSlots = $this->_calcBookableSlots($general, $booked, $range['from'], $range['to'], $this->_consultationDurationStep, $this->_consultationDurationMin, $this->_consultationDurationMax, false, null, false, true);

		if (isset($pParams['from'])) {
			$availableTimeSlots = $this->_convertToBootstrapCalendar($availableTimeSlots, false);
			if($this->_admin){
				$absentDays = $this->_absentDays(1);


				foreach($availableTimeSlots['result'] as &$res) {
					$res['absent'] = isset($absentDays[$res['id']]);
				}
			}


		}

		return new Result($availableTimeSlots);
	}

	private function _calcBookableSlots(array $pSlots, array $pBooked, DateTime $pFrom, DateTime $pTo, int $pDuration, int $pMin, int $pMax, bool $pGap, $pDay = null, $pStudentForTeacher = false, $consultation = false)
	{
		$absent = $this->_absentDays($consultation ? 1 : $this->_teacherID);

		$slots = [];

		$day = $pFrom;

		if ($pStudentForTeacher) {
			$today = DateTime::createFromFormat("Y-m-d", date('Y-m-01'), $this->timeZone);
			$today->sub(new DateInterval("P1D"));
			$dateTime = clone $today;
			$today = $today->format('Y-m-d');
		} else {
			$today = date('Y-m-d');
			try {
				$dateTime = new DateTime('now', $this->timeZone);
			} catch (Exception $e) {
				$dateTime = new DateTime();
			}
			$dateTime->add(new DateInterval('P1D'));
		}

		/* MONTHLY TIME RANGE */
		while ($day <= $pTo) {
			// Weekday
			$wd = strtolower($day->format('D'));

			$year = $day->format('Y');
			$month = $day->format('m');
			$d = $day->format('d');
			$date = "$year-$month-$d";

			if (!isset($absent[$date]) && $date > $today && isset($pSlots[$wd])) {
				foreach ($pSlots[$wd] as $slot) {
					$slot['begin']->setDate($year, $month, $d);
					/** @var DateTime $begin */
					$begin = clone $slot['begin'];
					$slot['end']->setDate($year, $month, $d);
					/** @var DateTime $end */
					$end = clone $slot['begin'];
					$end = $end->add(new DateInterval("PT{$pDuration}M"));
					while ($end < $slot['end']) {
						$is = $begin > $dateTime;

						if (!empty($pBooked[$date]) && $is) {
							foreach ($pBooked[$date] as $item) {     // GET BOOKED FOR TOMORROW + FUTURE
								if ($pGap) {
									if ($begin >= $item['begin'] && $begin <= $item['end'] || $end >= $item['begin'] && $end <= $item['end']) {
										$is = false;
										break;
									}
								} else {
									if ($begin >= $item['begin'] && $begin < $item['end'] || $end > $item['begin'] && $end <= $item['end']) {
										$is = false;
										break;
									}
								}
							}
						}

						if ($is) {
							$slots[$date][] = [
								"begin" => $begin->format('H:i'),
								"end" => $end->format('H:i'),
								'min' => $pMin,
							];
						}
						$begin = $begin->add(new DateInterval("PT{$pDuration}M"));
						$end = $end->add(new DateInterval("PT{$pDuration}M"));
					}
				}


				if (!empty($slots[$date])) {
					$globalMax = 0;
					$max = $pDuration;
					$lastBegin = null;
					for ($i = sizeof($slots[$date]) - 1; $i >= 0; $i--) {
						if ($max == $pDuration) {
							$max += $pDuration;
						} else if ($slots[$date][$i]['end'] == $lastBegin) {
							$max += $pDuration;
						} else {
							$max = $pDuration;
						}

						$lastBegin = $slots[$date][$i]['begin'];
						if ($max < $pMin) {
							unset($slots[$date][$i]);
							if (empty($slots[$date])) unset($slots[$date]);
						} else {
							$slots[$date][$i]['max'] = $max > $pMax ? $pMax : $max;
							if ($slots[$date][$i]['max'] > $globalMax) {
								$globalMax = $slots[$date][$i]['max'];
							}
						}
					}
					$globalMax = $globalMax >= 90 ? 90 : $pMin;
					if (isset($lots[$date])) {
						for ($i = sizeof($slots[$date]) - 1; $i >= 0; $i--) {
							if (isset($slots[$date][$i]) && $slots[$date][$i]['max'] >= $globalMax) {
								$slots[$date][$i]['suggested'] = true;
								break;
							}
						}
					}
				}
			}

			if ($pDay != null) break;
			$day = $day->add(new DateInterval('P1D'));
		}

		return $slots;
	}

	private function _absentDays($id): array
	{
		$days = [];

		$query = $this->db->createQueryBuilder()
			->select('datee')
			->from('teacher_absent')
			->where('teacherID=:tid')
			->setParameter('tid', $id)
			->execute();
		if ($query->rowCount()) {
			while ($row = $query->fetch(PDO::FETCH_NUM)) {
				$days[$row[0]] = $row[0];
			}
		}

		return $days;
	}

	/* HELPER FUNCTION */
	private function _getBookedTimeSlots(?string $pFrom, ?string $pTo, $consultation = false, $pStudentID = null, $pAttendeeData = false): array
	{
		$qb = $this->db->createQueryBuilder();
		$qb->select('c.datee, c.starttime, c.endtime, c.accepted');
		$qb->where("c.isSlotCancelled=0 AND (c.accepted=1 OR CONCAT(c.datee, ' ', c.starttime, ':00')>DATE_ADD(NOW(), INTERVAL 1 HOUR))");
		$qb->orderBy("c.datee, c.starttime");
		if ($consultation) {
			$qb->from('admincalendarbooking', 'c');
			$qb->andWhere('teacherID IS NOT NULL');
			$qb->addSelect('c.admincalendarbookingID as id');
		} else {
			$qb->from('calendarbooking', 'c');
			$qb->addSelect('c.calendarbookingID as id');
			if ($pStudentID != null) {
				$qb->andWhere("studentID=:sid AND teacherID IS NOT NULL");
				$qb->setParameter('sid', $this->_studentID);
			} else {
				$qb->andWhere("teacherID=:tid AND studentID IS NOT NULL");
				$qb->setParameter('tid', $this->_teacherID);
			}
		}

		if ($pAttendeeData) {
			$qb->addSelect('a.userID, a.firstname, a.lastname, a.email, a.telephone');
			if ($consultation || $pStudentID != null) {
				$qb->leftJoin('c', 'contact', 'a', 'a.userID=c.teacherID AND a.prmary=1');
				if (!$consultation) {
					$qb->leftJoin('c', 'contact', 's', 's.userID=c.studentID AND s.prmary=1');
					$qb->addSelect('s.place_name');
				}
			} else {
				$qb->leftJoin('c', 'contact', 'a', 'a.userID=c.studentID AND a.prmary=1');
				$qb->addSelect('a.place_name');
			}
		}
		if (!$consultation) {
			$qb->addSelect('c.isClassTaken');
		}

		if (!empty($pFrom)) {
			$qb->andWhere("c.datee>=:from");
			$qb->setParameter('from', $pFrom);
		}
		if (!empty($pTo)) {
			$qb->andWhere("c.datee<=:to");
			$qb->setParameter('to', $pTo);
		}

		$query = $qb->execute();

		$result = [];

		try {
			$now = new DateTime('now', $this->timeZone);
		} catch (Exception $e) {
			$now = new DateTime();
		}

		while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
			$e = [
				'id' => $row['id'],
				'begin' => DateTime::createFromFormat('Y-m-d G:i', $row['datee'] . ' ' . $row['starttime'], $this->timeZone),
				'end' => DateTime::createFromFormat('Y-m-d G:i', $row['datee'] . ' ' . $row['endtime'], $this->timeZone),
				'type' => empty($row['accepted']) ? 'pending' : 'accepted'
			];
			if ($e['begin'] <= $now) {
				if (empty($row['isClassTaken']) || empty($row['accepted'])) {
					continue;
				} else {
					$e['type'] = 'history';
				}
			}

			if ($pAttendeeData) {
				$address = $row['place_name'];
				if (!empty($address)) {
					$pos = strrpos($address, ' ');
					if ($pos !== false) {
						$address = substr($address, 0, $pos);
					}
				}
				$e['surname'] = $row['lastname'];
				$e['forename'] = $row['firstname'];
				$e['email'] = $row['email'];
				$e['telephone'] = $row['telephone'];
				$e['address'] = $address;
				$e['userID'] = intval($row['userID']);
			}

			$result[$row['datee']][] = $e;
		}

		return $result;
	}

	private function _sortBegin(array $array): array
	{
		usort($array, function ($i1, $i2) {
			if ($i1['begin'] == $i1['end']) {
				return 0;
			} elseif ($i2['begin'] > $i1['begin']) {
				return -1;
			} else {
				return 1;
			}
		});

		return $array;
	}

	/* HELPER FUNCTION */
	private function _availableSlots(bool $consultation = false, bool $studentForTeacher = false): array
	{
		$slots = [
			'mon' => [],
			'tue' => [],
			'wed' => [],
			'thu' => [],
			'fri' => [],
			'sat' => [],
			'sun' => [],
		];
		if ($studentForTeacher) {
			foreach ($slots as &$slot) {
				$slot[] = [
					'begin' => DateTime::createFromFormat('H:i', '00:00', $this->timeZone),
					'end' => DateTime::createFromFormat('H:i', '23:45', $this->timeZone)
				];
			}

			return $slots;
		}

		if ($consultation) {
			$sql = "SELECT * FROM `adminslots` LIMIT 1";
			$timeFormat = 'g:i a';
		} else {
			$sql = "SELECT * FROM `teacherslots` WHERE `teacherID`={$this->_teacherID}";
			$timeFormat = 'G:i';
		}
		try {
			$query = $this->db->executeQuery($sql);
		} catch (DBALException $e) {
			return [];
		}
		$row = $query->fetch(PDO::FETCH_ASSOC);
		if (empty($row)) return [];

		foreach ($slots as $day => &$data) {
			$field = $day == 'thu' ? 'thur' : $day;
			$time = $row["{$field}_time"];
			if (!empty($time)) {
				$time = explode('-', $time);


				$d = [
					'begin' => DateTime::createFromFormat($timeFormat, $time[0], $this->timeZone),
					'end' => DateTime::createFromFormat($timeFormat, $time[1], $this->timeZone)
				];

				if ($d['begin'] == false || $d['end'] == false) {
					continue;
				}

				$data[] = $d;
			}

			$times = $consultation ? null : json_decode($row["{$field}_additional"], true);
			$day = $this->_findRightMonth($day);

			if (!empty($times)) {
				$i = 1;
				foreach ($times as $item) {
					if (!isset($time["{$day}$i"])) {
						continue;
					}
					$time = explode(':', $item["{$day}$i"]);


					if (sizeof($time) < 3) {
						continue;
					}

					$d = [
						'begin' => DateTime::createFromFormat($timeFormat, "{$time[0]}:{$time[1]}", $this->timeZone),
						'end' => DateTime::createFromFormat($timeFormat, "{$time[2]}:{$time[3]}", $this->timeZone)
					];

					if ($d['begin'] == false || $d['end'] == false) {
						continue;
					}

					$data[] = $d;
					$i++;
				}
			}
			$data = $this->_sortBegin($data);
		}

		return $slots;
	}

	private function _findRightMonth($day)
	{
		$months = [
			'mon' => 'mon',
			'tue' => 'din',
			'wed' => 'woe',
			'thu' => 'don',
			'fri' => 'vri',
			'sat' => 'zat',
			'sun' => 'zon',
		];

		return $months[$day];
	}

	/**
	 * Simple method to book a timeslot
	 *
	 * @param $pParams
	 * @return Result
	 */
	function BookTutor($pParams): Result
	{
		if (empty($this->_teacherID) || empty($this->_studentID) || empty($pParams['date']) || empty($pParams['time']) || empty($pParams['duration'])) {
			return new Result(['msg' => 'Bad Request'], 400);
		}

		if (!$this->_admin && ($this->_studentID != $this->_userID || !$this->_student) && ($this->_teacherID != $this->_userID || !$this->_teacher)) {
			return new Result([], 403);
		}

		$duration = $pParams['duration'];

		if ($duration < $this->_lessonDurationMin || $duration > $this->_lessonDurationMax || $duration % $this->_lessonDurationStep !== 0) {
			$this->logger->debug('bad request: invalid duration');
			return new Result(['msg' => 'Bad Request'], 400);
		}

		$isFirstBook = $this->_isFirstBook($this->_studentID);
		$this->logger->debug('first book', ['firstBook' => $isFirstBook, 'idStudent' => $this->_studentID]);
		$result = $this->_book($this->_studentID, $this->_teacherID, $pParams['date'], $pParams['time'], $pParams['duration'], $this->_lessonDurationStep, $this->_lessonDurationMin, $this->_lessonDurationMax, true, false, $this->_teacher && !empty($this->_studentID));

		if ($result == null) {
			return new Result(['msg' => 'Slot Not Available'], 404);
		}

		$result['firstBook'] = $isFirstBook;
		return new Result($result);
	}

	/**
	 * method to display popup if that returns true (first book)
	 *
	 * @param $pParams
	 * @return bool
	 */
	private function _isFirstBook($studentID): bool
	{
		$sql = "SELECT count(studentID) as c FROM calendarbooking WHERE studentID={$studentID}";
		try {
			$q = $this->db->executeQuery($sql);
		} catch (DBALException $e) {
			return true;
		}
		$r = $q->fetch(PDO::FETCH_ASSOC);

		return ($r['c'] == 0 ? true : false);
	}

	/**
	 * Simple method to book a timeslot
	 *
	 * @param $pParams
	 * @return Result
	 */
	function BookConsultation($pParams): Result
	{
		if (empty($this->_teacherID) || empty($pParams['date']) || empty($pParams['time'])) {
			return new Result(['msg' => 'Bad Request'], 400);
		}

		$result = $this->_book($this->_teacherID, 1, $pParams['date'], $pParams['time'], $this->_consultationDurationStep, $this->_consultationDurationStep, $this->_consultationDurationMin, $this->_consultationDurationMax, false, true);
		if ($result == null) {
			return new Result(['msg' => 'Slot Not Available'], 404);
		}
		return new Result(['id' => $result['id']]);
	}

	private function _book($pStudent, $pTutor, $pDate, $pTime, $pDuration, int $pADuration, int $pAMin, int $pAMax, bool $pGap, bool $pConsultation = false, $teacherForStudent = false): ?array
	{
		$result = [];

		try {
			$dateObj = DateTime::createFromFormat("Y-m-d H:i", "$pDate $pTime");
			if ($dateObj == false) {
				$this->logger->debug('book: invalid time', ['function' => 'book', 'msg' => 'given date/time is invalid', 'date' => $pDate, 'time' => $pTime]);
				return null;
			}
		} catch (Exception $e) {
			$this->logger->debug('book: invalid time', ['function' => 'book', 'msg' => 'given date/time is invalid', 'date' => $pDate, 'time' => $pTime]);
			return null;
		}

		$slots = $this->_availableSlots($pConsultation, $this->_teacher);
		$this->logger->debug('book: available slots', $slots);

		$booked = $this->_getBookedTimeSlots($pDate, $pDate, $pConsultation, $this->_teacher ? $pStudent : null);
		$this->logger->debug('book: booked slots', $booked);

		$available = $this->_calcBookableSlots($slots, $booked, $dateObj, clone $dateObj, $pADuration, $pAMin, $pAMax, $pGap, null, $this->_teacher && !empty($this->_studentID));
		$this->logger->debug('book: bookable slots', $available);

		$slot = null;
		if (isset($available[$pDate])) {
			foreach ($available[$pDate] as $item) {
				if ($item['begin'] == $pTime) {
					if ($pDuration <= $item['max']) {
						$slot = $item;
					} else {
						$this->logger->notice('book: duration exceeds available time slot', ['function' => 'book', 'date' => $pDate, 'time' => $pTime, 'duration' => $pDuration]);
					}
					break;
				}
			}
		} else {
			$this->logger->notice('book: not available at requested day', ['function' => 'book', 'date' => $pDate, 'time' => $pTime]);
		}

		if ($slot == null) {
			$this->logger->notice('book: not available at requested time', ['function' => 'book', 'date' => $pDate, 'time' => $pTime]);
			return null;
		}

		$end = $dateObj->add(new DateInterval("PT{$pDuration}M"))->format("H:i");
		if ($pConsultation) {
			$sql = "INSERT INTO admincalendarbooking (teacherID, adminID, datee, starttime, endtime, accepted) VALUES ($pStudent, $pTutor, '$pDate', '$pTime', '$end', 0)";
		} else {
			$sql = "SELECT tlr.internal_level FROM teacher t LEFT JOIN teacherlevel_rate tlr ON tlr.ID=t.teacherlevel_rate_ID WHERE t.userID=$pTutor";
			try {
				$level = $this->db->executeQuery($sql)->fetch(PDO::FETCH_NUM)[0];
			} catch (DBALException $e) {
				$this->logger->critical('book: failed to retrieve teacher level rate', ['teacher' => $pTutor, 'exception' => $e]);
				return null;
			}

			$amount = Price::CalcByDuration($this->db, $pTutor, $pStudent, $pDuration);

			$externalBookingAmount = $amount['external'];
			$internalBookingAmount = $amount['internal'];
			$travelCosts = $amount['travelCosts'];
			$distance = $amount['distance'];

			$acc = $teacherForStudent ? 1 : 0;

			$sql = "INSERT INTO calendarbooking (studentID, teacherID, datee, starttime, endtime, internal_bookingamount, external_bookingamount, travel_cost, travel_distance, accepted, isClassTaken, duration, internal_teacher_level) VALUES ($pStudent, $pTutor, '$pDate', '$pTime', '$end', $internalBookingAmount, $externalBookingAmount, $travelCosts, $distance, $acc, $acc, $pDuration, '$level')";

			try {
				$text = $teacherForStudent ? 'Afspraak ingeboekt' : 'Bijlesverzoek ingediend';
				$sender = $teacherForStudent ? $pTutor : $pStudent;
				$receiver = $teacherForStudent ? $pStudent : $pTutor;
				$this->db->query("INSERT INTO email (senderID, receiverID, message, subject, `system`) VALUES ($sender, $receiver, '$text', '', 1)");
			} catch (DBALException $e) {
				$this->logger->critical('book: failed to send chat message', ['sender' => $sender, 'receiver' => $receiver, 'text' => $text, 'exception' => $e]);
				return null;
			}
		}

		try {
			$this->db->exec($sql);
		} catch (DBALException $e) {
			$this->logger->critical('book: failed to save appointment', ['exception' => $e]);
			return null;
		}

		$id = $this->db->lastInsertId();
		$result['id'] = $id;

		if (!$pConsultation) {
			$result['level'] = $amount['level'];
			$result['rate'] = $amount['rate'];
			$result['price'] = $amount['external'];
			$result['duration'] = $pDuration;
			$result['travelCosts'] = $amount['travelCosts'];
			$data = [
				'travel_cost' => $amount['travelCosts'],
				'teacher_level' => $amount['level'],
			];
			if ($this->_teacher) {
				$this->emailTutoring('tutor_books_himself_for_tutor', $id, 'teacher', $data);
				$this->emailTutoring('tutor_books_himself_for_student', $id, 'student', $data);
			} else {
				$this->emailTutoring('received_tutoring_request_for_tutor', $id, 'teacher', $data);
				$this->emailTutoring('confirm_tutoring_request_for_student', $id, 'student', $data);
			}
		}

		return $result;
	}

	/**
	 * Simple method to accept a timeslot
	 *
	 * @param $pParams array
	 * @return Result
	 */
	function AcceptTutor($pParams): Result
	{
		return $this->_accept($pParams, false);
	}

	/**
	 * Simple method to accept a timeslot
	 *
	 * @param $pParams array
	 * @return Result
	 */
	function AcceptConsultation($pParams): Result
	{
		return $this->_accept($pParams, true);
	}

	/**
	 * Simple method to cancel a timeslot
	 *
	 */
	function CancelTutor($pParams)
	{
		return $this->_cancel($pParams, false);
	}

	/**
	 * Simple method to delete a timeslot
	 *
	 */
	function DeleteAppointment($pParams)
	{
		$id = $pParams['id'];
		$q = "DELETE FROM calendarbooking WHERE calendarbookingID={$id}";

		try {
			$r = $this->db->exec($q);
		} catch (DBALException $e) {
			return new Result(['msg' => 'Something went wrong'], 400);
		}

		return new Result(['msg' => 'Success'], 200);
	}

	/**
	 * Simple method to cancel a timeslot
	 *
	 */
	function CancelConsultation($pParams)
	{
		return $this->_cancel($pParams, true);
	}

	private function _accept($pParams, $consultation = false): Result
	{
		if (empty($pParams['id'])) {
			return new Result(['msg' => 'Bad Request'], 400);
		}

		$id = $pParams['id'];

		if (!$consultation) {
			$q = "SELECT teacherID, studentID, datee, starttime FROM calendarbooking WHERE calendarbookingID={$id} AND isSlotCancelled = 0";
		} else {
			$q = "SELECT teacherID FROM admincalendarbooking WHERE admincalendarbookingID={$id} AND isSlotCancelled = 0";
		}

		try {
			$r = $this->db->executeQuery($q);
		} catch (DBALException $e) {
			return new Result([], 404);
		}

		$data = $r->fetch(PDO::FETCH_NUM);

		if (!$this->_admin && ($consultation || $data[0] != $this->_userID)) {
			return new Result([], 403);
		}

		if (!$consultation) {
			$u = "UPDATE calendarbooking SET accepted = 1 WHERE calendarbookingID={$id}";
			try {
				$this->db->exec("INSERT INTO email (senderID, receiverID, message, subject, `system`) VALUES ({$data[0]}, {$data[1]}, 'Accepted appointment {$data[2]} {$data[3]}', '', 1)");
			} catch (DBALException $e) {
			}
			$this->emailTutoring('accepted_tutoring_session_by_tutor', $id, 'student');
			$this->emailTutoring('accepted_tutoring_request_confirmation_for_tutor', $id, 'teacher');
		} else {
			$u = "UPDATE admincalendarbooking SET accepted = 1 WHERE admincalendarbookingID={$id}";
		}

		try {
			$f = $this->db->exec($u);
		} catch (DBALException $e) {
			return new Result([], 404);
		}

		return new Result();
	}

	private function _cancel($pParams, $consultation = false): Result
	{
		if (empty($pParams['id'])) {
			return new Result(['msg' => 'Bad Request'], 400);
		}

		$id = $pParams['id'];

		if (!$consultation) {
			$q = "SELECT teacherID, studentID, datee, starttime, accepted, isSlotCancelled FROM calendarbooking WHERE calendarbookingID={$id}";
		} else {
			$q = "SELECT teacherID FROM admincalendarbooking WHERE admincalendarbookingID={$id}";
		}

		try {
			$r = $this->db->executeQuery($q);
		} catch (DBALException $e) {
			return new Result([], 404);
		}

		$data = $r->fetch(PDO::FETCH_ASSOC);

		if (!$this->_admin && ($consultation || ($data['teacherID'] != $this->_userID || !$this->_teacher) && (!$this->_student || $data['studentID'] != $this->_userID))) {
			return new Result([], 403);
		}

		if (!$consultation) {
			try {
				$begin = DateTime::createFromFormat('Y-m-d H:i', $data['datee'] . ' ' . $data['starttime']);
				$now = new DateTime('now', new DateTimeZone('Europe/Amsterdam'));
			} catch (Exception $e) {
				return new Result([], 500);
			}
			$month = $now->format('Y-m');
			if (!$this->_admin && (!$this->_teacher || $begin->format('Y-m') < $month)
				&& (!$this->_student || $data['accepted'] == 1 && $begin < $now->add(new DateInterval('PT48H')))) {
				return new Result([], 403);
			}
		}

		if (!empty($data['isSlotCancelled'])) {
			return new Result([]);
		}

		if (!$consultation) {
			$u = "UPDATE calendarbooking SET isSlotCancelled = 1 WHERE calendarbookingID={$id}";

			$send = $this->_student ? $data['studentID'] : $data['teacherID'];
			$recv = $this->_student ? $data['teacherID'] : $data['studentID'];
			$text = $this->_student ? 'Bijles geannuleerd' : 'Bijles geannuleerd / geweigerd';
			try {
				$this->db->query("INSERT INTO email (senderID, receiverID, message, subject, `system`) VALUES ($send, $recv, '{$text} {$data['datee']} {$data['starttime']}', '', 1)");
			} catch (DBALException $e) {
			}

			if ($this->_student) {
				if (empty($data['accepted'])) {
					$this->emailTutoring('cancelled_tutoring_request_by_customer_for_tutor', $id, 'teacher');
					$this->emailTutoring('cancelled_tutoring_request_by_student_for_student', $id, 'student');
				} else {
					$this->emailTutoring('cancelled_tutoring_session_by_customer', $id, 'teacher');
					$this->emailTutoring('cancelled_tutoring_session_by_student_for_student', $id, 'student');
				}
			} else {
				if (empty($data['accepted'])) {
					$this->emailTutoring('refused_tutoring_request_confirmation_for_tutor', $id, 'teacher');
					$this->emailTutoring('cancelled_request_by_tutor_for_student', $id, 'student');
				} else {
					$this->emailTutoring('cancelled_tutoring_session_by_tutor_for_tutor', $id, 'teacher');
					$this->emailTutoring('cancelled_tutoring_session_by_tutor', $id, 'student');
				}
			}
		} else {
			$u = "UPDATE admincalendarbooking SET isSlotCancelled = 1 WHERE admincalendarbookingID={$id}";
		}

		try {
			$f = $this->db->exec($u);
		} catch (DBALException $e) {
			return new Result([], 404);
		}

		return new Result();
	}

	public function GetAppointments($pParams = []): Result
	{
		return $this->_appointments($pParams);
	}

	public function GetConsultations($pParams): Result
	{
		return $this->_appointments($pParams, true);
	}

	private function _appointments($pParams, $pConsultation = false)
	{
		$student = false;

		if (!empty($this->_teacherID)) {
			$id = $this->_teacherID;
			if (!$this->_admin && (!$this->_teacher || $id != $this->_userID)) {
				return new Result([], 403);
			}
		} elseif (!empty($this->_studentID)) {
			$id = $this->_studentID;
			$student = true;
			if (!$this->_admin && !$this->_teacher && (!$this->_student || $id != $this->_userID)) {
				return new Result([], 403);
			}
		} elseif ($pConsultation) {
			$id = null;
		} else {
			return new Result([], 400);
		}

		$from = null;
		$to = null;
		$history = Common::GetParam($pParams, 'history', 'bool', false);
		if (!$history) {
			$range = $this->_parseTimeRange($pParams);
			$from = $range['from']->format('Y-m-d');
			$to = $range['to']->format('Y-m-d');
		}

		$appointments = [];

		$booked = $this->_getBookedTimeSlots($from, $to, $pConsultation, $student ? $id : null, true);

		if (!$history && ($this->_teacher || (!$student || $pConsultation) && $this->_admin)) {
			$general = $this->_availableSlots($pConsultation, $this->_teacher && $student);

			if ($pConsultation) {
				$slots = $this->_calcBookableSlots($general, $booked, $range['from'], $range['to'], $this->_consultationDurationStep, $this->_consultationDurationMin, $this->_consultationDurationMax, false);
			} else {
				$slots = $this->_calcBookableSlots($general, $booked, $range['from'], $range['to'], $this->_lessonDurationStep, $this->_lessonDurationMin, $this->_lessonDurationMax, true);
			}

			foreach ($slots as $date => $slot) {
				$appointments[$date]['absent'] = false;
				$appointments[$date]['available'] = [];
			}
			$absent = $this->_absentDays($this->_teacherID);
			foreach ($absent as $date) {
				$appointments[$date]['absent'] = true;
				$appointments[$date]['id'] = $date;
			}

		}

		try {
			$now = new DateTime('now', new DateTimeZone('Europe/Amsterdam'));
		} catch (Exception $e) {
			$now = new DateTime();
		}
		$month = $now->format('Y-m');
		foreach ($booked as $date => $items) {
			foreach ($items as $item) {
				$t = clone $now;
				if (!$history && $item['type'] == 'history') {
					$item['type'] = 'accepted';
				}
				if ($this->_student && $item['type'] == 'accepted') {
					$t = $t->add(new DateInterval('PT48H'));
				}

				$e = $item;

				$e['id'] = intval($item['id']);
				$e['begin'] = $item['begin']->format("H:i");
				$e['end'] = $item['end']->format("H:i");
				$e['duration'] = $this->_getMinutesDifference($item['end'], $item['begin']);

				if ($this->_admin || $this->_teacher && $item['begin']->format('Y-m') >= $month
					|| $this->_student && $item['begin'] >= $t) {
					$e['deletable'] = true;
				} else {
					$e['deletable'] = false;
				}

				$appointments[$date][$item['type']][] = $e;
			}
		}

		if (isset($pParams['from'])) {
			$appointments = $this->_convertToBootstrapCalendar($appointments, true);
		}

		return new Result($appointments);
	}

	public function AddAbsentDay($pParams): Result
	{
		return $this->_editAbsentDay($pParams);
	}

	public function RemoveAbsentDay($pParams): Result
	{
		return $this->_editAbsentDay($pParams, true);
	}

	private function _editAbsentDay($pParams, bool $remove = false): Result
	{
		if (empty($this->_teacherID) || empty($pParams['date'])) {
			return new Result(['msg' => 'Bad Request'], 400);
		}

		$date = $pParams['date'];
		$teacher = $this->_teacherID;

		if (!$this->_admin && ($teacher != $this->_userID || !$this->_teacher)) {
			return new Result([], 403);
		}

		if ($remove) {
			$sql = "DELETE FROM teacher_absent WHERE teacherID={$this->_teacherID} AND datee='$date'";
		} else {
			$sql = "INSERT INTO teacher_absent (teacherID, datee) VALUES ({$this->_teacherID}, '$date')";
		}
		try {
			$this->db->exec($sql);
		} catch (DBALException $e) {
			return new Result([], 500);
		}

		return new Result();
	}

	/**
	 * @param DateTime $a
	 * @param DateTime $b
	 * @return int
	 */
	private function _getMinutesDifference(DateTime $a, DateTime $b): int
	{
		return abs($a->getTimestamp() - $b->getTimestamp()) / 60;
	}

	private function _convertToBootstrapCalendar($appointments, $grouped = false, $tid = NULL)
	{
		$data = [
			'success' => 1,
			'result' => []
		];

		$d = date('Y-m-d');
		$this->dateFormatter->setPattern('d MMMM');
		foreach ($appointments as $date => $list) {
			$a = [
				'id' => $date,
				'day' => $this->dateFormatter->format(DateTime::createFromFormat("Y-m-d", $date)->getTimestamp()),
				'title' => '',
				'start' => strtotime($date, 0) * 1000,
				'end' => (strtotime($date, 0) + 86340) * 1000,
				'url' => '',
				'class' => '',
				'absent' => isset($list['absent']) && $list['absent'],
				'available' => isset($list['available']) && $date >= $d ? $list['available'] : [],
				'pending' => isset($list['pending']) && $date >= $d ? $list['pending'] : [],
				'accepted' => isset($list['accepted']) ? $list['accepted'] : [],
				'tid' => $tid
			];
			if (!empty($list['available'])) {
				$a['available'] = $date >= $d ? $list['available'] : [];
			}
			if (!empty($list['pending'])) {
				$a['pending'] = $date >= $d ? $list['pending'] : [];
			}
			if (!empty($list['accepted'])) {
				$a['accepted'] = $list['accepted'];
			}

			if (!$grouped) {
				$a['available'] = $list;
				$a['class'] = 'event-inverse';
				$a['title'] = 'Beschikbaar';
			} elseif (isset($list['pending'])) {
				$a['class'] = 'event-warning';
				$a['title'] = 'In afwachting';
			} elseif (isset($list['accepted'])) {
				$a['class'] = 'event-info';
				$a['title'] = 'Bijles is geaccepteerd';
			} elseif (isset($list['available'])) {
				$a['class'] = 'event-inverse';
				$a['title'] = 'Beschikbaar';
			} elseif ($a['absent']) {
				$a['title'] = 'Absent';
			} else {
				$a = null;
			}

			if (!is_null($a)) {
				$data['result'][] = $a;
			}
		}

		return $data;
	}

	private function _parseTimeRange(array $pParams): array
	{
		$from = null;
		$to = null;

		$f = Common::GetParam($pParams, 'from');
		$t = Common::GetParam($pParams, 'to');
		$year = Common::GetParam($pParams, 'year');
		$month = Common::GetParam($pParams, 'month');

		if ($month > 0) {
			if ($year <= 0) {
				$year = date('Y');
			}
			$from = DateTime::createFromFormat("Y-m-d", "$year-$month-01");
		} else if ($year > 0) {
			$from = DateTime::createFromFormat("Y-m-d", "$year-01-01");
			$to = DateTime::createFromFormat("Y-m-d", "$year-12-31");
		} else {
			try {
				if ($f > 0) {
					$from = new DateTime('@' . ($f / 1000));
				}
				if ($t > 0) {
					$to = new DateTime('@' . ($t / 1000));
				}
			} catch (Exception $e) {
				// will be fixed below
			}
		}

		if (is_null($from)) {
			$from = new DateTime();
		}
		if (empty($to)) {
			try {
				$to = new DateTime("last day of " . $from->format("F of Y"));
			} catch (Exception $e) {
			}
		}

		return [
			'from' => $from,
			'to' => $to
		];
	}

	private function emailTutoring($pEmailName, $pID, $pTo, $pData = []): bool
	{
		$email = new Email($this->db, $pEmailName);

		if (!$email->Prepare($pID, $pData)) {
			return false;
		}

		return $email->Send($pTo);
	}

	public function ReportTutor($pParams = []): Result
	{
		return $this->_report($pParams);
	}

	public function ReportStudent($pParams): Result
	{
		return $this->_report($pParams, false);
	}

	private function _report(array $pParams, bool $pTeacher = true): Result
	{
		if (!$this->_admin) {
			return new Result([], 403);
		}

		$data = [];

		$time = $this->_parseTimeRange($pParams);
		$from = $time['from'];
		$to = $time['to'];

		$detailed = $pTeacher && !is_null($this->_teacherID) || !$pTeacher && !is_null($this->_studentID);
		$joinField = $pTeacher ^ $detailed ? 'teacherID' : 'studentID';
		$where = '';
		if ($detailed) {
			$where = ' AND ';
			if ($pTeacher) {
				$where .= "cb.teacherID={$this->_teacherID}";
			} else {
				$where .= "cb.studentID={$this->_studentID}";
			}
		}

		if ($detailed) {
			$sql = "SELECT c.userID,
			       c.firstname,
			       c.lastname,
			       cb.datee,
			       cb.starttime,
			       cb.internal_teacher_level AS 'level',
			       cb.endtime,
			       SUBSTRING(SEC_TO_TIME(cb.duration * 60), 1, 5) AS 'duration',
			       cb.internal_bookingamount                      AS 'amount',
			       cb.travel_cost,
			       cb.travel_distance,
       			   cb.teacherID,
       			   cb.studentID
			FROM calendarbooking cb
			         LEFT JOIN contact c on cb.$joinField = c.userID AND c.prmary = 1
			WHERE cb.datee>='{$from->format('Y-m-d')}' AND cb.datee<='{$to->format('Y-m-d')}' AND cb.accepted=1 AND cb.isSlotCancelled=0 $where
			ORDER BY cb.datee, cb.starttime";
		} else {
			$sql = "SELECT t.userID,t.firstname,t.lastname,t.internal_teacher_level,t.internal_teacher_level as level, SUM(t.internal_bookingamount) AS 'amount',SUM(t.tc) AS 'travel_cost',  SUBSTRING(SEC_TO_TIME(SUM(t.duration)*60), 1, 5) AS 'duration'
FROM (
         SELECT c.userID,
                c.firstname,
                c.lastname,
                cb.internal_bookingamount,
                cb.internal_teacher_level,
                IF(ctc.customRate IS NULL, cb.travel_cost, ctc.customRate*cb.travel_distance) AS 'tc',
                cb.duration
         FROM calendarbooking cb
                  LEFT JOIN contact c on cb.$joinField = c.userID AND c.prmary = 1
                  LEFT JOIN custom_travel_costs ctc ON cb.teacherID = ctc.teacherID AND cb.studentID = ctc.studentID
         WHERE cb.datee >= '{$from->format('Y-m-d')}'
           AND cb.datee <= '{$to->format('Y-m-d')}'
           AND cb.accepted = 1
           AND cb.isSlotCancelled = 0 $where
     ) AS t
GROUP BY t.userID, t.firstname, t.lastname, t.internal_teacher_level
ORDER BY t.firstname, t.lastname";


		}

		try {
			$query = $this->db->executeQuery($sql);
			while ($row = $query->fetch(PDO::FETCH_ASSOC)) {
				$travelCosts = $row['travel_cost'];


				if($detailed) {
					$sql = "SELECT customRate FROM custom_travel_costs WHERE teacherID = {$row['teacherID']} AND studentID = {$row['studentID']}";
					try {
						$customTravelCosts = $this->db->executeQuery($sql);
						$res = $customTravelCosts->fetch(PDO::FETCH_ASSOC);

						if ($res && !empty($res['customRate'])) {
							$travelCosts = $res['customRate'] * $row['travel_distance'];
						}

					} catch (DBALException $e) {}
				}

				$d = [];
				$d[$pTeacher ^ $detailed ? 'teacher' : 'student'] = $row['userID'];
				$d['forename'] = $row['firstname'];
				$d['surname'] = $row['lastname'];
				$d['duration'] = $row['duration'];
				$d['amount'] = round($row['amount'], 2);
				$d['travel_cost'] = round($travelCosts, 2);
				$d['sum'] = round($row['amount'] + $travelCosts, 2);
				$d['level'] = $row['level'];
				if ($detailed) {
					$d['date'] = $row['datee'];
					$d['begin'] = $row['starttime'];
					$d['end'] = $row['endtime'];
					$d['travel_distance'] = $row['travel_distance'];
				}
				$data[] = $d;
			}
		} catch (DBALException $e) {
		}

		return new Result($data);
	}

}
