<?php

namespace API\Endpoint;

$pwd = getcwd();
chdir(__DIR__);
require_once '../Endpoint.php';
chdir($pwd);

use API\Endpoint;
use API\Result;
use Common;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\FetchMode;
use Doctrine\DBAL\Statement;
use Geocoder\Exception\Exception;
use Geocoder\Model\Coordinates;
use Geocoder\Provider\GoogleMaps\GoogleMaps;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use Geocoder\StatefulGeocoder;
use Http\Client\Curl\Client;
use PostcodeNl_Api_RestClient;

class Geo extends Endpoint
{
	private $httpClient;
	private $provider;
	private $geoCoder;
	private $geoCoderNl;

	public function __construct(Connection $pDB, array $pParams = [])
	{
		parent::__construct($pDB, $pParams);

		$this->httpClient = new Client();
		$this->provider = new GoogleMaps($this->httpClient, null, 'AIzaSyA8R6xulq--St2WW6XG6mqorjyFjxex05w');
		$this->geoCoder = new StatefulGeocoder($this->provider, 'nl');
		$this->geoCoderNl = new PostcodeNl_Api_RestClient('ASIizK19KFX36BfqBtqO8rEDgS0MWAo3bUWqpGK8KbL', 'Ax0fqPb3aGZEoyFeIRoLfPVNNzzpJXBYuZpwhjqQLm3oFjdqgN');;
	}

	public function Address($pParams): Result
	{
		if (isset($pParams['ajax']) && !$this->_student && !$this->_teacher && !$this->_admin) {
			return new Result([], 403);
		}

		$address = Common::GetParam($pParams, "address", "string", "");
		$postalCode = Common::GetParam($pParams, "postalCode", "string", "");
		$streetNumber = Common::GetParam($pParams, "streetNumber", "string", "");

		if (empty($address) && empty($postalCode)) {
			return new Result([], 400);
		}

		$postalCode = strtoupper(str_replace(' ', '', $postalCode));

		$data = $this->_getCache($postalCode, $streetNumber);
		if ($data !== false) {
			if (isset($data['notFound'])) {
				return new Result([], 404);
			} else {
				return new Result($data);
			}
		}

		if (!empty($postalCode) && !empty($streetNumber)) {
			try {
				$data = $this->geoCoderNl->lookupAddress($postalCode, $streetNumber);
				$data = $this->_formatNl($data);
			} catch (\Exception $e) {
				$this->_setCache($postalCode, $streetNumber, ['notFound' => true]);
				return new Result([], 404);
			}
		} else {
			try {
				$query = GeocodeQuery::create(' ');
				if (!empty($address)) $query = $query->withText($address);
				elseif (!empty($postalCode)) {
					$query = $query->withText('');
					$query = $query->withData("components", "country:NL|postal_code:$postalCode");
				}

				$result = $this->geoCoder->geocodeQuery($query);
				if ($result->count() == 0) {
					$this->_setCache($postalCode, $streetNumber, ['notFound' => true]);
					return new Result([], 404);
				}
			} catch (Exception $e) {
				$this->_setCache($postalCode, $streetNumber, ['notFound' => true]);
				return new Result(['message' => $e->getMessage()], 404);
			}

			$data = $result->first()->toArray();
		}

		$this->_setCache($postalCode, $streetNumber, $data);

		return new Result($data);
	}

	public function Coordinate($pParams): Result
	{
		if (isset($pParams['ajax']) && !$this->_student && !$this->_teacher && !$this->_admin) {
			return new Result([], 403);
		}

		$lat = Common::GetParam($pParams, "lat", "float", 0);
		$long = Common::GetParam($pParams, "long", "float", 0);

		if (empty($lat) && empty($long)) {
			return new Result([], 400);
		}

		try {
			$query = ReverseQuery::create(new Coordinates($lat, $long));
			$result = $this->geoCoder->reverseQuery($query);
			if ($result->count() == 0) {
				return new Result([], 404);
			}
		} catch (Exception $e) {
			return new Result(['message' => $e->getMessage()], 404);
		}

		return new Result($result->first()->toArray());
	}

	private function _getCache($postalCode, $streetNumber)
	{
		$qb = $this->db->createQueryBuilder()
			->select('data')
			->from('postalcode_cache')
			->where('postalCode=?')
			->setParameter(0, $postalCode)
			->setMaxResults(1);
		if (!empty($streetNumber)) {
			$qb = $qb->andWhere('housenum=?')
				->setParameter(1, $streetNumber);
		}

		/** @var Statement $query */
		$query = $qb->execute();
		if ($query->rowCount() > 0) {
			$data = json_decode($query->fetch(FetchMode::NUMERIC)[0], true);
			if (empty($data['providedBy']) && !isset($data['notFound'])) {
				$data = $this->_formatNl($data);
			}

			return $data;
		}

		return false;
	}

	private function _setCache($postalCode, $streetNumber, $data)
	{
		try {
			$this->db->createQueryBuilder()
				->insert('postalcode_cache')
				->setValue('postalcode', '?')
				->setValue('housenum', '?')
				->setValue('data', '?')
				->setParameter(0, strtoupper(str_replace(' ', '', $postalCode)))
				->setParameter(1, empty($streetNumber) ? 0 : $streetNumber)
				->setParameter(2, json_encode($data))
				->execute();
		} catch (DBALException $e) {
			// TODO log
		}
	}

	private function _formatNl(array $data)
	{
		return [
			'providedBy' => 'PostcodeNl',
			'latitude' => $data['latitude'],
			'longitude' => $data['longitude'],
			'streetName' => $data['street'],
			'streetNumber' => $data['houseNumber'],
			'postalCode' => $data['postcode'],
			'locality' => $data['city'],
			'subLocality' => null,
			'bounds' => null,
			'adminLevels' => null,
			'country' => 'Nederland',
			'countryCode' => 'NL'
		];
	}
}
