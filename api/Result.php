<?php

namespace API;

class Result
{
	/** @var $_status int */
	private $_status;

	/** @var $_result array */
	private $_result;

	public function __construct($pResult = [], $pStatus = 200)
	{
		$this->_status = $pStatus;
		$this->_result = $pResult;
	}

	/**
	 * @return int
	 */
	public function getStatus(): int
	{
		return $this->_status;
	}

	/**
	 * @param int $status
	 */
	public function setStatus(int $status): void
	{
		$this->_status = $status;
	}

	/**
	 * @return array
	 */
	public function getResult(): array
	{
		return $this->_result;
	}

	/**
	 * @param array $result
	 */
	public function setResult(array $result): void
	{
		$this->_result = $result;
	}
}