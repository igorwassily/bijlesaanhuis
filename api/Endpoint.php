<?php

namespace API;

use Common;
use DateTimeZone;
use Doctrine\DBAL\Connection;
use IntlDateFormatter;
use Monolog\Formatter\JsonFormatter;
use Monolog\Handler\StreamHandler;
use Monolog\Logger;

require_once 'Result.php';

abstract class Endpoint
{
	protected Logger $logger;
	protected Connection $db;
	protected IntlDateFormatter $dateFormatter;
	protected DateTimeZone $timeZone;

	protected bool $_admin = false;
	protected bool $_teacher = false;
	protected bool $_student = false;
	protected ?int $_userID = null;
	protected ?int $_teacherID = null;
	protected ?int $_studentID = null;

	public function __construct(Connection $pDB, array $pParams = [])
	{
		$this->logger = new Logger('endpoint');
		$streamHandler = new StreamHandler('php://stderr', Logger::DEBUG);
		$streamHandler->setFormatter(new JsonFormatter());
		$this->logger->pushHandler($streamHandler);

		if (empty($pDB)) {
			$this->db = Common::GetDb();
			if (is_null($this->db)) {
				$this->logger->alert('failed to establish connection to database');
				die('["message": "failed to establish connection to database"]');
			}
		} else {
			$this->db = $pDB;
		}

		$this->_teacherID = Common::GetParam($pParams, 'teacherID', 'int', null);
		$this->_studentID = Common::GetParam($pParams, 'studentID', 'int', null);

		if (isset($_SESSION['AdminID'])) {
			$this->_admin = true;
			$this->_userID = $_SESSION['AdminID'];
			$this->logger->debug('signed in as admin', ['id' => $_SESSION['AdminID']]);
		}
		if (isset($_SESSION['TeacherID'])) {
			$this->_teacher = true;
			$this->_userID = $_SESSION['TeacherID'];
			$this->logger->debug('signed in as teacher', ['id' => $_SESSION['TeacherID']]);
		}
		if (isset($_SESSION['StudentID'])) {
			$this->_student = true;
			$this->_userID = $_SESSION['StudentID'];
			$this->logger->debug('signed in as student', ['id' => $_SESSION['StudentID']]);
		}

		$this->dateFormatter = new IntlDateFormatter('nl_NL', IntlDateFormatter::SHORT, IntlDateFormatter::SHORT);
		$this->timeZone = new DateTimeZone('Europe/Amsterdam');
	}

}
