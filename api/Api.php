<?php

namespace API;

use Common;
use Doctrine\DBAL\Connection;

class Api
{
	private const BASE = '/api';

	private array $_url;
	private ?Connection $_db;

	public function __construct()
	{
		$this->_db = Common::GetDb();

		$url = strtolower($_SERVER['REQUEST_URI']);
		$pos = strpos($url, "?");
		if ($pos > 0) $url = substr($url, 0, $pos);
		$pos = strpos($url, self::BASE);
		if ($pos === 0) $url = substr($url, strlen(self::BASE));
		$this->_url = explode('/', trim($url, '/'));
		$this->_url[0] = ucfirst($this->_url[0]);
	}

	public function __destruct()
	{
		$this->_db->close();
	}

	public function Handle() {
		if (is_null($this->_db)) {
			$this->_error(500, 'failed to establish connection to database');
			return;
		}

		if (sizeof($this->_url) < 2) {
			$this->_error(400, 'bad request');
			return;
		}

		$parameters = array_merge($_POST, $_GET);
		if (isset($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'bijlesaanhuis.nl') === false) {
			$parameters['ajax'] = true;
		}

		@include("../api/endpoint/{$this->_url[0]}.php");

		$class = "API\\Endpoint\\{$this->_url[0]}";
		$found = class_exists($class);
		if (!$found) {
			$this->_error(404, "not found");
			return;
		}

		/** @var $instance Endpoint */
		$instance = new $class($this->_db, $parameters);
		$method = $this->_url[1];
		if (!method_exists($instance, $method)) {
			$this->_error(404, "not found");
			return;
		}

		/** @var Result $result */
		$result = $instance->$method($parameters);

		if ($result->getStatus() != 200) {
			http_response_code($result->getStatus());
		}

		echo json_encode($result->getResult(), JSON_INVALID_UTF8_SUBSTITUTE);

	}

	private function _error(int $code, string $msg) {
		http_response_code($code);
		echo '{"message": "' . $msg . '"}';
	}
}