#!/bin/bash
set -e

mkdir -p /var/www/html/css
mkdir -p /var/www/html/js

chown www-data:www-data -R /var/www/html/css
chown www-data:www-data -R /var/www/html/js
chown www-data:www-data -R /var/www/laravel

exec docker-php-entrypoint apache2-foreground
